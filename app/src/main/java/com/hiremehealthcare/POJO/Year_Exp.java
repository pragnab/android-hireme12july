package com.hiremehealthcare.POJO;

/**
 * Created by ADMIN on 28-04-2018.
 */

public class Year_Exp {
    /**
     * year_exp_id : 1
     * year_exp_name : 2017
     */

    private String year_exp_id;
    private String year_exp_name;

    public String getYear_exp_id() {
        return year_exp_id;
    }

    public void setYear_exp_id(String year_exp_id) {
        this.year_exp_id = year_exp_id;
    }

    public String getYear_exp_name() {
        return year_exp_name;
    }

    public void setYear_exp_name(String year_exp_name) {
        this.year_exp_name = year_exp_name;
    }
}
