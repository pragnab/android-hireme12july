package com.hiremehealthcare.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by nareshp on 5/12/2018.
 */

public class ExpirePositionModel {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }
    public static class Datum implements Serializable {




        public String getMon() {
            return mon+"";
        }

        public void setMon(String mon) {
            this.mon = mon;
        }

        public String getTue() {
            return tue+"";
        }

        public void setTue(String tue) {
            this.tue = tue;
        }

        public String getWed() {
            return wed+"";
        }

        public void setWed(String wed) {
            this.wed = wed;
        }

        public String getThu() {
            return thu+"";
        }

        public void setThu(String thu) {
            this.thu = thu;
        }

        public String getFri() {
            return fri+"";
        }

        public void setFri(String fri) {
            this.fri = fri;
        }

        public String getSat() {
            return sat+"";
        }

        public void setSat(String sat) {
            this.sat = sat;
        }

        public String getSun() {
            return sun+"";
        }

        public void setSun(String sun) {
            this.sun = sun;
        }

        @SerializedName("mon")
        @Expose
        private String mon;
        @SerializedName("tue")
        @Expose
        private String tue;
        @SerializedName("wed")
        @Expose
        private String wed;
        @SerializedName("thu")
        @Expose
        private String thu;
        @SerializedName("fri")
        @Expose
        private String fri;
        @SerializedName("sat")
        @Expose
        private String sat;
        @SerializedName("sun")
        @Expose
        private String sun;

















        @SerializedName("position_id")
        @Expose
        private String positionId;
        @SerializedName("position_title")
        @Expose
        private String positionTitle;
        @SerializedName("month")
        @Expose
        private String month;
        @SerializedName("year")
        @Expose
        private String year;
        @SerializedName("license_id")
        @Expose
        private String licenseId;
        @SerializedName("license_name")
        @Expose
        private String licenseName;
        @SerializedName("benefits_id")
        @Expose
        private String benefitsId;
        @SerializedName("benefits_name")
        @Expose
        private String benefitsName;
        @SerializedName("extra_benefits_name")
        @Expose
        private Object extraBenefitsName;
        @SerializedName("extra_benefits_checked")
        @Expose
        private Object extraBenefitsChecked;
        @SerializedName("facility_id")
        @Expose
        private Object facilityId;
        @SerializedName("facility_name")
        @Expose
        private Object facilityName;
        @SerializedName("facility_address")
        @Expose
        private Object facilityAddress;
        @SerializedName("facility_city")
        @Expose
        private Object facilityCity;
        @SerializedName("state")
        @Expose
        private Object state;
        @SerializedName("facility_state")
        @Expose
        private Object facilityState;
        @SerializedName("facility_zipcode")
        @Expose
        private Object facilityZipcode;
        @SerializedName("certificate_name")
        @Expose
        private String certificateName;
        @SerializedName("certificate_id")
        @Expose
        private String certificateId;
        @SerializedName("specialty_id")
        @Expose
        private String specialtyId;
        @SerializedName("specialty_name")
        @Expose
        private String specialtyName;
        @SerializedName("manager_id")
        @Expose
        private String managerId;
        @SerializedName("manager_name")
        @Expose
        private String managerName;
        @SerializedName("shift_id")
        @Expose
        private String shiftId;
        @SerializedName("shift_name")
        @Expose
        private String shiftName;
        @SerializedName("hour_id")
        @Expose
        private String hourId;
        @SerializedName("hour_name")
        @Expose
        private String hourName;
        @SerializedName("position_description")
        @Expose
        private String positionDescription;

        public String getPositionId() {
            return positionId;
        }

        public void setPositionId(String positionId) {
            this.positionId = positionId;
        }

        public String getPositionTitle() {
            return positionTitle;
        }

        public void setPositionTitle(String positionTitle) {
            this.positionTitle = positionTitle;
        }

        public String getMonth() {
            return month;
        }

        public void setMonth(String month) {
            this.month = month;
        }

        public String getYear() {
            return year;
        }

        public void setYear(String year) {
            this.year = year;
        }

        public String getLicenseId() {
            return licenseId;
        }

        public void setLicenseId(String licenseId) {
            this.licenseId = licenseId;
        }

        public String getLicenseName() {
            return licenseName;
        }

        public void setLicenseName(String licenseName) {
            this.licenseName = licenseName;
        }

        public String getBenefitsId() {
            return benefitsId;
        }

        public void setBenefitsId(String benefitsId) {
            this.benefitsId = benefitsId;
        }

        public String getBenefitsName() {
            return benefitsName;
        }

        public void setBenefitsName(String benefitsName) {
            this.benefitsName = benefitsName;
        }

        public Object getExtraBenefitsName() {
            return extraBenefitsName;
        }

        public void setExtraBenefitsName(Object extraBenefitsName) {
            this.extraBenefitsName = extraBenefitsName;
        }

        public Object getExtraBenefitsChecked() {
            return extraBenefitsChecked;
        }

        public void setExtraBenefitsChecked(Object extraBenefitsChecked) {
            this.extraBenefitsChecked = extraBenefitsChecked;
        }

        public Object getFacilityId() {
            return facilityId;
        }

        public void setFacilityId(Object facilityId) {
            this.facilityId = facilityId;
        }

        public Object getFacilityName() {
            return facilityName;
        }

        public void setFacilityName(Object facilityName) {
            this.facilityName = facilityName;
        }

        public Object getFacilityAddress() {
            return facilityAddress;
        }

        public void setFacilityAddress(Object facilityAddress) {
            this.facilityAddress = facilityAddress;
        }

        public Object getFacilityCity() {
            return facilityCity;
        }

        public void setFacilityCity(Object facilityCity) {
            this.facilityCity = facilityCity;
        }

        public Object getState() {
            return state;
        }

        public void setState(Object state) {
            this.state = state;
        }

        public Object getFacilityState() {
            return facilityState;
        }

        public void setFacilityState(Object facilityState) {
            this.facilityState = facilityState;
        }

        public Object getFacilityZipcode() {
            return facilityZipcode;
        }

        public void setFacilityZipcode(Object facilityZipcode) {
            this.facilityZipcode = facilityZipcode;
        }

        public String getCertificateName() {
            return certificateName;
        }

        public void setCertificateName(String certificateName) {
            this.certificateName = certificateName;
        }

        public String getCertificateId() {
            return certificateId;
        }

        public void setCertificateId(String certificateId) {
            this.certificateId = certificateId;
        }

        public String getSpecialtyId() {
            return specialtyId;
        }

        public void setSpecialtyId(String specialtyId) {
            this.specialtyId = specialtyId;
        }

        public String getSpecialtyName() {
            return specialtyName;
        }

        public void setSpecialtyName(String specialtyName) {
            this.specialtyName = specialtyName;
        }

        public String getManagerId() {
            return managerId;
        }

        public void setManagerId(String managerId) {
            this.managerId = managerId;
        }

        public String getManagerName() {
            return managerName;
        }

        public void setManagerName(String managerName) {
            this.managerName = managerName;
        }

        public String getShiftId() {
            return shiftId;
        }

        public void setShiftId(String shiftId) {
            this.shiftId = shiftId;
        }

        public String getShiftName() {
            return shiftName;
        }

        public void setShiftName(String shiftName) {
            this.shiftName = shiftName;
        }

        public String getHourId() {
            return hourId;
        }

        public void setHourId(String hourId) {
            this.hourId = hourId;
        }

        public String getHourName() {
            return hourName;
        }

        public void setHourName(String hourName) {
            this.hourName = hourName+"";
        }

        public String getPositionDescription() {
            return positionDescription;
        }

        public void setPositionDescription(String positionDescription) {
            this.positionDescription = positionDescription;
        }

    }

}
