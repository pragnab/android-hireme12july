package com.hiremehealthcare.POJO;

/**
 * Created by nareshp on 5/3/2018.
 */

public class GettingBillingDetailModel {

    private String message;

    private int status;

    private Data data;

    public String getMessage ()
    {
        return message;
    }

    public void setMessage (String message)
    {
        this.message = message;
    }

    public int getStatus ()
    {
        return status;
    }

    public void setStatus (int status)
    {
        this.status = status;
    }

    public Data getData ()
    {
        return data;
    }

    public void setData (Data data)
    {
        this.data = data;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [message = "+message+", status = "+status+", data = "+data+"]";
    }

    public class Data
    {
        private String facility_name;

        private String facility_address;

        public String getFacility_name ()
        {
            return facility_name;
        }

        public void setFacility_name (String facility_name)
        {
            this.facility_name = facility_name;
        }

        public String getFacility_address ()
        {
            return facility_address;
        }

        public void setFacility_address (String facility_address)
        {
            this.facility_address = facility_address;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [facility_name = "+facility_name+", facility_address = "+facility_address+"]";
        }
    }

}
