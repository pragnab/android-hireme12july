package com.hiremehealthcare.POJO;

import java.util.List;

/**
 * Created by ADMIN on 27-04-2018.
 */

public class Hours {

    /**
     * status : 1
     * total : 5
     * hours : [{"hours_id":"1","hours_name":"Full Time"},{"hours_id":"2","hours_name":"Part Time"},{"hours_id":"3","hours_name":"PRN"},{"hours_id":"4","hours_name":"Weekends"},{"hours_id":"5","hours_name":"Temporary"}]
     */

    private int status;
    private int total;
    private List<HoursBean> hours;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<HoursBean> getHours() {
        return hours;
    }

    public void setHours(List<HoursBean> hours) {
        this.hours = hours;
    }

    public static class HoursBean {
        /**
         * hours_id : 1
         * hours_name : Full Time
         */

        private String hours_id;
        private String hours_name;

        public String getHours_id() {
            return hours_id;
        }

        public void setHours_id(String hours_id) {
            this.hours_id = hours_id;
        }

        public String getHours_name() {
            return hours_name;
        }

        public void setHours_name(String hours_name) {
            this.hours_name = hours_name;
        }
    }
}
