package com.hiremehealthcare.POJO;

/**
 * Created by ADMIN on 26-04-2018.
 */

public class Login
{
    /**
     * data : {"user_type":1,"user_id":"1","firstname":"Rushit","zipcode":"10001","redirect":2}
     * message : Successfully login.
     * status : 1
     */

    private DataBean data;
    private String message;
    private int status;

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public static class DataBean {
        /**
         * user_type : 1
         * user_id : 1
         * firstname : Rushit
         * zipcode : 10001
         * redirect : 2
         */

        private int user_type;
        private String user_id;
        private String firstname;
        private String zipcode;
        private int redirect;
        private String redirect_message;
        private String facility_id;

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        private String image;

        public String getCity() {
            return city+"";
        }

        public void setCity(String city) {
            this.city = city+"";
        }

        private String city;

        public String getFacility_id() {
            return facility_id;
        }

        public void setFacility_id(String facility_id) {
            this.facility_id = facility_id;
        }

        public String getRedirect_message() {
            return redirect_message;
        }

        public void setRedirect_message(String redirect_message) {
            this.redirect_message = redirect_message;
        }

        public int getUser_type() {
            return user_type;
        }

        public void setUser_type(int user_type) {
            this.user_type = user_type;
        }

        public String getUser_id() {
            return user_id;
        }

        public void setUser_id(String user_id) {
            this.user_id = user_id;
        }

        public String getFirstname() {
            return firstname;
        }

        public void setFirstname(String firstname) {
            this.firstname = firstname;
        }

        public String getZipcode() {
            return zipcode;
        }

        public void setZipcode(String zipcode) {
            this.zipcode = zipcode;
        }

        public int getRedirect() {
            return redirect;
        }

        public void setRedirect(int redirect) {
            this.redirect = redirect;
        }
    }
}
