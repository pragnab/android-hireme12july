package com.hiremehealthcare.POJO;

import java.util.List;

/**
 * Created by ADMIN on 27-04-2018.
 */

public class Month {

    /**
     * status : 1
     * total : 12
     * month : [{"month_id":"1","month_number":"01","month_name":"JAN"},{"month_id":"2","month_number":"02","month_name":"FEB"},{"month_id":"3","month_number":"03","month_name":"MAR"},{"month_id":"4","month_number":"04","month_name":"APR"},{"month_id":"5","month_number":"05","month_name":"MAY"},{"month_id":"6","month_number":"06","month_name":"JUN"},{"month_id":"7","month_number":"07","month_name":"JUL"},{"month_id":"8","month_number":"08","month_name":"AUG"},{"month_id":"9","month_number":"09","month_name":"SEP"},{"month_id":"10","month_number":"10","month_name":"OCT"},{"month_id":"11","month_number":"11","month_name":"NOV"},{"month_id":"12","month_number":"12","month_name":"DEC"}]
     */

    private int status;
    private int total;
    private List<MonthBean> month;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<MonthBean> getMonth() {
        return month;
    }

    public void setMonth(List<MonthBean> month) {
        this.month = month;
    }

    public static class MonthBean {
        /**
         * month_id : 1
         * month_number : 01
         * month_name : JAN
         */

        private String month_id;
        private String month_number;
        private String month_name;

        public String getMonth_id() {
            return month_id;
        }

        public void setMonth_id(String month_id) {
            this.month_id = month_id;
        }

        public String getMonth_number() {
            return month_number;
        }

        public void setMonth_number(String month_number) {
            this.month_number = month_number;
        }

        public String getMonth_name() {
            return month_name;
        }

        public void setMonth_name(String month_name) {
            this.month_name = month_name;
        }
    }
}
