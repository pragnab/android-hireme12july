package com.hiremehealthcare.POJO;

import java.util.List;

/**
 * Created by nareshp on 5/11/2018.
 */

public class ApplicantCertificate {

    private String status;

    private List<Data> data;

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public List<Data> getData ()
    {
        return data;
    }

    public void setData (List<Data> data)
    {
        this.data = data;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [status = "+status+", data = "+data+"]";
    }

    public class Data
    {
        private String expires_year;

        private String certificate;

        private String expires_label;

        private String applicant_certifications_id;

        private String earned_year;

        private String expires_month;

        private String earned_month;

        public String getExpires_year ()
        {
            return expires_year;
        }

        public void setExpires_year (String expires_year)
        {
            this.expires_year = expires_year;
        }

        public String getCertificate ()
    {
        return certificate;
    }

        public void setCertificate (String certificate)
        {
            this.certificate = certificate;
        }

        public String getExpires_label ()
        {
            return expires_label;
        }

        public void setExpires_label (String expires_label)
        {
            this.expires_label = expires_label;
        }

        public String getApplicant_certifications_id ()
        {
            return applicant_certifications_id;
        }

        public void setApplicant_certifications_id (String applicant_certifications_id)
        {
            this.applicant_certifications_id = applicant_certifications_id;
        }

        public String getEarned_year ()
        {
            return earned_year;
        }

        public void setEarned_year (String earned_year)
        {
            this.earned_year = earned_year;
        }

        public String getExpires_month ()
        {
            return expires_month;
        }

        public void setExpires_month (String expires_month)
        {
            this.expires_month = expires_month;
        }

        public String getEarned_month ()
        {
            return earned_month;
        }

        public void setEarned_month (String earned_month)
        {
            this.earned_month = earned_month;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [expires_year = "+expires_year+", certificate = "+certificate+", expires_label = "+expires_label+", applicant_certifications_id = "+applicant_certifications_id+", earned_year = "+earned_year+", expires_month = "+expires_month+", earned_month = "+earned_month+"]";
        }
    }

}
