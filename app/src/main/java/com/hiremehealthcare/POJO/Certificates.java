package com.hiremehealthcare.POJO;

import java.util.List;

/**
 * Created by ADMIN on 28-04-2018.
 */

public class Certificates {

    /**
     * status : 1
     * total : 7
     * certification : [{"certification_id":"1","certification_name":"RN"},{"certification_id":"2","certification_name":"CNA"},{"certification_id":"3","certification_name":"NRP"},{"certification_id":"4","certification_name":"BSN"},{"certification_id":"5","certification_name":"CRNA"},{"certification_id":"6","certification_name":"CCNA"},{"certification_id":"7","certification_name":"PALS"}]
     */

    private int status;
    private int total;
    private List<CertificationBean> certification;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<CertificationBean> getCertification() {
        return certification;
    }

    public void setCertification(List<CertificationBean> certification) {
        this.certification = certification;
    }

    public static class CertificationBean {
        /**
         * certification_id : 1
         * certification_name : RN
         */

        private String certification_id;
        private String certification_name;

        public String getCertification_id() {
            return certification_id;
        }

        public void setCertification_id(String certification_id) {
            this.certification_id = certification_id;
        }

        public String getCertification_name() {
            return certification_name;
        }

        public void setCertification_name(String certification_name) {
            this.certification_name = certification_name;
        }
    }
}
