package com.hiremehealthcare.POJO;

import java.util.List;

/**
 * Created by nareshp on 5/9/2018.
 */

public class ExperienceListModel {
    private String status;

    private List<Data> data;

    public String getStatus ()
    {
        return status;
    }

    public void setStatus (String status)
    {
        this.status = status;
    }

    public List<Data> getData ()
    {
        return data;
    }

    public void setData (List<Data> data)
    {
        this.data = data;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [status = "+status+", data = "+data+"]";
    }
    public class Data
    {
        private String experience_label;

        private String applicant_experience_location;

        private String speciality;

        private String applicant_experience_id;

        public String getExperience_label ()
        {
            return experience_label;
        }

        public void setExperience_label (String experience_label)
        {
            this.experience_label = experience_label;
        }

        public String getApplicant_experience_location ()
        {
            return applicant_experience_location;
        }

        public void setApplicant_experience_location (String applicant_experience_location)
        {
            this.applicant_experience_location = applicant_experience_location;
        }

        public String getSpeciality ()
        {
            return speciality;
        }

        public void setSpeciality (String speciality)
        {
            this.speciality = speciality;
        }

        public String getApplicant_experience_id ()
        {
            return applicant_experience_id;
        }

        public void setApplicant_experience_id (String applicant_experience_id)
        {
            this.applicant_experience_id = applicant_experience_id;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [experience_label = "+experience_label+", applicant_experience_location = "+applicant_experience_location+", speciality = "+speciality+", applicant_experience_id = "+applicant_experience_id+"]";
        }
    }
}
