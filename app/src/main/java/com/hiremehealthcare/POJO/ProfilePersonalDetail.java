package com.hiremehealthcare.POJO;

/**
 * Created by ADMIN on 03-05-2018.
 */

public class ProfilePersonalDetail {
    /**
     * status : 1
     * data : {"applicant_id":"11","applicant_firstname":"Pragnap","applicant_lastname":"Bhatta","applicant_email":"pragnabhatt.iipl@gmail.com","applicant_password":"dfa2ed8a4c5cc456c90547fda203fc53","applicant_phone":"12345678910","applicant_image":"","applicant_license":"1,2,3","applicant_specialities":"1,4,6","applicant_zipcode":"10000"}
     */

    private int status;
    private DataBean data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * applicant_id : 11
         * applicant_firstname : Pragnap
         * applicant_lastname : Bhatta
         * applicant_email : pragnabhatt.iipl@gmail.com
         * applicant_password : dfa2ed8a4c5cc456c90547fda203fc53
         * applicant_phone : 12345678910
         * applicant_image :
         * applicant_license : 1,2,3
         * applicant_specialities : 1,4,6
         * applicant_zipcode : 10000
         */

        private String applicant_id;
        private String applicant_firstname;
        private String applicant_lastname;
        private String applicant_email;
        private String applicant_password;
        private String applicant_phone;
        private String applicant_image;
        private String applicant_license;
        private String applicant_specialities;
        private String applicant_zipcode;

        public String getApplicant_city_id() {
            return applicant_city_id+"";
        }

        public void setApplicant_city_id(String applicant_city_id) {
            this.applicant_city_id = applicant_city_id;
        }

        private String applicant_city_id;

        public String getApplicant_state_id() {
            return applicant_state_id+"";
        }

        public void setApplicant_state_id(String applicant_state_id) {
            this.applicant_state_id = applicant_state_id;
        }

        private String applicant_state_id;

        public String getApplicant_id() {
            return applicant_id;
        }

        public void setApplicant_id(String applicant_id) {
            this.applicant_id = applicant_id;
        }

        public String getApplicant_firstname() {
            return applicant_firstname;
        }

        public void setApplicant_firstname(String applicant_firstname) {
            this.applicant_firstname = applicant_firstname;
        }

        public String getApplicant_lastname() {
            return applicant_lastname;
        }

        public void setApplicant_lastname(String applicant_lastname) {
            this.applicant_lastname = applicant_lastname;
        }

        public String getApplicant_email() {
            return applicant_email;
        }

        public void setApplicant_email(String applicant_email) {
            this.applicant_email = applicant_email;
        }

        public String getApplicant_password() {
            return applicant_password;
        }

        public void setApplicant_password(String applicant_password) {
            this.applicant_password = applicant_password;
        }

        public String getApplicant_phone() {
            return applicant_phone;
        }

        public void setApplicant_phone(String applicant_phone) {
            this.applicant_phone = applicant_phone;
        }

        public String getApplicant_image() {
            return applicant_image;
        }

        public void setApplicant_image(String applicant_image) {
            this.applicant_image = applicant_image;
        }

        public String getApplicant_license() {
            return applicant_license;
        }

        public void setApplicant_license(String applicant_license) {
            this.applicant_license = applicant_license;
        }

        public String getApplicant_specialities() {
            return applicant_specialities;
        }

        public void setApplicant_specialities(String applicant_specialities) {
            this.applicant_specialities = applicant_specialities;
        }

        public String getApplicant_zipcode() {
            return applicant_zipcode;
        }

        public void setApplicant_zipcode(String applicant_zipcode) {
            this.applicant_zipcode = applicant_zipcode;
        }
    }
}
