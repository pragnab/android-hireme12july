package com.hiremehealthcare.POJO;

/**
 * Created by nareshp on 4/27/2018.
 */

public class RegisteModel {
    /*{
        "message": "Please enter password must be 7 characters long!",
            "status": 0
    }*/

    int status;
    String message;
    private Data data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data
    {
        private int user_type;

        private String zipcode;

        private int user_id;

        private String firstname;

        public int getUser_type ()
        {
            return user_type;
        }

        public void setUser_type (int user_type)
        {
            this.user_type = user_type;
        }

        public String getZipcode ()
        {
            return zipcode;
        }

        public void setZipcode (String zipcode)
        {
            this.zipcode = zipcode;
        }

        public int getUser_id ()
        {
            return user_id;
        }

        public void setUser_id (int user_id)
        {
            this.user_id = user_id;
        }

        public String getFirstname ()
        {
            return firstname;
        }

        public void setFirstname (String firstname)
        {
            this.firstname = firstname;
        }

        @Override
        public String toString()
        {
            return "ClassPojo [user_type = "+user_type+", zipcode = "+zipcode+", user_id = "+user_id+", firstname = "+firstname+"]";
        }
    }
}
