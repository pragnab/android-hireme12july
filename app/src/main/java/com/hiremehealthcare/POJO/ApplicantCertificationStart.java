package com.hiremehealthcare.POJO;

import java.util.List;

public class ApplicantCertificationStart {

    /**
     * data : [{"applicant_certifications_id":"12","certificate":null,"earned_month":"JAN","earned_year":"1970","expires_month":"JUL","expires_year":"2017","expires_label":"JUL/2017"}]
     * status : 1
     */

    private int status;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * applicant_certifications_id : 12
         * certificate : null
         * earned_month : JAN
         * earned_year : 1970
         * expires_month : JUL
         * expires_year : 2017
         * expires_label : JUL/2017
         */

        private String applicant_certifications_id;
        private Object certificate;
        private String earned_month;
        private String earned_year;
        private String expires_month;
        private String expires_year;
        private String expires_label;

        public String getApplicant_certifications_id() {
            return applicant_certifications_id;
        }

        public void setApplicant_certifications_id(String applicant_certifications_id) {
            this.applicant_certifications_id = applicant_certifications_id;
        }

        public Object getCertificate() {
            return certificate;
        }

        public void setCertificate(Object certificate) {
            this.certificate = certificate;
        }

        public String getEarned_month() {
            return earned_month;
        }

        public void setEarned_month(String earned_month) {
            this.earned_month = earned_month;
        }

        public String getEarned_year() {
            return earned_year;
        }

        public void setEarned_year(String earned_year) {
            this.earned_year = earned_year;
        }

        public String getExpires_month() {
            return expires_month;
        }

        public void setExpires_month(String expires_month) {
            this.expires_month = expires_month;
        }

        public String getExpires_year() {
            return expires_year;
        }

        public void setExpires_year(String expires_year) {
            this.expires_year = expires_year;
        }

        public String getExpires_label() {
            return expires_label;
        }

        public void setExpires_label(String expires_label) {
            this.expires_label = expires_label;
        }
    }
}
