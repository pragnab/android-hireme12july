package com.hiremehealthcare.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by nareshp on 5/22/2018.
 */

public class ApplicantLicenseModel {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }
    public class Datum {

        @SerializedName("applicant_license_id")
        @Expose
        private String applicantLicenseId;
        @SerializedName("applicant_license_number")
        @Expose
        private String applicantLicenseNumber;
        @SerializedName("applicant_license_proof")
        @Expose
        private String applicantLicenseProof;
        @SerializedName("expires_label")
        @Expose
        private String expiresLabel;

        public String getApplicantLicenseId() {
            return applicantLicenseId;
        }

        public void setApplicantLicenseId(String applicantLicenseId) {
            this.applicantLicenseId = applicantLicenseId;
        }

        public String getApplicantLicenseNumber() {
            return applicantLicenseNumber;
        }

        public void setApplicantLicenseNumber(String applicantLicenseNumber) {
            this.applicantLicenseNumber = applicantLicenseNumber;
        }

        public String getApplicantLicenseProof() {
            return applicantLicenseProof;
        }

        public void setApplicantLicenseProof(String applicantLicenseProof) {
            this.applicantLicenseProof = applicantLicenseProof;
        }

        public String getExpiresLabel() {
            return expiresLabel;
        }

        public void setExpiresLabel(String expiresLabel) {
            this.expiresLabel = expiresLabel;
        }

    }

}
