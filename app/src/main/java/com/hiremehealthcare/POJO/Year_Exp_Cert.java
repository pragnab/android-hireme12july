package com.hiremehealthcare.POJO;

/**
 * Created by ADMIN on 28-04-2018.
 */

public class Year_Exp_Cert {
    /**
     * year_exp_cer_id : 1
     * year_exp_cer_name : 2010
     */

    private String year_exp_cer_id;
    private String year_exp_cer_name;

    public String getYear_exp_cer_id() {
        return year_exp_cer_id;
    }

    public void setYear_exp_cer_id(String year_exp_cer_id) {
        this.year_exp_cer_id = year_exp_cer_id;
    }

    public String getYear_exp_cer_name() {
        return year_exp_cer_name;
    }

    public void setYear_exp_cer_name(String year_exp_cer_name) {
        this.year_exp_cer_name = year_exp_cer_name;
    }
}
