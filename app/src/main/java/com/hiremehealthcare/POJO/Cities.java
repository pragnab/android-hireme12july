package com.hiremehealthcare.POJO;

import java.util.List;

public class Cities {
    /**
     * message : [{"city_id":"124","city_name":"Adjuntas","city_state_id":"54"},{"city_id":"166","city_name":"Aguada","city_state_id":"54"},{"city_id":"167","city_name":"Aguadilla","city_state_id":"54"},{"city_id":"169","city_name":"Aguas Buenas","city_state_id":"54"},{"city_id":"172","city_name":"Aguirre","city_state_id":"54"},{"city_id":"178","city_name":"Aibonito","city_state_id":"54"},{"city_id":"626","city_name":"Anasco","city_state_id":"54"},{"city_id":"674","city_name":"Angeles","city_state_id":"54"},{"city_id":"850","city_name":"Arecibo","city_state_id":"54"},{"city_id":"960","city_name":"Arroyo","city_state_id":"54"},{"city_id":"1376","city_name":"Bajadero","city_state_id":"54"},{"city_id":"1483","city_name":"Barceloneta","city_state_id":"54"},{"city_id":"1539","city_name":"Barranquitas","city_state_id":"54"},{"city_id":"1676","city_name":"Bayamon","city_state_id":"54"},{"city_id":"2723","city_name":"Boqueron","city_state_id":"54"},{"city_id":"3686","city_name":"Cabo Rojo","city_state_id":"54"},{"city_id":"3708","city_name":"Caguas","city_state_id":"54"},{"city_id":"3923","city_name":"Camuy","city_state_id":"54"},{"city_id":"3981","city_name":"Canovanas","city_state_id":"54"},{"city_id":"4135","city_name":"Carolina","city_state_id":"54"},{"city_id":"4278","city_name":"Castaner","city_state_id":"54"},{"city_id":"4307","city_name":"Catano","city_state_id":"54"},{"city_id":"4352","city_name":"Cayey","city_state_id":"54"},{"city_id":"4431","city_name":"Ceiba","city_state_id":"54"},{"city_id":"4952","city_name":"Ciales","city_state_id":"54"},{"city_id":"4959","city_name":"Cidra","city_state_id":"54"},{"city_id":"5352","city_name":"Coamo","city_state_id":"54"},{"city_id":"5609","city_name":"Comerio","city_state_id":"54"},{"city_id":"5873","city_name":"Corozal","city_state_id":"54"},{"city_id":"5918","city_name":"Coto Laurel","city_state_id":"54"},{"city_id":"6278","city_name":"Culebra","city_state_id":"54"},{"city_id":"7144","city_name":"Dorado","city_state_id":"54"},{"city_id":"8378","city_name":"Ensenada","city_state_id":"54"},{"city_id":"8740","city_name":"Fajardo","city_state_id":"54"},{"city_id":"9121","city_name":"Florida","city_state_id":"54"},{"city_id":"9273","city_name":"Fort Buchanan","city_state_id":"54"},{"city_id":"9919","city_name":"Garrochales","city_state_id":"54"},{"city_id":"11045","city_name":"Guanica","city_state_id":"54"},{"city_id":"11048","city_name":"Guayama","city_state_id":"54"},{"city_id":"11049","city_name":"Guayanilla","city_state_id":"54"},{"city_id":"11050","city_name":"Guaynabo","city_state_id":"54"},{"city_id":"11089","city_name":"Gurabo","city_state_id":"54"},{"city_id":"11631","city_name":"Hatillo","city_state_id":"54"},{"city_id":"12448","city_name":"Hormigueros","city_state_id":"54"},{"city_id":"12620","city_name":"Humacao","city_state_id":"54"},{"city_id":"12997","city_name":"Isabela","city_state_id":"54"},{"city_id":"13195","city_name":"Jayuya","city_state_id":"54"},{"city_id":"13398","city_name":"Juana Diaz","city_state_id":"54"},{"city_id":"13416","city_name":"Juncos","city_state_id":"54"},{"city_id":"14130","city_name":"La Plata","city_state_id":"54"},{"city_id":"14237","city_name":"Lajas","city_state_id":"54"},{"city_id":"14545","city_name":"Lares","city_state_id":"54"},{"city_id":"14561","city_name":"Las Marias","city_state_id":"54"},{"city_id":"14562","city_name":"Las Piedras","city_state_id":"54"},{"city_id":"15358","city_name":"Loiza","city_state_id":"54"},{"city_id":"20581","city_name":"Loiza","city_state_id":"54"},{"city_id":"15689","city_name":"Luquillo","city_state_id":"54"},{"city_id":"20912","city_name":"Luquillo","city_state_id":"54"},{"city_id":"16011","city_name":"Manati","city_state_id":"54"},{"city_id":"21234","city_name":"Manati","city_state_id":"54"},{"city_id":"16229","city_name":"Maricao","city_state_id":"54"},{"city_id":"21452","city_name":"Maricao","city_state_id":"54"},{"city_id":"16515","city_name":"Maunabo","city_state_id":"54"},{"city_id":"21738","city_name":"Maunabo","city_state_id":"54"},{"city_id":"16549","city_name":"Mayaguez","city_state_id":"54"},{"city_id":"21772","city_name":"Mayaguez","city_state_id":"54"},{"city_id":"16972","city_name":"Mercedita","city_state_id":"54"},{"city_id":"22195","city_name":"Mercedita","city_state_id":"54"},{"city_id":"17547","city_name":"Moca","city_state_id":"54"},{"city_id":"22770","city_name":"Moca","city_state_id":"54"},{"city_id":"17897","city_name":"Morovis","city_state_id":"54"},{"city_id":"23120","city_name":"Morovis","city_state_id":"54"},{"city_id":"18344","city_name":"Naguabo","city_state_id":"54"},{"city_id":"23567","city_name":"Naguabo","city_state_id":"54"},{"city_id":"18386","city_name":"Naranjito","city_state_id":"54"},{"city_id":"23609","city_name":"Naranjito","city_state_id":"54"},{"city_id":"19983","city_name":"Orocovis","city_state_id":"54"},{"city_id":"25206","city_name":"Orocovis","city_state_id":"54"},{"city_id":"25500","city_name":"Palmer","city_state_id":"54"},{"city_id":"25708","city_name":"Patillas","city_state_id":"54"},{"city_id":"25939","city_name":"Penuelas","city_state_id":"54"},{"city_id":"26612","city_name":"Ponce","city_state_id":"54"},{"city_id":"27039","city_name":"Puerto Real","city_state_id":"54"},{"city_id":"27058","city_name":"Punta Santiago","city_state_id":"54"},{"city_id":"27112","city_name":"Quebradillas","city_state_id":"54"},{"city_id":"27781","city_name":"Rincon","city_state_id":"54"},{"city_id":"27803","city_name":"Rio Blanco","city_state_id":"54"},{"city_id":"27808","city_name":"Rio Grande","city_state_id":"54"},{"city_id":"28178","city_name":"Roosevelt Roads","city_state_id":"54"},{"city_id":"28189","city_name":"Rosario","city_state_id":"54"},{"city_id":"28497","city_name":"Sabana Grande","city_state_id":"54"},{"city_id":"28498","city_name":"Sabana Hoyos","city_state_id":"54"},{"city_id":"28499","city_name":"Sabana Seca","city_state_id":"54"},{"city_id":"28661","city_name":"Saint Just","city_state_id":"54"},{"city_id":"28781","city_name":"Salinas","city_state_id":"54"},{"city_id":"28842","city_name":"San Antonio","city_state_id":"54"},{"city_id":"28862","city_name":"San German","city_state_id":"54"},{"city_id":"28872","city_name":"San Juan","city_state_id":"54"},{"city_id":"28879","city_name":"San Lorenzo","city_state_id":"54"},{"city_id":"28905","city_name":"San Sebastian","city_state_id":"54"},{"city_id":"28997","city_name":"Santa Isabel","city_state_id":"54"},{"city_id":"31763","city_name":"Toa Alta","city_state_id":"54"},{"city_id":"31764","city_name":"Toa Baja","city_state_id":"54"},{"city_id":"32049","city_name":"Trujillo Alto","city_state_id":"54"},{"city_id":"32405","city_name":"Utuado","city_state_id":"54"},{"city_id":"32568","city_name":"Vega Alta","city_state_id":"54"},{"city_id":"32569","city_name":"Vega Baja","city_state_id":"54"},{"city_id":"32711","city_name":"Vieques","city_state_id":"54"},{"city_id":"32728","city_name":"Villalba","city_state_id":"54"},{"city_id":"34790","city_name":"Yabucoa","city_state_id":"54"},{"city_id":"34821","city_name":"Yauco","city_state_id":"54"}]
     * status : 1
     */

    private int status;
    private List<MessageBean> message;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public List<MessageBean> getMessage() {
        return message;
    }

    public void setMessage(List<MessageBean> message) {
        this.message = message;
    }

    public static class MessageBean {
        /**
         * city_id : 124
         * city_name : Adjuntas
         * city_state_id : 54
         */

        private String city_id;
        private String city_name;
        private String city_state_id;

        public String getCity_id() {
            return city_id;
        }

        public void setCity_id(String city_id) {
            this.city_id = city_id;
        }

        public String getCity_name() {
            return city_name;
        }

        public void setCity_name(String city_name) {
            this.city_name = city_name;
        }

        public String getCity_state_id() {
            return city_state_id;
        }

        public void setCity_state_id(String city_state_id) {
            this.city_state_id = city_state_id;
        }
    }
}
