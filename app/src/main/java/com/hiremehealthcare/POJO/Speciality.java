package com.hiremehealthcare.POJO;

import java.util.List;

/**
 * Created by ADMIN on 28-04-2018.
 */

public class Speciality {


    /**
     * total : 15
     * specialty : [{"specialty_id":"1","specialty_name":"Cardiology"},{"specialty_id":"2","specialty_name":"Neurology"},{"specialty_id":"3","specialty_name":"Dermatology"},{"specialty_id":"4","specialty_name":"Surgery"},{"specialty_id":"5","specialty_name":"PACU"},{"specialty_id":"6","specialty_name":"Radiology"},{"specialty_id":"7","specialty_name":"Oncology"},{"specialty_id":"8","specialty_name":"Emergency"},{"specialty_id":"9","specialty_name":"Med/Surg"},{"specialty_id":"10","specialty_name":"OR"},{"specialty_id":"11","specialty_name":"Orthopedics"},{"specialty_id":"12","specialty_name":"ENT"},{"specialty_id":"13","specialty_name":"Pediatrics"},{"specialty_id":"14","specialty_name":"ICU"},{"specialty_id":"15","specialty_name":"IM"}]
     */

    private int total;
    private List<SpecialtyBean> specialty;

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<SpecialtyBean> getSpecialty() {
        return specialty;
    }

    public void setSpecialty(List<SpecialtyBean> specialty) {
        this.specialty = specialty;
    }

    public static class SpecialtyBean {
        /**
         * specialty_id : 1
         * specialty_name : Cardiology
         */

        private String specialty_id;
        private String specialty_name;

        public String getSpecialty_id() {
            return specialty_id;
        }

        public void setSpecialty_id(String specialty_id) {
            this.specialty_id = specialty_id;
        }

        public String getSpecialty_name() {
            return specialty_name;
        }

        public void setSpecialty_name(String specialty_name) {
            this.specialty_name = specialty_name;
        }
    }
}
