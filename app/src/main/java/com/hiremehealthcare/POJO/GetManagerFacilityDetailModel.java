package com.hiremehealthcare.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by nareshp on 6/5/2018.
 */

public class GetManagerFacilityDetailModel {
    @SerializedName("message")
    @Expose
    private List<Message> message = null;
    @SerializedName("status")
    @Expose
    private Integer status;

    public List<Message> getMessage() {
        return message;
    }

    public void setMessage(List<Message> message) {
        this.message = message;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
    public class Message {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("billing_id")
        @Expose
        private String billingId;
        @SerializedName("name")
        @Expose
        private String name;
        @SerializedName("address")
        @Expose
        private String address;
        @SerializedName("city")
        @Expose
        private String city;
        @SerializedName("state")
        @Expose
        private String state;
        @SerializedName("zipcode")
        @Expose
        private String zipcode;
        @SerializedName("contact_person")
        @Expose
        private String contactPerson;
        @SerializedName("phone")
        @Expose
        private String phone;
        @SerializedName("email")
        @Expose
        private String email;
        @SerializedName("benefits")
        @Expose
        private String benefits;
        @SerializedName("initiation_fee")
        @Expose
        private String initiationFee;
        @SerializedName("initiation_fee_method")
        @Expose
        private String initiationFeeMethod;
        @SerializedName("recurring_fee_amount")
        @Expose
        private String recurringFeeAmount;
        @SerializedName("recurring_fee_frequency")
        @Expose
        private String recurringFeeFrequency;
        @SerializedName("recurring_fee_method")
        @Expose
        private String recurringFeeMethod;
        @SerializedName("maximum_managers")
        @Expose
        private String maximumManagers;
        @SerializedName("fee_status")
        @Expose
        private String feeStatus;
        @SerializedName("transaction_id")
        @Expose
        private String transactionId;
        @SerializedName("image")
        @Expose
        private Object image;
        @SerializedName("status")
        @Expose
        private String status;
        @SerializedName("created_time")
        @Expose
        private String createdTime;
        @SerializedName("modified_time")
        @Expose
        private String modifiedTime;
        @SerializedName("created_ip")
        @Expose
        private String createdIp;
        @SerializedName("modified_ip")
        @Expose
        private String modifiedIp;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getBillingId() {
            return billingId;
        }

        public void setBillingId(String billingId) {
            this.billingId = billingId;
        }

        public String getName() {
            return name;
        }

        public void setName(String name) {
            this.name = name;
        }

        public String getAddress() {
            return address;
        }

        public void setAddress(String address) {
            this.address = address;
        }

        public String getCity() {
            return city;
        }

        public void setCity(String city) {
            this.city = city;
        }

        public String getState() {
            return state;
        }

        public void setState(String state) {
            this.state = state;
        }

        public String getZipcode() {
            return zipcode;
        }

        public void setZipcode(String zipcode) {
            this.zipcode = zipcode;
        }

        public String getContactPerson() {
            return contactPerson;
        }

        public void setContactPerson(String contactPerson) {
            this.contactPerson = contactPerson;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getBenefits() {
            return benefits;
        }

        public void setBenefits(String benefits) {
            this.benefits = benefits;
        }

        public String getInitiationFee() {
            return initiationFee;
        }

        public void setInitiationFee(String initiationFee) {
            this.initiationFee = initiationFee;
        }

        public String getInitiationFeeMethod() {
            return initiationFeeMethod;
        }

        public void setInitiationFeeMethod(String initiationFeeMethod) {
            this.initiationFeeMethod = initiationFeeMethod;
        }

        public String getRecurringFeeAmount() {
            return recurringFeeAmount;
        }

        public void setRecurringFeeAmount(String recurringFeeAmount) {
            this.recurringFeeAmount = recurringFeeAmount;
        }

        public String getRecurringFeeFrequency() {
            return recurringFeeFrequency;
        }

        public void setRecurringFeeFrequency(String recurringFeeFrequency) {
            this.recurringFeeFrequency = recurringFeeFrequency;
        }

        public String getRecurringFeeMethod() {
            return recurringFeeMethod;
        }

        public void setRecurringFeeMethod(String recurringFeeMethod) {
            this.recurringFeeMethod = recurringFeeMethod;
        }

        public String getMaximumManagers() {
            return maximumManagers;
        }

        public void setMaximumManagers(String maximumManagers) {
            this.maximumManagers = maximumManagers;
        }

        public String getFeeStatus() {
            return feeStatus;
        }

        public void setFeeStatus(String feeStatus) {
            this.feeStatus = feeStatus;
        }

        public String getTransactionId() {
            return transactionId;
        }

        public void setTransactionId(String transactionId) {
            this.transactionId = transactionId;
        }

        public Object getImage() {
            return image;
        }

        public void setImage(Object image) {
            this.image = image;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getCreatedTime() {
            return createdTime;
        }

        public void setCreatedTime(String createdTime) {
            this.createdTime = createdTime;
        }

        public String getModifiedTime() {
            return modifiedTime;
        }

        public void setModifiedTime(String modifiedTime) {
            this.modifiedTime = modifiedTime;
        }

        public String getCreatedIp() {
            return createdIp;
        }

        public void setCreatedIp(String createdIp) {
            this.createdIp = createdIp;
        }

        public String getModifiedIp() {
            return modifiedIp;
        }

        public void setModifiedIp(String modifiedIp) {
            this.modifiedIp = modifiedIp;
        }

    }
}
