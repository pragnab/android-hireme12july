package com.hiremehealthcare.POJO;

public class days_data {
    /**
     * status : 1
     * data : Mon, Tue, Wed, Thu, Fri
     */

    private int status;
    private String data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
