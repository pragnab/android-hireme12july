package com.hiremehealthcare.POJO;

import java.util.List;

/**
 * Created by ADMIN on 27-04-2018.
 */
//http://hiremehealthcare.iipldemo.com/app/api.php?type=masters&master_name=benefits
public class Benefits {

    /**
     * status : 1
     * total : 5
     * benefits : [{"benefits_id":"1","benefits_name":"401K","benefits_is_default":"1"},{"benefits_id":"2","benefits_name":"Dental","benefits_is_default":"1"},{"benefits_id":"3","benefits_name":"FSA Account","benefits_is_default":"1"},{"benefits_id":"4","benefits_name":"Healthcare","benefits_is_default":"1"},{"benefits_id":"5","benefits_name":"Moving Expense","benefits_is_default":"1"}] 1, 5, 15, 29, _____,
     * 4
     *
     */

    private int status;
    private int total;
    private List<BenefitsBean> benefits;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<BenefitsBean> getBenefits() {
        return benefits;
    }

    public void setBenefits(List<BenefitsBean> benefits) {
        this.benefits = benefits;
    }

    public static class BenefitsBean {
        /**
         * benefits_id : 1
         * benefits_name : 401K
         * benefits_is_default : 1
         */

        private String benefits_id;
        private String benefits_name;
        private String benefits_is_default;

        public String getBenefits_id() {
            return benefits_id;
        }

        public void setBenefits_id(String benefits_id) {
            this.benefits_id = benefits_id;
        }

        public String getBenefits_name() {
            return benefits_name;
        }

        public void setBenefits_name(String benefits_name) {
            this.benefits_name = benefits_name;
        }

        public String getBenefits_is_default() {
            return benefits_is_default;
        }

        public void setBenefits_is_default(String benefits_is_default) {
            this.benefits_is_default = benefits_is_default;
        }
    }
}
