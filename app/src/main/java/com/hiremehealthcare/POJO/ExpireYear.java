package com.hiremehealthcare.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by nareshp on 5/22/2018.
 */

public class ExpireYear {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("total")
    @Expose
    private Integer total;
    @SerializedName("year_exp")
    @Expose
    private List<YearExp> yearExp = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public List<YearExp> getYearExp() {
        return yearExp;
    }

    public void setYearExp(List<YearExp> yearExp) {
        this.yearExp = yearExp;
    }

    public static class YearExp {

        @SerializedName("year_exp_id")
        @Expose
        private String yearExpId;
        @SerializedName("year_exp_name")
        @Expose
        private String yearExpName;

        public String getYearExpId() {
            return yearExpId;
        }

        public void setYearExpId(String yearExpId) {
            this.yearExpId = yearExpId;
        }

        public String getYearExpName() {
            return yearExpName;
        }

        public void setYearExpName(String yearExpName) {
            this.yearExpName = yearExpName;
        }

    }

}
