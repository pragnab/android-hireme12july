package com.hiremehealthcare.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by nareshp on 6/2/2018.
 */

public class GetInterviewTimeDataModel {

    @SerializedName("message")
    @Expose
    private List<Message> message = null;
    @SerializedName("status")
    @Expose
    private Integer status;

    public List<Message> getMessage() {
        return message;
    }

    public void setMessage(List<Message> message) {
        this.message = message;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public class Message {

        @SerializedName("interview_id")
        @Expose
        private String interviewId;
        @SerializedName("interview_application_id")
        @Expose
        private String interviewApplicationId;
        @SerializedName("interview_interview_datetime")
        @Expose
        private String interviewInterviewDatetime;
        @SerializedName("interview_options")
        @Expose
        private String interviewOptions;
        @SerializedName("interview_is_selected")
        @Expose
        private String interviewIsSelected;

        public String getInterviewId() {
            return interviewId;
        }

        public void setInterviewId(String interviewId) {
            this.interviewId = interviewId;
        }

        public String getInterviewApplicationId() {
            return interviewApplicationId;
        }

        public void setInterviewApplicationId(String interviewApplicationId) {
            this.interviewApplicationId = interviewApplicationId;
        }

        public String getInterviewInterviewDatetime() {
            return interviewInterviewDatetime;
        }

        public void setInterviewInterviewDatetime(String interviewInterviewDatetime) {
            this.interviewInterviewDatetime = interviewInterviewDatetime;
        }

        public String getInterviewOptions() {
            return interviewOptions;
        }

        public void setInterviewOptions(String interviewOptions) {
            this.interviewOptions = interviewOptions;
        }

        public String getInterviewIsSelected() {
            return interviewIsSelected;
        }

        public void setInterviewIsSelected(String interviewIsSelected) {
            this.interviewIsSelected = interviewIsSelected;
        }

    }

}
