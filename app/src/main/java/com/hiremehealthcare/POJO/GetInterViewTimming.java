package com.hiremehealthcare.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by nareshp on 6/1/2018.
 */

public class GetInterViewTimming {

    @SerializedName("message")
    @Expose
    private List<Message> message = null;
    @SerializedName("status")
    @Expose
    private Integer status;

    public List<Message> getMessage() {
        return message;
    }

    public void setMessage(List<Message> message) {
        this.message = message;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public class Message {

        @SerializedName("interview_options")
        @Expose
        private Integer interviewOptions;
        @SerializedName("interview_interview_datetime")
        @Expose
        private String interviewInterviewDatetime;

        @SerializedName("interview_scheduled_id")
        @Expose
        private String interview_scheduled_id;

        public String getInterview_scheduled_id() {
            return interview_scheduled_id;
        }

        public void setInterview_scheduled_id(String interview_scheduled_id) {
            this.interview_scheduled_id = interview_scheduled_id;
        }

        public Integer getInterviewOptions() {
            return interviewOptions;
        }

        public void setInterviewOptions(Integer interviewOptions) {
            this.interviewOptions = interviewOptions;
        }

        public String getInterviewInterviewDatetime() {
            return interviewInterviewDatetime;
        }

        public void setInterviewInterviewDatetime(String interviewInterviewDatetime) {
            this.interviewInterviewDatetime = interviewInterviewDatetime;
        }

    }

}
