package com.hiremehealthcare.POJO;

import java.util.List;

/**
 * Created by ADMIN on 28-04-2018.
 */

public class Year {

    /**
     * status : 1
     * total : 51
     * year : [{"year_id":"1","year_name":"1970"},{"year_id":"2","year_name":"1971"},{"year_id":"3","year_name":"1972"},{"year_id":"4","year_name":"1973"},{"year_id":"5","year_name":"1974"},{"year_id":"6","year_name":"1975"},{"year_id":"7","year_name":"1976"},{"year_id":"8","year_name":"1977"},{"year_id":"9","year_name":"1978"},{"year_id":"10","year_name":"1979"},{"year_id":"11","year_name":"1980"},{"year_id":"12","year_name":"1981"},{"year_id":"13","year_name":"1982"},{"year_id":"14","year_name":"1983"},{"year_id":"15","year_name":"1984"},{"year_id":"16","year_name":"1985"},{"year_id":"17","year_name":"1986"},{"year_id":"18","year_name":"1987"},{"year_id":"19","year_name":"1988"},{"year_id":"20","year_name":"1989"},{"year_id":"21","year_name":"1990"},{"year_id":"22","year_name":"1991"},{"year_id":"23","year_name":"1992"},{"year_id":"24","year_name":"1993"},{"year_id":"25","year_name":"1994"},{"year_id":"26","year_name":"1995"},{"year_id":"27","year_name":"1996"},{"year_id":"28","year_name":"1997"},{"year_id":"29","year_name":"1998"},{"year_id":"30","year_name":"1999"},{"year_id":"31","year_name":"2000"},{"year_id":"32","year_name":"2001"},{"year_id":"33","year_name":"2002"},{"year_id":"34","year_name":"2003"},{"year_id":"35","year_name":"2004"},{"year_id":"36","year_name":"2005"},{"year_id":"37","year_name":"2006"},{"year_id":"38","year_name":"2007"},{"year_id":"39","year_name":"2008"},{"year_id":"40","year_name":"2009"},{"year_id":"41","year_name":"2010"},{"year_id":"42","year_name":"2011"},{"year_id":"43","year_name":"2012"},{"year_id":"44","year_name":"2013"},{"year_id":"45","year_name":"2014"},{"year_id":"46","year_name":"2015"},{"year_id":"47","year_name":"2016"},{"year_id":"48","year_name":"2017"},{"year_id":"49","year_name":"2018"},{"year_id":"50","year_name":"2019"},{"year_id":"51","year_name":"2020"}]
     */

    private int status;
    private int total;
    private List<YearBean> year;

    public void setYear_exp_cer(List<YearBean> year_exp_cer) {
        this.year_exp_cer = year_exp_cer;
    }

    private List<YearBean> year_exp_cer;

    public List<YearBean> getYear_exp_cer() {
        return year_exp_cer;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<YearBean> getYear() {
        return year;
    }

    public void setYear(List<YearBean> year) {
        this.year = year;
    }

    public static class YearBean {
        /**
         * year_id : 1
         * year_name : 1970
         */

        private String year_id;
        private String year_exp_cer_id;
        private String year_name;
        private String year_exp_cer_name;

        public String getYear_exp_cer_id() {
            return year_exp_cer_id;
        }

        public void setYear_exp_cer_id(String year_exp_cer_id) {
            this.year_exp_cer_id = year_exp_cer_id;
        }

        public String getYear_exp_cer_name() {
            return year_exp_cer_name;
        }

        public void setYear_exp_cer_name(String year_exp_cer_name) {
            this.year_exp_cer_name = year_exp_cer_name;
        }

        public String getYear_id() {
            return year_id;
        }

        public void setYear_id(String year_id) {
            this.year_id = year_id;
        }

        public String getYear_name() {
            return year_name;
        }

        public void setYear_name(String year_name) {
            this.year_name = year_name;
        }
    }
}
