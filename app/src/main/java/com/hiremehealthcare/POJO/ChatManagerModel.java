package com.hiremehealthcare.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by nareshp on 5/23/2018.
 */

public class ChatManagerModel {

    @SerializedName("message")
    @Expose
    private List<Message> message = null;
    @SerializedName("status")
    @Expose
    private Integer status;
    private Integer application_status;

    public List<Message> getMessage() {
        return message;
    }

    public void setMessage(List<Message> message) {
        this.message = message;
    }

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Integer getapplication_status() {
        return application_status;
    }

    public void setapplication_status(Integer application_status) {
        this.application_status = application_status;
    }

    public static class Message {

        @SerializedName("user1_id")
        @Expose
        private String user1Id;
        @SerializedName("user2_id")
        @Expose
        private String user2Id;
        @SerializedName("application_id")
        @Expose
        private String applicationId;
        @SerializedName("user1_name")
        @Expose
        private String user1Name;
        @SerializedName("user2_name")
        @Expose
        private String user2Name;
        @SerializedName("created_time")
        @Expose
        private String createdTime;
        @SerializedName("chating_msg")
        @Expose
        private String chatingMsg;
        @SerializedName("chat_user2_user1_id")
        @Expose
        private String chatUser2User1Id;
        @SerializedName("user_image")
        @Expose
        private String userImage;

        public String getUser1Id() {
            return user1Id;
        }

        public void setUser1Id(String user1Id) {
            this.user1Id = user1Id;
        }

        public String getUser2Id() {
            return user2Id;
        }

        public void setUser2Id(String user2Id) {
            this.user2Id = user2Id;
        }

        public String getApplicationId() {
            return applicationId;
        }

        public void setApplicationId(String applicationId) {
            this.applicationId = applicationId;
        }

        public String getUser1Name() {
            return user1Name;
        }

        public void setUser1Name(String user1Name) {
            this.user1Name = user1Name;
        }

        public String getUser2Name() {
            return user2Name;
        }

        public void setUser2Name(String user2Name) {
            this.user2Name = user2Name;
        }

        public String getCreatedTime() {
            return createdTime;
        }

        public void setCreatedTime(String createdTime) {
            this.createdTime = createdTime;
        }

        public String getChatingMsg() {
            return chatingMsg;
        }

        public void setChatingMsg(String chatingMsg) {
            this.chatingMsg = chatingMsg;
        }

        public String getChatUser2User1Id() {
            return chatUser2User1Id;
        }

        public void setChatUser2User1Id(String chatUser2User1Id) {
            this.chatUser2User1Id = chatUser2User1Id;
        }

        public String getUserImage() {
            return userImage;
        }

        public void setUserImage(String userImage) {
            this.userImage = userImage;
        }

    }

}
