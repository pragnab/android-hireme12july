package com.hiremehealthcare.POJO;

import java.util.List;

/**
 * Created by ADMIN on 28-04-2018.
 */

public class Shift {


    /**
     * status : 1
     * total : 3
     * shift : [{"shift_id":"1","shift_name":"Days"},{"shift_id":"2","shift_name":"Nights"},{"shift_id":"3","shift_name":"Evenings"}]
     */

    private int status;
    private int total;
    private List<ShiftBean> shift;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<ShiftBean> getShift() {
        return shift;
    }

    public void setShift(List<ShiftBean> shift) {
        this.shift = shift;
    }

    public static class ShiftBean {
        /**
         * shift_id : 1
         * shift_name : Days
         */

        private String shift_id;
        private String shift_name;

        public String getShift_id() {
            return shift_id;
        }

        public void setShift_id(String shift_id) {
            this.shift_id = shift_id+"";
        }

        public String getShift_name() {
            return shift_name;
        }

        public void setShift_name(String shift_name) {
            this.shift_name = shift_name;
        }
    }
}
