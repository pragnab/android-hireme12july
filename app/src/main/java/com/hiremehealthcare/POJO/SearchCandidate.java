package com.hiremehealthcare.POJO;

import java.util.List;

/**
 * Created by ADMIN on 18-05-2018.
 */

public class SearchCandidate {


    private int status;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {


        private String profile_completed;
        private String candidate_distance;
        private String id;
        private String firstname;
        private String lastname;
        private String email;
        private String password;
        private String phone;
        private String specialities;
        private String license;
        private String zipcode;
        private String image;
        private String status;
        private String applicant_status;
        private String created_time;
        private String modified_time;
        private String created_ip;
        private String modified_ip;
        private String specialities_name;
        private String license_name;

        public String getProfile_matched() {
            return profile_matched;
        }

        public void setProfile_matched(String profile_matched) {
            this.profile_matched = profile_matched;
        }

        private String profile_matched;
        private Object applicant_cerification_id;
        private Object applicant_cerification_cerificate;
        private Object applicant_cerification_cerificate_name;
        private Object applicant_cerification_earned_month;
        private Object applicant_cerification_earned_month_name;
        private Object applicant_cerification_earned_year;
        private Object applicant_cerification_earned_year_name;
        private Object applicant_cerification_expires_month;
        private Object applicant_cerification_expires_month_name;
        private Object applicant_cerification_expires_year;
        private Object applicant_cerification_expires_year_name;
        private Object applicant_cerification_expires_never;
        private Object applicant_education_id;
        private Object applicant_education_institute;
        private Object applicant_education_degree;
        private Object applicant_education_completion_month;
        private Object applicant_education_completion_month_name;
        private Object applicant_education_completion_year;
        private Object applicant_education_completion_year_name;
        private Object applicant_experience_id;
        private Object applicant_experience_location;
        private Object applicant_experience_speciality;
        private Object applicant_experience_speciality_name;
        private Object applicant_experience_duration_years;
        private Object applicant_experience_duration_years_name;
        private Object applicant_experience_duration_months;
        private Object applicant_experience_duration_months_name;
        private Object applicant_license_id;
        private Object applicant_license_proof;
        private Object applicant_license_number;
        private Object applicant_license_experience_month;
        private Object applicant_license_experience_month_name;
        private Object applicant_license_experience_year;
        private Object applicant_license_experience_year_name;

        public String getProfile_completed() {
            return profile_completed;
        }

        public void setProfile_completed(String profile_completed) {
            this.profile_completed = profile_completed;
        }

        public String getCandidate_distance() {
            return candidate_distance;
        }

        public void setCandidate_distance(String candidate_distance) {
            this.candidate_distance = candidate_distance;
        }

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getFirstname() {
            return firstname;
        }

        public void setFirstname(String firstname) {
            this.firstname = firstname;
        }

        public String getLastname() {
            return lastname;
        }

        public void setLastname(String lastname) {
            this.lastname = lastname;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getSpecialities() {
            return specialities;
        }

        public void setSpecialities(String specialities) {
            this.specialities = specialities;
        }

        public String getLicense() {
            return license;
        }

        public void setLicense(String license) {
            this.license = license;
        }

        public String getZipcode() {
            return zipcode;
        }

        public void setZipcode(String zipcode) {
            this.zipcode = zipcode;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getApplicant_status() {
            return applicant_status;
        }

        public void setApplicant_status(String applicant_status) {
            this.applicant_status = applicant_status;
        }

        public String getCreated_time() {
            return created_time;
        }

        public void setCreated_time(String created_time) {
            this.created_time = created_time;
        }

        public String getModified_time() {
            return modified_time;
        }

        public void setModified_time(String modified_time) {
            this.modified_time = modified_time;
        }

        public String getCreated_ip() {
            return created_ip;
        }

        public void setCreated_ip(String created_ip) {
            this.created_ip = created_ip;
        }

        public String getModified_ip() {
            return modified_ip;
        }

        public void setModified_ip(String modified_ip) {
            this.modified_ip = modified_ip;
        }

        public String getSpecialities_name() {
            return specialities_name;
        }

        public void setSpecialities_name(String specialities_name) {
            this.specialities_name = specialities_name;
        }

        public String getLicense_name() {
            return license_name;
        }

        public void setLicense_name(String license_name) {
            this.license_name = license_name;
        }

        public Object getApplicant_cerification_id() {
            return applicant_cerification_id;
        }

        public void setApplicant_cerification_id(Object applicant_cerification_id) {
            this.applicant_cerification_id = applicant_cerification_id;
        }

        public Object getApplicant_cerification_cerificate() {
            return applicant_cerification_cerificate;
        }

        public void setApplicant_cerification_cerificate(Object applicant_cerification_cerificate) {
            this.applicant_cerification_cerificate = applicant_cerification_cerificate;
        }

        public Object getApplicant_cerification_cerificate_name() {
            return applicant_cerification_cerificate_name;
        }

        public void setApplicant_cerification_cerificate_name(Object applicant_cerification_cerificate_name) {
            this.applicant_cerification_cerificate_name = applicant_cerification_cerificate_name;
        }

        public Object getApplicant_cerification_earned_month() {
            return applicant_cerification_earned_month;
        }

        public void setApplicant_cerification_earned_month(Object applicant_cerification_earned_month) {
            this.applicant_cerification_earned_month = applicant_cerification_earned_month;
        }

        public Object getApplicant_cerification_earned_month_name() {
            return applicant_cerification_earned_month_name;
        }

        public void setApplicant_cerification_earned_month_name(Object applicant_cerification_earned_month_name) {
            this.applicant_cerification_earned_month_name = applicant_cerification_earned_month_name;
        }

        public Object getApplicant_cerification_earned_year() {
            return applicant_cerification_earned_year;
        }

        public void setApplicant_cerification_earned_year(Object applicant_cerification_earned_year) {
            this.applicant_cerification_earned_year = applicant_cerification_earned_year;
        }

        public Object getApplicant_cerification_earned_year_name() {
            return applicant_cerification_earned_year_name;
        }

        public void setApplicant_cerification_earned_year_name(Object applicant_cerification_earned_year_name) {
            this.applicant_cerification_earned_year_name = applicant_cerification_earned_year_name;
        }

        public Object getApplicant_cerification_expires_month() {
            return applicant_cerification_expires_month;
        }

        public void setApplicant_cerification_expires_month(Object applicant_cerification_expires_month) {
            this.applicant_cerification_expires_month = applicant_cerification_expires_month;
        }

        public Object getApplicant_cerification_expires_month_name() {
            return applicant_cerification_expires_month_name;
        }

        public void setApplicant_cerification_expires_month_name(Object applicant_cerification_expires_month_name) {
            this.applicant_cerification_expires_month_name = applicant_cerification_expires_month_name;
        }

        public Object getApplicant_cerification_expires_year() {
            return applicant_cerification_expires_year;
        }

        public void setApplicant_cerification_expires_year(Object applicant_cerification_expires_year) {
            this.applicant_cerification_expires_year = applicant_cerification_expires_year;
        }

        public Object getApplicant_cerification_expires_year_name() {
            return applicant_cerification_expires_year_name;
        }

        public void setApplicant_cerification_expires_year_name(Object applicant_cerification_expires_year_name) {
            this.applicant_cerification_expires_year_name = applicant_cerification_expires_year_name;
        }

        public Object getApplicant_cerification_expires_never() {
            return applicant_cerification_expires_never;
        }

        public void setApplicant_cerification_expires_never(Object applicant_cerification_expires_never) {
            this.applicant_cerification_expires_never = applicant_cerification_expires_never;
        }

        public Object getApplicant_education_id() {
            return applicant_education_id;
        }

        public void setApplicant_education_id(Object applicant_education_id) {
            this.applicant_education_id = applicant_education_id;
        }

        public Object getApplicant_education_institute() {
            return applicant_education_institute;
        }

        public void setApplicant_education_institute(Object applicant_education_institute) {
            this.applicant_education_institute = applicant_education_institute;
        }

        public Object getApplicant_education_degree() {
            return applicant_education_degree;
        }

        public void setApplicant_education_degree(Object applicant_education_degree) {
            this.applicant_education_degree = applicant_education_degree;
        }

        public Object getApplicant_education_completion_month() {
            return applicant_education_completion_month;
        }

        public void setApplicant_education_completion_month(Object applicant_education_completion_month) {
            this.applicant_education_completion_month = applicant_education_completion_month;
        }

        public Object getApplicant_education_completion_month_name() {
            return applicant_education_completion_month_name;
        }

        public void setApplicant_education_completion_month_name(Object applicant_education_completion_month_name) {
            this.applicant_education_completion_month_name = applicant_education_completion_month_name;
        }

        public Object getApplicant_education_completion_year() {
            return applicant_education_completion_year;
        }

        public void setApplicant_education_completion_year(Object applicant_education_completion_year) {
            this.applicant_education_completion_year = applicant_education_completion_year;
        }

        public Object getApplicant_education_completion_year_name() {
            return applicant_education_completion_year_name;
        }

        public void setApplicant_education_completion_year_name(Object applicant_education_completion_year_name) {
            this.applicant_education_completion_year_name = applicant_education_completion_year_name;
        }

        public Object getApplicant_experience_id() {
            return applicant_experience_id;
        }

        public void setApplicant_experience_id(Object applicant_experience_id) {
            this.applicant_experience_id = applicant_experience_id;
        }

        public Object getApplicant_experience_location() {
            return applicant_experience_location;
        }

        public void setApplicant_experience_location(Object applicant_experience_location) {
            this.applicant_experience_location = applicant_experience_location;
        }

        public Object getApplicant_experience_speciality() {
            return applicant_experience_speciality;
        }

        public void setApplicant_experience_speciality(Object applicant_experience_speciality) {
            this.applicant_experience_speciality = applicant_experience_speciality;
        }

        public Object getApplicant_experience_speciality_name() {
            return applicant_experience_speciality_name;
        }

        public void setApplicant_experience_speciality_name(Object applicant_experience_speciality_name) {
            this.applicant_experience_speciality_name = applicant_experience_speciality_name;
        }

        public Object getApplicant_experience_duration_years() {
            return applicant_experience_duration_years;
        }

        public void setApplicant_experience_duration_years(Object applicant_experience_duration_years) {
            this.applicant_experience_duration_years = applicant_experience_duration_years;
        }

        public Object getApplicant_experience_duration_years_name() {
            return applicant_experience_duration_years_name;
        }

        public void setApplicant_experience_duration_years_name(Object applicant_experience_duration_years_name) {
            this.applicant_experience_duration_years_name = applicant_experience_duration_years_name;
        }

        public Object getApplicant_experience_duration_months() {
            return applicant_experience_duration_months;
        }

        public void setApplicant_experience_duration_months(Object applicant_experience_duration_months) {
            this.applicant_experience_duration_months = applicant_experience_duration_months;
        }

        public Object getApplicant_experience_duration_months_name() {
            return applicant_experience_duration_months_name;
        }

        public void setApplicant_experience_duration_months_name(Object applicant_experience_duration_months_name) {
            this.applicant_experience_duration_months_name = applicant_experience_duration_months_name;
        }

        public Object getApplicant_license_id() {
            return applicant_license_id;
        }

        public void setApplicant_license_id(Object applicant_license_id) {
            this.applicant_license_id = applicant_license_id;
        }

        public Object getApplicant_license_proof() {
            return applicant_license_proof;
        }

        public void setApplicant_license_proof(Object applicant_license_proof) {
            this.applicant_license_proof = applicant_license_proof;
        }

        public Object getApplicant_license_number() {
            return applicant_license_number;
        }

        public void setApplicant_license_number(Object applicant_license_number) {
            this.applicant_license_number = applicant_license_number;
        }

        public Object getApplicant_license_experience_month() {
            return applicant_license_experience_month;
        }

        public void setApplicant_license_experience_month(Object applicant_license_experience_month) {
            this.applicant_license_experience_month = applicant_license_experience_month;
        }

        public Object getApplicant_license_experience_month_name() {
            return applicant_license_experience_month_name;
        }

        public void setApplicant_license_experience_month_name(Object applicant_license_experience_month_name) {
            this.applicant_license_experience_month_name = applicant_license_experience_month_name;
        }

        public Object getApplicant_license_experience_year() {
            return applicant_license_experience_year;
        }

        public void setApplicant_license_experience_year(Object applicant_license_experience_year) {
            this.applicant_license_experience_year = applicant_license_experience_year;
        }

        public Object getApplicant_license_experience_year_name() {
            return applicant_license_experience_year_name;
        }

        public void setApplicant_license_experience_year_name(Object applicant_license_experience_year_name) {
            this.applicant_license_experience_year_name = applicant_license_experience_year_name;
        }
    }
}
