package com.hiremehealthcare.POJO;

import java.util.List;

public class Applicant_availability {
    /**
     * status : 1
     * data : [{"id":"190","firstname":"Rahul","lastname":"Bhimani","email":"pgweb90@yahoo.com","password":"7c6a180b36896a0a8c02787eeafb0e4c","phone":"9878987898","specialities":"50,49,45,44,28","license":"53,41,39,35","zipcode":"27157","state_id":"6","city_id":"5533","image":"","token":"","shift":"3","hours":"3","mon":"2","tue":"2","wed":"2","thu":"2","fri":"2","sat":"","sun":"1","status":"1","applicant_status":"1","created_time":"2019-02-07 00:17:53","modified_time":"2019-04-01 17:14:23","created_ip":"96.10.64.156","modified_ip":"192.168.30.30","shift_name":"Evenings","hour_name":"PRN"}]
     */

    private int status;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * id : 190
         * firstname : Rahul
         * lastname : Bhimani
         * email : pgweb90@yahoo.com
         * password : 7c6a180b36896a0a8c02787eeafb0e4c
         * phone : 9878987898
         * specialities : 50,49,45,44,28
         * license : 53,41,39,35
         * zipcode : 27157
         * state_id : 6
         * city_id : 5533
         * image :
         * token :
         * shift : 3
         * hours : 3
         * mon : 2
         * tue : 2
         * wed : 2
         * thu : 2
         * fri : 2
         * sat :
         * sun : 1
         * status : 1
         * applicant_status : 1
         * created_time : 2019-02-07 00:17:53
         * modified_time : 2019-04-01 17:14:23
         * created_ip : 96.10.64.156
         * modified_ip : 192.168.30.30
         * shift_name : Evenings
         * hour_name : PRN
         */

        private String id;
        private String firstname;
        private String lastname;
        private String email;
        private String password;
        private String phone;
        private String specialities;
        private String license;
        private String zipcode;
        private String state_id;
        private String city_id;
        private String image;
        private String token;
        private String shift;
        private String hours;
        private String mon;
        private String tue;
        private String wed;
        private String thu;
        private String fri;
        private String sat;
        private String sun;
        private String status;
        private String applicant_status;
        private String created_time;
        private String modified_time;
        private String created_ip;
        private String modified_ip;
        private String shift_name;
        private String hour_name;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getFirstname() {
            return firstname;
        }

        public void setFirstname(String firstname) {
            this.firstname = firstname;
        }

        public String getLastname() {
            return lastname;
        }

        public void setLastname(String lastname) {
            this.lastname = lastname;
        }

        public String getEmail() {
            return email;
        }

        public void setEmail(String email) {
            this.email = email;
        }

        public String getPassword() {
            return password;
        }

        public void setPassword(String password) {
            this.password = password;
        }

        public String getPhone() {
            return phone;
        }

        public void setPhone(String phone) {
            this.phone = phone;
        }

        public String getSpecialities() {
            return specialities;
        }

        public void setSpecialities(String specialities) {
            this.specialities = specialities;
        }

        public String getLicense() {
            return license;
        }

        public void setLicense(String license) {
            this.license = license;
        }

        public String getZipcode() {
            return zipcode;
        }

        public void setZipcode(String zipcode) {
            this.zipcode = zipcode;
        }

        public String getState_id() {
            return state_id;
        }

        public void setState_id(String state_id) {
            this.state_id = state_id;
        }

        public String getCity_id() {
            return city_id;
        }

        public void setCity_id(String city_id) {
            this.city_id = city_id;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getToken() {
            return token;
        }

        public void setToken(String token) {
            this.token = token;
        }

        public String getShift() {
            return shift;
        }

        public void setShift(String shift) {
            this.shift = shift;
        }

        public String getHours() {
            return hours;
        }

        public void setHours(String hours) {
            this.hours = hours;
        }

        public String getMon() {
            return mon+"";
        }

        public void setMon(String mon) {
            this.mon = mon;
        }

        public String getTue() {
            return tue+"";
        }

        public void setTue(String tue) {
            this.tue = tue;
        }

        public String getWed() {
            return wed+"";
        }

        public void setWed(String wed) {
            this.wed = wed;
        }

        public String getThu() {
            return thu+"";
        }

        public void setThu(String thu) {
            this.thu = thu;
        }

        public String getFri() {
            return fri+"";
        }

        public void setFri(String fri) {
            this.fri = fri;
        }

        public String getSat() {
            return sat+"";
        }

        public void setSat(String sat) {
            this.sat = sat;
        }

        public String getSun() {
            return sun+"";
        }

        public void setSun(String sun) {
            this.sun = sun;
        }

        public String getStatus() {
            return status;
        }

        public void setStatus(String status) {
            this.status = status;
        }

        public String getApplicant_status() {
            return applicant_status;
        }

        public void setApplicant_status(String applicant_status) {
            this.applicant_status = applicant_status;
        }

        public String getCreated_time() {
            return created_time;
        }

        public void setCreated_time(String created_time) {
            this.created_time = created_time;
        }

        public String getModified_time() {
            return modified_time;
        }

        public void setModified_time(String modified_time) {
            this.modified_time = modified_time;
        }

        public String getCreated_ip() {
            return created_ip;
        }

        public void setCreated_ip(String created_ip) {
            this.created_ip = created_ip;
        }

        public String getModified_ip() {
            return modified_ip;
        }

        public void setModified_ip(String modified_ip) {
            this.modified_ip = modified_ip;
        }

        public String getShift_name() {
            return shift_name+"";
        }

        public void setShift_name(String shift_name) {
            this.shift_name = shift_name;
        }

        public String getHour_name() {
            return hour_name+"";
        }

        public void setHour_name(String hour_name) {
            this.hour_name = hour_name;
        }
    }
}
