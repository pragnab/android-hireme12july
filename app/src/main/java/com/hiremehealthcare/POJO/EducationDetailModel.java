package com.hiremehealthcare.POJO;

import java.util.List;

/**
 * Created by nareshp on 5/4/2018.
 */

public class EducationDetailModel {
    int status;
    List<Data> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public List<Data> getDataList() {
        return data;
    }

    public void setDataList(List<Data> dataList) {
        this.data = dataList;
    }

    public class Data {
        private String completion_year;

        private String applicant_education_institute;

        private String completion_month;

        private String applicant_education_degree;

        private String applicant_education_id;

        public String getCompletion_year() {
            return completion_year;
        }

        public void setCompletion_year(String completion_year) {
            this.completion_year = completion_year;
        }

        public String getApplicant_education_institute() {
            return applicant_education_institute;
        }

        public void setApplicant_education_institute(String applicant_education_institute) {
            this.applicant_education_institute = applicant_education_institute;
        }

        public String getCompletion_month() {
            return completion_month;
        }

        public void setCompletion_month(String completion_month) {
            this.completion_month = completion_month;
        }

        public String getApplicant_education_degree() {
            return applicant_education_degree;
        }

        public void setApplicant_education_degree(String applicant_education_degree) {
            this.applicant_education_degree = applicant_education_degree;
        }

        public String getApplicant_education_id() {
            return applicant_education_id;
        }

        public void setApplicant_education_id(String applicant_education_id) {
            this.applicant_education_id = applicant_education_id;
        }

        @Override
        public String toString() {
            return "ClassPojo [completion_year = " + completion_year + ", applicant_education_institute = " + applicant_education_institute + ", completion_month = " + completion_month + ", applicant_education_degree = " + applicant_education_degree + ", applicant_education_id = " + applicant_education_id + "]";
        }
    }
}


