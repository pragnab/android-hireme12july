package com.hiremehealthcare.POJO;

/**
 * Created by ADMIN on 11-07-2018.
 */

public class SharePos {
    /**
     * message : {"facebook_url":"http://www.facebook.com/sharer.php?s=100&p[title]=\"Aksha+Makarani+shared+HireMe+Healthcare+post.\"&p[summary]=\"CMC+Main+is+in+need+of+a+BLS.++Review+Benefits+and+Shift+Details+via+http%3A%2F%2Fhiremehealthcare.iipldemo.com%2F+or+download+the+app+HireMe+Healthcare.\"&p[url]=\"http%3A%2F%2Fhiremehealthcare.iipldemo.com%2Fapp%2Fapi.php%3Ftype%3Dshare_position%26position_id%3D50%26applicant_id%3D40\"&p[images][0]=\"http://hiremehealthcare.iipldemo.com/images/hmh_logo.png\"","twitter_url":"#","google_plus_url":"https://plus.google.com/share?url=\"http%3A%2F%2Fhiremehealthcare.iipldemo.com%2Fapp%2Fapi.php%3Ftype%3Dshare_position%26position_id%3D50%26applicant_id%3D40\"","linkedin_url":"https://www.linkedin.com/shareArticle?mini=true&url=\"http%3A%2F%2Fhiremehealthcare.iipldemo.com%2Fapp%2Fapi.php%3Ftype%3Dshare_position%26position_id%3D50%26applicant_id%3D40\"&title=\"Aksha+Makarani+shared+HireMe+Healthcare+post.\"&summary=\"CMC+Main+is+in+need+of+a+BLS.++Review+Benefits+and+Shift+Details+via+http%3A%2F%2Fhiremehealthcare.iipldemo.com%2F+or+download+the+app+HireMe+Healthcare.\""}
     * status : 1
     */


    private MessageBean message;
    private int status;

    public MessageBean getMessage() {
        return message;
    }

    public void setMessage(MessageBean message) {
        this.message = message;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public static class MessageBean {
        /**
         * facebook_url : http://www.facebook.com/sharer.php?s=100&p[title]="Aksha+Makarani+shared+HireMe+Healthcare+post."&p[summary]="CMC+Main+is+in+need+of+a+BLS.++Review+Benefits+and+Shift+Details+via+http%3A%2F%2Fhiremehealthcare.iipldemo.com%2F+or+download+the+app+HireMe+Healthcare."&p[url]="http%3A%2F%2Fhiremehealthcare.iipldemo.com%2Fapp%2Fapi.php%3Ftype%3Dshare_position%26position_id%3D50%26applicant_id%3D40"&p[images][0]="http://hiremehealthcare.iipldemo.com/images/hmh_logo.png"
         * twitter_url : #
         * google_plus_url : https://plus.google.com/share?url="http%3A%2F%2Fhiremehealthcare.iipldemo.com%2Fapp%2Fapi.php%3Ftype%3Dshare_position%26position_id%3D50%26applicant_id%3D40"
         * linkedin_url : https://www.linkedin.com/shareArticle?mini=true&url="http%3A%2F%2Fhiremehealthcare.iipldemo.com%2Fapp%2Fapi.php%3Ftype%3Dshare_position%26position_id%3D50%26applicant_id%3D40"&title="Aksha+Makarani+shared+HireMe+Healthcare+post."&summary="CMC+Main+is+in+need+of+a+BLS.++Review+Benefits+and+Shift+Details+via+http%3A%2F%2Fhiremehealthcare.iipldemo.com%2F+or+download+the+app+HireMe+Healthcare."
         */

        private String facebook_url;
        private String twitter_url;
        private String google_plus_url;
        private String linkedin_url;

        public String getFacebook_url() {
            return facebook_url;
        }

        public void setFacebook_url(String facebook_url) {
            this.facebook_url = facebook_url;
        }

        public String getTwitter_url() {
            return twitter_url;
        }

        public void setTwitter_url(String twitter_url) {
            this.twitter_url = twitter_url;
        }

        public String getGoogle_plus_url() {
            return google_plus_url;
        }

        public void setGoogle_plus_url(String google_plus_url) {
            this.google_plus_url = google_plus_url;
        }

        public String getLinkedin_url() {
            return linkedin_url;
        }

        public void setLinkedin_url(String linkedin_url) {
            this.linkedin_url = linkedin_url;
        }
    }
}
