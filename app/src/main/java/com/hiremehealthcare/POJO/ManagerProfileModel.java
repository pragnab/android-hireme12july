package com.hiremehealthcare.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by nareshp on 5/21/2018.
 */

public class ManagerProfileModel {

    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private Data data;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public class Data {

        @SerializedName("manager_id")
        @Expose
        private String managerId;
        @SerializedName("manager_facility_id")
        @Expose
        private String managerFacilityId;
        @SerializedName("manager_firstname")
        @Expose
        private String managerFirstname;
        @SerializedName("manager_lastname")
        @Expose
        private String managerLastname;
        @SerializedName("manager_email")
        @Expose
        private String managerEmail;
        @SerializedName("manager_password")
        @Expose
        private String managerPassword;
        @SerializedName("manager_image")
        @Expose
        private String managerImage;
        @SerializedName("manager_phone")
        @Expose
        private String managerPhone;
        @SerializedName("manager_zipcode")
        @Expose
        private String managerZipcode;
        @SerializedName("manager_facility_number")
        @Expose
        private String managerFacilityNumber;

        public String getManagerId() {
            return managerId;
        }

        public void setManagerId(String managerId) {
            this.managerId = managerId;
        }

        public String getManagerFacilityId() {
            return managerFacilityId;
        }

        public void setManagerFacilityId(String managerFacilityId) {
            this.managerFacilityId = managerFacilityId;
        }

        public String getManagerFirstname() {
            return managerFirstname;
        }

        public void setManagerFirstname(String managerFirstname) {
            this.managerFirstname = managerFirstname;
        }

        public String getManagerLastname() {
            return managerLastname;
        }

        public void setManagerLastname(String managerLastname) {
            this.managerLastname = managerLastname;
        }

        public String getManagerEmail() {
            return managerEmail;
        }

        public void setManagerEmail(String managerEmail) {
            this.managerEmail = managerEmail;
        }

        public String getManagerPassword() {
            return managerPassword;
        }

        public void setManagerPassword(String managerPassword) {
            this.managerPassword = managerPassword;
        }

        public String getManagerImage() {
            return managerImage;
        }

        public void setManagerImage(String managerImage) {
            this.managerImage = managerImage;
        }

        public String getManagerPhone() {
            return managerPhone;
        }

        public void setManagerPhone(String managerPhone) {
            this.managerPhone = managerPhone;
        }

        public String getManagerZipcode() {
            return managerZipcode;
        }

        public void setManagerZipcode(String managerZipcode) {
            this.managerZipcode = managerZipcode;
        }

        public String getManagerFacilityNumber() {
            return managerFacilityNumber;
        }

        public void setManagerFacilityNumber(String managerFacilityNumber) {
            this.managerFacilityNumber = managerFacilityNumber;
        }

    }

}
