package com.hiremehealthcare.POJO;

import java.util.List;

public class ApplicantExperience {

    /**
     * data : [{"applicant_experience_id":"9","applicant_experience_location":"abc locatibbbbon","speciality":null,"experience_label":"01 Years 5 months"}]
     * status : 1
     */

    private int status;
    private List<DataBean> data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public List<DataBean> getData() {
        return data;
    }

    public void setData(List<DataBean> data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * applicant_experience_id : 9
         * applicant_experience_location : abc locatibbbbon
         * speciality : null
         * experience_label : 01 Years 5 months
         */

        private String applicant_experience_id;
        private String applicant_experience_location;
        private Object speciality;
        private String experience_label;

        public String getApplicant_experience_id() {
            return applicant_experience_id;
        }

        public void setApplicant_experience_id(String applicant_experience_id) {
            this.applicant_experience_id = applicant_experience_id;
        }

        public String getApplicant_experience_location() {
            return applicant_experience_location;
        }

        public void setApplicant_experience_location(String applicant_experience_location) {
            this.applicant_experience_location = applicant_experience_location;
        }

        public Object getSpeciality() {
            return speciality;
        }

        public void setSpeciality(Object speciality) {
            this.speciality = speciality;
        }

        public String getExperience_label() {
            return experience_label;
        }

        public void setExperience_label(String experience_label) {
            this.experience_label = experience_label;
        }
    }
}
