package com.hiremehealthcare.POJO;

public class ManagerProfile {

    /**
     * status : 1
     * data : {"manager_id":"10","manager_facility_id":"10","manager_firstname":"Hayley","manager_lastname":"Dentistry","manager_email":"hayleyswaringen@gmail.com","manager_password":"ee24d5acbe1b746136c271da942bf257","manager_image":"","manager_phone":"","manager_zipcode":"28204","manager_facility_number":"6561360880"}
     */

    private int status;
    private DataBean data;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public DataBean getData() {
        return data;
    }

    public void setData(DataBean data) {
        this.data = data;
    }

    public static class DataBean {
        /**
         * manager_id : 10
         * manager_facility_id : 10
         * manager_firstname : Hayley
         * manager_lastname : Dentistry
         * manager_email : hayleyswaringen@gmail.com
         * manager_password : ee24d5acbe1b746136c271da942bf257
         * manager_image :
         * manager_phone :
         * manager_zipcode : 28204
         * manager_facility_number : 6561360880
         */

        private String manager_id;
        private String manager_facility_id;
        private String manager_firstname;
        private String manager_lastname;
        private String manager_email;
        private String manager_password;
        private String manager_image;
        private String manager_phone;
        private String manager_zipcode;
        private String manager_facility_number;

        public String getManager_id() {
            return manager_id;
        }

        public void setManager_id(String manager_id) {
            this.manager_id = manager_id;
        }

        public String getManager_facility_id() {
            return manager_facility_id;
        }

        public void setManager_facility_id(String manager_facility_id) {
            this.manager_facility_id = manager_facility_id;
        }

        public String getManager_firstname() {
            return manager_firstname;
        }

        public void setManager_firstname(String manager_firstname) {
            this.manager_firstname = manager_firstname;
        }

        public String getManager_lastname() {
            return manager_lastname;
        }

        public void setManager_lastname(String manager_lastname) {
            this.manager_lastname = manager_lastname;
        }

        public String getManager_email() {
            return manager_email;
        }

        public void setManager_email(String manager_email) {
            this.manager_email = manager_email;
        }

        public String getManager_password() {
            return manager_password;
        }

        public void setManager_password(String manager_password) {
            this.manager_password = manager_password;
        }

        public String getManager_image() {
            return manager_image;
        }

        public void setManager_image(String manager_image) {
            this.manager_image = manager_image;
        }

        public String getManager_phone() {
            return manager_phone;
        }

        public void setManager_phone(String manager_phone) {
            this.manager_phone = manager_phone;
        }

        public String getManager_zipcode() {
            return manager_zipcode;
        }

        public void setManager_zipcode(String manager_zipcode) {
            this.manager_zipcode = manager_zipcode;
        }

        public String getManager_facility_number() {
            return manager_facility_number;
        }

        public void setManager_facility_number(String manager_facility_number) {
            this.manager_facility_number = manager_facility_number;
        }
    }
}
