package com.hiremehealthcare.POJO;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.List;

/**
 * Created by nareshp on 5/29/2018.
 */

public class CommunicationModel {
    @SerializedName("status")
    @Expose
    private Integer status;
    @SerializedName("data")
    @Expose
    private List<Datum> data = null;

    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    public List<Datum> getData() {
        return data;
    }

    public void setData(List<Datum> data) {
        this.data = data;
    }

    public static class Datum implements Serializable {

        @SerializedName("id")
        @Expose
        private String id;
        @SerializedName("position_id")
        @Expose
        private String positionId;
        @SerializedName("license")
        @Expose
        private String license;

        public String getLicense() {
            return license;
        }

        public void setLicense(String license) {
            this.license = license;
        }

        public String getApplicant_license_name() {
            return applicant_license_name;
        }

        public void setApplicant_license_name(String applicant_license_name) {
            this.applicant_license_name = applicant_license_name;
        }

        @SerializedName("applicant_license_name")
        @Expose

        private String applicant_license_name;
        @SerializedName("applicant_id")
        @Expose
        private String applicantId;
        @SerializedName("manager_id")
        @Expose
        private String manager_id;

        public String getManager_id() {
            return manager_id;
        }

        public void setManager_id(String manager_id) {
            this.manager_id = manager_id;
        }

        @SerializedName("app_status")
        @Expose
        private String appStatus;
        @SerializedName("firstname")
        @Expose
        private String firstname;
        @SerializedName("lastname")
        @Expose
        private String lastname;

        public String getFacility_name() {
            return facility_name;
        }

        public void setFacility_name(String facility_name) {
            this.facility_name = facility_name;
        }

        public String getSpecialty_name() {
            return specialty_name;
        }

        public void setSpecialty_name(String specialty_name) {
            this.specialty_name = specialty_name;
        }

        @SerializedName("facility_name")
        @Expose
        private String facility_name;
        @SerializedName("specialty_name")
        @Expose
        private String specialty_name;
        @SerializedName("image")
        @Expose
        private String image;
        @SerializedName("applicant_cerification_cerificate_name")
        @Expose
        private String applicantCerificationCerificateName;
        @SerializedName("unread_message")
        @Expose
        private String unreadMessage;

        public String getId() {
            return id;
        }

        public void setId(String id) {
            this.id = id;
        }

        public String getPositionId() {
            return positionId;
        }

        public void setPositionId(String positionId) {
            this.positionId = positionId;
        }

        public String getApplicantId() {
            return applicantId;
        }

        public void setApplicantId(String applicantId) {
            this.applicantId = applicantId;
        }

        public String getAppStatus() {
            return appStatus;
        }

        public void setAppStatus(String appStatus) {
            this.appStatus = appStatus;
        }

        public String getFirstname() {
            return firstname;
        }

        public void setFirstname(String firstname) {
            this.firstname = firstname;
        }

        public String getLastname() {
            return lastname;
        }

        public void setLastname(String lastname) {
            this.lastname = lastname;
        }

        public String getImage() {
            return image;
        }

        public void setImage(String image) {
            this.image = image;
        }

        public String getApplicantCerificationCerificateName() {
            return applicantCerificationCerificateName;
        }

        public void setApplicantCerificationCerificateName(String applicantCerificationCerificateName) {
            this.applicantCerificationCerificateName = applicantCerificationCerificateName;
        }

        public String getUnreadMessage() {
            return unreadMessage;
        }

        public void setUnreadMessage(String unreadMessage) {
            this.unreadMessage = unreadMessage;
        }

    }
}
