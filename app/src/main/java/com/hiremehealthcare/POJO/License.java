package com.hiremehealthcare.POJO;

import java.util.List;

/**
 * Created by ADMIN on 27-04-2018.
 */

public class License {

    /**
     * status : 1
     * total : 7
     * license : [{"license_id":"1","license_name":"RN"},{"license_id":"2","license_name":"CNA"},{"license_id":"3","license_name":"NRP"},{"license_id":"4","license_name":"BSN"},{"license_id":"5","license_name":"CRNA"},{"license_id":"6","license_name":"CCNA"},{"license_id":"7","license_name":"PALS"}]
     */

    private int status;
    private int total;
    private List<LicenseBean> license;

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getTotal() {
        return total;
    }

    public void setTotal(int total) {
        this.total = total;
    }

    public List<LicenseBean> getLicense() {
        return license;
    }

    public void setLicense(List<LicenseBean> license) {
        this.license = license;
    }

    public static class LicenseBean {
        /**
         * license_id : 1
         * license_name : RN
         */

        private String license_id;
        private String license_name;

        public String getLicense_id() {
            return license_id;
        }

        public void setLicense_id(String license_id) {
            this.license_id = license_id;
        }

        public String getLicense_name() {
            return license_name;
        }

        public void setLicense_name(String license_name) {
            this.license_name = license_name;
        }
    }
}
