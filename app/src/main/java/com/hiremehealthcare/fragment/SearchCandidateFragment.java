package com.hiremehealthcare.fragment;


import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.easywaylocation.EasyWayLocation;
import com.example.easywaylocation.Listener;
import com.google.gson.Gson;
import com.hiremehealthcare.CommonCls.CustomBoldTextView;
import com.hiremehealthcare.CommonCls.CustomButton;
import com.hiremehealthcare.CommonCls.CustomEditText;
import com.hiremehealthcare.CommonCls.DialogUtils;
import com.hiremehealthcare.CommonCls.ExpandableHeightGridView;
import com.hiremehealthcare.CommonCls.URLS;
import com.hiremehealthcare.CommonCls.Validations;
import com.hiremehealthcare.POJO.Certificates;
import com.hiremehealthcare.POJO.Cities;
import com.hiremehealthcare.POJO.License;
import com.hiremehealthcare.POJO.SearchCandidate;
import com.hiremehealthcare.POJO.Speciality;
import com.hiremehealthcare.POJO.States;
import com.hiremehealthcare.R;
import com.hiremehealthcare.Utils.GPSTracker;
import com.hiremehealthcare.activity.SearchCandidateDetailActivity;
import com.hiremehealthcare.adapter.CertificationAdapter;
import com.hiremehealthcare.adapter.CertificationMultiSelectAdapter;
import com.hiremehealthcare.adapter.LicenseAdapter;
import com.hiremehealthcare.adapter.SpecialityAdapter;
import com.hiremehealthcare.adapter.SpecialityExpAdapter;
import com.hiremehealthcare.adapter.SpecialityMultiSelectAdapter;
import com.hiremehealthcare.adapter.SpinnerAdapter;
import com.hiremehealthcare.database.MySharedPrefereces;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * A simple {@link Fragment} subclass.
 */
public class SearchCandidateFragment extends Fragment implements View.OnClickListener {


    private ExpandableHeightGridView gridlicence;
    private ExpandableHeightGridView grid_certification;
    private ExpandableHeightGridView grid_speciality;
    LicenseAdapter licenseAdapter;
    RequestQueue queue;
    SpecialityMultiSelectAdapter specialityAdapter;
    SpecialityExpAdapter specialityExpAdapter;
    Spinner spWithinMile;
    List<String> stringList;
    private CustomEditText etRegisterPinCode;
    private CustomBoldTextView btnSubmitBack;
    private ExpandableHeightGridView gridcertification;
    private ExpandableHeightGridView gridspeciality, grid_speciality_exp;
    private CustomButton btnSearchCandidate;
    private CustomButton btnClearSerch;
    CertificationMultiSelectAdapter certificationAdapter;
    Context ctx;
    private CustomEditText edtPinCode;
    GPSTracker gpsTracker;
    CustomButton btnNearMe;
    String TAG = "SearchCandidateFragment";
    MySharedPrefereces mySharedPrefereces;
    public static SearchCandidate searchCandidate;
    EasyWayLocation easyWayLocation;
    States states;
    Cities cities;
    SearchableSpinner spState, spCity;

    public SearchCandidateFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_search_candidate, container, false);
        ctx = getActivity();
        mySharedPrefereces = new MySharedPrefereces(ctx);
        btnNearMe = (CustomButton) rootView.findViewById(R.id.btnNearMe);
        btnNearMe.setOnClickListener(this);
        spState = (SearchableSpinner) rootView.findViewById(R.id.spState);
        spCity = (SearchableSpinner) rootView.findViewById(R.id.spCity);
        this.btnClearSerch = (CustomButton) rootView.findViewById(R.id.btnClearSerch);
        this.btnSearchCandidate = (CustomButton) rootView.findViewById(R.id.btnSearchCandidate);
        this.gridspeciality = (ExpandableHeightGridView) rootView.findViewById(R.id.grid_speciality);
        this.grid_speciality_exp = (ExpandableHeightGridView) rootView.findViewById(R.id.grid_speciality_exp);
        this.gridcertification = (ExpandableHeightGridView) rootView.findViewById(R.id.grid_certification);
        this.gridlicence = (ExpandableHeightGridView) rootView.findViewById(R.id.grid_licence);
        this.spWithinMile = (Spinner) rootView.findViewById(R.id.spWithinMile);
        this.btnSubmitBack = (CustomBoldTextView) rootView.findViewById(R.id.btnSubmitBack);
        this.edtPinCode = (CustomEditText) rootView.findViewById(R.id.edtPinCode);
        edtPinCode.setText(mySharedPrefereces.getZipCode());
        initAllControls(rootView);
        return rootView;
    }

    /*get state*/
    String[] namesNH = new String[0];
    String selectedStateID = "0", selectedCityID = "0";

    ArrayList<String> STATES = new ArrayList<>();
    ArrayList<String> STATES_ID = new ArrayList<>();

//    int toselectedpos_State = 0;
//    int toselectedpos_City = 0;

    private void getState() {
        String Url = URLS.STATE_URL;

        Log.d(TAG, "STATE_URL: url:- " + Url);
        System.out.println("this is STATE_URL url " + Url + "");
        StringRequest req = new StringRequest(Request.Method.GET, Url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "STATE_URL: response:- " + response);
                Gson gson = new Gson();

                states = gson.fromJson(response, States.class);

                namesNH = new String[states.getState().size() + 1];
//                namesNH = new String[states.getState().size()];
                //    System.out.println("alreadyselected_state " + alreadyselected_state + "");
                namesNH[0] = "Select State";
                namesNHCity = new String[1];

//                namesNHCity = new String[cities.getMessage().size()];
                namesNHCity[0] = "Select City";
                ArrayAdapter arrayAdapter = new ArrayAdapter(getContext(), R.layout.spinner_adapter, namesNHCity);
                spCity.setAdapter(arrayAdapter);
                //    spState.setSelection(-1);
                STATES = new ArrayList<>();
                STATES_ID = new ArrayList<>();
                for (int i = 0; i < states.getState().size(); i++) {
                    namesNH[i + 1] = states.getState().get(i).getState_name() + "";
                    STATES.add(states.getState().get(i).getState_name() + "");
                    STATES_ID.add(states.getState().get(i).getState_id() + "");

//                    namesNH[i] = states.getState().get(i).getState_name() + "";
                    //  System.out.println("alreadyselected_state states.getState().get(i).getState_id() " + states.getState().get(i).getState_id() + "");
//                    if (alreadyselected_state.compareToIgnoreCase(states.getState().get(i).getState_id()) == 0) {
//                        toselectedpos_State = i;
//                    }
                }
//                ArrayAdapter arrayAdapter1 = new ArrayAdapter(getContext(),android.R.layout.simple_spinner_dropdown_item,namesNH);
                ArrayAdapter arrayAdapter1 = new ArrayAdapter(getContext(), R.layout.spinner_adapter, namesNH);
                spState.setAdapter(arrayAdapter1);

                spState.setSelection(0);
                //  getCity(states.getState().get(2).getState_id() + "");
                spState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                      /*  System.out.println("this is selectesd state " + namesNH[i] + "");
                        System.out.println("this is selectesd state ID " + states.getState().get(i).getState_id() + "");
                        selectedStateID = states.getState().get(i).getState_id() + "";
                        System.out.println("this is selectesd state CODE " + states.getState().get(i).getState_code() + "");
                        getCity(states.getState().get(i).getState_id() + "");*/
                        if (i > 0) {
//                            System.out.println("this is selectesd state " + namesNH[i + 1] + "");
//                            System.out.println("this is selectesd state ID " + states.getState().get(i + 1).getState_id() + "");
//                            selectedStateID = states.getState().get(i + 1).getState_id() + "";
//                            System.out.println("this is selectesd state CODE " + states.getState().get(i + 1).getState_code() + "");
//                            getCity(states.getState().get(i + 1).getState_id() + "");

                            System.out.println("this is selectesd state " + namesNH[i] + "");
//                            System.out.println("this is selectesd state ID " + states.getState().get(i).getState_id() + "");
//                            selectedStateID = states.getState().get(i).getState_id() + "";
//                            System.out.println("this is selectesd state CODE " + states.getState().get(i).getState_code() + "");
//                            getCity(states.getState().get(i).getState_id() + "");
                            String s = namesNH[i] + "";
                            int pos = STATES.indexOf(s);
                            System.out.println("this is pos->>>>>>>>>>>>> " + pos + "");


                            selectedStateID = STATES_ID.get(pos);

                            System.out.println("this is selectedStateID->>>>>>>>>>>>> " + selectedStateID + "");
                            getCity(selectedStateID + "");









                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
                /**/
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "getYear: error:- " + error.getMessage());
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    int statusCode = error.networkResponse.statusCode;
                    System.out.println("STATUS CODE " + statusCode + "");
                    DialogUtils.showDialog4Activity(getActivity(), ctx.getResources().getString(R.string.app_name), statusCode + "", null);
                }

            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(req);
    }

    String[] namesNHCity = new String[0];
    ArrayList<String> CITIES_ = new ArrayList<>();
    ArrayList<String> CITIES_ID = new ArrayList<>();
    private void getCity(String stateID) {
        //  toselectedpos_City = 0;
        String Url = URLS.CITY_URL + stateID + "";
        Log.d(TAG, "CITY_URL: url:- " + Url);
        System.out.println("this is CITY_URL url " + Url + "");
        StringRequest req = new StringRequest(Request.Method.GET, Url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "CITY_URL: response:- " + response);
                Gson gson = new Gson();

                cities = gson.fromJson(response, Cities.class);

                namesNHCity = new String[cities.getMessage().size() + 1];
                CITIES_ID = new ArrayList<>();
                CITIES_ = new ArrayList<>();
//                namesNHCity = new String[cities.getMessage().size()];
                namesNHCity[0] = "Select City";

                for (int i = 0; i < cities.getMessage().size(); i++) {
//                    namesNHCity[i+1] = states.getState().get(i).getState_name() + "";
//                    namesNHCity[i] = cities.getMessage().get(i).getCity_name() + "";
                    namesNHCity[i + 1] = cities.getMessage().get(i).getCity_name() + "";
                    CITIES_.add(cities.getMessage().get(i).getCity_name() + "");
                    CITIES_ID.add(cities.getMessage().get(i).getCity_id() + "");

                    /* System.out.println("alreadyselected_state cities.getMessage().get(i).getCity_id() " + cities.getMessage().get(i).getCity_id() + "");*/
//                    if (alreadyselected_city.compareToIgnoreCase(cities.getMessage().get(i).getCity_id()) == 0) {
//                        toselectedpos_City = i;
//                        System.out.println("alreadyselected_state cities.getMessage().get(i).getCity_id() " + cities.getMessage().get(i).getCity_id() + "");
//                    }
                }
//                ArrayAdapter arrayAdapter1 = new ArrayAdapter(getContext(),android.R.layout.simple_spinner_dropdown_item,namesNH);
                ArrayAdapter arrayAdapter1 = new ArrayAdapter(getContext(), R.layout.spinner_adapter, namesNHCity);
                spCity.setAdapter(arrayAdapter1);
                spCity.setSelection(-1);
                spCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
//                        System.out.println("this is selectesd City " + namesNHCity[i] + "");
//                        System.out.println("this is selectesd City " + namesNHCity[i + 1] + "");
////                        System.out.println("this is selectesd City ID " + cities.getMessage().get(i).getCity_id() + "");
//                        System.out.println("this is selectesd City ID " + cities.getMessage().get(i + 1).getCity_id() + "");
////                        selectedCityID = cities.getMessage().get(i).getCity_id() + "";
//                        selectedCityID = cities.getMessage().get(i + 1).getCity_id() + "";
////                        System.out.println("this is selectesd CITY state CODE:::::::: " + cities.getMessage().get(i).getCity_state_id() + "");
//                        System.out.println("this is selectesd CITY state CODE:::::::: " + cities.getMessage().get(i + 1).getCity_state_id() + "");








//                        System.out.println("this is selectesd City " + namesNHCity[i + 1] + "");
////                        System.out.println("this is selectesd City ID " + cities.getMessage().get(i).getCity_id() + "");
//                        System.out.println("this is selectesd City ID " + cities.getMessage().get(i + 1).getCity_id() + "");
////                        selectedCityID = cities.getMessage().get(i).getCity_id() + "";
//                        selectedCityID = cities.getMessage().get(i + 1).getCity_id() + "";
////                        System.out.println("this is selectesd CITY state CODE:::::::: " + cities.getMessage().get(i).getCity_state_id() + "");
//                        System.out.println("this is selectesd CITY state CODE:::::::: " + cities.getMessage().get(i + 1).getCity_state_id() + "");
                        //   getCity(states.getState().get(i).getState_id() + "");

                        if (i > 0) {

                            System.out.println("this is selectesd City1111111111111 " + namesNHCity[i] + "");

                            String s = namesNHCity[i] + "";
                            int pos = CITIES_.indexOf(s);
                            System.out.println("this is pos->>>>>>>>>>>>> CITIES  " + pos + "");


                            selectedCityID = CITIES_ID.get(pos);

                            System.out.println("this is selectedStateID  selectedCityID->>>>>>>>>>>>> " + selectedCityID + "");

                        }



                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "getYear: error:- " + error.getMessage());
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    int statusCode = error.networkResponse.statusCode;
                    System.out.println("STATUS CODE " + statusCode + "");
                    DialogUtils.showDialog4Activity(getActivity(), ctx.getResources().getString(R.string.app_name), statusCode + "", null);
                }

            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(req);
    }

    public void initAllControls(View rootView) {
        this.gridlicence = (ExpandableHeightGridView) rootView.findViewById(R.id.grid_licence);
        this.grid_certification = (ExpandableHeightGridView) rootView.findViewById(R.id.grid_certification);
        this.grid_speciality = (ExpandableHeightGridView) rootView.findViewById(R.id.grid_speciality);
        spWithinMile = (Spinner) rootView.findViewById(R.id.spWithinMile);

        this.btnClearSerch = (CustomButton) rootView.findViewById(R.id.btnClearSerch);
        this.btnSearchCandidate = (CustomButton) rootView.findViewById(R.id.btnSearchCandidate);
        this.gridspeciality = (ExpandableHeightGridView) rootView.findViewById(R.id.grid_speciality);
        this.gridcertification = (ExpandableHeightGridView) rootView.findViewById(R.id.grid_certification);
        this.gridlicence = (ExpandableHeightGridView) rootView.findViewById(R.id.grid_licence);
        this.spWithinMile = (Spinner) rootView.findViewById(R.id.spWithinMile);
        this.btnSubmitBack = (CustomBoldTextView) rootView.findViewById(R.id.btnSubmitBack);

        this.gridlicence.setExpanded(true);
        this.grid_certification.setExpanded(true);
        grid_speciality.setExpanded(true);
        grid_speciality_exp.setExpanded(true);
        queue = Volley.newRequestQueue(ctx);
        btnClearSerch.setOnClickListener(this);
        btnSearchCandidate.setOnClickListener(this);
        stringList = new ArrayList<>();
        stringList.add("10 miles");
        stringList.add("20 miles");
        stringList.add("30 miles");
        stringList.add("40 miles");
        stringList.add("50 miles");
        stringList.add("60 miles");
        stringList.add("70 miles");
        stringList.add("80 miles");
        stringList.add("90 miles");
        stringList.add("100 miles");
        SpinnerAdapter spinnerAdapter = new SpinnerAdapter(ctx, stringList);
        spWithinMile.setAdapter(spinnerAdapter);
        getState();
        getLicense();
        getCertification();
        getspeciality();

        easyWayLocation = new EasyWayLocation(ctx);
        easyWayLocation.setListener(new Listener() {
            @Override
            public void locationOn() {
                Log.d(TAG, "locationOn: ");
            }

            @Override
            public void onPositionChanged() {
                Log.d(TAG, "onPositionChanged: ");
            }

            @Override
            public void locationCancelled() {
                Log.d(TAG, "locationCancelled: ");
            }
        });


    }

    /**
     * this method is used to getting certificatication  detail
     */

    private void getCertification() {
        String Url = URLS.CERTIFICATION_URL;
        Log.d(TAG, "getCertification: URL:- " + Url);
        StringRequest req = new StringRequest(Request.Method.GET, Url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "getCertification: Response:- " + response);
                Gson gson = new Gson();
                Certificates certificates = gson.fromJson(response, Certificates.class);

                if (certificates != null) {
                    certificationAdapter = new CertificationMultiSelectAdapter(certificates, ctx);
                    grid_certification.setAdapter(certificationAdapter);
                } else {
                    // DialogUtils.showDialog4Activity(Login.this, ctx.getResources().getString(R.string.app_name), logindata.getMessage() + "");

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "getCertification: Error:- " + error.getMessage());
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    int statusCode = error.networkResponse.statusCode;
                    System.out.println("STATUS CODE " + statusCode + "");
                    DialogUtils.showDialog4Activity(getActivity(), ctx.getResources().getString(R.string.app_name), statusCode + "", null);
                }

            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(req);
    }

    /**
     * this method is used to getting speciality from server
     */

    private void getspeciality() {
        String Url = URLS.SPECIALTY_URL;
        Log.d(TAG, "getspeciality: URL:- " + Url);
        StringRequest req = new StringRequest(Request.Method.GET, Url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "getspeciality: Response:- " + response);
                System.out.println("getspeciality RESPONSE " + response + "");
                Gson gson = new Gson();
                Speciality speciality = gson.fromJson(response, Speciality.class);
//                Type collectionType = new TypeToken<Collection<Speciality>>() {
//                }.getType();
//                Collection<Speciality> speciality = gson.fromJson(response, collectionType);
                if (speciality != null) {
                    if (speciality.getTotal() > 0) {
                        System.out.println("TEST " + speciality.getSpecialty().get(0).getSpecialty_id() + "");
                        System.out.println("TEST " + speciality.getSpecialty().get(0).getSpecialty_name() + "");
                        System.out.println("TEST " + speciality.getSpecialty().size() + "");
//                    for (int i = 0; i < speciality.getSpecialty().size(); i++) {
//                        addRadioButtons(i, rootView, speciality.getSpecialty().get(i).getSpecialty_name());
//                    }
                        specialityAdapter = new SpecialityMultiSelectAdapter(speciality, ctx);
                        specialityExpAdapter = new SpecialityExpAdapter(speciality, ctx);
                        grid_speciality.setAdapter(specialityAdapter);
                        grid_speciality_exp.setAdapter(specialityExpAdapter);

//                  for(int i=0;i<speciality.size();i++)
//                  {
//
//                  }
                    }
                } else {
                    // DialogUtils.showDialog4Activity(LoginActivity.this, ctx.getResources().getString(R.string.app_name), logindata.getMessage() + "");

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "getspeciality: Errror:- " + error.getMessage());
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    int statusCode = error.networkResponse.statusCode;
                    System.out.println("STATUS CODE " + statusCode + "");
                    DialogUtils.showDialog4Activity(ctx, getResources().getString(R.string.app_name), statusCode + "", null);
                }

            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(req);

    }

    private void getLicense() {
        String Url = URLS.LICENSE_URL;
        Log.d(TAG, "getLicense: URL" + Url);
        StringRequest req = new StringRequest(Request.Method.GET, Url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "getLicense: Response" + response);
                Gson gson = new Gson();
                License license = gson.fromJson(response, License.class);
//                Type collectionType = new TypeToken<Collection<License>>() {
//                }.getType();
//                Collection<License> license = gson.fromJson(response, collectionType);
                if (license != null) {
                    licenseAdapter = new LicenseAdapter(license, ctx);
                    gridlicence.setAdapter(licenseAdapter);
                    //  grid_certification.setAdapter(licenseAdapter);
                } else {
                    // DialogUtils.showDialog4Activity(LoginActivity.this, ctx.getResources().getString(R.string.app_name), logindata.getMessage() + "");

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "getLicense: Error" + error.getMessage());
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    int statusCode = error.networkResponse.statusCode;
                    System.out.println("STATUS CODE " + statusCode + "");
                    DialogUtils.showDialog4Activity(ctx, getResources().getString(R.string.app_name), statusCode + "", null);
                }

            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(req);
    }


    @Override
    public void onClick(View view) {
        int ID = view.getId();
        switch (ID) {
            case R.id.btnClearSerch:
                clearAll();

                break;
            case R.id.btnSearchCandidate:
                String license = licenseAdapter.selected.toString();
                String certificate = certificationAdapter.selected.toString();
                String speciality = specialityAdapter.selected.toString();
                String specialityExp = specialityExpAdapter.selected.toString();
                license = license.replace("[", "").replace("]", "").replace(" ", "");
                certificate = certificate.replace("[", "").replace("]", "").replace(" ", "");
                speciality = speciality.replace("[", "").replace("]", "").replace(" ", "");
                specialityExp = specialityExp.replace("[", "").replace("]", "").replace(" ", "");

                System.out.println("license " + license + "");
                System.out.println("certificate " + certificate + "");
                System.out.println("speciality " + speciality + "");
                System.out.println("specialityExp " + specialityExp + "");
                //if (edtPinCode.getText().toString().contentEquals("") || edtPinCode.getText().toString().trim().length() < 0) {
                //  DialogUtils.showDialog(ctx, ctx.getResources().getString(R.string.app_name), "Please enter Zip code");


                //}
                // else {
                System.out.println("selectedStateID :::::::::::::: " + selectedStateID + "");
                System.out.println("selectedCityID :::::::::::::: " + selectedCityID + "");
//                if (selectedStateID.contentEquals("0") || selectedCityID.contentEquals("0")) {
//                    System.out.println("selectedStateID :::::::::::::: " + selectedStateID + "");
//             //      /pppppppppppppppppppp DialogUtils.showDialog(ctx, ctx.getResources().getString(R.string.app_name), "Please Select State and City");
//                } else {
//                    System.out.println("this is search:::::::::::::::::::::::::::");
//                    searchCandidate();
//                }
                searchCandidate();

                break;
            case R.id.btnNearMe:
                onNearMeButtonClicked();
                break;
        }
    }

    /**
     * this method called when user clicked near me button
     */

    public void onNearMeButtonClicked() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (Validations.hasPermissions(ctx, Validations.RUN_TIME_PERMITIONS_LOCATION)) {
                processAfterPermmionGranted();
            } else {
                ActivityCompat.requestPermissions(getActivity(), Validations.RUN_TIME_PERMITIONS_LOCATION, Validations.REQUEST_CODE_PERMISSION);
            }
        } else {
            processAfterPermmionGranted();
        }


    }

    /**
     * getting pincode from latlong and set to edit text
     */

    public void processAfterPermmionGranted() {
        gpsTracker = new GPSTracker(ctx);
        if (gpsTracker.canGetLocation()) {
//            System.out.println("search fregment "+gpsTracker.canGetLocation()+"latitude "+lat);
            Log.d("SearchFragment", "latitude:" + gpsTracker.getLatitude() + " longitude:" + gpsTracker.getLongitude());
//            if (gpsTracker.getLatitude() > 0.0 && gpsTracker.getLongitude() > 0.0) {
            if (gpsTracker.getLatitude() > 0.0 || gpsTracker.getLongitude() > 0.0) {
                try {
                    edtPinCode.setText(getPostalCodeByCoordinates(ctx, gpsTracker.getLatitude(), gpsTracker.getLongitude()));
                } catch (Exception ex) {
                    Log.d("SearchFragment", "" + ex.getMessage());
                }
            } else {
                if (ctx != null) {
                    Toast.makeText(ctx, "GPS Prepairing Please Retry", Toast.LENGTH_LONG).show();
                }
            }

        } else {

            DialogUtils.showDialog4Activity(ctx, "GPS is settings", "GPS is not enabled. Do you want to go to settings menu?", new DialogUtils.DailogCallBackOkButtonClick() {
                @Override
                public void onDialogOkButtonClicked() {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
                }
            });

            //gpsTracker.showSettingsAlert();

        }
    }

    /**
     * this method getting pincode from latlong
     *
     * @param context context
     * @param lat     latitude
     * @param lon     longitude
     * @return String pincode if not found pincode than return blank
     * @throws IOException
     */

    public static String getPostalCodeByCoordinates(Context context, double lat, double lon) throws IOException {

        Geocoder mGeocoder = new Geocoder(context, Locale.getDefault());
        String zipcode = null;
        Address address = null;

        if (mGeocoder != null) {

            List<Address> addresses = mGeocoder.getFromLocation(lat, lon, 5);

            if (addresses != null && addresses.size() > 0) {

                for (int i = 0; i < addresses.size(); i++) {
                    address = addresses.get(i);
                    if (address.getPostalCode() != null) {
                        zipcode = address.getPostalCode();
                        Log.d("SearchFragment", "Postal code: " + address.getPostalCode());
                        break;
                    }

                }
                return zipcode;
            }
        }

        return "";
    }


    /**
     * this method is used to clear all selected data
     */
    private void clearAll() {
        edtPinCode.setText("");
        spWithinMile.setSelection(0);
        if (certificationAdapter != null) {
            certificationAdapter.clearSelected();
            certificationAdapter.notifyDataSetChanged();
        }
        if (licenseAdapter != null) {
            licenseAdapter.clearSelected();
            licenseAdapter.notifyDataSetChanged();
        }
        if (specialityAdapter != null) {
            specialityAdapter.clearSelected();
            specialityAdapter.notifyDataSetChanged();
        }
        spCity.setSelection(0);
        spState.setSelection(0);
        selectedCityID="";
        selectedStateID="";try {
            spCity.setSelection(0);
            spState.setSelection(0);
        }
        catch (Exception e)
        {

        }


    }

    /***
     * after search candidate button clicked send data to server
     */
    private void searchCandidate() {
        DialogUtils.showProgressDialog(ctx, "Please wait....");
        String miles = spWithinMile.getSelectedItem() + "";
        miles = miles.replace("miles", "");
        miles = miles.replace(" ", "");
        String license = licenseAdapter.selected.toString();
        String certificate = certificationAdapter.selected.toString();
        String speciality = specialityAdapter.selected.toString();
        String specialityExp = specialityExpAdapter.selected.toString();
        license = license.replace("[", "").replace("]", "").replace(" ", "");
        certificate = certificate.replace("[", "").replace("]", "").replace(" ", "");
        speciality = speciality.replace("[", "").replace("]", "").replace(" ", "");
        specialityExp = specialityExp.replace("[", "").replace("]", "").replace(" ", "");
        if (!license.contentEquals("")) {
            license = "&licence=" + license;
        }
        if (!certificate.contentEquals("")) {
            certificate = "&certificate=" + certificate;
        }
        if (!speciality.contentEquals("")) {
            speciality = "&speciality=" + speciality;
        }
        if (!specialityExp.contentEquals("")) {
            specialityExp = "&specialty_i=" + specialityExp;
        }
        String Url="";
        //  String Url = URLS.MANAGER_SEARCH_CANDIDATE + edtPinCode.getText().toString().trim() + "&miles=" + miles + license + certificate + speciality + specialityExp + "";
        if (selectedStateID.contentEquals("0") || selectedCityID.contentEquals("0")) {
             Url = URLS.MANAGER_SEARCH_CANDIDATE + edtPinCode.getText().toString().trim() + "&miles=" + miles + license + certificate + speciality + specialityExp + "" ;
        }
        else {
             Url = URLS.MANAGER_SEARCH_CANDIDATE + edtPinCode.getText().toString().trim() + "&miles=" + miles + license + certificate + speciality + specialityExp + "" + "&state_id=" + selectedStateID + "&city_id=" + selectedCityID + "";
        }
        Log.d(TAG, "searchCandidate: URL:- " + Url);
        StringRequest req = new StringRequest(Request.Method.GET, Url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "searchCandidate: Response:- " + response);
                Gson gson = new Gson();
                DialogUtils.hideProgressDialog();

                searchCandidate = gson.fromJson(response, SearchCandidate.class);

                if (searchCandidate != null) {
                    if (searchCandidate.getData() != null) {
                        if (searchCandidate.getData().size() > 0) {
                            //QualifiedCandidatesAdapter homeManagerAdapter = new QualifiedCandidatesAdapter(searchCandidate, ctx);
                            startActivity(new Intent(ctx, SearchCandidateDetailActivity.class));
                            /*if (homeManagerAdapter != null) {
                                homeManagerAdapter.notifyDataSetChanged();
                                HomeManagerFragment.rvHomeManagerList.setAdapter(homeManagerAdapter);
                            }

                            DashBoardManagerActivity.viewpager.setCurrentItem(0);
*/
                        } else {
                            DialogUtils.showDialog(ctx, ctx.getResources().getString(R.string.app_name), "No Candidate Found!");
                            clearAll();
                            //  DashBoardManagerActivity.viewpager.setCurrentItem(0);
                        }
                    }

                } else {    // DialogUtils.showDialog4Activity(Login.this, ctx.getResources().getString(R.string.app_name), logindata.getMessage() + "");

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "searchCandidate: Error:- " + error.getMessage());
                NetworkResponse networkResponse = error.networkResponse;
                DialogUtils.noInterNetDialog(ctx, "No Internet Connection", "No active internet connection available", new DialogUtils.NoInterNetDailogClickListner() {
                    @Override
                    public void onDialogOkButtonClicked() {
                        searchCandidate();
                    }

                    @Override
                    public void onDialogCancelButtonClicked() {

                    }
                });

            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(req);
    }
}
