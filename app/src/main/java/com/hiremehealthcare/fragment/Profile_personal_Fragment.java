/*
 * Copyright (c) 2017. Truiton (http://www.truiton.com/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 * Mohit Gupt (https://github.com/mohitgupt)
 *
 */
//https://stackoverflow.com/questions/8100831/stop-scrollview-from-setting-focus-on-edittext
package com.hiremehealthcare.fragment;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.hiremehealthcare.CommonCls.CustomBoldTextView;
import com.hiremehealthcare.CommonCls.CustomButton;
import com.hiremehealthcare.CommonCls.CustomEditText;
import com.hiremehealthcare.CommonCls.CustomTextView;
import com.hiremehealthcare.CommonCls.DialogUtils;
import com.hiremehealthcare.CommonCls.ExpandableHeightGridView;
import com.hiremehealthcare.CommonCls.ExpandableHeightListView;
import com.hiremehealthcare.CommonCls.URLS;
import com.hiremehealthcare.CommonCls.Validations;
import com.hiremehealthcare.POJO.ApplicantLicenseModel;
import com.hiremehealthcare.POJO.Cities;
import com.hiremehealthcare.POJO.ExpireYear;
import com.hiremehealthcare.POJO.License;
import com.hiremehealthcare.POJO.Month;
import com.hiremehealthcare.POJO.ProfilePersonalDetail;
import com.hiremehealthcare.POJO.Speciality;
import com.hiremehealthcare.POJO.States;
import com.hiremehealthcare.activity.DashBoardActivity;
import com.hiremehealthcare.adapter.Profile_licenseListAdpter;
import com.hiremehealthcare.R;
import com.hiremehealthcare.Utils.CommonRequestHelper;
import com.hiremehealthcare.Utils.FileUtils;
import com.hiremehealthcare.adapter.CustomYearExpireAdapter;
import com.hiremehealthcare.adapter.LicenseAdapter;
import com.hiremehealthcare.adapter.MonthCustomAdapter;
import com.hiremehealthcare.adapter.SpecialityAdapter;
import com.hiremehealthcare.adapter.SpecialityMultiSelectAdapter;
import com.hiremehealthcare.database.MySharedPrefereces;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * this class is used to show applicant profile personal detail
 */
public class Profile_personal_Fragment extends Fragment implements View.OnClickListener {
    private com.hiremehealthcare.CommonCls.CustomEditText edtfname;
    private com.hiremehealthcare.CommonCls.CustomEditText edtlname;
    private com.hiremehealthcare.CommonCls.CustomEditText edtemail;
    private com.hiremehealthcare.CommonCls.CustomEditText edtphn;
    private com.hiremehealthcare.CommonCls.CustomEditText edtphoto;
    private com.hiremehealthcare.CommonCls.CustomEditText edtoccupation;
    private com.hiremehealthcare.CommonCls.CustomBoldTextView tvlicense;
    private com.hiremehealthcare.CommonCls.ExpandableHeightGridView gridlicence;
    private com.hiremehealthcare.CommonCls.CustomButton btnback;
    private com.hiremehealthcare.CommonCls.CustomButton btnnxt;
    RequestQueue queue;
    private CustomBoldTextView tvlic;
    private com.hiremehealthcare.CommonCls.ExpandableHeightListView listlicence;
    private android.widget.ScrollView scrollview;
    private CustomEditText edtuploadlic;
    private CustomEditText edtlicno;
    private android.widget.Spinner tvwithin;
    private android.widget.Spinner tvwithinmonth;
    private CustomButton btnaddupload;
    private ExpandableHeightGridView gridspeciality;
    private CustomEditText edtzipcode;
    SpecialityMultiSelectAdapter specialityAdapter;
   public static Context ctx;
    LicenseAdapter licenseAdapter;
    MySharedPrefereces mySharedPrefereces;
    Profile_licenseListAdpter profile_licenseListAdpter;
    List<ApplicantLicenseModel.Datum> applicantLicenseData;
    private Bitmap bitmap;
    private String imgByteArray;
    Month month;
    ExpireYear year;
    CheckBox cbNeverExpire;
    ProfilePersonalDetail profilePersonalDetail;
    Speciality speciality;
    ApplicantLicenseModel applicantLicenseModel;
    License license;
    Dialog takeImgDialog;
    String TAG = "Profile_personal_Fragment";
    public static final int PROFILE_CAMERA_REQUEST = 5500;
    public static final int PROFILE_GALLERY_REQUEST = 6600;
    public static Profile_personal_Fragment profile_personal_fragment;
    ProgressDialog progressDialog;
    boolean isProfilePhotoUpload;
    States states;
    Cities cities;
    SearchableSpinner spState, spCity;

    public static Profile_personal_Fragment newInstance() {
        Profile_personal_Fragment fragment = new Profile_personal_Fragment();
        return fragment;
    }

    /*get state*/
    String[] namesNH = new String[0];
 public   String selectedStateID = "", selectedCityID = "";
 public   String selectedStateNAME = "", selectedCityNAME = "";
    int toselectedpos_State = 0;
    int toselectedpos_City = 0;

    private void getState() {
        String Url = URLS.STATE_URL;

        Log.d(TAG, "STATE_URL: url:- " + Url);
        System.out.println("this is STATE_URL url " + Url + "");
        StringRequest req = new StringRequest(Request.Method.GET, Url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "STATE_URL: response:- " + response);
                Gson gson = new Gson();

                states = gson.fromJson(response, States.class);

//                namesNH = new String[states.getState().size()+1];
                namesNH = new String[states.getState().size()];
                System.out.println("alreadyselected_state " + alreadyselected_state + "");
                //    namesNH[0]="Please Select state";
                for (int i = 0; i < states.getState().size(); i++) {
//                    namesNH[i+1] = states.getState().get(i).getState_name() + "";
                    namesNH[i] = states.getState().get(i).getState_name() + "";
                    //  System.out.println("alreadyselected_state states.getState().get(i).getState_id() " + states.getState().get(i).getState_id() + "");
                    if (alreadyselected_state.compareToIgnoreCase(states.getState().get(i).getState_id()) == 0) {
                        toselectedpos_State = i;
                    }
                }
//                ArrayAdapter arrayAdapter1 = new ArrayAdapter(getContext(),android.R.layout.simple_spinner_dropdown_item,namesNH);
                ArrayAdapter arrayAdapter1 = new ArrayAdapter(getContext(), R.layout.spinner_adapter, namesNH);
                spState.setAdapter(arrayAdapter1);

                spState.setSelection(toselectedpos_State);
                spState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        System.out.println("this is selectesd state " + namesNH[i] + "");
                        System.out.println("this is selectesd state ID " + states.getState().get(i).getState_id() + "");
                        selectedStateID = states.getState().get(i).getState_id() + "";
                        selectedStateNAME = states.getState().get(i).getState_name() + "";
                        System.out.println("this is selectesd state CODE " + states.getState().get(i).getState_code() + "");
                        getCity(states.getState().get(i).getState_id() + "");
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
                /**/
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "getYear: error:- " + error.getMessage());
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    int statusCode = error.networkResponse.statusCode;
                    System.out.println("STATUS CODE " + statusCode + "");
                    DialogUtils.showDialog4Activity(getActivity(), ctx.getResources().getString(R.string.app_name), statusCode + "", null);
                }

            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(req);
    }

    String[] namesNHCity = new String[0];

    private void getCity(String stateID) {
        toselectedpos_City = 0;
        String Url = URLS.CITY_URL + stateID + "";
        Log.d(TAG, "CITY_URL: url:- " + Url);
        System.out.println("this is CITY_URL url " + Url + "");
        StringRequest req = new StringRequest(Request.Method.GET, Url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "CITY_URL: response:- " + response);
                Gson gson = new Gson();

                cities = gson.fromJson(response, Cities.class);

//                namesNH = new String[states.getState().size()+1];
                namesNHCity = new String[cities.getMessage().size()];
                //    namesNH[0]="Please Select state";

                for (int i = 0; i < cities.getMessage().size(); i++) {
//                    namesNH[i+1] = states.getState().get(i).getState_name() + "";
                    namesNHCity[i] = cities.getMessage().get(i).getCity_name() + "";
                    /* System.out.println("alreadyselected_state cities.getMessage().get(i).getCity_id() " + cities.getMessage().get(i).getCity_id() + "");*/
                    if (alreadyselected_city.compareToIgnoreCase(cities.getMessage().get(i).getCity_id()) == 0) {
                        toselectedpos_City = i;
                        System.out.println("alreadyselected_state cities.getMessage().get(i).getCity_id() " + cities.getMessage().get(i).getCity_id() + "");
                    }
                }
//                ArrayAdapter arrayAdapter1 = new ArrayAdapter(getContext(),android.R.layout.simple_spinner_dropdown_item,namesNH);
//                ArrayAdapter arrayAdapter1 = new ArrayAdapter(getContext(), R.layout.spinner_adapter, namesNHCity);
                ArrayAdapter arrayAdapter1 = new ArrayAdapter(ctx, R.layout.spinner_adapter, namesNHCity);
                spCity.setAdapter(arrayAdapter1);
                spCity.setSelection(toselectedpos_City);
                spCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                        System.out.println("this is selectesd City " + namesNHCity[i] + "");
                        System.out.println("this is selectesd City ID " + cities.getMessage().get(i).getCity_id() + "");
                        selectedCityID = cities.getMessage().get(i).getCity_id() + "";
                        selectedCityNAME = cities.getMessage().get(i).getCity_name() + "";
                        System.out.println("this is selectesd CITY state CODE:::::::: " + cities.getMessage().get(i).getCity_state_id() + "");
                        //   getCity(states.getState().get(i).getState_id() + "");
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "getYear: error:- " + error.getMessage());
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    int statusCode = error.networkResponse.statusCode;
                    System.out.println("STATUS CODE " + statusCode + "");
                    DialogUtils.showDialog4Activity(getActivity(), ctx.getResources().getString(R.string.app_name), statusCode + "", null);
                }

            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(req);
    }

    /**
     * getting year data from server
     */
    private void getYear() {
        String Url = URLS.YEAR_EXP_URL;
        Log.d(TAG, "getYear: url:- " + Url);
        System.out.println("this is getYear url " + Url + "");
        StringRequest req = new StringRequest(Request.Method.GET, Url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "getYear: response:- " + response);
                Gson gson = new Gson();

                year = gson.fromJson(response, ExpireYear.class);
                ExpireYear.YearExp yearBean = new ExpireYear.YearExp();
                yearBean.setYearExpId("0");
                yearBean.setYearExpName("Year");
                year.getYearExp().add(0, yearBean);
                CustomYearExpireAdapter customAdapter = new CustomYearExpireAdapter(getActivity(), null, year);
                tvwithin.setAdapter(customAdapter);
//                Type collectionType = new TypeToken<Collection<Year>>() {
//                }.getType();
//                Collection<Year> year = gson.fromJson(response, collectionType);
                if (year != null) {

                    CallMonth();
                    // getLicense();
                } else {
                    // DialogUtils.showDialog4Activity(LoginActivity.this, ctx.getResources().getString(R.string.app_name), logindata.getMessage() + "");

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "getYear: error:- " + error.getMessage());
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    int statusCode = error.networkResponse.statusCode;
                    System.out.println("STATUS CODE " + statusCode + "");
                    DialogUtils.showDialog4Activity(getActivity(), ctx.getResources().getString(R.string.app_name), statusCode + "", null);
                }

            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(req);
    }

    /**
     * this method getting month data from server
     */
    private void CallMonth() {
        String Url = URLS.MONTH_URL;
        Log.d(TAG, "CallMonth: URL:- " + Url);
        StringRequest req = new StringRequest(Request.Method.GET, Url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "CallMonth: Response:- " + response);
                Gson gson = new Gson();

                month = gson.fromJson(response, Month.class);
                Month.MonthBean monthBean = new Month.MonthBean();
                monthBean.setMonth_id("0");
                monthBean.setMonth_name("Month");
                monthBean.setMonth_number("0");

                month.getMonth().add(0, monthBean);

                MonthCustomAdapter customAdapter = new MonthCustomAdapter(getActivity(), null, month.getMonth());
                tvwithinmonth.setAdapter(customAdapter);
//                Type collectionType = new TypeToken<Collection<Month>>() {
//                }.getType();
//                Collection<Month> month = gson.fromJson(response, collectionType);
                if (month != null) {
                    //getYear();
                } else {
                    // DialogUtils.showDialog4Activity(LoginActivity.this, ctx.getResources().getString(R.string.app_name), logindata.getMessage() + "");

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "CallMonth: Response:- " + error.getMessage());
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    int statusCode = error.networkResponse.statusCode;
                    System.out.println("STATUS CODE " + statusCode + "");
                    DialogUtils.showDialog4Activity(getActivity(), ctx.getResources().getString(R.string.app_name), statusCode + "", null);
                }

            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(req);

    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.propfile_personal_fragment, container, false);
        profile_personal_fragment = this;
        ctx = getActivity();
        progressDialog = new ProgressDialog(ctx);
        progressDialog.setCancelable(false);
        SpannableString spannableString = new SpannableString(getString(R.string.please_wait));
        Typeface typefaceSpan = Validations.setTypeface(ctx);
        spannableString.setSpan(typefaceSpan, 0, getString(R.string.please_wait).length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        progressDialog.setMessage(spannableString);
        mySharedPrefereces = new MySharedPrefereces(getContext());
        this.edtzipcode = (CustomEditText) view.findViewById(R.id.edt_zipcode);
        this.gridspeciality = (ExpandableHeightGridView) view.findViewById(R.id.grid_speciality);
        this.btnaddupload = (CustomButton) view.findViewById(R.id.btn_add_upload);
        btnaddupload.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onLicenseSubmitButtonClicked();
            }
        });
        this.tvwithinmonth = (Spinner) view.findViewById(R.id.spComplationMonth);
        this.tvwithin = (Spinner) view.findViewById(R.id.spComplationYear);
        this.edtlicno = (CustomEditText) view.findViewById(R.id.edt_lic_no);
        this.edtuploadlic = (CustomEditText) view.findViewById(R.id.edt_uploadlic);
        edtuploadlic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                isProfilePhotoUpload = false;
                onPhotoUploadButtonClicked();
            }
        });
        cbNeverExpire = (CheckBox) view.findViewById(R.id.cbNeverExpire);
        spState = (SearchableSpinner) view.findViewById(R.id.spState);
        spCity = (SearchableSpinner) view.findViewById(R.id.spCity);
        this.scrollview = (ScrollView) view.findViewById(R.id.scrollview);
        this.listlicence = (ExpandableHeightListView) view.findViewById(R.id.list_licence);
        this.tvlic = (CustomBoldTextView) view.findViewById(R.id.tv_lic);
        this.btnnxt = (CustomButton) view.findViewById(R.id.btn_nxt);
        this.btnback = (CustomButton) view.findViewById(R.id.btn_back);
        this.gridlicence = (ExpandableHeightGridView) view.findViewById(R.id.grid_licence);
        this.tvlicense = (CustomBoldTextView) view.findViewById(R.id.tv_license);
        this.edtoccupation = (CustomEditText) view.findViewById(R.id.edt_occupation);
        this.edtphoto = (CustomEditText) view.findViewById(R.id.edt_photo);
        this.edtphn = (CustomEditText) view.findViewById(R.id.edt_phn);
        this.edtemail = (CustomEditText) view.findViewById(R.id.edt_email);
        this.edtlname = (CustomEditText) view.findViewById(R.id.edt_lname);
        this.edtfname = (CustomEditText) view.findViewById(R.id.edt_fname);
        gridspeciality.setExpanded(true);

        queue = Volley.newRequestQueue(ctx);
        //  CallMonth();
        getYear();
        getLicense();
        //   getState();
        // getspeciality();

//        viewpager = (ViewPager) findViewById(R.id.viewpager);
        btnnxt.setOnClickListener(this);
        edtphoto.setOnClickListener(this);
        // SCROLL VIEW HACK

        // BOGUS
        //ScrollView view = (ScrollView)findViewById(R.id.scrollView);
        scrollview.setDescendantFocusability(ViewGroup.FOCUS_BEFORE_DESCENDANTS);
        scrollview.setFocusable(true);
        scrollview.setFocusableInTouchMode(true);
        scrollview.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                v.requestFocusFromTouch();
                return false;
            }
        });
//        scrollview.setOnTouchListener(new View.OnTouchListener() {
//
//            @Override
//            public boolean onTouch(View v, MotionEvent event) {
//
//                View focussedView = v.findFocus();
//                if (focussedView != null) {
//                    focussedView.clearFocus();
//                }
////                if (edtlicno.hasFocus()) {
////                    edtlicno.clearFocus();
////                }
////             else   if (edtuploadlic.hasFocus()) {
////                    edtuploadlic.clearFocus();
////                }
////                else    if (edtoccupation.hasFocus()) {
////                    edtoccupation.clearFocus();
////                }
////                else  if (edtphoto.hasFocus()) {
////                    edtphoto.clearFocus();
////                }
////                else if (edtphn.hasFocus()) {
////                    edtphn.clearFocus();
////                }
////                else    if (edtemail.hasFocus()) {
////                    edtemail.clearFocus();
////                }
////                else   if (edtlname.hasFocus()) {
////                    edtlname.clearFocus();
////                }
////                else      if (edtfname.hasFocus()) {
////                    edtfname.clearFocus();
////                }
//                return false;
//            }
//        });

        this.gridlicence.setExpanded(true);
        this.listlicence.setExpanded(true);

        applicantLicenseData = new ArrayList<>();
        profile_licenseListAdpter = new Profile_licenseListAdpter(ctx, applicantLicenseData, new Profile_licenseListAdpter.OnActionDeleteClickedListener() {
            @Override
            public void onDeleteButtonClicked(ApplicantLicenseModel.Datum data) {
                deleteApplicantLicence(data);
            }
        }, true);

        listlicence.setAdapter(profile_licenseListAdpter);
        getApplicantLicenceDetail();
        //CustomAdapter customAdapter = new CustomAdapter(getActivity(), null, expireYr);
        ///tvwithin.setAdapter(customAdapter);

        return view;
    }

    /**
     * this method is used to delete applicant license
     *
     * @param data
     */
    public void deleteApplicantLicence(final ApplicantLicenseModel.Datum data) {
        String url = URLS.DELETE_APPLICANT_LICENCE_DETAIL + mySharedPrefereces.getUserId() + "&license_id=" + data.getApplicantLicenseId();
        Log.d(TAG, "deleteApplicantLicence: URL " + url);
        CommonRequestHelper.getDataFromUrl(url, new CommonRequestHelper.VolleyResponseCustom() {
            @Override
            public void getServerResponse(String response) {
                Log.d(TAG, "deleteApplicantLicence: response " + response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int id = jsonObject.getInt("status");
                    if (id > 0) {
                        getApplicantLicenceDetail();
                        DialogUtils.showDialog4Activity(getActivity(), ctx.getResources().getString(R.string.app_name), jsonObject.getString("message") + "", null);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void getVolleyError(VolleyError volleyError) {
                Log.d(TAG, "deleteApplicantLicence: error " + volleyError.getMessage());
                DialogUtils.noInterNetDialog(ctx, "No Internet Connection", "No active internet connection available", new DialogUtils.NoInterNetDailogClickListner() {
                    @Override
                    public void onDialogOkButtonClicked() {
                        deleteApplicantLicence(data);
                    }

                    @Override
                    public void onDialogCancelButtonClicked() {

                    }
                });

            }
        }, ctx);

    }

    /**
     * on addlicense button clicked
     */

    public void onLicenseSubmitButtonClicked() {
        String licenceNumber = edtlicno.getText().toString();
        if (!cbNeverExpire.isChecked() && tvwithin.getSelectedItemPosition() == 0) {
            Toast.makeText(ctx, "Please Select Expire Year", Toast.LENGTH_LONG).show();
        } else if (!cbNeverExpire.isChecked() && tvwithinmonth.getSelectedItemPosition() == 0) {
            Toast.makeText(ctx, "Please Select Expire Month", Toast.LENGTH_LONG).show();
        } else if (licenceNumber.trim().length() == 0) {
            Toast.makeText(ctx, "Please Enter License Number", Toast.LENGTH_LONG).show();
        } else if (imgByteArray == null || imgByteArray.trim().length() == 0) {
            Toast.makeText(ctx, "Please Upload License Photo", Toast.LENGTH_LONG).show();
        } else {
            if (cbNeverExpire.isChecked()) {
                manageApplicantLicenceDetail("", licenceNumber, month.getMonth().get(tvwithinmonth.getSelectedItemPosition()).getMonth_id(), year.getYearExp().get(tvwithin.getSelectedItemPosition()).getYearExpId(), imgByteArray, "1");
            } else {
                manageApplicantLicenceDetail("", licenceNumber, month.getMonth().get(tvwithinmonth.getSelectedItemPosition()).getMonth_id(), year.getYearExp().get(tvwithin.getSelectedItemPosition()).getYearExpId(), imgByteArray, "");
            }
        }


    }

    public void onPhotoUploadButtonClicked() {
        //Toast.makeText(ctx,"Photo upload button clicked",Toast.LENGTH_LONG).show();
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Validations.hasPermissions(ctx, Validations.RUN_TIME_PERMITIONS)) {
                ActivityCompat.requestPermissions(getActivity(), Validations.RUN_TIME_PERMITIONS, Validations.REQUEST_CODE_PERMISSION);
            } else {
                try {
                    InputMethodManager imm = (InputMethodManager) ctx.getSystemService(
                            Context.INPUT_METHOD_SERVICE);
                    //imm.hideSoftInputFromWindow(  .getWindowToken(), 0);
                } catch (Exception ex) {
                }

                OpenCameraDialog();
            }
        } else {
            try {
                InputMethodManager imm = (InputMethodManager) ctx.getSystemService(
                        Context.INPUT_METHOD_SERVICE);
                //imm.hideSoftInputFromWindow(etPROFILEIMAGE  .getWindowToken(), 0);
            } catch (Exception ex) {
            }

            OpenCameraDialog();
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub

        if (requestCode == Validations.CAMERA_REQUEST
                && resultCode == Activity.RESULT_OK) {

            try {

                //Bitmap photo = (Bitmap) data.getExtras().get("data");

                if (data.getData() == null) {
                    bitmap = (Bitmap) data.getExtras().get("data");
                } else {
                    bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
                }


                //ivInsertLocationImageShow.setImageBitmap(bitmap);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                final byte[] byteImage = byteArrayOutputStream.toByteArray();


                imgByteArray = Base64.encodeToString(byteImage, Base64.DEFAULT);
                edtuploadlic.setText("img_" + System.currentTimeMillis() + ".jpg");


            } catch (Exception ex) {
                Toast.makeText(ctx, getString(R.string.something_went_wrong), Toast.LENGTH_LONG).show();
            }

        } else if (PROFILE_CAMERA_REQUEST == requestCode && resultCode == Activity.RESULT_OK) {
            try {

                if (data.getData() == null) {
                    bitmap = (Bitmap) data.getExtras().get("data");
                } else {
                    bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
                }
                /*Bitmap thumbnail = MediaStore.Images.Media.getBitmap(
                        getContentResolver(), imageUri);*/
                //ivShowImage.setImageBitmap(bitmap);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                final byte[] byteImage;
                byteImage = byteArrayOutputStream.toByteArray();
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        imgByteArray = Base64.encodeToString(byteImage, Base64.DEFAULT);

                        if (isProfilePhotoUpload) {
                            edtphoto.setText("img_" + System.currentTimeMillis() + ".jpg");
                            sendProfilePhotoToServer(imgByteArray);
                        } else {
                            edtuploadlic.setText("img_" + System.currentTimeMillis() + ".jpg");
                        }
                    }
                });


            } catch (Exception ex) {

                Toast.makeText(getContext(), getResources().getString(R.string.something_went_wrong), Toast.LENGTH_LONG).show();

            }
        } else if (PROFILE_GALLERY_REQUEST == requestCode && resultCode == Activity.RESULT_OK) {
            try {

                Uri uri = Uri.parse(data.getData().getPath());

                String fileUrl = FileUtils.getPath(getContext(), data.getData());
                /*if(fileUrl==null)
                    File file=FileUtils.getFile(getContext(),data.getData());*/

                /*if (Build.VERSION.SDK_INT < 11)
                    fileUrl = RealPathUtil.getRealPathFromURI_BelowAPI11(getContext(), data.getData());

                    // SDK >= 11 && SDK < 19
                else if (Build.VERSION.SDK_INT < 19)
                    fileUrl = RealPathUtil.getRealPathFromURI_API11to18(getContext(), data.getData());

                    // SDK > 19 (Android 4.4)
                else
                    fileUrl = RealPathUtil.getRealPathFromURI_API19(getContext(), data.getData());*/

                //String fileUrl=UploadDucumentUrl.getUriRealPath(getContext(),data.getData());
                Log.d(TAG, "onActivityResult: " + data.getData().getPath() + " file url:" + fileUrl);
                File file = new File(fileUrl);
                Log.d(TAG, "onActivityResult: " + "file is exist:" + file.exists() + " file absolute Path" + file.getAbsolutePath());
                if (file.exists() && file.length() > 0) {
                    byte[] byteImage = readByteFromFile(file);
                    imgByteArray = Base64.encodeToString(byteImage, Base64.DEFAULT);
                    edtphoto.setText(file.getName());
                    if (isProfilePhotoUpload) {
                        edtphoto.setText(file.getName());
                        sendProfilePhotoToServer(imgByteArray);
                    } else {
                        edtuploadlic.setText(file.getName());
                    }
                    Log.d("SaveExpensesFragment", imgByteArray);
                } else {
                    Log.d("SaveExpensesFragment", "image not exists or file size 0");
                }


            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(getContext(), getResources().getString(R.string.something_went_wrong), Toast.LENGTH_LONG).show();
            }
        }

    }


    public void sendProfilePhotoToServer(final String data) {
        progressDialog.show();
        String Url = URLS.UPLOAD_APPLICANT_IMAGE;
        queue = Volley.newRequestQueue(ctx);
        //String url = URLS.UPDATE_MANAGER_PROFILE_DETAIL;
        Log.d(TAG, "sendProfileDataToServer: URL: " + Url);

        StringRequest postRequest = new StringRequest(Request.Method.POST, Url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        Log.d(TAG, "sendProfileDataToServer: Response: " + response + "");
                        // response
                        Log.d("Response", response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int id = jsonObject.getInt("status");
                            if (id > 0) {
                                //getProfileDetailFromaApi();
                                imgByteArray = "";
                                getPersonalDetails();
                                DialogUtils.showDialog4Activity(getActivity(), ctx.getResources().getString(R.string.app_name), jsonObject.getString("message") + "", null);
                            } else {
                                DialogUtils.showDialog4Activity(getActivity(), ctx.getResources().getString(R.string.app_name), jsonObject.getString("message") + "", null);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Log.d(TAG, "sendProfileDataToServer: Error: " + error.getMessage());
                        // error
                        Log.d("Error.Response", error.getMessage());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("type", "upload_image_applicant");
                params.put("applicant_id", mySharedPrefereces.getUserId());
                params.put("image", data);
                Log.d(TAG, "getParams: request parameters" + params.toString());
                return params;
            }
        };
        queue.add(postRequest);

    }

    public byte[] readByteFromFile(File file) throws IOException {
        ByteArrayOutputStream ous = null;
        InputStream ios = null;
        try {
            byte[] buffer = new byte[4096];
            ous = new ByteArrayOutputStream();
            ios = new FileInputStream(file);
            int read = 0;
            while ((read = ios.read(buffer)) != -1) {
                ous.write(buffer, 0, read);
            }
        } finally {
            try {
                if (ous != null)
                    ous.close();
            } catch (IOException e) {
            }

            try {
                if (ios != null)
                    ios.close();
            } catch (IOException e) {
            }
        }
        return ous.toByteArray();
    }

    boolean firstTime = true;

    public void getApplicantLicenceDetail() {
        String Url = URLS.GET_APPLICANT_LICENCE_DETAIL + mySharedPrefereces.getUserId();//"35";//+mySharedPrefereces.getUserId();;+"&"+URLS.LICENSE_ID+"="+mySharedPrefereces.getFacility_id();
        Log.d("Applicant license", Url);
        applicantLicenseData.clear();
        CommonRequestHelper.getDataFromUrl(Url, new CommonRequestHelper.VolleyResponseCustom() {
            @Override
            public void getServerResponse(String response) {
                Log.d("Applicant licence", response);
                Gson gson = new Gson();

                applicantLicenseModel = gson.fromJson(response, ApplicantLicenseModel.class);
                if (applicantLicenseModel != null && applicantLicenseModel.getData() != null) {
                    applicantLicenseData.addAll(applicantLicenseModel.getData());
                    profile_licenseListAdpter.notifyDataSetChanged();

                }
            }

            @Override
            public void getVolleyError(VolleyError volleyError) {
                NetworkResponse networkResponse = volleyError.networkResponse;
                if (networkResponse != null) {
                    int statusCode = volleyError.networkResponse.statusCode;
                    System.out.println("STATUS CODE " + statusCode + "");
                    DialogUtils.showDialog4Activity(getActivity(), ctx.getResources().getString(R.string.app_name), statusCode + "", null);
                }

            }
        }, ctx);

    }

    /**
     * this method is used to manage applicant License Detail
     *
     * @param license_id  pass when update license
     * @param number      number of license
     * @param month       month of your license
     * @param year        year of license
     * @param image       image of license
     * @param neverExpire is never expire license
     */

    public void manageApplicantLicenceDetail(final String license_id, final String number, final String month, final String year, final String image, final String neverExpire) {
        //queue = Volley.newRequestQueue(ctx);
        String url = URLS.MANAGE_APPLICANT_LICECNSE;
        progressDialog.show();
        Log.d(TAG, "manageApplicantLicenceDetail: URL" + url);
        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        // response
                        Log.d(TAG, "manageApplicantLicenceDetail: response" + response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int id = jsonObject.getInt("status");
                            if (id > 0) {
                                edtuploadlic.setText("");
                                edtlicno.setText("");
                                cbNeverExpire.setChecked(false);
                                tvwithin.setSelection(0);
                                imgByteArray = "";
                                tvwithinmonth.setSelection(0);
                                getApplicantLicenceDetail();
                                DialogUtils.showDialog4Activity(getActivity(), ctx.getResources().getString(R.string.app_name), jsonObject.getString("message") + "", null);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        // error
                        Log.d(TAG, "manageApplicantLicenceDetail: response" + error.getMessage());
                        DialogUtils.noInterNetDialog(ctx, "No Internet Connection", "No active internet connection available", new DialogUtils.NoInterNetDailogClickListner() {
                            @Override
                            public void onDialogOkButtonClicked() {
                                manageApplicantLicenceDetail(license_id, number, month, year, image, neverExpire);
                            }

                            @Override
                            public void onDialogCancelButtonClicked() {

                            }
                        });

                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("type", "manage_applicant_license_detail");
                params.put("applicant_id", mySharedPrefereces.getUserId());
                params.put("license_id ", license_id);
                params.put("number", number);
                params.put("neverexpire", neverExpire);
                params.put("month", month);
                params.put("year", year);
                params.put("license_image", image);
                Log.d(TAG, "manageApplicantLicenceDetail getParams: " + params.toString());
                return params;
            }
        };
        queue.add(postRequest);
    }

    private void getspeciality() {
        String Url = URLS.SPECIALTY_URL;
        Log.d(TAG, "getspeciality: URL:-" + Url);
        StringRequest req = new StringRequest(Request.Method.GET, Url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "getspeciality: Response:-" + response);
                Gson gson = new Gson();

                speciality = gson.fromJson(response, Speciality.class);
//                Type collectionType = new TypeToken<Collection<Speciality>>() {
//                }.getType();
//                Collection<Speciality> speciality = gson.fromJson(response, collectionType);
                if (speciality != null) {

                    System.out.println("TEST " + speciality.getSpecialty().get(0).getSpecialty_id() + "");
                    System.out.println("TEST " + speciality.getSpecialty().get(0).getSpecialty_name() + "");
                    System.out.println("TEST " + speciality.getSpecialty().size() + "");
//                    for (int i = 0; i < speciality.getSpecialty().size(); i++) {
//                        addRadioButtons(i, rootView, speciality.getSpecialty().get(i).getSpecialty_name());
//                    }
                    specialityAdapter = new SpecialityMultiSelectAdapter(speciality, ctx);
                    gridspeciality.setAdapter(specialityAdapter);

                    getPersonalDetails();
//                  for(int i=0;i<speciality.size();i++)
//                  {
//
//                  }

                } else {

                    // DialogUtils.showDialog4Activity(LoginActivity.this, ctx.getResources().getString(R.string.app_name), logindata.getMessage() + "");

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "getspeciality: Response:-" + error.getMessage());
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    int statusCode = error.networkResponse.statusCode;
                    System.out.println("STATUS CODE " + statusCode + "");
                    DialogUtils.showDialog4Activity(getActivity(), ctx.getResources().getString(R.string.app_name), statusCode + "", null);
                }

            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(req);

    }

    /**
     * this method is used to getting Personal detail from server
     */
    String alreadyselected_state = "";
    String alreadyselected_city = "";

    private void getPersonalDetails() {

        String Url = URLS.PROFILE_PERSONALDETAIL_URL + mySharedPrefereces.getUserId();
        Log.d(TAG, "getPersonalDetails: URL:- " + Url);
        StringRequest req = new StringRequest(Request.Method.GET, Url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                System.out.println("CallLicense RESPONSE " + response + "");
                Gson gson = new Gson();
                Log.d(TAG, "getPersonalDetails: response:- " + response);
                profilePersonalDetail = gson.fromJson(response, ProfilePersonalDetail.class);
//                Type collectionType = new TypeToken<Collection<License>>() {
//                }.getType();
//                Collection<License> license = gson.fromJson(response, collectionType);
                if (profilePersonalDetail != null) {
                    edtfname.setText(profilePersonalDetail.getData().getApplicant_firstname() + "");
                    edtemail.setText(profilePersonalDetail.getData().getApplicant_email() + "");
                    edtlname.setText(profilePersonalDetail.getData().getApplicant_lastname() + "");
                    edtphn.setText(profilePersonalDetail.getData().getApplicant_phone() + "");
                    edtzipcode.setText(profilePersonalDetail.getData().getApplicant_zipcode() + "");
                    String s = profilePersonalDetail.getData().getApplicant_license() + "";
                    alreadyselected_city = profilePersonalDetail.getData().getApplicant_city_id();
                    alreadyselected_state = profilePersonalDetail.getData().getApplicant_state_id();
                  //  selectedStateNAME=profilePersonalDetail.getData().getst();
                  //  selectedCityID = alreadyselected_city;
                //    selectedStateID = alreadyselected_state;

                    System.out.println("alreadyselected_city:::::::::::::::  " + alreadyselected_city + "");
                    System.out.println("alreadyselected_state::::::::::::::: " + alreadyselected_state + "");

                    mySharedPrefereces.setUserImage(profilePersonalDetail.getData().getApplicant_image() + "");
                    if (firstTime) {
                        getState();
                        firstTime = false;
                    }
                    if (DashBoardActivity.civ_requests != null) {
                        Glide.with(ctx)
                                .load(profilePersonalDetail.getData().getApplicant_image() + "")
                                .error(R.drawable.no_image)
                                //.placeholder(R.drawable.no_image)
                                .into(DashBoardActivity.civ_requests);
                    }
                    licenseAdapter.selected = new ArrayList<>();
                    licenseAdapter.selected.clear();
                    if (s != null) {
                        if (s.contains(",")) {
                            String temp[] = s.split(",");
                            for (int i = 0; i < temp.length; i++) {
                                licenseAdapter.selected.add(temp[i] + "");
                            }
                        } else {
                            licenseAdapter.selected.add(s + "");
                        }
                        if (licenseAdapter != null) {
                            licenseAdapter.notifyDataSetChanged();
                        }
                    }
                    specialityAdapter.selected = new ArrayList<>();
                    specialityAdapter.selected.clear();
                    String speciality = profilePersonalDetail.getData().getApplicant_specialities() + "";
                    if (speciality != null) {
                        if (speciality.contains(",")) {
                            String temp[] = speciality.split(",");
                            for (int i = 0; i < temp.length; i++) {
                                specialityAdapter.selected.add(temp[i] + "");
                            }
                        } else {
//                            licenseAdapter.selected.add(speciality + "");

                            specialityAdapter.selected.add(speciality + "");
                        }
                        if (specialityAdapter != null) {
                            specialityAdapter.notifyDataSetChanged();
                        }
                    }


                } else {
                    // DialogUtils.showDialog4Activity(LoginActivity.this, ctx.getResources().getString(R.string.app_name), logindata.getMessage() + "");

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "getPersonalDetails: response:- " + error.getMessage());
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    int statusCode = error.networkResponse.statusCode;
                    System.out.println("STATUS CODE " + statusCode + "");
                    DialogUtils.showDialog4Activity(getActivity(), ctx.getResources().getString(R.string.app_name), statusCode + "", null);
                }

            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(req);
    }

    /**
     * this method is used to getting license detail from server
     */

    private void getLicense() {
        final String Url = URLS.LICENSE_URL;
        Log.d(TAG, "getLicense: URL" + Url);
        StringRequest req = new StringRequest(Request.Method.GET, Url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "getLicense: Response" + response);
                Gson gson = new Gson();

                license = gson.fromJson(response, License.class);
//                Type collectionType = new TypeToken<Collection<License>>() {
//                }.getType();
//                Collection<License> license = gson.fromJson(response, collectionType);
                if (license != null) {
                    licenseAdapter = new LicenseAdapter(license, ctx);
                    gridlicence.setAdapter(licenseAdapter);
                    getspeciality();
                } else {
                    // DialogUtils.showDialog4Activity(LoginActivity.this, ctx.getResources().getString(R.string.app_name), logindata.getMessage() + "");

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "getLicense: Error" + error.getMessage());
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    int statusCode = error.networkResponse.statusCode;
                    System.out.println("STATUS CODE " + statusCode + "");
                    DialogUtils.showDialog4Activity(getActivity(), ctx.getResources().getString(R.string.app_name), statusCode + "", null);
                }

            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(req);
    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.btn_nxt:
//ProfileFragment profileFragment=new ProfileFragment();
//profileFragment.changePage(2);
                //ProfileFragment.viewPager.setCurrentItem(1);
                getCheckUserInsertData();
                break;
            case R.id.edt_photo:

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                    if (!Validations.hasPermissions(ctx, Validations.RUN_TIME_PERMITIONS)) {
                        ActivityCompat.requestPermissions(getActivity(), Validations.RUN_TIME_PERMITIONS, Validations.REQUEST_CODE_PERMISSION);
                    } else {
                        try {
                            InputMethodManager imm = (InputMethodManager) ctx.getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(edtphoto.getWindowToken(), 0);
                        } catch (Exception ex) {
                        }
                        isProfilePhotoUpload = true;
                        OpenCameraDialog();
                    }
                } else {
                    try {
                        InputMethodManager imm = (InputMethodManager) ctx.getSystemService(
                                Context.INPUT_METHOD_SERVICE);
                        imm.hideSoftInputFromWindow(edtphoto.getWindowToken(), 0);
                    } catch (Exception ex) {
                    }
                    isProfilePhotoUpload = true;
                    OpenCameraDialog();
                }

                break;
        }
    }

    public void OpenCameraDialog() {

        try {

            takeImgDialog = new Dialog(getContext());
            //takeImgDialog.setTitle("Upload Photo");
            takeImgDialog.setContentView(R.layout.layout_dialog_capture_image);
            takeImgDialog.setCancelable(false);

            CustomTextView tvTakePhoto = (CustomTextView) takeImgDialog.findViewById(R.id.tvTakePhoto);
            CustomTextView tvChoosePhoto = (CustomTextView) takeImgDialog.findViewById(R.id.tvChoosePhoto);
            CustomTextView tvCancel = (CustomTextView) takeImgDialog.findViewById(R.id.tvCancel);

            tvTakePhoto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    /*File dir =new File(Environment.getExternalStorageDirectory().getAbsolutePath()+File.separator+"document_download");
                    if(!dir.exists())
                    {
                        dir.mkdir();
                    }
                    imageFileName="img_"+System.currentTimeMillis()+".jpg";
                    File imageFile = new File(dir, imageFileName);
                    imageUri = Uri.fromFile(imageFile);*/
                    Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                    startActivityForResult(cameraIntent, PROFILE_CAMERA_REQUEST);
                    takeImgDialog.dismiss();
                }
            });

            tvChoosePhoto.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {



                 /*   Intent pickPhoto = new Intent(Intent.ACTION_PICK,
                            android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);*/
                    Intent pickPhoto = new Intent(Intent.ACTION_GET_CONTENT);
                    String[] mimetypes = {"image/*", "application/*"};
                    pickPhoto.setType("image/*");
                    //pickPhoto.setAction(Intent.ACTION_GET_CONTENT);
                    //pickPhoto.putExtra(Intent.EXTRA_MIME_TYPES, mimetypes);
                    startActivityForResult(pickPhoto, PROFILE_GALLERY_REQUEST);
                    takeImgDialog.dismiss();
                }
            });

            tvCancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    takeImgDialog.dismiss();
                }
            });
            takeImgDialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
            takeImgDialog.show();
        } catch (Exception ex) {
        }

    }


    /**
     * this method called when use clicked next button
     * this method is validation of use personal detail
     */
    public void getCheckUserInsertData() {
        String firstName = edtfname.getText().toString();
        String lastName = edtlname.getText().toString();
        String email = edtemail.getText().toString();
        String phone = edtphn.getText().toString();
        String zipcode = edtzipcode.getText().toString();
        System.out.println("THIS IS SELECTED LICENCE " + licenseAdapter.getSelected() + "");
        System.out.println("THIS IS SELECTED LICENCE NAMES " + licenseAdapter.getSelectedItem() + "");
        String license = TextUtils.join(",", licenseAdapter.getSelected());
        String specility = TextUtils.join(",", specialityAdapter.getSelected());
        //    String license=getUnique(licenseAdapter.getSelected()+"");


        if (firstName.trim().length() == 0) {
            Toast.makeText(ctx, "Please Enter First Name", Toast.LENGTH_SHORT).show();
        } else if (!Validations.isValidName(firstName)) {
            Toast.makeText(ctx, "Please Enter Valid First Name", Toast.LENGTH_SHORT).show();
        } else if (lastName.trim().length() == 0) {
            Toast.makeText(ctx, "Please Enter Last Name", Toast.LENGTH_SHORT).show();
        } else if (!Validations.isValidName(lastName)) {
            Toast.makeText(ctx, "Please Enter Valid Last Name", Toast.LENGTH_SHORT).show();
        } else if (email.trim().length() == 0) {
            Toast.makeText(ctx, "Please Enter Email", Toast.LENGTH_SHORT).show();
        } else if (!Validations.isValidEmail(email)) {
            Toast.makeText(ctx, "Please Enter Valid Email", Toast.LENGTH_SHORT).show();
        } else if (zipcode.trim().length() == 0 || zipcode.trim().length() != 5) {
            Toast.makeText(ctx, "Please Enter Zip Code", Toast.LENGTH_SHORT).show();
        }
        else if (!Validations.isValidPhoneNumber(phone)) {
            Toast.makeText(ctx, "Please Enter Valid Phone Number", Toast.LENGTH_SHORT).show();
        }
        else {
            submitDataToServer(firstName, lastName, email, phone, zipcode, license, specility);
        }
    }


    /***
     * submit user personal detail to server
     * @param firstName
     * @param lastName
     * @param email
     * @param phone
     * @param zip
     * @param license
     * @param specialities
     */

    public void submitDataToServer(final String firstName, final String lastName, final String email, final String phone, final String zip, final String license, final String specialities) {
//        String Url = URLS.UPDATE_APPLICANT_PROFILE_URL + mySharedPrefereces.getUserId() + "&" + URLS.FIRST_NAME + "=" + firstName + "&" + URLS.LAST_NAME + "=" + lastName + "&" + URLS.EAMIL + "=" + email + "&" + URLS.PHONE + "=" + phone + "&" + URLS.ZIP_CODE + "=" + zip + "&" + URLS.LICENSE + "=" + license + "&" + URLS.SPECIALITIES + "=" + specialities;

        String Url = URLS.UPDATE_APPLICANT_PROFILE_URL + mySharedPrefereces.getUserId() + "&" + URLS.FIRST_NAME + "=" + firstName + "&" + URLS.LAST_NAME + "=" + lastName + "&" + URLS.EAMIL + "=" + email + "&" + URLS.PHONE + "=" + phone + "&" + URLS.ZIP_CODE + "=" + zip + "&" + URLS.LICENSE + "=" + license + "&" + URLS.SPECIALITIES + "=" + specialities + "&state_id=" + selectedStateID + "&city_id=" + selectedCityID + "";
        URL url = null;
        try {
            url = new URL(Url);
            URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef());
            url = uri.toURL();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        Log.d(TAG, "submitDataToServer: URL::::::::: " + url);
        StringRequest req = new StringRequest(Request.Method.GET, url.toString(), new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                System.out.println("CallLicense RESPONSE " + response + "");
                Log.d(TAG, "submitDataToServer: Response " + response);
//                Type collectionType = new TypeToken<Collection<License>>() {
//                }.getType();
//                Collection<License> license = gson.fromJson(response, collectionType);
                /*if (license != null) {
                    licenseAdapter = new LicenseAdapter(license, ctx);
                    gridlicence.setAdapter(licenseAdapter);
                    getspeciality();
                } else {
                    // DialogUtils.showDialog4Activity(LoginActivity.this, ctx.getResources().getString(R.string.app_name), logindata.getMessage() + "");

                }*/

                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String message = jsonObject.getString("message");
                    int status = jsonObject.getInt("status");
                    if (status == 1) {
                        getPersonalDetails();
//                        ProfileFragment.viewPager.setCurrentItem(1);
                        ProfileFragment.viewPager.setCurrentItem(1);
                    } else {
                        DialogUtils.showDialog4Activity(ctx, ctx.getResources().getString(R.string.app_name), message + "", null);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "submitDataToServer: Error " + error.getMessage());
                NetworkResponse networkResponse = error.networkResponse;
                DialogUtils.noInterNetDialog(ctx, "No Internet Connection", "No active internet connection available", new DialogUtils.NoInterNetDailogClickListner() {
                    @Override
                    public void onDialogOkButtonClicked() {
                        submitDataToServer(firstName, lastName, email, phone, zip, license, specialities);
                    }

                    @Override
                    public void onDialogCancelButtonClicked() {

                    }
                });


            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(req);

    }
}
