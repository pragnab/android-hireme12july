/*
 * Copyright (c) 2017. Truiton (http://www.truiton.com/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 * Mohit Gupt (https://github.com/mohitgupt)
 *
 */

package com.hiremehealthcare.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.android.volley.NetworkResponse;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.hiremehealthcare.CommonCls.DialogUtils;
import com.hiremehealthcare.CommonCls.URLS;
import com.hiremehealthcare.CommonCls.Validations;
import com.hiremehealthcare.POJO.ApplicantNearPositionModel;
import com.hiremehealthcare.R;
import com.hiremehealthcare.Utils.CommonRequestHelper;
import com.hiremehealthcare.activity.ViewPositionDetailActivity;
import com.hiremehealthcare.adapter.Applicant_Near_Duplicate;
import com.hiremehealthcare.database.MySharedPrefereces;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ApplicantNearPostionFragment extends Fragment {

    RecyclerView rvApplicantNearPosition;
    public List<ApplicantNearPositionModel.Data> applicantNearPositionModelList;
    public Applicant_Near_Duplicate applicantNearPositionAdapter;
    MySharedPrefereces mySharedPrefereces;
    ProgressDialog progressDialog;
    public static ApplicantNearPostionFragment applicantNearPostionFragment;
    Context context;
    String TAG = "ApplicantNearPostionFragment";
    private android.widget.ImageView ivnodata;

    public static ApplicantNearPostionFragment newInstance() {
        ApplicantNearPostionFragment fragment = new ApplicantNearPostionFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.fragment_item_one, container, false);
        this.rvApplicantNearPosition = (RecyclerView) rootView.findViewById(R.id.rvApplicantNearPosition);
        this.ivnodata = (ImageView) rootView.findViewById(R.id.iv_nodata);
        applicantNearPostionFragment = this;
        context = getContext();
        initAllControls(rootView);
        return rootView;
    }

    public void initAllControls(View rootView) {
        mySharedPrefereces = new MySharedPrefereces(context);
        progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        SpannableString spannableString = new SpannableString(getString(R.string.please_wait));
        Typeface typefaceSpan = Validations.setTypeface(context);
        spannableString.setSpan(typefaceSpan, 0, getString(R.string.please_wait).length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        progressDialog.setMessage(spannableString);
        rvApplicantNearPosition = (RecyclerView) rootView.findViewById(R.id.rvApplicantNearPosition);
        applicantNearPositionModelList = new ArrayList<>();
//        applicantNearPositionAdapter = new ApplicantNearPositionAdapter(applicantNearPositionModelList,  false,context,new ApplicantNearPositionAdapter.OnRecyclerViewItemClicked() {
//            @Override
//            public void onRecyclerItemClicked(ApplicantNearPositionModel.Data data) {
//                Intent intent=new Intent(context,ViewPositionDetailActivity.class);
//                intent.putExtra("position_id", data.getPositionId());
//                intent.putExtra("is_show_interest", true);
//                startActivity(intent);
//            }
//
//            @Override
//            public void onHeartButtonClicked(ApplicantNearPositionModel.Data data) {
//                onRecyclerViewHeartButtonClicked(data);
//            }
//        });
        applicantNearPositionAdapter = new Applicant_Near_Duplicate(applicantNearPositionModelList, false, context, new Applicant_Near_Duplicate.OnRecyclerViewItemClicked() {
            @Override
            public void onRecyclerItemClicked(ApplicantNearPositionModel.Data data) {
                Intent intent = new Intent(context, ViewPositionDetailActivity.class);
//                intent.putExtra("position_id", data.getPositionId() + "");

                System.out.println("THIS IS CLICKED EVENT->>>>>>>>>>>>>>>>>>>>>>> "+data.getPositionId()+"");
                intent.putExtra("position_id", data.getPositionId() + "");
                intent.putExtra("is_show_interest", true);
                startActivity(intent);
            }

            @Override
            public void onHeartButtonClicked(ApplicantNearPositionModel.Data data) {
                onRecyclerViewHeartButtonClicked(data);
            }

            @Override
            public void onAvailabilityButtonClicked(ApplicantNearPositionModel.Data data) {

            }
        });
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
        rvApplicantNearPosition.setLayoutManager(mLayoutManager);
        rvApplicantNearPosition.setItemAnimator(new DefaultItemAnimator());
        rvApplicantNearPosition.setAdapter(applicantNearPositionAdapter);
        getApplicantNearPosionDataFromApi();

    }

    /**
     * this method called when use clicked on heartButton from list in android
     * this method send interest on position api
     *
     * @param data
     */
    public void onRecyclerViewHeartButtonClicked(final ApplicantNearPositionModel.Data data) {
        progressDialog.show();
        String url = URLS.APPLICANT_SHOW_INTEREST_ON_POSITION + mySharedPrefereces.getUserId() + "&" + URLS.POSITION_ID + "=" + data.getPositionId();
        Log.d(TAG, "onRecyclerViewHeartButtonClicked: URL" + url);
        CommonRequestHelper.getDataFromUrl(url, new CommonRequestHelper.VolleyResponseCustom() {

            @Override
            public void getServerResponse(String response) {
                progressDialog.dismiss();
                Log.d(TAG, "onRecyclerViewHeartButtonClicked: Response" + response);
                Gson gson = new Gson();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int status = jsonObject.getInt("status");
                    String message = jsonObject.getString("message");
                    //22-oct Pragna ppppppppppp   getApplicantNearPosionDataFromApi();
                    DialogUtils.showDialog4Activity(getActivity(), context.getResources().getString(R.string.app_name), message, null);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                /*ApplicantNearPositionModel applicantNearPositionModel = gson.fromJson(response, ApplicantNearPositionModel.class);
                if(applicantNearPositionModel.getStatus()==1)
                {
                    if(applicantNearPositionModel.getData().size()==0)
                        Toast.makeText(context,"No data found",Toast.LENGTH_LONG).show();
                    applicantNearPositionModelList.addAll(applicantNearPositionModel.getData());
                    applicantNearPositionAdapter.notifyDataSetChanged();
                }
                else
                {

                }*/
            }

            @Override
            public void getVolleyError(VolleyError volleyError) {
                Log.d(TAG, "onRecyclerViewHeartButtonClicked: Error" + volleyError.getMessage());
                progressDialog.dismiss();
                NetworkResponse networkResponse = volleyError.networkResponse;
                DialogUtils.noInterNetDialog(context, "No Internet Connection", "No active internet connection available", new DialogUtils.NoInterNetDailogClickListner() {
                    @Override
                    public void onDialogOkButtonClicked() {
                        onRecyclerViewHeartButtonClicked(data);
                    }

                    @Override
                    public void onDialogCancelButtonClicked() {

                    }
                });

            }
        }, context);


    }


    /**
     * this method is used to getting applicant near position data from api
     */

    public void getApplicantNearPosionDataFromApi() {
        applicantNearPositionModelList.clear();
        progressDialog.show();
        //http://hiremehealthcare.iipldemo.com/app/api.php?type=positions_near_you&applicant_id=21&applicant_zipcode=28278
        String url = URLS.APPLICANT_NEAR_POSITION + mySharedPrefereces.getUserId() + "&" + URLS.APPLICANT_ZIP_CODE + "=" + mySharedPrefereces.getZipCode();
        //String url= URLS.APPLICANT_NEAR_POSITION+mySharedPrefereces.getUserId()+"&"+URLS.APPLICANT_ZIP_CODE+"=28207";//+mySharedPrefereces.getZipCode();
        Log.d(TAG, "getApplicantNearPosionDataFromApi: URL:- " + url);
        CommonRequestHelper.getDataFromUrl(url, new CommonRequestHelper.VolleyResponseCustom() {

            @Override
            public void getServerResponse(String response) {
                progressDialog.dismiss();
                Log.d(TAG, "getApplicantNearPosionDataFromApi: Response:- " + response);
                Gson gson = new Gson();
                ApplicantNearPositionModel applicantNearPositionModel = gson.fromJson(response, ApplicantNearPositionModel.class);
                if (applicantNearPositionModel.getStatus() == 1) {
                    //if(applicantNearPositionModel.getData().size()==0)
                    //Toast.makeText(context,"No data found",Toast.LENGTH_LONG).show();
                    if (applicantNearPositionModel.getData().size() > 0) {
                        applicantNearPositionModelList.addAll(applicantNearPositionModel.getData());
                        rvApplicantNearPosition.setVisibility(View.VISIBLE);
                        ivnodata.setVisibility(View.GONE);

                    } else {
                        //  applicantNearPositionAdapter.showDataEmptyDate();

                        rvApplicantNearPosition.setVisibility(View.GONE);
                        ivnodata.setVisibility(View.VISIBLE);


                    }
                    applicantNearPositionAdapter.notifyDataSetChanged();
                } else {
                    System.out.println("applicantNearPositionModel.getStatus() PROBLEM " + "");
                    rvApplicantNearPosition.setVisibility(View.GONE);
                    ivnodata.setVisibility(View.VISIBLE);
                }
            }

            @Override
            public void getVolleyError(VolleyError volleyError) {
                Log.d(TAG, "getApplicantNearPosionDataFromApi: Error:- " + volleyError.getMessage());
                progressDialog.dismiss();

            }
        }, context);
    }
}


/*
 * Copyright (c) 2017. Truiton (http://www.truiton.com/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 * Mohit Gupt (https://github.com/mohitgupt)
 *
 *//*


package com.hiremehealthcare.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.android.volley.NetworkResponse;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.hiremehealthcare.CommonCls.DialogUtils;
import com.hiremehealthcare.CommonCls.URLS;
import com.hiremehealthcare.CommonCls.Validations;
import com.hiremehealthcare.POJO.ApplicantNearPositionModel;
import com.hiremehealthcare.R;
import com.hiremehealthcare.Utils.CommonRequestHelper;
import com.hiremehealthcare.activity.ViewPositionDetailActivity;
import com.hiremehealthcare.adapter.ApplicantNearPositionAdapter;
import com.hiremehealthcare.adapter.ApplicationNearPos_ListAdapter;
import com.hiremehealthcare.database.MySharedPrefereces;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class ApplicantNearPostionFragment extends Fragment {

    RecyclerView rvApplicantNearPosition;
//    ListView rvApplicantNearPosition;
    public List<ApplicantNearPositionModel.Data> applicantNearPositionModelList;
    public ApplicantNearPositionAdapter applicantNearPositionAdapter;
//    public ApplicationNearPos_ListAdapter applicantNearPositionAdapter;
    MySharedPrefereces mySharedPrefereces;
    ProgressDialog progressDialog;
    public static ApplicantNearPostionFragment applicantNearPostionFragment;
    Context context;
    String TAG="ApplicantNearPostionFragment";
    public static ApplicantNearPostionFragment newInstance() {
        ApplicantNearPostionFragment fragment = new ApplicantNearPostionFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootView= inflater.inflate(R.layout.fragment_item_one, container, false);
        applicantNearPostionFragment=this;
        context=getContext();
        initAllControls(rootView);
        return rootView;
    }

    public void initAllControls(View rootView)
    {
        mySharedPrefereces = new MySharedPrefereces(context);
        progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        SpannableString spannableString =  new SpannableString(getString(R.string.please_wait));
        Typeface typefaceSpan = Validations.setTypeface(context);
        spannableString.setSpan(typefaceSpan, 0, getString(R.string.please_wait).length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        progressDialog.setMessage(spannableString);
        rvApplicantNearPosition = (RecyclerView) rootView.findViewById(R.id.rvApplicantNearPosition);
//        rvApplicantNearPosition = (ListView) rootView.findViewById(R.id.rvApplicantNearPosition);
        applicantNearPositionModelList = new ArrayList<>();
        applicantNearPositionAdapter = new ApplicantNearPositionAdapter(applicantNearPositionModelList,  false,context,new ApplicantNearPositionAdapter.OnRecyclerViewItemClicked() {
            @Override
            public void onRecyclerItemClicked(ApplicantNearPositionModel.Data data) {
                Intent intent=new Intent(context,ViewPositionDetailActivity.class);
                intent.putExtra("position_id", data.getPositionId());
                intent.putExtra("is_show_interest", true);
                startActivity(intent);
            }

            @Override
            public void onHeartButtonClicked(ApplicantNearPositionModel.Data data) {
                onRecyclerViewHeartButtonClicked(data);
            }

            @Override
            public void onAvailabilityButtonClicked(ApplicantNearPositionModel.Data data) {
                System.out.println("THIS IS AVAILABILITY CLICKED.......................................111111111111111 ");
            }
        });

//        applicantNearPositionAdapter=new ApplicationNearPos_ListAdapter(context,applicantNearPositionModelList,false);
      //  applicantNearPositionAdapter=new ApplicantNearPositionAdapter(context,applicantNearPositionModelList,false);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
        rvApplicantNearPosition.setLayoutManager(mLayoutManager);
        rvApplicantNearPosition.setItemAnimator(new DefaultItemAnimator());
        rvApplicantNearPosition.setAdapter(applicantNearPositionAdapter);


//        rvApplicantNearPosition.setAdapter(applicantNearPositionAdapter);
        getApplicantNearPosionDataFromApi();

    }

    */
/**
 * this method called when use clicked on heartButton from list in android
 * this method send interest on position api
 *
 * @param data
 * <p>
 * this method is used to getting applicant near position data from api
 * <p>
 * this method is used to getting applicant near position data from api
 * <p>
 * this method is used to getting applicant near position data from api
 *//*

    public void onRecyclerViewHeartButtonClicked(final ApplicantNearPositionModel.Data data)
    {
        progressDialog.show();
        String url= URLS.APPLICANT_SHOW_INTEREST_ON_POSITION+mySharedPrefereces.getUserId()+"&"+URLS.POSITION_ID+"="+data.getPositionId();
        Log.d(TAG, "onRecyclerViewHeartButtonClicked: URL"+url);
        CommonRequestHelper.getDataFromUrl(url, new CommonRequestHelper.VolleyResponseCustom() {

            @Override
            public void getServerResponse(String response) {
                progressDialog.dismiss();
                Log.d(TAG, "onRecyclerViewHeartButtonClicked: Response"+response);
                Gson gson = new Gson();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int status = jsonObject.getInt("status");
                    String message = jsonObject.getString("message");
                 //22-oct Pragna ppppppppppp   getApplicantNearPosionDataFromApi();
                    DialogUtils.showDialog4Activity(getActivity(), context.getResources().getString(R.string.app_name),message, null);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                */
/*ApplicantNearPositionModel applicantNearPositionModel = gson.fromJson(response, ApplicantNearPositionModel.class);
                if(applicantNearPositionModel.getStatus()==1)
                {
                    if(applicantNearPositionModel.getData().size()==0)
                        Toast.makeText(context,"No data found",Toast.LENGTH_LONG).show();
                    applicantNearPositionModelList.addAll(applicantNearPositionModel.getData());
                    applicantNearPositionAdapter.notifyDataSetChanged();
                }
                else
                {

                }*//*

            }

            @Override
            public void getVolleyError(VolleyError volleyError) {
                Log.d(TAG, "onRecyclerViewHeartButtonClicked: Error"+volleyError.getMessage());
                progressDialog.dismiss();
                NetworkResponse networkResponse = volleyError.networkResponse;
                DialogUtils.noInterNetDialog(context, "No Internet Connection", "No active internet connection available", new DialogUtils.NoInterNetDailogClickListner() {
                    @Override
                    public void onDialogOkButtonClicked() {
                        onRecyclerViewHeartButtonClicked(data);
                    }

                    @Override
                    public void onDialogCancelButtonClicked() {

                    }
                });

            }
        },context);


    }


    */
/**
 * this method is used to getting applicant near position data from api
 *//*


    public void getApplicantNearPosionDataFromApi()
    {
        applicantNearPositionModelList.clear();
        progressDialog.show();
        //http://hiremehealthcare.iipldemo.com/app/api.php?type=positions_near_you&applicant_id=21&applicant_zipcode=28278
        String url= URLS.APPLICANT_NEAR_POSITION+mySharedPrefereces.getUserId()+"&"+URLS.APPLICANT_ZIP_CODE+"="+mySharedPrefereces.getZipCode();
        //String url= URLS.APPLICANT_NEAR_POSITION+mySharedPrefereces.getUserId()+"&"+URLS.APPLICANT_ZIP_CODE+"=28207";//+mySharedPrefereces.getZipCode();
        Log.d(TAG, "getApplicantNearPosionDataFromApi: URL:- "+url);
        CommonRequestHelper.getDataFromUrl(url, new CommonRequestHelper.VolleyResponseCustom() {

            @Override
            public void getServerResponse(String response) {
                progressDialog.dismiss();
                Log.d(TAG, "getApplicantNearPosionDataFromApi: Response:- "+response);
                Gson gson = new Gson();
                ApplicantNearPositionModel applicantNearPositionModel = gson.fromJson(response, ApplicantNearPositionModel.class);
                if(applicantNearPositionModel.getStatus()==1)
                {
                    //if(applicantNearPositionModel.getData().size()==0)
                        //Toast.makeText(context,"No data found",Toast.LENGTH_LONG).show();
                    if(applicantNearPositionModel.getData().size()>0) {
                        applicantNearPositionModelList.addAll(applicantNearPositionModel.getData());
                    }
                    else
                    {
                        applicantNearPositionAdapter.showDataEmptyDate();
                    }
                    applicantNearPositionAdapter.notifyDataSetChanged();
                }
                else
                {

                }
            }

            @Override
            public void getVolleyError(VolleyError volleyError) {
                Log.d(TAG, "getApplicantNearPosionDataFromApi: Error:- "+volleyError.getMessage());
                progressDialog.dismiss();

            }
        },context);
    }
}
*/
