package com.hiremehealthcare.fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.Spanned;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.hiremehealthcare.CommonCls.CustomTextView;
import com.hiremehealthcare.CommonCls.DialogUtils;
import com.hiremehealthcare.CommonCls.ExpandableHeightGridView;
import com.hiremehealthcare.CommonCls.URLS;
import com.hiremehealthcare.CommonCls.Validations;
import com.hiremehealthcare.POJO.Benefits;
import com.hiremehealthcare.POJO.PandingPositionModel;
import com.hiremehealthcare.adapter.DaysMultiSelectAdapter_duplicate_flip;
import com.hiremehealthcare.adapter.Days_CheckBoxAdapter;
import com.hiremehealthcare.adapter.Days_adapter;
import com.hiremehealthcare.adapter.Evening_CheckBoxAdapter;
import com.hiremehealthcare.adapter.Evening_adapter;
import com.hiremehealthcare.adapter.ExpirePositionAdapter;
import com.hiremehealthcare.POJO.ExpirePositionModel;
import com.hiremehealthcare.POJO.Hours;
import com.hiremehealthcare.POJO.NearPositionDetailModel;
import com.hiremehealthcare.POJO.Shift;
import com.hiremehealthcare.R;
import com.hiremehealthcare.Utils.CommonRequestHelper;
import com.hiremehealthcare.activity.EditExpiredPositionActivity;
import com.hiremehealthcare.adapter.Night_CheckBoxAdapter;
import com.hiremehealthcare.adapter.Night_adapter;
import com.hiremehealthcare.database.MySharedPrefereces;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

/**
 * A simple {@link Fragment} subclass.
 */
public class ExpiredPositionFragment extends Fragment {

    RecyclerView rvExpirePositionList;
    List<ExpirePositionModel.Datum> expirePositionModelList;
    ExpirePositionAdapter expirePositionAdapter;
    public static Hours hours;
    public static Shift shifts;
    List<Hours.HoursBean> hoursBeanList;
    List<Shift.ShiftBean> shiftBeanList;
    RequestQueue queue;
    ProgressDialog progressDialog;
    MySharedPrefereces mySharedPrefereces;
    Context context;
    ImageView ivNoDataFound;
    String TAG = "ExpiredPositionFragment";
    Days_CheckBoxAdapter       _daDays_checkBoxAdapter;
    Night_CheckBoxAdapter _daNightcheckBoxAdapter;
    Evening_CheckBoxAdapter _daevening_checkBoxAdapter;
    private ExpandableHeightGridView griddaystime;
    ExpandableHeightGridView griddays, grideveningdaystime, gridnightdaystime;

    public ExpiredPositionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_expired_position, container, false);
        context = getContext();
        initAllControls(rootView);
        return rootView;
    }

    /**
     * Init Allcontrols and Object
     *
     * @param rootView parent view
     */
    public void initAllControls(View rootView) {
        hoursBeanList = new ArrayList<>();
        shiftBeanList = new ArrayList<>();
        mySharedPrefereces = new MySharedPrefereces(context);
        progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        SpannableString spannableString = new SpannableString(getString(R.string.please_wait));
        Typeface typefaceSpan = Validations.setTypeface(context);
        spannableString.setSpan(typefaceSpan, 0, getString(R.string.please_wait).length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        progressDialog.setMessage(spannableString);
        queue = Volley.newRequestQueue(context);
        rvExpirePositionList = (RecyclerView) rootView.findViewById(R.id.rvExpirePositionList);
        ivNoDataFound = (ImageView) rootView.findViewById(R.id.ivNoDataFound);
        expirePositionModelList = new ArrayList<>();
        expirePositionAdapter = new ExpirePositionAdapter(expirePositionModelList, context, shiftBeanList, hoursBeanList, new ExpirePositionAdapter.OnEditOrViewButtonListener() {
            @Override
            public void onEditButtonClicked(ExpirePositionModel.Datum data) {
                Intent i = new Intent(context, EditExpiredPositionActivity.class);
                i.putExtra("data", data);
                startActivityForResult(i, 1);
            }

            @Override
            public void onViewButtonClicked(ExpirePositionModel.Datum data) {
                getPositionDetail(data.getPositionId());
            }
        });
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
        rvExpirePositionList.setLayoutManager(mLayoutManager);
        rvExpirePositionList.setItemAnimator(new DefaultItemAnimator());
        rvExpirePositionList.setAdapter(expirePositionAdapter);
        getHours();
        //getPositiveDataFromApi();

    }

    public void getPositionDetail(final String position_id) {


        progressDialog.show();
        String url = URLS.APPLICANT_NEAR_POSITION_DETAIL + position_id;//data.getPositionId();
        //Log.d(TAG, "getApplicantNeatPosition: URL:" + url);\
        Log.d(TAG, "getPositionDetail: Url " + url);
        CommonRequestHelper.getDataFromUrl(url, new CommonRequestHelper.VolleyResponseCustom() {

            @Override
            public void getServerResponse(String response) {
                progressDialog.dismiss();
                Log.d(TAG, "getPositionDetail: response " + response);

                Gson gson = new Gson();


                NearPositionDetailModel nearPositionDetailModel;
                nearPositionDetailModel = gson.fromJson(response, NearPositionDetailModel.class);
                if (nearPositionDetailModel.getStatus() == 1) {
                    showDetailDailog(nearPositionDetailModel);
                   /* if(nearPositionDetailModel.getData().size()==0)
                        Toast.makeText(context,"No data found",Toast.LENGTH_LONG).show();*/
                    // applicantNearPositionModelList.addAll(applicantNearPositionDetailModel.getData());
                    // applicantNearPositionAdapter.notifyDataSetChanged();
                } else {

                }
                /*ApplicantNearPositionModel applicantN earPositionDetailModel = gson.fromJson(response, ApplicantNearPositionModel.class);
                if(applicantNearPositionDetailModel.getStatus()==1)
                {
                    if(applicantNearPositionDetailModel.getData().size()==0)
                        Toast.makeText(context,"No data found",Toast.LENGTH_LONG).show();
                    applicantNearPositionModelList.addAll(applicantNearPositionDetailModel.getData());
                    applicantNearPositionAdapter.notifyDataSetChanged();
                }
                else
                {

                }*/
            }

            @Override
            public void getVolleyError(VolleyError volleyError) {
                progressDialog.dismiss();
                Log.d(TAG, "getPositionDetail: message " + volleyError.getMessage());
                NetworkResponse networkResponse = volleyError.networkResponse;
                DialogUtils.noInterNetDialog(context, "No Internet Connection", "No active internet connection available", new DialogUtils.NoInterNetDailogClickListner() {
                    @Override
                    public void onDialogOkButtonClicked() {
                        getPositionDetail(position_id);
                    }

                    @Override
                    public void onDialogCancelButtonClicked() {

                    }
                });

            }
        }, context);

    }
    ArrayList<String> days = new ArrayList<>();
  //  ExpandableHeightGridView griddays,grideveningdaystime,gridnightdaystime;

    public void showDetailDailog(NearPositionDetailModel nearPositionDetailModel) {
        LayoutInflater inflater = LayoutInflater.from(context);




//        final View dialogView = inflater.inflate(R.layout.active_position_dialog_layout, null);
        final View dialogView = inflater.inflate(R.layout.active_position_dialog_layout, null);
        CustomTextView tvdays = (CustomTextView) dialogView.findViewById(R.id.tvdays);

        griddays = (ExpandableHeightGridView) dialogView.findViewById(R.id.grid_days);
        gridnightdaystime = (ExpandableHeightGridView) dialogView.findViewById(R.id.grid_night_days_time);
        grideveningdaystime = (ExpandableHeightGridView) dialogView.findViewById(R.id.grid_evening_days_time);
        griddaystime = (ExpandableHeightGridView) dialogView.findViewById(R.id.grid_days_time);
        griddaystime.setExpanded(true);


        gridnightdaystime.setExpanded(true);
        griddays.setExpanded(true);
        grideveningdaystime.setExpanded(true);
        ImageView ivCloseDialog = (ImageView) dialogView.findViewById(R.id.ivCloseDialog);
        CustomTextView tvFacilitySpeciality = (CustomTextView) dialogView.findViewById(R.id.tvFacilitySpeciality);
        CustomTextView tvFacilityLicense = (CustomTextView) dialogView.findViewById(R.id.tvFacilityLicense);
        CustomTextView tvFacilityCertificate = (CustomTextView) dialogView.findViewById(R.id.tvFacilityCertificate);
        CustomTextView tvFacilityShift = (CustomTextView) dialogView.findViewById(R.id.tvFacilityShift);
        CustomTextView tvFacilityHours = (CustomTextView) dialogView.findViewById(R.id.tvFacilityHours);
        CustomTextView tvFacilityExperienceRequired = (CustomTextView) dialogView.findViewById(R.id.tvFacilityExperienceRequired);
        CustomTextView tvFacilityBenefits = (CustomTextView) dialogView.findViewById(R.id.tvFacilityBenefits);
        CustomTextView tvFacilityDescription = (CustomTextView) dialogView.findViewById(R.id.tvFacilityDescription);
//        tvFacilitySpeciality.setText(nearPositionDetailModel.getData().getSpecialtyName() + "");
//        tvFacilityLicense.setText(nearPositionDetailModel.getData().getLicenseName() + "");
//        tvFacilityCertificate.setText(nearPositionDetailModel.getData().getCertificateName() + "");


        String s=       nearPositionDetailModel.getData().getSpecialtyName() + "";
        s=s.replace(",",", ");

        String s1=       nearPositionDetailModel.getData().getLicenseName() + "";
        s1=s1.replace(",",", ");


        String s2=       nearPositionDetailModel.getData().getCertificateName() + "";
        s2=s2.replace(",",", ");

        tvFacilitySpeciality.setText(s + "");
        tvFacilityLicense.setText(s1 + "");
        tvFacilityCertificate.setText(s2 + "");



        tvFacilityShift.setText(nearPositionDetailModel.getData().getShiftName() + "");
        tvFacilityHours.setText(nearPositionDetailModel.getData().getHourName() + "");
//        tvFacilityExperienceRequired.setText(nearPositionDetailModel.getData().getYear() + " Year And " + nearPositionDetailModel.getData().getMonth() + " Months");


        DaysMultiSelectAdapter_duplicate_flip _dayDaysMultiSelectAdapter;
        _dayDaysMultiSelectAdapter = new DaysMultiSelectAdapter_duplicate_flip(days, context);
        griddays.setExpanded(true);
        griddays.setAdapter(_dayDaysMultiSelectAdapter);
        Benefits days_all = new Benefits();
        List<Benefits.BenefitsBean> all_day = new ArrayList<>();
        Benefits.BenefitsBean day = new Benefits.BenefitsBean();
availability(nearPositionDetailModel);



        /*25 july Pragna*/
//        tvFacilityExperienceRequired.setText(nearPositionDetailModel.getData().getYear() + " Year And " + nearPositionDetailModel.getData().getMonth() + " Months");
        String y = "";
        String m = "";
        if (nearPositionDetailModel.getData().getYear() != null) {
            if (nearPositionDetailModel.getData().getYear().contentEquals("0") || nearPositionDetailModel.getData().getYear().contentEquals("1")) {
                y = " year ";
            } else {
                y = " years ";
            }
        }
        if (nearPositionDetailModel.getData().getMonth() != null) {
            System.out.println("THIS IS TEST TTTTT " + nearPositionDetailModel.getData().getMonth() + "");
            if (nearPositionDetailModel.getData().getMonth().contentEquals("0") || nearPositionDetailModel.getData().getMonth().contentEquals("1")) {
                m = " month ";
            } else {
                m = " months ";
            }

        }
        tvFacilityExperienceRequired.setText(nearPositionDetailModel.getData().getYear() + "" + y + nearPositionDetailModel.getData().getMonth() + "" + m);
        String temp = nearPositionDetailModel.getData().getBenefitsName() + "";temp=temp.replace(",", ", ");
        tvFacilityBenefits.setText(temp);
        tvFacilityDescription.setText(nearPositionDetailModel.getData().getPositionDescription() + "");

        final AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(context, R.style.myDialog));
        final AlertDialog b = builder.create();
        //  builder.setTitle("Material Style Dialog");
        builder.setCancelable(true);
        builder.setView(dialogView);
        b.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        final AlertDialog show = builder.show();
        ivCloseDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                show.dismiss();
            }
        });

    }

    /**
     * this method is use to getting hour data
     */

    private void getHours() {
        progressDialog.show();
        String Url = URLS.HOURS_URL;
        Log.d(TAG, "getHours: URL:-" + Url);
        StringRequest req = new StringRequest(Request.Method.GET, Url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                Log.d(TAG, "getHours: Response:-" + response);
                Gson gson = new Gson();

                hours = gson.fromJson(response, Hours.class);

                if (hours != null) {
                    hoursBeanList.addAll(hours.getHours());
                    getShift();

                } else {
                    // DialogUtils.showDialog4Activity(Login.this, ctx.getResources().getString(R.string.app_name), logindata.getMessage() + "");

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Log.d(TAG, "getHours: Errro:-" + error.getMessage());
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    int statusCode = error.networkResponse.statusCode;
                    System.out.println("STATUS CODE " + statusCode + "");
                    DialogUtils.showDialog4Activity(getActivity(), context.getResources().getString(R.string.app_name), statusCode + "", null);
                }

            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(req);

    }

    private void getShift() {
        progressDialog.show();
        String Url = URLS.SHIFT_URL;
        Log.d(TAG, "getShift: URL:- " + Url);
        StringRequest req = new StringRequest(Request.Method.GET, Url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                Log.d(TAG, "getShift: Response:- " + response);
                Gson gson = new Gson();
                shifts = gson.fromJson(response, Shift.class);

                if (shifts != null) {
                    shiftBeanList.addAll(shifts.getShift());
                    getExpiredDataFromApi();
                } else {
                    // DialogUtils.showDialog4Activity(Login.this, ctx.getResources().getString(R.string.app_name), logindata.getMessage() + "");

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    Log.d(TAG, "getShift: Error:- " + error.getMessage());
                    int statusCode = error.networkResponse.statusCode;
                    System.out.println("STATUS CODE " + statusCode + "");
                    DialogUtils.showDialog4Activity(context, context.getResources().getString(R.string.app_name), statusCode + "", null);
                }

            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(req);

    }

    /**
     * this method use to getting Expire data from api
     */

    public void getExpiredDataFromApi() {
        String url = URLS.MY_POSITION_EXPIRE + mySharedPrefereces.getUserId();
        Log.d(TAG, "getExpiredDataFromApi: URL:-" + url);
        CommonRequestHelper.getDataFromUrl(url, new CommonRequestHelper.VolleyResponseCustom() {
            @Override
            public void getServerResponse(String response) {
                Log.d(TAG, "getExpiredDataFromApi: Response:-" + response);
                Gson gson = new Gson();
                ExpirePositionModel expirePositionModel = gson.fromJson(response + "", ExpirePositionModel.class);
                if (expirePositionModel != null) {
                    if (expirePositionModel.getData().size() > 0) {
                        expirePositionModelList.clear();
                        expirePositionModelList.addAll(expirePositionModel.getData());
                        expirePositionAdapter.notifyDataSetChanged();
                        ivNoDataFound.setVisibility(View.GONE);
                    } else {
                        ivNoDataFound.setVisibility(View.VISIBLE);
                    }
                    //activePostionDataList.addAll(activePositionModel.getData());
                    //activePositionAdapter.notifyDataSetChanged();
                }

            }

            @Override
            public void getVolleyError(VolleyError volleyError) {
                Log.d(TAG, "getExpiredDataFromApi: Error:-" + volleyError.getMessage());

            }
        }, context);
        /*for (int i = 0; i < 30; i++) {
            dashbordManagerModelList.add(new DashbordManagerModel());
        }
        activePositionAdapter.notifyDataSetChanged();*/
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (requestCode == 1) {
            getExpiredDataFromApi();
        }
    }
    private void availability( NearPositionDetailModel datamodel) {
        ArrayList<String> days = new ArrayList<>();
        NearPositionDetailModel.Data data =datamodel.getData();
        //   getavailability(data.getPositionId() + "");
        days = new ArrayList<>();
        days.add("Mon");
        Days_adapter.selected=new ArrayList<>();
        Days_adapter.selected_name=new ArrayList<>();

        Night_adapter.selected=new ArrayList<>();
        Night_adapter.selected_name=new ArrayList<>();


        Evening_adapter.selected=new ArrayList<>();
        Evening_adapter.selected_name=new ArrayList<>();


        days.add("Tue");
        days.add("Wed");
        days.add("Thu");
        days.add("Fri");
        days.add("Sat");
        days.add("Sun");
        System.out.println("availability MON : " + data.getMon() + " TUE : " + data.getTue() + " WED " + data.getWed() + " THR " + data.getThu() + " FRI " + data.getFri() + "");
        //   List<String> items = Arrays.asList(data.getMon().split("\\s*,\\s*"));

//PostNewPostion.MON.addAll(Arrays.asList(data.getMon().toLowerCase().split("\\s+")));
        List<String> items = new ArrayList<>();
//                    items.addAll(Arrays.asList(data.getMon().toLowerCase().split("\\s+")));
        items=    Arrays.asList(data.getMon().split("\\s*,\\s*"));
        //  PostNewPostion.MON = new ArrayList<Integer>(items.size());
        PostNewPostion.MON = new ArrayList<>();
        List<Integer> items_int = new ArrayList<>();
        for (int i = 0; i < items.size(); i++) {
            if(  isNumeric(items.get(i) + "")) {
                items_int.add(Integer.parseInt(items.get(i) + ""));
                if (items_int.get(i).equals(1)) {
                    Days_adapter.selected.add("1");
                    Days_adapter.selected_name.add("Mon");
                }
                if (items_int.get(i).equals(2)) {
                    Evening_adapter.selected.add("1");
                    Evening_adapter.selected_name.add("Mon");
                }
                if (items_int.get(i).equals(3)) {
                    Night_adapter.selected.add("1");
                    Night_adapter.selected_name.add("Mon");
                }
            }
        }
        //  List<String> items_tue = new ArrayList<>();
//                    items_tue=(Arrays.asList(data.getTue().split("\\s+")));
        List<String> items_tue = Arrays.asList(data.getTue().split("\\s*,\\s*"));

        //  PostNewPostion.MON = new ArrayList<Integer>(items.size());
        PostNewPostion.TUE = new ArrayList<>();
        List<Integer> items_int_tue = new ArrayList<>();
        //    System.out.println("TUE ```````````@ "+items_tue.toString()+"");
        for (int i = 0; i < items_tue.size(); i++) {
            System.out.println("items_tue.get(i) ````@@@@@@@@@@ "+ items_tue.get(i) + "");
            if(  isNumeric(items_tue.get(i) + "")) {
                items_int_tue.add(Integer.parseInt(items_tue.get(i) + ""));
                if (items_int_tue.get(i).equals(1)) {
                    Days_adapter.selected.add("2");
                    Days_adapter.selected_name.add("Tue");
                }
                if (items_int_tue.get(i).equals(2)) {
                    Evening_adapter.selected.add("2");
                    Evening_adapter.selected_name.add("Tue");
                }
                if (items_int_tue.get(i).equals(3)) {
                    Night_adapter.selected.add("2");
                    Night_adapter.selected_name.add("Tue");
                }
            }
        }


        List<String> items_wed = Arrays.asList(data.getWed().split("\\s*,\\s*"));

        //  PostNewPostion.MON = new ArrayList<Integer>(items.size());
        PostNewPostion.WED = new ArrayList<>();
        List<Integer> items_int_wed = new ArrayList<>();
        //   System.out.println("WED ```````````@ "+items_wed.toString()+"");
        for (int i = 0; i < items_wed.size(); i++) {
//                        System.out.println("items_tue.get(i) ````@@@@@@@@@@ "+ items_tue.get(i) + "");
            if(  isNumeric(items_wed.get(i) + "")) {
                items_int_wed.add(Integer.parseInt(items_wed.get(i) + ""));
                if (items_int_wed.get(i).equals(1)) {
                    Days_adapter.selected.add("3");
                    Days_adapter.selected_name.add("Wed");
                }
                if (items_int_wed.get(i).equals(2)) {
                    Evening_adapter.selected.add("3");
                    Evening_adapter.selected_name.add("Wed");
                }
                if (items_int_wed.get(i).equals(3)) {
                    Night_adapter.selected.add("3");
                    Night_adapter.selected_name.add("Wed");
                }
            }
        }


        List<String> items_Thr = Arrays.asList(data.getThu().split("\\s*,\\s*"));

        //  PostNewPostion.MON = new ArrayList<Integer>(items.size());
        PostNewPostion.THR = new ArrayList<>();
        List<Integer> items_int_thr = new ArrayList<>();
        //  System.out.println("THRU ```````````@ "+items_Thr.toString()+"");
        for (int i = 0; i < items_Thr.size(); i++) {
//                        System.out.println("items_tue.get(i) ````@@@@@@@@@@ "+ items_tue.get(i) + "");
            if(  isNumeric(items_Thr.get(i) + "")) {
                items_int_thr.add(Integer.parseInt(items_Thr.get(i) + ""));
                if (items_int_thr.get(i).equals(1)) {
                    Days_adapter.selected.add("4");
                    Days_adapter.selected_name.add("Thr");
                }
                if (items_int_thr.get(i).equals(2)) {
                    Evening_adapter.selected.add("4");
                    Evening_adapter.selected_name.add("Thr");
                }
                if (items_int_thr.get(i).equals(3)) {
                    Night_adapter.selected.add("4");
                    Night_adapter.selected_name.add("Thr");
                }
            }
        }


        List<String> items_Fri = Arrays.asList(data.getFri().split("\\s*,\\s*"));

        //  PostNewPostion.MON = new ArrayList<Integer>(items.size());
        PostNewPostion.FRI = new ArrayList<>();
        List<Integer> items_int_fri = new ArrayList<>();
        //   System.out.println("FRID ```````````@ "+items_Fri.toString()+"");
        for (int i = 0; i < items_Fri.size(); i++) {
//                        System.out.println("items_tue.get(i) ````@@@@@@@@@@ "+ items_tue.get(i) + "");
            if(  isNumeric(items_Fri.get(i) + "")) {
                items_int_fri.add(Integer.parseInt(items_Fri.get(i) + ""));
                if (items_int_fri.get(i).equals(1)) {
                    Days_adapter.selected.add("5");
                    Days_adapter.selected_name.add("Fri");
                }
                if (items_int_fri.get(i).equals(2)) {
                    Evening_adapter.selected.add("5");
                    Evening_adapter.selected_name.add("Fri");
                }
                if (items_int_fri.get(i).equals(3)) {
                    Night_adapter.selected.add("5");
                    Night_adapter.selected_name.add("Fri");
                }
            }
        }


        List<String> items_Sat = Arrays.asList(data.getSat().split("\\s*,\\s*"));

        //  PostNewPostion.MON = new ArrayList<Integer>(items.size());
        PostNewPostion.FRI = new ArrayList<>();
        List<Integer> items_int_sat = new ArrayList<>();
        //   System.out.println("SAT ```````````@ "+items_Sat.toString()+"");
        for (int i = 0; i < items_Sat.size(); i++) {
//                        System.out.println("items_tue.get(i) ````@@@@@@@@@@ "+ items_tue.get(i) + "");
            if(  isNumeric(items_Sat.get(i) + "")) {
                items_int_sat.add(Integer.parseInt(items_Sat.get(i) + ""));
                if (items_int_sat.get(i).equals(1)) {
                    Days_adapter.selected.add("6");
                    Days_adapter.selected_name.add("Sat");
                }
                if (items_int_sat.get(i).equals(2)) {
                    Evening_adapter.selected.add("6");
                    Evening_adapter.selected_name.add("Sat");
                }
                if (items_int_sat.get(i).equals(3)) {
                    Night_adapter.selected.add("6");
                    Night_adapter.selected_name.add("Sat");
                }
            }
        }


        List<String> items_Sun = Arrays.asList(data.getSun().split("\\s*,\\s*"));

        //  PostNewPostion.MON = new ArrayList<Integer>(items.size());
        PostNewPostion.FRI = new ArrayList<>();
        List<Integer> items_int_sun = new ArrayList<>();
        //   System.out.println("SUN ```````````@ "+items_Sun.toString()+"");
        for (int i = 0; i < items_Sun.size(); i++) {
//                        System.out.println("items_tue.get(i) ````@@@@@@@@@@ "+ items_tue.get(i) + "");
            if(  isNumeric(items_Sun.get(i) + "")) {
                items_int_sun.add(Integer.parseInt(items_Sun.get(i) + ""));
                if (items_int_sun.get(i).equals(1)) {
                    Days_adapter.selected.add("7");
                    Days_adapter.selected_name.add("Sun");
                }
                if (items_int_sun.get(i).equals(2)) {
                    Evening_adapter.selected.add("7");
                    Evening_adapter.selected_name.add("Sun");
                }
                if (items_int_sun.get(i).equals(3)) {
                    Night_adapter.selected.add("7");
                    Night_adapter.selected_name.add("Sun");
                }
            }
        }



        PostNewPostion.MON.addAll(items_int);
        PostNewPostion.TUE.addAll(items_int_tue);
        PostNewPostion.WED.addAll(items_int_wed);
        PostNewPostion.THR.addAll(items_int_thr);
        PostNewPostion.FRI.addAll(items_int_fri);
        PostNewPostion.SAT.addAll(items_int_sat);
        PostNewPostion.SUN.addAll(items_int_sun);

//        System.out.println("this is monday " + PostNewPostion.MON.toString() + "");
//        System.out.println("this is Tuesday " + PostNewPostion.TUE.toString() + "");
//        System.out.println("this is TuesdayDays_adapter.selected_name``````````@ " + Days_adapter.selected_name .toString()+ "");
//        System.out.println("this is TuesdayEvening_adapter.selected_name``````````@ " + Evening_adapter.selected_name.toString() + "");
        //    v_v = LayoutInflater.from(_parent.getContext()).inflate(R.layout.testforflip, _parent, false);
        //    v_v = LayoutInflater.from(_parent.getContext()).inflate(R.layout.testforflip_availability, _parent, false);
        //  holder.row_item_flipper.removeView(v_v);
        // AnimationFactory.flipTransition(holder.row_item_flipper,
        //  AnimationFactory.FlipDirection.LEFT_RIGHT);




        // holder.row_item_flipper.addView(v_v);
        // initView(v_v);
        DaysMultiSelectAdapter_duplicate_flip _dayDaysMultiSelectAdapter;
        _dayDaysMultiSelectAdapter = new DaysMultiSelectAdapter_duplicate_flip(days, context);
        griddays.setExpanded(true);
        griddays.setAdapter(_dayDaysMultiSelectAdapter);
        Benefits days_all = new Benefits();
        List<Benefits.BenefitsBean> all_day = new ArrayList<>();
        Benefits.BenefitsBean day = new Benefits.BenefitsBean();


        day.setBenefits_id("1");
        day.setBenefits_name("Mon");
        all_day.add(day);

        day = new Benefits.BenefitsBean();
        day.setBenefits_id("2");
        day.setBenefits_name("Tue");
        all_day.add(day);

        day = new Benefits.BenefitsBean();
        day.setBenefits_id("3");
        day.setBenefits_name("Wed");
        all_day.add(day);


        day = new Benefits.BenefitsBean();
        day.setBenefits_id("4");
        day.setBenefits_name("Thu");
        all_day.add(day);


        day = new Benefits.BenefitsBean();
        day.setBenefits_id("5");
        day.setBenefits_name("Fri");
        all_day.add(day);


        day = new Benefits.BenefitsBean();
        day.setBenefits_id("6");
        day.setBenefits_name("Sat");
        all_day.add(day);


        day = new Benefits.BenefitsBean();
        day.setBenefits_id("7");
        day.setBenefits_name("Sun");
        all_day.add(day);


        days_all.setBenefits(all_day);
        Benefits test = new Benefits();
/*
                           _daDays_checkBoxAdapter = new Days_CheckBoxAdapter(days_all, context, 1,test,true);
                    griddaystime.setAdapter(_daDays_checkBoxAdapter);*/


        Days_adapter  _daDays_checkBoxAdapter = new Days_adapter(days_all, context, 1, test, true);
        griddaystime.setAdapter(_daDays_checkBoxAdapter);

        Evening_adapter   _daevening_checkBoxAdapter = new Evening_adapter(days_all, context, 2, true);
        grideveningdaystime.setAdapter(_daevening_checkBoxAdapter);


        Night_adapter   _daNightcheckBoxAdapter = new Night_adapter(days_all, context, 3, true);
        gridnightdaystime.setAdapter(_daNightcheckBoxAdapter);
    }
    private boolean isNumeric(String string) {
        if(Pattern.matches("\\d{1,3}((\\.\\d{1,2})?)", string))
            return true;
        else
            return false;
    }
}
