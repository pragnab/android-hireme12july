package com.hiremehealthcare.fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.SpannableString;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.hiremehealthcare.CommonCls.DialogUtils;
import com.hiremehealthcare.CommonCls.URLS;
import com.hiremehealthcare.CommonCls.Validations;
import com.hiremehealthcare.POJO.CommunicationModel;
import com.hiremehealthcare.activity.ContactCandidate;
import com.hiremehealthcare.R;
import com.hiremehealthcare.Utils.CommonRequestHelper;
import com.hiremehealthcare.activity.DashBoardActivity;
import com.hiremehealthcare.activity.DashBoardManagerActivity;
import com.hiremehealthcare.activity.ManagerChatDetailActivity;
import com.hiremehealthcare.activity.ViewPositionDetailActivity;
import com.hiremehealthcare.adapter.CommunicationAdapter;
import com.hiremehealthcare.database.MySharedPrefereces;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 * this class is use to chat user list
 */
public class CommunicationFragment extends Fragment {


    ListView ivChatManager;
    List<CommunicationModel.Datum> communicationModelList;
    CommunicationAdapter communicationAdapter;
    MySharedPrefereces mySharedPrefereces;
    Context context;
    String TAG = "CommunicationFragment";
    ProgressDialog progressDialog;
    public static CommunicationFragment communicationFragment;
TextView iv_nodata;
    public CommunicationFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_chat_manager, container, false);
        initAllControls(rootView);
        return rootView;
    }

    public void initAllControls(View rootView) {
        System.out.println("hellooo communication ::::::::::::: ");
        communicationFragment = this;
        context = getContext();
        progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        SpannableString spannableString = new SpannableString(getString(R.string.please_wait));
        Typeface typefaceSpan = Validations.setTypeface(context);
        spannableString.setSpan(typefaceSpan, 0, getString(R.string.please_wait).length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        progressDialog.setMessage(spannableString);
        ivChatManager = (ListView) rootView.findViewById(R.id.ivChatManager);
        iv_nodata = (TextView) rootView.findViewById(R.id.iv_nodata);
        communicationModelList = new ArrayList<>();
        mySharedPrefereces = new MySharedPrefereces(context);
        communicationAdapter = new CommunicationAdapter(getContext(), communicationModelList, new CommunicationAdapter.OnPandingAdapterRemoveOrFavoriteButtonClicked() {
            @Override
            public void onContactCandidateButtonClicked(CommunicationModel.Datum data) {
                contactCandidateButtonClicked(data);
            }

            @Override
            public void onRemoveButtonClicked(CommunicationModel.Datum data) {
                onRemoveChatButtonClicked(data);
            }

            @Override
            public void onListItemClicked(CommunicationModel.Datum data) {

                Intent intent = new Intent(getContext(), ManagerChatDetailActivity.class);
                intent.putExtra("data", data);
                startActivityForResult(intent, 105);

            }
        });
        ivChatManager.setAdapter(communicationAdapter);
        getCommunicationData();

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == 105) {
            getCommunicationData();
            if (mySharedPrefereces.getUserType() == 2) {
                DashBoardManagerActivity.dashBoardManagerActivity.getNumberOfMessageSpecificUser();
            } else {
                DashBoardActivity.dashBoardActivity.getNumberOfMessageSpecificUser();
            }
        }
    }

    public void contactCandidateButtonClicked(CommunicationModel.Datum data) {
        if (data != null) {
            if (mySharedPrefereces.getUserType() == 1) {
                Intent intent = new Intent(context, ViewPositionDetailActivity.class);
                intent.putExtra("is_show_interest", false);
                intent.putExtra("position_id", data.getPositionId() + "");
                startActivity(intent);
            } else {
                Intent i = new Intent(context,
                        ContactCandidate.class);
                i.putExtra("ID", data.getApplicantId() + "");
                i.putExtra("IMAGE", data.getImage() + "");
                startActivity(i);
            }

        }
    }

    /**
     * this method is used to getting list of communication
     */

    public void getCommunicationData() {
        //DialogUtils.showProgressDialog(context, "Please wait....");
        progressDialog.show();
        communicationModelList.clear();
        String Url = URLS.COMMUNICATION + "&" + URLS.USER_ID + "=" + mySharedPrefereces.getUserId() + "&" + URLS.USER_TYPE + "=" + mySharedPrefereces.getUserType();
        Log.d(TAG, "getCommunicationData:URL:- " + Url);
        CommonRequestHelper.getDataFromUrl(Url, new CommonRequestHelper.VolleyResponseCustom() {
            @Override
            public void getServerResponse(String response) {
                Log.d(TAG, "getCommunicationData:Response:- " + response);
                progressDialog.dismiss();
                //DialogUtils.hideProgressDialog();
                Gson gson = new Gson();

                CommunicationModel communicationModel = gson.fromJson(response, CommunicationModel.class);
                if (communicationModel != null) {
                    if(communicationModel.getData()!=null&&communicationModel.getData().size()>0) {
                        iv_nodata.setVisibility(View.GONE);
                        communicationModelList.addAll(communicationModel.getData());
                        communicationAdapter.notifyDataSetChanged();
                    }
                    else{
                        iv_nodata.setVisibility(View.VISIBLE);
                    }
                } else {

                }

            }

            @Override
            public void getVolleyError(VolleyError volleyError) {
                Log.d(TAG, "getCommunicationData:Error:- " + volleyError.getMessage());
                progressDialog.dismiss();
                //DialogUtils.hideProgressDialog();
            }
        }, context);

        /*ॐ मृत्युंजय महादेव त्राहिमां शरणागतम
जन्म मृत्यु जरा व्याधि पीड़ितं कर्म बंधनः   for(int counter=0;counter<10;counter++)
        {
            communicationModelList.add(new ChatManagerModel());

        }
        chatManagerAdapter.notifyDataSetChanged();*/
    }

    public void onRemoveChatButtonClicked(CommunicationModel.Datum data) {
        //Toast.makeText(getContext(), "Remove Button Clicked", Toast.LENGTH_SHORT).show();
        removeCommunicationData(data);
    }

    public void removeCommunicationData(final CommunicationModel.Datum data) {
        progressDialog.show();
        String Url = URLS.REMOVE_COMMUNICATION + data.getId();
        Log.d(TAG, "removeCommunicationData: Url " + Url);
        CommonRequestHelper.getDataFromUrl(Url, new CommonRequestHelper.VolleyResponseCustom() {
            @Override
            public void getServerResponse(String response) {
                progressDialog.dismiss();
                Log.d(TAG, "removeCommunicationData: Response " + response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String status = jsonObject.getString("status");
                    String message = jsonObject.getString("message");
                    if (status.equals("1")) {
                        DialogUtils.showDialog4Activity(getActivity(), context.getResources().getString(R.string.app_name), message + "", null);
                        getCommunicationData();
                    } else {
                        DialogUtils.showDialog4Activity(getActivity(), context.getResources().getString(R.string.app_name), message + "", null);
                    }
                } catch (Exception ex) {
                    Log.d(TAG, "json error " + ex.getMessage());
                }

            }

            @Override
            public void getVolleyError(VolleyError volleyError) {
                progressDialog.dismiss();
                Log.d(TAG, "removeCommunicationData: Error" + volleyError.getMessage());
                DialogUtils.noInterNetDialog(context, "No Internet Connection", "No active internet connection available", new DialogUtils.NoInterNetDailogClickListner() {
                    @Override
                    public void onDialogOkButtonClicked() {
                        removeCommunicationData(data);
                    }

                    @Override
                    public void onDialogCancelButtonClicked() {

                    }
                });


            }
        }, context);
    }

}
