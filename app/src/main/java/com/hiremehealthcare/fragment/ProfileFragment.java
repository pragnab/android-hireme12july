/*
 * Copyright (c) 2017. Truiton (http://www.truiton.com/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 * Mohit Gupt (https://github.com/mohitgupt)
 *
 */

package com.hiremehealthcare.fragment;
//https://stackoverflow.com/questions/41413150/fragment-tabs-inside-fragment/41656303

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hiremehealthcare.CommonCls.CustomTextView;
import com.hiremehealthcare.R;

import java.util.ArrayList;
import java.util.List;

public class ProfileFragment extends Fragment {
    TabLayout tabs;
   public static ViewPager viewPager;

    public static ProfileFragment newInstance() {
        ProfileFragment fragment = new ProfileFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_profile, container, false);
        viewPager = (ViewPager) view.findViewById(R.id.viewpager);
        viewPager.setOffscreenPageLimit(4);
        setupViewPager(viewPager);
        // Set Tabs inside Toolbar
        tabs = (TabLayout) view.findViewById(R.id.result_tabs);
        tabs.setupWithViewPager(viewPager);
        //createTabIcons();

        return view;

    }

    private void createTabIcons() {

        CustomTextView tabOne = (CustomTextView) LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab, null);
        tabOne.setText("1" + "\nPersonal");
        tabOne.setTextColor(getResources().getColor(R.color.colorBlack));
        //  tabOne.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_dash26, 0, 0);
        tabs.getTabAt(0).setCustomView(tabOne);

       /* CustomTextView tabTwo = (CustomTextView) LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab, null);
        tabTwo.setText("2" + "\n" + "Education");
        tabTwo.setTextColor(getResources().getColor(R.color.colorBlack));
        //   tabTwo.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_category, 0, 0);
        tabs.getTabAt(1).setCustomView(tabTwo);

        CustomTextView tabThree = (CustomTextView) LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab, null);
        tabThree.setText("3" + "\n" + "Experience");
        tabThree.setTextColor(getResources().getColor(R.color.colorBlack));
        //   tabTwo.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_category, 0, 0);
        tabs.getTabAt(2).setCustomView(tabThree);*/

       /*24 july Pragna*/
        CustomTextView tabTwo = (CustomTextView) LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab, null);
//        tabTwo.setText("2" + "\n" + "Education");
        tabTwo.setText("2" + "\n" + "Experience");
        tabTwo.setTextColor(getResources().getColor(R.color.colorBlack));
        //   tabTwo.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_category, 0, 0);
        tabs.getTabAt(1).setCustomView(tabTwo);

        CustomTextView tabThree = (CustomTextView) LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab, null);
//        tabThree.setText("3" + "\n" + "Experience");
        tabThree.setText("3" + "\n" + "Education");
        tabThree.setTextColor(getResources().getColor(R.color.colorBlack));
        //   tabTwo.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_category, 0, 0);
        tabs.getTabAt(2).setCustomView(tabThree);




        CustomTextView tabFour = (CustomTextView) LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab, null);
        tabFour.setText("4" + "\n" + "Availability");
        tabFour.setTextColor(getResources().getColor(R.color.colorBlack));
        //   tabTwo.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_category, 0, 0);
        tabs.getTabAt(3).setCustomView(tabFour);



        CustomTextView tabFive = (CustomTextView) LayoutInflater.from(getActivity()).inflate(R.layout.custom_tab, null);
        tabFour.setText("5" + "\n" + "  Submit  ");
        tabFour.setTextColor(getResources().getColor(R.color.colorBlack));
        //   tabTwo.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_category, 0, 0);
        tabs.getTabAt(4).setCustomView(tabFour);








//        TextView tabThree = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
//        tabThree.setText("Tab 3");
//        tabThree.setCompoundDrawablesWithIntrinsicBounds(0, R.drawable.ic_order, 0, 0);
//        tabLayout.getTabAt(2).setCustomView(tabThree);
    }

    // Add Fragments to Tabs
    private void setupViewPager(ViewPager viewPager) {


        Adapter adapter = new Adapter(getChildFragmentManager());

        adapter.addFragment(new Profile_personal_Fragment(), "1" + "\nPersonal");
        /*adapter.addFragment(new Profile_Education_Fragment(), "2" + "\n" + "Education");
        adapter.addFragment(new Profile_ExpFragment(), "3" + "\n" + "Experience"); */
        adapter.addFragment(new Profile_ExpFragment(), "2" + "\n" + "Experience");
        adapter.addFragment(new Profile_Education_Fragment(), "3" + "\n" + "Education");
        adapter.addFragment(new Profile_AvailabilityFragment(), "4" + "\n" + "Availability");
        adapter.addFragment(new Profile_submitFragment(), "5" + "\n" + "  Submit  ");
//        adapter.addFragment(new ItemOneFragment(), "Month");
//        adapter.addFragment(new ItemOneFragment(), "Month");
//        adapter.addFragment(new ItemOneFragment(), "My Teams");
        viewPager.setAdapter(adapter);


    }

    public void changePage(int pgNo) {
        viewPager.setCurrentItem(pgNo);
    }

    static class Adapter extends FragmentPagerAdapter {
        private      List<Fragment> mFragmentList = new ArrayList<>();
        private final List<String> mFragmentTitleList = new ArrayList<>();
        private FragmentManager fragMan;
        public Adapter(FragmentManager manager) {
            super(manager);
        }

        @Override
        public Fragment getItem(int position) {
            return mFragmentList.get(position);
        }

        @Override
        public int getCount() {
            return mFragmentList.size();
        }



        public void clearAll() //You can clear any specified page if you want...
        {
            for(int i=0; i<mFragmentList.size(); i++)
                fragMan.beginTransaction().remove(mFragmentList.get(i)).commit();
            mFragmentList.clear();
            mFragmentList=new ArrayList<Fragment>();
            notifyDataSetChanged();
        }
        public void addFragment(Fragment fragment, String title) {
            mFragmentList.add(fragment);
            mFragmentTitleList.add(title);
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitleList.get(position);
        }
    }
}


