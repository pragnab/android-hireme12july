package com.hiremehealthcare.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.SpannableString;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.hiremehealthcare.CommonCls.CustomButton;
import com.hiremehealthcare.CommonCls.CustomEditText;
import com.hiremehealthcare.CommonCls.DialogUtils;
import com.hiremehealthcare.CommonCls.ExpandableHeightListView;
import com.hiremehealthcare.CommonCls.URLS;
import com.hiremehealthcare.CommonCls.Validations;
import com.hiremehealthcare.POJO.ExperienceListModel;
import com.hiremehealthcare.POJO.Speciality;
import com.hiremehealthcare.R;
import com.hiremehealthcare.Utils.CommonRequestHelper;
import com.hiremehealthcare.adapter.CustomSpecialityAdapter;
import com.hiremehealthcare.adapter.ExperienceListAdpter;
import com.hiremehealthcare.adapter.SpinnerAdapter;
import com.hiremehealthcare.database.MySharedPrefereces;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by ADMIN on 03-05-2018.
 * this class is use to show experience of applicant
 */

public class Profile_ExpFragment extends Fragment implements View.OnClickListener {

    ExpandableHeightListView lvExperienceList;
    CustomEditText etExpLocation;
    Spinner spExpSelectSpecialty, spExpYear, spExpMonth;
    CustomButton btnAddExperience, btnBack, btnNext;
    List<String> monthList;
    List<String> yearList;
    ProgressDialog progressDialog;
    Speciality speciality;
    List<ExperienceListModel.Data> experienceList;
    ExperienceListAdpter experienceListAdpter;
    public String updated_id = "";
    MySharedPrefereces mySharedPrefereces;
    public static Profile_ExpFragment profile_expFragment;
    ExperienceListModel experienceListModel;
    Context context;
    String TAG="Profile_ExpFragment";
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.profile_expfragment, container, false);
        context=getContext();
        profile_expFragment=this;
        initAllControls(view);
        return view;
    }

    public void initAllControls(View rootView) {
        mySharedPrefereces = new MySharedPrefereces(context);
        progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        SpannableString spannableString =  new SpannableString(getString(R.string.please_wait));
        Typeface typefaceSpan = Validations.setTypeface(context);
        spannableString.setSpan(typefaceSpan, 0, getString(R.string.please_wait).length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        progressDialog.setMessage(spannableString);
        lvExperienceList = (ExpandableHeightListView) rootView.findViewById(R.id.lvExperienceList);
        etExpLocation = (CustomEditText) rootView.findViewById(R.id.etExpLocation);
        spExpSelectSpecialty = (Spinner) rootView.findViewById(R.id.spExpSelectSpecialty);
        spExpYear = (Spinner) rootView.findViewById(R.id.spExpYear);
        spExpMonth = (Spinner) rootView.findViewById(R.id.spExpMonth);
        btnAddExperience = (CustomButton) rootView.findViewById(R.id.btnAddExperience);
        btnBack = (CustomButton) rootView.findViewById(R.id.btnBack);
        btnNext = (CustomButton) rootView.findViewById(R.id.btnNext);
        experienceList = new ArrayList<>();
        experienceListAdpter = new ExperienceListAdpter(context, experienceList, new ExperienceListAdpter.OnGettingEditAndDeleteMethod() {
            @Override
            public void onDeleteButtonClicked(int postion) {
                Profile_ExpFragment.this.onDeleteButtonClicked(experienceList.get(postion));
            }

            @Override
            public void onUpdateButtonClicked(int position) {
                Profile_ExpFragment.this.onUpdateButtonClicked(experienceList.get(position));
            }
        },true);
        lvExperienceList.setAdapter(experienceListAdpter);
        lvExperienceList.setExpanded(true);
        btnAddExperience.setOnClickListener(this);
        btnBack.setOnClickListener(this);
        btnNext.setOnClickListener(this);
        monthList = new ArrayList<>();
        yearList = new ArrayList<>();
        getSpecility();
        getMonthData();
        getYearData();
        SpinnerAdapter monthSpinnerAdapter = new SpinnerAdapter(context, monthList);
        SpinnerAdapter yearSpinnerAdapter = new SpinnerAdapter(context, yearList);
        spExpMonth.setAdapter(monthSpinnerAdapter);
        spExpYear.setAdapter(yearSpinnerAdapter);
        getExperienceList();
    }

    /**
     * this method called when user clicked update button
     * @param data
     */
    public void onUpdateButtonClicked(ExperienceListModel.Data data) {
        updated_id = data.getApplicant_experience_id();
        etExpLocation.setText(data.getApplicant_experience_location());
        int couter = 0;
        int selectedspecility = 0;
        for (Speciality.SpecialtyBean specialtyBean : speciality.getSpecialty()) {
            if (specialtyBean.getSpecialty_name().equals(data.getSpeciality())) {
                selectedspecility = couter;
            }
            couter++;
        }
        spExpSelectSpecialty.setSelection(selectedspecility);
        String[] duration = data.getExperience_label().split(" ");
        int year=0,month=0;
        if (duration.length > 0)
            year = Integer.parseInt(duration[0]);
        if(duration.length>2)
        {
            month= Integer.parseInt(duration[2]);
        }
        spExpMonth.setSelection(month+1);
        spExpYear.setSelection(year+1);


    }

    /**
     * this method called when use clicked delete Experience button Clicked
     * @param data which object you want to delete
     */
    public void onDeleteButtonClicked(final ExperienceListModel.Data data) {

        progressDialog.show();
        String url = URLS.DELETE_APPLICANT_EXPERIENCE+"&" +URLS.APPLICANT_ID+ "="+mySharedPrefereces.getUserId()+"&"+URLS.EXPERIENCE_ID+"="+data.getApplicant_experience_id();
        Log.d("Experience Url", url);
        CommonRequestHelper.getDataFromUrl(url, new CommonRequestHelper.VolleyResponseCustom() {
            @Override
            public void getServerResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getInt("status") == 1)
                        getExperienceList();
                    else {
                        DialogUtils.showDialog4Activity(getActivity(), context.getResources().getString(R.string.app_name), jsonObject.getString("message"), null);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
                progressDialog.dismiss();

            }

            @Override
            public void getVolleyError(VolleyError volleyError) {
                progressDialog.dismiss();
                NetworkResponse networkResponse = volleyError.networkResponse;
                DialogUtils.noInterNetDialog(context, "No Internet Connection", "No active internet connection available", new DialogUtils.NoInterNetDailogClickListner() {
                    @Override
                    public void onDialogOkButtonClicked() {
                        onDeleteButtonClicked(data);
                    }

                    @Override
                    public void onDialogCancelButtonClicked() {

                    }
                });

            }
        }, context);

    }

    /**
     * +this method is used to getting experiencelist from server
     */

    public void getExperienceList() {
        experienceList.clear();
        progressDialog.show();
        String url = URLS.APPLICANT_EXPERIENCE_LIST + mySharedPrefereces.getUserId();
        Log.d(TAG, "getExperienceList: URL:- " + url);
        CommonRequestHelper.getDataFromUrl(url, new CommonRequestHelper.VolleyResponseCustom() {
            @Override
            public void getServerResponse(String response) {
                Gson gson = new Gson();
                Log.d(TAG, "getExperienceList: URL:- " + response);

                experienceListModel = gson.fromJson(response, ExperienceListModel.class);
                if (experienceListModel != null) {
                    experienceList.addAll(experienceListModel.getData());
                    experienceListAdpter.notifyDataSetChanged();
                } else {
                    Log.d("Experience", "is null");
                }
                progressDialog.dismiss();

            }

            @Override
            public void getVolleyError(VolleyError volleyError) {
                progressDialog.dismiss();
                Log.d(TAG, "getExperienceList: URL:- " + volleyError.getMessage());
                NetworkResponse networkResponse = volleyError.networkResponse;
                if (networkResponse != null) {
                    int statusCode = volleyError.networkResponse.statusCode;
                    System.out.println("STATUS CODE " + statusCode + "");
                    DialogUtils.showDialog4Activity(getActivity(), context.getResources().getString(R.string.app_name), statusCode + "", null);
                }
            }
        }, context);
    }

    public void getMonthData() {
        monthList.add("Months");
        for (int i = 0; i < 12; i++) {
            monthList.add("" + i);
        }
    }

    public void getYearData() {
        yearList.add("Years");
        for (int i = 0; i < 26; i++) {
            yearList.add("" + i);
        }
    }

    /**
     * this method is used to getting specility from server
     */

    public void getSpecility() {
        progressDialog.show();
        Log.d(TAG, "getSpecility: URL:- "+URLS.SPECIALTY_URL);
        CommonRequestHelper.getDataFromUrl(URLS.SPECIALTY_URL, new CommonRequestHelper.VolleyResponseCustom() {
            @Override
            public void getServerResponse(String response) {
                Log.d(TAG, "getServerResponse: Response: " + response);
                Gson gson = new Gson();
                progressDialog.dismiss();
                speciality = gson.fromJson(response, Speciality.class);
                if (speciality != null) {
                    Speciality.SpecialtyBean specialtyBean = new Speciality.SpecialtyBean();
                    specialtyBean.setSpecialty_id("0");
                    specialtyBean.setSpecialty_name("Select Specialty");
                    speciality.getSpecialty().add(0, specialtyBean);
                    CustomSpecialityAdapter customSpecialityAdapter = new CustomSpecialityAdapter(context, null, speciality);
                    spExpSelectSpecialty.setAdapter(customSpecialityAdapter);
                }
            }

            @Override
            public void getVolleyError(VolleyError volleyError) {
                progressDialog.dismiss();
                Log.d(TAG, "getServerResponse: Error: " + volleyError.getMessage());
                NetworkResponse networkResponse = volleyError.networkResponse;
                if (networkResponse != null) {
                    int statusCode = volleyError.networkResponse.statusCode;
                    System.out.println("STATUS CODE " + statusCode + "");
                 //   DialogUtils.showDialog4Activity(getActivity(), context.getResources().getString(R.string.app_name), statusCode + "", null);
                }
            }
        }, context);

    }

    @Override
    public void onClick(View view) {
        if (view == btnAddExperience) {
            onAddExperienceButtonClicked();
            /*pragna 24 july */
        } else if (view == btnBack) {

//            ProfileFragment.viewPager.setCurrentItem(1);
            ProfileFragment.viewPager.setCurrentItem(0);
        } else if (view == btnNext) {
//            ProfileFragment.viewPager.setCurrentItem(3);
            ProfileFragment.viewPager.setCurrentItem(2);

        }

    }

    /**
     * this method called when add Experience button clicked
     */

    public void onAddExperienceButtonClicked() {
        String location = etExpLocation.getText().toString();
        int selectedPosition = spExpSelectSpecialty.getSelectedItemPosition();
        int yearSelectedPosition = spExpYear.getSelectedItemPosition();
        int monthSelectedPosition = spExpMonth.getSelectedItemPosition();
        if (location.trim().length() <= 0) {
            Toast.makeText(context, "Please Enter Location Name", Toast.LENGTH_LONG).show();
        } else if (selectedPosition == 0) {
            Toast.makeText(context, "Please Select Speciality", Toast.LENGTH_LONG).show();
        } else if (yearSelectedPosition == 0) {
            Toast.makeText(context, "Please Select Years", Toast.LENGTH_LONG).show();
        } else if (monthSelectedPosition == 0) {
            Toast.makeText(context, "Please Select Month", Toast.LENGTH_LONG).show();
        } else {
            sendAddExperienceDataToServer(location, spExpYear.getSelectedItem().toString(), spExpMonth.getSelectedItem().toString(), speciality.getSpecialty().get(spExpSelectSpecialty.getSelectedItemPosition()).getSpecialty_id(), updated_id);
        }
    }

    /**
     * this method is used to send experience data to server
     * @param location which location
     * @param year which year
     * @param month which month
     * @param selectedSpeciality_id which specility
     * @param experiance_id experienced
     */

    public void sendAddExperienceDataToServer(final String location, final String year, final String month, final String selectedSpeciality_id, final String experiance_id) {

        progressDialog.show();

        String Url = URLS.INSERT_APPLICANT_EXPERIENCE + "&" + URLS.APPLICANT_ID + "="+mySharedPrefereces.getUserId()+"&" + URLS.LOCATION + "=" + location + "&" + URLS.SPECIALITY + "=" + selectedSpeciality_id + "&" + URLS.YEAR + "=" + year + "&+" + URLS.MONTH + "=" + month + "&" + URLS.EXPERIENCE_ID + "=" + experiance_id;
        URL url = null;
        try {
            url = new URL(Url);
            URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef());
            url = uri.toURL();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "sendAddExperienceDataToServer: URL:-"+url.toString());
        CommonRequestHelper.getDataFromUrl(url.toString(), new CommonRequestHelper.VolleyResponseCustom() {
            @Override
            public void getServerResponse(String response) {
                Log.d(TAG, "sendAddExperienceDataToServer: Reponse:-"+response);
                progressDialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    String message = jsonObject.getString("message");
                    int status = jsonObject.getInt("status");
                    etExpLocation.setText("");
                    spExpSelectSpecialty.setSelection(0);
                    spExpMonth.setSelection(0);
                    spExpYear.setSelection(0);
                    getExperienceList();
                    if (status == 1) {
                        updated_id = "";
                        ProfileFragment.viewPager.setCurrentItem(2);
                    } else {
                        DialogUtils.showDialog4Activity(context, context.getResources().getString(R.string.app_name), message + "", null);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void getVolleyError(VolleyError volleyError) {
                progressDialog.dismiss();
                Log.d(TAG, "sendAddExperienceDataToServer: Error:-" + volleyError.getMessage());
                NetworkResponse networkResponse = volleyError.networkResponse;
                DialogUtils.noInterNetDialog(context, "No Internet Connection", "No active internet connection available", new DialogUtils.NoInterNetDailogClickListner() {
                    @Override
                    public void onDialogOkButtonClicked() {
                        sendAddExperienceDataToServer(location,year,month,selectedSpeciality_id,experiance_id);
                    }

                    @Override
                    public void onDialogCancelButtonClicked() {

                    }
                });



            }
        }, context);
    }
}
