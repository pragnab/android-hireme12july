package com.hiremehealthcare.fragment;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.hiremehealthcare.CommonCls.CustomBoldTextView;
import com.hiremehealthcare.CommonCls.CustomTextView;
import com.hiremehealthcare.CommonCls.DialogUtils;
import com.hiremehealthcare.CommonCls.URLS;
import com.hiremehealthcare.POJO.GetManagerFacilityDetailModel;
import com.hiremehealthcare.POJO.ManagerProfileModel;
import com.hiremehealthcare.R;
import com.hiremehealthcare.Utils.CommonRequestHelper;
import com.hiremehealthcare.activity.DashBoardManagerActivity;
import com.hiremehealthcare.activity.HomeManagerFrgmentActivity;
import com.hiremehealthcare.activity.PostNewPositionActivity;
import com.hiremehealthcare.activity.SheduledInterviewsFragmentActivity;
import com.hiremehealthcare.database.MySharedPrefereces;

public class ManagerDashboardFragment_updated extends Fragment {
    /*  private ImageView userimg;
      private com.hiremehealthcare.CommonCls.CustomTextView tvname;
      private com.hiremehealthcare.CommonCls.CustomBoldTextView tvtxt2;
      private com.hiremehealthcare.CommonCls.CustomTextView tvtxt3;
      private LinearLayout lincandidates;
      private LinearLayout linsearchcandidates;
      private LinearLayout linmypostion;
      private LinearLayout lincommunication;
      private LinearLayout lincommingsoon;*/
    MySharedPrefereces mySharedPrefereces;
    Context context;
    String TAG = " ManagerDashboardFragment_updated  ";
    private ImageView userimg;
    private CustomTextView tvname;
    private CustomBoldTextView tvtxt2;
    private CustomTextView tvtxt3;
    private LinearLayout lincandidates;
    private LinearLayout linsearchcandidates;
    private LinearLayout linmypostion;
    private LinearLayout lincommunication;
    private LinearLayout lininterviewSchedule;
    private LinearLayout lincommingsoon;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootview = inflater.inflate(R.layout.manager_dashboard_updated, null);
//        this.lincommingsoon = (LinearLayout) rootview.findViewById(R.id.lin_comming_soon);
        this.lincommingsoon = (LinearLayout) rootview.findViewById(R.id.lin_post_newposition);
        this.lininterviewSchedule = (LinearLayout) rootview.findViewById(R.id.lin_interviewSchedule);
        this.lincommunication = (LinearLayout) rootview.findViewById(R.id.lin_communication);
        this.linmypostion = (LinearLayout) rootview.findViewById(R.id.lin_mypostion);
        this.linsearchcandidates = (LinearLayout) rootview.findViewById(R.id.lin_search_candidates);
        this.lincandidates = (LinearLayout) rootview.findViewById(R.id.lin_candidates);
        this.tvtxt3 = (CustomTextView) rootview.findViewById(R.id.tv_txt3);
        this.tvtxt2 = (CustomBoldTextView) rootview.findViewById(R.id.tv_txt2);
        this.tvname = (CustomTextView) rootview.findViewById(R.id.tv_name);
        this.userimg = (ImageView) rootview.findViewById(R.id.user_img);
        context = getActivity();
        queue = Volley.newRequestQueue(context);
        mySharedPrefereces = new MySharedPrefereces(context);
      /*  this.lincommingsoon = (LinearLayout) rootview.findViewById(R.id.lin_comming_soon);
        this.linmypostion = (LinearLayout) rootview.findViewById(R.id.lin_my_postion);
        this.lincommunication = (LinearLayout) rootview.findViewById(R.id.lin_communication);
        this.linmypostion = (LinearLayout) rootview.findViewById(R.id.lin_mypostion);
        this.linsearchcandidates = (LinearLayout) rootview.findViewById(R.id.lin_search_candidates);
        this.lincandidates = (LinearLayout) rootview.findViewById(R.id.lin_candidates);
        this.tvtxt3 = (CustomTextView) rootview.findViewById(R.id.tv_txt3);
        this.tvtxt2 = (CustomBoldTextView) rootview.findViewById(R.id.tv_txt2);
        this.tvname = (CustomTextView) rootview.findViewById(R.id.tv_name);
        this.userimg = (ImageView) rootview.findViewById(R.id.user_img);*/
        getProfileDetailFromaApi();
        getManagerFacilityDetail();
        /*lincandidates.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DashBoardManagerActivity.viewpager.setCurrentItem(1);
            }
        });
        linsearchcandidates.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DashBoardManagerActivity.viewpager.setCurrentItem(2);
            }
        });
        linmypostion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DashBoardManagerActivity.viewpager.setCurrentItem(3);
            }
        });
        lincommunication.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DashBoardManagerActivity.viewpager.setCurrentItem(5);
            }
        });
        lininterviewSchedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DashBoardManagerActivity.viewpager.setCurrentItem(4);
            }
        });

        lincommingsoon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DashBoardManagerActivity.viewpager.setCurrentItem(6);
            }
        });*/
        lincandidates.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                DashBoardManagerActivity.viewpager.setCurrentItem(1);
                Intent i = new Intent(getActivity(), HomeManagerFrgmentActivity.class);
                startActivity(i);

            }
        });
        linsearchcandidates.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DashBoardManagerActivity.viewpager.setCurrentItem(1);
            }
        });
        linmypostion.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DashBoardManagerActivity.viewpager.setCurrentItem(2);
            }
        });
        lincommunication.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DashBoardManagerActivity.viewpager.setCurrentItem(3);
            }
        });
        lininterviewSchedule.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //  DashBoardManagerActivity.viewpager.setCurrentItem(4);
                Intent i = new Intent(getActivity(), SheduledInterviewsFragmentActivity.class);
                startActivity(i);
            }
        });

        lincommingsoon.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                /*22-March changes pragna*/
                //  DashBoardManagerActivity.viewpager.setCurrentItem(4);

                Intent i = new Intent(getActivity(), PostNewPositionActivity.class);
                startActivity(i);
            }
        });

        return rootview;
    }
    RequestQueue queue;
    ManagerProfileModel managerProfileModel;
    public void getManagerFacilityDetail() {
        String Url = URLS.MANAGER_FACILITY_DETAIL + mySharedPrefereces.getUserId();
        Log.d(TAG, "getManagerFacilityDetail: Url " + Url);
        StringRequest req = new StringRequest(Request.Method.GET, Url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "getManagerFacilityDetail: response " + response);
                Gson gson = new Gson();
                GetManagerFacilityDetailModel managerFacilityDetailModel = gson.fromJson(response, GetManagerFacilityDetailModel.class);

                if (managerFacilityDetailModel != null) {

//                    tvfacility.setText(managerFacilityDetailModel.getMessage().get(0).getName() + " " + managerFacilityDetailModel.getMessage().get(0).getAddress() + " " + managerFacilityDetailModel.getMessage().get(0).getCity() + " " + managerFacilityDetailModel.getMessage().get(0).getState() + " " + managerFacilityDetailModel.getMessage().get(0).getZipcode());
                    tvtxt2.setText(managerFacilityDetailModel.getMessage().get(0).getName() + " ( "+managerFacilityDetailModel.getMessage().get(0).getBillingId()+" )");
                } else {

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.d(TAG, "getManagerFacilityDetail: response " + error.getMessage());
                if (networkResponse != null) {
                    int statusCode = error.networkResponse.statusCode;
                    System.out.println("STATUS CODE " + statusCode + "");
                    DialogUtils.showDialog4Activity(getActivity(), context.getResources().getString(R.string.app_name), statusCode + "", null);
                }

            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(req);

    }
    public void getProfileDetailFromaApi() {

        String Url = URLS.GET_MANAGER_PROFILE_DETAIL + mySharedPrefereces.getUserId();
        Log.d(TAG, "getProfileDetailFromaApi: URL:-" + Url);
        CommonRequestHelper.getDataFromUrl(Url, new CommonRequestHelper.VolleyResponseCustom() {
            @Override
            public void getServerResponse(String response) {
                Log.d(TAG, "getProfileDetailFromaApi: response:-" + response);

                Gson gson = new Gson();

                managerProfileModel = gson.fromJson(response, ManagerProfileModel.class);
                if (managerProfileModel.getData() != null) {
                    tvname.setText(managerProfileModel.getData().getManagerFirstname() + " " + managerProfileModel.getData().getManagerLastname() + "");
                    tvtxt3.setText(managerProfileModel.getData().getManagerZipcode() + "");

                    //  tvtxt2.setText(managerProfileModel.getData().get() + "");
//                    tvtxt2.setText(mySharedPrefereces.getCity() + "");



//                    tvtxt2.setText(mySharedPrefereces.ge() + "");
//                    Glide.with(context).load(mySharedPrefereces.getUserImage() + "").error(R.drawable.no_image).into(userimg);

                    Glide.with(context)
                            .load(managerProfileModel.getData().getManagerImage() + "")
                            .error(R.drawable.no_image)
                            //.placeholder(R.drawable.no_image)
                            .into(userimg);
                    //  a;sldkl;


                }
            }

            @Override
            public void getVolleyError(VolleyError volleyError) {

                Log.d(TAG, "getProfileDetailFromaApi: Error:-" + volleyError.getMessage());
                DialogUtils.noInterNetDialog(context, "No Internet Connection", "No active internet connection available", new DialogUtils.NoInterNetDailogClickListner() {
                    @Override
                    public void onDialogOkButtonClicked() {
                        getProfileDetailFromaApi();
                        getManagerFacilityDetail();
                    }

                    @Override
                    public void onDialogCancelButtonClicked() {

                    }
                });

                //Log.d("UpdateProfileFragment", volleyError);
            }
        }, context);

    }
}
