package com.hiremehealthcare.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.hiremehealthcare.CommonCls.CustomButton;
import com.hiremehealthcare.CommonCls.CustomTextView;
import com.hiremehealthcare.CommonCls.DialogUtils;
import com.hiremehealthcare.CommonCls.ExpandableHeightGridView;
import com.hiremehealthcare.CommonCls.ExpandableHeightListView;
import com.hiremehealthcare.CommonCls.URLS;
import com.hiremehealthcare.CommonCls.Validations;
import com.hiremehealthcare.POJO.Benefits;
import com.hiremehealthcare.POJO.CandidateDetail;
import com.hiremehealthcare.POJO.ProfilePersonalDetail;
import com.hiremehealthcare.adapter.DaysMultiSelectAdapter;
import com.hiremehealthcare.adapter.DaysMultiSelectAdapter_duplicate_flip;
import com.hiremehealthcare.adapter.Days_adapter;
import com.hiremehealthcare.adapter.Evening_adapter;
import com.hiremehealthcare.adapter.Night_adapter;
import com.hiremehealthcare.adapter.Profile_licenseListAdpter;
import com.hiremehealthcare.activity.DashBoardActivity;
import com.hiremehealthcare.POJO.ApplicantLicenseModel;
import com.hiremehealthcare.POJO.License;
import com.hiremehealthcare.POJO.Speciality;
import com.hiremehealthcare.R;
import com.hiremehealthcare.Utils.CommonRequestHelper;
import com.hiremehealthcare.adapter.ExperienceListAdpter;
import com.hiremehealthcare.adapter.Profile_CertificationListAdpter;
import com.hiremehealthcare.adapter.Profile_DegreeListAdpter;
import com.hiremehealthcare.database.MySharedPrefereces;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by ADMIN on 03-05-2018.
 * this fragment is use only show user selected data
 */

public class Profile_submitFragment extends Fragment implements View.OnClickListener {
    private com.hiremehealthcare.CommonCls.CustomButton btnSubmitBack;
    private com.hiremehealthcare.CommonCls.CustomButton btnSubmitPriview;
    Context context;
    String TAG = "Profile_submitFragment";
    ProgressDialog progressDialog;
    MySharedPrefereces mySharedPrefereces;
    public static Days_adapter _daDays_checkBoxAdapter;
    public static Night_adapter _daNightcheckBoxAdapter;
    public static Evening_adapter _daevening_checkBoxAdapter;
    private com.hiremehealthcare.CommonCls.ExpandableHeightGridView griddays;
    ExpandableHeightGridView  grideveningdaystime, gridnightdaystime;
    ExpandableHeightGridView griddaystime;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.profile_submitfragment, container, false);
        context = getContext();
        initAllControls(view);
        return view;
    }

    public void initAllControls(View rootView) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        mySharedPrefereces = new MySharedPrefereces(getContext());
        SpannableString spannableString = new SpannableString(getString(R.string.please_wait));
        Typeface typefaceSpan = Validations.setTypeface(context);
        spannableString.setSpan(typefaceSpan, 0, getString(R.string.please_wait).length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        progressDialog.setMessage(spannableString);
        btnSubmitPriview = (CustomButton) rootView.findViewById(R.id.btnSubmitPriview);
        btnSubmitBack = (CustomButton) rootView.findViewById(R.id.btnSubmitBack);
        btnSubmitPriview.setOnClickListener(this);
        btnSubmitBack.setOnClickListener(this);
    }

    /**
     * thid method is used to show use profile dilaog
     */
    ArrayList<String> days;
    Benefits test;
    public void onPriviewButtonClicked() {
        String firstName = "", lastName = "", email = "", phone = "", picture = "", speciality = "", license = "", zip_code = "";
        if (Profile_personal_Fragment.profile_personal_fragment != null) {
            if (Profile_personal_Fragment.profile_personal_fragment.profilePersonalDetail != null) {
                firstName = Profile_personal_Fragment.profile_personal_fragment.profilePersonalDetail.getData().getApplicant_firstname();
                lastName = Profile_personal_Fragment.profile_personal_fragment.profilePersonalDetail.getData().getApplicant_lastname();
                email = Profile_personal_Fragment.profile_personal_fragment.profilePersonalDetail.getData().getApplicant_email();
                phone = Profile_personal_Fragment.profile_personal_fragment.profilePersonalDetail.getData().getApplicant_phone();
                picture = Profile_personal_Fragment.profile_personal_fragment.profilePersonalDetail.getData().getApplicant_image();
                speciality = Profile_personal_Fragment.profile_personal_fragment.profilePersonalDetail.getData().getApplicant_specialities();
                zip_code = Profile_personal_Fragment.profile_personal_fragment.profilePersonalDetail.getData().getApplicant_zipcode();
                license = Profile_personal_Fragment.profile_personal_fragment.profilePersonalDetail.getData().getApplicant_license();
            }
        }

        LayoutInflater inflater = LayoutInflater.from(context);
        final View dialogView = inflater.inflate(R.layout.priview_inflater, null);
        ImageView ivCloseDialog = (ImageView) dialogView.findViewById(R.id.ivCloseDialog);
        CustomTextView tvPreviewProfileFirstName = (CustomTextView) dialogView.findViewById(R.id.tvPreviewProfileFirstName);
        CustomTextView tvPreviewProfileLastName = (CustomTextView) dialogView.findViewById(R.id.tvPreviewProfileLastName);
        CustomTextView tvPreviewProfileEmail = (CustomTextView) dialogView.findViewById(R.id.tvPreviewProfileEmail);
        CustomTextView tvPreviewProfilePhone = (CustomTextView) dialogView.findViewById(R.id.tvPreviewProfilePhone);
        CustomTextView tvPreviewProfileLicenseList = (CustomTextView) dialogView.findViewById(R.id.tvPreviewProfileLicenseList);
        CustomTextView tvPreviewProfileSpeciltyList = (CustomTextView) dialogView.findViewById(R.id.tvPreviewProfileSpeciltyList);
        CustomTextView tvPreviewProfileZipCode = (CustomTextView) dialogView.findViewById(R.id.tvPreviewProfileZipCode);
        CustomTextView tvPreviewProfileSate = (CustomTextView) dialogView.findViewById(R.id.tvPreviewProfileSate);
        CustomTextView tvPreviewProfileCity = (CustomTextView) dialogView.findViewById(R.id.tvPreviewProfileCity);


        tvPreviewProfileSate.setText(Profile_personal_Fragment.profile_personal_fragment.selectedStateNAME);
        tvPreviewProfileCity.setText(Profile_personal_Fragment.profile_personal_fragment.selectedCityNAME);

        CustomButton btnSubmitProfile = (CustomButton) dialogView.findViewById(R.id.btnSubmitProfile);
        ExpandableHeightListView lvProfilePreviewLicense = (ExpandableHeightListView) dialogView.findViewById(R.id.lvProfilePreviewLicense);
        ExpandableHeightListView list_degree = (ExpandableHeightListView) dialogView.findViewById(R.id.list_degree);
        ExpandableHeightListView list_certification = (ExpandableHeightListView) dialogView.findViewById(R.id.list_certification);
        ExpandableHeightListView lvExperienceList = (ExpandableHeightListView) dialogView.findViewById(R.id.lvExperienceList);

        CustomTextView tvPostPositionShift = (CustomTextView) dialogView.findViewById(R.id.tvPostPositionShift);
        //String shiftnewLine = TextUtils.join("\n", Profile_AvailabilityFragment.profile_availabilityFragment.shiftAdapter.getSelectedItemName()+"");
      //  String hrnewLine = TextUtils.join("\n", Profile_AvailabilityFragment.profile_availabilityFragment.shiftAdapter.getSelectedItemName().get(0).toString()+"");




        CustomTextView tvPostPositionHours = (CustomTextView) dialogView.findViewById(R.id.tvPostPositionHours);
     //   String hrnewLine = TextUtils.join("\n", Profile_AvailabilityFragment.profile_availabilityFragment.hoursAdapter.getSelectedItemName()+"");
        try {

            tvPostPositionShift.setText(Profile_AvailabilityFragment.profile_availabilityFragment.shiftAdapter.getSelectedItemName().get(0).toString() + "");
            tvPostPositionHours.setText(Profile_AvailabilityFragment.profile_availabilityFragment.hoursAdapter.getSelectedItemName().get(0).toString() + "");
        }catch (Exception e)
        {
            System.out.println("this is error !!!!!!!!!!!!!!!");
        }


        // tvPostPositionShift.setText(shiftnewLine);

        ExpandableHeightGridView griddays, grideveningdaystime, gridnightdaystime;
        ExpandableHeightGridView griddaystime;
        days = new ArrayList<>();
        days.add("Mon");

        days.add("Tue");
        days.add("Wed");
        days.add("Thu");
        days.add("Fri");
        days.add("Sat");
        days.add("Sun");
        griddays = (ExpandableHeightGridView) dialogView.findViewById(R.id.grid_days);
        gridnightdaystime = (ExpandableHeightGridView) dialogView.findViewById(R.id.grid_night_days_time);
        grideveningdaystime = (ExpandableHeightGridView) dialogView.findViewById(R.id.grid_evening_days_time);
        gridnightdaystime.setExpanded(true);
        griddays.setExpanded(true);
        grideveningdaystime.setExpanded(true);


        griddaystime = (ExpandableHeightGridView) dialogView.findViewById(R.id.grid_days_time);
        griddays = (ExpandableHeightGridView) dialogView.findViewById(R.id.grid_days);
        gridnightdaystime = (ExpandableHeightGridView) dialogView.findViewById(R.id.grid_night_days_time);
        grideveningdaystime = (ExpandableHeightGridView) dialogView.findViewById(R.id.grid_evening_days_time);
        gridnightdaystime.setExpanded(true);
        griddays.setExpanded(true);
        grideveningdaystime.setExpanded(true);

        Benefits days_all = new Benefits();
        List<Benefits.BenefitsBean> all_day = new ArrayList<>();
        Benefits.BenefitsBean day = new Benefits.BenefitsBean();
        day.setBenefits_id("1");
        day.setBenefits_name("Mon");
        all_day.add(day);

        day = new Benefits.BenefitsBean();
        day.setBenefits_id("2");
        day.setBenefits_name("Tue");
        all_day.add(day);

        day = new Benefits.BenefitsBean();
        day.setBenefits_id("3");
        day.setBenefits_name("Wed");
        all_day.add(day);


        day = new Benefits.BenefitsBean();
        day.setBenefits_id("4");
        day.setBenefits_name("Thu");
        all_day.add(day);


        day = new Benefits.BenefitsBean();
        day.setBenefits_id("5");
        day.setBenefits_name("Fri");
        all_day.add(day);


        day = new Benefits.BenefitsBean();
        day.setBenefits_id("6");
        day.setBenefits_name("Sat");
        all_day.add(day);


        day = new Benefits.BenefitsBean();
        day.setBenefits_id("7");
        day.setBenefits_name("Sun");
        all_day.add(day);


        days_all.setBenefits(all_day);
        griddays.setExpanded(true);
        DaysMultiSelectAdapter _dayDaysMultiSelectAdapter;
        _dayDaysMultiSelectAdapter = new DaysMultiSelectAdapter(days, context);
        griddays.setAdapter(_dayDaysMultiSelectAdapter);
        test = new Benefits();
        //  test=days_all;
        _daDays_checkBoxAdapter = new Days_adapter(days_all, context, 1, test, true);
        griddaystime.setAdapter(_daDays_checkBoxAdapter);

//        _daDays_checkBoxAdapter = new Days_CheckBoxAdapter_updated(days_all, ctx, 1, test, false);
        griddaystime.setAdapter(_daDays_checkBoxAdapter);

        _daevening_checkBoxAdapter = new Evening_adapter(days_all, context, 2, true);
        grideveningdaystime.setAdapter(_daevening_checkBoxAdapter);


        _daNightcheckBoxAdapter = new Night_adapter(days_all, context, 3, true);
        gridnightdaystime.setAdapter(_daNightcheckBoxAdapter);



        lvProfilePreviewLicense.setExpanded(true);
        list_degree.setExpanded(true);
        list_certification.setExpanded(true);
        lvExperienceList.setExpanded(true);
        CircleImageView profile_image = (CircleImageView) dialogView.findViewById(R.id.profile_image);
        //if(picture!=null && !picture.trim().equals("")) {
        Glide.with(context)
                .load(picture)
                .error(R.drawable.no_image)
                //.placeholder(R.drawable.no_image)
                .into(profile_image);

        if(DashBoardActivity.civ_requests!=null) {
            Glide.with(context)
                    .load(picture)
                    .error(R.drawable.no_image)
                    //.placeholder(R.drawable.no_image)
                    .into(DashBoardActivity.civ_requests);
        }




        //}
        Log.d(TAG, "onPriviewButtonClicked:glide url:- " + picture);

        if (Profile_ExpFragment.profile_expFragment != null) {
            if (Profile_ExpFragment.profile_expFragment.experienceListModel != null) {
                ExperienceListAdpter experienceListAdpter = new ExperienceListAdpter(context, Profile_ExpFragment.profile_expFragment.experienceListModel.getData(), new ExperienceListAdpter.OnGettingEditAndDeleteMethod() {
                    @Override
                    public void onDeleteButtonClicked(int postion) {
                    }

                    @Override
                    public void onUpdateButtonClicked(int position) {
                    }
                }, false);
                lvExperienceList.setAdapter(experienceListAdpter);
            }
        }
        if (Profile_Education_Fragment.profile_education_fragment.applicantCertificate != null) {
            Profile_CertificationListAdpter profile_certificationListAdpter = new Profile_CertificationListAdpter(context, Profile_Education_Fragment.profile_education_fragment.applicantCertificate, new Profile_CertificationListAdpter.OnDeleteListener() {
                @Override
                public void onDeleteButtonClicked(int position) {
                    //onCertificationDelete(certificates.getData().get(position));
                }
            }, false);
            list_certification.setAdapter(profile_certificationListAdpter);
        }
        if (Profile_Education_Fragment.profile_education_fragment != null) {
            if (Profile_Education_Fragment.profile_education_fragment.educationDetailModel != null) {
                Profile_DegreeListAdpter profileDegreeListAdpter = new Profile_DegreeListAdpter(context, Profile_Education_Fragment.profile_education_fragment.educationDetailModel.getDataList(), new Profile_DegreeListAdpter.OnGettingEditAndDeleteMethod() {
                    @Override
                    public void onDeleteButtonClicked(int postion) {
                        //onDegreeDeleteButtonClicked(educationDataList.get(postion));
                        //Toast.makeText(ctx,educationDataList.get(postion).getApplicant_education_institute()+" delete",Toast.LENGTH_LONG).show();
                    }

                    @Override
                    public void onUpdateButtonClicked(int position) {
                        //onDegreeUpdateButtonClicked(educationDataList.get(position));
                        //Toast.makeText(ctx,educationDataList.get(position).getApplicant_education_institute()+" update",Toast.LENGTH_LONG).show();
                    }
                }, false);
                list_degree.setAdapter(profileDegreeListAdpter);
            }
        }
        tvPreviewProfileFirstName.setText(firstName);
        tvPreviewProfileLastName.setText(lastName);
        tvPreviewProfileEmail.setText(email);
        tvPreviewProfilePhone.setText(phone);
        tvPreviewProfileZipCode.setText(zip_code);
        String specility_string = "";
        if (Profile_personal_Fragment.profile_personal_fragment.speciality != null) {
            if (speciality != null && !speciality.trim().equals("")) {
                if (speciality.contains(",")) {
                    String temp[] = speciality.split(",");
                    for (int i = 0; i < temp.length; i++) {
                        for (Speciality.SpecialtyBean data : Profile_personal_Fragment.profile_personal_fragment.speciality.getSpecialty()) {
                            if (data.getSpecialty_id().equals(temp[i])) {
                                specility_string = specility_string + data.getSpecialty_name() + "\n";
                            }
                        }
                    }

                    specility_string = getUnique(specility_string+"");

                    specility_string = specility_string.replace(",", "\n");
                    specility_string = specility_string.replaceFirst("\n", "");
                } else {
                    for (Speciality.SpecialtyBean data : Profile_personal_Fragment.profile_personal_fragment.speciality.getSpecialty()) {
                        if (data.getSpecialty_id().equals(speciality)) {
                            specility_string = specility_string + data.getSpecialty_name() + "\n";
                        }
                    }
                }
            }
        }
        tvPreviewProfileSpeciltyList.setText(specility_string);

        String license_string = "";
        if (Profile_personal_Fragment.profile_personal_fragment.license != null) {
            if (license != null && !license.trim().equals("")) {
                if (license.contains(",")) {
                    license_string = Profile_personal_Fragment.profile_personal_fragment.license.getLicense().get(0).getLicense_name()+"";
                    String temp[] = license.split(",");
                    for (int i = 0; i < temp.length; i++) {

                        for (License.LicenseBean data : Profile_personal_Fragment.profile_personal_fragment.license.getLicense()) {
                            if (data.getLicense_id().equals(temp[i])) {
//                                license_string=license_string+data.getLicense_name()+"\n";
                                license_string = license_string + data.getLicense_name() + ",";
                            }
                        }
                    }

                    license_string = getUnique(license_string);

                    license_string = license_string.replace(",", "\n");
                    license_string = license_string.replaceFirst("\n", "");
                } else {
                  /*  for (Speciality.SpecialtyBean data : Profile_personal_Fragment.profile_personal_fragment.da.getLicense()) {
                        if (data.getSpecialty_id().equals(speciality)) {
                         *//*   license_string = license_string + data.getSpecialty_name() + "\n";*//*
                            license_string = license_string + "";

                        }
                    }*/
                }
            }
        }
        tvPreviewProfileLicenseList.setText(license_string);

        if (Profile_personal_Fragment.profile_personal_fragment.applicantLicenseModel != null) {
            Profile_licenseListAdpter profile_licenseListAdpter = new Profile_licenseListAdpter(context, Profile_personal_Fragment.profile_personal_fragment.applicantLicenseModel.getData(), new Profile_licenseListAdpter.OnActionDeleteClickedListener() {
                @Override
                public void onDeleteButtonClicked(ApplicantLicenseModel.Datum data) {

                }
            }, false);

            lvProfilePreviewLicense.setAdapter(profile_licenseListAdpter);
        }

        /*TextView titileTextView = (TextView) dialogView.findViewById(R.id.tv_titile);
        TextView msgTextView = (TextView) dialogView.findViewById(R.id.tv_msg);
        Button dialogButtonOKButton = (Button) dialogView.findViewById(R.id.dialogButtonOK);
        titileTextView.setTypeface(Validations.setTypeface(context));
        msgTextView.setTypeface(Validations.setTypeface(context));
        dialogButtonOKButton.setTypeface(Validations.setTypeface(context));
        msgTextView.setText(message + "");
        titileTextView.setText(title + "");*/
//        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
//
        final AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(context, R.style.myDialog));
        final AlertDialog b = builder.create();
        //  builder.setTitle("Material Style Dialog");
        builder.setCancelable(true);
        builder.setView(dialogView);
        b.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        final AlertDialog show = builder.show();
        ivCloseDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                show.dismiss();
            }
        });
        btnSubmitProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                show.dismiss();
                onSubmitButtonClicked();
            }
        });
        /*dialogButtonOKButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                show.dismiss();

            }
        });*/

   //     availability(Profile_personal_Fragment.profile_personal_fragment.profilePersonalDetail.getData());

    }

    String getUnique(String input) {
        String UniqueS = "";

        String[] words = input.split(",");    //Split the word from String
        for (int i = 0; i < words.length; i++)        //Outer loop for Comparison
        {
            if (words[i] != null) {

                for (int j = i + 1; j < words.length; j++)    //Inner loop for Comparison
                {

                    if (words[i].equals(words[j]))    //Checking for both strings are equal
                    {
                        words[j] = null;            //Delete the duplicate words
                    }
                }
            }
        }
        for (int k = 0; k < words.length; k++)        //Displaying the String without duplicate words
        {
            if (words[k] != null) {
                System.out.println(words[k]);
                UniqueS = UniqueS + "," + words[k];
            }

        }

        return UniqueS;
    }

    public void onSubmitButtonClicked() {
        progressDialog.dismiss();
        String Url = URLS.CONFIRM_APPLICANT_PROFILE + mySharedPrefereces.getUserId();
        Log.d(TAG, "onSubmitButtonClicked: Url:- " + Url);
        CommonRequestHelper.getDataFromUrl(Url, new CommonRequestHelper.VolleyResponseCustom() {
            @Override
            public void getServerResponse(String response) {
                progressDialog.dismiss();
                Log.d(TAG, "onSubmitButtonClicked: Url:- " + response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int id = jsonObject.getInt("status");
                    if (id > 0) {
                        DashBoardActivity.viewpager.setCurrentItem(0);
                        DialogUtils.showDialog4Activity(getActivity(), context.getResources().getString(R.string.app_name), jsonObject.getString("message") + "", null);
                    } else {
                        DialogUtils.showDialog4Activity(getActivity(), context.getResources().getString(R.string.app_name), jsonObject.getString("message") + "", null);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void getVolleyError(VolleyError volleyError) {
                progressDialog.dismiss();
                Log.d(TAG, "onSubmitButtonClicked: Url:- " + volleyError.getMessage());
                DialogUtils.noInterNetDialog(getContext(), "No Internet Connection", "No active internet connection available", new DialogUtils.NoInterNetDailogClickListner() {
                    @Override
                    public void onDialogOkButtonClicked() {
                        onSubmitButtonClicked();
                    }

                    @Override
                    public void onDialogCancelButtonClicked() {

                    }
                });

            }
        }, getContext());
    }

    @Override
    public void onClick(View view) {
        if (view == btnSubmitPriview) {
            onPriviewButtonClicked();
        } else if (view == btnSubmitBack) {
            /*24 july Pragna*/
//            ProfileFragment.viewPager.setCurrentItem(2);
//            ProfileFragment.viewPager.setCurrentItem(2);
            ProfileFragment.viewPager.setCurrentItem(3);
        }
    }
    private boolean isNumeric(String string) {
        if(Pattern.matches("\\d{1,3}((\\.\\d{1,2})?)", string))
            return true;
        else
            return false;
    }

}
