package com.hiremehealthcare.fragment;

import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.hiremehealthcare.CommonCls.CustomBoldTextView;
import com.hiremehealthcare.CommonCls.CustomButton;
import com.hiremehealthcare.CommonCls.CustomTextView;
import com.hiremehealthcare.CommonCls.DialogUtils;
import com.hiremehealthcare.CommonCls.ExpandableHeightGridView;
import com.hiremehealthcare.CommonCls.URLS;
import com.hiremehealthcare.POJO.ActivePositionModel;
import com.hiremehealthcare.POJO.Applicant_availability;
import com.hiremehealthcare.POJO.Benefits;
import com.hiremehealthcare.POJO.Hours;
import com.hiremehealthcare.POJO.Shift;
import com.hiremehealthcare.R;
import com.hiremehealthcare.adapter.DaysMultiSelectAdapter;
import com.hiremehealthcare.adapter.DaysMultiSelectAdapter_duplicate_flip;
import com.hiremehealthcare.adapter.Days_CheckBoxAdapter;
import com.hiremehealthcare.adapter.Days_adapter;
import com.hiremehealthcare.adapter.Evening_CheckBoxAdapter;
import com.hiremehealthcare.adapter.Evening_adapter;
import com.hiremehealthcare.adapter.HoursAdapter;
import com.hiremehealthcare.adapter.Night_CheckBoxAdapter;
import com.hiremehealthcare.adapter.Night_adapter;
import com.hiremehealthcare.adapter.ShiftAdapter;
import com.hiremehealthcare.database.MySharedPrefereces;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.regex.Pattern;

public class Profile_AvailabilityFragment extends Fragment {
    private com.hiremehealthcare.CommonCls.CustomBoldTextView tvavailability;
    private com.hiremehealthcare.CommonCls.CustomTextView tvDays;
    private com.hiremehealthcare.CommonCls.CustomTextView tvEvening;
    private android.widget.LinearLayout v;
    private com.hiremehealthcare.CommonCls.CustomTextView tvNights;
    private com.hiremehealthcare.CommonCls.ExpandableHeightGridView griddays;
    private com.hiremehealthcare.CommonCls.ExpandableHeightGridView griddaystime;
    private com.hiremehealthcare.CommonCls.ExpandableHeightGridView grideveningdaystime;
    private com.hiremehealthcare.CommonCls.ExpandableHeightGridView gridnightdaystime;
    private android.widget.LinearLayout lin;

    Context ctx;
    RequestQueue queue;
    HoursAdapter hoursAdapter;
    ShiftAdapter shiftAdapter;
//    public static Days_CheckBoxAdapter _daDays_checkBoxAdapter;
//    public static Night_CheckBoxAdapter _daNightcheckBoxAdapter;
//    public static Evening_CheckBoxAdapter _daevening_checkBoxAdapter;

    public static Days_adapter _daDays_checkBoxAdapter;
    public static Night_adapter _daNightcheckBoxAdapter;
    public static Evening_adapter _daevening_checkBoxAdapter;
    DaysMultiSelectAdapter _dayDaysMultiSelectAdapter;
    String TAG = "Profile_AvailabilityFragment :  ";
    ArrayList<String> days;
    Benefits days_all;
    private CustomBoldTextView tvShift;
    private ExpandableHeightGridView gridtime;
    private CustomBoldTextView tvShifthour;
    private ExpandableHeightGridView gridhr;
    private android.widget.ScrollView scrollview;
    private com.hiremehealthcare.CommonCls.CustomButton btnback;
    private com.hiremehealthcare.CommonCls.CustomButton btnnxt;
    MySharedPrefereces mySharedPrefereces;
    String APPLICANT_ID = "";
    public static Profile_AvailabilityFragment profile_availabilityFragment;
    public static Profile_AvailabilityFragment newInstance() {
        Profile_AvailabilityFragment fragment = new Profile_AvailabilityFragment();
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.profile_availabilityfragment, null);
        this.scrollview = (ScrollView) v.findViewById(R.id.scrollview);
        this.btnnxt = (CustomButton) v.findViewById(R.id.btn_nxt);
        this.btnback = (CustomButton) v.findViewById(R.id.btn_back);

        profile_availabilityFragment=this;
        ctx = getActivity();
        queue = Volley.newRequestQueue(ctx);
        mySharedPrefereces = new MySharedPrefereces(ctx);
        APPLICANT_ID = mySharedPrefereces.getUserId() + "";
        init(v);
        btnnxt.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                nxtbtnclicked();
            }
        });
        btnback.setOnClickListener(new View.OnClickListener() {
                                       @Override
                                       public void onClick(View view) {
                                           ProfileFragment.viewPager.setCurrentItem(2);
                                       }
                                   }
        );

        /*4-feb-19 Pragna*/

        griddaystime.setExpanded(true);
        gridnightdaystime.setExpanded(true);
        grideveningdaystime.setExpanded(true);
        griddays.setExpanded(true);
        gridtime.setExpanded(true);
        gridhr.setExpanded(true);
        days = new ArrayList<>();
        days.add("Mon");

        days.add("Tue");
        days.add("Wed");
        days.add("Thu");
        days.add("Fri");
        days.add("Sat");
        days.add("Sun");

        days_all = new Benefits();
        List<Benefits.BenefitsBean> all_day = new ArrayList<>();
        Benefits.BenefitsBean day = new Benefits.BenefitsBean();
        day.setBenefits_id("1");
        day.setBenefits_name("Mon");
        all_day.add(day);

        day = new Benefits.BenefitsBean();
        day.setBenefits_id("2");
        day.setBenefits_name("Tue");
        all_day.add(day);

        day = new Benefits.BenefitsBean();
        day.setBenefits_id("3");
        day.setBenefits_name("Wed");
        all_day.add(day);


        day = new Benefits.BenefitsBean();
        day.setBenefits_id("4");
        day.setBenefits_name("Thu");
        all_day.add(day);


        day = new Benefits.BenefitsBean();
        day.setBenefits_id("5");
        day.setBenefits_name("Fri");
        all_day.add(day);


        day = new Benefits.BenefitsBean();
        day.setBenefits_id("6");
        day.setBenefits_name("Sat");
        all_day.add(day);


        day = new Benefits.BenefitsBean();
        day.setBenefits_id("7");
        day.setBenefits_name("Sun");
        all_day.add(day);

        griddays.setExpanded(true);
        griddaystime.setExpanded(true);
        grideveningdaystime.setExpanded(true);
        gridnightdaystime.setExpanded(true);
        days_all.setBenefits(all_day);

        _dayDaysMultiSelectAdapter = new DaysMultiSelectAdapter(days, ctx);
        griddays.setAdapter(_dayDaysMultiSelectAdapter);

        _daDays_checkBoxAdapter = new Days_adapter(days_all, ctx, 1, false);
        griddaystime.setAdapter(_daDays_checkBoxAdapter);


        _daevening_checkBoxAdapter = new Evening_adapter(days_all, ctx, 2, false);
        grideveningdaystime.setAdapter(_daevening_checkBoxAdapter);


        _daNightcheckBoxAdapter = new Night_adapter(days_all, ctx, 3, false);
        gridnightdaystime.setAdapter(_daNightcheckBoxAdapter);
//getApplicantAvailability(APPLICANT_ID+"");
        getShift();
        return v;
    }

    private void nxtbtnclicked() {
        String url = "";
        url = URLS.APPLICANT_AVAILABILITY;
        String selectedDays_to_Send = "";
        if (PostNewPostion.MON != null && PostNewPostion.MON.size() > 0) {
            List<String> uniqueList = getUniqueValues(PostNewPostion.MON);
            String mon = uniqueList.toString() + "";
            mon = mon.replace(" ", "");
            mon = mon.replace("[", "");
            mon = mon.replace("]", "");
            selectedDays_to_Send = selectedDays_to_Send + "&mon=" + mon;
        }
        if (PostNewPostion.TUE != null && PostNewPostion.TUE.size() > 0) {
            List<String> uniqueList = getUniqueValues(PostNewPostion.TUE);
            String tue = uniqueList.toString() + "";
            tue = tue.replace(" ", "");
            tue = tue.replace("[", "");
            tue = tue.replace("]", "");
            selectedDays_to_Send = selectedDays_to_Send + "&tue=" + tue;
        }
        if (PostNewPostion.WED != null && PostNewPostion.WED.size() > 0) {
            List<String> uniqueList = getUniqueValues(PostNewPostion.WED);
            String wed = uniqueList.toString() + "";
            wed = wed.replace(" ", "");
            wed = wed.replace("[", "");
            wed = wed.replace("]", "");
            selectedDays_to_Send = selectedDays_to_Send + "&wed=" + wed;
        }
        if (PostNewPostion.THR != null && PostNewPostion.THR.size() > 0) {
            List<String> uniqueList = getUniqueValues(PostNewPostion.THR);
            String thr = uniqueList.toString() + "";
            thr = thr.replace(" ", "");
            thr = thr.replace("[", "");
            thr = thr.replace("]", "");
            selectedDays_to_Send = selectedDays_to_Send + "&thu=" + thr;
        }
        if (PostNewPostion.FRI != null && PostNewPostion.FRI.size() > 0) {
            List<String> uniqueList = getUniqueValues(PostNewPostion.FRI);
            String fri = uniqueList.toString() + "";
            fri = fri.replace(" ", "");
            fri = fri.replace("[", "");
            fri = fri.replace("]", "");
            selectedDays_to_Send = selectedDays_to_Send + "&fri=" + fri;
        }
        if (PostNewPostion.SAT != null && PostNewPostion.SAT.size() > 0) {
            List<String> uniqueList = getUniqueValues(PostNewPostion.SAT);
            String sat = uniqueList.toString() + "";
            sat = sat.replace(" ", "");
            sat = sat.replace("[", "");
            sat = sat.replace("]", "");
            selectedDays_to_Send = selectedDays_to_Send + "&sat=" + sat;
        }
        if (PostNewPostion.SUN != null && PostNewPostion.SUN.size() > 0) {
            List<String> uniqueList = getUniqueValues(PostNewPostion.SUN);
            String sun = uniqueList.toString() + "";
            sun = sun.replace(" ", "");
            sun = sun.replace("[", "");
            sun = sun.replace("]", "");
            selectedDays_to_Send = selectedDays_to_Send + "&sun=" + sun;
        }
        System.out.println("this is final string ::::::::::::::::::::```````````````` " + selectedDays_to_Send + "");
        List<String> selectedShift = shiftAdapter.getSelected();
        List<String> selectedHour = hoursAdapter.getSelectedHr();
        String shiftStringList = TextUtils.join(",", selectedShift);
        String hoursStringList = TextUtils.join(",", selectedHour);
        System.out.println("FINALLY SELCTED DAYS ARE IN TO EDIT POS FRAGMENT ::::::::::::::::::::::::::::::::: ** " + selectedDays_to_Send + "");
      //  url = url + "&shift=" + selectedShift + "&hours=" + selectedHour + selectedDays_to_Send + "";
        url = url +APPLICANT_ID+ "&shift=" + shiftStringList + "&hours=" + hoursStringList + selectedDays_to_Send + "";
        System.out.println("FINAL URL :!!!!!!!!!!!!!!!!! "+url+"");
        StringRequest req = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getInt("status") == 1) {
                        DialogUtils.showDialog4Activity(ctx, getResources().getString(R.string.app_name), jsonObject.getString("message"), new DialogUtils.DailogCallBackOkButtonClick() {
                            @Override
                            public void onDialogOkButtonClicked() {
                                //  getActivity().setResult(Activity.RESULT_OK, null);
                                //  getActivity().finish();
                                ProfileFragment.viewPager.setCurrentItem(4);

                            }
                        });

                    } else {
                        DialogUtils.showDialog4Activity(ctx, getResources().getString(R.string.app_name), jsonObject.getString("message"), null);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });


        queue.add(req);
    }

    private void getApplicantAvailability(String APPLICANT_ID) {
        String Url = URLS.GET_APPLICANT_AVAILABILITY + "" + APPLICANT_ID;
        Log.d(TAG, "availability : URL:- " + Url);
        StringRequest req = new StringRequest(Request.Method.GET, Url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Gson gson = new Gson();
                response=response.replaceAll("[^\\p{Print}]","");
                Applicant_availability applicant_availability = gson.fromJson(response, Applicant_availability.class);

                if (applicant_availability != null) {


                    if (applicant_availability.getData() != null) {

                        ;
                        // if (shifts.getShift().get(0) != null) {
                        shiftAdapter.getSelected().clear();
                        shiftAdapter.getSelectedItemName().clear();
                        shiftAdapter.getSelected().add(applicant_availability.getData().get(0).getShift() + "");
                        shiftAdapter.getSelectedItemName().add(applicant_availability.getData().get(0).getShift_name() + "" + "");

                        hoursAdapter.getSelectedHr().clear();
                        hoursAdapter.getSelectedItemName().clear();
                        hoursAdapter.getSelectedHr().add(applicant_availability.getData().get(0).getHours() + "");
                        hoursAdapter.getSelectedItemName().add(applicant_availability.getData().get(0).getHour_name() + "" + "");
                        shiftAdapter.notifyDataSetChanged();
                        hoursAdapter.notifyDataSetChanged();

                        //  }
                        availability(applicant_availability.getData().get(0));
                    }
                }
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {

            }
        });
        queue.add(req);
    }

    private void getShift() {
        String Url = URLS.SHIFT_URL;
        Log.d(TAG, "getShift: URL:- " + Url);
        StringRequest req = new StringRequest(Request.Method.GET, Url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "getShift: response:- " + response);
                System.out.println("getShift RESPONSE " + response + "");
                Gson gson = new Gson();
                Shift shifts = gson.fromJson(response, Shift.class);

                if (shifts != null) {


                    if (shifts.getShift() != null) {
                        getHours();
                        //  if (shifts.getShift().get(0) != null) {
                        shiftAdapter = new ShiftAdapter(shifts, ctx);
                        gridtime.setAdapter(shiftAdapter);
//                        if (shifts.getShift().get(0) != null) {
//                            shiftAdapter.getSelected().clear();
//                            shiftAdapter.getSelectedItemName().clear();
//                            shiftAdapter.getSelected().add(shifts.getShift().get(0).getShift_id() + "");
//                            shiftAdapter.getSelectedItemName().add(shifts.getShift().get(0).getShift_name() + "" + "");
//                        }
                        if (EditPostionFragment._daDays_checkBoxAdapter != null) {
                            EditPostionFragment._daDays_checkBoxAdapter.notifyDataSetChanged();
                        }
                        if (EditPostionFragment._daevening_checkBoxAdapter != null) {
                            EditPostionFragment._daevening_checkBoxAdapter.notifyDataSetChanged();
                        }
                        if (EditPostionFragment._daNightcheckBoxAdapter != null) {
                            EditPostionFragment._daNightcheckBoxAdapter.notifyDataSetChanged();
                        }
                        // }
                    }

                } else {
                    // DialogUtils.showDialog4Activity(Login.this, ctx.getResources().getString(R.string.app_name), logindata.getMessage() + "");

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "getShift: Error:- " + error.getMessage());
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    int statusCode = error.networkResponse.statusCode;
                    System.out.println("STATUS CODE " + statusCode + "");
                    DialogUtils.showDialog4Activity(ctx, ctx.getResources().getString(R.string.app_name), statusCode + "", null);
                }

            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(req);

    }

    /**
     * this method is used to getting hour data from server
     */

    private void getHours() {
        String Url = URLS.HOURS_URL;
        Log.d(TAG, "getHours: URL " + Url);
        StringRequest req = new StringRequest(Request.Method.GET, Url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "getHours: Response " + response);
                System.out.println("CallHours RESPONSE " + response + "");
                Gson gson = new Gson();
                Hours hours = gson.fromJson(response, Hours.class);

                if (hours != null) {
                    hoursAdapter = new HoursAdapter(hours, ctx);
                    gridhr.setAdapter(hoursAdapter);
                    gridhr.setChoiceMode(GridView.CHOICE_MODE_SINGLE);
                    getApplicantAvailability(APPLICANT_ID);

                } else {
                    // DialogUtils.showDialog4Activity(Login.this, ctx.getResources().getString(R.string.app_name), logindata.getMessage() + "");

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "getHours: error:- " + error.getMessage());
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    int statusCode = error.networkResponse.statusCode;
                    System.out.println("STATUS CODE " + statusCode + "");
                    DialogUtils.showDialog4Activity(getActivity(), ctx.getResources().getString(R.string.app_name), statusCode + "", null);
                }

            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(req);

    }


    private void init(View v) {

        this.scrollview = (ScrollView) v.findViewById(R.id.scrollview);
        this.lin = (LinearLayout) v.findViewById(R.id.lin);
        this.gridnightdaystime = (ExpandableHeightGridView) v.findViewById(R.id.grid_night_days_time);
        this.grideveningdaystime = (ExpandableHeightGridView) v.findViewById(R.id.grid_evening_days_time);
        this.griddaystime = (ExpandableHeightGridView) v.findViewById(R.id.grid_days_time);
        this.griddays = (ExpandableHeightGridView) v.findViewById(R.id.grid_days);
        this.tvNights = (CustomTextView) v.findViewById(R.id.tv_Nights);
        this.v = (LinearLayout) v.findViewById(R.id.v);
        this.tvEvening = (CustomTextView) v.findViewById(R.id.tv_Evening);
        this.tvDays = (CustomTextView) v.findViewById(R.id.tv_Days);
        this.tvavailability = (CustomBoldTextView) v.findViewById(R.id.tv_availability);
        this.gridhr = (ExpandableHeightGridView) v.findViewById(R.id.grid_hr);
        this.tvShifthour = (CustomBoldTextView) v.findViewById(R.id.tv_Shifthour);
        this.gridtime = (ExpandableHeightGridView) v.findViewById(R.id.grid_time);
        this.tvShift = (CustomBoldTextView) v.findViewById(R.id.tv_Shift);
    }

    private boolean isNumeric(String string) {
        if (Pattern.matches("\\d{1,3}((\\.\\d{1,2})?)", string))
            return true;
        else
            return false;
    }

    @SuppressWarnings({"unchecked", "rawtypes"})
    public static List getUniqueValues(List input) {
        return new ArrayList<>(new HashSet<>(input));
    }

    private void availability(Applicant_availability.DataBean data) {
        ArrayList<String> days = new ArrayList<>();
        //   getavailability(data.getPositionId() + "");
        days = new ArrayList<>();
        days.add("Mon");
        Days_adapter.selected = new ArrayList<>();
        Days_adapter.selected_name = new ArrayList<>();

        Night_adapter.selected = new ArrayList<>();
        Night_adapter.selected_name = new ArrayList<>();


        Evening_adapter.selected = new ArrayList<>();
        Evening_adapter.selected_name = new ArrayList<>();


        days.add("Tue");
        days.add("Wed");
        days.add("Thu");
        days.add("Fri");
        days.add("Sat");
        days.add("Sun");
        System.out.println("availability MON : " + data.getMon() + " TUE : " + data.getTue() + " WED " + data.getWed() + " THR " + data.getThu() + " FRI " + data.getFri() + "");
        //   List<String> items = Arrays.asList(data.getMon().split("\\s*,\\s*"));

//PostNewPostion.MON.addAll(Arrays.asList(data.getMon().toLowerCase().split("\\s+")));
        List<String> items = new ArrayList<>();
//                    items.addAll(Arrays.asList(data.getMon().toLowerCase().split("\\s+")));
        items = Arrays.asList(data.getMon().split("\\s*,\\s*"));
        //  PostNewPostion.MON = new ArrayList<Integer>(items.size());
        PostNewPostion.MON = new ArrayList<>();
        List<Integer> items_int = new ArrayList<>();
        for (int i = 0; i < items.size(); i++) {
            if (isNumeric(items.get(i) + "")) {
                items_int.add(Integer.parseInt(items.get(i) + ""));
                if (items_int.get(i).equals(1)) {
                    Days_adapter.selected.add("1");
                    Days_adapter.selected_name.add("Mon");
                }
                if (items_int.get(i).equals(2)) {
                    Evening_adapter.selected.add("1");
                    Evening_adapter.selected_name.add("Mon");
                }
                if (items_int.get(i).equals(3)) {
                    Night_adapter.selected.add("1");
                    Night_adapter.selected_name.add("Mon");
                }
            }
        }
        //  List<String> items_tue = new ArrayList<>();
//                    items_tue=(Arrays.asList(data.getTue().split("\\s+")));
        List<String> items_tue = Arrays.asList(data.getTue().split("\\s*,\\s*"));

        //  PostNewPostion.MON = new ArrayList<Integer>(items.size());
        PostNewPostion.TUE = new ArrayList<>();
        List<Integer> items_int_tue = new ArrayList<>();
        //    System.out.println("TUE ```````````@ "+items_tue.toString()+"");
        for (int i = 0; i < items_tue.size(); i++) {
            System.out.println("items_tue.get(i) ````@@@@@@@@@@ " + items_tue.get(i) + "");
            if (isNumeric(items_tue.get(i) + "")) {
                items_int_tue.add(Integer.parseInt(items_tue.get(i) + ""));
                if (items_int_tue.get(i).equals(1)) {
                    Days_adapter.selected.add("2");
                    Days_adapter.selected_name.add("Tue");
                }
                if (items_int_tue.get(i).equals(2)) {
                    Evening_adapter.selected.add("2");
                    Evening_adapter.selected_name.add("Tue");
                }
                if (items_int_tue.get(i).equals(3)) {
                    Night_adapter.selected.add("2");
                    Night_adapter.selected_name.add("Tue");
                }
            }
        }


        List<String> items_wed = Arrays.asList(data.getWed().split("\\s*,\\s*"));

        //  PostNewPostion.MON = new ArrayList<Integer>(items.size());
        PostNewPostion.WED = new ArrayList<>();
        List<Integer> items_int_wed = new ArrayList<>();
        //   System.out.println("WED ```````````@ "+items_wed.toString()+"");
        for (int i = 0; i < items_wed.size(); i++) {
//                        System.out.println("items_tue.get(i) ````@@@@@@@@@@ "+ items_tue.get(i) + "");
            if (isNumeric(items_wed.get(i) + "")) {
                items_int_wed.add(Integer.parseInt(items_wed.get(i) + ""));
                if (items_int_wed.get(i).equals(1)) {
                    Days_adapter.selected.add("3");
                    Days_adapter.selected_name.add("Wed");
                }
                if (items_int_wed.get(i).equals(2)) {
                    Evening_adapter.selected.add("3");
                    Evening_adapter.selected_name.add("Wed");
                }
                if (items_int_wed.get(i).equals(3)) {
                    Night_adapter.selected.add("3");
                    Night_adapter.selected_name.add("Wed");
                }
            }
        }


        List<String> items_Thr = Arrays.asList(data.getThu().split("\\s*,\\s*"));

        //  PostNewPostion.MON = new ArrayList<Integer>(items.size());
        PostNewPostion.THR = new ArrayList<>();
        List<Integer> items_int_thr = new ArrayList<>();
        //  System.out.println("THRU ```````````@ "+items_Thr.toString()+"");
        for (int i = 0; i < items_Thr.size(); i++) {
//                        System.out.println("items_tue.get(i) ````@@@@@@@@@@ "+ items_tue.get(i) + "");
            if (isNumeric(items_Thr.get(i) + "")) {
                items_int_thr.add(Integer.parseInt(items_Thr.get(i) + ""));
                if (items_int_thr.get(i).equals(1)) {
                    Days_adapter.selected.add("4");
                    Days_adapter.selected_name.add("Thr");
                }
                if (items_int_thr.get(i).equals(2)) {
                    Evening_adapter.selected.add("4");
                    Evening_adapter.selected_name.add("Thr");
                }
                if (items_int_thr.get(i).equals(3)) {
                    Night_adapter.selected.add("4");
                    Night_adapter.selected_name.add("Thr");
                }
            }
        }


        List<String> items_Fri = Arrays.asList(data.getFri().split("\\s*,\\s*"));

        //  PostNewPostion.MON = new ArrayList<Integer>(items.size());
        PostNewPostion.FRI = new ArrayList<>();
        List<Integer> items_int_fri = new ArrayList<>();
        //   System.out.println("FRID ```````````@ "+items_Fri.toString()+"");
        for (int i = 0; i < items_Fri.size(); i++) {
//                        System.out.println("items_tue.get(i) ````@@@@@@@@@@ "+ items_tue.get(i) + "");
            if (isNumeric(items_Fri.get(i) + "")) {
                items_int_fri.add(Integer.parseInt(items_Fri.get(i) + ""));
                if (items_int_fri.get(i).equals(1)) {
                    Days_adapter.selected.add("5");
                    Days_adapter.selected_name.add("Fri");
                }
                if (items_int_fri.get(i).equals(2)) {
                    Evening_adapter.selected.add("5");
                    Evening_adapter.selected_name.add("Fri");
                }
                if (items_int_fri.get(i).equals(3)) {
                    Night_adapter.selected.add("5");
                    Night_adapter.selected_name.add("Fri");
                }
            }
        }


        List<String> items_Sat = Arrays.asList(data.getSat().split("\\s*,\\s*"));

        //  PostNewPostion.MON = new ArrayList<Integer>(items.size());
        PostNewPostion.FRI = new ArrayList<>();
        List<Integer> items_int_sat = new ArrayList<>();
        //   System.out.println("SAT ```````````@ "+items_Sat.toString()+"");
        for (int i = 0; i < items_Sat.size(); i++) {
//                        System.out.println("items_tue.get(i) ````@@@@@@@@@@ "+ items_tue.get(i) + "");
            if (isNumeric(items_Sat.get(i) + "")) {
                items_int_sat.add(Integer.parseInt(items_Sat.get(i) + ""));
                if (items_int_sat.get(i).equals(1)) {
                    Days_adapter.selected.add("6");
                    Days_adapter.selected_name.add("Sat");
                }
                if (items_int_sat.get(i).equals(2)) {
                    Evening_adapter.selected.add("6");
                    Evening_adapter.selected_name.add("Sat");
                }
                if (items_int_sat.get(i).equals(3)) {
                    Night_adapter.selected.add("6");
                    Night_adapter.selected_name.add("Sat");
                }
            }
        }


        List<String> items_Sun = Arrays.asList(data.getSun().split("\\s*,\\s*"));

        //  PostNewPostion.MON = new ArrayList<Integer>(items.size());
        PostNewPostion.FRI = new ArrayList<>();
        List<Integer> items_int_sun = new ArrayList<>();
        //   System.out.println("SUN ```````````@ "+items_Sun.toString()+"");
        for (int i = 0; i < items_Sun.size(); i++) {
//                        System.out.println("items_tue.get(i) ````@@@@@@@@@@ "+ items_tue.get(i) + "");
            if (isNumeric(items_Sun.get(i) + "")) {
                items_int_sun.add(Integer.parseInt(items_Sun.get(i) + ""));
                if (items_int_sun.get(i).equals(1)) {
                    Days_adapter.selected.add("7");
                    Days_adapter.selected_name.add("Sun");
                }
                if (items_int_sun.get(i).equals(2)) {
                    Evening_adapter.selected.add("7");
                    Evening_adapter.selected_name.add("Sun");
                }
                if (items_int_sun.get(i).equals(3)) {
                    Night_adapter.selected.add("7");
                    Night_adapter.selected_name.add("Sun");
                }
            }
        }


        PostNewPostion.MON.addAll(items_int);
        PostNewPostion.TUE.addAll(items_int_tue);
        PostNewPostion.WED.addAll(items_int_wed);
        PostNewPostion.THR.addAll(items_int_thr);
        PostNewPostion.FRI.addAll(items_int_fri);
        PostNewPostion.SAT.addAll(items_int_sat);
        PostNewPostion.SUN.addAll(items_int_sun);

//        System.out.println("this is monday " + PostNewPostion.MON.toString() + "");
//        System.out.println("this is Tuesday " + PostNewPostion.TUE.toString() + "");
//        System.out.println("this is TuesdayDays_adapter.selected_name``````````@ " + Days_adapter.selected_name .toString()+ "");
//        System.out.println("this is TuesdayEvening_adapter.selected_name``````````@ " + Evening_adapter.selected_name.toString() + "");
        //    v_v = LayoutInflater.from(_parent.getContext()).inflate(R.layout.testforflip, _parent, false);
        //    v_v = LayoutInflater.from(_parent.getContext()).inflate(R.layout.testforflip_availability, _parent, false);
        //  holder.row_item_flipper.removeView(v_v);
        // AnimationFactory.flipTransition(holder.row_item_flipper,
        //  AnimationFactory.FlipDirection.LEFT_RIGHT);


        // holder.row_item_flipper.addView(v_v);
        // initView(v_v);
        DaysMultiSelectAdapter_duplicate_flip _dayDaysMultiSelectAdapter;
        _dayDaysMultiSelectAdapter = new DaysMultiSelectAdapter_duplicate_flip(days, ctx);
        griddays.setExpanded(true);
        griddays.setAdapter(_dayDaysMultiSelectAdapter);
        Benefits days_all = new Benefits();
        List<Benefits.BenefitsBean> all_day = new ArrayList<>();
        Benefits.BenefitsBean day = new Benefits.BenefitsBean();


        day.setBenefits_id("1");
        day.setBenefits_name("Mon");
        all_day.add(day);

        day = new Benefits.BenefitsBean();
        day.setBenefits_id("2");
        day.setBenefits_name("Tue");
        all_day.add(day);

        day = new Benefits.BenefitsBean();
        day.setBenefits_id("3");
        day.setBenefits_name("Wed");
        all_day.add(day);


        day = new Benefits.BenefitsBean();
        day.setBenefits_id("4");
        day.setBenefits_name("Thu");
        all_day.add(day);


        day = new Benefits.BenefitsBean();
        day.setBenefits_id("5");
        day.setBenefits_name("Fri");
        all_day.add(day);


        day = new Benefits.BenefitsBean();
        day.setBenefits_id("6");
        day.setBenefits_name("Sat");
        all_day.add(day);


        day = new Benefits.BenefitsBean();
        day.setBenefits_id("7");
        day.setBenefits_name("Sun");
        all_day.add(day);


        days_all.setBenefits(all_day);
        Benefits test = new Benefits();
/*
                           _daDays_checkBoxAdapter = new Days_CheckBoxAdapter(days_all, context, 1,test,true);
                    griddaystime.setAdapter(_daDays_checkBoxAdapter);*/


        _daDays_checkBoxAdapter = new Days_adapter(days_all, ctx, 1, test, false);
        griddaystime.setAdapter(_daDays_checkBoxAdapter);

        _daevening_checkBoxAdapter = new Evening_adapter(days_all, ctx, 2, false);
        grideveningdaystime.setAdapter(_daevening_checkBoxAdapter);


        _daNightcheckBoxAdapter = new Night_adapter(days_all, ctx, 3, false);
        gridnightdaystime.setAdapter(_daNightcheckBoxAdapter);
    }

}
