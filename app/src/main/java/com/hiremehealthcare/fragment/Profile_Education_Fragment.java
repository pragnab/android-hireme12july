/*
 * Copyright (c) 2017. Truiton (http://www.truiton.com/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 * Mohit Gupt (https://github.com/mohitgupt)
 *
 */
//https://stackoverflow.com/questions/8100831/stop-scrollview-from-setting-focus-on-edittext
package com.hiremehealthcare.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.SpannableString;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.hiremehealthcare.CommonCls.CustomBoldTextView;
import com.hiremehealthcare.CommonCls.CustomButton;
import com.hiremehealthcare.CommonCls.CustomEditText;
import com.hiremehealthcare.CommonCls.DialogUtils;
import com.hiremehealthcare.CommonCls.ExpandableHeightGridView;
import com.hiremehealthcare.CommonCls.ExpandableHeightListView;
import com.hiremehealthcare.CommonCls.URLS;
import com.hiremehealthcare.CommonCls.Validations;
import com.hiremehealthcare.POJO.ApplicantCertificate;
import com.hiremehealthcare.POJO.Certificates;
import com.hiremehealthcare.POJO.EducationDetailModel;
import com.hiremehealthcare.POJO.Month;
import com.hiremehealthcare.POJO.Year;
import com.hiremehealthcare.R;
import com.hiremehealthcare.adapter.CertificationListAdapter;
import com.hiremehealthcare.adapter.CustomForCertiYearAdapter;
import com.hiremehealthcare.adapter.CustomYearAdapter;
import com.hiremehealthcare.adapter.LicenseAdapter;
import com.hiremehealthcare.adapter.MonthCustomAdapter;
import com.hiremehealthcare.adapter.Profile_CertificationListAdpter;
import com.hiremehealthcare.adapter.Profile_DegreeListAdpter;
import com.hiremehealthcare.adapter.SpecialityAdapter;
import com.hiremehealthcare.database.MySharedPrefereces;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

public class Profile_Education_Fragment extends Fragment implements View.OnClickListener {// implements View.OnClickListener {
    private CustomEditText edtfname;
    private CustomEditText edtlname;
    private CustomEditText edtemail;
    private CustomEditText edtphn;
    private CustomEditText edtphoto;
    private CustomEditText edtoccupation;
    private CustomBoldTextView tvlicense;
    private ExpandableHeightGridView gridlicence;
    private CustomButton btnback;
    private CustomButton btnnxt;
    RequestQueue queue;
    private CustomBoldTextView tvlic;
    private ExpandableHeightListView listlicence;
    private ScrollView scrollview;
    private CustomEditText edtuploadlic;
    private CustomEditText edtlicno;
    private Spinner spComplationYear;
    private Spinner spComplationMonth;
    private CustomButton btnaddupload;
    private ExpandableHeightGridView gridspeciality;
    private CustomEditText edtzipcode;
    SpecialityAdapter specialityAdapter;
    private CustomBoldTextView tvdegree;
    private ExpandableHeightListView listdegree;
    private CustomEditText edtINSTITUTE;
    private CustomEditText edtDEGREES;
    Profile_DegreeListAdpter profileDegreeListAdpter;
    private CustomButton btnaddedu;
    private ExpandableHeightListView listcertification;
    private Spinner tvcertificate;
    private Spinner spEarnedMonth;
    private Spinner spEarnedYear;
    private Spinner spExpireMonth;
    private Spinner spExpireYear;
    private CustomButton btnaddcer;
    List<EducationDetailModel.Data> educationDataList;
    CertificationListAdapter certificationListAdapter;
    Profile_CertificationListAdpter profile_certificationListAdpter;
    Year year;
    Year year_expire_certificaiton;
    Month month;
    String selectedInstitudeId = "";
    MySharedPrefereces mySharedPrefereces;
    Certificates certificates;
    ProgressDialog progressDialog;
    EducationDetailModel educationDetailModel;
    public static Profile_Education_Fragment profile_education_fragment;
    ApplicantCertificate applicantCertificate;
    CheckBox cbCertificationNeverExpire;
    String TAG="Profile_Education_Fragment";

    public static Profile_Education_Fragment newInstance() {
        Profile_Education_Fragment fragment = new Profile_Education_Fragment();
        return fragment;
    }

    Context ctx;
    LicenseAdapter licenseAdapter;

    // String[] expireYr = {"YYYY", "2018", "2019", "2020", "2021"};
    // String[] expireMonth = {"MM", "JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"};

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.propfile_education_fragment, container, false);
        profile_education_fragment=this;
        ctx = getActivity();
        progressDialog = new ProgressDialog(ctx);
        progressDialog.setCancelable(false);
        SpannableString spannableString =  new SpannableString(getString(R.string.please_wait));
        Typeface typefaceSpan = Validations.setTypeface(ctx);
        spannableString.setSpan(typefaceSpan, 0, getString(R.string.please_wait).length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        progressDialog.setMessage(spannableString);
        mySharedPrefereces = new MySharedPrefereces(ctx);
        this.btnaddcer = (CustomButton) view.findViewById(R.id.btn_addcer);
        btnaddcer.setOnClickListener(this);
        this.spExpireYear = (Spinner) view.findViewById(R.id.spExpireYear);
        this.spExpireMonth = (Spinner) view.findViewById(R.id.spExpireMonth);
        this.spEarnedYear = (Spinner) view.findViewById(R.id.spEarnedYear);
        this.spEarnedMonth = (Spinner) view.findViewById(R.id.spEarnedMonth);
        this.tvcertificate = (Spinner) view.findViewById(R.id.tv_certificate);
        this.listcertification = (ExpandableHeightListView) view.findViewById(R.id.list_certification);
        this.btnaddedu = (CustomButton) view.findViewById(R.id.btn_addedu);
        cbCertificationNeverExpire = (CheckBox) view.findViewById(R.id.cbCertificationNeverExpire);
        cbCertificationNeverExpire.setTypeface(Validations.setTypeface(ctx));
        this.btnaddedu.setOnClickListener(this);
        queue = Volley.newRequestQueue(ctx);
        this.scrollview = (ScrollView) view.findViewById(R.id.scrollview);
        this.btnnxt = (CustomButton) view.findViewById(R.id.btn_nxt);
        this.btnback = (CustomButton) view.findViewById(R.id.btn_back);
        this.spComplationYear = (Spinner) view.findViewById(R.id.spComplationYear);
        this.spComplationMonth = (Spinner) view.findViewById(R.id.spComplationMonth);
        this.edtDEGREES = (CustomEditText) view.findViewById(R.id.edt_DEGREES);
        this.edtINSTITUTE = (CustomEditText) view.findViewById(R.id.edt_INSTITUTE);
        this.listdegree = (ExpandableHeightListView) view.findViewById(R.id.list_degree);
        educationDataList = new ArrayList<>();
        profileDegreeListAdpter = new Profile_DegreeListAdpter(ctx, educationDataList, new Profile_DegreeListAdpter.OnGettingEditAndDeleteMethod() {
            @Override
            public void onDeleteButtonClicked(int postion) {
                onDegreeDeleteButtonClicked(educationDataList.get(postion));
                //Toast.makeText(ctx,educationDataList.get(postion).getApplicant_education_institute()+" delete",Toast.LENGTH_LONG).show();
            }

            @Override
            public void onUpdateButtonClicked(int position) {
                onDegreeUpdateButtonClicked(educationDataList.get(position));
                //Toast.makeText(ctx,educationDataList.get(position).getApplicant_education_institute()+" update",Toast.LENGTH_LONG).show();
            }
        },true);
        listdegree.setAdapter(profileDegreeListAdpter);
        this.tvdegree = (CustomBoldTextView) view.findViewById(R.id.tv_degree);


        //profile_certificationListAdpter = new Profile_CertificationListAdpter(ctx);
        //listcertification.setAdapter(profile_certificationListAdpter);
        //  gridspeciality.setExpanded(true);
        listdegree.setExpanded(true);
        listcertification.setExpanded(true);
        //CustomAdapter customAdapter = new CustomAdapter(getActivity(), null, expireYr);
        // spComplationYear.setAdapter(customAdapter);
           /*exp adapter*/
       /* spEarnedYear.setAdapter(customAdapter);
        spExpireYear.setAdapter(customAdapter);

        customAdapter = new CustomAdapter(getActivity(), null, expireMonth);
        spComplationMonth.setAdapter(customAdapter);
        spEarnedMonth.setAdapter(customAdapter);
        spExpireMonth.setAdapter(customAdapter);
*/

        btnback.setOnClickListener(this);
        btnnxt.setOnClickListener(this);
        getCertification();
        getYear();
        getApplicantCertification();
        return view;

    }

    /**
     * Degree update button clicked
     * @param data which data updated button clicked
     */
    public void onDegreeUpdateButtonClicked(EducationDetailModel.Data data) {
        selectedInstitudeId = data.getApplicant_education_id();
        edtINSTITUTE.setText(data.getApplicant_education_institute());
        edtDEGREES.setText(data.getApplicant_education_degree());
        int positionYear = 0;
        int positionMoth = 0;
        for (Year.YearBean yearBean : year.getYear()) {
            if (yearBean.getYear_name().equals(data.getCompletion_year()))
                spComplationYear.setSelection(positionYear);
            positionYear++;
        }
        for (Month.MonthBean monthBean : month.getMonth()) {
            if (monthBean.getMonth_name().equals(data.getCompletion_month())) {
                spComplationMonth.setSelection(positionMoth);
            }
            positionMoth++;
        }

    }

    /**
     * this method is used to delelte degrees
     * @param data which degree is clicked
     */

    public void onDegreeDeleteButtonClicked(final EducationDetailModel.Data data) {
        progressDialog.show();
        String Url = URLS.PROFILE_APPLICATION_EDUCATION_DELETE + "&" + URLS.EDUCATION_ID + "=" + data.getApplicant_education_id() + "&" + URLS.APPLICANT_ID + "=" + mySharedPrefereces.getUserId();
        Log.d(TAG, "onDegreeDeleteButtonClicked: URL:-" + Url);
        StringRequest req = new StringRequest(Request.Method.GET, Url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                Log.d(TAG, "onDegreeDeleteButtonClicked: Response:-" + response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getInt("status") == 1)
                        getEducationDetail();
                    else {
                        DialogUtils.showDialog4Activity(getActivity(), ctx.getResources().getString(R.string.app_name), jsonObject.getString("message"), null);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "onDegreeDeleteButtonClicked: Error:-" + error.getMessage());
                progressDialog.dismiss();
                NetworkResponse networkResponse = error.networkResponse;
                DialogUtils.noInterNetDialog(ctx, "No Internet Connection", "No active internet connection available", new DialogUtils.NoInterNetDailogClickListner() {
                    @Override
                    public void onDialogOkButtonClicked() {
                        onDegreeDeleteButtonClicked(data);
                    }

                    @Override
                    public void onDialogCancelButtonClicked() {

                    }
                });


            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(req);
    }

    /**
     * this method is used to getting Applicant Certification
     */

    public void getApplicantCertification() {
        progressDialog.show();

        String Url = URLS.APPLICANT_CERTIFICATE_LIST + mySharedPrefereces.getUserId();
        Log.d(TAG, "getApplicantCertification: URL:-" + Url);
        StringRequest req = new StringRequest(Request.Method.GET, Url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                Log.d(TAG, "getApplicantCertification: Response:-" + response);
                Gson gson = new Gson();

                applicantCertificate = gson.fromJson(response, ApplicantCertificate.class);

                if (applicantCertificate != null) {
                    profile_certificationListAdpter = new Profile_CertificationListAdpter(ctx, applicantCertificate, new Profile_CertificationListAdpter.OnDeleteListener() {
                        @Override
                        public void onDeleteButtonClicked(int position) {
                            onCertificationDelete(applicantCertificate.getData().get(position));
                        }
                    },true);
                    listcertification.setAdapter(profile_certificationListAdpter);
                } else {
                    // DialogUtils.showDialog4Activity(LoginActivity.this, ctx.getResources().getString(R.string.app_name), logindata.getMessage() + "");

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Log.d(TAG, "getApplicantCertification: Error:-" + error.getMessage());
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    int statusCode = error.networkResponse.statusCode;
                    System.out.println("STATUS CODE " + statusCode + "");
                    DialogUtils.showDialog4Activity(getActivity(), ctx.getResources().getString(R.string.app_name), statusCode + "", null);
                }

            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(req);

    }

    /**
     * this method is used to delete certification
     * @param data which item remove
     */

    public void onCertificationDelete(final ApplicantCertificate.Data data) {

        progressDialog.show();
        String Url = URLS.PROFILE_APPLICATION_CERTIFICATE_DELETE + "&" + URLS.CERTIFICATE_ID + "=" + data.getApplicant_certifications_id() + "&" + URLS.APPLICANT_ID + "=" + mySharedPrefereces.getUserId();
        Log.d(TAG, "onCertificationDelete: URL:-"+Url);
        StringRequest req = new StringRequest(Request.Method.GET, Url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                Log.d(TAG, "onCertificationDelete: Response:-" + response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getInt("status") == 1)
                        getApplicantCertification();
                    else {
                        DialogUtils.showDialog4Activity(getActivity(), ctx.getResources().getString(R.string.app_name), jsonObject.getString("message"), null);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "onCertificationDelete: Error:-" + error.getMessage());
                progressDialog.dismiss();
                DialogUtils.noInterNetDialog(ctx, "No Internet Connection", "No active internet connection available", new DialogUtils.NoInterNetDailogClickListner() {
                    @Override
                    public void onDialogOkButtonClicked() {
                        onCertificationDelete(data);
                    }

                    @Override
                    public void onDialogCancelButtonClicked() {

                    }
                });


            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(req);

    }

    /**
     * this method is used to getting Certification list
     */
    private void getCertification() {
        progressDialog.show();
        String Url = URLS.CERTIFICATION_URL;
        Log.d(TAG, "getCertification: URL" + Url);
        StringRequest req = new StringRequest(Request.Method.GET, Url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "getCertification: Response" + response);
                Gson gson = new Gson();
                progressDialog.dismiss();
                certificates = gson.fromJson(response, Certificates.class);

                if (certificates != null) {
                    Certificates.CertificationBean certificationBean=new Certificates.CertificationBean();
                    certificationBean.setCertification_id("0");
                    certificationBean.setCertification_name("Select Additional Certification");
                    certificates.getCertification().set(0, certificationBean);
                    certificationListAdapter = new CertificationListAdapter(certificates, ctx);
                    tvcertificate.setAdapter(certificationListAdapter);
                } else {
                    // DialogUtils.showDialog4Activity(LoginActivity.this, ctx.getResources().getString(R.string.app_name), logindata.getMessage() + "");

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "getCertification: Error" + error.getMessage());
                progressDialog.dismiss();


            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(req);
    }

    /**
     * this method is used to getting Applicant education Detail
     */

    private void getEducationDetail() {
        progressDialog.show();
        educationDataList.clear();
        String Url = URLS.PROFILE_APPLICANT_EDUCATION_DETAIL_URL + mySharedPrefereces.getUserId();
        Log.d(TAG, "getEducationDetail: URL" + Url);
        StringRequest req = new StringRequest(Request.Method.GET, Url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                Log.d(TAG, "getEducationDetail:Response:" + response);
                Gson gson = new Gson();

                educationDetailModel = gson.fromJson(response, EducationDetailModel.class);
                if (educationDetailModel != null) {

                    if (educationDetailModel.getDataList() != null) {
                        educationDataList.addAll(educationDetailModel.getDataList());
                        profileDegreeListAdpter.notifyDataSetChanged();
                    } else {
                        System.out.println("data list is null " + response + "");
                    }
                } else {
                    System.out.println("Education model null " + response + "");
                }
//


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Log.d(TAG, "getEducationDetail:Error:" + error.getMessage());
                NetworkResponse networkResponse = error.networkResponse;


            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(req);
    }

    /**
     * this method is used to getting year data from server;
     */

    private void getYear() {
        getExpireYear();
        progressDialog.show();
        String Url = URLS.YEAR_URL;
        Log.d(TAG, "getYear: URL" + Url);
        StringRequest req = new StringRequest(Request.Method.GET, Url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "getYear: Response" + response);
                Gson gson = new Gson();
                progressDialog.dismiss();
                year = gson.fromJson(response, Year.class);
                Year.YearBean yearBean = new Year.YearBean();
                yearBean.setYear_id("0");
                yearBean.setYear_name("Year");
                year.getYear().add(0, yearBean);
                CustomYearAdapter customAdapter = new CustomYearAdapter(getActivity(), null, year);
                spComplationYear.setAdapter(customAdapter);
                spEarnedYear.setAdapter(customAdapter);
                //spExpireYear.setAdapter(customAdapter);
//                Type collectionType = new TypeToken<Collection<Year>>() {
//                }.getType();
//                Collection<Year> year = gson.fromJson(response, collectionType);
                if (year != null) {

                    CallMonth();
                    // getLicense();
                } else {
                    // DialogUtils.showDialog4Activity(LoginActivity.this, ctx.getResources().getString(R.string.app_name), logindata.getMessage() + "");

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "getYear: Error" + error.getMessage());
                progressDialog.dismiss();
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    int statusCode = error.networkResponse.statusCode;
                    System.out.println("STATUS CODE " + statusCode + "");
                    DialogUtils.showDialog4Activity(getActivity(), ctx.getResources().getString(R.string.app_name), statusCode + "", null);
                }

            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(req);
    }

    /**
     * this method getting Expire Year data From Server
     */

    private void getExpireYear() {
        progressDialog.show();
        String Url = URLS.YEAR_EXP_CER_URL;
        Log.d(TAG, "getExpireYear: URL" + Url);
        StringRequest req = new StringRequest(Request.Method.GET, Url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "getExpireYear: Response" + response);
                Gson gson = new Gson();
                progressDialog.dismiss();
                year_expire_certificaiton = gson.fromJson(response, Year.class);
                Year.YearBean yearBean = new Year.YearBean();
                yearBean.setYear_exp_cer_id("0");
                yearBean.setYear_exp_cer_name("Year");
                year_expire_certificaiton.getYear_exp_cer().add(0, yearBean);
                CustomForCertiYearAdapter customAdapter = new CustomForCertiYearAdapter(getActivity(), null, year_expire_certificaiton);
                //spComplationYear.setAdapter(customAdapter);
                //spEarnedYear.setAdapter(customAdapter);
                spExpireYear.setAdapter(customAdapter);
//                Type collectionType = new TypeToken<Collection<Year>>() {
//                }.getType();
//                Collection<Year> year = gson.fromJson(response, collectionType);
                if (year != null) {

                    //CallMonth();
                    // getLicense();
                } else {
                    // DialogUtils.showDialog4Activity(LoginActivity.this, ctx.getResources().getString(R.string.app_name), logindata.getMessage() + "");

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "getExpireYear: Error" + error.getMessage());
                progressDialog.dismiss();
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    int statusCode = error.networkResponse.statusCode;
                    System.out.println("STATUS CODE " + statusCode + "");
                    DialogUtils.showDialog4Activity(getActivity(), ctx.getResources().getString(R.string.app_name), statusCode + "", null);
                }

            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(req);
    }

    /**
     * this method is used to getting month data
     */

    private void CallMonth() {
        String Url = URLS.MONTH_URL;
        progressDialog.show();
        Log.d(TAG, "CallMonth: URL" + Url);
        StringRequest req = new StringRequest(Request.Method.GET, Url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                System.out.println("CallMonth RESPONSE " + response + "");
                Log.d(TAG, "CallMonth: Response" + response);
                Gson gson = new Gson();
                progressDialog.dismiss();
                month = gson.fromJson(response, Month.class);
                Month.MonthBean monthBean = new Month.MonthBean();
                monthBean.setMonth_id("0");
                monthBean.setMonth_name("Month");
                monthBean.setMonth_number("0");
                month.getMonth().add(0, monthBean);
                MonthCustomAdapter customAdapter = new MonthCustomAdapter(getActivity(), null, month.getMonth());
                spComplationMonth.setAdapter(customAdapter);
                spEarnedMonth.setAdapter(customAdapter);
                spExpireMonth.setAdapter(customAdapter);

//                Type collectionType = new TypeToken<Collection<Month>>() {
//                }.getType();
//                Collection<Month> month = gson.fromJson(response, collectionType);
                if (month != null) {
                    //getYear();
                } else {
                    // DialogUtils.showDialog4Activity(LoginActivity.this, ctx.getResources().getString(R.string.app_name), logindata.getMessage() + "");

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "CallMonth: Error" + error.getMessage());
                progressDialog.dismiss();
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    int statusCode = error.networkResponse.statusCode;
                    System.out.println("STATUS CODE " + statusCode + "");
                    DialogUtils.showDialog4Activity(getActivity(), ctx.getResources().getString(R.string.app_name), statusCode + "", null);
                }

            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(req);
        getEducationDetail();

    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            /*24 july Pragna*/
            case R.id.btn_back:
//                ProfileFragment.viewPager.setCurrentItem(0);
                ProfileFragment.viewPager.setCurrentItem(1);
                break;

            case R.id.btn_nxt:
//                ProfileFragment.viewPager.setCurrentItem(2);
                ProfileFragment.viewPager.setCurrentItem(3);
                break;
            case R.id.btn_addedu:
                onEducationAddButtonClicked();
                break;
            case R.id.btn_addcer:
                onAddCertificationButtonClicked();
                break;

        }
    }

    /**
     * this method called when user clicked on addCertification button
     */

    public void onAddCertificationButtonClicked() {
        int earnMonthPosition = spEarnedMonth.getSelectedItemPosition();
        int earnYearPosition = spEarnedYear.getSelectedItemPosition();
        int expireMonthPosition = spExpireMonth.getSelectedItemPosition();
        int expireYearPosition = spExpireYear.getSelectedItemPosition();
        int selectedCertificationPosition=tvcertificate.getSelectedItemPosition();
        if(selectedCertificationPosition==0)
        {
            Toast.makeText(ctx, "Please Select Certification", Toast.LENGTH_LONG).show();
        }
        else if (earnMonthPosition == 0) {
            Toast.makeText(ctx, "Please Select Earned Month", Toast.LENGTH_LONG).show();
        } else if (earnYearPosition == 0) {
            Toast.makeText(ctx, "Please Select Earned Year", Toast.LENGTH_LONG).show();
        } else if (expireMonthPosition == 0 && !cbCertificationNeverExpire.isChecked()) {
            Toast.makeText(ctx, "Please Select Expire Month", Toast.LENGTH_LONG).show();
        } else if (expireYearPosition == 0 && !cbCertificationNeverExpire.isChecked()) {
            Toast.makeText(ctx, "Please Select Expire Year", Toast.LENGTH_LONG).show();
        } else {
            String selected_certification_id = certificates.getCertification().get(tvcertificate.getSelectedItemPosition()).getCertification_id();
            //String selected_certification_id=certificates.getCertification().get(tvcertificate.getSelectedItemPosition()).getCertification_id();
            if(cbCertificationNeverExpire.isChecked()) {
                GetManageApplicantCertificates(mySharedPrefereces.getUserId(), "",selected_certification_id, month.getMonth().get(spEarnedMonth.getSelectedItemPosition()).getMonth_id(), year.getYear().get(spEarnedYear.getSelectedItemPosition()).getYear_id(), month.getMonth().get(spExpireMonth.getSelectedItemPosition()).getMonth_id(), year_expire_certificaiton.getYear_exp_cer().get(spExpireYear.getSelectedItemPosition()).getYear_exp_cer_id(), "1");
            }else
            {
                GetManageApplicantCertificates(mySharedPrefereces.getUserId(),"", selected_certification_id, month.getMonth().get(spEarnedMonth.getSelectedItemPosition()).getMonth_id(), year.getYear().get(spEarnedYear.getSelectedItemPosition()).getYear_id(), month.getMonth().get(spExpireMonth.getSelectedItemPosition()).getMonth_id(), year_expire_certificaiton.getYear_exp_cer().get(spExpireYear.getSelectedItemPosition()).getYear_exp_cer_id(), "");
            }
        }
    }

    /**
     * this method is uset to insert certificates
     * @param APPLICATEI_ID login applicant id
     * @param CERTIFICATE_ID if you want to update it pass certification id
     * @param EARNED_MONTH which month earned cerficate
     * @param EARNED_YEAR which year earned cerficates
     * @param EXPIRE_MONTH which moth expired certificate
     * @param EXPIRE_YEAR which year expired certificate
     * @param NEVEREXPIRE is Certificate never expire
     */

    private void GetManageApplicantCertificates(final String APPLICATEI_ID, final String CERTIFICATE_ID, final String CERTIFICATE, final String EARNED_MONTH, final String EARNED_YEAR, final String EXPIRE_MONTH, final String EXPIRE_YEAR, final String NEVEREXPIRE) {
        progressDialog.show();
        final String Url = URLS.ManageApplicantCertificates + APPLICATEI_ID + "&"
                + URLS.CERTIFICATE_ID + "=" + CERTIFICATE_ID + "&"
                + URLS.CERTIFICATE + "=" + CERTIFICATE + "&"
                + URLS.EARNED_MONTH + "=" + EARNED_MONTH + "&"
                + URLS.EARNED_YEAR + "=" + EARNED_YEAR + "&"
                + URLS.EXPIRE_MONTH + "=" + EXPIRE_MONTH + "&"
                + URLS.EXPIRE_YEAR + "=" + EXPIRE_YEAR + "&"
                + URLS.NEVEREXPIRE + "=" + NEVEREXPIRE + "";
        Log.d(TAG, "GetManageApplicantCertificates: URL:- "+Url);
        StringRequest putRequest = new StringRequest(Request.Method.POST, Url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d(TAG, "GetManageApplicantCertificates: URL:- "+response);
                        progressDialog.dismiss();

                        System.out.println("API$$$$$ GetManageApplicantCertificates response " + response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int status = jsonObject.getInt("status");
                            String message = jsonObject.getString("message");
                            if (status == 1) {
                                DialogUtils.showDialog4Activity(getActivity(), ctx.getResources().getString(R.string.app_name), message, null);
                                getApplicantCertification();
                                tvcertificate.setSelection(0);
                                spEarnedMonth.setSelection(0);
                                spEarnedYear.setSelection(0);
                                spExpireMonth.setSelection(0);
                                spExpireYear.setSelection(0);
                            } else {
                                DialogUtils.showDialog4Activity(getActivity(), ctx.getResources().getString(R.string.app_name), message, null);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error
                        progressDialog.dismiss();
                        Log.d(TAG, "GetManageApplicantCertificates: URL:- " + error.getMessage());
                        NetworkResponse networkResponse = error.networkResponse;
                        DialogUtils.noInterNetDialog(ctx, "No Internet Connection", "No active internet connection available", new DialogUtils.NoInterNetDailogClickListner() {
                            @Override
                            public void onDialogOkButtonClicked() {
                                GetManageApplicantCertificates(APPLICATEI_ID,CERTIFICATE_ID,CERTIFICATE,EARNED_MONTH,EARNED_YEAR,EXPIRE_MONTH,EXPIRE_YEAR,NEVEREXPIRE);
                            }

                            @Override
                            public void onDialogCancelButtonClicked() {

                            }
                        });

                    }
                }
        );
        putRequest.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(putRequest);

    }


    /**
     * this method is used when use clicked Add Education button clicked
     * this method checking validation
     */
    public void onEducationAddButtonClicked() {
        String institude = edtINSTITUTE.getText().toString();
        String degree = edtDEGREES.getText().toString();
        // Month.MonthBean month = (Month.MonthBean) spComplationMonth.getSelectedItem();
        String monthId = month.getMonth().get(spComplationMonth.getSelectedItemPosition()).getMonth_id();
        String yearId = year.getYear().get(spComplationYear.getSelectedItemPosition()).getYear_id();
        if (spComplationMonth.getSelectedItemPosition() == 0) {

            Toast.makeText(ctx,"Please Select Complation Month",Toast.LENGTH_LONG).show();

        } else if (spComplationYear.getSelectedItemPosition() == 0) {
            Toast.makeText(ctx,"Please Select Complation Year",Toast.LENGTH_LONG).show();
        } else if (institude.trim().length() <= 0) {
            Toast.makeText(ctx, "Please Enter Institude Name", Toast.LENGTH_SHORT).show();
        } else if (degree.trim().length() <= 0) {
            Toast.makeText(ctx, "Please Enter Degree Name", Toast.LENGTH_SHORT).show();
        } else {
            educationDetailSendToServer(institude, degree, yearId, monthId, selectedInstitudeId);
        }


    }

    /***
     * this mehtod education detail to sever
     * @param institude user entered institude name
     * @param degreeName use enter degree name
     * @param yearid which year
     * @param monthId which month
     * @param institudeId institude id
     */
    public void educationDetailSendToServer(final String institude, final String degreeName, final String yearid, final String monthId, final String institudeId) {

        String Url = URLS.PROFILE_APPLICATION_EDUCATION_INSERT + "&" + URLS.INSTITUDE + "=" + institude + "&" + URLS.DEGREE + "=" + degreeName + "&" + URLS.YEAR + "=" + yearid + "&" + URLS.MONTH + "=" + monthId + "&" + URLS.APPLICANT_ID + "=" + mySharedPrefereces.getUserId() + "&" + URLS.EDUCATION_ID + "=" + institudeId;
        URL url = null;
        try {
            url = new URL(Url);
            URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef());
            url = uri.toURL();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        Log.d(TAG, "educationDetailSendToServer: URL" + url.toString());
        StringRequest req = new StringRequest(Request.Method.GET, url.toString(), new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "educationDetailSendToServer: Response" + response);
                System.out.println("CallMonth RESPONSE " + response + "");
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getInt("status") == 1) {
                        edtDEGREES.setText("");
                        edtINSTITUTE.setText("");
                        spComplationYear.setSelection(0);
                        spComplationMonth.setSelection(0);
                        selectedInstitudeId = "";
                        getEducationDetail();
                        DialogUtils.showDialog4Activity(getActivity(), ctx.getResources().getString(R.string.app_name), jsonObject.getString("message"), null);
                    } else {
                        DialogUtils.showDialog4Activity(getActivity(), ctx.getResources().getString(R.string.app_name), jsonObject.getString("message"), null);
                    }
                } catch (JSONException e) {


                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.d(TAG, "educationDetailSendToServer: Error" + error.getMessage());
                DialogUtils.noInterNetDialog(ctx, "No Internet Connection", "No active internet connection available", new DialogUtils.NoInterNetDailogClickListner() {
                    @Override
                    public void onDialogOkButtonClicked() {
                        educationDetailSendToServer(institude,degreeName,yearid,monthId,institudeId);
                    }

                    @Override
                    public void onDialogCancelButtonClicked() {

                    }
                });


            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(req);
    }
}
