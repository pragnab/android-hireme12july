package com.hiremehealthcare.fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.SpannableString;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ListView;

import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.hiremehealthcare.activity.ManagerChatDetailActivity;
import com.hiremehealthcare.CommonCls.URLS;
import com.hiremehealthcare.CommonCls.Validations;
import com.hiremehealthcare.POJO.CommunicationModel;
import com.hiremehealthcare.POJO.SheduleInterViewModel;
import com.hiremehealthcare.activity.ContactCandidate;
import com.hiremehealthcare.R;
import com.hiremehealthcare.Utils.CommonRequestHelper;
import com.hiremehealthcare.adapter.SheduleInterViewAdapter;
import com.hiremehealthcare.database.MySharedPrefereces;

import java.util.ArrayList;
import java.util.List;


/**
 * A simple {@link Fragment} subclass.
 */
public class UpComingScheduleInterviewsFragment extends Fragment {

    ProgressDialog progressDialog;
    ListView ivInterviewSheduleUpComing;
    List<SheduleInterViewModel.Datum> sheduleInterviewData;
    SheduleInterViewAdapter sheduleInterViewAdapter;
    Context context;
    ImageView ivNoDataFound;
    String TAG = "UpComingScheduleInterviewsFragment";
    MySharedPrefereces mySharedPrefereces;

    public UpComingScheduleInterviewsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView=inflater.inflate(R.layout.fragment_up_coming_schedule_interviews, container, false);
        context=getContext();
        initAllCotrols(rootView);
        return rootView;
    }

    public void initAllCotrols(View rootView)
    {
        sheduleInterviewData = new ArrayList<>();
        progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        mySharedPrefereces=new MySharedPrefereces(getContext());
        SpannableString spannableString =  new SpannableString(getString(R.string.please_wait));
        Typeface typefaceSpan = Validations.setTypeface(context);
        spannableString.setSpan(typefaceSpan, 0, getString(R.string.please_wait).length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        progressDialog.setMessage(spannableString);
        ivInterviewSheduleUpComing = (ListView) rootView.findViewById(R.id.ivInterviewSheduleUpComing);
        ivNoDataFound = (ImageView) rootView.findViewById(R.id.ivNoDataFound);
        sheduleInterViewAdapter=new SheduleInterViewAdapter(context, sheduleInterviewData, new SheduleInterViewAdapter.OnPandingAdapterRemoveOrFavoriteButtonClicked() {


            @Override
            public void onChatButtonClicked(SheduleInterViewModel.Datum data) {
                onItemChatButtonClicked( data);
            }

            @Override
            public void onCandidateDetailButtonClicked(SheduleInterViewModel.Datum data) {
                onDetailButtonClicked(data);
            }

            @Override
            public void onListItemClicked(SheduleInterViewModel.Datum data) {

            }
        });
        ivInterviewSheduleUpComing.setAdapter(sheduleInterViewAdapter);
        getSheduledInterviewUpcomming();

    }

    /**
     * This method called when use clicked on chat button in list
     * @param data
     */
    public void onItemChatButtonClicked(SheduleInterViewModel.Datum data)
    {
        CommunicationModel.Datum communicationdata=new CommunicationModel.Datum();
        communicationdata.setApplicantId(data.getApplicantId());
        communicationdata.setId(data.getApplicationId());
        communicationdata.setImage(data.getImage());
        communicationdata.setFirstname(data.getFirstname());
        communicationdata.setLastname(data.getLastname());
        Intent intent = new Intent(context, ManagerChatDetailActivity.class);
        intent.putExtra("data", communicationdata);
        startActivity(intent);
    }

    /**
     * this method is called when user clicked on detail button clicked
     * @param data
     */
    public void onDetailButtonClicked(SheduleInterViewModel.Datum data)
    {
        Intent i = new Intent(context,
                ContactCandidate.class);

        i.putExtra("ID", data.getApplicantId()+"");
        i.putExtra("IMAGE", data.getImage()+"");
        context.startActivity(i);
    }

    /**
     * this method is used to getting upcomming shedule interviews data
     */
    public void getSheduledInterviewUpcomming()
    {
        progressDialog.show();
        String Url= URLS.SCHEDULED_INTERVIEWS_UPCOMING+"&"+URLS.MANAGER_ID+"="+mySharedPrefereces.getUserId();
        Log.d(TAG, "getSheduledInterviewUpcomming: URL:- "+Url);
        CommonRequestHelper.getDataFromUrl(Url, new CommonRequestHelper.VolleyResponseCustom() {
            @Override
            public void getServerResponse(String response) {
                progressDialog.dismiss();
                Log.d(TAG, "getSheduledInterviewUpcomming: Response:- "+response);
                Gson gson = new Gson();
                SheduleInterViewModel sheduleInterViewModel = gson.fromJson(response, SheduleInterViewModel.class);
                if(sheduleInterViewModel!=null)
                {
                    if(sheduleInterViewModel.getData().size()>0) {
                        sheduleInterviewData.addAll(sheduleInterViewModel.getData());
                        sheduleInterViewAdapter.notifyDataSetChanged();
                        ivNoDataFound.setVisibility(View.GONE);
                    }else
                    {
                        ivNoDataFound.setVisibility(View.VISIBLE);
                    }
                }
            }

            @Override
            public void getVolleyError(VolleyError volleyError) {
                Log.d(TAG, "getSheduledInterviewUpcomming: Error:- "+volleyError.getMessage());

                progressDialog.dismiss();
            }
        },context);

    }

}
