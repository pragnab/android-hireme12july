package com.hiremehealthcare.fragment;


import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hiremehealthcare.CommonCls.CustomButton;
import com.hiremehealthcare.activity.PostNewPositionActivity;
import com.hiremehealthcare.R;
import com.hiremehealthcare.adapter.ViewPagerAdapter;

/**
 * A simple {@link Fragment} subclass.
 * this class show active or expire position tab in Manager Login
 */
public class DashBordManagerFragment extends Fragment {


    TabLayout tablayout;
    ViewPager viewpager;
    CustomButton btnPostNewPosition;
    Context context;
    public DashBordManagerFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView= inflater.inflate(R.layout.fragment_dash_bord_manager, container, false);
        context=getContext();
        initAllControls(rootView);
        return rootView;
    }

    public void initAllControls(View rootView)
    {
        tablayout = (TabLayout) rootView.findViewById(R.id.tablayout);
        viewpager = (ViewPager) rootView.findViewById(R.id.viewpager);
        btnPostNewPosition = (CustomButton) rootView.findViewById(R.id.btnPostNewPosition);
        btnPostNewPosition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                startActivityForResult(new Intent(context, PostNewPositionActivity.class),05);
            }
        });
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new ActivePostionFragment(), "Active");
        adapter.addFragment(new ExpiredPositionFragment(), "Expired");
        viewpager.setAdapter(adapter);
        tablayout.setupWithViewPager(viewpager );


    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode==05)
        {
            ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
            adapter.addFragment(new ActivePostionFragment(), "Active");
            adapter.addFragment(new ExpiredPositionFragment(), "Expired");
            viewpager.setAdapter(adapter);
            tablayout.setupWithViewPager(viewpager );
        }
    }
}
