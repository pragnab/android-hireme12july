package com.hiremehealthcare.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.hiremehealthcare.CommonCls.CustomBoldTextView;
import com.hiremehealthcare.CommonCls.CustomButton;
import com.hiremehealthcare.CommonCls.CustomEditText;
import com.hiremehealthcare.CommonCls.CustomTextView;
import com.hiremehealthcare.CommonCls.DialogUtils;
import com.hiremehealthcare.CommonCls.ExpandableHeightGridView;
import com.hiremehealthcare.CommonCls.URLS;
import com.hiremehealthcare.CommonCls.Validations;
import com.hiremehealthcare.POJO.ActivePositionModel;
import com.hiremehealthcare.POJO.Benefits;
import com.hiremehealthcare.POJO.Certificates;
import com.hiremehealthcare.POJO.ExpirePositionModel;
import com.hiremehealthcare.POJO.GetManagerFacilityDetailModel;
import com.hiremehealthcare.POJO.Hours;
import com.hiremehealthcare.POJO.License;
import com.hiremehealthcare.POJO.Shift;
import com.hiremehealthcare.POJO.Speciality;
import com.hiremehealthcare.R;
import com.hiremehealthcare.Utils.CommonRequestHelper;
import com.hiremehealthcare.adapter.BenefitsAdapter;
import com.hiremehealthcare.adapter.CertificationAdapter;
import com.hiremehealthcare.adapter.CertificationMultiSelectAdapter;
import com.hiremehealthcare.adapter.CustomBenefitsAdapter;
import com.hiremehealthcare.adapter.DaysMultiSelectAdapter;
import com.hiremehealthcare.adapter.DaysMultiSelectAdapter_duplicate_flip;
import com.hiremehealthcare.adapter.Days_CheckBoxAdapter;
import com.hiremehealthcare.adapter.Days_adapter;
import com.hiremehealthcare.adapter.Evening_CheckBoxAdapter;
import com.hiremehealthcare.adapter.Evening_adapter;
import com.hiremehealthcare.adapter.HoursAdapter;
import com.hiremehealthcare.adapter.LicenseAdapter;
import com.hiremehealthcare.adapter.Night_CheckBoxAdapter;
import com.hiremehealthcare.adapter.Night_adapter;
import com.hiremehealthcare.adapter.ShiftAdapter;
import com.hiremehealthcare.adapter.SpecialityAdapter;
import com.hiremehealthcare.adapter.SpinnerAdapter;
import com.hiremehealthcare.database.MySharedPrefereces;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.regex.Pattern;

public class EditExpiredPositionFragment extends Fragment implements View.OnClickListener {
    Benefits days_all;
    //    public static int DAYS_EVENING_NIGHT = 0;
//    public static int HOURS_FULL_PART_PRN_WEEKENDS = 0;
    private CustomTextView tvfacility;
    private CustomBoldTextView tvcertification;
    private ExpandableHeightGridView gridcertification;
    private ExpandableHeightGridView grid_benefits_custom;
    private CustomBoldTextView tvlicense;
    private ExpandableHeightGridView gridlicence;
    private CustomBoldTextView tvspeciality;
    private ExpandableHeightGridView gridspeciality;
    private CustomBoldTextView tvTIME;
    private CustomBoldTextView tvShift;
    private ExpandableHeightGridView gridtime;
    private CustomBoldTextView tvShifthour;
    private ExpandableHeightGridView gridhr;
    private ScrollView scrollview;
    RequestQueue queue;
    private ExpandableHeightGridView gridbenefitscustom;
    private ExpandableHeightGridView griddays;
    private ExpandableHeightGridView griddaystime;
    Context ctx;
    HoursAdapter hoursAdapter;
    BenefitsAdapter benefitadapter;
    //BenefitsAdapter customBenifitsAdapter;
    ShiftAdapter shiftAdapter;
    CertificationMultiSelectAdapter certificationAdapter;
    LicenseAdapter licenseAdapter;
    SpecialityAdapter specialityAdapter;
    private CustomBoldTextView tvbenefits;
    private ExpandableHeightGridView gridbenefits;
    private CustomEditText edtbenefit;
    private Spinner spExpYear;
    private Spinner spExpMonth;
    private CustomEditText edtdesc;
    List<String> monthList;
    List<String> yearList;
    CustomButton btnSubmitBack, btnSubmitPriview,btnDeletepostion;
    MySharedPrefereces mySharedPrefereces;
    List<String> customLayout;
    CustomBenefitsAdapter customBenefitsAdapter;
    CustomButton btnAddMoreBenifits;
    ProgressDialog progressDialog;
    String TAG = "EditPostion";

    ExpirePositionModel.Datum data;

    ArrayList<String> days;
    DaysMultiSelectAdapter _dayDaysMultiSelectAdapter;
    private ExpandableHeightGridView grideveningdaystime;
    private ExpandableHeightGridView gridnightdaystime;
    ArrayList<String> selected_local = new ArrayList<>();
    ArrayList<String> selected_name_local = new ArrayList<>();
//    public static Days_CheckBoxAdapter _daDays_checkBoxAdapter;
//    public static Night_CheckBoxAdapter _daNightcheckBoxAdapter;
//    public static Evening_CheckBoxAdapter _daevening_checkBoxAdapter;
public static Days_adapter _daDays_checkBoxAdapter;
    public static Night_adapter _daNightcheckBoxAdapter;
    public static Evening_adapter _daevening_checkBoxAdapter;

    public EditExpiredPositionFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        //  View view = inflater.inflate(R.layout.fragment_edit_expired_position, container, false);
        View view = inflater.inflate(R.layout.fragment_edit_postion, container, false);
        if (getArguments() != null) {
            data = (ExpirePositionModel.Datum) getArguments().getSerializable("data");
        }

        ctx = getContext();
        System.out.println("this is data to get MONDAY^^^ " + data.getMon() + "");
//        String availability = "";
//
//        if (data.getMon().contains("1")) {
//            //availability = availability + " , " + nearPositionDetailModel.getData().getMon();
//            selected_local.add("1");
//            selected_name_local.add("Mon");
//            availability = availability + " , " + "Mon";
//
//        }
//        if (data.getTue().contains("1")) {
//            // availability = availability + " , " + nearPositionDetailModel.getData().getTue();
//            availability = availability + " , " + "Tue";
//            selected_local.add("2");
//            selected_name_local.add("Tue");
//        }
//        if (data.getWed().contains("1")) {
//            // availability = availability + " , " + nearPositionDetailModel.getData().getWed();
//            availability = availability + " , " + "Wed";
//            selected_local.add("3");
//            selected_name_local.add("Wed");
//        }
//        if (data.getThu().contains("1")) {
//            // availability = availability + " , " + nearPositionDetailModel.getData().getThu();
//            availability = availability + " , " + "Thu";
//            selected_local.add("4");
//            selected_name_local.add("Thu");
//
//        }
//        if (data.getFri().contains("1")) {
//            //     availability = availability + " , " + nearPositionDetailModel.getData().getFri();
//            availability = availability + " , " + "Fri";
//            selected_local.add("5");
//            selected_name_local.add("Fri");
//
//        }
//        if (data.getSat().contains("1")) {
//            //  availability = availability + " , " + nearPositionDetailModel.getData().getSat();
//            availability = availability + " , " + "Sat";
//            selected_local.add("6");
//            selected_name_local.add("Sat");
//
//        }
//        if (data.getSun().contains("1")) {
//            // availability = availability + " , " + nearPositionDetailModel.getData().getSun();
//            availability = availability + " , " + "Sun";
//            selected_local.add("7");
//            selected_name_local.add("Sun");
//        }
//        if (availability == null || availability.contentEquals("")) {
//            availability = "N/A";
//        }
//
//
//        if (Days_CheckBoxAdapter.selected != null) {
//            Days_CheckBoxAdapter.selected = new ArrayList<>();
//        }
//        if (Days_CheckBoxAdapter.selected_name != null) {
//            Days_CheckBoxAdapter.selected_name = new ArrayList<>();
//        }
//        if (Evening_CheckBoxAdapter.selected != null) {
//            Evening_CheckBoxAdapter.selected = new ArrayList<>();
//        }
//        if (Evening_CheckBoxAdapter.selected_name != null) {
//            Evening_CheckBoxAdapter.selected_name = new ArrayList<>();
//        }
//        if (Night_CheckBoxAdapter.selected != null) {
//            Night_CheckBoxAdapter.selected = new ArrayList<>();
//        }
//        if (Night_CheckBoxAdapter.selected_name != null) {
//            Night_CheckBoxAdapter.selected_name = new ArrayList<>();
//        }


        this.edtdesc = (CustomEditText) view.findViewById(R.id.edt_desc);
        btnSubmitBack = (CustomButton) view.findViewById(R.id.btnSubmitBack);
        btnSubmitPriview = (CustomButton) view.findViewById(R.id.btnSubmitPriview);
        btnDeletepostion = (CustomButton) view.findViewById(R.id.btnDeletepostion);

        btnSubmitPriview.setOnClickListener(this);
        btnSubmitBack.setOnClickListener(this);
        btnDeletepostion.setOnClickListener(this);
        this.spExpMonth = (Spinner) view.findViewById(R.id.spExpMonth);
        this.spExpYear = (Spinner) view.findViewById(R.id.spExpYear);
        btnAddMoreBenifits = (CustomButton) view.findViewById(R.id.btnAddMoreBenifits);
        btnAddMoreBenifits.setOnClickListener(this);
        this.edtbenefit = (CustomEditText) view.findViewById(R.id.edt_benefit);
        this.gridbenefits = (ExpandableHeightGridView) view.findViewById(R.id.grid_benefits);
        grid_benefits_custom = (ExpandableHeightGridView) view.findViewById(R.id.grid_benefits_custom);
        this.tvbenefits = (CustomBoldTextView) view.findViewById(R.id.tv_benefits);
        this.scrollview = (ScrollView) view.findViewById(R.id.scrollview);
        this.gridhr = (ExpandableHeightGridView) view.findViewById(R.id.grid_hr);
        this.tvShifthour = (CustomBoldTextView) view.findViewById(R.id.tv_Shifthour);
        this.gridtime = (ExpandableHeightGridView) view.findViewById(R.id.grid_time);
        this.tvShift = (CustomBoldTextView) view.findViewById(R.id.tv_Shift);
        this.tvTIME = (CustomBoldTextView) view.findViewById(R.id.tv_TIME);
        this.gridspeciality = (ExpandableHeightGridView) view.findViewById(R.id.grid_speciality);
        this.tvspeciality = (CustomBoldTextView) view.findViewById(R.id.tv_speciality);
        this.gridlicence = (ExpandableHeightGridView) view.findViewById(R.id.grid_licence);
        this.tvlicense = (CustomBoldTextView) view.findViewById(R.id.tv_license);
        this.gridcertification = (ExpandableHeightGridView) view.findViewById(R.id.grid_certification);
        this.tvcertification = (CustomBoldTextView) view.findViewById(R.id.tv_certification);
        this.tvfacility = (CustomTextView) view.findViewById(R.id.tv_facility);



        /*4-feb-19 Pragna*/

        this.griddaystime = (ExpandableHeightGridView) view.findViewById(R.id.grid_days_time);
        griddaystime.setExpanded(true);
        this.griddays = (ExpandableHeightGridView) view.findViewById(R.id.grid_days);
       // this.tvNights = (TextView) view.findViewById(R.id.tv_Nights);
      //  this.tvEvening = (TextView) view.findViewById(R.id.tv_Evening);
      //  this.tvDays = (TextView) view.findViewById(R.id.tv_Days);
      //  this.tvavailability = (TextView) view.findViewById(R.id.tv_availability);
        this.gridnightdaystime = (ExpandableHeightGridView) view.findViewById(R.id.grid_night_days_time);
        this.grideveningdaystime = (ExpandableHeightGridView) view.findViewById(R.id.grid_evening_days_time);
        this.gridbenefitscustom = (ExpandableHeightGridView) view.findViewById(R.id.grid_benefits_custom);

        gridnightdaystime.setExpanded(true);
        grideveningdaystime.setExpanded(true);
        availability(data);
//        this.gridnightdaystime = (ExpandableHeightGridView) view.findViewById(R.id.grid_night_days_time);
//        this.grideveningdaystime = (ExpandableHeightGridView) view.findViewById(R.id.grid_evening_days_time);
//        this.gridbenefitscustom = (ExpandableHeightGridView) view.findViewById(R.id.grid_benefits_custom);
//        this.griddaystime = (ExpandableHeightGridView) view.findViewById(R.id.grid_days_time);
//        griddaystime.setExpanded(true);
//        this.griddays = (ExpandableHeightGridView) view.findViewById(R.id.grid_days);
//        gridnightdaystime.setExpanded(true);
//        grideveningdaystime.setExpanded(true);

        gridspeciality.setExpanded(true);
        gridlicence.setExpanded(true);
        gridcertification.setExpanded(true);
        gridtime.setExpanded(true);
        gridhr.setExpanded(true);
        gridbenefits.setExpanded(true);

        griddays.setExpanded(true);


        days = new ArrayList<>();
        days.add("Mon");

        days.add("Tue");
        days.add("Wed");
        days.add("Thu");
        days.add("Fri");
        days.add("Sat");
        days.add("Sun");

        days_all = new Benefits();
        List<Benefits.BenefitsBean> all_day = new ArrayList<>();
        Benefits.BenefitsBean day = new Benefits.BenefitsBean();
        day.setBenefits_id("1");
        day.setBenefits_name("Mon");
        all_day.add(day);

        day = new Benefits.BenefitsBean();
        day.setBenefits_id("2");
        day.setBenefits_name("Tue");
        all_day.add(day);

        day = new Benefits.BenefitsBean();
        day.setBenefits_id("3");
        day.setBenefits_name("Wed");
        all_day.add(day);


        day = new Benefits.BenefitsBean();
        day.setBenefits_id("4");
        day.setBenefits_name("Thu");
        all_day.add(day);


        day = new Benefits.BenefitsBean();
        day.setBenefits_id("5");
        day.setBenefits_name("Fri");
        all_day.add(day);


        day = new Benefits.BenefitsBean();
        day.setBenefits_id("6");
        day.setBenefits_name("Sat");
        all_day.add(day);


        day = new Benefits.BenefitsBean();
        day.setBenefits_id("7");
        day.setBenefits_name("Sun");
        all_day.add(day);


        days_all.setBenefits(all_day);
        ArrayList<String> test = new ArrayList<>();
//test.add("1");
//test.add("2");
//test.add("5");
        /*if already selected ******************/


        Benefits Allreay_Selected = new Benefits();
        List<Benefits.BenefitsBean> all_day_already_added = new ArrayList<>();
        // availability = "";
        if (data.getMon().contains("1")) {
            day = new Benefits.BenefitsBean();
            day.setBenefits_id("1");
            day.setBenefits_name("Mon");
            all_day_already_added.add(day);
            Allreay_Selected.setBenefits(all_day_already_added);

        }
        if (data.getTue().contains("0")) {
            day = new Benefits.BenefitsBean();
            day.setBenefits_id("2");
            day.setBenefits_name("Tue");
            all_day_already_added.add(day);
            Allreay_Selected.setBenefits(all_day_already_added);

        }
        if (data.getWed().contains("1")) {
            day = new Benefits.BenefitsBean();
            day.setBenefits_id("3");
            day.setBenefits_name("Wed");
            all_day_already_added.add(day);
            Allreay_Selected.setBenefits(all_day_already_added);

        }
        if (data.getThu().contains("0")) {
            day = new Benefits.BenefitsBean();
            day.setBenefits_id("4");
            day.setBenefits_name("Thu");
            all_day_already_added.add(day);
            Allreay_Selected.setBenefits(all_day_already_added);

        }
        if (data.getFri().contains("1")) {
            day = new Benefits.BenefitsBean();
            day.setBenefits_id("5");
            day.setBenefits_name("Fri");
            all_day_already_added.add(day);
            Allreay_Selected.setBenefits(all_day_already_added);

        }
        if (data.getSat().contains("1")) {
            // availability = availability + " , " + nearPositionDetailModel.getData().getSat();
            day = new Benefits.BenefitsBean();
            day.setBenefits_id("6");
            day.setBenefits_name("Sat");
            all_day_already_added.add(day);
            Allreay_Selected.setBenefits(all_day_already_added);
        }
        if (data.getSun().contains("0")) {
            //   availability = availability + " , " + nearPositionDetailModel.getData().getSun();
            day = new Benefits.BenefitsBean();
            day.setBenefits_id("7");
            day.setBenefits_name("Sun");
            all_day_already_added.add(day);
            Allreay_Selected.setBenefits(all_day_already_added);
        }
      //  if (availability == null || availability.contentEquals("")) {
            //  availability = "N/A";
     //   }

        // data.setText(availability + "");


//        _daDays_checkBoxAdapter = new Days_CheckBoxAdapter(days_all, ctx, 1,test);
        /*_daDays_checkBoxAdapter = new Days_CheckBoxAdapter(days_all, ctx, 1, Allreay_Selected,selected_local,selected_name_local);*/
        _daDays_checkBoxAdapter = new Days_adapter(days_all, ctx, 1, selected_local, selected_name_local,false);
        griddaystime.setAdapter(_daDays_checkBoxAdapter);


        /*  _daevening_checkBoxAdapter = new Evening_CheckBoxAdapter(days_all, ctx, 2);*/
        _daevening_checkBoxAdapter = new Evening_adapter(days_all, ctx, 2, selected_local, selected_name_local,false);
        grideveningdaystime.setAdapter(_daevening_checkBoxAdapter);


        /*   _daNightcheckBoxAdapter = new Night_CheckBoxAdapter(days_all, ctx, 3);*/
        _daNightcheckBoxAdapter = new Night_adapter(days_all, ctx, 3, selected_local, selected_name_local,false);
        gridnightdaystime.setAdapter(_daNightcheckBoxAdapter);


        progressDialog = new ProgressDialog(ctx);
        SpannableString spannableString = new SpannableString(getString(R.string.please_wait));
        Typeface typefaceSpan = Validations.setTypeface(ctx);
        spannableString.setSpan(typefaceSpan, 0, getString(R.string.please_wait).length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        progressDialog.setMessage(spannableString);
        progressDialog.setCancelable(false);
        mySharedPrefereces = new MySharedPrefereces(ctx);
        queue = Volley.newRequestQueue(ctx);
        gridspeciality.setExpanded(true);
        gridlicence.setExpanded(true);
        gridcertification.setExpanded(true);
        gridtime.setExpanded(true);
        gridhr.setExpanded(true);
        gridbenefits.setExpanded(true);
        customLayout = new ArrayList<>();
        customBenefitsAdapter = new CustomBenefitsAdapter(customLayout, ctx);
        grid_benefits_custom.setAdapter(customBenefitsAdapter);
        grid_benefits_custom.setExpanded(true);


        _dayDaysMultiSelectAdapter = new DaysMultiSelectAdapter(days, ctx);
        griddays.setAdapter(_dayDaysMultiSelectAdapter);

        customLayout = new ArrayList<>();


        getLicense();
        getCertification();
        getShift();
//    pppppppppp    getHours();
        getspeciality();
        getBenifits();
        monthList = new ArrayList<>();
        yearList = new ArrayList<>();
        getMonthData();
        getYearData();
        SpinnerAdapter monthSpinnerAdapter = new SpinnerAdapter(ctx, monthList);
        SpinnerAdapter yearSpinnerAdapter = new SpinnerAdapter(ctx, yearList);
        spExpMonth.setAdapter(monthSpinnerAdapter);
        spExpYear.setAdapter(yearSpinnerAdapter);


        for (int j = 0; j < yearList.size(); j++) {
            if (data.getYear().equals(String.valueOf(yearList.get(j)))) {
                spExpYear.setSelection(j);

            }
        }

        for (int j = 0; j < monthList.size(); j++) {
            if (data.getMonth().equals(String.valueOf(monthList.get(j)))) {
                spExpMonth.setSelection(j);
            }
        }

        edtdesc.setText(data.getPositionDescription());

        getManagerFacilityDetail();

        return view;


    }

    /**
     * show month list in spinner
     */
    public void getMonthData() {
        monthList.add("Months");
        for (int i = 0; i < 12; i++) {
            monthList.add("" + i);
        }


    }

    /**
     * show user list in spinner
     */
    public void getYearData() {
        yearList.add("Years");
        for (int i = 0; i < 26; i++) {
            yearList.add("" + i);
        }

    }

    /**
     * getting benifits data from server
     */

    private void getBenifits() {
        String Url = URLS.BENEFITS_URL;
        Log.d(TAG, "getBenifits: URL" + Url);
        StringRequest req = new StringRequest(Request.Method.GET, Url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "getBenifits: Response" + response);
                Gson gson = new Gson();
                Benefits benefits = gson.fromJson(response, Benefits.class);

                if (benefits != null) {
                    benefitadapter = new BenefitsAdapter(benefits, ctx);

                    for (int i = 0; i < benefits.getTotal(); i++) {
                        if (benefits.getBenefits().get(i).getBenefits_is_default().contentEquals("1")) {
                            //benefitadapter.selected.add(benefits.getBenefits().get(i).getBenefits_id() + "");
                        }
                    }
                    gridbenefits.setAdapter(benefitadapter);
                    //gridhr.setChoiceMode(GridView.CHOICE_MODE_SINGLE);


                } else {
                    // DialogUtils.showDialog4Activity(Login.this, ctx.getResources().getString(R.string.app_name), logindata.getMessage() + "");

                }

                if (data.getBenefitsId() != null) {
                    if (data.getBenefitsId().contains(",")) {
                        String temp[] = data.getBenefitsId().split(",");
                        for (int i = 0; i < temp.length; i++) {
                            benefitadapter.selected.add(temp[i] + "");
                        }
                    } else {
                        benefitadapter.selected.add(data.getBenefitsId() + "");
                    }
                    if (benefitadapter != null) {
                        benefitadapter.notifyDataSetChanged();
                    }
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.d(TAG, "getBenifits: Error" + error.getMessage());
                if (networkResponse != null) {
                    int statusCode = error.networkResponse.statusCode;
                    System.out.println("STATUS CODE " + statusCode + "");
                    DialogUtils.showDialog4Activity(getActivity(), ctx.getResources().getString(R.string.app_name), statusCode + "", null);
                }

            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(req);
    }

    /**
     * this method is used to getting Hours detail from server
     */

    private void getHours() {
        final String Url = URLS.HOURS_URL;
        Log.d(TAG, "getHours: URL" + Url);
        StringRequest req = new StringRequest(Request.Method.GET, Url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "getHours: Response" + Url);
                Gson gson = new Gson();
                Hours hours = gson.fromJson(response, Hours.class);

                if (hours != null) {
                    hoursAdapter = new HoursAdapter(hours, ctx);
                    gridhr.setAdapter(hoursAdapter);
                    gridhr.setChoiceMode(GridView.CHOICE_MODE_SINGLE);
                    hoursAdapter.notifyDataSetChanged();

                    if (data.getShiftName().compareToIgnoreCase("Evenings") == 0) {
                        EditPostionFragment.DAYS_EVENING_NIGHT = 11;
                    } else if (data.getShiftName().compareToIgnoreCase("Nights") == 0) {
                        EditPostionFragment.DAYS_EVENING_NIGHT = 12;
                    } else if (data.getShiftName().compareToIgnoreCase("Days") == 0) {
                        EditPostionFragment.DAYS_EVENING_NIGHT = 10;
                    }
                    System.out.println("THIS IS SET ******************* " + EditPostionFragment.DAYS_EVENING_NIGHT + "");
                    Days_CheckBoxAdapter.selected_name = new ArrayList<>();
                    Days_CheckBoxAdapter.selected = new ArrayList<>();

                    Evening_CheckBoxAdapter.selected_name = new ArrayList<>();
                    Evening_CheckBoxAdapter.selected = new ArrayList<>();


                    Night_CheckBoxAdapter.selected_name = new ArrayList<>();
                    Night_CheckBoxAdapter.selected = new ArrayList<>();


                } else {
                    // DialogUtils.showDialog4Activity(Login.this, ctx.getResources().getString(R.string.app_name), logindata.getMessage() + "");
                }


                if (data.getHourId() != null) {
                    if (data.getHourId().contains(",")) {
                        String temp[] = data.getHourId().split(",");
                        for (int i = 0; i < temp.length; i++) {
                            hoursAdapter.getSelectedHr().add(temp[i] + "");
                        }
                    } else {
                        hoursAdapter.getSelectedHr().add(data.getHourId() + "");
                    }
                    if (hoursAdapter != null) {
                        System.out.println("data.getHourId()aaaaaaaaaaaaaaaaaa " + data.getHourId() + "");
                        System.out.println("data.getHourId()aaaaaaaaaaaaaaaaaa " + data.getHourName() + "");
                        String s = data.getHourName() + "";
                        s = s.trim();
                        s.replace(" ", "");
                        if (s.compareToIgnoreCase("Full Time") == 0) {
                            EditPostionFragment.HOURS_FULL_PART_PRN_WEEKENDS = 1;
                        } else if (s.compareToIgnoreCase("Part Time") == 0) {
                            EditPostionFragment.HOURS_FULL_PART_PRN_WEEKENDS = 2;
                        } else if (s.compareToIgnoreCase("PRN") == 0) {
                            EditPostionFragment.HOURS_FULL_PART_PRN_WEEKENDS = 3;
                        } else if (s.compareToIgnoreCase("Weekends") == 0) {
                            EditPostionFragment.HOURS_FULL_PART_PRN_WEEKENDS = 4;
                        }
                        if (EditPostionFragment._daDays_checkBoxAdapter != null) {
                            EditPostionFragment._daDays_checkBoxAdapter.notifyDataSetChanged();
                        }
                        if (EditPostionFragment._daevening_checkBoxAdapter != null) {
                            EditPostionFragment._daevening_checkBoxAdapter.notifyDataSetChanged();
                        }
                        if (EditPostionFragment._daNightcheckBoxAdapter != null) {
                            EditPostionFragment._daNightcheckBoxAdapter.notifyDataSetChanged();
                        }
                        hoursAdapter.notifyDataSetChanged();
                    }
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "getHours: Error" + error.getMessage());
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    int statusCode = error.networkResponse.statusCode;
                    System.out.println("STATUS CODE " + statusCode + "");
                    DialogUtils.showDialog4Activity(getActivity(), ctx.getResources().getString(R.string.app_name), statusCode + "", null);
                }

            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(req);

    }

    /**
     * this method is used to gettting shift data from Server
     */

    private void getShift() {
        String Url = URLS.SHIFT_URL;
        Log.d(TAG, "getShift: URL" + Url);
        StringRequest req = new StringRequest(Request.Method.GET, Url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "getShift: Response" + response);

                if (ShiftAdapter.selected_name != null) {
                    ShiftAdapter.selected_name = new ArrayList<>();
                }
                if (ShiftAdapter.selected != null) {
                    ShiftAdapter.selected = new ArrayList<>();
                }
                Gson gson = new Gson();
                Shift shifts = gson.fromJson(response, Shift.class);

                if (shifts != null) {
                    getHours();
                    shiftAdapter = new ShiftAdapter(shifts, ctx);
                    gridtime.setAdapter(shiftAdapter);
                } else {
                    // DialogUtils.showDialog4Activity(Login.this, ctx.getResources().getString(R.string.app_name), logindata.getMessage() + "");
                }

                if (data.getShiftId() != null) {
//                    if (data.getShiftId().contains(",")) {
                    if (data.getShiftId().contains(",") && data.getShiftName().contains(",")) {
                        String temp[] = data.getShiftId().split(",");
                        String temp1[] = data.getShiftName().split(",");
                        for (int i = 0; i < temp.length; i++) {
                            shiftAdapter.getSelected().add(temp[i] + "");
                            shiftAdapter.getSelectedItemName().add(temp1[i] + "");
                        }
                    } else {
                        shiftAdapter.getSelected().add(data.getShiftId() + "");
                        shiftAdapter.getSelectedItemName().add(data.getShiftName() + "");
                    }
                    if (shiftAdapter != null) {

                        System.out.println("data.getShiftId()aaaaaaaaaaaaaaaaaa " + data.getShiftId() + "");
                        System.out.println("data.getShiftId()aaaaaaaaaaaaaaaaaa " + data.getShiftName() + "");
                        String s = data.getShiftName() + "";
                        s = s.trim();
                        s.replace(" ", "");
                        /*  getHours();*/
                        System.out.println("THIS IS data.getShiftName() ************* " + data.getShiftName() + " AND S is::::::::::::::::  " + s + "");
                        if (s.compareToIgnoreCase("Days") == 0) {
                            EditPostionFragment.DAYS_EVENING_NIGHT = 1;
                        } else if (s.compareToIgnoreCase("Evenings") == 0) {
                            EditPostionFragment.DAYS_EVENING_NIGHT = 2;
                        } else if (s.compareToIgnoreCase("Nights") == 0) {
                            EditPostionFragment.DAYS_EVENING_NIGHT = 3;
                        }
                        /*else if (s.compareToIgnoreCase("Weekends") == 0) {
                            EditPostionFragment.DAYS_EVENING_NIGHT = 4;
                        }
*/


                        if (EditPostionFragment._daDays_checkBoxAdapter != null) {
                            EditPostionFragment._daDays_checkBoxAdapter.notifyDataSetChanged();
                        }
                        if (EditPostionFragment._daevening_checkBoxAdapter != null) {
                            EditPostionFragment._daevening_checkBoxAdapter.notifyDataSetChanged();
                        }
                        if (EditPostionFragment._daNightcheckBoxAdapter != null) {
                            EditPostionFragment._daNightcheckBoxAdapter.notifyDataSetChanged();
                        }


                        shiftAdapter.notifyDataSetChanged();
                    }
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "getShift: Error" + error.getMessage());
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    int statusCode = error.networkResponse.statusCode;
                    System.out.println("STATUS CODE " + statusCode + "");
                    DialogUtils.showDialog4Activity(ctx, ctx.getResources().getString(R.string.app_name), statusCode + "", null);
                }

            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(req);

    }

    /**
     * This method is used to gettng Certification data from Server
     */

    private void getCertification() {

        String Url = URLS.CERTIFICATION_URL;
        Log.d(TAG, "getCertification: URL" + Url);
        StringRequest req = new StringRequest(Request.Method.GET, Url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "getCertification: Response" + response);
                Gson gson = new Gson();
                Certificates certificates = gson.fromJson(response, Certificates.class);

                if (certificates != null) {
                    certificationAdapter = new CertificationMultiSelectAdapter(certificates, ctx);
                    gridcertification.setAdapter(certificationAdapter);
                } else {
                    // DialogUtils.showDialog4Activity(Login.this, ctx.getResources().getString(R.string.app_name), logindata.getMessage() + "");
                }

                if (data.getCertificateId() != null) {
                    if (data.getCertificateId().contains(",")) {
                        String temp[] = data.getCertificateId().split(",");
                        for (int i = 0; i < temp.length; i++) {
                            certificationAdapter.selected.add(temp[i] + "");
                        }
                    } else {
                        certificationAdapter.selected.add(data.getCertificateId() + "");
                    }
                    if (certificationAdapter != null) {
                        certificationAdapter.notifyDataSetChanged();
                    }
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "getCertification: Error" + error.getMessage());
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    int statusCode = error.networkResponse.statusCode;
                    System.out.println("STATUS CODE " + statusCode + "");
                    DialogUtils.showDialog4Activity(getActivity(), ctx.getResources().getString(R.string.app_name), statusCode + "", null);
                }

            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(req);
    }

    /**
     * this method is used to getting License detail from api
     */

    private void getLicense() {
        String Url = URLS.LICENSE_URL;
        Log.d(TAG, "getLicense: URL:" + Url);
        StringRequest req = new StringRequest(Request.Method.GET, Url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "getLicense: Response:" + response);
                Gson gson = new Gson();
                License license = gson.fromJson(response, License.class);
//                Type collectionType = new TypeToken<Collection<License>>() {
//                }.getType();
//                Collection<License> license = gson.fromJson(response, collectionType);
                if (license != null) {
                    licenseAdapter = new LicenseAdapter(license, ctx);
                    gridlicence.setAdapter(licenseAdapter);
                } else {
                    // DialogUtils.showDialog4Activity(Login.this, ctx.getResources().getString(R.string.app_name), logindata.getMessage() + "");

                }

                if (data.getLicenseId() != null) {
                    if (data.getLicenseId().contains(",")) {
                        String temp[] = data.getLicenseId().split(",");
                        for (int i = 0; i < temp.length; i++) {
                            licenseAdapter.selected.add(temp[i] + "");
                        }
                    } else {
                        licenseAdapter.selected.add(data.getLicenseId() + "");
                    }
                    if (licenseAdapter != null) {
                        licenseAdapter.notifyDataSetChanged();
                    }
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "getLicense: Error:" + error.getMessage());
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    int statusCode = error.networkResponse.statusCode;
                    System.out.println("STATUS CODE " + statusCode + "");
                    DialogUtils.showDialog4Activity(getActivity(), ctx.getResources().getString(R.string.app_name), statusCode + "", null);
                }

            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(req);
    }

    /**
     * this mehod is is used to getting speciality from server
     */
    private void getspeciality() {
        String Url = URLS.SPECIALTY_URL;
        Log.d(TAG, "getspeciality: URL:- " + Url);
        StringRequest req = new StringRequest(Request.Method.GET, Url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "getspeciality: Response:- " + response);
                Gson gson = new Gson();
                Speciality speciality = gson.fromJson(response, Speciality.class);
//                Type collectionType = new TypeToken<Collection<Speciality>>() {
//                }.getType();
//                Collection<Speciality> speciality = gson.fromJson(response, collectionType);
                if (speciality != null) {

                    System.out.println("TEST " + speciality.getSpecialty().get(0).getSpecialty_id() + "");
                    System.out.println("TEST " + speciality.getSpecialty().get(0).getSpecialty_name() + "");
                    System.out.println("TEST " + speciality.getSpecialty().size() + "");
//                    for (int i = 0; i < speciality.getSpecialty().size(); i++) {
//                        addRadioButtons(i, rootView, speciality.getSpecialty().get(i).getSpecialty_name());
//                    }
                    specialityAdapter = new SpecialityAdapter(speciality, ctx);
                    gridspeciality.setAdapter(specialityAdapter);

//                  for(int i=0;i<speciality.size();i++)
//                  {
//
//                  }

                } else {
                    // DialogUtils.showDialog4Activity(Login.this, ctx.getResources().getString(R.string.app_name), logindata.getMessage() + "");

                }

                if (data.getSpecialtyId() != null) {
                    if (data.getSpecialtyId().contains(",")) {
                        String temp[] = data.getSpecialtyId().split(",");
                        for (int i = 0; i < temp.length; i++) {
                            specialityAdapter.selected.add(temp[i] + "");
                        }
                    } else {
                        specialityAdapter.selected.add(data.getSpecialtyId() + "");
                    }
                    if (specialityAdapter != null) {
                        specialityAdapter.notifyDataSetChanged();
                    }
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "getspeciality: Error:- " + error.getMessage());
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    int statusCode = error.networkResponse.statusCode;
                    System.out.println("STATUS CODE " + statusCode + "");
                    DialogUtils.showDialog4Activity(getActivity(), ctx.getResources().getString(R.string.app_name), statusCode + "", null);
                }

            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(req);

    }

    @Override
    public void onClick(View view) {

        if (view == btnSubmitBack) {
            List<String> selectedCertification = certificationAdapter.getSelected();
            List<String> selectedLicense = licenseAdapter.getSelected();
            List<String> selectedSpeciality = specialityAdapter.getSelected();
            List<String> selectedShift = shiftAdapter.getSelected();
            List<String> selectedHour = hoursAdapter.getSelectedHr();
            List<String> selectedBanifits = benefitadapter.getSelected();
            int selectedExperienseYearPosition = spExpYear.getSelectedItemPosition();
            int selectedExperienseMonthPosition = spExpMonth.getSelectedItemPosition();
            if (selectedCertification == null || selectedCertification.size() == 0) {
                Toast.makeText(ctx, "Please Select Atleast One Certification", Toast.LENGTH_LONG).show();
            } else if (selectedLicense == null || selectedLicense.size() == 0) {
                Toast.makeText(ctx, "Please Select Atleast One Licence", Toast.LENGTH_LONG).show();
            } else if (selectedSpeciality == null || selectedSpeciality.size() == 0) {
                Toast.makeText(ctx, "Please Select Atleast One Speciality", Toast.LENGTH_LONG).show();
            } else if (selectedShift == null || selectedShift.size() == 0) {
                Toast.makeText(ctx, "Please Select Atleast One Shift", Toast.LENGTH_LONG).show();
            } else if (selectedHour == null || selectedHour.size() == 0) {
                Toast.makeText(ctx, "Please Select Atleast One Hour", Toast.LENGTH_LONG).show();
            } else if (selectedBanifits == null || selectedBanifits.size() == 0) {
                Toast.makeText(ctx, "Please Select Atleast One Benifits", Toast.LENGTH_LONG).show();
            } else if (selectedExperienseYearPosition == 0) {
                Toast.makeText(ctx, "Please Select Experience Required Year", Toast.LENGTH_LONG).show();
            } else if (selectedExperienseMonthPosition == 0) {
                Toast.makeText(ctx, "Please Select Experience Required Month", Toast.LENGTH_LONG).show();
            } else if (edtdesc.getText().toString().trim().length() == 0) {
                Toast.makeText(ctx, "Please Enter Description", Toast.LENGTH_LONG).show();
            } else {
                String certiStringList = TextUtils.join(",", selectedCertification);
                String licenceStringList = TextUtils.join(",", selectedLicense);
                String specilityStringList = TextUtils.join(",", selectedSpeciality);
                String shiftStringList = TextUtils.join(",", selectedShift);
                String hoursStringList = TextUtils.join(",", selectedHour);
                String benifitsListString = TextUtils.join(",", selectedBanifits);
                String custombenifits;
                if (customBenefitsAdapter.getSelected().size() > 0) {
                    custombenifits = TextUtils.join(",", customBenefitsAdapter.getSelected());
                } else {
                    custombenifits = "";
                }
                sendPostPositionData(certiStringList, licenceStringList, specilityStringList, shiftStringList, hoursStringList, benifitsListString, spExpYear.getSelectedItem().toString(), spExpMonth.getSelectedItem().toString(), custombenifits, mySharedPrefereces.getFacility_id());


            }


        } else if (view == btnAddMoreBenifits) {
            String userInsert = edtbenefit.getText().toString();
            if (userInsert.trim().length() == 0) {
                Toast.makeText(ctx, "Please Enter Benifit Name", Toast.LENGTH_LONG).show();
            } else {
                customLayout.add(userInsert);
                edtbenefit.setText("");
                customBenefitsAdapter.notifyDataSetChanged();
            }
        } else if (view == btnSubmitPriview) {
            onPreviewButtonClicked();
        }
        else if (view == btnDeletepostion) {
            onDeleteClicked(data.getPositionId() + "");
        }
    }


    private void onDeleteClicked(String postionID) {

        String urls = URLS.DELETE_POSITION + postionID + "";
        Log.d(TAG, "onpositionDeleteClicked: Url " + urls+"");
        StringRequest req=new StringRequest(Request.Method.GET, urls, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                Log.d(TAG, "onpositionDeleteClicked: response " + response);


                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getInt("status") == 1) {
                        DialogUtils.showDialog4Activity(ctx, getResources().getString(R.string.app_name), jsonObject.getString("message"), new DialogUtils.DailogCallBackOkButtonClick() {
                            @Override
                            public void onDialogOkButtonClicked() {
                                getActivity().setResult(Activity.RESULT_OK, null);
                                getActivity().finish();

                            }
                        });

                    } else {
                        DialogUtils.showDialog4Activity(ctx, getResources().getString(R.string.app_name), jsonObject.getString("message"), null);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.d(TAG, "onpositionDeleteClicked: response " + error.getMessage());
                if (networkResponse != null) {
                    int statusCode = error.networkResponse.statusCode;
                    System.out.println("STATUS CODE onpositionDeleteClicked   " + statusCode + "");
                    DialogUtils.showDialog4Activity(getActivity(), ctx.getResources().getString(R.string.app_name), statusCode + "", null);
                }
            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(req);
    }
//    public void onPreviewButtonClicked() {
//        LayoutInflater inflater = LayoutInflater.from(ctx);
//        final View dialogView = inflater.inflate(R.layout.post_new_position_layout, null);
//        ExpandableHeightGridView griddays,grideveningdaystime,gridnightdaystime;
//        ExpandableHeightGridView griddaystime;
//        days=new ArrayList<>();
//        days.add("Mon");
//
//        days.add("Tue");
//        days.add("Wed");
//        days.add("Thu");
//        days.add("Fri");
//        days.add("Sat");
//        days.add("Sun");
//        griddays = (ExpandableHeightGridView) dialogView.findViewById(R.id.grid_days);
//        griddaystime = (ExpandableHeightGridView) dialogView.findViewById(R.id.grid_days_time);
//        gridnightdaystime = (ExpandableHeightGridView) dialogView.findViewById(R.id.grid_night_days_time);
//        grideveningdaystime = (ExpandableHeightGridView) dialogView.findViewById(R.id.grid_evening_days_time);
//        gridnightdaystime.setExpanded(true);
//        griddays.setExpanded(true);
//        grideveningdaystime.setExpanded(true);
//
//
//
//        ArrayList<String> selected_days = _daDays_checkBoxAdapter.selected;
//        ArrayList<String> selected_Evening_days = _daevening_checkBoxAdapter.selected;
//        ArrayList<String> selected_Night_days = _daNightcheckBoxAdapter.selected;
//
//        String selectedDays_to_Send = "";
//        if (selected_days != null && selected_days.size() > 0) {
//            selectedDays_to_Send = _daDays_checkBoxAdapter.selected_name.toString() + "";
//        } else if (selected_Evening_days != null && selected_Evening_days.size() > 0) {
//            selectedDays_to_Send = _daevening_checkBoxAdapter.selected_name.toString() + "";
//        } else if (selected_Night_days != null && selected_Night_days.size() > 0) {
//            selectedDays_to_Send = _daNightcheckBoxAdapter.selected_name.toString() + "";
//        }
//
//        System.out.println("this is selected :::::::::::: " + selected_days.toString() + "");
//        System.out.println("this is selected NAME :::::::::::: " + _daDays_checkBoxAdapter.selected_name.toString() + "");
//
//        selectedDays_to_Send = selectedDays_to_Send + "";
//        selectedDays_to_Send = selectedDays_to_Send.replace(" ", "");
//        selectedDays_to_Send = selectedDays_to_Send.replace("[", "");
//        selectedDays_to_Send = selectedDays_to_Send.replace("]", "");
//        selectedDays_to_Send = selectedDays_to_Send.replace(",", " , ");
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//
//        ImageView ivCloseDialog = (ImageView) dialogView.findViewById(R.id.ivCloseDialog);
//        //List<String> certification_list=certificationAdapter.getSelectedItemString();
//        String certiWithNewLine = TextUtils.join("\n", certificationAdapter.getSelectedItemString());
//        CustomTextView tvPostPositionCertification = (CustomTextView) dialogView.findViewById(R.id.tvPostPositionCertification);
//        tvPostPositionCertification.setText(certiWithNewLine);
//        CustomTextView tvPostPositionLicence = (CustomTextView) dialogView.findViewById(R.id.tvPostPositionLicence);
//        String licencenseWithNewLine = TextUtils.join("\n", licenseAdapter.getSelectedItem());
//        CustomTextView tvPostPositionDays = (CustomTextView) dialogView.findViewById(R.id.tvPostPositionDays);
//        tvPostPositionDays.setText(selectedDays_to_Send+"");
//
//        tvPostPositionLicence.setText(licencenseWithNewLine);
//        CustomTextView tvPostPositionSpeciality = (CustomTextView) dialogView.findViewById(R.id.tvPostPositionSpeciality);
//        String specialityNew = TextUtils.join("\n", specialityAdapter.getSelectedItemName());
//        tvPostPositionSpeciality.setText(specialityNew);
//        CustomTextView tvPostPositionShift = (CustomTextView) dialogView.findViewById(R.id.tvPostPositionShift);
//        String shiftnewLine = TextUtils.join("\n", shiftAdapter.getSelectedItemName());
//        tvPostPositionShift.setText(shiftnewLine);
//        CustomTextView tvPostPositionHours = (CustomTextView) dialogView.findViewById(R.id.tvPostPositionHours);
//        String hoursNewLine = TextUtils.join("\n", hoursAdapter.getSelectedItemName());
//        tvPostPositionHours.setText(hoursNewLine);
//        CustomTextView tvPostPositionBenitits = (CustomTextView) dialogView.findViewById(R.id.tvPostPositionBenitits);
//        List benifitList = benefitadapter.getSelectedName();
//        benifitList.addAll(customBenefitsAdapter.getSelected());
//        String benifits = TextUtils.join("\n", benifitList);
//        tvPostPositionBenitits.setText(benifits);
//        CustomTextView tvPostPositionRequiredExperience = (CustomTextView) dialogView.findViewById(R.id.tvPostPositionRequiredExperience);
//        String requiredExperience = "";
//
//       /* if (spExpMonth.getSelectedItemPosition() != 0) {
//            requiredExperience = "month " + spExpMonth.getSelectedItem().toString() + " ";
//        }
//        if (spExpYear.getSelectedItemPosition() != 0) {
//            requiredExperience = requiredExperience + "year " + spExpYear.getSelectedItem().toString();
//        }
//        tvPostPositionRequiredExperience.setText(requiredExperience);*/
//
//        DaysMultiSelectAdapter_duplicate_flip _dayDaysMultiSelectAdapter;
//        _dayDaysMultiSelectAdapter = new DaysMultiSelectAdapter_duplicate_flip(days, ctx);
//        griddays.setExpanded(true);
//        griddaystime.setExpanded(true);
//        griddays.setAdapter(_dayDaysMultiSelectAdapter);
//        Benefits days_all = new Benefits();
//        List<Benefits.BenefitsBean> all_day = new ArrayList<>();
//        Benefits.BenefitsBean day = new Benefits.BenefitsBean();
//
//
//
//
//
//        day.setBenefits_id("1");
//        day.setBenefits_name("Mon");
//        all_day.add(day);
//
//        day = new Benefits.BenefitsBean();
//        day.setBenefits_id("2");
//        day.setBenefits_name("Tue");
//        all_day.add(day);
//
//        day = new Benefits.BenefitsBean();
//        day.setBenefits_id("3");
//        day.setBenefits_name("Wed");
//        all_day.add(day);
//
//
//        day = new Benefits.BenefitsBean();
//        day.setBenefits_id("4");
//        day.setBenefits_name("Thu");
//        all_day.add(day);
//
//
//        day = new Benefits.BenefitsBean();
//        day.setBenefits_id("5");
//        day.setBenefits_name("Fri");
//        all_day.add(day);
//
//
//        day = new Benefits.BenefitsBean();
//        day.setBenefits_id("6");
//        day.setBenefits_name("Sat");
//        all_day.add(day);
//
//
//        day = new Benefits.BenefitsBean();
//        day.setBenefits_id("7");
//        day.setBenefits_name("Sun");
//        all_day.add(day);
//
//
//        days_all.setBenefits(all_day);
//        Benefits       test = new Benefits();
//        Days_CheckBoxAdapter       _daDays_checkBoxAdapter;
//        Night_CheckBoxAdapter _daNightcheckBoxAdapter;
//        Evening_CheckBoxAdapter _daevening_checkBoxAdapter;
//
//
//
//        _daevening_checkBoxAdapter = new Evening_CheckBoxAdapter(days_all, ctx, 2,true);
//        grideveningdaystime.setAdapter(_daevening_checkBoxAdapter);
//
//
//        _daNightcheckBoxAdapter = new Night_CheckBoxAdapter(days_all, ctx, 3,true);
//        gridnightdaystime.setAdapter(_daNightcheckBoxAdapter);
//
//
//        _daDays_checkBoxAdapter = new Days_CheckBoxAdapter(days_all, ctx, 1,test,true);
//        griddaystime.setAdapter(_daDays_checkBoxAdapter);
//        /*25 july Pragna*/
//        String y = "", m = "";
//        if (spExpYear.getSelectedItemPosition() != 0) {
//            if (spExpYear.getSelectedItem().toString().contentEquals("0") || spExpYear.getSelectedItem().toString().contentEquals("1")) {
//                y = " year ";
//            } else {
//                y = " years ";
//            }
////            requiredExperience =   spExpYear.getSelectedItem().toString()+" year ";
//            requiredExperience = spExpYear.getSelectedItem().toString() + y + "";
//        }
//        if (spExpMonth.getSelectedItemPosition() != 0) {
//            //requiredExperience = requiredExperience+""+ spExpMonth.getSelectedItem().toString() + " month  ";
//            if (spExpMonth.getSelectedItem().toString().contentEquals("0") || spExpMonth.getSelectedItem().toString().contentEquals("1")) {
//                m = " month ";
//            } else {
//                m = " months ";
//            }
//        }
//        requiredExperience = requiredExperience + "" + spExpMonth.getSelectedItem().toString() + m + "";
//        tvPostPositionRequiredExperience.setText(requiredExperience + "");
//        CustomTextView tvPostPositionDescription = (CustomTextView) dialogView.findViewById(R.id.tvPostPositionDescription);
//        tvPostPositionDescription.setText(edtdesc.getText().toString() + "");
//        final AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(ctx, R.style.myDialog));
//        final AlertDialog b = builder.create();
//        //  builder.setTitle("Material Style Dialog");
//        builder.setCancelable(true);
//        builder.setView(dialogView);
//        b.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
//        final AlertDialog show = builder.show();
//        ivCloseDialog.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                show.dismiss();
//            }
//        });
//    }
public void onPreviewButtonClicked() {
    LayoutInflater inflater = LayoutInflater.from(ctx);
    final View dialogView = inflater.inflate(R.layout.post_new_position_layout, null);
    ExpandableHeightGridView griddays, grideveningdaystime, gridnightdaystime;
    ExpandableHeightGridView griddaystime;

    ArrayList<String> selected_days = _daDays_checkBoxAdapter.selected;
    ArrayList<String> selected_Evening_days = _daevening_checkBoxAdapter.selected;
    ArrayList<String> selected_Night_days = _daNightcheckBoxAdapter.selected;

    String selectedDays_to_Send = "";
    if (selected_days != null && selected_days.size() > 0) {
        selectedDays_to_Send = _daDays_checkBoxAdapter.selected_name.toString() + "";
    } else if (selected_Evening_days != null && selected_Evening_days.size() > 0) {
        selectedDays_to_Send = _daevening_checkBoxAdapter.selected_name.toString() + "";
    } else if (selected_Night_days != null && selected_Night_days.size() > 0) {
        selectedDays_to_Send = _daNightcheckBoxAdapter.selected_name.toString() + "";
    }

    System.out.println("this is selected :::::::::::: " + selected_days.toString() + "");
    System.out.println("this is selected NAME :::::::::::: " + _daDays_checkBoxAdapter.selected_name.toString() + "");

    selectedDays_to_Send = selectedDays_to_Send + "";
    selectedDays_to_Send = selectedDays_to_Send.replace(" ", "");
    selectedDays_to_Send = selectedDays_to_Send.replace("[", "");
    selectedDays_to_Send = selectedDays_to_Send.replace("]", "");
    selectedDays_to_Send = selectedDays_to_Send.replace(",", " , ");
      /*  if (selectedDays_to_Send.contains(",")) {


            selectedDays_to_Send = selectedDays_to_Send.replace(",", "=1&");
        }
        selectedDays_to_Send = selectedDays_to_Send + "=1";*/
    //    selectedDays_to_Send = selectedDays_to_Send.toLowerCase();

    days = new ArrayList<>();
    days.add("Mon");

    days.add("Tue");
    days.add("Wed");
    days.add("Thu");
    days.add("Fri");
    days.add("Sat");
    days.add("Sun");
    griddaystime = (ExpandableHeightGridView) dialogView.findViewById(R.id.grid_days_time);
    griddays = (ExpandableHeightGridView) dialogView.findViewById(R.id.grid_days);
    gridnightdaystime = (ExpandableHeightGridView) dialogView.findViewById(R.id.grid_night_days_time);
    grideveningdaystime = (ExpandableHeightGridView) dialogView.findViewById(R.id.grid_evening_days_time);
    gridnightdaystime.setExpanded(true);
    griddays.setExpanded(true);
    grideveningdaystime.setExpanded(true);
    ImageView ivCloseDialog = (ImageView) dialogView.findViewById(R.id.ivCloseDialog);
    //List<String> certification_list=certificationAdapter.getSelectedItemString();
    String certiWithNewLine = TextUtils.join("\n", certificationAdapter.getSelectedItemString());
    CustomTextView tvPostPositionCertification = (CustomTextView) dialogView.findViewById(R.id.tvPostPositionCertification);
    CustomTextView tvPostPositionDays = (CustomTextView) dialogView.findViewById(R.id.tvPostPositionDays);
    tvPostPositionCertification.setText(certiWithNewLine);
    tvPostPositionDays.setText(selectedDays_to_Send + "");
    CustomTextView tvPostPositionLicence = (CustomTextView) dialogView.findViewById(R.id.tvPostPositionLicence);
    String licencenseWithNewLine = TextUtils.join("\n", licenseAdapter.getSelectedItem());
    tvPostPositionLicence.setText(licencenseWithNewLine);
    CustomTextView tvPostPositionSpeciality = (CustomTextView) dialogView.findViewById(R.id.tvPostPositionSpeciality);
    String specialityNew = TextUtils.join("\n", specialityAdapter.getSelectedItemName());
    tvPostPositionSpeciality.setText(specialityNew);
    CustomTextView tvPostPositionShift = (CustomTextView) dialogView.findViewById(R.id.tvPostPositionShift);
    String shiftnewLine = TextUtils.join("\n", shiftAdapter.getSelectedItemName());
    tvPostPositionShift.setText(shiftnewLine);
    CustomTextView tvPostPositionHours = (CustomTextView) dialogView.findViewById(R.id.tvPostPositionHours);
    String hoursNewLine = TextUtils.join("\n", hoursAdapter.getSelectedItemName());
    tvPostPositionHours.setText(hoursNewLine);
    CustomTextView tvPostPositionBenitits = (CustomTextView) dialogView.findViewById(R.id.tvPostPositionBenitits);
    List benifitList = benefitadapter.getSelectedName();
    benifitList.addAll(customBenefitsAdapter.getSelected());
    String benifits = TextUtils.join("\n", benifitList);
    tvPostPositionBenitits.setText(benifits);
    CustomTextView tvPostPositionRequiredExperience = (CustomTextView) dialogView.findViewById(R.id.tvPostPositionRequiredExperience);
    String requiredExperience = "";




       /* if (spExpMonth.getSelectedItemPosition() != 0) {
            requiredExperience = "month " + spExpMonth.getSelectedItem().toString() + " ";
        }
        if (spExpYear.getSelectedItemPosition() != 0) {
            requiredExperience = requiredExperience + "year " + spExpYear.getSelectedItem().toString();
        }*/
    /*25 july Pragna*/
    DaysMultiSelectAdapter_duplicate_flip _dayDaysMultiSelectAdapter;
    _dayDaysMultiSelectAdapter = new DaysMultiSelectAdapter_duplicate_flip(days, ctx);
    griddays.setExpanded(true);
    griddaystime.setExpanded(true);
    griddays.setAdapter(_dayDaysMultiSelectAdapter);
    Benefits days_all = new Benefits();
    List<Benefits.BenefitsBean> all_day = new ArrayList<>();
    Benefits.BenefitsBean day = new Benefits.BenefitsBean();


    day.setBenefits_id("1");
    day.setBenefits_name("Mon");
    all_day.add(day);

    day = new Benefits.BenefitsBean();
    day.setBenefits_id("2");
    day.setBenefits_name("Tue");
    all_day.add(day);

    day = new Benefits.BenefitsBean();
    day.setBenefits_id("3");
    day.setBenefits_name("Wed");
    all_day.add(day);


    day = new Benefits.BenefitsBean();
    day.setBenefits_id("4");
    day.setBenefits_name("Thu");
    all_day.add(day);


    day = new Benefits.BenefitsBean();
    day.setBenefits_id("5");
    day.setBenefits_name("Fri");
    all_day.add(day);


    day = new Benefits.BenefitsBean();
    day.setBenefits_id("6");
    day.setBenefits_name("Sat");
    all_day.add(day);


    day = new Benefits.BenefitsBean();
    day.setBenefits_id("7");
    day.setBenefits_name("Sun");
    all_day.add(day);


    days_all.setBenefits(all_day);
    Benefits test = new Benefits();

    Days_adapter _daDays_checkBoxAdapter;
    Night_adapter _daNightcheckBoxAdapter;
    Evening_adapter _daevening_checkBoxAdapter;
    // _daevening_checkBoxAdapter = new Evening_CheckBoxAdapter(days_all, ctx, 2, true);
    _daevening_checkBoxAdapter = new Evening_adapter(days_all, ctx, 2, true);
    grideveningdaystime.setAdapter(_daevening_checkBoxAdapter);


    _daNightcheckBoxAdapter = new Night_adapter(days_all, ctx, 3, true);
    gridnightdaystime.setAdapter(_daNightcheckBoxAdapter);


    _daDays_checkBoxAdapter = new Days_adapter(days_all, ctx, 1,  true);
    griddaystime.setAdapter(_daDays_checkBoxAdapter);

    String y = "", m = "";
    if (spExpYear.getSelectedItemPosition() != 0) {
        if (spExpYear.getSelectedItem().toString().contentEquals("0") || spExpYear.getSelectedItem().toString().contentEquals("1")) {
            y = " year ";
        } else {
            y = " years ";
        }
//            requiredExperience =   spExpYear.getSelectedItem().toString()+" year ";
        requiredExperience = spExpYear.getSelectedItem().toString() + y + "";
    }
    if (spExpMonth.getSelectedItemPosition() != 0) {
        //requiredExperience = requiredExperience+""+ spExpMonth.getSelectedItem().toString() + " month  ";
        if (spExpMonth.getSelectedItem().toString().contentEquals("0") || spExpMonth.getSelectedItem().toString().contentEquals("1")) {
            m = " month ";
        } else {
            m = " months ";
        }
    }
    requiredExperience = requiredExperience + "" + spExpMonth.getSelectedItem().toString() + m + "";
    tvPostPositionRequiredExperience.setText(requiredExperience + "");
    CustomTextView tvPostPositionDescription = (CustomTextView) dialogView.findViewById(R.id.tvPostPositionDescription);
    tvPostPositionDescription.setText(edtdesc.getText().toString() + "");
    final AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(ctx, R.style.myDialog));
    final AlertDialog b = builder.create();
    //  builder.setTitle("Material Style Dialog");
    builder.setCancelable(true);
    builder.setView(dialogView);
    b.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
    final AlertDialog show = builder.show();
    ivCloseDialog.setOnClickListener(new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            show.dismiss();
        }
    });
}
    /**
     * this method is used to send new position data to server
     *
     * @param certiStringList         how many certifice select from list
     * @param licenceStringList       how many license select from list
     * @param specilityStringList     how many license select from list
     * @param shiftStringList         which shift selected
     * @param hoursStringList         which hour selected
     * @param benifitsListString      which benifits selected
     * @param selectedExperienceYear  experiense year
     * @param selectedExperienceMonth experiense month
     * @param customBenifit           custom added benifits
     * @param facility_id             facility id
     */

    public void sendPostPositionData(final String certiStringList, final String licenceStringList, final String specilityStringList, final String shiftStringList, final String hoursStringList, final String benifitsListString, final String selectedExperienceYear, final String selectedExperienceMonth, final String customBenifit, final String facility_id) {


//        ArrayList<String> selected_days = _daDays_checkBoxAdapter.selected;
//        ArrayList<String> selected_Evening_days = _daevening_checkBoxAdapter.selected;
//        ArrayList<String> selected_Night_days = _daNightcheckBoxAdapter.selected;
//
//        String selectedDays_to_Send = "";
//        if (selected_days != null && selected_days.size() > 0) {
//            selectedDays_to_Send = _daDays_checkBoxAdapter.selected_name.toString() + "";
//        } else if (selected_Evening_days != null && selected_Evening_days.size() > 0) {
//            selectedDays_to_Send = _daevening_checkBoxAdapter.selected_name.toString() + "";
//        } else if (selected_Night_days != null && selected_Night_days.size() > 0) {
//            selectedDays_to_Send = _daNightcheckBoxAdapter.selected_name.toString() + "";
//        }
//
//        System.out.println("this is selected :::::::::::: " + selected_days.toString() + "");
//        System.out.println("this is selected NAME :::::::::::: " + _daDays_checkBoxAdapter.selected_name.toString() + "");
//
//        selectedDays_to_Send = selectedDays_to_Send + "";
//        selectedDays_to_Send = selectedDays_to_Send.replace(" ", "");
//        selectedDays_to_Send = selectedDays_to_Send.replace("[", "");
//        selectedDays_to_Send = selectedDays_to_Send.replace("]", "");
//        if (selectedDays_to_Send.contains(",")) {
//
//
//            selectedDays_to_Send = selectedDays_to_Send.replace(",", "=1&");
//        }
//        selectedDays_to_Send = selectedDays_to_Send + "=1";
//        selectedDays_to_Send = selectedDays_to_Send.toLowerCase();
//        System.out.println("FINALLY SELCTED DAYS ARE IN TO EDIT POS FRAGMENT ::::::::::::::::::::::::::::::::: ** " + selectedDays_to_Send + "");


        String selectedDays_to_Send = "";
        if (PostNewPostion.MON != null && PostNewPostion.MON.size() > 0) {
            List<String> uniqueList =    getUniqueValues(PostNewPostion.MON);
            String mon = uniqueList.toString() + "";
            mon = mon.replace(" ", "");
            mon = mon.replace("[", "");
            mon = mon.replace("]", "");
            selectedDays_to_Send = selectedDays_to_Send + "&mon="+mon;
        }
        if (PostNewPostion.TUE != null && PostNewPostion.TUE.size() > 0) {
            List<String> uniqueList =    getUniqueValues(PostNewPostion.TUE);
            String tue = uniqueList.toString() + "";
            tue = tue.replace(" ", "");
            tue = tue.replace("[", "");
            tue = tue.replace("]", "");
            selectedDays_to_Send = selectedDays_to_Send + "&tue="+tue;
        }
        if (PostNewPostion.WED != null && PostNewPostion.WED.size() > 0) {
            List<String> uniqueList =    getUniqueValues(PostNewPostion.WED);
            String wed = uniqueList.toString() + "";
            wed = wed.replace(" ", "");
            wed = wed.replace("[", "");
            wed = wed.replace("]", "");
            selectedDays_to_Send = selectedDays_to_Send + "&wed="+wed;
        }if (PostNewPostion.THR != null && PostNewPostion.THR.size() > 0) {
            List<String> uniqueList =    getUniqueValues(PostNewPostion.THR);
            String thr = uniqueList.toString() + "";
            thr = thr.replace(" ", "");
            thr = thr.replace("[", "");
            thr = thr.replace("]", "");
            selectedDays_to_Send = selectedDays_to_Send + "&thu="+thr;
        }
        if (PostNewPostion.FRI != null && PostNewPostion.FRI.size() > 0) {
            List<String> uniqueList =    getUniqueValues(PostNewPostion.FRI);
            String fri = uniqueList.toString() + "";
            fri = fri.replace(" ", "");
            fri = fri.replace("[", "");
            fri = fri.replace("]", "");
            selectedDays_to_Send = selectedDays_to_Send +"&fri="+ fri;
        }
        if (PostNewPostion.SAT != null && PostNewPostion.SAT.size() > 0) {
            List<String> uniqueList =    getUniqueValues(PostNewPostion.SAT);
            String sat = uniqueList.toString() + "";
            sat = sat.replace(" ", "");
            sat = sat.replace("[", "");
            sat = sat.replace("]", "");
            selectedDays_to_Send = selectedDays_to_Send +"&sat="+ sat;
        } if (PostNewPostion.SUN != null && PostNewPostion.SUN.size() > 0) {
            List<String> uniqueList =    getUniqueValues(PostNewPostion.SUN);
            String sun = uniqueList.toString() + "";
            sun = sun.replace(" ", "");
            sun = sun.replace("[", "");
            sun = sun.replace("]", "");
            selectedDays_to_Send = selectedDays_to_Send +"&sun="+ sun;
        }
        System.out.println("this is final string ::::::::::::::::::::```````````````` "+selectedDays_to_Send+"");




       /* String urls = URLS.POST_NEW_POSITION + "&" +
                URLS.CERTIFICATION + "=" + certiStringList + "&" +
                URLS.LICENSE + "=" + licenceStringList + "&" +
                URLS.SPECIALITIES + "=" + specilityStringList + "&" +
                URLS.SHIFT + "=" + shiftStringList + "&" +
                URLS.HOURS + "=" + hoursStringList + "&" +
                URLS.BENEFITS + "=" + benifitsListString + "&" +
                URLS.CUSTOM_BENEFITS + "=" + customBenifit + "&" +
                URLS.EXPERIENCE_YEAR + "=" + selectedExperienceYear + "&" +
                URLS.EXPERIENCE_MONTH + "=" + selectedExperienceMonth + "&" +
                URLS.POSITION_DESCRIPTION + "=" + edtdesc.getText().toString() + "&" +
                URLS.FACILITY_ID + "=" + facility_id + "&" +
                URLS.MANAGER_ID + "=" + mySharedPrefereces.getUserId() + "&" +
                URLS.ID + "=" + data.getPositionId();*/

     /*   String urls = URLS.POST_NEW_POSITION + "&" +
                URLS.CERTIFICATION + "=" + certiStringList + "&" +
                URLS.LICENSE + "=" + licenceStringList + "&" +
                URLS.SPECIALITIES + "=" + specilityStringList + "&" +
                URLS.SHIFT + "=" + shiftStringList + "&" +
                URLS.HOURS + "=" + hoursStringList + "&" +
                URLS.BENEFITS + "=" + benifitsListString + "&" +
                URLS.CUSTOM_BENEFITS + "=" + customBenifit + "&" +
                URLS.EXPERIENCE_YEAR + "=" + selectedExperienceYear + "&" +
                URLS.EXPERIENCE_MONTH + "=" + selectedExperienceMonth + "&" +
                URLS.POSITION_DESCRIPTION + "=" + edtdesc.getText().toString() + "&" +
                URLS.FACILITY_ID + "=" + facility_id + "&" +
                URLS.MANAGER_ID + "=" + mySharedPrefereces.getUserId() + "&" +
                URLS.ID + "=" + data.getPositionId() + "&" + selectedDays_to_Send + "";*/

        String urls = URLS.POST_NEW_POSITION + "&" +
                URLS.CERTIFICATION + "=" + certiStringList + "&" +
                URLS.LICENSE + "=" + licenceStringList + "&" +
                URLS.SPECIALITIES + "=" + specilityStringList + "&" +
                URLS.SHIFT + "=" + shiftStringList + "&" +
                URLS.HOURS + "=" + hoursStringList + "&" +
                URLS.BENEFITS + "=" + benifitsListString + "&" +
                URLS.CUSTOM_BENEFITS + "=" + customBenifit + "&" +
                URLS.EXPERIENCE_YEAR + "=" + selectedExperienceYear + "&" +
                URLS.EXPERIENCE_MONTH + "=" + selectedExperienceMonth + "&" +
                URLS.POSITION_DESCRIPTION + "=" + edtdesc.getText().toString() + "&" +
                URLS.FACILITY_ID + "=" + facility_id + "&" +
                URLS.MANAGER_ID + "=" + mySharedPrefereces.getUserId() + "&" +
                URLS.ID + "=" + data.getPositionId() + "" + selectedDays_to_Send + "";

        URL url = null;
        try {
            url = new URL(urls);
            URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef());
            url = uri.toURL();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        Log.d(TAG, "sendPostPositionData: URL:-" + url);

        CommonRequestHelper.getDataFromUrl(url.toString(), new CommonRequestHelper.VolleyResponseCustom() {
            @Override
            public void getServerResponse(String response) {
                Log.d(TAG, "sendPostPositionData: Response:-" + response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getInt("status") == 1) {
                        DialogUtils.showDialog4Activity(ctx, getResources().getString(R.string.app_name), jsonObject.getString("message"), new DialogUtils.DailogCallBackOkButtonClick() {
                            @Override
                            public void onDialogOkButtonClicked() {
                                getActivity().setResult(Activity.RESULT_OK, null);
                                getActivity().finish();

                            }
                        });

                    } else {
                        DialogUtils.showDialog4Activity(ctx, getResources().getString(R.string.app_name), jsonObject.getString("message"), null);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void getVolleyError(VolleyError volleyError) {
                Log.d(TAG, "sendPostPositionData: Error:-" + volleyError.getMessage());
                NetworkResponse networkResponse = volleyError.networkResponse;
                DialogUtils.noInterNetDialog(ctx, "No Internet Connection", "No active internet connection available", new DialogUtils.NoInterNetDailogClickListner() {
                    @Override
                    public void onDialogOkButtonClicked() {
                        sendPostPositionData(certiStringList, licenceStringList, specilityStringList, shiftStringList, hoursStringList, benifitsListString, selectedExperienceYear, selectedExperienceMonth, customBenifit, facility_id);
                    }

                    @Override
                    public void onDialogCancelButtonClicked() {

                    }
                });


            }
        }, ctx);
    }

    public void getManagerFacilityDetail() {
        String Url = URLS.MANAGER_FACILITY_DETAIL + mySharedPrefereces.getUserId();
        Log.d(TAG, "getManagerFacilityDetail: Url " + Url);
        StringRequest req = new StringRequest(Request.Method.GET, Url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "getManagerFacilityDetail: response " + response);
                Gson gson = new Gson();
                GetManagerFacilityDetailModel managerFacilityDetailModel = gson.fromJson(response, GetManagerFacilityDetailModel.class);

                if (managerFacilityDetailModel != null) {

                    tvfacility.setText(managerFacilityDetailModel.getMessage().get(0).getName() + " " + managerFacilityDetailModel.getMessage().get(0).getAddress() + " " + managerFacilityDetailModel.getMessage().get(0).getCity() + " " + managerFacilityDetailModel.getMessage().get(0).getState() + " " + managerFacilityDetailModel.getMessage().get(0).getZipcode());
                } else {

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.d(TAG, "getManagerFacilityDetail: response " + error.getMessage());
                if (networkResponse != null) {
                    int statusCode = error.networkResponse.statusCode;
                    System.out.println("STATUS CODE " + statusCode + "");
                    DialogUtils.showDialog4Activity(getActivity(), ctx.getResources().getString(R.string.app_name), statusCode + "", null);
                }

            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(req);

    }
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static List getUniqueValues(List input) {
        return new ArrayList<>(new HashSet<>(input));
    }
    private void availability( ExpirePositionModel.Datum data ) {
        ArrayList<String> days = new ArrayList<>();
        //   getavailability(data.getPositionId() + "");
        days = new ArrayList<>();
        days.add("Mon");
        Days_adapter.selected=new ArrayList<>();
        Days_adapter.selected_name=new ArrayList<>();

        Night_adapter.selected=new ArrayList<>();
        Night_adapter.selected_name=new ArrayList<>();


        Evening_adapter.selected=new ArrayList<>();
        Evening_adapter.selected_name=new ArrayList<>();


        days.add("Tue");
        days.add("Wed");
        days.add("Thu");
        days.add("Fri");
        days.add("Sat");
        days.add("Sun");
        System.out.println("availability MON : " + data.getMon() + " TUE : " + data.getTue() + " WED " + data.getWed() + " THR " + data.getThu() + " FRI " + data.getFri() + "");
        //   List<String> items = Arrays.asList(data.getMon().split("\\s*,\\s*"));

//PostNewPostion.MON.addAll(Arrays.asList(data.getMon().toLowerCase().split("\\s+")));
        List<String> items = new ArrayList<>();
//                    items.addAll(Arrays.asList(data.getMon().toLowerCase().split("\\s+")));
        items=    Arrays.asList(data.getMon().split("\\s*,\\s*"));
        //  PostNewPostion.MON = new ArrayList<Integer>(items.size());
        PostNewPostion.MON = new ArrayList<>();
        List<Integer> items_int = new ArrayList<>();
        for (int i = 0; i < items.size(); i++) {
            if(  isNumeric(items.get(i) + "")) {
                items_int.add(Integer.parseInt(items.get(i) + ""));
                if (items_int.get(i).equals(1)) {
                    Days_adapter.selected.add("1");
                    Days_adapter.selected_name.add("Mon");
                }
                if (items_int.get(i).equals(2)) {
                    Evening_adapter.selected.add("1");
                    Evening_adapter.selected_name.add("Mon");
                }
                if (items_int.get(i).equals(3)) {
                    Night_adapter.selected.add("1");
                    Night_adapter.selected_name.add("Mon");
                }
            }
        }
        //  List<String> items_tue = new ArrayList<>();
//                    items_tue=(Arrays.asList(data.getTue().split("\\s+")));
        List<String> items_tue = Arrays.asList(data.getTue().split("\\s*,\\s*"));

        //  PostNewPostion.MON = new ArrayList<Integer>(items.size());
        PostNewPostion.TUE = new ArrayList<>();
        List<Integer> items_int_tue = new ArrayList<>();
        //    System.out.println("TUE ```````````@ "+items_tue.toString()+"");
        for (int i = 0; i < items_tue.size(); i++) {
            System.out.println("items_tue.get(i) ````@@@@@@@@@@ "+ items_tue.get(i) + "");
            if(  isNumeric(items_tue.get(i) + "")) {
                items_int_tue.add(Integer.parseInt(items_tue.get(i) + ""));
                if (items_int_tue.get(i).equals(1)) {
                    Days_adapter.selected.add("2");
                    Days_adapter.selected_name.add("Tue");
                }
                if (items_int_tue.get(i).equals(2)) {
                    Evening_adapter.selected.add("2");
                    Evening_adapter.selected_name.add("Tue");
                }
                if (items_int_tue.get(i).equals(3)) {
                    Night_adapter.selected.add("2");
                    Night_adapter.selected_name.add("Tue");
                }
            }
        }


        List<String> items_wed = Arrays.asList(data.getWed().split("\\s*,\\s*"));

        //  PostNewPostion.MON = new ArrayList<Integer>(items.size());
        PostNewPostion.WED = new ArrayList<>();
        List<Integer> items_int_wed = new ArrayList<>();
        //   System.out.println("WED ```````````@ "+items_wed.toString()+"");
        for (int i = 0; i < items_wed.size(); i++) {
//                        System.out.println("items_tue.get(i) ````@@@@@@@@@@ "+ items_tue.get(i) + "");
            if(  isNumeric(items_wed.get(i) + "")) {
                items_int_wed.add(Integer.parseInt(items_wed.get(i) + ""));
                if (items_int_wed.get(i).equals(1)) {
                    Days_adapter.selected.add("3");
                    Days_adapter.selected_name.add("Wed");
                }
                if (items_int_wed.get(i).equals(2)) {
                    Evening_adapter.selected.add("3");
                    Evening_adapter.selected_name.add("Wed");
                }
                if (items_int_wed.get(i).equals(3)) {
                    Night_adapter.selected.add("3");
                    Night_adapter.selected_name.add("Wed");
                }
            }
        }


        List<String> items_Thr = Arrays.asList(data.getThu().split("\\s*,\\s*"));

        //  PostNewPostion.MON = new ArrayList<Integer>(items.size());
        PostNewPostion.THR = new ArrayList<>();
        List<Integer> items_int_thr = new ArrayList<>();
        //  System.out.println("THRU ```````````@ "+items_Thr.toString()+"");
        for (int i = 0; i < items_Thr.size(); i++) {
//                        System.out.println("items_tue.get(i) ````@@@@@@@@@@ "+ items_tue.get(i) + "");
            if(  isNumeric(items_Thr.get(i) + "")) {
                items_int_thr.add(Integer.parseInt(items_Thr.get(i) + ""));
                if (items_int_thr.get(i).equals(1)) {
                    Days_adapter.selected.add("4");
                    Days_adapter.selected_name.add("Thr");
                }
                if (items_int_thr.get(i).equals(2)) {
                    Evening_adapter.selected.add("4");
                    Evening_adapter.selected_name.add("Thr");
                }
                if (items_int_thr.get(i).equals(3)) {
                    Night_adapter.selected.add("4");
                    Night_adapter.selected_name.add("Thr");
                }
            }
        }


        List<String> items_Fri = Arrays.asList(data.getFri().split("\\s*,\\s*"));

        //  PostNewPostion.MON = new ArrayList<Integer>(items.size());
        PostNewPostion.FRI = new ArrayList<>();
        List<Integer> items_int_fri = new ArrayList<>();
        //   System.out.println("FRID ```````````@ "+items_Fri.toString()+"");
        for (int i = 0; i < items_Fri.size(); i++) {
//                        System.out.println("items_tue.get(i) ````@@@@@@@@@@ "+ items_tue.get(i) + "");
            if(  isNumeric(items_Fri.get(i) + "")) {
                items_int_fri.add(Integer.parseInt(items_Fri.get(i) + ""));
                if (items_int_fri.get(i).equals(1)) {
                    Days_adapter.selected.add("5");
                    Days_adapter.selected_name.add("Fri");
                }
                if (items_int_fri.get(i).equals(2)) {
                    Evening_adapter.selected.add("5");
                    Evening_adapter.selected_name.add("Fri");
                }
                if (items_int_fri.get(i).equals(3)) {
                    Night_adapter.selected.add("5");
                    Night_adapter.selected_name.add("Fri");
                }
            }
        }


        List<String> items_Sat = Arrays.asList(data.getSat().split("\\s*,\\s*"));

        //  PostNewPostion.MON = new ArrayList<Integer>(items.size());
        PostNewPostion.FRI = new ArrayList<>();
        List<Integer> items_int_sat = new ArrayList<>();
        //   System.out.println("SAT ```````````@ "+items_Sat.toString()+"");
        for (int i = 0; i < items_Sat.size(); i++) {
//                        System.out.println("items_tue.get(i) ````@@@@@@@@@@ "+ items_tue.get(i) + "");
            if(  isNumeric(items_Sat.get(i) + "")) {
                items_int_sat.add(Integer.parseInt(items_Sat.get(i) + ""));
                if (items_int_sat.get(i).equals(1)) {
                    Days_adapter.selected.add("6");
                    Days_adapter.selected_name.add("Sat");
                }
                if (items_int_sat.get(i).equals(2)) {
                    Evening_adapter.selected.add("6");
                    Evening_adapter.selected_name.add("Sat");
                }
                if (items_int_sat.get(i).equals(3)) {
                    Night_adapter.selected.add("6");
                    Night_adapter.selected_name.add("Sat");
                }
            }
        }


        List<String> items_Sun = Arrays.asList(data.getSun().split("\\s*,\\s*"));

        //  PostNewPostion.MON = new ArrayList<Integer>(items.size());
        PostNewPostion.FRI = new ArrayList<>();
        List<Integer> items_int_sun = new ArrayList<>();
        //   System.out.println("SUN ```````````@ "+items_Sun.toString()+"");
        for (int i = 0; i < items_Sun.size(); i++) {
//                        System.out.println("items_tue.get(i) ````@@@@@@@@@@ "+ items_tue.get(i) + "");
            if(  isNumeric(items_Sun.get(i) + "")) {
                items_int_sun.add(Integer.parseInt(items_Sun.get(i) + ""));
                if (items_int_sun.get(i).equals(1)) {
                    Days_adapter.selected.add("7");
                    Days_adapter.selected_name.add("Sun");
                }
                if (items_int_sun.get(i).equals(2)) {
                    Evening_adapter.selected.add("7");
                    Evening_adapter.selected_name.add("Sun");
                }
                if (items_int_sun.get(i).equals(3)) {
                    Night_adapter.selected.add("7");
                    Night_adapter.selected_name.add("Sun");
                }
            }
        }



        PostNewPostion.MON.addAll(items_int);
        PostNewPostion.TUE.addAll(items_int_tue);
        PostNewPostion.WED.addAll(items_int_wed);
        PostNewPostion.THR.addAll(items_int_thr);
        PostNewPostion.FRI.addAll(items_int_fri);
        PostNewPostion.SAT.addAll(items_int_sat);
        PostNewPostion.SUN.addAll(items_int_sun);

//        System.out.println("this is monday " + PostNewPostion.MON.toString() + "");
//        System.out.println("this is Tuesday " + PostNewPostion.TUE.toString() + "");
//        System.out.println("this is TuesdayDays_adapter.selected_name``````````@ " + Days_adapter.selected_name .toString()+ "");
//        System.out.println("this is TuesdayEvening_adapter.selected_name``````````@ " + Evening_adapter.selected_name.toString() + "");
        //    v_v = LayoutInflater.from(_parent.getContext()).inflate(R.layout.testforflip, _parent, false);
        //    v_v = LayoutInflater.from(_parent.getContext()).inflate(R.layout.testforflip_availability, _parent, false);
        //  holder.row_item_flipper.removeView(v_v);
        // AnimationFactory.flipTransition(holder.row_item_flipper,
        //  AnimationFactory.FlipDirection.LEFT_RIGHT);




        // holder.row_item_flipper.addView(v_v);
        // initView(v_v);
        DaysMultiSelectAdapter_duplicate_flip _dayDaysMultiSelectAdapter;
        _dayDaysMultiSelectAdapter = new DaysMultiSelectAdapter_duplicate_flip(days, ctx);
        griddays.setExpanded(true);
        griddays.setAdapter(_dayDaysMultiSelectAdapter);
        Benefits days_all = new Benefits();
        List<Benefits.BenefitsBean> all_day = new ArrayList<>();
        Benefits.BenefitsBean day = new Benefits.BenefitsBean();


        day.setBenefits_id("1");
        day.setBenefits_name("Mon");
        all_day.add(day);

        day = new Benefits.BenefitsBean();
        day.setBenefits_id("2");
        day.setBenefits_name("Tue");
        all_day.add(day);

        day = new Benefits.BenefitsBean();
        day.setBenefits_id("3");
        day.setBenefits_name("Wed");
        all_day.add(day);


        day = new Benefits.BenefitsBean();
        day.setBenefits_id("4");
        day.setBenefits_name("Thu");
        all_day.add(day);


        day = new Benefits.BenefitsBean();
        day.setBenefits_id("5");
        day.setBenefits_name("Fri");
        all_day.add(day);


        day = new Benefits.BenefitsBean();
        day.setBenefits_id("6");
        day.setBenefits_name("Sat");
        all_day.add(day);


        day = new Benefits.BenefitsBean();
        day.setBenefits_id("7");
        day.setBenefits_name("Sun");
        all_day.add(day);


        days_all.setBenefits(all_day);
        Benefits test = new Benefits();
/*
                           _daDays_checkBoxAdapter = new Days_CheckBoxAdapter(days_all, context, 1,test,true);
                    griddaystime.setAdapter(_daDays_checkBoxAdapter);*/


          _daDays_checkBoxAdapter = new Days_adapter(days_all, ctx, 1, test, false);
        griddaystime.setAdapter(_daDays_checkBoxAdapter);

           _daevening_checkBoxAdapter = new Evening_adapter(days_all, ctx, 2, false);
        grideveningdaystime.setAdapter(_daevening_checkBoxAdapter);


           _daNightcheckBoxAdapter = new Night_adapter(days_all, ctx, 3, false);
        gridnightdaystime.setAdapter(_daNightcheckBoxAdapter);
    }
    private boolean isNumeric(String string) {
        if(Pattern.matches("\\d{1,3}((\\.\\d{1,2})?)", string))
            return true;
        else
            return false;
    }
}

