
/*https://stackoverflow.com/questions/6176391/stop-scrollview-from-auto-scrolling-to-an-edittext*/
package com.hiremehealthcare.fragment;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.android.volley.RequestQueue;
import com.hiremehealthcare.R;


public class SearchManagerFragment extends Fragment  {

    public static SearchManagerFragment newInstance() {
        SearchManagerFragment fragment = new SearchManagerFragment();
        return fragment;
    }


    RequestQueue queue;
    Context ctx;
    String[] countryNames = {"15 miles", "20 miles", "25 miles"};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //   return inflater.inflate(R.layout.search_fragment, container, false);

        View rootView = inflater.inflate(R.layout.search_managerfragment, container, false);
        init(rootView);

//        CustomAdapter customAdapter = new CustomAdapter(getActivity(), null, countryNames);
//        tvwithin.setAdapter(customAdapter);
        return rootView;
    }

    private void init(View rootView) {

    }
}
