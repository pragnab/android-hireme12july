/*https://stackoverflow.com/questions/6176391/stop-scrollview-from-auto-scrolling-to-an-edittext*/
package com.hiremehealthcare.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.location.Address;
import android.location.Geocoder;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.example.easywaylocation.EasyWayLocation;
import com.example.easywaylocation.Listener;
import com.google.gson.Gson;
import com.hiremehealthcare.CommonCls.CustomBoldTextView;
import com.hiremehealthcare.CommonCls.CustomButton;
import com.hiremehealthcare.CommonCls.CustomEditText;
import com.hiremehealthcare.CommonCls.DialogUtils;
import com.hiremehealthcare.CommonCls.ExpandableHeightGridView;
import com.hiremehealthcare.CommonCls.URLS;
import com.hiremehealthcare.CommonCls.Validations;
import com.hiremehealthcare.POJO.ApplicantNearPositionModel;
import com.hiremehealthcare.POJO.Benefits;
import com.hiremehealthcare.POJO.Certificates;
import com.hiremehealthcare.POJO.Hours;
import com.hiremehealthcare.POJO.License;
import com.hiremehealthcare.POJO.Shift;
import com.hiremehealthcare.POJO.Speciality;
import com.hiremehealthcare.R;
import com.hiremehealthcare.activity.SearchPositionDetailActivity;
import com.hiremehealthcare.Utils.CommonRequestHelper;
import com.hiremehealthcare.Utils.GPSTracker;
import com.hiremehealthcare.adapter.CertificationAdapter;
import com.hiremehealthcare.adapter.CertificationMultiSelectAdapter;
import com.hiremehealthcare.adapter.CustomAdapter;
import com.hiremehealthcare.adapter.DaysMultiSelectAdapter;
import com.hiremehealthcare.adapter.Days_CheckBoxAdapter;
import com.hiremehealthcare.adapter.Days_adapter;
import com.hiremehealthcare.adapter.Evening_CheckBoxAdapter;
import com.hiremehealthcare.adapter.Evening_adapter;
import com.hiremehealthcare.adapter.HoursAdapter;
import com.hiremehealthcare.adapter.LicenseAdapter;
import com.hiremehealthcare.adapter.Night_CheckBoxAdapter;
import com.hiremehealthcare.adapter.Night_adapter;
import com.hiremehealthcare.adapter.ShiftAdapter;
import com.hiremehealthcare.adapter.SpecialityAdapter;
import com.hiremehealthcare.adapter.SpecialityMultiSelectAdapter;
import com.hiremehealthcare.database.MySharedPrefereces;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;


public class SearchFragment extends Fragment implements View.OnClickListener {
    CustomBoldTextView tvspeciality;
    //    private RadioGroup rg;
    private com.hiremehealthcare.CommonCls.ExpandableHeightGridView gridspeciality, grid_licence, grid_certification, grid_time, grid_hr;
    private com.hiremehealthcare.CommonCls.CustomBoldTextView tvpos;
    private com.hiremehealthcare.CommonCls.CustomBoldTextView tvsearchby;
    private com.hiremehealthcare.CommonCls.CustomEditText edtzipcode;
    private com.hiremehealthcare.CommonCls.CustomBoldTextView tvor;
    //    private com.hiremehealthcare.CommonCls.CustomEditText tvwithin;
    private Spinner tvwithin;
    private com.hiremehealthcare.CommonCls.CustomBoldTextView tvlicense;
    private com.hiremehealthcare.CommonCls.ExpandableHeightGridView gridlicence;
    private com.hiremehealthcare.CommonCls.CustomBoldTextView tvcertification;
    private com.hiremehealthcare.CommonCls.ExpandableHeightGridView gridcertification;
    private com.hiremehealthcare.CommonCls.CustomBoldTextView tvTIME;
    private com.hiremehealthcare.CommonCls.CustomBoldTextView tvShift;
    private com.hiremehealthcare.CommonCls.ExpandableHeightGridView gridtime;
    private com.hiremehealthcare.CommonCls.CustomBoldTextView tvShifthour;
    private com.hiremehealthcare.CommonCls.ExpandableHeightGridView gridhr;
    private com.hiremehealthcare.CommonCls.CustomButton btnsearch;
    private com.hiremehealthcare.CommonCls.CustomButton btnclearsearch;
    private android.widget.ScrollView scrollview;
    HoursAdapter hoursAdapter;
    ShiftAdapter shiftAdapter;
    CertificationMultiSelectAdapter certificationAdapter;
    LicenseAdapter licenseAdapter;
    SpecialityMultiSelectAdapter specialityAdapter;
    ProgressDialog progressDialog;
    MySharedPrefereces mySharedPrefereces;
    CustomButton btnNearMe;
    GPSTracker gpsTracker;
    Context context;
    String TAG = "SearchFragment";
    EasyWayLocation easyWayLocation;


   /* public static Days_CheckBoxAdapter _daDays_checkBoxAdapter;
    public static Night_CheckBoxAdapter _daNightcheckBoxAdapter;
    public static Evening_CheckBoxAdapter _daevening_checkBoxAdapter;*/


    public static Days_adapter _daDays_checkBoxAdapter;
    public static Night_adapter _daNightcheckBoxAdapter;
    public static Evening_adapter _daevening_checkBoxAdapter;
    DaysMultiSelectAdapter _dayDaysMultiSelectAdapter;
    private ExpandableHeightGridView griddays;
    private ExpandableHeightGridView griddaystime;
    ArrayList<String> days;
    Benefits days_all;
    private ExpandableHeightGridView grideveningdaystime;
    private ExpandableHeightGridView gridnightdaystime;
    private ExpandableHeightGridView gridbenefitscustom;


    public static ApplicantNearPositionModel applicantNearPositionModel;

    public static SearchFragment newInstance() {
        SearchFragment fragment = new SearchFragment();
        return fragment;
    }


    RequestQueue queue;
    Context ctx;
    //    String[] countryNames = {"15 miles", "30 miles", "60 miles"};
    String[] countryNames = {"10 miles", "20 miles", "30 miles", "40 miles", "50 miles", "60 miles", "70 miles", "80 miles", "90 miles", "100 miles"};

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        //   return inflater.inflate(R.layout.search_fragment, container, false);

        View rootView = inflater.inflate(R.layout.search_fragment, container, false);
        init(rootView);
        CustomAdapter customAdapter = new CustomAdapter(getActivity(), null, countryNames);
        tvwithin.setAdapter(customAdapter);
        return rootView;
    }

    private void init(View rootView) {
        ctx = getActivity();

        progressDialog = new ProgressDialog(ctx);
        SpannableString spannableString = new SpannableString(getString(R.string.please_wait));
        Typeface typefaceSpan = Validations.setTypeface(ctx);
        spannableString.setSpan(typefaceSpan, 0, getString(R.string.please_wait).length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        progressDialog.setMessage(spannableString);
        progressDialog.setCancelable(false);
        mySharedPrefereces = new MySharedPrefereces(ctx);
        this.scrollview = (ScrollView) rootView.findViewById(R.id.scrollview);
        this.btnclearsearch = (CustomButton) rootView.findViewById(R.id.btn_clearsearch);
        btnNearMe = (CustomButton) rootView.findViewById(R.id.btnNearMe);
        btnNearMe.setOnClickListener(this);
        this.btnsearch = (CustomButton) rootView.findViewById(R.id.btn_search);
        this.gridhr = (ExpandableHeightGridView) rootView.findViewById(R.id.grid_hr);
        this.tvShifthour = (CustomBoldTextView) rootView.findViewById(R.id.tv_Shifthour);
        this.gridtime = (ExpandableHeightGridView) rootView.findViewById(R.id.grid_time);
        this.tvShift = (CustomBoldTextView) rootView.findViewById(R.id.tv_Shift);
        this.tvTIME = (CustomBoldTextView) rootView.findViewById(R.id.tv_TIME);
        this.gridcertification = (ExpandableHeightGridView) rootView.findViewById(R.id.grid_certification);
        this.tvcertification = (CustomBoldTextView) rootView.findViewById(R.id.tv_certification);
        this.gridlicence = (ExpandableHeightGridView) rootView.findViewById(R.id.grid_licence);
        this.tvlicense = (CustomBoldTextView) rootView.findViewById(R.id.tv_license);
        this.tvwithin = (Spinner) rootView.findViewById(R.id.spComplationYear);
        this.tvor = (CustomBoldTextView) rootView.findViewById(R.id.tv_or);
        this.edtzipcode = (CustomEditText) rootView.findViewById(R.id.edt_zipcode);
        edtzipcode.setText(mySharedPrefereces.getZipCode());
        this.tvsearchby = (CustomBoldTextView) rootView.findViewById(R.id.tv_searchby);
        this.tvpos = (CustomBoldTextView) rootView.findViewById(R.id.tv_pos);
        this.gridspeciality = (ExpandableHeightGridView) rootView.findViewById(R.id.grid_speciality);
        this.grid_licence = (ExpandableHeightGridView) rootView.findViewById(R.id.grid_licence);
        this.grid_certification = (ExpandableHeightGridView) rootView.findViewById(R.id.grid_certification);
        this.grid_time = (ExpandableHeightGridView) rootView.findViewById(R.id.grid_time);
        this.grid_hr = (ExpandableHeightGridView) rootView.findViewById(R.id.grid_hr);
        gridspeciality.setExpanded(true);
        grid_licence.setExpanded(true);
        grid_certification.setExpanded(true);
        grid_time.setExpanded(true);
        grid_hr.setExpanded(true);
        btnclearsearch.setOnClickListener(this);
        btnsearch.setOnClickListener(this);


        scrollview.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (edtzipcode.hasFocus()) {
                    edtzipcode.clearFocus();
                }
                return false;
            }
        });











        /*4-feb-19 Pragna*/
        this.gridnightdaystime = (ExpandableHeightGridView) rootView.findViewById(R.id.grid_night_days_time);
        this.grideveningdaystime = (ExpandableHeightGridView) rootView.findViewById(R.id.grid_evening_days_time);
        this.gridbenefitscustom = (ExpandableHeightGridView) rootView.findViewById(R.id.grid_benefits_custom);
        this.griddaystime = (ExpandableHeightGridView) rootView.findViewById(R.id.grid_days_time);
        this.griddays = (ExpandableHeightGridView) rootView.findViewById(R.id.grid_days);
        griddaystime.setExpanded(true);
        gridnightdaystime.setExpanded(true);
        grideveningdaystime.setExpanded(true);
        griddays.setExpanded(true);
        days = new ArrayList<>();
        days.add("Mon");

        days.add("Tue");
        days.add("Wed");
        days.add("Thu");
        days.add("Fri");
        days.add("Sat");
        days.add("Sun");

        days_all = new Benefits();
        List<Benefits.BenefitsBean> all_day = new ArrayList<>();
        Benefits.BenefitsBean day = new Benefits.BenefitsBean();
        day.setBenefits_id("1");
        day.setBenefits_name("Mon");
        all_day.add(day);

        day = new Benefits.BenefitsBean();
        day.setBenefits_id("2");
        day.setBenefits_name("Tue");
        all_day.add(day);

        day = new Benefits.BenefitsBean();
        day.setBenefits_id("3");
        day.setBenefits_name("Wed");
        all_day.add(day);


        day = new Benefits.BenefitsBean();
        day.setBenefits_id("4");
        day.setBenefits_name("Thu");
        all_day.add(day);


        day = new Benefits.BenefitsBean();
        day.setBenefits_id("5");
        day.setBenefits_name("Fri");
        all_day.add(day);


        day = new Benefits.BenefitsBean();
        day.setBenefits_id("6");
        day.setBenefits_name("Sat");
        all_day.add(day);


        day = new Benefits.BenefitsBean();
        day.setBenefits_id("7");
        day.setBenefits_name("Sun");
        all_day.add(day);

        griddays.setExpanded(true);
        days_all.setBenefits(all_day);

        _dayDaysMultiSelectAdapter = new DaysMultiSelectAdapter(days, ctx);
        griddays.setAdapter(_dayDaysMultiSelectAdapter);

        _daDays_checkBoxAdapter = new Days_adapter(days_all, ctx, 1,false);
        griddaystime.setAdapter(_daDays_checkBoxAdapter);


        _daevening_checkBoxAdapter = new Evening_adapter(days_all, ctx, 2,false);
        grideveningdaystime.setAdapter(_daevening_checkBoxAdapter);


        _daNightcheckBoxAdapter = new Night_adapter(days_all, ctx, 3,false);
        gridnightdaystime.setAdapter(_daNightcheckBoxAdapter);


        //   this.rg = (RadioGroup) rootView.findViewById(R.id.rg);
        this.tvspeciality = (CustomBoldTextView) rootView.findViewById(R.id.tv_speciality);
        queue = Volley.newRequestQueue(ctx);

//        edtzipcode.setFocusableInTouchMode(false);
//        edtzipcode.setFocusable(false);
//        edtzipcode.setFocusableInTouchMode(true);
//        edtzipcode.setFocusable(true);
        getspeciality();
        getLicense();
        getCertification();
        getShift();
        //    getHours();
        easyWayLocation = new EasyWayLocation(ctx);
        easyWayLocation.setListener(new Listener() {
            @Override
            public void locationOn() {
                Log.d(TAG, "locationOn: ");
            }

            @Override
            public void onPositionChanged() {
                Log.d(TAG, "onPositionChanged: ");
            }

            @Override
            public void locationCancelled() {
                Log.d(TAG, "locationCancelled: ");
            }
        });
    }

    /**
     * this method is used to getting hour data from server
     */

    private void getHours() {
        String Url = URLS.HOURS_URL;
        Log.d(TAG, "getHours: URL " + Url);
        StringRequest req = new StringRequest(Request.Method.GET, Url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "getHours: Response " + response);
                System.out.println("CallHours RESPONSE " + response + "");
                Gson gson = new Gson();
                Hours hours = gson.fromJson(response, Hours.class);

                if (hours != null) {
                    hoursAdapter = new HoursAdapter(hours, ctx);
                    grid_hr.setAdapter(hoursAdapter);
                    grid_hr.setChoiceMode(GridView.CHOICE_MODE_SINGLE);


                } else {
                    // DialogUtils.showDialog4Activity(Login.this, ctx.getResources().getString(R.string.app_name), logindata.getMessage() + "");

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "getHours: error:- " + error.getMessage());
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    int statusCode = error.networkResponse.statusCode;
                    System.out.println("STATUS CODE " + statusCode + "");
                    DialogUtils.showDialog4Activity(getActivity(), ctx.getResources().getString(R.string.app_name), statusCode + "", null);
                }

            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(req);

    }

    /**
     * getting shift data from server
     */

    private void getShift() {
        String Url = URLS.SHIFT_URL;
        Log.d(TAG, "getShift: URL:- " + Url);
        StringRequest req = new StringRequest(Request.Method.GET, Url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "getShift: response:- " + response);
                System.out.println("getShift RESPONSE " + response + "");
                Gson gson = new Gson();
                Shift shifts = gson.fromJson(response, Shift.class);

                if (shifts != null) {

                    getHours();
                    shiftAdapter = new ShiftAdapter(shifts, ctx);
                    grid_time.setAdapter(shiftAdapter);


                    if (EditPostionFragment._daDays_checkBoxAdapter != null) {
                        EditPostionFragment._daDays_checkBoxAdapter.notifyDataSetChanged();
                    }
                    if (EditPostionFragment._daevening_checkBoxAdapter != null) {
                        EditPostionFragment._daevening_checkBoxAdapter.notifyDataSetChanged();
                    }
                    if (EditPostionFragment._daNightcheckBoxAdapter != null) {
                        EditPostionFragment._daNightcheckBoxAdapter.notifyDataSetChanged();
                    }


                } else {
                    // DialogUtils.showDialog4Activity(Login.this, ctx.getResources().getString(R.string.app_name), logindata.getMessage() + "");

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "getShift: Error:- " + error.getMessage());
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    int statusCode = error.networkResponse.statusCode;
                    System.out.println("STATUS CODE " + statusCode + "");
                    DialogUtils.showDialog4Activity(ctx, ctx.getResources().getString(R.string.app_name), statusCode + "", null);
                }

            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(req);

    }

    /**
     * this method is used to getting Certification data from server
     */

    private void getCertification() {

        String Url = URLS.CERTIFICATION_URL;
        Log.d(TAG, "getCertification: URL:- " + Url);
        StringRequest req = new StringRequest(Request.Method.GET, Url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "getCertification: response:- " + response);
                Gson gson = new Gson();
                Certificates certificates = gson.fromJson(response, Certificates.class);

                if (certificates != null) {
                    certificationAdapter = new CertificationMultiSelectAdapter(certificates, ctx);
                    grid_certification.setAdapter(certificationAdapter);
                } else {
                    // DialogUtils.showDialog4Activity(Login.this, ctx.getResources().getString(R.string.app_name), logindata.getMessage() + "");

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "getCertification: Error:- " + error.getMessage());
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    int statusCode = error.networkResponse.statusCode;
                    System.out.println("STATUS CODE " + statusCode + "");
                    DialogUtils.showDialog4Activity(getActivity(), ctx.getResources().getString(R.string.app_name), statusCode + "", null);
                }

            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(req);
    }

    /**
     * this method is used to getting license data from server
     */

    private void getLicense() {
        String Url = URLS.LICENSE_URL;
        Log.d(TAG, "getLicense: URL:- " + Url);
        StringRequest req = new StringRequest(Request.Method.GET, Url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                System.out.println("CallLicense RESPONSE " + response + "");
                Log.d(TAG, "getLicense: Response:- " + response);
                Gson gson = new Gson();
                License license = gson.fromJson(response, License.class);
//                Type collectionType = new TypeToken<Collection<License>>() {
//                }.getType();
//                Collection<License> license = gson.fromJson(response, collectionType);
                if (license != null) {
                    licenseAdapter = new LicenseAdapter(license, ctx);
                    grid_licence.setAdapter(licenseAdapter);
                } else {
                    // DialogUtils.showDialog4Activity(Login.this, ctx.getResources().getString(R.string.app_name), logindata.getMessage() + "");

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "getLicense: Response:- " + error.getMessage());
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    int statusCode = error.networkResponse.statusCode;
                    System.out.println("STATUS CODE " + statusCode + "");
                    DialogUtils.showDialog4Activity(getActivity(), ctx.getResources().getString(R.string.app_name), statusCode + "", null);
                }

            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(req);
    }

    /**
     * this method is used to getting specility data from server
     */

    private void getspeciality() {
        String Url = URLS.SPECIALTY_URL;
        Log.d(TAG, "getspeciality: URL:- " + Url);
        StringRequest req = new StringRequest(Request.Method.GET, Url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "getspeciality: Response:- " + response);
                Gson gson = new Gson();
                Speciality speciality = gson.fromJson(response, Speciality.class);
//                Type collectionType = new TypeToken<Collection<Speciality>>() {
//                }.getType();
//                Collection<Speciality> speciality = gson.fromJson(response, collectionType);
                if (speciality != null) {

                    System.out.println("TEST " + speciality.getSpecialty().get(0).getSpecialty_id() + "");
                    System.out.println("TEST " + speciality.getSpecialty().get(0).getSpecialty_name() + "");
                    System.out.println("TEST " + speciality.getSpecialty().size() + "");
//                    for (int i = 0; i < speciality.getSpecialty().size(); i++) {
//                        addRadioButtons(i, rootView, speciality.getSpecialty().get(i).getSpecialty_name());
//                    }
                    specialityAdapter = new SpecialityMultiSelectAdapter(speciality, ctx);
                    gridspeciality.setAdapter(specialityAdapter);

//                  for(int i=0;i<speciality.size();i++)
//                  {
//
//                  }

                } else {
                    // DialogUtils.showDialog4Activity(Login.this, ctx.getResources().getString(R.string.app_name), logindata.getMessage() + "");

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "getspeciality: Error:- " + error.getMessage());
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    int statusCode = error.networkResponse.statusCode;
                    System.out.println("STATUS CODE " + statusCode + "");
                    DialogUtils.showDialog4Activity(getActivity(), ctx.getResources().getString(R.string.app_name), statusCode + "", null);
                }

            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(req);

    }

    public void addRadioButtons(int number, View rootView, String Name) {

        for (int row = 0; row < 1; row++) {
            RadioGroup ll = new RadioGroup(getActivity());
            ll.setOrientation(LinearLayout.VERTICAL);

//            for (int i = 1; i <= number; i++) {
            for (int i = 1; i <= 1; i++) {
                RadioButton rdbtn = new RadioButton(getActivity());
                rdbtn.setId((row * 2) + i);
//                rdbtn.setText("Radio " + rdbtn.getId());
                rdbtn.setText("Radio " + Name);
                ll.addView(rdbtn);
            }
            // ((ViewGroup) rootView.findViewById(R.id.rg)).addView(ll);
        }

    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {

            case R.id.btn_search:
                System.out.println("on search clicked ");

                String selectedDays_to_Send = _daDays_checkBoxAdapter.selected_name.toString() + "";
                selectedDays_to_Send = selectedDays_to_Send + "";
                selectedDays_to_Send = selectedDays_to_Send.replace(" ", "");
                selectedDays_to_Send = selectedDays_to_Send.replace("[", "");
                selectedDays_to_Send = selectedDays_to_Send.replace("]", "");
                if (selectedDays_to_Send.contains(",")) {


                    selectedDays_to_Send = selectedDays_to_Send.replace(",", "=1&");
                }
                selectedDays_to_Send = selectedDays_to_Send + "=1";
                selectedDays_to_Send = selectedDays_to_Send.toLowerCase();
                System.out.println("FINALLY SELCTED DAYS ARE IN SEARCH FRAGMENT ::::::::::::::::::::::::::::::::: ** " + selectedDays_to_Send + "");

                if (hoursAdapter != null) {
                    ArrayList<String> hr = hoursAdapter.getSelectedHr();
                    System.out.println("hr " + hr.toString() + "");
                }
                if (certificationAdapter != null) {
                    ArrayList<String> certificate = certificationAdapter.getSelected();
                    System.out.println("certificate " + certificate.toString() + "");
                }
                if (licenseAdapter != null) {
                    ArrayList<String> license = licenseAdapter.getSelected();
                    System.out.println("license " + license.toString() + "");
                }
                if (specialityAdapter != null) {
                    ArrayList<String> speciality = specialityAdapter.getSelected();
                    System.out.println("speciality " + speciality.toString() + "");
                }
                if (shiftAdapter != null) {
                    ArrayList<String> shift = shiftAdapter.getSelected();
                    System.out.println("shift " + shift.toString() + "");
                }
                String pincode = edtzipcode.getText().toString();
                if (pincode == null && pincode.equals("")) {
                    Toast.makeText(ctx, "Please Enter PinCode", Toast.LENGTH_LONG).show();
                } else if (pincode.trim().length() != 5) {
                    Toast.makeText(ctx, "Please Enter Valid Pincode", Toast.LENGTH_LONG).show();
                } else {
                    if (specialityAdapter.getSelected().size() > 5) {
                        //Toast.makeText(ctx,"Please Select ")
                    }
                    getttingSearchDataFromApi(pincode, selectedDays_to_Send);
                }

                break;
            case R.id.btn_clearsearch:
                if (hoursAdapter != null) {
                    hoursAdapter.clearSeletedHR();
                    hoursAdapter.notifyDataSetChanged();
                }
                if (certificationAdapter != null) {
                    certificationAdapter.clearSelected();
                    certificationAdapter.notifyDataSetChanged();
                }
                if (licenseAdapter != null) {
                    licenseAdapter.clearSelected();
                    licenseAdapter.notifyDataSetChanged();
                }
                if (specialityAdapter != null) {
                    specialityAdapter.clearSelected();
                    specialityAdapter.notifyDataSetChanged();
                }
                if (shiftAdapter != null) {
                    shiftAdapter.clearSelected();
                    shiftAdapter.notifyDataSetChanged();
                }
                if (_daDays_checkBoxAdapter != null) {
                    EditPostionFragment.DAYS_EVENING_NIGHT = 0;
                    EditPostionFragment.HOURS_FULL_PART_PRN_WEEKENDS = 0;
                    _daDays_checkBoxAdapter.clearSelected();
                    _daDays_checkBoxAdapter.notifyDataSetChanged();
                }
                if (_daevening_checkBoxAdapter != null) {
                    EditPostionFragment.DAYS_EVENING_NIGHT = 0;
                    EditPostionFragment.HOURS_FULL_PART_PRN_WEEKENDS = 0;
                    _daevening_checkBoxAdapter.clearSelected();
                    _daevening_checkBoxAdapter.notifyDataSetChanged();
                }
                if (_daNightcheckBoxAdapter != null) {
                    EditPostionFragment.DAYS_EVENING_NIGHT = 0;
                    EditPostionFragment.HOURS_FULL_PART_PRN_WEEKENDS = 0;
                    _daNightcheckBoxAdapter.clearSelected();
                    _daNightcheckBoxAdapter.notifyDataSetChanged();
                }


                break;
            case R.id.btnNearMe:
                onNearMeButtonClicked();
                break;

        }
    }

    /**
     * this method is called when use clicked nearme button
     */
    public void onNearMeButtonClicked() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (Validations.hasPermissions(ctx, Validations.RUN_TIME_PERMITIONS_LOCATION)) {
                processAfterPermmionGranted();
            } else {
                ActivityCompat.requestPermissions(getActivity(), Validations.RUN_TIME_PERMITIONS_LOCATION, Validations.REQUEST_CODE_PERMISSION);
            }
        } else {
            processAfterPermmionGranted();
        }


    }

    /**
     * getting pincode from latlong and set to edit text
     */
    public void processAfterPermmionGranted() {
        gpsTracker = new GPSTracker(ctx);
        if (gpsTracker.canGetLocation()) {
            //easyWayLocation.

            Log.d(TAG, "processAfterPermmionGranted: latitude " + easyWayLocation.getLatitude() + "longitude:" + easyWayLocation.getLongitude());
            if (gpsTracker.getLatitude() > 0.0 && gpsTracker.getLongitude() > 0.0) {
                try {
                    edtzipcode.setText(getPostalCodeByCoordinates(ctx, gpsTracker.getLatitude(), gpsTracker.getLongitude()));
                } catch (Exception ex) {
                    Log.d("SearchFragment ", "" + ex.getMessage());
                }
            } else {
                Toast.makeText(ctx, "GPS Prepairing. It Take 30 to 60 Seconds.Please Try Again ", Toast.LENGTH_LONG).show();
            }
        } else {

            DialogUtils.showDialog4Activity(ctx, "GPS is settings", "GPS is not enabled. Do you want to go to settings menu?", new DialogUtils.DailogCallBackOkButtonClick() {
                @Override
                public void onDialogOkButtonClicked() {
                    Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                    startActivity(intent);
                }
            });
            //gpsTracker.showSettingsAlert();

        }
    }

    /**
     * this method is used to getting pincode from latlong
     *
     * @param context context
     * @param lat     latitude
     * @param lon     longitude
     * @return pincode if not found than return blank data
     * @throws IOException
     */

    public static String getPostalCodeByCoordinates(Context context, double lat, double lon) throws IOException {

        Geocoder mGeocoder = new Geocoder(context, Locale.getDefault());
        String zipcode = null;
        Address address = null;

        if (mGeocoder != null) {

            List<Address> addresses = mGeocoder.getFromLocation(lat, lon, 5);

            if (addresses != null && addresses.size() > 0) {

                for (int i = 0; i < addresses.size(); i++) {
                    address = addresses.get(i);
                    if (address.getPostalCode() != null) {
                        zipcode = address.getPostalCode();
                        Log.d("SearchFragment", "Postal code: " + address.getPostalCode());
                        break;
                    }

                }
                return zipcode;
            }
        }

        return "";
    }

    /**
     * this method called after user clicked on search button
     *
     * @param pinCode
     */
    public void getttingSearchDataFromApi(final String pinCode, final String selectedDays_to_Send) {
        System.out.println("IN TO SEARCH::::* " + _daDays_checkBoxAdapter.selected + "");
        progressDialog.show();

        List<String> speciality_list = specialityAdapter.getSelected();
        String speciality = TextUtils.join(",", speciality_list);
        List<String> license_list = licenseAdapter.getSelected();
        String liscenStringList = TextUtils.join(",", license_list);
        List<String> certification_list = new ArrayList<>();
        if (certificationAdapter != null) {
            certification_list = certificationAdapter.getSelected();
        }
        String certificationList = TextUtils.join(",", certification_list);
        List<String> shift = shiftAdapter.getSelected();
        String shiftString = TextUtils.join(",", shift);
        List<String> hoursList = hoursAdapter.getSelectedHr();
        String stringHours = TextUtils.join(",", hoursList);

        String Url = URLS.SEARCH_POSITIONS_FOR_APPLICANT + URLS.APPLICANT_ZIP_CODE + "=" + mySharedPrefereces.getZipCode();
        if (speciality_list.size() > 0) {
            Url = Url + "&" + URLS.SPECIALTY + "=" + speciality;
        }
        if (license_list.size() > 0) {
            Url = Url + "&" + URLS.LICENSE + "=" + liscenStringList;
        }
        if (certification_list.size() > 0)
            Url = Url + "&" + URLS.CERTIFICATION + "=" + certificationList;
        if (shift.size() > 0)
            Url = Url + "&" + URLS.SHIFT + "=" + shiftString;
        if (hoursList.size() > 0)
            Url = Url + "&" + URLS.HOURS + "=" + stringHours;

//        Url = Url + "&" + URLS.ZIP_CODE + "=" + pinCode;
        Url = Url + "&" + URLS.ZIP_CODE + "=" + pinCode + "&" + selectedDays_to_Send + "";
        // if(tvwithin.getSelectedItemPosition()!=0)
        Url = Url + "&" + URLS.MILES + "=" + countryNames[tvwithin.getSelectedItemPosition()];

        URL url = null;
        try {
            url = new URL(Url);
            URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef());
            url = uri.toURL();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "getttingSearchDataFromApi: URL:- " + url.toString());
        CommonRequestHelper.getDataFromUrl(url.toString(), new CommonRequestHelper.VolleyResponseCustom() {
            @Override
            public void getServerResponse(String response) {
                Log.d("SearchFragment", response);
                Log.d(TAG, "getttingSearchDataFromApi: Response:- " + response);
                progressDialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getInt("status") == 1) {
                        Gson gson = new Gson();

                        applicantNearPositionModel = gson.fromJson(response, ApplicantNearPositionModel.class);
                        if (applicantNearPositionModel != null) {
                            if (applicantNearPositionModel.getData().size() != 0) {
                                startActivity(new Intent(getContext(), SearchPositionDetailActivity.class));
                                /*if(ApplicantNearPostionFragment.applicantNearPostionFragment!=null)
                                {
                                    ApplicantNearPostionFragment.applicantNearPostionFragment.applicantNearPositionModelList.clear();
                                    ApplicantNearPostionFragment.applicantNearPostionFragment.applicantNearPositionModelList.addAll(applicantNearPositionModel.getData());
                                    ApplicantNearPostionFragment.applicantNearPostionFragment.applicantNearPositionAdapter.notifyDataSetChanged();
                                    DashBoardActivity.dashBoardActivity.viewpager.setCurrentItem(0);
                                }*/
                            } else {
//                                DialogUtils.showDialog(ctx, ctx.getResources().getString(R.string.app_name),
//                                        "No Position Found!");
                                DialogUtils.showDialog(ctx, ctx.getResources().getString(R.string.app_name),
                                        "No result found, please try again");
                            }
                        }

                        /*JSONArray data = jsonObject.getJSONArray("data");
                        if(data.length()==0)
                        {
                            Toast.makeText(ctx,"No Search Data Found",Toast.LENGTH_LONG).show();
                        }*/
                    } else {

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void getVolleyError(VolleyError volleyError) {
                Log.d(TAG, "getttingSearchDataFromApi: Errror:- " + volleyError.getMessage());
                progressDialog.dismiss();
                DialogUtils.noInterNetDialog(ctx, "No Internet Connection", "No active internet connection available", new DialogUtils.NoInterNetDailogClickListner() {
                    @Override
                    public void onDialogOkButtonClicked() {
                        getttingSearchDataFromApi(pinCode, selectedDays_to_Send + "");
                    }

                    @Override
                    public void onDialogCancelButtonClicked() {

                    }
                });


            }
        }, ctx);

    }
}
