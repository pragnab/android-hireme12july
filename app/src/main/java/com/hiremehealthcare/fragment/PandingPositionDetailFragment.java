package com.hiremehealthcare.fragment;


import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.SpannableString;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.hiremehealthcare.CommonCls.CustomBoldTextView;
import com.hiremehealthcare.CommonCls.CustomButton;
import com.hiremehealthcare.CommonCls.CustomTextView;
import com.hiremehealthcare.CommonCls.DialogUtils;
import com.hiremehealthcare.CommonCls.ExpandableHeightGridView;
import com.hiremehealthcare.CommonCls.URLS;
import com.hiremehealthcare.CommonCls.Validations;
import com.hiremehealthcare.POJO.Benefits;
import com.hiremehealthcare.POJO.NearPositionDetailModel;
import com.hiremehealthcare.POJO.PandingPositionModel;
import com.hiremehealthcare.POJO.SharePos;
import com.hiremehealthcare.R;
import com.hiremehealthcare.Utils.CommonRequestHelper;
import com.hiremehealthcare.adapter.DaysMultiSelectAdapter_duplicate_flip;
import com.hiremehealthcare.adapter.Days_adapter;
import com.hiremehealthcare.adapter.Evening_adapter;
import com.hiremehealthcare.adapter.Night_adapter;
import com.hiremehealthcare.database.MySharedPrefereces;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;


/**
 * A simple {@link Fragment} subclass.
 */
public class PandingPositionDetailFragment extends Fragment implements View.OnClickListener {


    PandingPositionModel.Datum data;
    CustomBoldTextView tvFacilityName;
    CustomTextView tvFacilityAddress, tvFacilitySpeciality, tvFacilityLicense, tvFacilityCertificate, tvFacilityShift, tvFacilityHours, tvFacilityExperienceRequired, tvFacilityBenefits, tvFacilityDescription;
    CustomButton btnViewOnTheMap;
    ProgressDialog progressDialog;
    CustomButton btnPositionDetailSharePositionDetail;
    CustomButton bntIamInterested;
    CustomTextView tvFacilityBenefitsExtra;
    MySharedPrefereces mySharedPrefereces;
    Context context;
    String TAG="PandingPositionDetailFragment";
    double latitude,longitude;
    private CustomBoldTextView tvavailability;
    private CustomTextView tvDays;
    private CustomTextView tvEvening;
    private android.widget.LinearLayout v;
    private CustomTextView tvNights;
    private com.hiremehealthcare.CommonCls.ExpandableHeightGridView griddays;
    private com.hiremehealthcare.CommonCls.ExpandableHeightGridView griddaystime;
    private com.hiremehealthcare.CommonCls.ExpandableHeightGridView grideveningdaystime;
    private com.hiremehealthcare.CommonCls.ExpandableHeightGridView gridnightdaystime;
    private android.widget.LinearLayout lin;
    private CustomTextView tvdays;
    public PandingPositionDetailFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView= inflater.inflate(R.layout.fragment_panding_position_detail, container, false);
        this.bntIamInterested = (CustomButton) rootView.findViewById(R.id.bntIamInterested);
        this.btnPositionDetailSharePositionDetail = (CustomButton) rootView.findViewById(R.id.btnPositionDetailSharePositionDetail);
        this.tvFacilityDescription = (CustomTextView) rootView.findViewById(R.id.tvFacilityDescription);
        this.tvFacilityBenefitsExtra = (CustomTextView) rootView.findViewById(R.id.tvFacilityBenefitsExtra);
        this.tvFacilityBenefits = (CustomTextView) rootView.findViewById(R.id.tvFacilityBenefits);
        this.tvFacilityExperienceRequired = (CustomTextView) rootView.findViewById(R.id.tvFacilityExperienceRequired);
        this.tvdays = (CustomTextView) rootView.findViewById(R.id.tvdays);
        this.lin = (LinearLayout) rootView.findViewById(R.id.lin);
        this.gridnightdaystime = (ExpandableHeightGridView) rootView.findViewById(R.id.grid_night_days_time);
        this.grideveningdaystime = (ExpandableHeightGridView) rootView.findViewById(R.id.grid_evening_days_time);
        this.griddaystime = (ExpandableHeightGridView) rootView.findViewById(R.id.grid_days_time);
        this.griddays = (ExpandableHeightGridView) rootView.findViewById(R.id.grid_days);
        this.tvNights = (CustomTextView) rootView.findViewById(R.id.tv_Nights);
        griddays.setExpanded(true);
        grideveningdaystime.setExpanded(true);
        gridnightdaystime.setExpanded(true);
        griddays.setExpanded(true);
        this.v = (LinearLayout) rootView.findViewById(R.id.v);
        this.tvEvening = (CustomTextView) rootView.findViewById(R.id.tv_Evening);
        this.tvDays = (CustomTextView) rootView.findViewById(R.id.tv_Days);
        this.tvavailability = (CustomBoldTextView) rootView.findViewById(R.id.tv_availability);
        this.tvFacilityHours = (CustomTextView) rootView.findViewById(R.id.tvFacilityHours);
        this.tvFacilityShift = (CustomTextView) rootView.findViewById(R.id.tvFacilityShift);
        this.tvFacilityCertificate = (CustomTextView) rootView.findViewById(R.id.tvFacilityCertificate);
        this.tvFacilityLicense = (CustomTextView) rootView.findViewById(R.id.tvFacilityLicense);
        this.tvFacilitySpeciality = (CustomTextView) rootView.findViewById(R.id.tvFacilitySpeciality);
        this.btnViewOnTheMap = (CustomButton) rootView.findViewById(R.id.btnViewOnTheMap);
        this.tvFacilityAddress = (CustomTextView) rootView.findViewById(R.id.tvFacilityAddress);
        this.tvFacilityName = (CustomBoldTextView) rootView.findViewById(R.id.tvFacilityName);
        context =getContext();
        data = (PandingPositionModel.Datum)getArguments().getSerializable("data");
        initAllControls(rootView);
        return rootView;
    }

    public void initAllControls(View rootView)
    {
        mySharedPrefereces = new MySharedPrefereces(context);
        progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        SpannableString spannableString =  new SpannableString(getString(R.string.please_wait));
        Typeface typefaceSpan = Validations.setTypeface(context);
        spannableString.setSpan(typefaceSpan, 0, getString(R.string.please_wait).length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        progressDialog.setMessage(spannableString);
        btnPositionDetailSharePositionDetail = (CustomButton) rootView.findViewById(R.id.btnPositionDetailSharePositionDetail);
        bntIamInterested = (CustomButton) rootView.findViewById(R.id.bntIamInterested);
        btnViewOnTheMap = (CustomButton) rootView.findViewById(R.id.btnViewOnTheMap);
        btnViewOnTheMap.setOnClickListener(this);
        tvFacilityBenefitsExtra = (CustomTextView) rootView.findViewById(R.id.tvFacilityBenefitsExtra);
        tvFacilityName = (CustomBoldTextView) rootView.findViewById(R.id.tvFacilityName);
        tvFacilityAddress = (CustomTextView) rootView.findViewById(R.id.tvFacilityAddress);
        tvFacilitySpeciality = (CustomTextView) rootView.findViewById(R.id.tvFacilitySpeciality);
        tvFacilityLicense = (CustomTextView) rootView.findViewById(R.id.tvFacilityLicense);
        tvFacilityCertificate = (CustomTextView) rootView.findViewById(R.id.tvFacilityCertificate);
        tvFacilityShift = (CustomTextView) rootView.findViewById(R.id.tvFacilityShift);
        tvFacilityHours = (CustomTextView) rootView.findViewById(R.id.tvFacilityHours);
        tvFacilityExperienceRequired = (CustomTextView) rootView.findViewById(R.id.tvFacilityExperienceRequired);
        tvFacilityBenefits = (CustomTextView) rootView.findViewById(R.id.tvFacilityBenefits);
        tvFacilityDescription = (CustomTextView) rootView.findViewById(R.id.tvFacilityDescription);
        btnPositionDetailSharePositionDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                shareData("hello");
                   /*11-july Pragna*/
                //  shareData(nearPositionDetailModel.getData().getPositionId() + "&applicant_id=" + mySharedPrefereces.getUserId());
                shareData(data.getPositionId() + "", mySharedPrefereces.getUserId() + "");
            }
        });
        bntIamInterested.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                IAmInterested();
            }
        });
        tvFacilityName.setText(data.getFacilityName());
        tvFacilityAddress.setText(data.getFacilityAddress());
       /*  tvFacilitySpeciality.setText(data.getSpecialtyName());
        tvFacilityLicense.setText(data.getLicenseName());
        tvFacilityCertificate.setText(data.getCertificateName());
        tvFacilityShift.setText(data.getSpecialtyName());
        tvFacilityHours.setText(data.getHourName());
        tvFacilityExperienceRequired.setText("-");
        String temp = data.getBenefitsName().replace(",", "\n");
        tvFacilityBenefits.setText(temp);
        tvFacilityDescription.setText(data.getPositionDescription());*/
        getApplicantNearPositionDetail();
    }

   /* public void shareData(String data)
    {
        Intent sendIntent = new Intent();
        sendIntent.setAction(Intent.ACTION_SEND);
        sendIntent.putExtra(Intent.EXTRA_TEXT, ""+data);
        sendIntent.setType("text/plain");
        startActivity(sendIntent);
    }*/
    SharePos sharePos;
    /**
     * DISPLAY DIALOG
     **/
    public static void showDialog(final Context context, String title, String message, final SharePos sharePos) {

        LayoutInflater inflater = LayoutInflater.from(context);
        final View dialogView = inflater.inflate(R.layout.sharepos_fragment, null);
        ImageButton btnlink = (ImageButton) dialogView.findViewById(R.id.btn_link);
        ImageButton btngplus = (ImageButton) dialogView.findViewById(R.id.btn_gplus);
        ImageButton btntwitter = (ImageButton) dialogView.findViewById(R.id.btn_twitter);
        ImageButton btnfb = (ImageButton) dialogView.findViewById(R.id.btn_fb);

        final AlertDialog.Builder builder = new AlertDialog.Builder(context);

        //  final AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.myDialog));
        final AlertDialog b = builder.create();
        //  builder.setTitle("Material Style Dialog");
        builder.setCancelable(true);
        builder.setView(dialogView);
        b.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        final AlertDialog show = builder.show();
        btnlink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                show.dismiss();
                if (sharePos.getMessage().getLinkedin_url().length() >= 5) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(sharePos.getMessage().getLinkedin_url() + ""));
                    context.startActivity(browserIntent);
                }

            }
        });
        btngplus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                show.dismiss();
                if (sharePos.getMessage().getGoogle_plus_url().length() >= 5) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(sharePos.getMessage().getGoogle_plus_url() + ""));
                    context.startActivity(browserIntent);
                }
            }
        });
        btntwitter.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                show.dismiss();
                if (sharePos.getMessage().getTwitter_url().length() >= 5) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(sharePos.getMessage().getTwitter_url() + ""));
                    context.startActivity(browserIntent);
                }
            }
        });
        btnfb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                show.dismiss();
                if (sharePos.getMessage().getFacebook_url().length() >= 5) {
                    Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(sharePos.getMessage().getFacebook_url() + ""));
                    context.startActivity(browserIntent);
                }
            }
        });
    }

    public void shareData(String position_id, String applicant_id) {
        /*   //Eg.http://hiremehealthcare.iipldemo.com/app/api.php?type=share_position&position_id=50&applicant_id=40*/
//        Intent sendIntent = new Intent();
//        sendIntent.setAction(Intent.ACTION_SEND);
//        sendIntent.putExtra(Intent.EXTRA_TEXT, "" + data);
//        sendIntent.setType("text/plain");
//        startActivity(sendIntent);
       progressDialog.show();
       String url = URLS.SHARE_POSITION + position_id + "&applicant_id=" + applicant_id + "";
       Log.d(TAG, "shareData: URL:-" + url);
       CommonRequestHelper.getDataFromUrl(url, new CommonRequestHelper.VolleyResponseCustom() {

           @Override
           public void getServerResponse(String response) {
               progressDialog.dismiss();
               Log.d(TAG, "shareData: REsponse:" + response);

               Gson gson = new Gson();


               sharePos = gson.fromJson(response, SharePos.class);
               if (sharePos.getStatus() == 1) {
                   showDialog(context, "", "", sharePos);
//                    tvFacilityName.setText(nearPositionDetailModel.getData().getFacilityName());
//                    String address = nearPositionDetailModel.getData().getFacilityAddress() + "\n" + nearPositionDetailModel.getData().getFacilityCity() + " " + nearPositionDetailModel.getData().getFacilityState() + " " + nearPositionDetailModel.getData().getFacilityZipcode();
//                    tvFacilityAddress.setText(address);
//                    tvFacilitySpeciality.setText(nearPositionDetailModel.getData().getSpecialtyName());
//                    tvFacilityLicense.setText(nearPositionDetailModel.getData().getLicenseName());
//                    tvFacilityCertificate.setText(nearPositionDetailModel.getData().getCertificateName());
//                    tvFacilityShift.setText(nearPositionDetailModel.getData().getShiftName());
//                    tvFacilityHours.setText(nearPositionDetailModel.getData().getHourName());
//                    String experienceRegired = "";
//                    if (!nearPositionDetailModel.getData().getYear().equals("0"))
//                        experienceRegired = nearPositionDetailModel.getData().getYear() + " Year ";
//                    if (!nearPositionDetailModel.getData().getMonth().equals("0")) {
//                        if (!experienceRegired.equals(""))
//                            experienceRegired = experienceRegired + "And " + nearPositionDetailModel.getData().getMonth() + " Month";
//                        else
//                            experienceRegired = nearPositionDetailModel.getData().getMonth() + " Month";
//                    }
//                    tvFacilityExperienceRequired.setText(experienceRegired);
//                    String temp = nearPositionDetailModel.getData().getBenefitsName().replace(",", "\n");
//                    tvFacilityBenefits.setText(temp);
//                    tvFacilityDescription.setText(nearPositionDetailModel.getData().getPositionDescription());
//                    if (nearPositionDetailModel.getData().getExtraBenefitsName() != null && !nearPositionDetailModel.getData().getExtraBenefitsName().equals("null") && !nearPositionDetailModel.getData().getExtraBenefitsName().equals("")) {
//                        tvFacilityBenefitsExtra.setVisibility(View.VISIBLE);
//                        tvFacilityBenefitsExtra.setText(nearPositionDetailModel.getData().getExtraBenefitsName().replace(",", "\n"));
//
//                    } else {
//                        tvFacilityBenefitsExtra.setVisibility(View.GONE);
//                    }
//                   /* if(nearPositionDetailModel.getData().size()==0)
//                        Toast.makeText(context,"No data found",Toast.LENGTH_LONG).show();*/
//                    // applicantNearPositionModelList.addAll(applicantNearPositionDetailModel.getData());
//                    // applicantNearPositionAdapter.notifyDataSetChanged();
               } else {

               }
                /*ApplicantNearPositionModel applicantNearPositionDetailModel = gson.fromJson(response, ApplicantNearPositionModel.class);
                if(applicantNearPositionDetailModel.getStatus()==1)
                {
                    if(applicantNearPositionDetailModel.getData().size()==0)
                        Toast.makeText(context,"No data found",Toast.LENGTH_LONG).show();
                    applicantNearPositionModelList.addAll(applicantNearPositionDetailModel.getData());
                    applicantNearPositionAdapter.notifyDataSetChanged();
                }
                else
                {

                }*/
           }

           @Override
           public void getVolleyError(VolleyError volleyError) {
               progressDialog.dismiss();
               Log.d(TAG, "getApplicantNeatPosition: Error:" + volleyError.getMessage());
               NetworkResponse networkResponse = volleyError.networkResponse;
               DialogUtils.noInterNetDialog(context, "No Internet Connection", "No active internet connection available", new DialogUtils.NoInterNetDailogClickListner() {
                   @Override
                   public void onDialogOkButtonClicked() {
                //       getApplicantNeatPosition();
                   }

                   @Override
                   public void onDialogCancelButtonClicked() {

                   }
               });

           }
       }, context);
   }


    public void IAmInterested()
    {
        progressDialog.show();
        String url= URLS.APPLICANT_SHOW_INTEREST_ON_POSITION+mySharedPrefereces.getUserId()+"&"+URLS.POSITION_ID+"="+data.getPositionId();
        Log.d("ApplicantNear", "Url" + url);
        CommonRequestHelper.getDataFromUrl(url, new CommonRequestHelper.VolleyResponseCustom() {

            @Override
            public void getServerResponse(String response) {
                progressDialog.dismiss();
                Log.d("ApplicantNearPosition", response);
                Gson gson = new Gson();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int status = jsonObject.getInt("status");
                    String message = jsonObject.getString("message");
                    DialogUtils.showDialog4Activity(getActivity(), context.getResources().getString(R.string.app_name),message, null);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                /*ApplicantNearPositionModel applicantNearPositionModel = gson.fromJson(response, ApplicantNearPositionModel.class);
                if(applicantNearPositionModel.getStatus()==1)
                {
                    if(applicantNearPositionModel.getData().size()==0)
                        Toast.makeText(context,"No data found",Toast.LENGTH_LONG).show();
                    applicantNearPositionModelList.addAll(applicantNearPositionModel.getData());
                    applicantNearPositionAdapter.notifyDataSetChanged();
                }
                else
                {

                }*/
            }

            @Override
            public void getVolleyError(VolleyError volleyError) {
                progressDialog.dismiss();
                NetworkResponse networkResponse = volleyError.networkResponse;

            }
        },context);

    }

    /**
     * this method getting applicant near position detail data
     */

    public void getApplicantNearPositionDetail()
    {

        progressDialog.show();
        String url= URLS.APPLICANT_NEAR_POSITION_DETAIL+data.getPositionId();
        Log.d(TAG, "getApplicantNearPositionDetail: URL  " + url);
        CommonRequestHelper.getDataFromUrl(url, new CommonRequestHelper.VolleyResponseCustom() {

            @Override
            public void getServerResponse(String response) {
                progressDialog.dismiss();
                Log.d(TAG, "getApplicantNearPositionDetail: Response  " + response);
                Gson gson = new Gson();
                NearPositionDetailModel nearPositionDetailModel = gson.fromJson(response, NearPositionDetailModel.class);
                if (nearPositionDetailModel.getStatus() == 1) {
                    if (nearPositionDetailModel.getData() != null) {
                    /*tvFacilityName.setText(nearPositionDetailModel.getData().getPositionTitle());
                    tvFacilityAddress.setText(nearPositionDetailModel.getData().getFacilityAddress());*/
                        tvFacilityName.setText(nearPositionDetailModel.getData().getFacilityName() + "");
                        String address = nearPositionDetailModel.getData().getFacilityAddress() + "\n" + nearPositionDetailModel.getData().getFacilityCity() + " " + nearPositionDetailModel.getData().getFacilityState() + " " + nearPositionDetailModel.getData().getFacilityZipcode();
                        tvFacilityAddress.setText(address);
                        tvFacilitySpeciality.setText(nearPositionDetailModel.getData().getSpecialtyName());
                        tvFacilityLicense.setText(nearPositionDetailModel.getData().getLicenseName());
                        tvFacilityCertificate.setText(nearPositionDetailModel.getData().getCertificateName());
                        tvFacilityShift.setText(nearPositionDetailModel.getData().getShiftName());
                        tvFacilityHours.setText(nearPositionDetailModel.getData().getHourName());
                        availability(nearPositionDetailModel);
                        String experienceRegired = "";
                        if (!nearPositionDetailModel.getData().getYear().equals("0"))
                            experienceRegired = nearPositionDetailModel.getData().getYear() + " Year ";
                        if (!nearPositionDetailModel.getData().getMonth().equals("0")) {
                            if (!experienceRegired.equals(""))
                                experienceRegired = experienceRegired + "And " + nearPositionDetailModel.getData().getMonth() + " Month";
                            else
                                experienceRegired = nearPositionDetailModel.getData().getMonth() + " Month";
                        }
                        tvFacilityExperienceRequired.setText(experienceRegired);
                        //tvFacilityExperienceRequired.setText(nearPositionDetailModel.getData().getYear()+" Year And "+nearPositionDetailModel.getData().getMonth()+" Months");
                        String temp = nearPositionDetailModel.getData().getBenefitsName().replace(",", "\n");
                        tvFacilityBenefits.setText(temp);
                        tvFacilityDescription.setText(nearPositionDetailModel.getData().getPositionDescription());
                        if (nearPositionDetailModel.getData().getExtraBenefitsName() != null && !nearPositionDetailModel.getData().getExtraBenefitsName().equals("null") && !nearPositionDetailModel.getData().getExtraBenefitsName().equals("")) {
                            tvFacilityBenefitsExtra.setVisibility(View.VISIBLE);
//                        tvFacilityBenefitsExtra.setText(nearPositionDetailModel.getData().getExtraBenefitsName().replace(",", "\n"));
                            tvFacilityBenefitsExtra.setText(nearPositionDetailModel.getData().getExtraBenefitsChecked().toString().replace(",", "\n"));

                        } else {
                            tvFacilityBenefitsExtra.setVisibility(View.GONE);
                        }
                   /* if(nearPositionDetailModel.getData().size()==0)
                        Toast.makeText(context,"No data found",Toast.LENGTH_LONG).show();*/
                        // applicantNearPositionModelList.addAll(applicantNearPositionDetailModel.getData());
                        // applicantNearPositionAdapter.notifyDataSetChanged();
                    } else {

                    }
                /*ApplicantNearPositionModel applicantNearPositionDetailModel = gson.fromJson(response, ApplicantNearPositionModel.class);
                if(applicantNearPositionDetailModel.getStatus()==1)
                {
                    if(applicantNearPositionDetailModel.getData().size()==0)
                        Toast.makeText(context,"No data found",Toast.LENGTH_LONG).show();
                    applicantNearPositionModelList.addAll(applicantNearPositionDetailModel.getData());
                    applicantNearPositionAdapter.notifyDataSetChanged();
                }
                else
                {

                }*/
                }
            }

            @Override
            public void getVolleyError(VolleyError volleyError) {
                progressDialog.dismiss();
                Log.d(TAG, "getApplicantNearPositionDetail: Volley" + volleyError.getMessage());
                NetworkResponse networkResponse = volleyError.networkResponse;
                DialogUtils.noInterNetDialog(context, "No Internet Connection", "No active internet connection available", new DialogUtils.NoInterNetDailogClickListner() {
                    @Override
                    public void onDialogOkButtonClicked() {
                        //getApplicantNearPositionDetail();
                    }

                    @Override
                    public void onDialogCancelButtonClicked() {

                    }
                });

            }
        },context);
    }


    @Override
    public void onClick(View view) {
        if(view==btnViewOnTheMap)
        {
            if(data.getFacilityAddress()!=null && !data.getFacilityAddress().equals("")) {
                Intent searchAddress = new Intent(Intent.ACTION_VIEW, Uri.parse("geo:0,0?q=" + data.getFacilityAddress()));
                startActivity(searchAddress);
            }
            else
            {
                Toast.makeText(context,"No Address Found",Toast.LENGTH_LONG).show();
            }
        }
    }

    public void getLocationFromAddress(String strAddress){

        Geocoder coder = new Geocoder(context);
        List<Address> address;


        try {
            address = coder.getFromLocationName(strAddress,5);
            if (address==null) {
                return ;
            }
            Address location=address.get(0);
            latitude=location.getLatitude();
            longitude=location.getLongitude();


        }
        catch (Exception ex)
        {

        }
    }
    private void availability( NearPositionDetailModel datamodel) {
        ArrayList<String> days = new ArrayList<>();
        NearPositionDetailModel.Data data =datamodel.getData();
        //   getavailability(data.getPositionId() + "");
        days = new ArrayList<>();

        Days_adapter.selected=new ArrayList<>();
        Days_adapter.selected_name=new ArrayList<>();

        Night_adapter.selected=new ArrayList<>();
        Night_adapter.selected_name=new ArrayList<>();


        Evening_adapter.selected=new ArrayList<>();
        Evening_adapter.selected_name=new ArrayList<>();

        days.add("Mon");
        days.add("Tue");
        days.add("Wed");
        days.add("Thu");
        days.add("Fri");
        days.add("Sat");
        days.add("Sun");
        System.out.println("availability MON : " + data.getMon() + " TUE : " + data.getTue() + " WED " + data.getWed() + " THR " + data.getThu() + " FRI " + data.getFri() + "");
        //   List<String> items = Arrays.asList(data.getMon().split("\\s*,\\s*"));

//PostNewPostion.MON.addAll(Arrays.asList(data.getMon().toLowerCase().split("\\s+")));
        List<String> items = new ArrayList<>();
//                    items.addAll(Arrays.asList(data.getMon().toLowerCase().split("\\s+")));
        items=    Arrays.asList(data.getMon().split("\\s*,\\s*"));
        //  PostNewPostion.MON = new ArrayList<Integer>(items.size());
        PostNewPostion.MON = new ArrayList<>();
        List<Integer> items_int = new ArrayList<>();
        for (int i = 0; i < items.size(); i++) {
            if(  isNumeric(items.get(i) + "")) {
                items_int.add(Integer.parseInt(items.get(i) + ""));
                if (items_int.get(i).equals(1)) {
                    Days_adapter.selected.add("1");
                    Days_adapter.selected_name.add("Mon");
                }
                if (items_int.get(i).equals(2)) {
                    Evening_adapter.selected.add("1");
                    Evening_adapter.selected_name.add("Mon");
                }
                if (items_int.get(i).equals(3)) {
                    Night_adapter.selected.add("1");
                    Night_adapter.selected_name.add("Mon");
                }
            }
        }
        //  List<String> items_tue = new ArrayList<>();
//                    items_tue=(Arrays.asList(data.getTue().split("\\s+")));
        List<String> items_tue = Arrays.asList(data.getTue().split("\\s*,\\s*"));

        //  PostNewPostion.MON = new ArrayList<Integer>(items.size());
        PostNewPostion.TUE = new ArrayList<>();
        List<Integer> items_int_tue = new ArrayList<>();
        //    System.out.println("TUE ```````````@ "+items_tue.toString()+"");
        for (int i = 0; i < items_tue.size(); i++) {
            System.out.println("items_tue.get(i) ````@@@@@@@@@@ "+ items_tue.get(i) + "");
            if(  isNumeric(items_tue.get(i) + "")) {
                items_int_tue.add(Integer.parseInt(items_tue.get(i) + ""));
                if (items_int_tue.get(i).equals(1)) {
                    Days_adapter.selected.add("2");
                    Days_adapter.selected_name.add("Tue");
                }
                if (items_int_tue.get(i).equals(2)) {
                    Evening_adapter.selected.add("2");
                    Evening_adapter.selected_name.add("Tue");
                }
                if (items_int_tue.get(i).equals(3)) {
                    Night_adapter.selected.add("2");
                    Night_adapter.selected_name.add("Tue");
                }
            }
        }


        List<String> items_wed = Arrays.asList(data.getWed().split("\\s*,\\s*"));

        //  PostNewPostion.MON = new ArrayList<Integer>(items.size());
        PostNewPostion.WED = new ArrayList<>();
        List<Integer> items_int_wed = new ArrayList<>();
        //   System.out.println("WED ```````````@ "+items_wed.toString()+"");
        for (int i = 0; i < items_wed.size(); i++) {
//                        System.out.println("items_tue.get(i) ````@@@@@@@@@@ "+ items_tue.get(i) + "");
            if(  isNumeric(items_wed.get(i) + "")) {
                items_int_wed.add(Integer.parseInt(items_wed.get(i) + ""));
                if (items_int_wed.get(i).equals(1)) {
                    Days_adapter.selected.add("3");
                    Days_adapter.selected_name.add("Wed");
                }
                if (items_int_wed.get(i).equals(2)) {
                    Evening_adapter.selected.add("3");
                    Evening_adapter.selected_name.add("Wed");
                }
                if (items_int_wed.get(i).equals(3)) {
                    Night_adapter.selected.add("3");
                    Night_adapter.selected_name.add("Wed");
                }
            }
        }


        List<String> items_Thr = Arrays.asList(data.getThu().split("\\s*,\\s*"));

        //  PostNewPostion.MON = new ArrayList<Integer>(items.size());
        PostNewPostion.THR = new ArrayList<>();
        List<Integer> items_int_thr = new ArrayList<>();
        //  System.out.println("THRU ```````````@ "+items_Thr.toString()+"");
        for (int i = 0; i < items_Thr.size(); i++) {
//                        System.out.println("items_tue.get(i) ````@@@@@@@@@@ "+ items_tue.get(i) + "");
            if(  isNumeric(items_Thr.get(i) + "")) {
                items_int_thr.add(Integer.parseInt(items_Thr.get(i) + ""));
                if (items_int_thr.get(i).equals(1)) {
                    Days_adapter.selected.add("4");
                    Days_adapter.selected_name.add("Thr");
                }
                if (items_int_thr.get(i).equals(2)) {
                    Evening_adapter.selected.add("4");
                    Evening_adapter.selected_name.add("Thr");
                }
                if (items_int_thr.get(i).equals(3)) {
                    Night_adapter.selected.add("4");
                    Night_adapter.selected_name.add("Thr");
                }
            }
        }


        List<String> items_Fri = Arrays.asList(data.getFri().split("\\s*,\\s*"));

        //  PostNewPostion.MON = new ArrayList<Integer>(items.size());
        PostNewPostion.FRI = new ArrayList<>();
        List<Integer> items_int_fri = new ArrayList<>();
        //   System.out.println("FRID ```````````@ "+items_Fri.toString()+"");
        for (int i = 0; i < items_Fri.size(); i++) {
//                        System.out.println("items_tue.get(i) ````@@@@@@@@@@ "+ items_tue.get(i) + "");
            if(  isNumeric(items_Fri.get(i) + "")) {
                items_int_fri.add(Integer.parseInt(items_Fri.get(i) + ""));
                if (items_int_fri.get(i).equals(1)) {
                    Days_adapter.selected.add("5");
                    Days_adapter.selected_name.add("Fri");
                }
                if (items_int_fri.get(i).equals(2)) {
                    Evening_adapter.selected.add("5");
                    Evening_adapter.selected_name.add("Fri");
                }
                if (items_int_fri.get(i).equals(3)) {
                    Night_adapter.selected.add("5");
                    Night_adapter.selected_name.add("Fri");
                }
            }
        }


        List<String> items_Sat = Arrays.asList(data.getSat().split("\\s*,\\s*"));

        //  PostNewPostion.MON = new ArrayList<Integer>(items.size());
        PostNewPostion.FRI = new ArrayList<>();
        List<Integer> items_int_sat = new ArrayList<>();
        //   System.out.println("SAT ```````````@ "+items_Sat.toString()+"");
        for (int i = 0; i < items_Sat.size(); i++) {
//                        System.out.println("items_tue.get(i) ````@@@@@@@@@@ "+ items_tue.get(i) + "");
            if(  isNumeric(items_Sat.get(i) + "")) {
                items_int_sat.add(Integer.parseInt(items_Sat.get(i) + ""));
                if (items_int_sat.get(i).equals(1)) {
                    Days_adapter.selected.add("6");
                    Days_adapter.selected_name.add("Sat");
                }
                if (items_int_sat.get(i).equals(2)) {
                    Evening_adapter.selected.add("6");
                    Evening_adapter.selected_name.add("Sat");
                }
                if (items_int_sat.get(i).equals(3)) {
                    Night_adapter.selected.add("6");
                    Night_adapter.selected_name.add("Sat");
                }
            }
        }


        List<String> items_Sun = Arrays.asList(data.getSun().split("\\s*,\\s*"));

        //  PostNewPostion.MON = new ArrayList<Integer>(items.size());
        PostNewPostion.FRI = new ArrayList<>();
        List<Integer> items_int_sun = new ArrayList<>();
        //   System.out.println("SUN ```````````@ "+items_Sun.toString()+"");
        for (int i = 0; i < items_Sun.size(); i++) {
//                        System.out.println("items_tue.get(i) ````@@@@@@@@@@ "+ items_tue.get(i) + "");
            if(  isNumeric(items_Sun.get(i) + "")) {
                items_int_sun.add(Integer.parseInt(items_Sun.get(i) + ""));
                if (items_int_sun.get(i).equals(1)) {
                    Days_adapter.selected.add("7");
                    Days_adapter.selected_name.add("Sun");
                }
                if (items_int_sun.get(i).equals(2)) {
                    Evening_adapter.selected.add("7");
                    Evening_adapter.selected_name.add("Sun");
                }
                if (items_int_sun.get(i).equals(3)) {
                    Night_adapter.selected.add("7");
                    Night_adapter.selected_name.add("Sun");
                }
            }
        }



        PostNewPostion.MON.addAll(items_int);
        PostNewPostion.TUE.addAll(items_int_tue);
        PostNewPostion.WED.addAll(items_int_wed);
        PostNewPostion.THR.addAll(items_int_thr);
        PostNewPostion.FRI.addAll(items_int_fri);
        PostNewPostion.SAT.addAll(items_int_sat);
        PostNewPostion.SUN.addAll(items_int_sun);

//        System.out.println("this is monday " + PostNewPostion.MON.toString() + "");
//        System.out.println("this is Tuesday " + PostNewPostion.TUE.toString() + "");
//        System.out.println("this is TuesdayDays_adapter.selected_name``````````@ " + Days_adapter.selected_name .toString()+ "");
//        System.out.println("this is TuesdayEvening_adapter.selected_name``````````@ " + Evening_adapter.selected_name.toString() + "");
        //    v_v = LayoutInflater.from(_parent.getContext()).inflate(R.layout.testforflip, _parent, false);
        //    v_v = LayoutInflater.from(_parent.getContext()).inflate(R.layout.testforflip_availability, _parent, false);
        //  holder.row_item_flipper.removeView(v_v);
        // AnimationFactory.flipTransition(holder.row_item_flipper,
        //  AnimationFactory.FlipDirection.LEFT_RIGHT);




        // holder.row_item_flipper.addView(v_v);
        // initView(v_v);
        DaysMultiSelectAdapter_duplicate_flip _dayDaysMultiSelectAdapter;
        _dayDaysMultiSelectAdapter = new DaysMultiSelectAdapter_duplicate_flip(days, context);
        griddays.setExpanded(true);
        griddays.setAdapter(_dayDaysMultiSelectAdapter);
        Benefits days_all = new Benefits();
        List<Benefits.BenefitsBean> all_day = new ArrayList<>();
        Benefits.BenefitsBean day = new Benefits.BenefitsBean();


        day.setBenefits_id("1");
        day.setBenefits_name("Mon");
        all_day.add(day);

        day = new Benefits.BenefitsBean();
        day.setBenefits_id("2");
        day.setBenefits_name("Tue");
        all_day.add(day);

        day = new Benefits.BenefitsBean();
        day.setBenefits_id("3");
        day.setBenefits_name("Wed");
        all_day.add(day);


        day = new Benefits.BenefitsBean();
        day.setBenefits_id("4");
        day.setBenefits_name("Thu");
        all_day.add(day);


        day = new Benefits.BenefitsBean();
        day.setBenefits_id("5");
        day.setBenefits_name("Fri");
        all_day.add(day);


        day = new Benefits.BenefitsBean();
        day.setBenefits_id("6");
        day.setBenefits_name("Sat");
        all_day.add(day);


        day = new Benefits.BenefitsBean();
        day.setBenefits_id("7");
        day.setBenefits_name("Sun");
        all_day.add(day);


        days_all.setBenefits(all_day);
        Benefits test = new Benefits();
/*
                           _daDays_checkBoxAdapter = new Days_CheckBoxAdapter(days_all, context, 1,test,true);
                    griddaystime.setAdapter(_daDays_checkBoxAdapter);*/

        griddaystime.setExpanded(true);
        Days_adapter  _daDays_checkBoxAdapter = new Days_adapter(days_all, context, 1, test, true);
        griddaystime.setAdapter(_daDays_checkBoxAdapter);

        Evening_adapter   _daevening_checkBoxAdapter = new Evening_adapter(days_all, context, 2, true);
        grideveningdaystime.setAdapter(_daevening_checkBoxAdapter);


        Night_adapter   _daNightcheckBoxAdapter = new Night_adapter(days_all, context, 3, true);
        gridnightdaystime.setAdapter(_daNightcheckBoxAdapter);
    }
    private boolean isNumeric(String string) {
        if(Pattern.matches("\\d{1,3}((\\.\\d{1,2})?)", string))
            return true;
        else
            return false;
    }
}
