/*
 * Copyright (c) 2017. Truiton (http://www.truiton.com/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 * Mohit Gupt (https://github.com/mohitgupt)
 *
 */

package com.hiremehealthcare.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.SpannableString;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.hiremehealthcare.CommonCls.DialogUtils;
import com.hiremehealthcare.CommonCls.URLS;
import com.hiremehealthcare.CommonCls.Validations;
import com.hiremehealthcare.POJO.PandingPositionModel;
import com.hiremehealthcare.R;
import com.hiremehealthcare.Utils.CommonRequestHelper;
import com.hiremehealthcare.activity.PandingPositionDetailActivity;
import com.hiremehealthcare.adapter.PendingAdapter;
import com.hiremehealthcare.database.MySharedPrefereces;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class PendingFragment extends Fragment {
    private android.widget.ListView ivpending;
    Context ctx;
    ProgressDialog progressDialog;
    MySharedPrefereces mySharedPrefereces;
    List<PandingPositionModel.Datum> pandingDataList;
    PendingAdapter pendingAdapter;
    Context context;
    LinearLayout ivNoDataFound;
    TextView tv_nodata;
    String TAG="PendingFragment";
    public static PendingFragment newInstance() {
        PendingFragment fragment = new PendingFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.pending_fragment, container, false);
        context=getContext();
        initAllControls(view);
        return view;
    }

    public void initAllControls(View rootView)
    {
        this.ivpending = (ListView) rootView.findViewById(R.id.iv_pending);
        ivNoDataFound = (LinearLayout) rootView.findViewById(R.id.ivNoDataFound);
        tv_nodata = (TextView) rootView.findViewById(R.id.tv_nodata);
        pandingDataList = new ArrayList<>();
        progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        SpannableString spannableString =  new SpannableString(getString(R.string.please_wait));
        Typeface typefaceSpan = Validations.setTypeface(context);
        spannableString.setSpan(typefaceSpan, 0, getString(R.string.please_wait).length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        progressDialog.setMessage(spannableString);
        mySharedPrefereces = new MySharedPrefereces(context);
        ctx = getActivity();
        pendingAdapter = new PendingAdapter(ctx, pandingDataList, new PendingAdapter.OnPandingAdapterRemoveOrFavoriteButtonClicked() {
            @Override
            public void onFavoriteButtonClicked(PandingPositionModel.Datum data) {
               // Toast.makeText(ctx, "favorite button clicked", Toast.LENGTH_SHORT).show();
                IAmInterested(data);
            }

            @Override
            public void onRemoveButtonClicked(PandingPositionModel.Datum data) {
                onRemoveButtonClickedOuter(data);
            }

            @Override
            public void onListItemClicked(PandingPositionModel.Datum data) {
                Intent intent = new Intent(context,PandingPositionDetailActivity.class);
                intent.putExtra("data", data);
                startActivity(intent);
            }
        });
        ivpending.setAdapter(pendingAdapter);
        getPandingPositionData();
    }

    /**
     * this method called when user clicked in Favorite button in list
     * @param data which position list item
     */

    public void IAmInterested(PandingPositionModel.Datum data)
    {
        progressDialog.show();
        String url= URLS.APPLICANT_SHOW_INTEREST_ON_POSITION+mySharedPrefereces.getUserId()+"&"+URLS.POSITION_ID+"="+data.getPositionId();
        Log.d(TAG, "IAmInterested: URL:"+url);
        CommonRequestHelper.getDataFromUrl(url, new CommonRequestHelper.VolleyResponseCustom() {

            @Override
            public void getServerResponse(String response) {
                progressDialog.dismiss();
                Log.d(TAG, "IAmInterested: Response:"+response);
                Gson gson = new Gson();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int status = jsonObject.getInt("status");
                    String message = jsonObject.getString("message");
                    DialogUtils.showDialog4Activity(getActivity(), context.getResources().getString(R.string.app_name),message, null);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                /*ApplicantNearPositionModel applicantNearPositionModel = gson.fromJson(response, ApplicantNearPositionModel.class);
                if(applicantNearPositionModel.getStatus()==1)
                {
                    if(applicantNearPositionModel.getData().size()==0)
                        Toast.makeText(context,"No data found",Toast.LENGTH_LONG).show();
                    applicantNearPositionModelList.addAll(applicantNearPositionModel.getData());
                    applicantNearPositionAdapter.notifyDataSetChanged();
                }
                else
                {

                }*/
            }

            @Override
            public void getVolleyError(VolleyError volleyError) {
                Log.d(TAG, "IAmInterested: Response:"+volleyError.getMessage());
                progressDialog.dismiss();
                NetworkResponse networkResponse = volleyError.networkResponse;
                if (networkResponse != null) {
                    int statusCode = volleyError.networkResponse.statusCode;
                    System.out.println("STATUS CODE " + statusCode + "");
                    DialogUtils.showDialog4Activity(getActivity(), context.getResources().getString(R.string.app_name), statusCode + "", null);
                }
            }
        },context);

    }

    /**
     * onRemove button clicked  in list
     * @param data
     */

    public void onRemoveButtonClickedOuter(final PandingPositionModel.Datum data)
    {
        String Url=URLS.MY_POSITION_REMOVE+data.getId();
        Log.d(TAG, "onRemoveButtonClickedOuter: URL" + Url);
        progressDialog.show();
        Log.d("PendingFragment", Url);
        CommonRequestHelper.getDataFromUrl(Url, new CommonRequestHelper.VolleyResponseCustom() {
            @Override
            public void getServerResponse(String response) {
                progressDialog.dismiss();
                Log.d(TAG, "onRemoveButtonClickedOuter: Response" + response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if(jsonObject.getInt("status")==1)
                    {
                        DialogUtils.showDialog4Activity(getActivity(), context.getResources().getString(R.string.app_name),  jsonObject.getString("message")+ "", null);
                        getPandingPositionData();
                    }
                    else
                    {
                        DialogUtils.showDialog4Activity(getActivity(), context.getResources().getString(R.string.app_name),  jsonObject.getString("message")+ "", null);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void getVolleyError(VolleyError volleyError) {
                Log.d(TAG, "onRemoveButtonClickedOuter: Error" + volleyError.getMessage());
                progressDialog.dismiss();NetworkResponse networkResponse = volleyError.networkResponse;
                DialogUtils.noInterNetDialog(ctx, "No Internet Connection", "No active internet connection available", new DialogUtils.NoInterNetDailogClickListner() {
                    @Override
                    public void onDialogOkButtonClicked() {
                        onRemoveButtonClickedOuter(data);
                    }

                    @Override
                    public void onDialogCancelButtonClicked() {

                    }
                });


            }
        }, context);


    }

    /**
     * this method is used to getting pandding data from server
     */
    public void getPandingPositionData()
    {
        progressDialog.show();
        pandingDataList.clear();
        String Url = URLS.MY_POSITION_PANDING + mySharedPrefereces.getUserId();
        Log.d(TAG, "getPandingPositionData: URL "+Url);
        CommonRequestHelper.getDataFromUrl(Url, new CommonRequestHelper.VolleyResponseCustom() {
            @Override
            public void getServerResponse(String response) {
                progressDialog.dismiss();
                Log.d(TAG, "getPandingPositionData: Response "+response);
                Gson gson = new Gson();
                Log.d("Experience response", response);
                PandingPositionModel pandingPositionModel = gson.fromJson(response, PandingPositionModel.class);
                if(pandingPositionModel!=null)
                {
                    pandingDataList.addAll(pandingPositionModel.getData());
                    pendingAdapter.notifyDataSetChanged();
                    if(pandingPositionModel.getData()!=null && pandingPositionModel.getData().size()==0)
                    {
                        ivNoDataFound.setVisibility(View.VISIBLE);
                        tv_nodata.setText("No Pending Position Found");
                    }
                    else
                    {
                        ivNoDataFound.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void getVolleyError(VolleyError volleyError) {
                progressDialog.dismiss();
                Log.d(TAG, "getPandingPositionData: Response "+volleyError.getMessage());
            }
        },context);
    }
}
