package com.hiremehealthcare.fragment;


import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.hiremehealthcare.CommonCls.DialogUtils;
import com.hiremehealthcare.CommonCls.URLS;
import com.hiremehealthcare.POJO.HomeManagerModel;
import com.hiremehealthcare.POJO.SearchCandidate;
import com.hiremehealthcare.R;
import com.hiremehealthcare.adapter.HomeManagerAdapter;
import com.hiremehealthcare.database.MySharedPrefereces;

import java.util.ArrayList;
import java.util.List;

/**
 * A simple {@link Fragment} subclass.
 */
public class HomeManagerFragment extends Fragment {

    Context ctx;
    MySharedPrefereces mySharedPrefereces;
    RequestQueue queue;
    public static RecyclerView rvHomeManagerList;
    List<HomeManagerModel> homeManagerModelList;
    public static HomeManagerAdapter homeManagerAdapter;
    ImageView ivNoDataFound;

    String TAG = "HomeManagerFragment";

    public HomeManagerFragment() {
        // Required empty public constructor
    }

    @Override
    public void onResume() {
        super.onResume();
        System.out.println("this is on resume called....");
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView = inflater.inflate(R.layout.fragment_home_manager, container, false);
        ctx = getActivity();
        queue = Volley.newRequestQueue(ctx);
        mySharedPrefereces = new MySharedPrefereces(ctx);
        initAllControls(rootView);
        return rootView;
    }

    public void initAllControls(View rootView) {
        rvHomeManagerList = (RecyclerView) rootView.findViewById(R.id.rvHomeManagerList);
        ivNoDataFound = (ImageView) rootView.findViewById(R.id.ivNoDataFound);
        homeManagerModelList = new ArrayList<>();
        //   homeManagerAdapter = new HomeManagerAdapter(homeManagerModelList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ctx);
        rvHomeManagerList.setLayoutManager(mLayoutManager);
        rvHomeManagerList.setItemAnimator(new DefaultItemAnimator());

        //  getApplicantNearPosionDataFromApi();
        getManagerNearCandidate();
    }

    /**
     * this method is use to get manager near candidate
     */
    private void getManagerNearCandidate() {
        DialogUtils.showProgressDialog(ctx, "Please wait....");
        //   String Url = URLS.MANAGER_SEARCH_CANDIDATE + mySharedPrefereces.getZipCode()+"";
        String Url = URLS.MANAGER_NEAR_CANDIDATE + mySharedPrefereces.getZipCode() + "";
        Log.d(TAG, "getManagerNearCandidate: URL:-" + Url);
        StringRequest req = new StringRequest(Request.Method.GET, Url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "getManagerNearCandidate: Response:-" + response);
                Gson gson = new Gson();
                DialogUtils.hideProgressDialog();
                SearchCandidate searchCandidate = gson.fromJson(response, SearchCandidate.class);

                if (searchCandidate != null) {
                    if (searchCandidate.getData() != null) {
                        if (searchCandidate.getData().size() > 0) {
                            homeManagerAdapter = new HomeManagerAdapter(searchCandidate, ctx);
                            rvHomeManagerList.setAdapter(homeManagerAdapter);
                            ivNoDataFound.setVisibility(View.GONE);
                        } else {
                            ivNoDataFound.setVisibility(View.VISIBLE);
                            //DialogUtils.showDialog(ctx, ctx.getResources().getString(R.string.app_name), "No Candidate Found!");

                        }
                    }

                } else {    // DialogUtils.showDialog4Activity(Login.this, ctx.getResources().getString(R.string.app_name), logindata.getMessage() + "");

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "getManagerNearCandidate: Error:-" + error.getMessage());
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    int statusCode = error.networkResponse.statusCode;
                    System.out.println("STATUS CODE " + statusCode + "");
                    DialogUtils.showDialog4Activity(getActivity(), ctx.getResources().getString(R.string.app_name), statusCode + "", null);
                }

            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(req);
    }


}
