package com.hiremehealthcare.fragment;


import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hiremehealthcare.R;
import com.hiremehealthcare.adapter.ViewPagerAdapter;


/**
 * A simple {@link Fragment} subclass.
 */
public class SheduledInterviewsFragment extends Fragment {


    TabLayout tablayout;
    ViewPager viewpager;
    public SheduledInterviewsFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View rootView= inflater.inflate(R.layout.fragment_sheduled_interviews, container, false);
        initAllControls(rootView);
        return rootView;
    }

    public void initAllControls(View rootView)
    {
        tablayout = (TabLayout) rootView.findViewById(R.id.tablayout);
        viewpager = (ViewPager) rootView.findViewById(R.id.viewpager);
        ViewPagerAdapter adapter = new ViewPagerAdapter(getChildFragmentManager());
        adapter.addFragment(new PreviousSheduledInterviewsFragment(), "Previous");
        adapter.addFragment(new UpComingScheduleInterviewsFragment(), "Upcoming");
        viewpager.setAdapter(adapter);
        tablayout.setupWithViewPager(viewpager );
        viewpager.setCurrentItem(1);
    }

}
