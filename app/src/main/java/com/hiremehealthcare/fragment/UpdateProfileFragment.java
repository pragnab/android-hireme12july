package com.hiremehealthcare.fragment;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.os.Build;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.text.SpannableString;
import android.text.Spanned;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import com.google.gson.Gson;
import com.hiremehealthcare.CommonCls.CustomButton;
import com.hiremehealthcare.CommonCls.CustomEditText;
import com.hiremehealthcare.CommonCls.CustomTextView;
import com.hiremehealthcare.CommonCls.DialogUtils;
import com.hiremehealthcare.CommonCls.URLS;
import com.hiremehealthcare.CommonCls.Validations;
import com.hiremehealthcare.POJO.ManagerProfileModel;
import com.hiremehealthcare.R;
import com.hiremehealthcare.Utils.CommonRequestHelper;
import com.hiremehealthcare.database.MySharedPrefereces;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.util.HashMap;
import java.util.Map;

import de.hdodenhof.circleimageview.CircleImageView;

import com.bumptech.glide.Glide;

/**
 * this class is used to update manager profile
 */
public class UpdateProfileFragment extends Fragment implements View.OnClickListener {
    private com.hiremehealthcare.CommonCls.CustomTextView tvManagerProfileFirstName;
    private com.hiremehealthcare.CommonCls.CustomEditText etManagerProfileFirstName;
    private com.hiremehealthcare.CommonCls.CustomTextView tvManagerProfileLastName;
    private com.hiremehealthcare.CommonCls.CustomEditText etManagerProfileLastName;
    private com.hiremehealthcare.CommonCls.CustomTextView tvManagerProfileEmail;
    private com.hiremehealthcare.CommonCls.CustomEditText etManagerProfileEmail;
    private com.hiremehealthcare.CommonCls.CustomTextView tvManagerProfilePhone;
    private com.hiremehealthcare.CommonCls.CustomEditText etManagerProfilePhone;
    private com.hiremehealthcare.CommonCls.CustomTextView tvManagerProfileZipCode;
    private com.hiremehealthcare.CommonCls.CustomEditText etManagerProfileZipCode;
    private com.hiremehealthcare.CommonCls.CustomTextView tvManagerProfileImage;
    private com.hiremehealthcare.CommonCls.CustomEditText etPROFILEIMAGE;
    private com.hiremehealthcare.CommonCls.CustomButton btnChangePasswordSave;
    MySharedPrefereces mySharedPrefereces;
    ProgressDialog progressDialog;
    ManagerProfileModel managerProfileModel;
    private Bitmap bitmap;
    private String imgByteArray = "";
    RequestQueue queue;
    Context context;
    String TAG = "UpdateProfileFragment";
    CircleImageView profile_image;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View rootView = inflater.inflate(R.layout.update_profile_fragment, container, false);
        context = getContext();
        initAllControls(rootView);
        return rootView;
    }

    private void initAllControls(View rootView) {
        progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        SpannableString spannableString = new SpannableString(getString(R.string.please_wait));
        Typeface typefaceSpan = Validations.setTypeface(context);
        spannableString.setSpan(typefaceSpan, 0, getString(R.string.please_wait).length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        progressDialog.setMessage(spannableString);
        btnChangePasswordSave = (CustomButton) rootView.findViewById(R.id.btnChangePasswordSave);
        btnChangePasswordSave.setOnClickListener(this);
        etPROFILEIMAGE = (CustomEditText) rootView.findViewById(R.id.etPROFILEIMAGE);
        profile_image = (CircleImageView) rootView.findViewById(R.id.profile_image);
        tvManagerProfileImage = (CustomTextView) rootView.findViewById(R.id.tvManagerProfileImage);
        etManagerProfileZipCode = (CustomEditText) rootView.findViewById(R.id.etManagerProfileZipCode);
        tvManagerProfileZipCode = (CustomTextView) rootView.findViewById(R.id.tvManagerProfileZipCode);
        etManagerProfilePhone = (CustomEditText) rootView.findViewById(R.id.etManagerProfilePhone);
        tvManagerProfilePhone = (CustomTextView) rootView.findViewById(R.id.tvManagerProfilePhone);
        etManagerProfileEmail = (CustomEditText) rootView.findViewById(R.id.etManagerProfileEmail);
        tvManagerProfileEmail = (CustomTextView) rootView.findViewById(R.id.tvManagerProfileEmail);
        etManagerProfileLastName = (CustomEditText) rootView.findViewById(R.id.etManagerProfileLastName);
        tvManagerProfileLastName = (CustomTextView) rootView.findViewById(R.id.tvManagerProfileLastName);
        etManagerProfileFirstName = (CustomEditText) rootView.findViewById(R.id.etManagerProfileFirstName);
        tvManagerProfileFirstName = (CustomTextView) rootView.findViewById(R.id.tvManagerProfileFirstName);
        etPROFILEIMAGE.setOnClickListener(this);
        mySharedPrefereces = new MySharedPrefereces(context);
        getProfileDetailFromaApi();


    }

    /**
     * this method is used to getting manager profie datail from server
     */
    public void getProfileDetailFromaApi() {
        progressDialog.show();
        String Url = URLS.GET_MANAGER_PROFILE_DETAIL + mySharedPrefereces.getUserId();
        Log.d(TAG, "getProfileDetailFromaApi: URL:-" + Url);
        CommonRequestHelper.getDataFromUrl(Url, new CommonRequestHelper.VolleyResponseCustom() {
            @Override
            public void getServerResponse(String response) {
                Log.d(TAG, "getProfileDetailFromaApi: response:-" + response);
                progressDialog.dismiss();
                Gson gson = new Gson();

                managerProfileModel = gson.fromJson(response, ManagerProfileModel.class);
                if (managerProfileModel.getData() != null) {
                    showDataToUser();
                }
            }

            @Override
            public void getVolleyError(VolleyError volleyError) {
                progressDialog.dismiss();
                Log.d(TAG, "getProfileDetailFromaApi: Error:-" + volleyError.getMessage());
                DialogUtils.noInterNetDialog(context, "No Internet Connection", "No active internet connection available", new DialogUtils.NoInterNetDailogClickListner() {
                    @Override
                    public void onDialogOkButtonClicked() {
                        getProfileDetailFromaApi();
                    }

                    @Override
                    public void onDialogCancelButtonClicked() {

                    }
                });

                //Log.d("UpdateProfileFragment", volleyError);
            }
        }, context);

    }

    /**
     * this method is used to show user detail
     */
    public void showDataToUser() {

        etManagerProfileFirstName.setText(managerProfileModel.getData().getManagerFirstname());
        etManagerProfileLastName.setText(managerProfileModel.getData().getManagerLastname());
        etManagerProfileEmail.setText(managerProfileModel.getData().getManagerEmail());
        etManagerProfilePhone.setText(managerProfileModel.getData().getManagerPhone());
        etManagerProfileZipCode.setText(managerProfileModel.getData().getManagerZipcode());
        Glide.with(context)
                .load(managerProfileModel.getData().getManagerImage())
                //.placeholder(R.drawable.no_image)
                .error(R.drawable.no_image)
                .into(profile_image);
        System.out.println("on finish called ::::::::::::::::::::: ");

}

    @Override
    public void onClick(View view) {

        if (view == btnChangePasswordSave) {
            onSaveButtonClicked();
        } else if (view == etPROFILEIMAGE) {
            startCamera();
        }

    }


    public void startCamera() {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            if (!Validations.hasPermissions(context, Validations.RUN_TIME_PERMITIONS)) {
                ActivityCompat.requestPermissions(getActivity(), Validations.RUN_TIME_PERMITIONS, Validations.REQUEST_CODE_PERMISSION);
            } else {
                try {
                    InputMethodManager imm = (InputMethodManager) context.getSystemService(
                            Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(etPROFILEIMAGE.getWindowToken(), 0);
                } catch (Exception ex) {
                }

                Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
                startActivityForResult(cameraIntent, Validations.CAMERA_REQUEST);
            }
        } else {
            try {
                InputMethodManager imm = (InputMethodManager) context.getSystemService(
                        Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(etPROFILEIMAGE.getWindowToken(), 0);
            } catch (Exception ex) {
            }

            Intent cameraIntent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            startActivityForResult(cameraIntent, Validations.CAMERA_REQUEST);
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        // TODO Auto-generated method stub
        if (requestCode == Validations.CAMERA_REQUEST
                && resultCode == Activity.RESULT_OK) {

            try {

                //Bitmap photo = (Bitmap) data.getExtras().get("data");

                if (data.getData() == null) {
                    bitmap = (Bitmap) data.getExtras().get("data");
                } else {
                    bitmap = MediaStore.Images.Media.getBitmap(getActivity().getContentResolver(), data.getData());
                }


                //ivInsertLocationImageShow.setImageBitmap(bitmap);
                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);
                final byte[] byteImage = byteArrayOutputStream.toByteArray();


                imgByteArray = Base64.encodeToString(byteImage, Base64.DEFAULT);
                uploadImage();
                etPROFILEIMAGE.setText("img_" + System.currentTimeMillis() + ".jpg");


            } catch (Exception ex) {
                Toast.makeText(context, getString(R.string.something_went_wrong), Toast.LENGTH_LONG).show();
            }

        }

    }

    /**
     * this method is called when user clicked save button
     */

    public void onSaveButtonClicked() {
        String firstName = etManagerProfileFirstName.getText().toString();
        String lastName = etManagerProfileLastName.getText().toString();
        String email = etManagerProfileEmail.getText().toString();
        String phone = etManagerProfilePhone.getText().toString();
        String zip_code = etManagerProfileZipCode.getText().toString();
        if (firstName.trim().length() == 0) {
            Toast.makeText(context, "Please Enter First Name", Toast.LENGTH_LONG).show();
        } else if (!Validations.isValidName(firstName)) {
            Toast.makeText(context, "Please Enter Valid First Name", Toast.LENGTH_LONG).show();
        } else if (lastName.trim().length() == 0) {
            Toast.makeText(context, "Please Enter Last Name", Toast.LENGTH_LONG).show();
        } else if (!Validations.isValidName(lastName)) {
            Toast.makeText(context, "Please Enter Valid Last Name", Toast.LENGTH_LONG).show();
        } else if (!Validations.isValidEmail(email)) {
            Toast.makeText(context, "Please Enter Valid Email", Toast.LENGTH_LONG).show();
        }

        /*25-march-2019*/
       /* else if (phone.length() == 0) {
            Toast.makeText(context, "Please Enter Phone Number", Toast.LENGTH_LONG).show();
        }
        //else if (phone.trim().length() < 10||phone.trim().length()>12) {
        else if (phone.trim().length() != 10) {
            Toast.makeText(context, "Please Enter Valid Phone Number", Toast.LENGTH_LONG).show();



        }*/
        else if (!Validations.isValidPhoneNumber(phone)) {
            Toast.makeText(context, "Please Enter Valid Phone Number", Toast.LENGTH_SHORT).show();
        }

        else if (zip_code.trim().length() == 0) {
            Toast.makeText(context, "Please Enter ZipCode", Toast.LENGTH_LONG).show();
        } else if (zip_code.trim().length() != 5) {
            Toast.makeText(context, "Please Enter Valid ZipCode", Toast.LENGTH_LONG).show();
        } else {
            if (imgByteArray.equals("")) {
                sendProfileDataToServer(mySharedPrefereces.getUserId(), firstName, lastName, email, phone, zip_code, false);
            } else {
                sendProfileDataToServer(mySharedPrefereces.getUserId(), firstName, lastName, email, phone, zip_code, true);
            }

        }


    }

    /**
     * this method send use data to server
     *
     * @param manager_id    manager id getting from server
     * @param firstName     first name of manager
     * @param lastName      last name of manager
     * @param email         email of manger
     * @param phone         phone nuber of manager
     * @param zipcode       zip code of manager
     * @param isImageSelect is image upload or not
     */

    public void sendProfileDataToServer(final String manager_id, final String firstName, final String lastName, final String email, final String phone, final String zipcode, final boolean isImageSelect) {

        progressDialog.show();
        queue = Volley.newRequestQueue(context);
        String url = URLS.UPDATE_MANAGER_PROFILE_DETAIL;
        Log.d(TAG, "sendProfileDataToServer: URL: " + url);

        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        Log.d(TAG, "sendProfileDataToServer: Response: " + response + "");
                        // response
                        Log.d("Response", response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int id = jsonObject.getInt("status");
                            if (id > 0) {
                                /*25-march pragna to finish fragment */
                            //pppppppppppppp    getProfileDetailFromaApi();
                               /* DialogUtils.showDialog4Activity(getActivity(), context.getResources().getString(R.string.app_name), jsonObject.getString("message") + "", null);*/

                                DialogUtils.showDialog4Activity(getActivity(), context.getResources().getString(R.string.app_name), jsonObject.getString("message") + "", new DialogUtils.DailogCallBackOkButtonClick() {
                                    @Override
                                    public void onDialogOkButtonClicked() {
                                        getActivity().finish();
                                    }
                                });
                            } else {
                                DialogUtils.showDialog4Activity(getActivity(), context.getResources().getString(R.string.app_name), jsonObject.getString("message") + "", null);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Log.d(TAG, "sendProfileDataToServer: Response: " + error.getMessage());
                        DialogUtils.noInterNetDialog(context, "No Internet Connection", "No active internet connection available", new DialogUtils.NoInterNetDailogClickListner() {
                            @Override
                            public void onDialogOkButtonClicked() {
                                sendProfileDataToServer(manager_id, firstName, lastName, email, phone, zipcode, isImageSelect);
                            }

                            @Override
                            public void onDialogCancelButtonClicked() {

                            }
                        });

                        // error
                        Log.d("Error.Response", error.getMessage());
                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("type", "manage_manager_profile");
                params.put("manager_id", manager_id);
                params.put("firstname", firstName);
                params.put("lastname", lastName);
                params.put("email", email);
                params.put("phone", phone);
                params.put("zipcode", zipcode);
                /*if(isImageSelect)
                {
                    params.put("image", imgByteArray);
                }*/
                Log.d(TAG, "getParams: request parameters" + params.toString());
                return params;
            }
        };
        queue.add(postRequest);
    }

    public void uploadImage() {

        progressDialog.show();
        queue = Volley.newRequestQueue(context);
        String url = URLS.UPDATE_MANAGER_PROFILE_DETAIL;
        Log.d(TAG, "uploadImage: URL: " + url);

        StringRequest postRequest = new StringRequest(Request.Method.POST, url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        Log.d(TAG, "uploadImage: Response: " + response);
                        // response
                        Log.d("Response", response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int id = jsonObject.getInt("status");
                            if (id > 0) {
                                getProfileDetailFromaApi();
                                DialogUtils.showDialog4Activity(getActivity(), context.getResources().getString(R.string.app_name), jsonObject.getString("message") + "", null);
                            } else {
                                DialogUtils.showDialog4Activity(getActivity(), context.getResources().getString(R.string.app_name), jsonObject.getString("message") + "", null);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Log.d(TAG, "sendProfileDataToServer: Response: " + error.getMessage());
                        // error
                        Log.d("Error.Response", error.getMessage());
                        DialogUtils.noInterNetDialog(context, "No Internet Connection", "No active internet connection available", new DialogUtils.NoInterNetDailogClickListner() {
                            @Override
                            public void onDialogOkButtonClicked() {
                                uploadImage();
                            }

                            @Override
                            public void onDialogCancelButtonClicked() {
                                //finish();
                            }
                        });

                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("type", "upload_image_manager");
                params.put("manager_id", mySharedPrefereces.getUserId());
                params.put("image", imgByteArray);
                /*if(isImageSelect)
                {
                    params.put("image", imgByteArray);
                }*/
                Log.d(TAG, "uploadImage: request parameters" + params.toString());
                return params;
            }
        };
        queue.add(postRequest);
    }


}
