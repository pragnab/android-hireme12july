/*
 * Copyright (c) 2017. Truiton (http://www.truiton.com/).
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *       http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * Contributors:
 * Mohit Gupt (https://github.com/mohitgupt)
 *
 */

package com.hiremehealthcare.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.SpannableString;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.NetworkResponse;
import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.hiremehealthcare.CommonCls.DialogUtils;
import com.hiremehealthcare.CommonCls.URLS;
import com.hiremehealthcare.CommonCls.Validations;
import com.hiremehealthcare.POJO.PandingPositionModel;
import com.hiremehealthcare.R;
import com.hiremehealthcare.Utils.CommonRequestHelper;
import com.hiremehealthcare.activity.PandingPositionDetailActivity;
import com.hiremehealthcare.adapter.PendingAdapter;
import com.hiremehealthcare.database.MySharedPrefereces;

import org.json.JSONException;
import org.json.JSONObject;
import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.List;

public class ProcessingFragment extends Fragment {

    private android.widget.ListView ivpending;

    Context context;
    ProgressDialog progressDialog;
    MySharedPrefereces mySharedPrefereces;
    List<PandingPositionModel.Datum> pandingDataList;
    PendingAdapter pendingAdapter;
    LinearLayout ivNoDataFound;
    TextView tv_nodata;
    String TAG = "ProcessingFragment";

    public static ProcessingFragment newInstance() {
        ProcessingFragment fragment = new ProcessingFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View rootview= inflater.inflate(R.layout.pending_fragment, container, false);
        initAllControls(rootview);
        return rootview;
    }

    public void initAllControls(View rootView)
    {
        context = getActivity();
        this.ivpending = (ListView) rootView.findViewById(R.id.iv_pending);
        this.tv_nodata = (TextView) rootView.findViewById(R.id.tv_nodata);
        progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        pandingDataList = new ArrayList<>();
        ivNoDataFound = (LinearLayout) rootView.findViewById(R.id.ivNoDataFound);
        SpannableString spannableString =  new SpannableString(getString(R.string.please_wait));
        Typeface typefaceSpan = Validations.setTypeface(context);
        spannableString.setSpan(typefaceSpan, 0, getString(R.string.please_wait).length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        progressDialog.setMessage(spannableString);
        mySharedPrefereces = new MySharedPrefereces(context);


        pendingAdapter = new PendingAdapter(context, pandingDataList, new PendingAdapter.OnPandingAdapterRemoveOrFavoriteButtonClicked() {
            @Override
            public void onFavoriteButtonClicked(PandingPositionModel.Datum data) {
                Toast.makeText(context, "favorite button clicked", Toast.LENGTH_SHORT).show();
                IAmInterested(data);
            }

            @Override
            public void onRemoveButtonClicked(PandingPositionModel.Datum data) {
                onRemoveButtonClickedOuter(data);
            }

            @Override
            public void onListItemClicked(PandingPositionModel.Datum data) {
                Intent intent = new Intent(context,PandingPositionDetailActivity.class);
                intent.putExtra("data", data);
                startActivity(intent);
            }
        });
        ivpending.setAdapter(pendingAdapter);
        getMyPositionProcessingData();
    }

    /**
     * this method called when user clicked on heart button (favorite button)
     * @param data
     */

    public void IAmInterested(PandingPositionModel.Datum data)
    {
        progressDialog.show();
        String url= URLS.APPLICANT_SHOW_INTEREST_ON_POSITION+mySharedPrefereces.getUserId()+"&"+URLS.POSITION_ID+"="+data.getPositionId();
        Log.d(TAG, "IAmInterested:URL:- "+url);
        CommonRequestHelper.getDataFromUrl(url, new CommonRequestHelper.VolleyResponseCustom() {

            @Override
            public void getServerResponse(String response) {
                progressDialog.dismiss();
                Log.d(TAG, "IAmInterested:Response:- "+response);
                Gson gson = new Gson();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int status = jsonObject.getInt("status");
                    String message = jsonObject.getString("message");
                    DialogUtils.showDialog4Activity(getActivity(), context.getResources().getString(R.string.app_name),message, null);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                /*ApplicantNearPositionModel applicantNearPositionModel = gson.fromJson(response, ApplicantNearPositionModel.class);
                if(applicantNearPositionModel.getStatus()==1)
                {
                    if(applicantNearPositionModel.getData().size()==0)
                        Toast.makeText(context,"No data found",Toast.LENGTH_LONG).show();
                    applicantNearPositionModelList.addAll(applicantNearPositionModel.getData());
                    applicantNearPositionAdapter.notifyDataSetChanged();
                }
                else
                {

                }*/
            }

            @Override
            public void getVolleyError(VolleyError volleyError) {
                Log.d(TAG, "IAmInterested:Error:- "+volleyError.getMessage());
                progressDialog.dismiss();
                NetworkResponse networkResponse = volleyError.networkResponse;

            }
        },context);

    }

    /**
     * this method clicked when user clicked on remove button on list
     * @param data
     */

    public void onRemoveButtonClickedOuter(final PandingPositionModel.Datum data)
    {
        String Url=URLS.MY_POSITION_REMOVE+data.getId();
        progressDialog.show();
        Log.d(TAG, "onRemoveButtonClickedOuter: URL:- "+Url);
        CommonRequestHelper.getDataFromUrl(Url, new CommonRequestHelper.VolleyResponseCustom() {
            @Override
            public void getServerResponse(String response) {
                progressDialog.dismiss();
                Log.d(TAG, "onRemoveButtonClickedOuter: Response:- "+response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if(jsonObject.getInt("status")==1)
                    {
                        DialogUtils.showDialog4Activity(getActivity(), context.getResources().getString(R.string.app_name),  jsonObject.getString("message")+ "", null);
                        getMyPositionProcessingData();
                    }
                    else
                    {
                        DialogUtils.showDialog4Activity(getActivity(), context.getResources().getString(R.string.app_name),  jsonObject.getString("message")+ "", null);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void getVolleyError(VolleyError volleyError) {
                Log.d(TAG, "onRemoveButtonClickedOuter: Error:- "+volleyError.getMessage());
                progressDialog.dismiss();NetworkResponse networkResponse = volleyError.networkResponse;
                DialogUtils.noInterNetDialog(context, "No Internet Connection", "No active internet connection available", new DialogUtils.NoInterNetDailogClickListner() {
                    @Override
                    public void onDialogOkButtonClicked() {
                        onRemoveButtonClickedOuter(data);
                    }

                    @Override
                    public void onDialogCancelButtonClicked() {

                    }
                });


            }
        }, context);


    }


    /**
     * this method is used to getting my position processing data from server
     */

    public void getMyPositionProcessingData()
    {
        progressDialog.show();
        pandingDataList.clear();
        String Url = URLS.MY_POSITION_PROCESSING + mySharedPrefereces.getUserId();
        Log.d(TAG, "getMyPositionProcessingData: URL:- "+Url);
        CommonRequestHelper.getDataFromUrl(Url, new CommonRequestHelper.VolleyResponseCustom() {
            @Override
            public void getServerResponse(String response) {
                progressDialog.dismiss();
                Log.d(TAG, "getMyPositionProcessingData: Response:- "+response);
                Gson gson = new Gson();
                Log.d("Experience response", response);
                PandingPositionModel pandingPositionModel = gson.fromJson(response, PandingPositionModel.class);
                if(pandingPositionModel!=null)
                {
                    pandingDataList.addAll(pandingPositionModel.getData());
                    pendingAdapter.notifyDataSetChanged();
                    if(pandingPositionModel.getData()!=null && pandingPositionModel.getData().size()==0)
                    {
                        ivNoDataFound.setVisibility(View.VISIBLE);
                        tv_nodata.setText("No Processing Position Found");
                    }
                    else
                    {
                        ivNoDataFound.setVisibility(View.GONE);
                    }
                }
            }

            @Override
            public void getVolleyError(VolleyError volleyError) {
                progressDialog.dismiss();
                Log.d(TAG, "getMyPositionProcessingData: Response:- "+volleyError.getMessage());
            }
        },context);
    }
}
