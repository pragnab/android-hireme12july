package com.hiremehealthcare.fragment;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextUtils;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.hiremehealthcare.CommonCls.CustomBoldTextView;
import com.hiremehealthcare.CommonCls.CustomButton;
import com.hiremehealthcare.CommonCls.CustomEditText;
import com.hiremehealthcare.CommonCls.CustomTextView;
import com.hiremehealthcare.CommonCls.DialogUtils;
import com.hiremehealthcare.CommonCls.ExpandableHeightGridView;
import com.hiremehealthcare.CommonCls.URLS;
import com.hiremehealthcare.CommonCls.Validations;
import com.hiremehealthcare.POJO.Benefits;
import com.hiremehealthcare.POJO.Certificates;
import com.hiremehealthcare.POJO.GetManagerFacilityDetailModel;
import com.hiremehealthcare.POJO.Hours;
import com.hiremehealthcare.POJO.License;
import com.hiremehealthcare.POJO.Month;
import com.hiremehealthcare.POJO.Shift;
import com.hiremehealthcare.POJO.Speciality;
import com.hiremehealthcare.R;
import com.hiremehealthcare.Utils.CommonRequestHelper;
import com.hiremehealthcare.adapter.BenefitsAdapter;
import com.hiremehealthcare.adapter.CertificationMultiSelectAdapter;
import com.hiremehealthcare.adapter.CustomBenefitsAdapter;
import com.hiremehealthcare.adapter.DaysMultiSelectAdapter;
import com.hiremehealthcare.adapter.DaysMultiSelectAdapter_duplicate_flip;
import com.hiremehealthcare.adapter.Days_CheckBoxAdapter;
//import com.hiremehealthcare.adapter.Days_CheckBoxAdapter_updated;
import com.hiremehealthcare.adapter.Days_CheckBoxAdapter_bk;
import com.hiremehealthcare.adapter.Days_adapter;
import com.hiremehealthcare.adapter.Evening_CheckBoxAdapter;
import com.hiremehealthcare.adapter.Evening_adapter;
import com.hiremehealthcare.adapter.HoursAdapter;
import com.hiremehealthcare.adapter.LicenseAdapter;
import com.hiremehealthcare.adapter.Night_CheckBoxAdapter;
import com.hiremehealthcare.adapter.Night_adapter;
import com.hiremehealthcare.adapter.ShiftAdapter;
import com.hiremehealthcare.adapter.SpecialityAdapter;
import com.hiremehealthcare.adapter.SpinnerAdapter;
import com.hiremehealthcare.database.MySharedPrefereces;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * Created by ADMIN on 11-05-2018.
 */

public class PostNewPostion extends Fragment implements View.OnClickListener {
    private CustomTextView tvfacility;
    private CustomBoldTextView tvcertification;
    private ExpandableHeightGridView gridcertification;
    private ExpandableHeightGridView grid_benefits_custom;
    private CustomBoldTextView tvlicense;
    private ExpandableHeightGridView gridlicence;
    private CustomBoldTextView tvspeciality;
    private ExpandableHeightGridView gridspeciality;
    private CustomBoldTextView tvTIME;
    private CustomBoldTextView tvShift;
    private ExpandableHeightGridView gridtime;
    private CustomBoldTextView tvShifthour;
    private ExpandableHeightGridView gridhr;
    private ScrollView scrollview;
    RequestQueue queue;
    Context ctx;
    HoursAdapter hoursAdapter;
    BenefitsAdapter benefitadapter;
    //BenefitsAdapter customBenifitsAdapter;
    ShiftAdapter shiftAdapter;
    CertificationMultiSelectAdapter certificationAdapter;
    LicenseAdapter licenseAdapter;
    SpecialityAdapter specialityAdapter;
    private CustomBoldTextView tvbenefits;
    private ExpandableHeightGridView gridbenefits;
    private CustomEditText edtbenefit;
    private Spinner spExpYear;
    private Spinner spExpMonth;
    private CustomEditText edtdesc;
    List<String> monthList;
    List<String> yearList;
    CustomButton btnSubmitBack, btnSubmitPriview;
    MySharedPrefereces mySharedPrefereces;
    List<String> customLayout;
    CustomBenefitsAdapter customBenefitsAdapter;
    CustomButton btnAddMoreBenifits;
    ProgressDialog progressDialog;
    String TAG = "PostNewPostion";
    private CustomBoldTextView tvavailability;
    private CustomTextView tvDays;
    private CustomTextView tvEvening;
    private android.widget.LinearLayout v;
    private CustomTextView tvNights;
    private ExpandableHeightGridView griddays;
    private ExpandableHeightGridView griddaystime;
    private ExpandableHeightGridView grideveningdaystime;
    private ExpandableHeightGridView gridnightdaystime;
    private android.widget.LinearLayout lin;
    private ExpandableHeightGridView gridbenefitscustom;
    /*29- March pragna*/
    public static ArrayList<Integer> MON = new ArrayList<>();
    public static ArrayList<Integer> TUE = new ArrayList<>();
    public static ArrayList<Integer> WED = new ArrayList<>();
    public static ArrayList<Integer> THR = new ArrayList<>();
    public static ArrayList<Integer> FRI = new ArrayList<>();
    public static ArrayList<Integer> SAT = new ArrayList<>();
    public static ArrayList<Integer> SUN = new ArrayList<>();
    /*4-Feb-2019 pragna*/
    public static int DAYS_EVENING_NIGHT = 1;

    //    public static Days_CheckBoxAdapter _daDays_checkBoxAdapter;
    public static Days_adapter _daDays_checkBoxAdapter;
    //    public static Days_CheckBoxAdapter_updated _daDays_checkBoxAdapter;
//    public static Night_CheckBoxAdapter _daNightcheckBoxAdapter;
    public static Night_adapter _daNightcheckBoxAdapter;
    //    public static Evening_CheckBoxAdapter _daevening_checkBoxAdapter;
    public static Evening_adapter _daevening_checkBoxAdapter;
    ArrayList<String> days;
    Benefits test;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.postionpostnew, container, false);
        this.gridbenefitscustom = (ExpandableHeightGridView) view.findViewById(R.id.grid_benefits_custom);
        this.lin = (LinearLayout) view.findViewById(R.id.lin);
        this.gridnightdaystime = (ExpandableHeightGridView) view.findViewById(R.id.grid_night_days_time);
        this.grideveningdaystime = (ExpandableHeightGridView) view.findViewById(R.id.grid_evening_days_time);
        this.griddaystime = (ExpandableHeightGridView) view.findViewById(R.id.grid_days_time);
        griddaystime.setExpanded(true);
        MON = new ArrayList<>();
        this.griddays = (ExpandableHeightGridView) view.findViewById(R.id.grid_days);
        this.tvNights = (CustomTextView) view.findViewById(R.id.tv_Nights);
        this.v = (LinearLayout) view.findViewById(R.id.v);
        this.tvEvening = (CustomTextView) view.findViewById(R.id.tv_Evening);
        this.tvDays = (CustomTextView) view.findViewById(R.id.tv_Days);
        this.tvavailability = (CustomBoldTextView) view.findViewById(R.id.tv_availability);
        ctx = getContext();
        days = new ArrayList<>();
        this.edtdesc = (CustomEditText) view.findViewById(R.id.edt_desc);
        btnSubmitBack = (CustomButton) view.findViewById(R.id.btnSubmitBack);
        btnSubmitPriview = (CustomButton) view.findViewById(R.id.btnSubmitPriview);
        btnSubmitPriview.setOnClickListener(this);
        btnSubmitBack.setOnClickListener(this);
        this.spExpMonth = (Spinner) view.findViewById(R.id.spExpMonth);
        this.spExpYear = (Spinner) view.findViewById(R.id.spExpYear);
        btnAddMoreBenifits = (CustomButton) view.findViewById(R.id.btnAddMoreBenifits);
        btnAddMoreBenifits.setOnClickListener(this);
        this.edtbenefit = (CustomEditText) view.findViewById(R.id.edt_benefit);
        this.gridbenefits = (ExpandableHeightGridView) view.findViewById(R.id.grid_benefits);
        grid_benefits_custom = (ExpandableHeightGridView) view.findViewById(R.id.grid_benefits_custom);
        this.tvbenefits = (CustomBoldTextView) view.findViewById(R.id.tv_benefits);
        this.scrollview = (ScrollView) view.findViewById(R.id.scrollview);
        this.gridhr = (ExpandableHeightGridView) view.findViewById(R.id.grid_hr);
        this.tvShifthour = (CustomBoldTextView) view.findViewById(R.id.tv_Shifthour);
        this.gridtime = (ExpandableHeightGridView) view.findViewById(R.id.grid_time);
        this.tvShift = (CustomBoldTextView) view.findViewById(R.id.tv_Shift);
        this.tvTIME = (CustomBoldTextView) view.findViewById(R.id.tv_TIME);
        this.gridspeciality = (ExpandableHeightGridView) view.findViewById(R.id.grid_speciality);
        this.tvspeciality = (CustomBoldTextView) view.findViewById(R.id.tv_speciality);
        this.gridlicence = (ExpandableHeightGridView) view.findViewById(R.id.grid_licence);
        this.tvlicense = (CustomBoldTextView) view.findViewById(R.id.tv_license);
        this.gridcertification = (ExpandableHeightGridView) view.findViewById(R.id.grid_certification);
        this.tvcertification = (CustomBoldTextView) view.findViewById(R.id.tv_certification);
        this.tvfacility = (CustomTextView) view.findViewById(R.id.tv_facility);
        progressDialog = new ProgressDialog(ctx);
        SpannableString spannableString = new SpannableString(getString(R.string.please_wait));
        Typeface typefaceSpan = Validations.setTypeface(ctx);
        spannableString.setSpan(typefaceSpan, 0, getString(R.string.please_wait).length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        progressDialog.setMessage(spannableString);
        progressDialog.setCancelable(false);
        mySharedPrefereces = new MySharedPrefereces(ctx);
        queue = Volley.newRequestQueue(ctx);
        gridspeciality.setExpanded(true);
        gridlicence.setExpanded(true);
        gridcertification.setExpanded(true);
        gridtime.setExpanded(true);
        gridhr.setExpanded(true);
        gridbenefits.setExpanded(true);
        customLayout = new ArrayList<>();
        customBenefitsAdapter = new CustomBenefitsAdapter(customLayout, ctx);
        grid_benefits_custom.setAdapter(customBenefitsAdapter);
        grid_benefits_custom.setExpanded(true);
        getLicense();
        getCertification();
        getShift();
        getHours();
        getspeciality();
        getBenifits();
        monthList = new ArrayList<>();
        yearList = new ArrayList<>();
        getMonthData();
        getYearData();
        SpinnerAdapter monthSpinnerAdapter = new SpinnerAdapter(ctx, monthList);
        SpinnerAdapter yearSpinnerAdapter = new SpinnerAdapter(ctx, yearList);
        spExpMonth.setAdapter(monthSpinnerAdapter);
        spExpYear.setAdapter(yearSpinnerAdapter);

        getManagerFacilityDetail();

        /*4-feb-19 Pragna*/
        this.gridnightdaystime = (ExpandableHeightGridView) view.findViewById(R.id.grid_night_days_time);
        this.grideveningdaystime = (ExpandableHeightGridView) view.findViewById(R.id.grid_evening_days_time);
        this.gridbenefitscustom = (ExpandableHeightGridView) view.findViewById(R.id.grid_benefits_custom);

        gridnightdaystime.setExpanded(true);
        grideveningdaystime.setExpanded(true);
        days = new ArrayList<>();
        days.add("Mon");

        days.add("Tue");
        days.add("Wed");
        days.add("Thu");
        days.add("Fri");
        days.add("Sat");
        days.add("Sun");

        Benefits days_all = new Benefits();
        List<Benefits.BenefitsBean> all_day = new ArrayList<>();
        Benefits.BenefitsBean day = new Benefits.BenefitsBean();
        day.setBenefits_id("1");
        day.setBenefits_name("Mon");
        all_day.add(day);

        day = new Benefits.BenefitsBean();
        day.setBenefits_id("2");
        day.setBenefits_name("Tue");
        all_day.add(day);

        day = new Benefits.BenefitsBean();
        day.setBenefits_id("3");
        day.setBenefits_name("Wed");
        all_day.add(day);


        day = new Benefits.BenefitsBean();
        day.setBenefits_id("4");
        day.setBenefits_name("Thu");
        all_day.add(day);


        day = new Benefits.BenefitsBean();
        day.setBenefits_id("5");
        day.setBenefits_name("Fri");
        all_day.add(day);


        day = new Benefits.BenefitsBean();
        day.setBenefits_id("6");
        day.setBenefits_name("Sat");
        all_day.add(day);


        day = new Benefits.BenefitsBean();
        day.setBenefits_id("7");
        day.setBenefits_name("Sun");
        all_day.add(day);


        days_all.setBenefits(all_day);
        griddays.setExpanded(true);
        DaysMultiSelectAdapter _dayDaysMultiSelectAdapter;
        _dayDaysMultiSelectAdapter = new DaysMultiSelectAdapter(days, ctx);
        griddays.setAdapter(_dayDaysMultiSelectAdapter);
        test = new Benefits();
        //  test=days_all;
        _daDays_checkBoxAdapter = new Days_adapter(days_all, ctx, 1, test, false);
        griddaystime.setAdapter(_daDays_checkBoxAdapter);

//        _daDays_checkBoxAdapter = new Days_CheckBoxAdapter_updated(days_all, ctx, 1, test, false);
        griddaystime.setAdapter(_daDays_checkBoxAdapter);

        _daevening_checkBoxAdapter = new Evening_adapter(days_all, ctx, 2, false);
        grideveningdaystime.setAdapter(_daevening_checkBoxAdapter);


        _daNightcheckBoxAdapter = new Night_adapter(days_all, ctx, 3, false);
        gridnightdaystime.setAdapter(_daNightcheckBoxAdapter);


        return view;

    }

    /**
     * show month list in spinner
     */
    public void getMonthData() {
        monthList.add("Months");
        for (int i = 0; i < 12; i++) {
            monthList.add("" + i);
        }
    }

    /**
     * show user list in spinner
     */
    public void getYearData() {
        yearList.add("Years");
        for (int i = 0; i < 26; i++) {
            yearList.add("" + i);
        }
    }

    /**
     * getting benifits data from server
     */

    private void getBenifits() {
        String Url = URLS.BENEFITS_URL;
        Log.d(TAG, "getBenifits: URL" + Url);
        StringRequest req = new StringRequest(Request.Method.GET, Url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "getBenifits: Response" + response);
                Gson gson = new Gson();
                Benefits benefits = gson.fromJson(response, Benefits.class);

                if (benefits != null) {
                    benefitadapter = new BenefitsAdapter(benefits, ctx);

                    for (int i = 0; i < benefits.getTotal(); i++) {
                        if (benefits.getBenefits().get(i).getBenefits_is_default().contentEquals("1")) {
                            //benefitadapter.selected.add(benefits.getBenefits().get(i).getBenefits_id() + "");
                        }
                    }


                    gridbenefits.setAdapter(benefitadapter);
                    //gridhr.setChoiceMode(GridView.CHOICE_MODE_SINGLE);


                } else {
                    // DialogUtils.showDialog4Activity(Login.this, ctx.getResources().getString(R.string.app_name), logindata.getMessage() + "");

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.d(TAG, "getBenifits: Error" + error.getMessage());
                if (networkResponse != null) {
                    int statusCode = error.networkResponse.statusCode;
                    System.out.println("STATUS CODE " + statusCode + "");
                    DialogUtils.showDialog4Activity(getActivity(), ctx.getResources().getString(R.string.app_name), statusCode + "", null);
                }

            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(req);
    }


    public void getManagerFacilityDetail() {
        String Url = URLS.MANAGER_FACILITY_DETAIL + mySharedPrefereces.getUserId();
        Log.d(TAG, "getManagerFacilityDetail: Url " + Url);
        StringRequest req = new StringRequest(Request.Method.GET, Url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "getManagerFacilityDetail: response " + response);
                Gson gson = new Gson();
                GetManagerFacilityDetailModel managerFacilityDetailModel = gson.fromJson(response, GetManagerFacilityDetailModel.class);

                if (managerFacilityDetailModel != null) {

                    tvfacility.setText(managerFacilityDetailModel.getMessage().get(0).getName() + " " + managerFacilityDetailModel.getMessage().get(0).getAddress() + " " + managerFacilityDetailModel.getMessage().get(0).getCity() + " " + managerFacilityDetailModel.getMessage().get(0).getState() + " " + managerFacilityDetailModel.getMessage().get(0).getZipcode());
                } else {

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                Log.d(TAG, "getManagerFacilityDetail: response " + error.getMessage());
                if (networkResponse != null) {
                    int statusCode = error.networkResponse.statusCode;
                    System.out.println("STATUS CODE " + statusCode + "");
                    DialogUtils.showDialog4Activity(getActivity(), ctx.getResources().getString(R.string.app_name), statusCode + "", null);
                }

            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(req);

    }


    /**
     * this method is used to getting Hours detail from server
     */

    private void getHours() {
        final String Url = URLS.HOURS_URL;
        Log.d(TAG, "getHours: URL" + Url);
        StringRequest req = new StringRequest(Request.Method.GET, Url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "getHours: Response" + Url);
                Gson gson = new Gson();
                Hours hours = gson.fromJson(response, Hours.class);

                if (hours != null) {
                    hoursAdapter = new HoursAdapter(hours, ctx);
                    gridhr.setAdapter(hoursAdapter);
                    gridhr.setChoiceMode(GridView.CHOICE_MODE_SINGLE);


                } else {
                    // DialogUtils.showDialog4Activity(Login.this, ctx.getResources().getString(R.string.app_name), logindata.getMessage() + "");

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "getHours: Error" + error.getMessage());
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    int statusCode = error.networkResponse.statusCode;
                    System.out.println("STATUS CODE " + statusCode + "");
                    DialogUtils.showDialog4Activity(getActivity(), ctx.getResources().getString(R.string.app_name), statusCode + "", null);
                }

            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(req);

    }

    /**
     * this method is used to gettting shift data from Server
     */

    private void getShift() {
        String Url = URLS.SHIFT_URL;
        Log.d(TAG, "getShift: URL" + Url);
        StringRequest req = new StringRequest(Request.Method.GET, Url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "getShift: Response" + response);
                Gson gson = new Gson();
                Shift shifts = gson.fromJson(response, Shift.class);

                if (shifts != null) {
                    shiftAdapter = new ShiftAdapter(shifts, ctx);
                    gridtime.setAdapter(shiftAdapter);
                } else {
                    // DialogUtils.showDialog4Activity(Login.this, ctx.getResources().getString(R.string.app_name), logindata.getMessage() + "");

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "getShift: Error" + error.getMessage());
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    int statusCode = error.networkResponse.statusCode;
                    System.out.println("STATUS CODE " + statusCode + "");
                    DialogUtils.showDialog4Activity(ctx, ctx.getResources().getString(R.string.app_name), statusCode + "", null);
                }

            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(req);

    }

    /**
     * This method is used to gettng Certification data from Server
     */

    private void getCertification() {

        String Url = URLS.CERTIFICATION_URL;
        Log.d(TAG, "getCertification: URL" + Url);
        StringRequest req = new StringRequest(Request.Method.GET, Url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "getCertification: Response" + response);
                Gson gson = new Gson();
                Certificates certificates = gson.fromJson(response, Certificates.class);

                if (certificates != null) {
                    certificationAdapter = new CertificationMultiSelectAdapter(certificates, ctx);
                    gridcertification.setAdapter(certificationAdapter);
                } else {
                    // DialogUtils.showDialog4Activity(Login.this, ctx.getResources().getString(R.string.app_name), logindata.getMessage() + "");

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "getCertification: Error" + error.getMessage());
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    int statusCode = error.networkResponse.statusCode;
                    System.out.println("STATUS CODE " + statusCode + "");
                    DialogUtils.showDialog4Activity(getActivity(), ctx.getResources().getString(R.string.app_name), statusCode + "", null);
                }

            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(req);
    }

    /**
     * this method is used to getting License detail from api
     */

    private void getLicense() {
        String Url = URLS.LICENSE_URL;
        Log.d(TAG, "getLicense: URL:" + Url);
        StringRequest req = new StringRequest(Request.Method.GET, Url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "getLicense: Response:" + response);
                Gson gson = new Gson();
                License license = gson.fromJson(response, License.class);
//                Type collectionType = new TypeToken<Collection<License>>() {
//                }.getType();
//                Collection<License> license = gson.fromJson(response, collectionType);
                if (license != null) {
                    licenseAdapter = new LicenseAdapter(license, ctx);
                    gridlicence.setAdapter(licenseAdapter);
                } else {
                    // DialogUtils.showDialog4Activity(Login.this, ctx.getResources().getString(R.string.app_name), logindata.getMessage() + "");

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "getLicense: Error:" + error.getMessage());
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    int statusCode = error.networkResponse.statusCode;
                    System.out.println("STATUS CODE " + statusCode + "");
                    DialogUtils.showDialog4Activity(getActivity(), ctx.getResources().getString(R.string.app_name), statusCode + "", null);
                }

            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(req);
    }

    /**
     * this mehod is is used to getting speciality from server
     */
    private void getspeciality() {
        String Url = URLS.SPECIALTY_URL;
        Log.d(TAG, "getspeciality: URL:- " + Url);
        StringRequest req = new StringRequest(Request.Method.GET, Url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "getspeciality: Response:- " + response);
                Gson gson = new Gson();
                Speciality speciality = gson.fromJson(response, Speciality.class);
//                Type collectionType = new TypeToken<Collection<Speciality>>() {
//                }.getType();
//                Collection<Speciality> speciality = gson.fromJson(response, collectionType);
                if (speciality != null) {

                    System.out.println("TEST " + speciality.getSpecialty().get(0).getSpecialty_id() + "");
                    System.out.println("TEST " + speciality.getSpecialty().get(0).getSpecialty_name() + "");
                    System.out.println("TEST " + speciality.getSpecialty().size() + "");
//                    for (int i = 0; i < speciality.getSpecialty().size(); i++) {
//                        addRadioButtons(i, rootView, speciality.getSpecialty().get(i).getSpecialty_name());
//                    }
                    specialityAdapter = new SpecialityAdapter(speciality, ctx);
                    gridspeciality.setAdapter(specialityAdapter);

//                  for(int i=0;i<speciality.size();i++)
//                  {
//
//                  }

                } else {
                    // DialogUtils.showDialog4Activity(Login.this, ctx.getResources().getString(R.string.app_name), logindata.getMessage() + "");

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "getspeciality: Error:- " + error.getMessage());
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    int statusCode = error.networkResponse.statusCode;
                    System.out.println("STATUS CODE " + statusCode + "");
                    DialogUtils.showDialog4Activity(getActivity(), ctx.getResources().getString(R.string.app_name), statusCode + "", null);
                }

            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(req);

    }

    @Override
    public void onClick(View view) {

        if (view == btnSubmitBack) {
            String selectedDays_to_Send = "";
            if (MON != null && MON.size() > 0) {
                String mon = MON.toString() + "";
                mon = mon.replace(" ", "");
                mon = mon.replace("[", "");
                mon = mon.replace("]", "");
                selectedDays_to_Send = selectedDays_to_Send + "&mon="+mon;
            }
            if (TUE != null && TUE.size() > 0) {
                String tue = TUE.toString() + "";
                tue = tue.replace(" ", "");
                tue = tue.replace("[", "");
                tue = tue.replace("]", "");
                selectedDays_to_Send = selectedDays_to_Send + "&tue="+tue;
            }
            if (WED != null && WED.size() > 0) {
                String wed = WED.toString() + "";
                wed = wed.replace(" ", "");
                wed = wed.replace("[", "");
                wed = wed.replace("]", "");
                selectedDays_to_Send = selectedDays_to_Send + "&wed="+wed;
            }if (THR != null && THR.size() > 0) {
                String thr = THR.toString() + "";
                thr = thr.replace(" ", "");
                thr = thr.replace("[", "");
                thr = thr.replace("]", "");
                selectedDays_to_Send = selectedDays_to_Send + "&thr="+thr;
            }
            if (FRI != null && FRI.size() > 0) {
                String fri = FRI.toString() + "";
                fri = fri.replace(" ", "");
                fri = fri.replace("[", "");
                fri = fri.replace("]", "");
                selectedDays_to_Send = selectedDays_to_Send +"&fri="+ fri;
            }
            if (SAT != null && SAT.size() > 0) {

                String sat = SAT.toString() + "";
                sat = sat.replace(" ", "");
                sat = sat.replace("[", "");
                sat = sat.replace("]", "");
                selectedDays_to_Send = selectedDays_to_Send +"&fri="+ sat;
            } if (SUN != null && SUN.size() > 0) {

                String sun = SUN.toString() + "";
                sun = sun.replace(" ", "");
                sun = sun.replace("[", "");
                sun = sun.replace("]", "");
                selectedDays_to_Send = selectedDays_to_Send +"&fri="+ sun;
            }
            System.out.println("this is final string11111111111111111 ::::::::::::::::::::```````````````` "+selectedDays_to_Send+"");
            List<String> selectedCertification = certificationAdapter.getSelected();
            List<String> selectedLicense = licenseAdapter.getSelected();
            List<String> selectedSpeciality = specialityAdapter.getSelected();
            List<String> selectedShift = shiftAdapter.getSelected();
            List<String> selectedHour = hoursAdapter.getSelectedHr();
            List<String> selectedBanifits = benefitadapter.getSelected();
            int selectedExperienseYearPosition = spExpYear.getSelectedItemPosition();
            int selectedExperienseMonthPosition = spExpMonth.getSelectedItemPosition();
            if (selectedCertification == null || selectedCertification.size() == 0) {
                Toast.makeText(ctx, "Please Select  Certification", Toast.LENGTH_LONG).show();
            } else if (selectedLicense == null || selectedLicense.size() == 0) {
                Toast.makeText(ctx, "Please Select Atleast One Licence", Toast.LENGTH_LONG).show();
            } else if (selectedSpeciality == null || selectedSpeciality.size() == 0) {
                Toast.makeText(ctx, "Please Select Speciality", Toast.LENGTH_LONG).show();
            } else if (selectedShift == null || selectedShift.size() == 0) {
                Toast.makeText(ctx, "Please Select Atleast One Shift", Toast.LENGTH_LONG).show();
            } else if (selectedHour == null || selectedHour.size() == 0) {
                Toast.makeText(ctx, "Please Select Atleast One Hour", Toast.LENGTH_LONG).show();
            } else if (selectedBanifits == null || selectedBanifits.size() == 0) {
                Toast.makeText(ctx, "Please Select Atleast One Benifits", Toast.LENGTH_LONG).show();
            } else if (selectedExperienseYearPosition == 0) {
                Toast.makeText(ctx, "Please Select Experience Required Year", Toast.LENGTH_LONG).show();
            } else if (selectedExperienseMonthPosition == 0) {
                Toast.makeText(ctx, "Please Select Experience Required Month", Toast.LENGTH_LONG).show();
            } else if (edtdesc.getText().toString().trim().length() == 0) {
                Toast.makeText(ctx, "Please Enter Description", Toast.LENGTH_LONG).show();
            } else {
                String certiStringList = TextUtils.join(",", selectedCertification);
                String licenceStringList = TextUtils.join(",", selectedLicense);
                String specilityStringList = TextUtils.join(",", selectedSpeciality);
                String shiftStringList = TextUtils.join(",", selectedShift);
                String hoursStringList = TextUtils.join(",", selectedHour);
                String benifitsListString = TextUtils.join(",", selectedBanifits);
                String custombenifits;
                if (customBenefitsAdapter.getSelected().size() > 0) {
                    custombenifits = TextUtils.join(",", customBenefitsAdapter.getSelected());
                } else {
                    custombenifits = "";
                }
                sendPostPositionData(certiStringList, licenceStringList, specilityStringList, shiftStringList, hoursStringList, benifitsListString, spExpYear.getSelectedItem().toString(), spExpMonth.getSelectedItem().toString(), custombenifits, mySharedPrefereces.getFacility_id());


            }


        } else if (view == btnAddMoreBenifits) {
            String userInsert = edtbenefit.getText().toString();
            if (userInsert.trim().length() == 0) {
                Toast.makeText(ctx, "Please Enter Benifit Name", Toast.LENGTH_LONG).show();
            } else {
                customLayout.add(userInsert);
                edtbenefit.setText("");
                customBenefitsAdapter.notifyDataSetChanged();
            }
        } else if (view == btnSubmitPriview) {
            System.out.println("mon`````````````` " + MON.toString() + "");
            System.out.println("tue`````````````` " + TUE.toString() + "");
            System.out.println("wed`````````````` " + WED.toString() + "");
            System.out.println("Thr`````````````` " + THR.toString() + "");
            System.out.println("Fri`````````````` " + FRI.toString() + "");
            System.out.println("SAT`````````````` " + SAT.toString() + "");
            System.out.println("SUN`````````````` " + SUN.toString() + "");


            onPreviewButtonClicked();
        }
    }

    public void onPreviewButtonClicked() {
        LayoutInflater inflater = LayoutInflater.from(ctx);
        final View dialogView = inflater.inflate(R.layout.post_new_position_layout, null);
        ExpandableHeightGridView griddays, grideveningdaystime, gridnightdaystime;
        ExpandableHeightGridView griddaystime;
        days = new ArrayList<>();
        days.add("Mon");

        days.add("Tue");
        days.add("Wed");
        days.add("Thu");
        days.add("Fri");
        days.add("Sat");
        days.add("Sun");
        griddays = (ExpandableHeightGridView) dialogView.findViewById(R.id.grid_days);
        gridnightdaystime = (ExpandableHeightGridView) dialogView.findViewById(R.id.grid_night_days_time);
        grideveningdaystime = (ExpandableHeightGridView) dialogView.findViewById(R.id.grid_evening_days_time);
        gridnightdaystime.setExpanded(true);
        griddays.setExpanded(true);
        grideveningdaystime.setExpanded(true);


        griddaystime = (ExpandableHeightGridView) dialogView.findViewById(R.id.grid_days_time);
        ImageView ivCloseDialog = (ImageView) dialogView.findViewById(R.id.ivCloseDialog);
        //List<String> certification_list=certificationAdapter.getSelectedItemString();
        String certiWithNewLine = TextUtils.join("\n", certificationAdapter.getSelectedItemString());
        CustomTextView tvPostPositionCertification = (CustomTextView) dialogView.findViewById(R.id.tvPostPositionCertification);
        tvPostPositionCertification.setText(certiWithNewLine);
        CustomTextView tvPostPositionLicence = (CustomTextView) dialogView.findViewById(R.id.tvPostPositionLicence);
        String licencenseWithNewLine = TextUtils.join("\n", licenseAdapter.getSelectedItem());
        tvPostPositionLicence.setText(licencenseWithNewLine);
        CustomTextView tvPostPositionSpeciality = (CustomTextView) dialogView.findViewById(R.id.tvPostPositionSpeciality);
        String specialityNew = TextUtils.join("\n", specialityAdapter.getSelectedItemName());
        tvPostPositionSpeciality.setText(specialityNew);
        CustomTextView tvPostPositionShift = (CustomTextView) dialogView.findViewById(R.id.tvPostPositionShift);
        String shiftnewLine = TextUtils.join("\n", shiftAdapter.getSelectedItemName());
        tvPostPositionShift.setText(shiftnewLine);
        CustomTextView tvPostPositionHours = (CustomTextView) dialogView.findViewById(R.id.tvPostPositionHours);
        String hoursNewLine = TextUtils.join("\n", hoursAdapter.getSelectedItemName());
        tvPostPositionHours.setText(hoursNewLine);
        CustomTextView tvPostPositionBenitits = (CustomTextView) dialogView.findViewById(R.id.tvPostPositionBenitits);
        List benifitList = benefitadapter.getSelectedName();
        benifitList.addAll(customBenefitsAdapter.getSelected());
        String benifits = TextUtils.join("\n", benifitList);
        tvPostPositionBenitits.setText(benifits);
        CustomTextView tvPostPositionRequiredExperience = (CustomTextView) dialogView.findViewById(R.id.tvPostPositionRequiredExperience);
        String requiredExperience = "";

        /*8-March Pragna*/
       /* if (spExpYear.getSelectedItemPosition() != 0) {
            requiredExperience = requiredExperience + "year " + spExpYear.getSelectedItem().toString();
        }
        if (spExpMonth.getSelectedItemPosition() != 0) {

            requiredExperience = requiredExperience + " month " + spExpMonth.getSelectedItem().toString() + " ";
        }*/

        DaysMultiSelectAdapter_duplicate_flip _dayDaysMultiSelectAdapter;
        _dayDaysMultiSelectAdapter = new DaysMultiSelectAdapter_duplicate_flip(days, ctx);
        griddays.setExpanded(true);
        griddaystime.setExpanded(true);
        griddays.setAdapter(_dayDaysMultiSelectAdapter);
        Benefits days_all = new Benefits();
        List<Benefits.BenefitsBean> all_day = new ArrayList<>();
        Benefits.BenefitsBean day = new Benefits.BenefitsBean();


        day.setBenefits_id("1");
        day.setBenefits_name("Mon");
        all_day.add(day);

        day = new Benefits.BenefitsBean();
        day.setBenefits_id("2");
        day.setBenefits_name("Tue");
        all_day.add(day);

        day = new Benefits.BenefitsBean();
        day.setBenefits_id("3");
        day.setBenefits_name("Wed");
        all_day.add(day);


        day = new Benefits.BenefitsBean();
        day.setBenefits_id("4");
        day.setBenefits_name("Thu");
        all_day.add(day);


        day = new Benefits.BenefitsBean();
        day.setBenefits_id("5");
        day.setBenefits_name("Fri");
        all_day.add(day);


        day = new Benefits.BenefitsBean();
        day.setBenefits_id("6");
        day.setBenefits_name("Sat");
        all_day.add(day);


        day = new Benefits.BenefitsBean();
        day.setBenefits_id("7");
        day.setBenefits_name("Sun");
        all_day.add(day);


        days_all.setBenefits(all_day);
        Benefits test = new Benefits();
//        Days_CheckBoxAdapter _daDays_checkBoxAdapter;
//        Night_CheckBoxAdapter _daNightcheckBoxAdapter;
//        Evening_CheckBoxAdapter _daevening_checkBoxAdapter;

        Days_adapter _daDays_checkBoxAdapter;
        Night_adapter _daNightcheckBoxAdapter;
        Evening_adapter _daevening_checkBoxAdapter;
        // _daevening_checkBoxAdapter = new Evening_CheckBoxAdapter(days_all, ctx, 2, true);
        _daevening_checkBoxAdapter = new Evening_adapter(days_all, ctx, 2, true);
        grideveningdaystime.setAdapter(_daevening_checkBoxAdapter);


        _daNightcheckBoxAdapter = new Night_adapter(days_all, ctx, 3, true);
        gridnightdaystime.setAdapter(_daNightcheckBoxAdapter);


        _daDays_checkBoxAdapter = new Days_adapter(days_all, ctx, 1, test, true);
        griddaystime.setAdapter(_daDays_checkBoxAdapter);


        String y = "", m = "";
        if (spExpYear.getSelectedItemPosition() != 0) {
            if (spExpYear.getSelectedItem().toString().contentEquals("0") || spExpYear.getSelectedItem().toString().contentEquals("1")) {
                y = " year ";
            } else {
                y = " years ";
            }
//            requiredExperience =   spExpYear.getSelectedItem().toString()+" year ";
            requiredExperience = spExpYear.getSelectedItem().toString() + y + "";
        }
        if (spExpMonth.getSelectedItemPosition() != 0) {
            //requiredExperience = requiredExperience+""+ spExpMonth.getSelectedItem().toString() + " month  ";
            if (spExpMonth.getSelectedItem().toString().contentEquals("0") || spExpMonth.getSelectedItem().toString().contentEquals("1")) {
                m = " month ";
            } else {
                m = " months ";
            }
        }

        requiredExperience = requiredExperience + "" + spExpMonth.getSelectedItem().toString() + m + "";

        tvPostPositionRequiredExperience.setText(requiredExperience);
        CustomTextView tvPostPositionDescription = (CustomTextView) dialogView.findViewById(R.id.tvPostPositionDescription);
        tvPostPositionDescription.setText(edtdesc.getText().toString());
        final AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(ctx, R.style.myDialog));
        final AlertDialog b = builder.create();
        //  builder.setTitle("Material Style Dialog");
        builder.setCancelable(true);
        builder.setView(dialogView);
        b.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        final AlertDialog show = builder.show();
        ivCloseDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                show.dismiss();
            }
        });
    }

    /**
     * this method is used to send new position data to server
     *
     * @param certiStringList         how many certifice select from list
     * @param licenceStringList       how many license select from list
     * @param specilityStringList     how many license select from list
     * @param shiftStringList         which shift selected
     * @param hoursStringList         which hour selected
     * @param benifitsListString      which benifits selected
     * @param selectedExperienceYear  experiense year
     * @param selectedExperienceMonth experiense month
     * @param customBenifit           custom added benifits
     * @param facility_id             facility id
     */

    public void sendPostPositionData(final String certiStringList, final String licenceStringList, final String specilityStringList, final String shiftStringList, final String hoursStringList, final String benifitsListString, final String selectedExperienceYear, final String selectedExperienceMonth, final String customBenifit, final String facility_id) {
        progressDialog.show();
//        ArrayList<String> selected_days = _daDays_checkBoxAdapter.selected;
//        System.out.println("this is selected :::::::::::: " + selected_days.toString() + "");
//        System.out.println("this is selected NAME :::::::::::: " + _daDays_checkBoxAdapter.selected_name.toString() + "");
//        String selectedDays_to_Send = _daDays_checkBoxAdapter.selected_name.toString() + "";
//        selectedDays_to_Send = selectedDays_to_Send + "";
//        selectedDays_to_Send = selectedDays_to_Send.replace(" ", "");
//        selectedDays_to_Send = selectedDays_to_Send.replace("[", "");
//        selectedDays_to_Send = selectedDays_to_Send.replace("]", "");
//        if (selectedDays_to_Send.contains(",")) {
//
//
//            selectedDays_to_Send = selectedDays_to_Send.replace(",", "=1,");
//        }
//        selectedDays_to_Send = selectedDays_to_Send + "=1";
//        selectedDays_to_Send = selectedDays_to_Send.toLowerCase();
//        System.out.println("FINALLY SELCTED DAYS ARE ::::::::::::::::::::::::::::::::: ** " + selectedDays_to_Send + "");




        String selectedDays_to_Send = "";
        if (MON != null && MON.size() > 0) {
            List<String> uniqueList =    getUniqueValues(MON);
            String mon = uniqueList.toString() + "";
            mon = mon.replace(" ", "");
            mon = mon.replace("[", "");
            mon = mon.replace("]", "");
            selectedDays_to_Send = selectedDays_to_Send + "&mon="+mon;
        }
        if (TUE != null && TUE.size() > 0) {
            List<String> uniqueList =    getUniqueValues(TUE);
            String tue = uniqueList.toString() + "";
            tue = tue.replace(" ", "");
            tue = tue.replace("[", "");
            tue = tue.replace("]", "");
            selectedDays_to_Send = selectedDays_to_Send + "&tue="+tue;
        }
        if (WED != null && WED.size() > 0) {
            List<String> uniqueList =    getUniqueValues(WED);
            String wed = uniqueList.toString() + "";
            wed = wed.replace(" ", "");
            wed = wed.replace("[", "");
            wed = wed.replace("]", "");
            selectedDays_to_Send = selectedDays_to_Send + "&wed="+wed;
        }if (THR != null && THR.size() > 0) {
            List<String> uniqueList =    getUniqueValues(THR);
            String thr = uniqueList.toString() + "";
            thr = thr.replace(" ", "");
            thr = thr.replace("[", "");
            thr = thr.replace("]", "");
            selectedDays_to_Send = selectedDays_to_Send + "&thu="+thr;
        }
        if (FRI != null && FRI.size() > 0) {
            List<String> uniqueList =    getUniqueValues(FRI);
            String fri = uniqueList.toString() + "";
            fri = fri.replace(" ", "");
            fri = fri.replace("[", "");
            fri = fri.replace("]", "");
            selectedDays_to_Send = selectedDays_to_Send +"&fri="+ fri;
        }
        if (SAT != null && SAT.size() > 0) {
            List<String> uniqueList =    getUniqueValues(SAT);
            String sat = uniqueList.toString() + "";
            sat = sat.replace(" ", "");
            sat = sat.replace("[", "");
            sat = sat.replace("]", "");
            selectedDays_to_Send = selectedDays_to_Send +"&sat="+ sat;
        } if (SUN != null && SUN.size() > 0) {
            List<String> uniqueList =    getUniqueValues(SUN);
            String sun = uniqueList.toString() + "";
            sun = sun.replace(" ", "");
            sun = sun.replace("[", "");
            sun = sun.replace("]", "");
            selectedDays_to_Send = selectedDays_to_Send +"&sun="+ sun;
        }
        System.out.println("this is final string ::::::::::::::::::::```````````````` "+selectedDays_to_Send+"");

        /*if (days != null && selected_days != null) {
            for (int i = 0; i < selected_days.size(); i++) {
                if (days.contains(selected_days.get(i))) {

                }
            }
        }*/


      /*  String urls = URLS.POST_NEW_POSITION + "" + "&" +
                URLS.CERTIFICATION + "=" + certiStringList + "&" +
                URLS.LICENSE + "=" + licenceStringList + "&" +
                URLS.SPECIALITIES + "=" + specilityStringList + "&" +
                URLS.SHIFT + "=" + shiftStringList + "&" +
                URLS.HOURS + "=" + hoursStringList + "&" +
                URLS.BENEFITS + "=" + benifitsListString + "&" +
                URLS.CUSTOM_BENEFITS + "=" + customBenifit + "&" +
                URLS.EXPERIENCE_YEAR + "=" + selectedExperienceYear + "&" +
                URLS.EXPERIENCE_MONTH + "=" + selectedExperienceMonth + "&" +
                URLS.POSITION_DESCRIPTION + "=" + edtdesc.getText().toString() + "&" +
                URLS.FACILITY_ID + "=" + facility_id + "&" +
                URLS.MANAGER_ID + "=" + mySharedPrefereces.getUserId();*/

      /*  String urls = URLS.POST_NEW_POSITION + "" + "&" +
                URLS.CERTIFICATION + "=" + certiStringList + "&" +
                URLS.LICENSE + "=" + licenceStringList + "&" +
                URLS.SPECIALITIES + "=" + specilityStringList + "&" +
                URLS.SHIFT + "=" + shiftStringList + "&" +
                URLS.HOURS + "=" + hoursStringList + "&" +
                URLS.BENEFITS + "=" + benifitsListString + "&" +
                URLS.CUSTOM_BENEFITS + "=" + customBenifit + "&" +
                URLS.EXPERIENCE_YEAR + "=" + selectedExperienceYear + "&" +
                URLS.EXPERIENCE_MONTH + "=" + selectedExperienceMonth + "&" +
                URLS.POSITION_DESCRIPTION + "=" + edtdesc.getText().toString() + "&" +
                URLS.FACILITY_ID + "=" + facility_id + "&" +
                URLS.MANAGER_ID + "=" + mySharedPrefereces.getUserId() + "&" + selectedDays_to_Send + "";*/

        String urls = URLS.POST_NEW_POSITION + "" + "&" +
                URLS.CERTIFICATION + "=" + certiStringList + "&" +
                URLS.LICENSE + "=" + licenceStringList + "&" +
                URLS.SPECIALITIES + "=" + specilityStringList + "&" +
                URLS.SHIFT + "=" + shiftStringList + "&" +
                URLS.HOURS + "=" + hoursStringList + "&" +
                URLS.BENEFITS + "=" + benifitsListString + "&" +
                URLS.CUSTOM_BENEFITS + "=" + customBenifit + "&" +
                URLS.EXPERIENCE_YEAR + "=" + selectedExperienceYear + "&" +
                URLS.EXPERIENCE_MONTH + "=" + selectedExperienceMonth + "&" +
                URLS.POSITION_DESCRIPTION + "=" + edtdesc.getText().toString() + "&" +
                URLS.FACILITY_ID + "=" + facility_id + "&" +
                URLS.MANAGER_ID + "=" + mySharedPrefereces.getUserId() + "" + selectedDays_to_Send + "";//removed &&
        // urls = urls.replace(" ", "%20");
        URL url = null;
        try {
            url = new URL(urls);
            URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef());
            url = uri.toURL();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        Log.d(TAG, "sendPostPositionData: URL:-" + url);

        CommonRequestHelper.getDataFromUrl(url.toString(), new CommonRequestHelper.VolleyResponseCustom() {
            @Override
            public void getServerResponse(String response) {
                progressDialog.dismiss();
                Log.d(TAG, "sendPostPositionData: Response:-" + response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getInt("status") == 1) {
                        DialogUtils.showDialog4Activity(ctx, getResources().getString(R.string.app_name), jsonObject.getString("message"), new DialogUtils.DailogCallBackOkButtonClick() {
                            @Override
                            public void onDialogOkButtonClicked() {
                                getActivity().finish();
                            }
                        });

                    } else {
                        DialogUtils.showDialog4Activity(ctx, getResources().getString(R.string.app_name), jsonObject.getString("message"), null);

                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void getVolleyError(VolleyError volleyError) {
                progressDialog.dismiss();
                Log.d(TAG, "sendPostPositionData: Error:-" + volleyError.getMessage());
                NetworkResponse networkResponse = volleyError.networkResponse;
                DialogUtils.noInterNetDialog(ctx, "No Internet Connection", "No active internet connection available", new DialogUtils.NoInterNetDailogClickListner() {
                    @Override
                    public void onDialogOkButtonClicked() {
                        sendPostPositionData(certiStringList, licenceStringList, specilityStringList, shiftStringList, hoursStringList, benifitsListString, selectedExperienceYear, selectedExperienceMonth, customBenifit, facility_id);
                    }

                    @Override
                    public void onDialogCancelButtonClicked() {

                    }
                });


            }
        }, ctx);
    }
    @SuppressWarnings({ "unchecked", "rawtypes" })
    public static List getUniqueValues(List input) {
        return new ArrayList<>(new HashSet<>(input));
    }

}
