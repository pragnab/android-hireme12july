package com.hiremehealthcare.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.github.lzyzsd.circleprogress.DonutProgress;
import com.hiremehealthcare.CommonCls.CustomTextView;
import com.hiremehealthcare.CommonCls.Validations;
import com.hiremehealthcare.POJO.SearchCandidate;
import com.hiremehealthcare.activity.ContactCandidate;
import com.hiremehealthcare.R;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by nareshp on 5/3/2018.
 */

public class HomeManagerAdapter extends RecyclerView.Adapter<HomeManagerAdapter.MyViewHolder> {
    private SearchCandidate searchCandidate;
    Context ctx;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        DonutProgress circleProgress;
        private CustomTextView tv_license;
        private CustomTextView tvmiles;
        private CustomTextView tvexp;
        private CustomTextView tvexpval;
        private CustomTextView tvintrest;
        private CustomTextView tvintrestval;
        CircleImageView iv_img;
        LinearLayout lin_main;


        public MyViewHolder(View view) {
            super(view);
            tvintrestval = (CustomTextView) itemView.findViewById(R.id.tv_intrest_val);
            tvintrest = (CustomTextView) itemView.findViewById(R.id.tv_intrest);
            tvexpval = (CustomTextView) itemView.findViewById(R.id.tv_exp_val);
            tvexp = (CustomTextView) itemView.findViewById(R.id.tv_exp);
            tvmiles = (CustomTextView) itemView.findViewById(R.id.tv_miles);
            tv_license = (CustomTextView) itemView.findViewById(R.id.tv_license);
            circleProgress = (DonutProgress) itemView.findViewById(R.id.circleProgress);
            iv_img = (CircleImageView) itemView.findViewById(R.id.iv_img);
            lin_main = (LinearLayout) itemView.findViewById(R.id.lin_main);
        }
    }


    public HomeManagerAdapter(SearchCandidate searchCandidate, Context ctx) {
        this.searchCandidate = searchCandidate;
        this.ctx = ctx;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.home_manager_row_inflater, parent, false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
      //  holder.tvmiles.setText(searchCandidate.getData().get(position).getCandidate_distance().replace("null", "0") + " miles");
        holder.tvmiles.setText(searchCandidate.getData().get(position).getCandidate_distance()+"".replace("null", "0") + " miles");
        String complted = searchCandidate.getData().get(position).getProfile_completed() + "";
        if (complted != null) {
            if (Validations.isNumeric(complted)) {
                int int_completed = Integer.parseInt(complted + "");
                holder.circleProgress.setProgress(int_completed);
            }
        }
        holder.lin_main.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //System.out.println("CLICKED... "+position+"");
                System.out.println("CLICKED... " + searchCandidate.getData().get(position).getId() + "");
                Intent i = new Intent(ctx,
                        ContactCandidate.class);

                i.putExtra("ID", searchCandidate.getData().get(position).getId() + "");
                i.putExtra("IMAGE", searchCandidate.getData().get(position).getImage() + "");
                ctx.startActivity(i);
            }
        });
        Glide.with(ctx)
                .load(searchCandidate.getData().get(position).getImage()).error(R.drawable.no_image)
                .into(holder.iv_img);
        if (searchCandidate.getData().get(position).getLicense_name() != null && !searchCandidate.getData().get(position).getLicense_name().equals("") && !searchCandidate.getData().get(position).getLicense_name().equals("null")) {
            // holder.tv_license.setText(searchCandidate.getData().get(position).getLicense_name() + "".replace("null", "N/A"));
            String ss = searchCandidate.getData().get(position).getLicense_name() + "";
//            ss = ss.replace("null", "N/A").replace(",", " , ");
            ss = ss.replace("null", "N/A").replace(",", ", ");
            holder.tv_license.setText(ss);


        } else {
//            holder.tv_license.setText("No LIC");
            holder.tv_license.setText("No License");
        }
        if (!searchCandidate.getData().get(position).getSpecialities_name().equals("")) {
//            holder.tvintrestval.setText(searchCandidate.getData().get(position).getSpecialities_name() + "".replace("null", "N/A")); String ss = searchCandidate.getData().get(position).getLicense_name() + "";
            String ss = searchCandidate.getData().get(position).getSpecialities_name() + "";
//            ss = ss.replace("null", "N/A").replace(",", " , ");
            ss = ss.replace("null", "N/A").replace(",", ", ");
            holder.tvintrestval.setText(ss);


        } else {
            holder.tvintrestval.setText("No Interest");
        }

        String expYear = searchCandidate.getData().get(position).getApplicant_experience_duration_years() + "";
        String expMonth = searchCandidate.getData().get(position).getApplicant_experience_duration_months() + "";
        String expNames = searchCandidate.getData().get(position).getApplicant_experience_speciality_name() + "";
        String s = "N/A";
        if (!expNames.contentEquals("")) {
            String arrexpYear[] = expYear.split(",");
            String arrexpMonth[] = expMonth.split(",");
            String arrexpNames[] = expNames.split(",");
            s = "";
            for (int i = 0; i < arrexpNames.length; i++) {

               /* if (arrexpYear[i].equals("0")) {
                    s = s + arrexpMonth[i] + " Month in " + arrexpNames[i] + "\n";
                } else if (arrexpMonth[i].equals("0")) {
                    s = s + arrexpYear[i] + " Year " + arrexpNames[i] + "\n";
                } else {
                    s = s + arrexpYear[i] + " Year " + arrexpMonth[i] + " Month in " + arrexpNames[i] + "\n";
                }*/
               /*25 July Pragna*/

                if (arrexpYear[i].equals("0")) {
                    s = s + arrexpMonth[i] + " Month in " + arrexpNames[i] + "\n";
                } else if (arrexpMonth[i].equals("0")) {
                    s = s + arrexpYear[i] + " Year " + arrexpNames[i] + "\n";
                } else {
/*25 july Pragna*/
                    String y = "";
                    String m = "";
//                                    s = s + arrexpYear[i] + " Year " + arrexpMonth[i] + " Month in " + arrexpNames[i] + "\n";
                    if (arrexpYear[i].contentEquals("1") || arrexpYear[i].contentEquals("0")) {
                        y = " year ";
                    } else {
                        y = " years ";
                    }
                    if (arrexpMonth[i].contentEquals("1") || arrexpMonth[i].contentEquals("0")) {
                        m = " month ";
                    } else {
                        m = " months ";
                    }

                    //   s = s + arrexpYear[i] + " Year " + arrexpMonth[i] + " Month in " + arrexpNames[i] + "\n";
                    s = s + arrexpYear[i] + y + arrexpMonth[i] + m + arrexpNames[i] + "\n";


                }

                //s = s +arrexpNames[i] + ",";
            }

//            s = s.substring(0, s.length() - 1);

            holder.tvexpval.setText(s + "");

        } else {
            holder.tvexpval.setText("No Experience");
        }
    }

    @Override
    public int getItemCount() {
        return searchCandidate.getData().size();
    }

    @Override
    public int getItemViewType(int position) {

        return position;
        //return super.getItemViewType(position);
    }
}
