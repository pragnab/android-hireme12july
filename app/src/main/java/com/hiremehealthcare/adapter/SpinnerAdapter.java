package com.hiremehealthcare.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import com.hiremehealthcare.CommonCls.CustomTextView;
import com.hiremehealthcare.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Harshida on 28-03-2018.
 * this adapter is use in dialog
 */

public class SpinnerAdapter extends BaseAdapter {


    private Context ctx;
    List<String> AllStatuslist = new ArrayList<>();

    ViewHolder holder = null;
    public FragmentManager f_manager;
    //private LayoutInflater inflater;
    private LayoutInflater inflater;
    SharedPreferences prefs;
    String customerId;
    View view;

    public SpinnerAdapter(Context ctx, List<String> AllStatuslist) {
        this.ctx = ctx;
        this.AllStatuslist = AllStatuslist;
        inflater = LayoutInflater.from(this.ctx);

    }

    @Override
    public int getCount() {
        return AllStatuslist.size();
    }

    @Override
    public Object getItem(int position) {
        return AllStatuslist.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    private class ViewHolder {

        CustomTextView txt_status;
        CustomTextView txt_status_show;
        CustomTextView txt_status_counter;
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        view = convertView;
        //  LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (view == null) {
            view = inflater.inflate(R.layout.spinner_raw, null);
            holder = new ViewHolder();
            holder.txt_status = (CustomTextView) view.findViewById(R.id.txt_spinner);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        //    holder.txt_status.setTypeface(Validations.setTypeface(ctx));


        holder.txt_status.setText(AllStatuslist.get(position));


        return view;
    }


}
