package com.hiremehealthcare.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.daimajia.swipe.SwipeLayout;
import com.hiremehealthcare.CommonCls.CustomButton;
import com.hiremehealthcare.CommonCls.CustomTextView;
import com.hiremehealthcare.POJO.ActivePositionModel;
import com.hiremehealthcare.POJO.Hours;
import com.hiremehealthcare.POJO.Shift;
import com.hiremehealthcare.activity.Qualified_CandidatesActivity;
import com.hiremehealthcare.R;
import com.hiremehealthcare.database.MySharedPrefereces;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by nareshp on 5/3/2018.
 */

public class ActivePositionAdapter extends RecyclerView.Adapter<ActivePositionAdapter.MyViewHolder> {
    private List<ActivePositionModel.Datum> dashbordManagerModelList;
    MySharedPrefereces mySharedPrefereces;
    Context context;
    List<Shift.ShiftBean> shiftBeanList;
    List<Hours.HoursBean> hoursBeanList;
    HashMap<Integer, List<View>> hashMap;
    RequestQueue queue;
    OnEditOrViewButtonListener onEditOrViewButtonListener;

    public interface OnEditOrViewButtonListener {

        public void onEditButtonClicked(ActivePositionModel.Datum data);
        public void onEdit_SideButtonClicked(ActivePositionModel.Datum data);

        public void onViewButtonClicked(ActivePositionModel.Datum data);

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public CustomTextView tvActivePositionLicense, tvActivePositionSpecility, tvActivePositionEpire;
        SwipeLayout slManagerDashBord;
        LinearLayout left_view, llShiftContainer, llHoursContainer;
        LinearLayout right_view;
        CustomButton btn_candidates, btnViewPosition, btnEditPosition;

        public MyViewHolder(View view) {
            super(view);

            slManagerDashBord = (SwipeLayout) view.findViewById(R.id.slManagerDashBord);
            left_view = (LinearLayout) view.findViewById(R.id.left_view);
            right_view = (LinearLayout) view.findViewById(R.id.right_view);
            llHoursContainer = (LinearLayout) view.findViewById(R.id.llHoursContainer);
            llShiftContainer = (LinearLayout) view.findViewById(R.id.llShiftContainer);
            tvActivePositionLicense = (CustomTextView) view.findViewById(R.id.tvActivePositionLicense);
            tvActivePositionSpecility = (CustomTextView) view.findViewById(R.id.tvActivePositionSpecility);
            tvActivePositionEpire = (CustomTextView) view.findViewById(R.id.tvActivePositionEpire);
            btn_candidates = (CustomButton) view.findViewById(R.id.btn_candidates);
            btnViewPosition = (CustomButton) view.findViewById(R.id.btnViewPosition);
            btnEditPosition = (CustomButton) view.findViewById(R.id.btnEditPosition);

        }
    }


    public ActivePositionAdapter(List<ActivePositionModel.Datum> dashbordManagerModelList, Context context, List<Shift.ShiftBean> shiftBeanList, List<Hours.HoursBean> hoursBeanList, OnEditOrViewButtonListener onEditOrViewButtonListener) {
        this.dashbordManagerModelList = dashbordManagerModelList;
        this.context = context;
        queue = Volley.newRequestQueue(context);
        mySharedPrefereces = new MySharedPrefereces(context);
        this.shiftBeanList = shiftBeanList;
        this.hoursBeanList = hoursBeanList;
        hashMap = new HashMap<>();
        this.onEditOrViewButtonListener = onEditOrViewButtonListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.active_position_row_inflater, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {
        final ActivePositionModel.Datum data = dashbordManagerModelList.get(position);
        holder.slManagerDashBord.setShowMode(SwipeLayout.ShowMode.LayDown);
        holder.slManagerDashBord.addDrag(SwipeLayout.DragEdge.Left, holder.left_view);
        holder.slManagerDashBord.addDrag(SwipeLayout.DragEdge.Right, holder.right_view);
        if (data != null) {
//            holder.tvActivePositionLicense.setText(data.getLicenseName() + "".replace(",", " , "));
           // holder.tvActivePositionLicense.setText(data.getLicenseName() + "".replace(",", ", "));
            String s=data.getLicenseName() + "";
            s=s.replace(",",", ");
//            holder.tvActivePositionLicense.setText(data.getLicenseName() + "".replace(",", ", "));
            holder.tvActivePositionLicense.setText(s+"");
//            holder.tvActivePositionSpecility.setText(data.getSpecialtyName() + "".replace(",", " , "));
        //    holder.tvActivePositionSpecility.setText(data.getSpecialtyName() + "".replace(",", ", "));
            String s1=data.getSpecialtyName()+"";
            s1=s1.replace(",",", ");
//            holder.tvActivePositionSpecility.setText(data.getSpecialtyName() + "".replace(",", ", "));
            holder.tvActivePositionSpecility.setText(s1+"");
        }
        //holder.tvActivePositionEpire.setText(data.get);
        LayoutInflater inflater = (LayoutInflater) context.getSystemService
                (Context.LAYOUT_INFLATER_SERVICE);

        LinearLayout linearLayoutHour = null;
        int tempCounterHour = 0;
        int lastPositionHour = 6;
        final List<View> viewListHour = new ArrayList<>();
        holder.llHoursContainer.removeAllViews();
        for (int counterHour = 0; counterHour < hoursBeanList.size(); counterHour++) {
            tempCounterHour++;
            if (tempCounterHour == 1) {
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                linearLayoutHour = new LinearLayout(context);
                linearLayoutHour.setWeightSum(2f);
                linearLayoutHour.setLayoutParams(params);
            }
            View view = inflater.inflate(R.layout.textview_custom_item_inflater, null);
            CustomTextView tvCustomTextView = (CustomTextView) view.findViewById(R.id.tvCustomTextView);
            tvCustomTextView.setText(hoursBeanList.get(counterHour).getHours_name());
            LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 1f);
            params1.setMargins(3, 3, 3, 3);
            view.setLayoutParams(params1);
            if (hoursBeanList.get(counterHour).getHours_name().equals(data.getHourName())) {
                view.setBackground(context.getResources().getDrawable(R.drawable.border_checkbox_selelct));
            } else {
                view.setBackground(context.getResources().getDrawable(R.drawable.border_uncheck_select));
            }
            /*8-feb-2019 pragna*/
            holder.left_view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onEditOrViewButtonListener.onEditButtonClicked(data);
                }
            });
            /*25 may pragna*/
            holder.btn_candidates.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    get_qualified_candidates_for_position(dashbordManagerModelList.get(position).getPositionId() + "");
                }
            });
            holder.btnViewPosition.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    /*Intent intent=new Intent(context,ViewPositionDetailActivity.class);
                    intent.putExtra("position_id", data.getPositionId());
                    context.startActivity(intent);*/
                    onEditOrViewButtonListener.onViewButtonClicked(data);
                }
            });
            holder.btnEditPosition.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onEditOrViewButtonListener.onEditButtonClicked(data);
                }
            });


            /*view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    for(View view1:viewListHour)
                    {
                        if(view==view1)
                        {
                            view1.setBackground(context.getResources().getDrawable(R.drawable.border_zfill));
                        }
                        else
                        {
                            view1.setBackground(context.getResources().getDrawable(R.drawable.border_yfill));
                        }
                    }
                }
            });*/
            linearLayoutHour.addView(view);
            viewListHour.add(view);
            if (tempCounterHour == 2 || counterHour == hoursBeanList.size() - 1) {
                holder.llHoursContainer.addView(linearLayoutHour);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                linearLayoutHour = new LinearLayout(context);
                linearLayoutHour.setWeightSum(2f);
                linearLayoutHour.setLayoutParams(params);
                tempCounterHour = 0;
            }

        }


        LinearLayout linearLayoutShift = null;
        int tempCounterShift = 0;
        int lastPositionShift = 6;
        final List<View> viewListShift = new ArrayList<>();
        holder.llShiftContainer.removeAllViews();
        for (int counterShift = 0; counterShift < shiftBeanList.size(); counterShift++) {
            tempCounterShift++;
            if (tempCounterShift == 1) {
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                linearLayoutShift = new LinearLayout(context);
                linearLayoutShift.setWeightSum(2f);
                linearLayoutShift.setLayoutParams(params);


            }
            View view = inflater.inflate(R.layout.textview_custom_item_inflater, null);
            CustomTextView tvCustomTextView = (CustomTextView) view.findViewById(R.id.tvCustomTextView);
            tvCustomTextView.setText(shiftBeanList.get(counterShift).getShift_name());
            LinearLayout.LayoutParams params1 = new LinearLayout.LayoutParams(0, LinearLayout.LayoutParams.WRAP_CONTENT, 1f);
            params1.setMargins(3, 3, 3, 3);
            view.setLayoutParams(params1);
            /*view.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    for(View view1:viewListShift)
                    {
                        if(view==view1)
                        {
                            view1.setBackground(context.getResources().getDrawable(R.drawable.border_zfill));
                        }
                        else
                        {
                            view1.setBackground(context.getResources().getDrawable(R.drawable.border_yfill));
                        }
                    }
                }
            });*/
            if (shiftBeanList.get(counterShift).getShift_name().equals(data.getShiftName())) {
                view.setBackground(context.getResources().getDrawable(R.drawable.border_checkbox_selelct));
            } else {
                view.setBackground(context.getResources().getDrawable(R.drawable.border_uncheck_select));
            }
            linearLayoutShift.addView(view);
            viewListShift.add(view);
            if (tempCounterShift == 2 || counterShift == shiftBeanList.size() - 1) {
                holder.llShiftContainer.addView(linearLayoutShift);
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(LinearLayout.LayoutParams.MATCH_PARENT, LinearLayout.LayoutParams.WRAP_CONTENT);
                linearLayoutShift = new LinearLayout(context);
                linearLayoutShift.setWeightSum(2f);
                linearLayoutShift.setLayoutParams(params);
                tempCounterShift = 0;
            }

        }

    }

    private void get_qualified_candidates_for_position(String PosID) {
        Intent i = new Intent(context, Qualified_CandidatesActivity.class);
        i.putExtra("ID", PosID + "");
        context.startActivity(i);

        //   DialogUtils.showProgressDialog(context, "Please Wait...");

//        String Url = URLS.MANAGER_GET_QUALIFIED_CANDIDATES_FOR_POSITION + mySharedPrefereces.getUserId() + "&manager_zipcode=" + mySharedPrefereces.getZipCode() + "&position_id=" + PosID + "";
//        System.out.println("this is get_qualified_candidates_for_position url " + Url + "");
//        StringRequest req = new StringRequest(Request.Method.GET, Url, new Response.Listener<String>() {
//
//            @Override
//            public void onResponse(String response) {
//                System.out.println("get_qualified_candidates_for_position RESPONSE " + response + "");
//                DialogUtils.hideProgressDialog();
//                Gson gson = new Gson();
//                SearchCandidate searchCandidate = gson.fromJson(response, SearchCandidate.class);
//////                Type collectionType = new TypeToken<Collection<Speciality>>() {
//////                }.getType();
//////                Collection<Speciality> speciality = gson.fromJson(response, collectionType);
//                if (searchCandidate != null) {
//                    if (searchCandidate.getData().size() > 0) {
//
//                    } else {
//                        DialogUtils.showDialog(context, context.getResources().getString(R.string.app_name), "No Qualified Candidates for this Position");
//                    }
//                }
//
//
//
//            }
//        }, new Response.ErrorListener() {
//            @Override
//            public void onErrorResponse(VolleyError error) {
//                NetworkResponse networkResponse = error.networkResponse;
//                DialogUtils.hideProgressDialog();
//                if (networkResponse != null) {
//                    int statusCode = error.networkResponse.statusCode;
//                    System.out.println("STATUS CODE " + statusCode + "");
//                    //      DialogUtils.showDialog(context, context.getString(R.string.app_name) statusCode + "", null);
//                }
//
//            }
//        });
//        req.setRetryPolicy(new DefaultRetryPolicy(
//                100000,
//                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
//                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
//
//
//        queue.add(req);

    }

    @Override
    public int getItemCount() {
        return dashbordManagerModelList.size();
    }

    @Override
    public int getItemViewType(int position) {

        return position;
        //return super.getItemViewType(position);
    }
}
