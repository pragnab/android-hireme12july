package com.hiremehealthcare.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.hiremehealthcare.CommonCls.CustomTextView;
import com.hiremehealthcare.POJO.ApplicantNearPositionModel;
import com.hiremehealthcare.R;


import java.util.List;

/**
 * Created by Harshida on 28-03-2018.
 * this adapter is use in dialog
 */

public class ApplicationNearPos_ListAdapter extends BaseAdapter {
//public class ApplicationNearPos_ListAdapter extends BaseFlipAdapter {


    private Context ctx;
//    List<String> AllStatuslist = new ArrayList<>();

    ViewHolder holder = null;
    public FragmentManager f_manager;
    //private LayoutInflater inflater;
    private LayoutInflater inflater;
    SharedPreferences prefs;
    String customerId;
    View view;
    List<ApplicantNearPositionModel.Data> applicantNearPositionDataList;

    boolean fromSearch;

    public ApplicationNearPos_ListAdapter(Context ctx, List<ApplicantNearPositionModel.Data> applicantNearPositionDataList, boolean fromSearch) {
        this.ctx = ctx;
        this.fromSearch = fromSearch;
        inflater = LayoutInflater.from(this.ctx);
        this.applicantNearPositionDataList = applicantNearPositionDataList;
    }

    @Override
    public int getCount() {
        return applicantNearPositionDataList.size();
    }

    @Override
    public Object getItem(int position) {
        return applicantNearPositionDataList.get(position);
    }


    @Override
    public long getItemId(int position) {
        return position;
    }


    private class ViewHolder {

        TextView tvFacilityName,tvFacilityLicense,tvFacilityCertificate;
        CustomTextView txt_status_show;
        CustomTextView txt_status_counter;
        Button btn_availability;
        LinearLayout lin_master;
        ViewFlipper row_item_flipper;
     //   ViewFlipper mViewFlipper;
    }

    @NonNull
    @Override
    public View getView(final int position, final View convertView, ViewGroup parent) {
        view = convertView;
        //  LayoutInflater inflater = (LayoutInflater) context.getSystemService(Cont ext.LAYOUT_INFLATER_SERVICE);
        if (view == null) {
            view = inflater.inflate(R.layout.temp, null);
            holder = new ViewHolder();
            holder.tvFacilityName = (TextView) view.findViewById(R.id.tvFacilityName);
            holder.tvFacilityLicense = (TextView) view.findViewById(R.id.tvFacilityLicense);
            holder.tvFacilityCertificate = (TextView) view.findViewById(R.id.tvFacilityCertificate);
            holder.btn_availability = (Button) view.findViewById(R.id.btn_availability);
            holder.lin_master = (LinearLayout) view.findViewById(R.id.lin_master);
            holder.row_item_flipper = (ViewFlipper) view.findViewById(R.id.row_item_flipper);
            // Get the ViewFlipper
          //  holder.      mViewFlipper = (ViewFlipper) view.findViewById(R.id.viewFlipper);
//            holder._easEasyFlipView = (EasyFlipView) view.findViewById(R.id.myEasyFlipView);

            view.setTag(holder);
            holder. btn_availability.setTag(position);
        } else {
            holder = (ViewHolder) view.getTag();
        }

        //    holder.txt_status.setTypeface(Validations.setTypeface(ctx));


        holder.tvFacilityName.setText(applicantNearPositionDataList.get(position).getFacilityName() + "");
        holder.tvFacilityLicense.setText(applicantNearPositionDataList.get(position).getLicenseName() + "");
        holder.tvFacilityCertificate.setText(applicantNearPositionDataList.get(position).getCertificateName() + "");
        // Add all the images to the ViewFlipper
        for (int i = 0; i < 2; i++) {
            ImageView imageView = new ImageView(ctx);
            imageView.setImageResource(R.drawable.logo_u);
            holder.  row_item_flipper.addView(imageView);
        }
        if(holder.btn_availability.getTag().equals(position)) {
            holder.btn_availability.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
//           View     a =new CustomTextView(ctx);
                   // holder.btn_availability.animate().translationY(200);
                    System.out.println("THIS IS POSTION TAG FOUND FOR>>>>>>>????????????? "+position+"");
//                    holder.mViewFlipper.startFlipping();
holder.row_item_flipper.showNext();
               //  holder.   lin_master.setVisibility(View.INVISIBLE);
              //   notifyDataSetChanged();

                }
            });
        }

        return view;
    }




}
