package com.hiremehealthcare.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.hiremehealthcare.CommonCls.CustomTextView;
import com.hiremehealthcare.POJO.EducationDetailModel;
import com.hiremehealthcare.POJO.ExperienceListModel;
import com.hiremehealthcare.R;

import java.util.List;

/**
 * Created by ADMIN on 30-04-2018.
 */

public class ExperienceListAdpter extends BaseAdapter {
    private Context mContext;
    List<ExperienceListModel.Data> educationDetailModelList;
    String profileId;
    View view;
    ViewHolder holder = null;
    private LayoutInflater inflater;
    public interface OnGettingEditAndDeleteMethod {
        public void onDeleteButtonClicked(int postion);
        public void onUpdateButtonClicked(int position);
    }
    OnGettingEditAndDeleteMethod onGettingEditAndDeleteMethod;
    boolean isShowAction;

    // SearchHistoryAdapter(Context mContext,List<GridRecord> gridData)
    public ExperienceListAdpter(Context mContext, List<ExperienceListModel.Data> educationDetailModelList, OnGettingEditAndDeleteMethod onGettingEditAndDeleteMethod,boolean isShowAction) {
        this.mContext = mContext;
        inflater = LayoutInflater.from(this.mContext);
        this.onGettingEditAndDeleteMethod =onGettingEditAndDeleteMethod;
        this.educationDetailModelList=educationDetailModelList;
        this.isShowAction=isShowAction;
    }


    @Override
    public int getCount() {
//        return gridData.size();
        return educationDetailModelList.size();
    }

    private class ViewHolder {


    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {


        view = convertView;
        ExperienceListModel.Data data=educationDetailModelList.get(position);
        //  LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (view == null) {
            view = inflater.inflate(R.layout.experience_row_inflater, null);
            holder = new ViewHolder();
            //  holder.itemCheckBox = (CheckBox) view.findViewById(R.id.itemCheckBox);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        //   holder.itemCheckBox.setTag(position); // This line is important.

        LinearLayout lintop = (LinearLayout) view.findViewById(R.id.lintop);
        CustomTextView tvLocation = (CustomTextView) view.findViewById(R.id.tvLocation);
        CustomTextView tvActionTitle = (CustomTextView) view.findViewById(R.id.tvActionTitle);

        CustomTextView tvSpeciality = (CustomTextView) view.findViewById(R.id.tvSpeciality);
        CustomTextView tvDuration = (CustomTextView) view.findViewById(R.id.tvDuration);
        ImageView ivExpUpdate = (ImageView) view.findViewById(R.id.ivExpUpdate);
        ivExpUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onGettingEditAndDeleteMethod.onUpdateButtonClicked(position);
            }
        });
        ImageView ivExpDelete = (ImageView) view.findViewById(R.id.ivExpDelete);
        ivExpDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onGettingEditAndDeleteMethod.onDeleteButtonClicked(position);
            }
        });
        if(isShowAction)
        {
            tvActionTitle.setVisibility(View.VISIBLE);
            ivExpUpdate.setVisibility(View.VISIBLE);
            ivExpDelete.setVisibility(View.VISIBLE);
        }
        else
        {
            tvActionTitle.setVisibility(View.GONE);
            ivExpUpdate.setVisibility(View.GONE);
            ivExpDelete.setVisibility(View.GONE);
        }
        tvLocation.setText(data.getApplicant_experience_location());
        tvSpeciality.setText(data.getSpeciality());
        tvDuration.setText(data.getExperience_label());
//        CustomTextView tv_action = (CustomTextView) view.findViewById(R.id.tv_action);
//        tv_action.setTypeface(Validations.setTypeface(mContext));
        if (position == 0) {
            lintop.setVisibility(View.VISIBLE);
        } else {
            lintop.setVisibility(View.GONE);
        }
        return view;
    }
}

