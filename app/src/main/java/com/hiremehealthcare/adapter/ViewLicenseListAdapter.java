package com.hiremehealthcare.adapter;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AlertDialog;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.hiremehealthcare.CommonCls.CustomTextView;
import com.hiremehealthcare.CommonCls.Validations;
import com.hiremehealthcare.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pragna on 30-4-2018.
 */

public class ViewLicenseListAdapter extends BaseAdapter {


    private Context ctx;
    // List<String> AllStatuslist = new ArrayList<>();
    //   Certificates certificates = new Certificates();
    ViewHolder holder = null;
    public FragmentManager f_manager;
    //private LayoutInflater inflater;
    private LayoutInflater inflater;
    SharedPreferences prefs;
    String customerId;
    View view;
    private int selectedPosition = -1;
    private ArrayList<String> selected = new ArrayList<String>();
    List<String> listlicesnseYear;
    List<String> listlicesnseMonth;
    List<String> listlicesnseNo;
    List<String> listlicesnseFile;
    Activity activity;
    private ImageView imageView;
    private ImageView ivCloseDialog;
    /* public ViewLicenseListAdapter(Certificates certificates, Context ctx) {
         this.ctx = ctx;
         this.certificates = certificates;
         inflater = LayoutInflater.from(this.ctx);

     }
 */
    public ViewLicenseListAdapter(Context ctx, List<String> listlicesnseYear, List<String> listlicesnseMonth, List<String> listlicesnseNo, List<String> listlicesnseFile, Activity activity) {
        this.ctx = ctx;
        this.activity=activity;
        inflater = LayoutInflater.from(this.ctx);
        this.listlicesnseYear = listlicesnseYear;
        this.listlicesnseMonth = listlicesnseMonth;
        this.listlicesnseNo = listlicesnseNo;
        this.listlicesnseFile = listlicesnseFile;
    }

    @Override
    public int getCount() {
        return listlicesnseNo.size();
    }

    @Override
    public Object getItem(int position) {
        return listlicesnseNo.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void clearSelected() {

        selected.clear();
    }

    public ArrayList<String> getSelected() {


        return selected;
    }

    private class ViewHolder {

        // CheckBox itemCheckBox;
        LinearLayout lintop = (LinearLayout) view.findViewById(R.id.lintop);
        CustomTextView tvLicenseCertificate = (CustomTextView) view.findViewById(R.id.tvLicenseCertificate);
        CustomTextView tvLicenseEarned = (CustomTextView) view.findViewById(R.id.tvLicenseEarned);
        CustomTextView tvLicenseExpire = (CustomTextView) view.findViewById(R.id.tvLicenseExpire);
        ImageView ivLicenseDelete = (ImageView) view.findViewById(R.id.ivLicenseDelete);
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        view = convertView;
        //  LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (view == null) {
            view = inflater.inflate(R.layout.profile_license_adpter_, null);
            holder = new ViewHolder();
            holder.tvLicenseCertificate = (CustomTextView) view.findViewById(R.id.tvLicenseCertificate);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.lintop.setTag(position); // This line is important.
        if (position == 0) {
            holder.lintop.setVisibility(View.VISIBLE);
        } else {
            holder.lintop.setVisibility(View.GONE);
        }
        holder.tvLicenseCertificate.setText(listlicesnseNo.get(position) + "");
        String s=listlicesnseFile.get(position) + "";
        int i=s.lastIndexOf("/");
        s=s.substring(i+1,s.length());
        holder.tvLicenseEarned.setText(s+ "");
        if (listlicesnseYear.size() > position) {
            holder.tvLicenseExpire.setText(listlicesnseMonth.get(position)+"/"+listlicesnseYear.get(position) + "");
        } else {
            holder.tvLicenseExpire.setText("N/A");
        }
        holder.tvLicenseCertificate.setTypeface(Validations.setTypeface(ctx));
        holder.tvLicenseEarned.setTypeface(Validations.setTypeface(ctx));
        holder.tvLicenseExpire.setTypeface(Validations.setTypeface(ctx));
        holder.tvLicenseEarned.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onLicenseButtonClicked(ctx,listlicesnseFile.get(position) + "");
            }
        });
        //    holder.txt_status.setTypeface(Validations.setTypeface(ctx));
//        if (position == selectedPosition) {
//            holder.itemCheckBox.setChecked(true);
//        } else {
//            holder.itemCheckBox.setChecked(false);
//        }
//        holder.itemCheckBox.setChecked(selected.contains(certificates.getCertification().get(position).getCertification_id() + ""));
//        // holder.itemCheckBox.setOnClickListener(onStateChangedListener(holder.itemCheckBox, position));
//        holder.itemCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
//            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
//                int pos = (Integer) buttonView.getTag();
//                if (!buttonView.isChecked()) {
//                    selected.remove(certificates.getCertification().get(pos).getCertification_id() + "");
//                } else if (buttonView.isChecked()) {
//                    if (!selected.contains(certificates.getCertification().get(pos).getCertification_id() + "")) {
//                        selected.add(certificates.getCertification().get(pos).getCertification_id() + "");
//                    }
//                }
//            }
//        });
        return view;
    }

    public void onLicenseButtonClicked(Context ctx,String filename) {
        LayoutInflater inflater = LayoutInflater.from(activity);
        final View dialogView = inflater.inflate(R.layout.fullimage, null);
        this.ivCloseDialog = (ImageView) dialogView.findViewById(R.id.ivCloseDialog);
        this.imageView = (ImageView) dialogView.findViewById(R.id.imageView);
        System.out.println("IMAGE PATH "+filename+"");
//
        final AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(activity, R.style.myDialog));
        final AlertDialog b = builder.create();
        //  builder.setTitle("Material Style Dialog");
        builder.setCancelable(true);
        builder.setView(dialogView);
        b.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        Glide.with(ctx)
                .load(filename+"")
                .error(R.drawable.no_image)
                .into(imageView);

        final AlertDialog show = builder.show();
        ivCloseDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                show.dismiss();
            }
        });
        /*dialogButtonOKButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                show.dismiss();

            }
        });*/
    }



}
