package com.hiremehealthcare.adapter;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ViewFlipper;


import com.hiremehealthcare.CommonCls.AnimationFactory;
import com.hiremehealthcare.CommonCls.CustomBoldTextView;
import com.hiremehealthcare.CommonCls.CustomTextView;
import com.hiremehealthcare.POJO.ApplicantNearPositionModel;
import com.hiremehealthcare.R;
//import com.wajahatkarim3.easyflipviewpager.BookFlipPageTransformer;

import java.util.List;

import static com.hiremehealthcare.R.animator.card_flip_left_in;
import static com.hiremehealthcare.R.animator.card_flip_left_out;
import static com.hiremehealthcare.R.animator.card_flip_right_out;
import static com.hiremehealthcare.R.animator.flipin;
import static com.hiremehealthcare.R.animator.flipout;
import static com.hiremehealthcare.R.animator.shrink_to_middle;


public class ApplicantNearPositionAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<ApplicantNearPositionModel.Data> applicantNearPositionDataList;
    OnRecyclerViewItemClicked onRecyclerViewItemClicked;
    private static final int VIEW_TYPE_EMPTY_LIST_PLACEHOLDER = 0;
    private static final int VIEW_TYPE_OBJECT_VIEW = 1;
    int empty_return = 0;
    boolean fromSearch = false;
    View v_v;
    private Animation slide_in_left, slide_in_right, slide_out_left, slide_out_right;
    Context context;

    public interface OnRecyclerViewItemClicked {
        public void onRecyclerItemClicked(ApplicantNearPositionModel.Data data);

        public void onHeartButtonClicked(ApplicantNearPositionModel.Data data);

        public void onAvailabilityButtonClicked(ApplicantNearPositionModel.Data data);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public CustomTextView tvFacilityTime, tvFacilityShift, tvFacilitySpeciality, tvFacilityCertificate, tvFacilityLicense;
        public CustomBoldTextView tvFacilityName;
        public ImageView ivHeartButton;
        Button btn_availability;
        public ViewFlipper row_item_flipper;
        ImageView imageView2, imageView1;

        public MyViewHolder(View view) {
            super(view);
            ivHeartButton = (ImageView) view.findViewById(R.id.ivHeartButton);
            tvFacilityName = (CustomBoldTextView) view.findViewById(R.id.tvFacilityName);
            tvFacilityTime = (CustomTextView) view.findViewById(R.id.tvFacilityTime);
            tvFacilityShift = (CustomTextView) view.findViewById(R.id.tvFacilityShift);
            tvFacilitySpeciality = (CustomTextView) view.findViewById(R.id.tvFacilitySpeciality);
            tvFacilityLicense = (CustomTextView) view.findViewById(R.id.tvFacilityLicense);
            tvFacilityCertificate = (CustomTextView) view.findViewById(R.id.tvFacilityCertificate);
            btn_availability = (Button) view.findViewById(R.id.btn_availability);


            imageView1 = (ImageView) view.findViewById(R.id.imageView1);
            imageView2 = (ImageView) view.findViewById(R.id.imageView2);

            row_item_flipper = (ViewFlipper) view.findViewById(R.id.row_item_flipper);
        }

        public void bindClickedListener(final ApplicantNearPositionModel.Data data) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onRecyclerViewItemClicked.onRecyclerItemClicked(data);
                }
            });
        }
    }

    public class MyEmptyViewHolder extends RecyclerView.ViewHolder {
        public CustomTextView tvFacilityTime, tvFacilityShift, tvFacilitySpeciality, tvFacilityCertificate;
        public CustomBoldTextView tvFacilityName;
        public ImageView ivHeartButton;

        public MyEmptyViewHolder(View view) {
            super(view);
        }
    }


    public ApplicantNearPositionAdapter(List<ApplicantNearPositionModel.Data> applicantNearPositionDataList, boolean fromSearch, Context context, OnRecyclerViewItemClicked onRecyclerViewItemClicked) {
        this.applicantNearPositionDataList = applicantNearPositionDataList;
        this.onRecyclerViewItemClicked = onRecyclerViewItemClicked;
        this.fromSearch = fromSearch;
        this.context = context;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        if (viewType == VIEW_TYPE_EMPTY_LIST_PLACEHOLDER) {
            System.out.println("VIEW_TYPE_EMPTY_LIST_PLACEHOLDER :::::::::::::::::::::::: ");
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.empty_data_layout, parent, false);

            return new MyEmptyViewHolder(itemView);
        } else {
            System.out.println("NOT VIEW_TYPE_EMPTY_LIST_PLACEHOLDER :::::::::::::::::::::::: ");
     /*   View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.applicant_near_position_row_inflater, parent, false);*/
//            View itemView = LayoutInflater.from(parent.getContext())
//                    .inflate(R.layout.tempfor_test_flip, parent, false);
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.temp, parent, false);
            v_v= LayoutInflater.from(parent.getContext()).inflate(R.layout.testforflip, parent, false);
            return new MyViewHolder(itemView);
        }
    }


    //    @SuppressLint("ResourceType")
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if (viewHolder instanceof MyViewHolder) {
            final MyViewHolder holder = (MyViewHolder) viewHolder;
            final ApplicantNearPositionModel.Data data = applicantNearPositionDataList.get(position);
            holder.bindClickedListener(data);
            holder.tvFacilityName.setText(data.getFacilityName());



//            holder.imageView1.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//                    System.out.println("IMAGE VIEW CLICKED.......................");
//                    AnimationFactory.flipTransition(holder.row_item_flipper,
//                            AnimationFactory.FlipDirection.LEFT_RIGHT);
//                    TextView imageView = new TextView(context);
//                    imageView.setText("R.drawable.logo");
////
//                    holder.row_item_flipper.addView(imageView);
//                    holder.row_item_flipper.showNext();
//                }
//            });
            holder.btn_availability.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                    onRecyclerViewItemClicked.onAvailabilityButtonClicked(data);
//                    System.out.println("THIS IS CLIKED......................... >>>>>>>>>>>>>>>>>>>>>>       ");
//
////                    ViewPager vp=new ViewPager(context);
//
//                    //  for (int i = 0; i < 2; i++) {
//
//                    TextView imageView = new TextView(context);
//                    imageView.setText("R.drawable.logo");
////
//                    holder.row_item_flipper.addView(imageView);
//
//
//                    //   BookFlipPageTransformer bookFlipPageTransformer = new BookFlipPageTransformer();
//
////// Enable / Disable scaling while flipping. If true, then next page will scale in (zoom in). By default, its true.
////                    bookFlipPageTransformer.setEnableScale(true);
////
////// The amount of scale the page will zoom. By default, its 5 percent.
////                    bookFlipPageTransformer.setScaleAmountPercent(10f);
////
////                    //   vp.setPageTransformer();
//////                    YoYo.with(Techniques.SlideInRight)
//////                            .duration(700)
//////
//////                            .playOn(imageView);
//                    holder.row_item_flipper.setAutoStart(false);
//
//                    AnimationFactory.flipTransition(holder.row_item_flipper,
//                            AnimationFactory.FlipDirection.LEFT_RIGHT);


                    //    holder.row_item_flipper.setAnimateFirstView(true);

// Assign the page transformer to the ViewPager.
//                    vp.setPageTransformer(true, bookFlipPageTransformer);



                   // v_v= LayoutInflater.from(context).inflate(R.layout.testforflip, parent, false);
                  AnimationFactory.flipTransition(holder.row_item_flipper,
                            AnimationFactory.FlipDirection.LEFT_RIGHT);
                //    TextView imageView = new TextView(context);
                //    imageView.setText("R.drawable.logo");
//


                    if(v_v.getParent() != null) {
                        ((ViewGroup)v_v.getParent()).removeView(v_v); // <- fix
                    }
                    else{
                        System.out.println("THIS IS ELSE CONDTION AAAAAAAAAAAAAAAAAAAAAAAAAAAAA ");
                    }
                    holder.  row_item_flipper.addView(v_v);
                    holder.row_item_flipper.showNext();
                    v_v.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            System.out.println("this is clicked:::::::::::::::::::::::::::::");
                       //     AnimationFactory.flipTransition(holder.row_item_flipper,
                           //         AnimationFactory.FlipDirection.RIGHT_LEFT);

                            holder.row_item_flipper.showPrevious();


                        }
                    });


                    //   holder.row_item_flipper.setInAnimation(slide_in_right);
                    // holder.row_item_flipper.setInAnimation(context,R.animator.);
                    //      holder.row_item_flipper.setInAnimation(context, card_flip_left_out);


                    //   holder.row_item_flipper.setInAnimation(context,flipin);
                    //    holder.row_item_flipper.setOutAnimation(context,flipout);


                    // holder.row_item_flipper.setInAnimation(context,card_flip_left_in);
                    //    holder.row_item_flipper.setOutAnimation(context,card_flip_right_out);

                    //      holder.row_item_flipper.setInAnimation(context,flipout);
//                    holder.row_item_flipper.setOutAnimation(context,flipout);
                 //   holder.row_item_flipper.showNext();


                    // }

                }
            });


            //  slide_in_left = AnimationUtils.loadAnimation(context, R.animator.card_flip_left_out);
//            slide_in_right = AnimationUtils.loadAnimation(this, R.anim.slide_in_right);
//            slide_out_left = AnimationUtils.loadAnimation(this, R.anim.slide_out_left);
//            slide_out_right = AnimationUtils.loadAnimation(this, R.anim.slide_out_right);
//            holder.btn_availability.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//
//                    onRecyclerViewItemClicked.onAvailabilityButtonClicked(data);
//                    System.out.println("THIS IS CLIKED......................... >>>>>>>>>>>>>>>>>>>>>>       ");
//
////                    ViewPager vp=new ViewPager(context);
//
//                  //  for (int i = 0; i < 2; i++) {
//
//                    TextView imageView = new TextView(context);
//                    imageView.setText("R.drawable.logo");
////                    vp.addView(imageView);
//                    holder.  row_item_flipper.addView(imageView);
////                    holder.  row_item_flipper.addView(vp);
//                    BookFlipPageTransformer bookFlipPageTransformer = new BookFlipPageTransformer();
//
////// Enable / Disable scaling while flipping. If true, then next page will scale in (zoom in). By default, its true.
////                    bookFlipPageTransformer.setEnableScale(true);
////
////// The amount of scale the page will zoom. By default, its 5 percent.
////                    bookFlipPageTransformer.setScaleAmountPercent(10f);
////
////                    //   vp.setPageTransformer();
//////                    YoYo.with(Techniques.SlideInRight)
//////                            .duration(700)
//////
//////                            .playOn(imageView);
//                    holder.row_item_flipper.setAutoStart(false);
//                    holder.row_item_flipper.setAnimateFirstView(true);
//
//// Assign the page transformer to the ViewPager.
////                    vp.setPageTransformer(true, bookFlipPageTransformer);
//                    imageView.setOnClickListener(new View.OnClickListener() {
//                        @Override
//                        public void onClick(View view) {
//                            System.out.println("this is clicked:::::::::::::::::::::::::::::");
//                            holder.row_item_flipper.showPrevious();
//                        }
//                    });
//                 //   holder.row_item_flipper.setInAnimation(slide_in_right);
//                   // holder.row_item_flipper.setInAnimation(context,R.animator.);
//              //      holder.row_item_flipper.setInAnimation(context, card_flip_left_out);
//
//
//                   holder.row_item_flipper.setInAnimation(context,flipin);
//                    holder.row_item_flipper.setOutAnimation(context,flipout);
//
//              //      holder.row_item_flipper.setInAnimation(context,flipout);
////                    holder.row_item_flipper.setOutAnimation(context,flipout);
//                        holder.row_item_flipper.showNext();
//                   // }
//
//                }
//            });


            holder.ivHeartButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onRecyclerViewItemClicked.onHeartButtonClicked(data);
                    if (fromSearch) {


                        holder.ivHeartButton.setImageResource(R.drawable.ic_favorite_black_24dp);
                    } else {
                        holder.ivHeartButton.setImageResource(R.drawable.ic_favorite_red_24dp);

                    }
                }
            });
            /*   holder.tvFacilityTime.setText(data.getHourName()+"");*/

            String hr = data.getHourName() + "";
            hr = hr.replace("null", "");
            hr = hr.replace("NULL", "");
            holder.tvFacilityTime.setText(hr + "");
            holder.tvFacilityShift.setText(data.getShiftName() + "");
            holder.tvFacilitySpeciality.setText(data.getSpecialtyName() + "");
//            holder.tvFacilityCertificate.setText(data.getCertificateName());
            /*13-July Pragna */
            String certificateName = data.getCertificateName() + "";
            certificateName = certificateName.replace("null", "");
            certificateName = certificateName.replace("NULL", "");
            System.out.println("certificateName 0  "+certificateName+"");
//            holder.tvFacilityCertificate.setText(certificateName + "");
            holder.tvFacilityCertificate.setText(certificateName + "");
            holder.tvFacilityLicense.setText(position + " :  " + data.getLicenseName() + "");


            /*26-sept pragna */
            if (fromSearch) {
                if (data.getApp_status().contentEquals("0")) {

                    holder.ivHeartButton.setImageResource(R.drawable.ic_favorite_black_24dp);
                } else {
                    holder.ivHeartButton.setImageResource(R.drawable.ic_favorite_red_24dp);
                }
            }
        } else {

        }


    }

    @Override
    public int getItemCount() {
        System.out.println("THIS IS MUST PRINT ::::::::::::::::: applicantNearPositionDataList.size()  "+applicantNearPositionDataList.size() +"");
        if (applicantNearPositionDataList.size() > 0) {
            return applicantNearPositionDataList.size();
        } else {
            return empty_return;
        }
    }

    @Override
    public long getItemId(int position) {
        //    return super.getItemId(position);
        return position;
    }

    public void showDataEmptyDate() {
        empty_return = 1;
    }

    @Override
    public int getItemViewType(int position) {
        System.out.println("applicantNearPositionDataList.isEmpty() "+applicantNearPositionDataList.isEmpty()+"");
        if (applicantNearPositionDataList.isEmpty()) {
            return VIEW_TYPE_EMPTY_LIST_PLACEHOLDER;
        }
        else {
//            return position;
             return VIEW_TYPE_OBJECT_VIEW;
        }
    }
           //return position;
    //}

}
