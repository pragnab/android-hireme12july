package com.hiremehealthcare.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.hiremehealthcare.CommonCls.CustomTextView;
import com.hiremehealthcare.POJO.ApplicantCertificate;
import com.hiremehealthcare.R;

import java.util.List;

/**
 * Created by ADMIN on 30-04-2018.
 */

public class Profile_CertificationListAdpter extends BaseAdapter {
    private Context mContext;
    //    List<GridRecord> gridData;
    List<String> gridData;
    //    DataAdapter mDbHelper;
    String profileId;
    View view;
    ViewHolder holder = null;
    private LayoutInflater inflater;
    ApplicantCertificate applicantCertificate;
    OnDeleteListener onDeleteListener;
    boolean isActionshow;
    public interface OnDeleteListener{
        public void onDeleteButtonClicked(int position);
    }

    // SearchHistoryAdapter(Context mContext,List<GridRecord> gridData)
    public Profile_CertificationListAdpter(Context mContext, ApplicantCertificate applicantCertificate,OnDeleteListener onDeleteListener,boolean isActionshow) {
        this.mContext = mContext;
        inflater = LayoutInflater.from(this.mContext);
        this.applicantCertificate=applicantCertificate;
        this.onDeleteListener=onDeleteListener;
        this.isActionshow=isActionshow;
//        this.gridData = gridData;
        //  mDbHelper = new DataAdapter(mContext);
//        this.profileId = profileId;
    }


    @Override
    public int getCount() {
//        return gridData.size();
        return applicantCertificate.getData().size();
    }

    private class ViewHolder {


    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {

        ApplicantCertificate.Data data = applicantCertificate.getData().get(position);
        view = convertView;
        //  LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (view == null) {
            view = inflater.inflate(R.layout.profile_certificate_adpter, null);
            holder = new ViewHolder();
            //  holder.itemCheckBox = (CheckBox) view.findViewById(R.id.itemCheckBox);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        //   holder.itemCheckBox.setTag(position); // This line is important.

        LinearLayout lintop = (LinearLayout) view.findViewById(R.id.lintop);
        CustomTextView tvLicenseCertificate = (CustomTextView) view.findViewById(R.id.tvLicenseCertificate);
        CustomTextView tvLicenseEarned = (CustomTextView) view.findViewById(R.id.tvLicenseFile);
        CustomTextView tvLicenseExpire = (CustomTextView) view.findViewById(R.id.tvLicenseExpire);
        CustomTextView tvActionTitle = (CustomTextView) view.findViewById(R.id.tvActionTitle);
        ImageView ivLicenseDelete = (ImageView) view.findViewById(R.id.ivLicenseDelete);
        ivLicenseDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onDeleteListener.onDeleteButtonClicked(position);
            }
        });
        if(isActionshow)
        {
            tvActionTitle.setVisibility(View.VISIBLE);
            ivLicenseDelete.setVisibility(View.VISIBLE);
        }
        else
        {
            tvActionTitle.setVisibility(View.GONE);
            ivLicenseDelete.setVisibility(View.GONE);
        }
        tvLicenseCertificate.setText(data.getCertificate());
        tvLicenseEarned.setText(data.getEarned_month()+"/"+data.getEarned_year());
        tvLicenseExpire.setText(data.getExpires_label());
        //CustomTextView tv_action = (CustomTextView) view.findViewById(R.id.tv_action);
        //tv_action.setTypeface(Validations.setTypeface(mContext));
        if (position == 0) {
            lintop.setVisibility(View.VISIBLE);
        } else {
            lintop.setVisibility(View.GONE);
        }
        return view;
    }
}

