package com.hiremehealthcare.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.hiremehealthcare.CommonCls.CustomTextView;
import com.hiremehealthcare.POJO.ApplicantLicenseModel;
import com.hiremehealthcare.R;

import java.util.List;

/**
 * Created by ADMIN on 30-04-2018.
 */

public class Profile_licenseListAdpter extends BaseAdapter {
    private Context mContext;
    //    List<GridRecord> gridData;
    List<String> gridData;
    //    DataAdapter mDbHelper;
    String profileId;
    View view;
    ViewHolder holder = null;
    private LayoutInflater inflater;
    List<ApplicantLicenseModel.Datum> applicantLicenseModelList;
    OnActionDeleteClickedListener onActionDeleteClickedListener;
    boolean isActionShow;
    String TAG="Profile_licenseListAdpter";
    public interface OnActionDeleteClickedListener
    {
        public void onDeleteButtonClicked(ApplicantLicenseModel.Datum data);
    }

    // SearchHistoryAdapter(Context mContext,List<GridRecord> gridData)
    public Profile_licenseListAdpter(Context mContext, List<ApplicantLicenseModel.Datum> applicantLicenseModelList,OnActionDeleteClickedListener onActionDeleteClickedListener,boolean isActionShow) {
        this.mContext = mContext;
        this.onActionDeleteClickedListener=onActionDeleteClickedListener;
        this.applicantLicenseModelList=applicantLicenseModelList;
        inflater = LayoutInflater.from(this.mContext);
        this.isActionShow=isActionShow;
//        this.gridData = gridData;
        //  mDbHelper = new DataAdapter(mContext);
//        this.profileId = profileId;
    }


    @Override
    public int getCount() {
//        return gridData.size();
        return applicantLicenseModelList.size();
    }

    private class ViewHolder {
        public CustomTextView tv_action;
        public CustomTextView tvLicenseNumber;
        public CustomTextView tvLicenseFile;
        public CustomTextView tvLicenseExpire;
        public CustomTextView tvActionTitle;
        public ImageView ivLicenseDelete,ivShowFile;


    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup viewGroup) {


        view = convertView;
        //  LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (view == null) {
            view = inflater.inflate(R.layout.profile_license_adpter, null);
            holder = new ViewHolder();
            holder.tv_action = (CustomTextView) view.findViewById(R.id.tv_action);
            holder.tvLicenseNumber = (CustomTextView) view.findViewById(R.id.tvLicenseNumber);
            holder.tvActionTitle = (CustomTextView) view.findViewById(R.id.tvActionTitle);
            holder.tvLicenseFile = (CustomTextView) view.findViewById(R.id.tvLicenseFile);
            holder.tvLicenseFile.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                }
            });
            holder.tvLicenseExpire = (CustomTextView) view.findViewById(R.id.tvLicenseExpire);
            holder.ivLicenseDelete = view.findViewById(R.id.ivLicenseDelete);
            holder.ivShowFile = view.findViewById(R.id.ivShowFile);
            //  holder.itemCheckBox = (CheckBox) view.findViewById(R.id.itemCheckBox);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        //   holder.itemCheckBox.setTag(position); // This line is important.
        final ApplicantLicenseModel.Datum data = applicantLicenseModelList.get(position);
        LinearLayout lintop = (LinearLayout) view.findViewById(R.id.lintop);
        holder.tvLicenseNumber.setText(data.getApplicantLicenseNumber());
        holder.tvLicenseFile.setText(data.getApplicantLicenseProof());
        holder.tvLicenseExpire.setText(data.getExpiresLabel());
        holder.ivLicenseDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onActionDeleteClickedListener.onDeleteButtonClicked(data);
            }
        });
        holder.ivShowFile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showImageInDialog(data.getApplicantLicenseProof());
            }
        });
        if(isActionShow)
        {
            holder.tvActionTitle.setVisibility(View.VISIBLE);
            holder.ivLicenseDelete.setVisibility(View.VISIBLE);
        }
        else {
            holder.tvActionTitle.setVisibility(View.GONE);
            holder.ivLicenseDelete.setVisibility(View.GONE);
        }
        // tv_action.setTypeface(Validations.setTypeface(mContext));
        if (position == 0) {
            lintop.setVisibility(View.VISIBLE);
        } else {
            lintop.setVisibility(View.GONE);
        }
        return view;
    }

    public void showImageInDialog(String image_path)
    {
        LayoutInflater inflater = LayoutInflater.from(mContext);
        final View dialogView = inflater.inflate(R.layout.image_show_dilog_layout, null);
        ImageView ivCloseDialog = (ImageView) dialogView.findViewById(R.id.ivCloseDialog);
        ImageView ivLicensePhoto = (ImageView) dialogView.findViewById(R.id.ivLicensePhoto);
        final AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(mContext, R.style.myDialog));
        final AlertDialog b = builder.create();
        //  builder.setTitle("Material Style Dialog");
        builder.setCancelable(true);
        builder.setView(dialogView);
        b.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        Glide.with(mContext)
                .load(image_path)
                .into(ivLicensePhoto);
        Log.d(TAG, "showImageInDialog:image_path:- "+image_path);
        final AlertDialog show = builder.show();
        ivCloseDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                show.dismiss();
            }
        });
    }
}

