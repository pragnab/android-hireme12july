package com.hiremehealthcare.adapter;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import com.daimajia.swipe.SwipeLayout;
import com.hiremehealthcare.CommonCls.CustomButton;
import com.hiremehealthcare.CommonCls.CustomTextView;
import com.hiremehealthcare.CommonCls.ExpandableHeightGridView;
import com.hiremehealthcare.POJO.ExpirePositionModel;
import com.hiremehealthcare.POJO.Hours;
import com.hiremehealthcare.POJO.Shift;
import com.hiremehealthcare.activity.Qualified_CandidatesActivity;
import com.hiremehealthcare.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by nareshp on 5/3/2018.
 */

public class ExpirePositionAdapter extends RecyclerView.Adapter<ExpirePositionAdapter.MyViewHolder> {
    private List<ExpirePositionModel.Datum> expirePositionModelList;
    Context context;
    List<Shift.ShiftBean> shiftBeanList;
    List<Hours.HoursBean> hoursBeanList;
    OnEditOrViewButtonListener onEditOrViewButtonListener;

    public interface OnEditOrViewButtonListener {

        public void onEditButtonClicked(ExpirePositionModel.Datum data);

        public void onViewButtonClicked(ExpirePositionModel.Datum data);

    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public CustomTextView tvExpirePositionExpire, tvExpirePositionLicense, tvExpirePositionSpecility;
        SwipeLayout slManagerDashBord;
        LinearLayout left_view;
        LinearLayout right_view;
        CustomButton btn_candidates, btnViewPosition, btnEditPosition;
        ExpandableHeightGridView gridShift, gridHour;

        public MyViewHolder(View view) {
            super(view);
            slManagerDashBord = (SwipeLayout) view.findViewById(R.id.slManagerDashBord);
            left_view = (LinearLayout) view.findViewById(R.id.left_view);
            right_view = (LinearLayout) view.findViewById(R.id.right_view);
            gridShift = (ExpandableHeightGridView) view.findViewById(R.id.gridShift);
            gridHour = (ExpandableHeightGridView) view.findViewById(R.id.gridHour);
            tvExpirePositionExpire = (CustomTextView) view.findViewById(R.id.tvExpirePositionExpire);
            tvExpirePositionLicense = (CustomTextView) view.findViewById(R.id.tvExpirePositionLicense);
            tvExpirePositionSpecility = (CustomTextView) view.findViewById(R.id.tvExpirePositionSpecility);
            btn_candidates = (CustomButton) view.findViewById(R.id.btn_candidates);
            btnViewPosition = (CustomButton) view.findViewById(R.id.btnViewPosition);
            btnEditPosition = (CustomButton) view.findViewById(R.id.btnEditPosition);

        }
    }


    public ExpirePositionAdapter(List<ExpirePositionModel.Datum> dashbordManagerModelList, Context context, List<Shift.ShiftBean> shiftBeanList, List<Hours.HoursBean> hoursBeanList, OnEditOrViewButtonListener onEditOrViewButtonListener) {
        this.expirePositionModelList = dashbordManagerModelList;
        this.context = context;
        this.shiftBeanList = shiftBeanList;
        this.hoursBeanList = hoursBeanList;
        this.onEditOrViewButtonListener = onEditOrViewButtonListener;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.expired_position_row_inflater, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, final int position) {

        final ExpirePositionModel.Datum data = expirePositionModelList.get(position);
        holder.slManagerDashBord.setShowMode(SwipeLayout.ShowMode.LayDown);
        holder.slManagerDashBord.addDrag(SwipeLayout.DragEdge.Left, holder.left_view);
        holder.slManagerDashBord.addDrag(SwipeLayout.DragEdge.Right, holder.right_view);
        holder.tvExpirePositionLicense.setText(data.getLicenseName() + "");
        holder.tvExpirePositionSpecility.setText(data.getSpecialtyName() + "");

          /*25 may pragna*/
        holder.btn_candidates.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                get_qualified_candidates_for_position(expirePositionModelList.get(position).getPositionId() + "");
            }
        });

        holder.btnViewPosition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onEditOrViewButtonListener.onViewButtonClicked(data);
            }
        });

        holder.btnEditPosition.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onEditOrViewButtonListener.onEditButtonClicked(data);
            }
        });

        holder.gridHour.setExpanded(true);
        holder.gridShift.setExpanded(true);
        List<String> shiftList = new ArrayList<>();
        List<String> hourList = new ArrayList<>();
        int shift_counter = 0;
        int shift_selected = 0;
        for (Shift.ShiftBean shiftBean : shiftBeanList) {
            shiftList.add(shiftBean.getShift_name() + "");
            String s = shiftBean.getShift_name() + "";
            if (s.equals(data.getShiftName() + "")) {
                shift_selected = shift_counter;
            }

            shift_counter++;
        }
        int hour_counter = 0;
        int hour_selected = 0;

        for (Hours.HoursBean hoursBean : hoursBeanList) {
            hourList.add(hoursBean.getHours_name() + "");
            String s = data.getHourName() + "";
//            if(data.getHourName().equals(hoursBean.getHours_name()+""))
            if (s.equals(hoursBean.getHours_name() + ""))
                hour_selected = hour_counter;
            hour_counter++;
        }
        TextViewGridAdapter textViewGridAdapterForShift = new TextViewGridAdapter(shiftList, context, shift_selected);
        holder.gridShift.setAdapter(textViewGridAdapterForShift);
        TextViewGridAdapter textViewGridAdapterForHour = new TextViewGridAdapter(hourList, context, hour_selected);
        holder.gridHour.setAdapter(textViewGridAdapterForHour);
    }

    @Override
    public int getItemCount() {
        return expirePositionModelList.size();
    }

    @Override
    public int getItemViewType(int position) {

        return position;
        //return super.getItemViewType(position);
    }

    private void get_qualified_candidates_for_position(String PosID) {
        Intent i = new Intent(context, Qualified_CandidatesActivity.class);
        i.putExtra("ID", PosID + "");
        context.startActivity(i);
    }
}
