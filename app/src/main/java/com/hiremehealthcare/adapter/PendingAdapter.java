package com.hiremehealthcare.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.daimajia.swipe.SwipeLayout;
import com.daimajia.swipe.adapters.BaseSwipeAdapter;
import com.hiremehealthcare.CommonCls.AnimationFactory;
import com.hiremehealthcare.CommonCls.CustomBoldTextView;
import com.hiremehealthcare.CommonCls.CustomTextView;
import com.hiremehealthcare.CommonCls.ExpandableHeightGridView;
import com.hiremehealthcare.CommonCls.Validations;
import com.hiremehealthcare.POJO.ApplicantLicenseModel;
import com.hiremehealthcare.POJO.Benefits;
import com.hiremehealthcare.POJO.PandingPositionModel;
import com.hiremehealthcare.R;
import com.hiremehealthcare.fragment.PostNewPostion;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

/**
 * Created by ADMIN on 30-04-2018.
 */

public class PendingAdapter extends BaseSwipeAdapter {
    private Context mContext;
    String profileId;
    List<PandingPositionModel.Datum> dataList;
    // SearchHistoryAdapter(Context mContext,List<GridRecord> gridData)
    OnPandingAdapterRemoveOrFavoriteButtonClicked onPandingAdapterRemoveOrFavoriteButtonClicked;
    ExpandableHeightGridView griddays, grideveningdaystime, gridnightdaystime;
    private ExpandableHeightGridView griddaystime;
    private ExpandableHeightGridView mNightDaysTimeGrid;
    private ExpandableHeightGridView mEveningDaysTimeGrid;
    Button btn_availability;
    View view;
    ViewGroup _parent;
    View v_v;
    public ViewFlipper row_item_flipper;
    boolean FLAG=true;
  Days_adapter _daDays_checkBoxAdapter;
//    Night_CheckBoxAdapter _daNightcheckBoxAdapter;
//    Evening_CheckBoxAdapter _daevening_checkBoxAdapter;

     Night_adapter _daNightcheckBoxAdapter;
       Evening_adapter _daevening_checkBoxAdapter;
    public interface OnPandingAdapterRemoveOrFavoriteButtonClicked {
        public void onFavoriteButtonClicked(PandingPositionModel.Datum data);

        public void onRemoveButtonClicked(PandingPositionModel.Datum data);

        public void onListItemClicked(PandingPositionModel.Datum data);
    }

    public PendingAdapter(Context mContext, List<PandingPositionModel.Datum> datumList, OnPandingAdapterRemoveOrFavoriteButtonClicked onPandingAdapterRemoveOrFavoriteButtonClicked) {
        this.mContext = mContext;
        this.dataList = datumList;
        this.onPandingAdapterRemoveOrFavoriteButtonClicked = onPandingAdapterRemoveOrFavoriteButtonClicked;
//        this.gridData = gridData;
        //  mDbHelper = new DataAdapter(mContext);
//        this.profileId = profileId;
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    @Override
    public View generateView(int position, ViewGroup parent) {
//        return LayoutInflater.from(mContext).inflate(R.layout.pending_adpter, null);
        _parent = parent;
        return LayoutInflater.from(mContext).inflate(R.layout.pending_adpter_, null);
    }

    @Override
    public void fillValues(final int position, View convertView) {
        final PandingPositionModel.Datum data = dataList.get(position);
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onPandingAdapterRemoveOrFavoriteButtonClicked.onListItemClicked(data);
            }
        });
        CustomBoldTextView tvPendingFacilityName = (CustomBoldTextView) convertView.findViewById(R.id.tvPendingFacilityName);
        TextView tvPendingDays = (TextView) convertView.findViewById(R.id.tvPendingDays);
        tvPendingFacilityName.setText(data.getFacilityName() + "");
        CustomTextView tvPendingFacilityTime = (CustomTextView) convertView.findViewById(R.id.tvPendingFacilityTime);
        tvPendingFacilityTime.setText(data.getHourName() + "");
        CustomTextView tvPendingFacilityShift = (CustomTextView) convertView.findViewById(R.id.tvPendingFacilityShift);
        tvPendingFacilityShift.setText(data.getShiftName() + "");
        CustomTextView tvPendingFacilitySpeciality = (CustomTextView) convertView.findViewById(R.id.tvPendingFacilitySpeciality);
        CustomTextView tvPendingFacilityLicense = (CustomTextView) convertView.findViewById(R.id.tvPendingFacilityLicense);

        griddays = (ExpandableHeightGridView) convertView.findViewById(R.id.grid_days);
        gridnightdaystime = (ExpandableHeightGridView) convertView.findViewById(R.id.grid_night_days_time);
        grideveningdaystime = (ExpandableHeightGridView) convertView.findViewById(R.id.grid_evening_days_time);
        btn_availability = (Button) convertView.findViewById(R.id.btn_availability);
        gridnightdaystime.setExpanded(true);
        grideveningdaystime.setExpanded(true);
        griddays = (ExpandableHeightGridView) convertView.findViewById(R.id.grid_days);
        griddaystime = (ExpandableHeightGridView) convertView.findViewById(R.id.grid_days_time);
        view = (View) convertView.findViewById(R.id.test);
        row_item_flipper = (ViewFlipper) convertView.findViewById(R.id.row_item_flipper);

       // view.setVisibility(View.GONE);
        btn_availability.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                System.out.println("this is click event of availability::::::::::::::");
                v_v = LayoutInflater.from(_parent.getContext()).inflate(R.layout.testforflip_availability_bk, _parent, false);
                row_item_flipper.removeView(v_v);
                AnimationFactory.flipTransition(row_item_flipper,
                        AnimationFactory.FlipDirection.LEFT_RIGHT);
//                    if(v_v.getParent() != null) {
//                        ((ViewGroup)v_v.getParent()).removeView(v_v); // <- fix
//                    }
//                    else{
//                        System.out.println("THIS IS ELSE CONDTION AAAAAAAAAAAAAAAAAAAAAAAAAAAAA ");
//                    }
                row_item_flipper.addView(v_v);
            }
        });

        griddaystime.setExpanded(true);
        mEveningDaysTimeGrid = (ExpandableHeightGridView) convertView.findViewById(R.id.grid_evening_days_time);
        mNightDaysTimeGrid = (ExpandableHeightGridView) convertView.findViewById(R.id.grid_night_days_time);
        //    tvPendingFacilityLicense.setText(data.getLicenseName());
        String s1 = data.getLicenseName() + "";
        s1 = s1.replace("null", "N/A").replace(",", " , ");
        tvPendingFacilityLicense.setText(s1 + "");
        // tvPendingFacilitySpeciality.setText(data.getSpecialtyName());

        String certificateName = data.getCertificateName() + "";
        certificateName = certificateName.replace("null", "");
        certificateName = certificateName.replace("NULL", "");


        String s2 = data.getSpecialtyName() + "";
        s2 = s2.replace("null", "N/A").replace(",", " , ");
        tvPendingFacilitySpeciality.setText(s2 + "");

        CustomTextView tvPendingFacilityCertificate = (CustomTextView) convertView.findViewById(R.id.tvPendingFacilityCertificate);
        // tvPendingFacilityCertificate.setText(data.getCertificateName()+"");
        tvPendingFacilityCertificate.setText(certificateName + "");

        /*availability */
       /* String availability = "";
        if (data.getMon().contains("1")) {
            availability = availability + " , " + data.getMon();

        }
        if (data.getTue().contains("1")) {
            availability = availability + " , " + data.getTue();

        }
        if (data.getWed().contains("1")) {
            availability = availability + " , " + data.getWed();

        }
        if (data.getThu().contains("1")) {
            availability = availability + " , " + data.getThu();

        }
        if (data.getFri().contains("1")) {
            availability = availability + " , " + data.getFri();

        }
        if (data.getSat().contains("1")) {
            availability = availability + " , " + data.getSat();

        }
        if (data.getSun().contains("1")) {
            availability = availability + " , " + data.getSun();

        }
        if (availability == null || availability.contentEquals("")) {
            availability = "N/A";
        }


        tvPendingDays.setText(availability+"");*/

        String availability = "";
        if (data.getMon().contains("1")) {
            //availability = availability + " , " + nearPositionDetailModel.getData().getMon();
            availability = availability + " , " + "Mon";

        }
        if (data.getTue().contains("1")) {
            // availability = availability + " , " + nearPositionDetailModel.getData().getTue();
            availability = availability + " , " + "Tue";

        }
        if (data.getWed().contains("1")) {
            // availability = availability + " , " + nearPositionDetailModel.getData().getWed();
            availability = availability + " , " + "Wed";

        }
        if (data.getThu().contains("1")) {
            // availability = availability + " , " + nearPositionDetailModel.getData().getThu();
            availability = availability + " , " + "Thu";

        }
        if (data.getFri().contains("1")) {
            //     availability = availability + " , " + nearPositionDetailModel.getData().getFri();
            availability = availability + " , " + "Fri";

        }
        if (data.getSat().contains("1")) {
            //  availability = availability + " , " + nearPositionDetailModel.getData().getSat();
            availability = availability + " , " + "Sat";

        }
        if (data.getSun().contains("1")) {
            // availability = availability + " , " + nearPositionDetailModel.getData().getSun();
            availability = availability + " , " + "Sun";

        }
        if (availability == null || availability.contentEquals("")) {
            availability = "N/A";
        }
        availability = availability.replaceFirst(",", "");
        tvPendingDays.setText(availability + "");






        availability(data,convertView);







        LinearLayout bottom_wrapper_2 = (LinearLayout) convertView.findViewById(R.id.bottom_wrapper_2);
        bottom_wrapper_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onPandingAdapterRemoveOrFavoriteButtonClicked.onRemoveButtonClicked(data);
            }
        });
        ImageView ivPendingHeartButton = (ImageView) convertView.findViewById(R.id.ivPendingHeartButton);
        ivPendingHeartButton.setVisibility(View.GONE);
        ivPendingHeartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onPandingAdapterRemoveOrFavoriteButtonClicked.onFavoriteButtonClicked(data);
            }
        });

        //     TextView txt_num = (TextView) convertView.findViewById(R.id.txt_num);
//        SwipeLayout swipe = (SwipeLayout) convertView.findViewById(R.id.swipe);
//        final TextView tv_name = (TextView) convertView.findViewById(R.id.txt_num);
//        TextView txt_manage = (TextView) convertView.findViewById(R.id.txt_manage);

        // tv_name.setText(mContext.getString(R.string.searchHistory)+" "+position+"");
//        tv_name.setText(gridData.get(position) + "");
//        tv_name.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//             //   search.setQuery(tv_name.getText().toString().trim(), false);
//            }
//        });
//
//        txt_manage.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                System.out.println(position + " on delete clcik ");
////                mDbHelper.open();
////                mDbHelper.deleteSearchHistoryData(gridData.get(position), profileId);
////                mDbHelper.close();
////                getHistoryData();
//
//            }
//        });
    }

    private void availability( PandingPositionModel.Datum data,View convertView) {
        ArrayList<String> days = new ArrayList<>();
     //   getavailability(data.getPositionId() + "");
        days = new ArrayList<>();
        days.add("Mon");
        Days_adapter.selected=new ArrayList<>();
        Days_adapter.selected_name=new ArrayList<>();

        Night_adapter.selected=new ArrayList<>();
        Night_adapter.selected_name=new ArrayList<>();


        Evening_adapter.selected=new ArrayList<>();
        Evening_adapter.selected_name=new ArrayList<>();

        griddays = (ExpandableHeightGridView) convertView.findViewById(R.id.grid_days);
        gridnightdaystime = (ExpandableHeightGridView) convertView.findViewById(R.id.grid_night_days_time);
        grideveningdaystime = (ExpandableHeightGridView) convertView.findViewById(R.id.grid_evening_days_time);



        days.add("Tue");
        days.add("Wed");
        days.add("Thu");
        days.add("Fri");
        days.add("Sat");
        days.add("Sun");
        System.out.println("availability MON : " + data.getMon() + " TUE : " + data.getTue() + " WED " + data.getWed() + " THR " + data.getThu() + " FRI " + data.getFri() + "");
        //   List<String> items = Arrays.asList(data.getMon().split("\\s*,\\s*"));

//PostNewPostion.MON.addAll(Arrays.asList(data.getMon().toLowerCase().split("\\s+")));
        List<String> items = new ArrayList<>();
//                    items.addAll(Arrays.asList(data.getMon().toLowerCase().split("\\s+")));
        items=    Arrays.asList(data.getMon().split("\\s*,\\s*"));
        //  PostNewPostion.MON = new ArrayList<Integer>(items.size());
        PostNewPostion.MON = new ArrayList<>();
        List<Integer> items_int = new ArrayList<>();
        for (int i = 0; i < items.size(); i++) {
            if(  isNumeric(items.get(i) + "")) {
                items_int.add(Integer.parseInt(items.get(i) + ""));
                if (items_int.get(i).equals(1)) {
                    Days_adapter.selected.add("1");
                    Days_adapter.selected_name.add("Mon");
                }
                if (items_int.get(i).equals(2)) {
                    Evening_adapter.selected.add("1");
                    Evening_adapter.selected_name.add("Mon");
                }
                if (items_int.get(i).equals(3)) {
                    Night_adapter.selected.add("1");
                    Night_adapter.selected_name.add("Mon");
                }
            }
        }
        //  List<String> items_tue = new ArrayList<>();
//                    items_tue=(Arrays.asList(data.getTue().split("\\s+")));
        List<String> items_tue = Arrays.asList(data.getTue().split("\\s*,\\s*"));

        //  PostNewPostion.MON = new ArrayList<Integer>(items.size());
        PostNewPostion.TUE = new ArrayList<>();
        List<Integer> items_int_tue = new ArrayList<>();
    //    System.out.println("TUE ```````````@ "+items_tue.toString()+"");
        for (int i = 0; i < items_tue.size(); i++) {
            System.out.println("items_tue.get(i) ````@@@@@@@@@@ "+ items_tue.get(i) + "");
            if(  isNumeric(items_tue.get(i) + "")) {
                items_int_tue.add(Integer.parseInt(items_tue.get(i) + ""));
                if (items_int_tue.get(i).equals(1)) {
                    Days_adapter.selected.add("2");
                    Days_adapter.selected_name.add("Tue");
                }
                if (items_int_tue.get(i).equals(2)) {
                    Evening_adapter.selected.add("2");
                    Evening_adapter.selected_name.add("Tue");
                }
                if (items_int_tue.get(i).equals(3)) {
                    Night_adapter.selected.add("2");
                    Night_adapter.selected_name.add("Tue");
                }
            }
        }


        List<String> items_wed = Arrays.asList(data.getWed().split("\\s*,\\s*"));

        //  PostNewPostion.MON = new ArrayList<Integer>(items.size());
        PostNewPostion.WED = new ArrayList<>();
        List<Integer> items_int_wed = new ArrayList<>();
     //   System.out.println("WED ```````````@ "+items_wed.toString()+"");
        for (int i = 0; i < items_wed.size(); i++) {
//                        System.out.println("items_tue.get(i) ````@@@@@@@@@@ "+ items_tue.get(i) + "");
            if(  isNumeric(items_wed.get(i) + "")) {
                items_int_wed.add(Integer.parseInt(items_wed.get(i) + ""));
                if (items_int_wed.get(i).equals(1)) {
                    Days_adapter.selected.add("3");
                    Days_adapter.selected_name.add("Wed");
                }
                if (items_int_wed.get(i).equals(2)) {
                    Evening_adapter.selected.add("3");
                    Evening_adapter.selected_name.add("Wed");
                }
                if (items_int_wed.get(i).equals(3)) {
                    Night_adapter.selected.add("3");
                    Night_adapter.selected_name.add("Wed");
                }
            }
        }


        List<String> items_Thr = Arrays.asList(data.getThu().split("\\s*,\\s*"));

        //  PostNewPostion.MON = new ArrayList<Integer>(items.size());
        PostNewPostion.THR = new ArrayList<>();
        List<Integer> items_int_thr = new ArrayList<>();
      //  System.out.println("THRU ```````````@ "+items_Thr.toString()+"");
        for (int i = 0; i < items_Thr.size(); i++) {
//                        System.out.println("items_tue.get(i) ````@@@@@@@@@@ "+ items_tue.get(i) + "");
            if(  isNumeric(items_Thr.get(i) + "")) {
                items_int_thr.add(Integer.parseInt(items_Thr.get(i) + ""));
                if (items_int_thr.get(i).equals(1)) {
                    Days_adapter.selected.add("4");
                    Days_adapter.selected_name.add("Thr");
                }
                if (items_int_thr.get(i).equals(2)) {
                    Evening_adapter.selected.add("4");
                    Evening_adapter.selected_name.add("Thr");
                }
                if (items_int_thr.get(i).equals(3)) {
                    Night_adapter.selected.add("4");
                    Night_adapter.selected_name.add("Thr");
                }
            }
        }


        List<String> items_Fri = Arrays.asList(data.getFri().split("\\s*,\\s*"));

        //  PostNewPostion.MON = new ArrayList<Integer>(items.size());
        PostNewPostion.FRI = new ArrayList<>();
        List<Integer> items_int_fri = new ArrayList<>();
     //   System.out.println("FRID ```````````@ "+items_Fri.toString()+"");
        for (int i = 0; i < items_Fri.size(); i++) {
//                        System.out.println("items_tue.get(i) ````@@@@@@@@@@ "+ items_tue.get(i) + "");
            if(  isNumeric(items_Fri.get(i) + "")) {
                items_int_fri.add(Integer.parseInt(items_Fri.get(i) + ""));
                if (items_int_fri.get(i).equals(1)) {
                    Days_adapter.selected.add("5");
                    Days_adapter.selected_name.add("Fri");
                }
                if (items_int_fri.get(i).equals(2)) {
                    Evening_adapter.selected.add("5");
                    Evening_adapter.selected_name.add("Fri");
                }
                if (items_int_fri.get(i).equals(3)) {
                    Night_adapter.selected.add("5");
                    Night_adapter.selected_name.add("Fri");
                }
            }
        }


        List<String> items_Sat = Arrays.asList(data.getSat().split("\\s*,\\s*"));

        //  PostNewPostion.MON = new ArrayList<Integer>(items.size());
        PostNewPostion.FRI = new ArrayList<>();
        List<Integer> items_int_sat = new ArrayList<>();
     //   System.out.println("SAT ```````````@ "+items_Sat.toString()+"");
        for (int i = 0; i < items_Sat.size(); i++) {
//                        System.out.println("items_tue.get(i) ````@@@@@@@@@@ "+ items_tue.get(i) + "");
            if(  isNumeric(items_Sat.get(i) + "")) {
                items_int_sat.add(Integer.parseInt(items_Sat.get(i) + ""));
                if (items_int_sat.get(i).equals(1)) {
                    Days_adapter.selected.add("6");
                    Days_adapter.selected_name.add("Sat");
                }
                if (items_int_sat.get(i).equals(2)) {
                    Evening_adapter.selected.add("6");
                    Evening_adapter.selected_name.add("Sat");
                }
                if (items_int_sat.get(i).equals(3)) {
                    Night_adapter.selected.add("6");
                    Night_adapter.selected_name.add("Sat");
                }
            }
        }


        List<String> items_Sun = Arrays.asList(data.getSun().split("\\s*,\\s*"));

        //  PostNewPostion.MON = new ArrayList<Integer>(items.size());
        PostNewPostion.FRI = new ArrayList<>();
        List<Integer> items_int_sun = new ArrayList<>();
     //   System.out.println("SUN ```````````@ "+items_Sun.toString()+"");
        for (int i = 0; i < items_Sun.size(); i++) {
//                        System.out.println("items_tue.get(i) ````@@@@@@@@@@ "+ items_tue.get(i) + "");
            if(  isNumeric(items_Sun.get(i) + "")) {
                items_int_sun.add(Integer.parseInt(items_Sun.get(i) + ""));
                if (items_int_sun.get(i).equals(1)) {
                    Days_adapter.selected.add("7");
                    Days_adapter.selected_name.add("Sun");
                }
                if (items_int_sun.get(i).equals(2)) {
                    Evening_adapter.selected.add("7");
                    Evening_adapter.selected_name.add("Sun");
                }
                if (items_int_sun.get(i).equals(3)) {
                    Night_adapter.selected.add("7");
                    Night_adapter.selected_name.add("Sun");
                }
            }
        }



        PostNewPostion.MON.addAll(items_int);
        PostNewPostion.TUE.addAll(items_int_tue);
        PostNewPostion.WED.addAll(items_int_wed);
        PostNewPostion.THR.addAll(items_int_thr);
        PostNewPostion.FRI.addAll(items_int_fri);
        PostNewPostion.SAT.addAll(items_int_sat);
        PostNewPostion.SUN.addAll(items_int_sun);

//        System.out.println("this is monday " + PostNewPostion.MON.toString() + "");
//        System.out.println("this is Tuesday " + PostNewPostion.TUE.toString() + "");
//        System.out.println("this is TuesdayDays_adapter.selected_name``````````@ " + Days_adapter.selected_name .toString()+ "");
//        System.out.println("this is TuesdayEvening_adapter.selected_name``````````@ " + Evening_adapter.selected_name.toString() + "");
        //    v_v = LayoutInflater.from(_parent.getContext()).inflate(R.layout.testforflip, _parent, false);
    //    v_v = LayoutInflater.from(_parent.getContext()).inflate(R.layout.testforflip_availability, _parent, false);
      //  holder.row_item_flipper.removeView(v_v);
       // AnimationFactory.flipTransition(holder.row_item_flipper,
              //  AnimationFactory.FlipDirection.LEFT_RIGHT);




       // holder.row_item_flipper.addView(v_v);
       // initView(v_v);
        DaysMultiSelectAdapter_duplicate_flip _dayDaysMultiSelectAdapter;
        _dayDaysMultiSelectAdapter = new DaysMultiSelectAdapter_duplicate_flip(days, mContext);
        griddays.setExpanded(true);
        griddays.setAdapter(_dayDaysMultiSelectAdapter);
        Benefits days_all = new Benefits();
        List<Benefits.BenefitsBean> all_day = new ArrayList<>();
        Benefits.BenefitsBean day = new Benefits.BenefitsBean();


        day.setBenefits_id("1");
        day.setBenefits_name("Mon");
        all_day.add(day);

        day = new Benefits.BenefitsBean();
        day.setBenefits_id("2");
        day.setBenefits_name("Tue");
        all_day.add(day);

        day = new Benefits.BenefitsBean();
        day.setBenefits_id("3");
        day.setBenefits_name("Wed");
        all_day.add(day);


        day = new Benefits.BenefitsBean();
        day.setBenefits_id("4");
        day.setBenefits_name("Thu");
        all_day.add(day);


        day = new Benefits.BenefitsBean();
        day.setBenefits_id("5");
        day.setBenefits_name("Fri");
        all_day.add(day);


        day = new Benefits.BenefitsBean();
        day.setBenefits_id("6");
        day.setBenefits_name("Sat");
        all_day.add(day);


        day = new Benefits.BenefitsBean();
        day.setBenefits_id("7");
        day.setBenefits_name("Sun");
        all_day.add(day);


        days_all.setBenefits(all_day);
        Benefits test = new Benefits();
/*
                           _daDays_checkBoxAdapter = new Days_CheckBoxAdapter(days_all, context, 1,test,true);
                    griddaystime.setAdapter(_daDays_checkBoxAdapter);*/


//        Days_adapter  _daDays_checkBoxAdapter = new Days_adapter(days_all, mContext, 1, test, true);
//        griddaystime.setAdapter(_daDays_checkBoxAdapter);
//
//        Evening_adapter   _daevening_checkBoxAdapter = new Evening_adapter(days_all, mContext, 2, true);
//        grideveningdaystime.setAdapter(_daevening_checkBoxAdapter);
//
//
//        Night_adapter   _daNightcheckBoxAdapter = new Night_adapter(days_all, mContext, 3, true);
//        gridnightdaystime.setAdapter(_daNightcheckBoxAdapter);

          _daDays_checkBoxAdapter = new Days_adapter(days_all, mContext, 1, test, FLAG);
        griddaystime.setAdapter(_daDays_checkBoxAdapter);

           _daevening_checkBoxAdapter = new Evening_adapter(days_all, mContext, 2, FLAG);
        grideveningdaystime.setAdapter(_daevening_checkBoxAdapter);


           _daNightcheckBoxAdapter = new Night_adapter(days_all, mContext, 3, FLAG);
        gridnightdaystime.setAdapter(_daNightcheckBoxAdapter);
    }

    @Override
    public int getCount() {
//        return gridData.size();
        return dataList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    private boolean isNumeric(String string) {
        if(Pattern.matches("\\d{1,3}((\\.\\d{1,2})?)", string))
            return true;
        else
            return false;
    }
}

