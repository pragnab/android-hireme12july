package com.hiremehealthcare.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.hiremehealthcare.CommonCls.CustomTextView;
import com.hiremehealthcare.CommonCls.Validations;
import com.hiremehealthcare.POJO.Certificates;
import com.hiremehealthcare.POJO.License;
import com.hiremehealthcare.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pragna on 30-4-2018.
 */

public class LicenseAdapter extends BaseAdapter {


    private Context ctx;
    // List<String> AllStatuslist = new ArrayList<>();
    License license = new License();
    ViewHolder holder = null;
    public FragmentManager f_manager;
    //private LayoutInflater inflater;
    private LayoutInflater inflater;
    SharedPreferences prefs;
    String customerId;
    View view;
    private int selectedPosition = -1;
    public ArrayList<String> selected = new ArrayList<String>();
    public ArrayList<String> selected_item = new ArrayList<String>();

    public LicenseAdapter(License license, Context ctx) {
        this.ctx = ctx;
        this.license = license;
        inflater = LayoutInflater.from(this.ctx);

    }

    @Override
    public int getCount() {
        return license.getLicense().size();
    }

    @Override
    public Object getItem(int position) {
        return license.getLicense().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public ArrayList<String> getSelected() {


        return selected;
    }

    public List<String> getSelectedItem() {
        return selected_item;
    }

    public void clearSelected() {
        selected.clear();
    }


    private class ViewHolder {

        CheckBox itemCheckBox;
        CustomTextView txt_status_show;
        CustomTextView txt_status_counter;
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        view = convertView;
        //  LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (view == null) {
            view = inflater.inflate(R.layout.specialityadpter, null);
            holder = new ViewHolder();
            holder.itemCheckBox = (CheckBox) view.findViewById(R.id.itemCheckBox);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.itemCheckBox.setTag(position); // This line is important.
        //   holder.itemCheckBox.setText(license.getLicense().get(position).getLicense_id()+"=> "+license.getLicense().get(position).getLicense_name() + "");
        holder.itemCheckBox.setText(license.getLicense().get(position).getLicense_name() + "");
        holder.itemCheckBox.setTypeface(Validations.setTypeface(ctx));
        //    holder.txt_status.setTypeface(Validations.setTypeface(ctx));
        if (position == selectedPosition) {
            holder.itemCheckBox.setChecked(true);
            // selected.add(license.getLicense().get(position).getLicense_id() + "");

        } else {
            holder.itemCheckBox.setChecked(false);
            //  selected.remove(license.getLicense().get(position).getLicense_id() + "");
        }
        holder.itemCheckBox.setChecked(selected.contains(license.getLicense().get(position).getLicense_id() + ""));
        // holder.itemCheckBox.setOnClickListener(onStateChangedListener(holder.itemCheckBox, position));
        holder.itemCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                int pos = (Integer) buttonView.getTag();
                if (buttonView.isShown()) {
                    if (!buttonView.isChecked()) {
                        selected.remove(license.getLicense().get(position).getLicense_id() + "");
                        selected_item.remove(license.getLicense().get(position).getLicense_name() + "");
                        System.out.println("Selected ITEMS REMOVE " + selected_item + "");
                        System.out.println("Selected REMOVE  " + selected + "");
                    } else if (buttonView.isChecked()) {
                        if (!selected.contains(license.getLicense().get(position).getLicense_id() + "")) {
                            selected.add(license.getLicense().get(position).getLicense_id() + "");
                            selected_item.add(license.getLicense().get(position).getLicense_name() + "");
                            System.out.println("Selected ITEMS ADD " + selected_item + "");
                            System.out.println("Selected ADD  " + selected + "");
                        }
                    }
                }


            }
        });
        return view;
    }


}