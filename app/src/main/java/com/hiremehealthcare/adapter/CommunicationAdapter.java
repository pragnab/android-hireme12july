package com.hiremehealthcare.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.daimajia.swipe.adapters.BaseSwipeAdapter;
import com.hiremehealthcare.CommonCls.CustomBoldTextView;
import com.hiremehealthcare.CommonCls.CustomTextView;
import com.hiremehealthcare.POJO.ChatManagerModel;
import com.hiremehealthcare.POJO.CommunicationModel;
import com.hiremehealthcare.POJO.PandingPositionModel;
import com.hiremehealthcare.R;
import com.hiremehealthcare.database.MySharedPrefereces;

import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by ADMIN on 30-04-2018.
 */

public class CommunicationAdapter extends BaseSwipeAdapter {
    private Context mContext;
    String profileId;
    List<CommunicationModel.Datum> communicationModelList;
    MySharedPrefereces mySharedPrefereces;
    // SearchHistoryAdapter(Context mContext,List<GridRecord> gridData)
    OnPandingAdapterRemoveOrFavoriteButtonClicked onPandingAdapterRemoveOrFavoriteButtonClicked;
    public interface OnPandingAdapterRemoveOrFavoriteButtonClicked
    {
        public void onContactCandidateButtonClicked(CommunicationModel.Datum data);
        public void onRemoveButtonClicked(CommunicationModel.Datum data);
        public void onListItemClicked(CommunicationModel.Datum communicationModel);
    }
    public CommunicationAdapter(Context mContext, List<CommunicationModel.Datum> communicationModelList, OnPandingAdapterRemoveOrFavoriteButtonClicked onPandingAdapterRemoveOrFavoriteButtonClicked) {
        this.mContext = mContext;
        mySharedPrefereces = new MySharedPrefereces(mContext);
        this.communicationModelList=communicationModelList;
        this.onPandingAdapterRemoveOrFavoriteButtonClicked=onPandingAdapterRemoveOrFavoriteButtonClicked;
//        this.gridData = gridData;
        //  mDbHelper = new DataAdapter(mContext);
//        this.profileId = profileId;
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    @Override
    public View generateView(int position, ViewGroup parent) {
        return LayoutInflater.from(mContext).inflate(R.layout.chat_manager_row_inflater, null);
    }

    @Override
    public void fillValues(final int position, View convertView) {
        final CommunicationModel.Datum data = communicationModelList.get(position);
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onPandingAdapterRemoveOrFavoriteButtonClicked.onListItemClicked(data);
            }
        });
        CustomBoldTextView tvCommunicationName = (CustomBoldTextView) convertView.findViewById(R.id.tvCommunicationName);
        CustomBoldTextView tvNumberUnreadMessage = (CustomBoldTextView) convertView.findViewById(R.id.tvNumberUnreadMessage);
        tvCommunicationName.setText(data.getFirstname()+" "+data.getLastname()+"");
        CircleImageView cvCommunicationImage = (CircleImageView) convertView.findViewById(R.id.cvCommunicationImage);
        CustomTextView tvCommunicationCertificate = (CustomTextView) convertView.findViewById(R.id.tvCommunicationCertificate);
        CustomTextView tvCommunicationCandidateDetail = (CustomTextView) convertView.findViewById(R.id.tvCommunicationCandidateDetail);
        CustomTextView tvCommunicationFacilityName = (CustomTextView) convertView.findViewById(R.id.tvCommunicationFacilityName);
        CustomTextView tvCommunicationSpecialityName = (CustomTextView) convertView.findViewById(R.id.tvCommunicationSpecialityName);
        LinearLayout llApplicantDataContainer=(LinearLayout)convertView.findViewById(R.id.llApplicantDataContainer);
        if(mySharedPrefereces.getUserType()==1)
        {
            tvCommunicationCandidateDetail.setText("Position Detail");
            llApplicantDataContainer.setVisibility(View.VISIBLE);
        //    tvCommunicationFacilityName.setText(data.getFacility_name());
            String ss=data.getFacility_name()+"";
            ss = ss.replace("null", "N/A").replace(",", " , ");
            tvCommunicationFacilityName.setText(ss+"");
//            tvCommunicationSpecialityName.setText(data.getSpecialty_name());

            String s1=data.getSpecialty_name()+"";
            s1 = s1.replace("null", "N/A").replace(",", " , ");
            tvCommunicationSpecialityName.setText(s1+"");


            tvCommunicationCertificate.setVisibility(View.GONE);
        }
        else
        {
            /*25 July Pragna*/
//            tvCommunicationCandidateDetail.setText("Candidate Details");
            tvCommunicationCandidateDetail.setText("Candidate Detail");
            llApplicantDataContainer.setVisibility(View.GONE);
            tvCommunicationCertificate.setVisibility(View.VISIBLE);
        }
        Glide.with(mContext)
                .load(data.getImage())
                .error(R.drawable.no_image)
                .into(cvCommunicationImage);
       // tvCommunicationCertificate.setText(data.getApplicant_license_name());
        String s1=data.getApplicant_license_name()+"";
        s1 = s1.replace("null", "N/A").replace(",", " , ");
        tvCommunicationCertificate.setText(s1+"");

        //tvCommunicationCertificate.setText(data.getApplicantCerificationCerificateName());
        tvCommunicationCandidateDetail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onPandingAdapterRemoveOrFavoriteButtonClicked.onContactCandidateButtonClicked(data);
            }
        });
        ImageView ivDeleteCommunication = (ImageView) convertView.findViewById(R.id.ivDeleteCommunication);
        ivDeleteCommunication.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onPandingAdapterRemoveOrFavoriteButtonClicked.onRemoveButtonClicked(data);
            }
        });
        if(data.getUnreadMessage()!=null&& !data.getUnreadMessage().equals("null") && !data.getUnreadMessage().equals(""))
        {
            tvNumberUnreadMessage.setText(data.getUnreadMessage());
            tvNumberUnreadMessage.setVisibility(View.VISIBLE);

        }
        else
        {
            tvNumberUnreadMessage.setVisibility(View.GONE);
        }
        /*CustomBoldTextView tvPendingFacilityName = (CustomBoldTextView) convertView.findViewById(R.id.tvPendingFacilityName);
        tvPendingFacilityName.setText(data.getFacilityName());
        CustomTextView tvPendingFacilityTime = (CustomTextView) convertView.findViewById(R.id.tvPendingFacilityTime);
        tvPendingFacilityTime.setText(data.getHourName());
        CustomTextView tvPendingFacilityShift = (CustomTextView) convertView.findViewById(R.id.tvPendingFacilityShift);
        tvPendingFacilityShift.setText(data.getShiftName());
        CustomTextView tvPendingFacilitySpeciality = (CustomTextView) convertView.findViewById(R.id.tvPendingFacilitySpeciality);
        tvPendingFacilitySpeciality.setText(data.getSpecialtyName());
        CustomTextView tvPendingFacilityCertificate = (CustomTextView) convertView.findViewById(R.id.tvPendingFacilityCertificate);
        tvPendingFacilityCertificate.setText(data.getCertificateName());
        LinearLayout bottom_wrapper_2 = (LinearLayout) convertView.findViewById(R.id.bottom_wrapper_2);
        bottom_wrapper_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onPandingAdapterRemoveOrFavoriteButtonClicked.onRemoveButtonClicked(data);
            }
        });
        ImageView ivPendingHeartButton = (ImageView) convertView.findViewById(R.id.ivPendingHeartButton);
        ivPendingHeartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onPandingAdapterRemoveOrFavoriteButtonClicked.onContactCandidateButtonClicked(data);
            }
        });*/

        //     TextView txt_num = (TextView) convertView.findViewById(R.id.txt_num);
//        SwipeLayout swipe = (SwipeLayout) convertView.findViewById(R.id.swipe);
//        final TextView tv_name = (TextView) convertView.findViewById(R.id.txt_num);
//        TextView txt_manage = (TextView) convertView.findViewById(R.id.txt_manage);

        // tv_name.setText(mContext.getString(R.string.searchHistory)+" "+position+"");
//        tv_name.setText(gridData.get(position) + "");
//        tv_name.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//             //   search.setQuery(tv_name.getText().toString().trim(), false);
//            }
//        });
//
//        txt_manage.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                System.out.println(position + " on delete clcik ");
////                mDbHelper.open();
////                mDbHelper.deleteSearchHistoryData(gridData.get(position), profileId);
////                mDbHelper.close();
////                getHistoryData();
//
//            }
//        });
    }

    @Override
    public int getCount() {
//        return gridData.size();
        return communicationModelList.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}

