package com.hiremehealthcare.adapter;

import android.content.Context;
import android.graphics.Movie;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.hiremehealthcare.CommonCls.CustomTextView;
import com.hiremehealthcare.POJO.ChatManagerModel;
import com.hiremehealthcare.POJO.ManagerChatDetailModel;
import com.hiremehealthcare.R;
import com.hiremehealthcare.database.MySharedPrefereces;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by nareshp on 5/23/2018.
 */

public class ManagerChatDetailAdapter extends RecyclerView.Adapter<ManagerChatDetailAdapter.MyViewHolder> {
    private List<ChatManagerModel.Message> managerChatDetailModelList;
    private static final int RIGHT_MSG = 0;
    private static final int LEFT_MSG = 1;
    MySharedPrefereces mySharedPrefereces;
    SimpleDateFormat simpleDateFormat;
    Context context;
    Date today;
    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView title, year, genre;
        public CustomTextView tvChatView;
        public CustomTextView tvTimeAndDateView;
        public CircleImageView ivUserLogo;

        public MyViewHolder(View view) {
            super(view);
            tvChatView = (CustomTextView) view.findViewById(R.id.tvChatView);
            tvTimeAndDateView = (CustomTextView) view.findViewById(R.id.tvTimeAndDateView);
            ivUserLogo = (CircleImageView) view.findViewById(R.id.ivUserLogo);
            today = new Date();
            simpleDateFormat=new SimpleDateFormat("yyyy-MM-dd kk:mm:ss");

            //title = (TextView) view.findViewById(R.id.title);
            //genre = (TextView) view.findViewById(R.id.genre);
            //year = (TextView) view.findViewById(R.id.year);
        }
    }


    public ManagerChatDetailAdapter(List<ChatManagerModel.Message> managerChatDetailModelList, Context context) {
        this.managerChatDetailModelList = managerChatDetailModelList;
        this.context=context;
        mySharedPrefereces = new MySharedPrefereces(context);

    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView;

        if (viewType == RIGHT_MSG) {
             itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.chat_message_right, parent, false);

            return new MyViewHolder(itemView);
        } else if (viewType == LEFT_MSG) {
             itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.chat_message_left, parent, false);

            return new MyViewHolder(itemView);
        }

        return null;
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {
        ChatManagerModel.Message managerChatDetailModel = managerChatDetailModelList.get(position);
        holder.tvChatView.setText(managerChatDetailModel.getChatingMsg());
        String todayString = simpleDateFormat.format(new Date());

        if(holder.ivUserLogo!=null)
        {
            Glide.with(context)
                    .load(managerChatDetailModel.getUserImage()+"")
                    .error(R.drawable.no_image)
                    .into(holder.ivUserLogo);
        }
        try {
            today = simpleDateFormat.parse(todayString);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        holder.tvTimeAndDateView.setText(managerChatDetailModel.getCreatedTime());
       /* try {
            Date date=simpleDateFormat.parse(managerChatDetailModel.getCreatedTime());
            //holder.tvTimeAndDateView.setText(getDifferentDate(today,date));


        } catch (ParseException e) {
            e.printStackTrace();
        }*/

       /* holder.title.setText(movie.getTitle());
        holder.genre.setText(movie.getGenre());
        holder.year.setText(movie.getYear());*/
    }
    public String getDifferentDate(Date current_date,Date messageDate)
    {
        long diff = current_date.getTime() - messageDate.getTime();
        long diffSeconds = diff / 1000 % 60;
        long diffMinutes = diff / (60 * 1000) % 60;
        long diffHours = diff / (60 * 60 * 1000);
        int diffInDays = (int) diff / (1000 * 60 * 60 * 24);
        if(diffInDays==0)
        {
            if(diffHours==0)
            {
                if(diffMinutes==0)
                {

                    return diffSeconds+" second ago";
                }
                else
                {
                    return diffMinutes+" minutes ago";
                }
            }
            else
            {
                return diffHours+" hours ago";
            }

        }
        else
        {
            return diffInDays+" days ago";
        }
    }

    @Override
    public int getItemViewType(int position) {
        //return super.getItemViewType(position);
        ChatManagerModel.Message managerChatDetailModel = managerChatDetailModelList.get(position);

        if(mySharedPrefereces.getUserType()==Integer.parseInt(managerChatDetailModel.getChatUser2User1Id()))
        {
            return RIGHT_MSG;
        }
        else
        {
            return LEFT_MSG;
        }
        /*if(mySharedPrefereces.getUserType()==2) {
            if (managerChatDetailModel.getUser2Id().equals(mySharedPrefereces.getUserId())) {
                return RIGHT_MSG;
            } else {
                return LEFT_MSG;
            }
        }else
        {
            if (managerChatDetailModel.getUser1Id().equals(mySharedPrefereces.getUserId())) {
                return RIGHT_MSG;
            } else {
                return LEFT_MSG;
            }
        }*/

    }

    @Override
    public int getItemCount() {
        return managerChatDetailModelList.size();
    }
}
