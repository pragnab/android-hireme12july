package com.hiremehealthcare.adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.ViewFlipper;

import com.android.volley.RequestQueue;
import com.hiremehealthcare.CommonCls.AnimationFactory;
import com.hiremehealthcare.CommonCls.CustomBoldTextView;
import com.hiremehealthcare.CommonCls.CustomTextView;
import com.hiremehealthcare.CommonCls.ExpandableHeightGridView;
import com.hiremehealthcare.POJO.ApplicantNearPositionModel;
import com.hiremehealthcare.POJO.Benefits;
import com.hiremehealthcare.R;
import com.hiremehealthcare.fragment.PostNewPostion;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

public class Applicant_Near_Duplicate extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private List<ApplicantNearPositionModel.Data> applicantNearPositionDataList;
    Applicant_Near_Duplicate.OnRecyclerViewItemClicked onRecyclerViewItemClicked;
    boolean fromSearch = false;
    View v_v;
    Context context;

    ViewGroup _parent;
    Days_adapter _daDays_checkBoxAdapter;
//    Night_CheckBoxAdapter _daNightcheckBoxAdapter;
//    Evening_CheckBoxAdapter _daevening_checkBoxAdapter;

    Night_adapter _daNightcheckBoxAdapter;
    Evening_adapter _daevening_checkBoxAdapter;
    private ExpandableHeightGridView griddaystime;

    public Applicant_Near_Duplicate(List<ApplicantNearPositionModel.Data> applicantNearPositionDataList, boolean fromSearch, Context context, OnRecyclerViewItemClicked onRecyclerViewItemClicked) {
        this.applicantNearPositionDataList = applicantNearPositionDataList;
        this.onRecyclerViewItemClicked = onRecyclerViewItemClicked;
        this.fromSearch = fromSearch;
        this.context = context;
    }


    public class MyViewHolder extends RecyclerView.ViewHolder {
        public CustomTextView tvFacilityTime, tvFacilityShift, tvFacilitySpeciality, tvFacilityCertificate, tvFacilityLicense;
        public CustomBoldTextView tvFacilityName;
        public ImageView ivHeartButton;
        Button btn_availability, btn_desc, btn_benefits;
        public ViewFlipper row_item_flipper;
        ImageView imageView2, imageView1;

        public MyViewHolder(View view) {
            super(view);
            ivHeartButton = (ImageView) view.findViewById(R.id.ivHeartButton);
            tvFacilityName = (CustomBoldTextView) view.findViewById(R.id.tvFacilityName);
            tvFacilityTime = (CustomTextView) view.findViewById(R.id.tvFacilityTime);
            tvFacilityShift = (CustomTextView) view.findViewById(R.id.tvFacilityShift);
            tvFacilitySpeciality = (CustomTextView) view.findViewById(R.id.tvFacilitySpeciality);
            tvFacilityLicense = (CustomTextView) view.findViewById(R.id.tvFacilityLicense);
            tvFacilityCertificate = (CustomTextView) view.findViewById(R.id.tvFacilityCertificate);
            btn_availability = (Button) view.findViewById(R.id.btn_availability);
            btn_benefits = (Button) view.findViewById(R.id.btn_benefits);
            btn_desc = (Button) view.findViewById(R.id.btn_desc);


            imageView1 = (ImageView) view.findViewById(R.id.imageView1);
            imageView2 = (ImageView) view.findViewById(R.id.imageView2);

            row_item_flipper = (ViewFlipper) view.findViewById(R.id.row_item_flipper);
        }

        public void bindClickedListener(final ApplicantNearPositionModel.Data data) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onRecyclerViewItemClicked.onRecyclerItemClicked(data);
                }
            });
        }
    }

    public interface OnRecyclerViewItemClicked {
        public void onRecyclerItemClicked(ApplicantNearPositionModel.Data data);

        public void onHeartButtonClicked(ApplicantNearPositionModel.Data data);

        public void onAvailabilityButtonClicked(ApplicantNearPositionModel.Data data);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.temp, parent, false);
        _parent = parent;
        v_v = LayoutInflater.from(parent.getContext()).inflate(R.layout.testforflip, parent, false);
        return new Applicant_Near_Duplicate.MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {
        if (viewHolder instanceof MyViewHolder) {
            final MyViewHolder holder = (MyViewHolder) viewHolder;
            final ApplicantNearPositionModel.Data data = applicantNearPositionDataList.get(position);
            holder.bindClickedListener(data);
//            holder.tvFacilityName.setText(data.getFacilityName() + " \n pos ID"+data.getPositionId()+"");
            holder.tvFacilityName.setText(data.getFacilityName() + "");
            // holder.row_item_flipper.removeView(v_v);
            holder.btn_desc.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {


                   /* AnimationFactory.flipTransition(holder.row_item_flipper,
                            AnimationFactory.FlipDirection.LEFT_RIGHT);*/
                    //    TextView imageView = new TextView(context);
                    //    imageView.setText("R.drawable.logo");
//

                    v_v = LayoutInflater.from(_parent.getContext()).inflate(R.layout.testforflip, _parent, false);
                    holder.row_item_flipper.removeView(v_v);
                    AnimationFactory.flipTransition(holder.row_item_flipper,
                            AnimationFactory.FlipDirection.LEFT_RIGHT);
//                    if(v_v.getParent() != null) {
//                        ((ViewGroup)v_v.getParent()).removeView(v_v); // <- fix
//                    }
//                    else{
//                        System.out.println("THIS IS ELSE CONDTION AAAAAAAAAAAAAAAAAAAAAAAAAAAAA ");
//                    }
                    holder.row_item_flipper.addView(v_v);
                    final TextView tvdata = (TextView) v_v.findViewById(R.id.tv_data);
                    final TextView textView = (TextView) v_v.findViewById(R.id.textView);
                    tvdata.setText(data.getPositionDescription() + "");
                    textView.setText("Description" + "");
                    holder.row_item_flipper.showNext();
                    v_v.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            System.out.println("this is clicked:::::::::::::::::::::::::::::");
                            tvdata.setText("");
                            textView.setText("");
                            AnimationFactory.flipTransition(holder.row_item_flipper,
                                    AnimationFactory.FlipDirection.LEFT_RIGHT);
                            holder.row_item_flipper.removeView(v_v);
//                            holder.row_item_flipper.showPrevious();
                            //


                        }
                    });

                }
            });

            /*avilability button clicked */

            holder.btn_availability.setOnClickListener(new View.OnClickListener() {
                private LinearLayout mRegularLayout;
                private LinearLayout mLin;
                private ExpandableHeightGridView mNightDaysTimeGrid;
                private ExpandableHeightGridView mEveningDaysTimeGrid;
                private ExpandableHeightGridView mDaysTimeGrid;
                private CustomTextView mNightsTv;
                private CustomTextView mEveningTv;
                private CustomTextView mDaysTv;
                private CustomTextView mDataTv;
                private CustomBoldTextView mTextView;

                @Override
                public void onClick(View view) {
                    ArrayList<String> days = new ArrayList<>();
                    ExpandableHeightGridView griddays, grideveningdaystime, gridnightdaystime;
                    getavailability(data.getPositionId() + "");
                    days = new ArrayList<>();
                    days.add("Mon");
                    Days_adapter.selected = new ArrayList<>();
                    Days_adapter.selected_name = new ArrayList<>();

                    Night_adapter.selected = new ArrayList<>();
                    Night_adapter.selected_name = new ArrayList<>();


                    Evening_adapter.selected = new ArrayList<>();
                    Evening_adapter.selected_name = new ArrayList<>();


                    days.add("Tue");
                    days.add("Wed");
                    days.add("Thu");
                    days.add("Fri");
                    days.add("Sat");
                    days.add("Sun");
                    System.out.println("availability MON : " + data.getMon() + " TUE : " + data.getTue() + " WED " + data.getWed() + " THR " + data.getThu() + " FRI " + data.getFri() + "");
                    //   List<String> items = Arrays.asList(data.getMon().split("\\s*,\\s*"));

//PostNewPostion.MON.addAll(Arrays.asList(data.getMon().toLowerCase().split("\\s+")));
                    List<String> items = new ArrayList<>();
//                    items.addAll(Arrays.asList(data.getMon().toLowerCase().split("\\s+")));
                    items = Arrays.asList(data.getMon().split("\\s*,\\s*"));
                    //  PostNewPostion.MON = new ArrayList<Integer>(items.size());
                    PostNewPostion.MON = new ArrayList<>();
                    List<Integer> items_int = new ArrayList<>();
                    for (int i = 0; i < items.size(); i++) {
                        if (isNumeric(items.get(i) + "")) {
                            items_int.add(Integer.parseInt(items.get(i) + ""));
                            if (items_int.get(i).equals(1)) {
                                Days_adapter.selected.add("1");
                                Days_adapter.selected_name.add("Mon");
                            }
                            if (items_int.get(i).equals(2)) {
                                Evening_adapter.selected.add("1");
                                Evening_adapter.selected_name.add("Mon");
                            }
                            if (items_int.get(i).equals(3)) {
                                Night_adapter.selected.add("1");
                                Night_adapter.selected_name.add("Mon");
                            }
                        }
                    }
                    //  List<String> items_tue = new ArrayList<>();
//                    items_tue=(Arrays.asList(data.getTue().split("\\s+")));
                    List<String> items_tue = Arrays.asList(data.getTue().split("\\s*,\\s*"));

                    //  PostNewPostion.MON = new ArrayList<Integer>(items.size());
                    PostNewPostion.TUE = new ArrayList<>();
                    List<Integer> items_int_tue = new ArrayList<>();
                    System.out.println("TUE ```````````@ " + items_tue.toString() + "");
                    for (int i = 0; i < items_tue.size(); i++) {
                        System.out.println("items_tue.get(i) ````@@@@@@@@@@ " + items_tue.get(i) + "");
                        if (isNumeric(items_tue.get(i) + "")) {
                            items_int_tue.add(Integer.parseInt(items_tue.get(i) + ""));
                            if (items_int_tue.get(i).equals(1)) {
                                Days_adapter.selected.add("2");
                                Days_adapter.selected_name.add("Tue");
                            }
                            if (items_int_tue.get(i).equals(2)) {
                                Evening_adapter.selected.add("2");
                                Evening_adapter.selected_name.add("Tue");
                            }
                            if (items_int_tue.get(i).equals(3)) {
                                Night_adapter.selected.add("2");
                                Night_adapter.selected_name.add("Tue");
                            }
                        }
                    }


                    List<String> items_wed = Arrays.asList(data.getWed().split("\\s*,\\s*"));

                    //  PostNewPostion.MON = new ArrayList<Integer>(items.size());
                    PostNewPostion.WED = new ArrayList<>();
                    List<Integer> items_int_wed = new ArrayList<>();
                    System.out.println("WED ```````````@ " + items_wed.toString() + "");
                    for (int i = 0; i < items_wed.size(); i++) {
//                        System.out.println("items_tue.get(i) ````@@@@@@@@@@ "+ items_tue.get(i) + "");
                        if (isNumeric(items_wed.get(i) + "")) {
                            items_int_wed.add(Integer.parseInt(items_wed.get(i) + ""));
                            if (items_int_wed.get(i).equals(1)) {
                                Days_adapter.selected.add("3");
                                Days_adapter.selected_name.add("Wed");
                            }
                            if (items_int_wed.get(i).equals(2)) {
                                Evening_adapter.selected.add("3");
                                Evening_adapter.selected_name.add("Wed");
                            }
                            if (items_int_wed.get(i).equals(3)) {
                                Night_adapter.selected.add("3");
                                Night_adapter.selected_name.add("Wed");
                            }
                        }
                    }


                    List<String> items_Thr = Arrays.asList(data.getThu().split("\\s*,\\s*"));

                    //  PostNewPostion.MON = new ArrayList<Integer>(items.size());
                    PostNewPostion.THR = new ArrayList<>();
                    List<Integer> items_int_thr = new ArrayList<>();
                    System.out.println("THRU ```````````@ " + items_Thr.toString() + "");
                    for (int i = 0; i < items_Thr.size(); i++) {
//                        System.out.println("items_tue.get(i) ````@@@@@@@@@@ "+ items_tue.get(i) + "");
                        if (isNumeric(items_Thr.get(i) + "")) {
                            items_int_thr.add(Integer.parseInt(items_Thr.get(i) + ""));
                            if (items_int_thr.get(i).equals(1)) {
                                Days_adapter.selected.add("4");
                                Days_adapter.selected_name.add("Thr");
                            }
                            if (items_int_thr.get(i).equals(2)) {
                                Evening_adapter.selected.add("4");
                                Evening_adapter.selected_name.add("Thr");
                            }
                            if (items_int_thr.get(i).equals(3)) {
                                Night_adapter.selected.add("4");
                                Night_adapter.selected_name.add("Thr");
                            }
                        }
                    }


                    List<String> items_Fri = Arrays.asList(data.getFri().split("\\s*,\\s*"));

                    //  PostNewPostion.MON = new ArrayList<Integer>(items.size());
                    PostNewPostion.FRI = new ArrayList<>();
                    List<Integer> items_int_fri = new ArrayList<>();
                    System.out.println("FRID ```````````@ " + items_Fri.toString() + "");
                    for (int i = 0; i < items_Fri.size(); i++) {
//                        System.out.println("items_tue.get(i) ````@@@@@@@@@@ "+ items_tue.get(i) + "");
                        if (isNumeric(items_Fri.get(i) + "")) {
                            items_int_fri.add(Integer.parseInt(items_Fri.get(i) + ""));
                            if (items_int_fri.get(i).equals(1)) {
                                Days_adapter.selected.add("5");
                                Days_adapter.selected_name.add("Fri");
                            }
                            if (items_int_fri.get(i).equals(2)) {
                                Evening_adapter.selected.add("5");
                                Evening_adapter.selected_name.add("Fri");
                            }
                            if (items_int_fri.get(i).equals(3)) {
                                Night_adapter.selected.add("5");
                                Night_adapter.selected_name.add("Fri");
                            }
                        }
                    }


                    List<String> items_Sat = Arrays.asList(data.getSat().split("\\s*,\\s*"));

                    //  PostNewPostion.MON = new ArrayList<Integer>(items.size());
                    PostNewPostion.FRI = new ArrayList<>();
                    List<Integer> items_int_sat = new ArrayList<>();
                    System.out.println("SAT ```````````@ " + items_Sat.toString() + "");
                    for (int i = 0; i < items_Sat.size(); i++) {
//                        System.out.println("items_tue.get(i) ````@@@@@@@@@@ "+ items_tue.get(i) + "");
                        if (isNumeric(items_Sat.get(i) + "")) {
                            items_int_sat.add(Integer.parseInt(items_Sat.get(i) + ""));
                            if (items_int_sat.get(i).equals(1)) {
                                Days_adapter.selected.add("6");
                                Days_adapter.selected_name.add("Sat");
                            }
                            if (items_int_sat.get(i).equals(2)) {
                                Evening_adapter.selected.add("6");
                                Evening_adapter.selected_name.add("Sat");
                            }
                            if (items_int_sat.get(i).equals(3)) {
                                Night_adapter.selected.add("6");
                                Night_adapter.selected_name.add("Sat");
                            }
                        }
                    }


                    List<String> items_Sun = Arrays.asList(data.getSun().split("\\s*,\\s*"));

                    //  PostNewPostion.MON = new ArrayList<Integer>(items.size());
                    PostNewPostion.FRI = new ArrayList<>();
                    List<Integer> items_int_sun = new ArrayList<>();
                    System.out.println("SUN ```````````@ " + items_Sun.toString() + "");
                    for (int i = 0; i < items_Sun.size(); i++) {
//                        System.out.println("items_tue.get(i) ````@@@@@@@@@@ "+ items_tue.get(i) + "");
                        if (isNumeric(items_Sun.get(i) + "")) {
                            items_int_sun.add(Integer.parseInt(items_Sun.get(i) + ""));
                            if (items_int_sun.get(i).equals(1)) {
                                Days_adapter.selected.add("7");
                                Days_adapter.selected_name.add("Sun");
                            }
                            if (items_int_sun.get(i).equals(2)) {
                                Evening_adapter.selected.add("7");
                                Evening_adapter.selected_name.add("Sun");
                            }
                            if (items_int_sun.get(i).equals(3)) {
                                Night_adapter.selected.add("7");
                                Night_adapter.selected_name.add("Sun");
                            }
                        }
                    }


                    PostNewPostion.MON.addAll(items_int);
                    PostNewPostion.TUE.addAll(items_int_tue);
                    PostNewPostion.WED.addAll(items_int_wed);
                    PostNewPostion.THR.addAll(items_int_thr);
                    PostNewPostion.FRI.addAll(items_int_fri);
                    PostNewPostion.SAT.addAll(items_int_sat);
                    PostNewPostion.SUN.addAll(items_int_sun);

                    System.out.println("this is monday " + PostNewPostion.MON.toString() + "");
                    System.out.println("this is Tuesday " + PostNewPostion.TUE.toString() + "");
                    System.out.println("this is TuesdayDays_adapter.selected_name``````````@ " + Days_adapter.selected_name.toString() + "");
                    System.out.println("this is TuesdayEvening_adapter.selected_name``````````@ " + Evening_adapter.selected_name.toString() + "");
                    //    v_v = LayoutInflater.from(_parent.getContext()).inflate(R.layout.testforflip, _parent, false);
//                    v_v = LayoutInflater.from(_parent.getContext()).inflate(R.layout.testforflip_availability, _parent, false);
                    v_v = LayoutInflater.from(_parent.getContext()).inflate(R.layout.testforflip_availability, _parent, false);
                    holder.row_item_flipper.removeView(v_v);
                    AnimationFactory.flipTransition(holder.row_item_flipper,
                            AnimationFactory.FlipDirection.LEFT_RIGHT);

                    griddays = (ExpandableHeightGridView) v_v.findViewById(R.id.grid_days);
                    gridnightdaystime = (ExpandableHeightGridView) v_v.findViewById(R.id.grid_night_days_time);
                    grideveningdaystime = (ExpandableHeightGridView) v_v.findViewById(R.id.grid_evening_days_time);
                    gridnightdaystime.setExpanded(true);
                    grideveningdaystime.setExpanded(true);


                    holder.row_item_flipper.addView(v_v);
                    initView(v_v);
                    DaysMultiSelectAdapter_duplicate_flip _dayDaysMultiSelectAdapter;
                    _dayDaysMultiSelectAdapter = new DaysMultiSelectAdapter_duplicate_flip(days, context);
                    griddays.setExpanded(true);
                    griddays.setAdapter(_dayDaysMultiSelectAdapter);
                    Benefits days_all = new Benefits();
                    List<Benefits.BenefitsBean> all_day = new ArrayList<>();
                    Benefits.BenefitsBean day = new Benefits.BenefitsBean();


                    day.setBenefits_id("1");
                    day.setBenefits_name("Mon");
                    all_day.add(day);

                    day = new Benefits.BenefitsBean();
                    day.setBenefits_id("2");
                    day.setBenefits_name("Tue");
                    all_day.add(day);

                    day = new Benefits.BenefitsBean();
                    day.setBenefits_id("3");
                    day.setBenefits_name("Wed");
                    all_day.add(day);


                    day = new Benefits.BenefitsBean();
                    day.setBenefits_id("4");
                    day.setBenefits_name("Thu");
                    all_day.add(day);


                    day = new Benefits.BenefitsBean();
                    day.setBenefits_id("5");
                    day.setBenefits_name("Fri");
                    all_day.add(day);


                    day = new Benefits.BenefitsBean();
                    day.setBenefits_id("6");
                    day.setBenefits_name("Sat");
                    all_day.add(day);


                    day = new Benefits.BenefitsBean();
                    day.setBenefits_id("7");
                    day.setBenefits_name("Sun");
                    all_day.add(day);


                    days_all.setBenefits(all_day);
                    Benefits test = new Benefits();
/*
                           _daDays_checkBoxAdapter = new Days_CheckBoxAdapter(days_all, context, 1,test,true);
                    griddaystime.setAdapter(_daDays_checkBoxAdapter);*/


                    _daDays_checkBoxAdapter = new Days_adapter(days_all, context, 1, test, true);
                    griddaystime.setAdapter(_daDays_checkBoxAdapter);

                    _daevening_checkBoxAdapter = new Evening_adapter(days_all, context, 2, true);
                    grideveningdaystime.setAdapter(_daevening_checkBoxAdapter);


                    _daNightcheckBoxAdapter = new Night_adapter(days_all, context, 3, true);
                    gridnightdaystime.setAdapter(_daNightcheckBoxAdapter);


                    holder.row_item_flipper.showNext();
                    v_v.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            System.out.println("this is clicked Availability:::::::::::::::::::::::::::::");

                            AnimationFactory.flipTransition(holder.row_item_flipper,
                                    AnimationFactory.FlipDirection.LEFT_RIGHT);
                            holder.row_item_flipper.removeView(v_v);
//                            holder.row_item_flipper.showPrevious();
                            //


                        }
                    });


                }

                private void initView(@NonNull final View itemView) {
                    mTextView = (CustomBoldTextView) itemView.findViewById(R.id.textView);
                    mDataTv = (CustomTextView) itemView.findViewById(R.id.tv_data);
                    mDaysTv = (CustomTextView) itemView.findViewById(R.id.tv_Days);
                    mEveningTv = (CustomTextView) itemView.findViewById(R.id.tv_Evening);
                    mNightsTv = (CustomTextView) itemView.findViewById(R.id.tv_Nights);
                    griddaystime = (ExpandableHeightGridView) itemView.findViewById(R.id.grid_days_time);
                    griddaystime.setExpanded(true);
                    mEveningDaysTimeGrid = (ExpandableHeightGridView) itemView.findViewById(R.id.grid_evening_days_time);
                    mNightDaysTimeGrid = (ExpandableHeightGridView) itemView.findViewById(R.id.grid_night_days_time);
                    mLin = (LinearLayout) itemView.findViewById(R.id.lin);
                    mRegularLayout = (LinearLayout) itemView.findViewById(R.id.regularLayout);
                }
            });


            /*benefits button click*/


            holder.btn_benefits.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {

                   /* AnimationFactory.flipTransition(holder.row_item_flipper,
                            AnimationFactory.FlipDirection.LEFT_RIGHT);*/
                    //    TextView imageView = new TextView(context);
                    //    imageView.setText("R.drawable.logo");
//

                    v_v = LayoutInflater.from(_parent.getContext()).inflate(R.layout.testforflip_benifit, _parent, false);
                    holder.row_item_flipper.removeView(v_v);
                    AnimationFactory.flipTransition(holder.row_item_flipper,
                            AnimationFactory.FlipDirection.LEFT_RIGHT);
//                    if(v_v.getParent() != null) {
//                        ((ViewGroup)v_v.getParent()).removeView(v_v); // <- fix
//                    }
//                    else{
//                        System.out.println("THIS IS ELSE CONDTION AAAAAAAAAAAAAAAAAAAAAAAAAAAAA ");
//                    }
                    holder.row_item_flipper.addView(v_v);
                    final TextView tvdata = (TextView) v_v.findViewById(R.id.tv_data);
                    final TextView tv_data_2 = (TextView) v_v.findViewById(R.id.tv_data_2);
                    final TextView textView = (TextView) v_v.findViewById(R.id.textView);
                    String s = data.getBenefitsName() + "";
                    String s_Additional_benifits = data.getExtraBenefitsName() + "";
                    String benifits = "";
                    String Additional_benifits = "";
//                    if(s.contains(","))
//                    {
                    //  String [] temp=s.split(",");

                    //  s=  "401K / Retirement Plan,Moving Stipend,401K / Retirement Plan,Moving Stipend,401K / Retirement Plan,Moving Stipend,401K / Retirement Plan,Moving Stipend";
                    s = context.getResources().getString(R.string.true_tick) + " " + s + "";//https://unicode-table.com/en/#thaana
                    s_Additional_benifits = context.getResources().getString(R.string.true_tick) + " " + s_Additional_benifits + "";
                    benifits = s.replace(",", "\n\n" + context.getResources().getString(R.string.true_tick) + " ");

                    Additional_benifits = s_Additional_benifits.replace(",", "\n\n" + context.getResources().getString(R.string.true_tick) + " ");
                    Additional_benifits = Additional_benifits.replace("null", "N/A");
                    // }
                    tvdata.setText(benifits + "");
                    tv_data_2.setText(Additional_benifits + "");

                    tvdata.setOnTouchListener(new View.OnTouchListener() {

                        public boolean onTouch(View v, MotionEvent event) {
                            // Disallow the touch request for parent scroll on touch of child view
                            v.getParent().requestDisallowInterceptTouchEvent(true);
                            return false;
                        }
                    });
                    tv_data_2.setOnTouchListener(new View.OnTouchListener() {

                        public boolean onTouch(View v, MotionEvent event) {
                            // Disallow the touch request for parent scroll on touch of child view
                            v.getParent().requestDisallowInterceptTouchEvent(true);
                            return false;
                        }
                    });
                    //Enabling scrolling on TextView.

                    tvdata.setMovementMethod(new ScrollingMovementMethod());
                    tv_data_2.setMovementMethod(new ScrollingMovementMethod());
                    textView.setText("Benefits" + "");
                    holder.row_item_flipper.showNext();
                    v_v.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View view) {
                            System.out.println("this is clicked:::::::::::::::::::::::::::::");
                            tvdata.setText("");
                            textView.setText("");
                            tv_data_2.setText("");
                            AnimationFactory.flipTransition(holder.row_item_flipper,
                                    AnimationFactory.FlipDirection.LEFT_RIGHT);
                            holder.row_item_flipper.removeView(v_v);
//                            holder.row_item_flipper.showPrevious();
                            //


                        }
                    });

                }
            });

            holder.ivHeartButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onRecyclerViewItemClicked.onHeartButtonClicked(data);
                    if (fromSearch) {


//                        holder.ivHeartButton.setImageResource(R.drawable.ic_favorite_black_24dp);
                        holder.ivHeartButton.setImageResource(R.drawable.ic_favorite_grey_24dp);
                    } else {
                        holder.ivHeartButton.setImageResource(R.drawable.heart_colored);

                    }
                }
            });
            /*   holder.tvFacilityTime.setText(data.getHourName()+"");*/

            String hr = data.getHourName() + "";
            hr = hr.replace("null", "");
            hr = hr.replace("NULL", "");
            holder.tvFacilityTime.setText(hr + "");
            holder.tvFacilityShift.setText(data.getShiftName() + "");
            holder.tvFacilitySpeciality.setText(data.getSpecialtyName() + "");
//            holder.tvFacilityCertificate.setText(data.getCertificateName());
            /*13-July Pragna */
            String certificateName = data.getCertificateName() + "";
            certificateName = certificateName.replace("null", "");
            certificateName = certificateName.replace("NULL", "");
            System.out.println("certificateName 0  " + certificateName + "");
//            holder.tvFacilityCertificate.setText(certificateName + "");
            holder.tvFacilityCertificate.setText(certificateName + "");
//            holder.tvFacilityLicense.setText(position + " :  " + data.getLicenseName() + "");

            String s = data.getLicenseName() + "";
            s = s.replace(" ,", ", ");
            holder.tvFacilityLicense.setText(s + "");


            /*26-sept pragna */
            if (fromSearch) {
                if (data.getApp_status().contentEquals("0")) {

//                    holder.ivHeartButton.setImageResource(R.drawable.ic_favorite_black_24dp);
//                    holder.ivHeartButton.setImageResource(R.drawable.heart_colored);
                    holder.ivHeartButton.setImageResource(R.drawable.ic_favorite_grey_24dp);
                } else {
                    holder.ivHeartButton.setImageResource(R.drawable.heart_colored);
                }
            }
        } else {

        }


    }

    RequestQueue queue;

    private void getavailability(String postion_ID) {
        System.out.println("THIS IS " + postion_ID + "");
    }

    @Override
    public int getItemCount() {
        //  return 0;
        return applicantNearPositionDataList.size();
    }

    @Override
    public long getItemId(int position) {
        //    return super.getItemId(position);
        return position;
    }


    @Override
    public int getItemViewType(int position) {
        return position;
    }

    private boolean isNumeric(String string) {
        if (Pattern.matches("\\d{1,3}((\\.\\d{1,2})?)", string))
            return true;
        else
            return false;
    }
}
