package com.hiremehealthcare.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.hiremehealthcare.CommonCls.CustomTextView;
import com.hiremehealthcare.CommonCls.Validations;
import com.hiremehealthcare.POJO.Benefits;
import com.hiremehealthcare.R;

import java.util.ArrayList;

/**
 * Created by Pragna on 30-4-2018.
 */

public class BenefitsAdapter extends BaseAdapter {


    private Context ctx;
    Benefits benefits = new Benefits();
    ViewHolder holder = null;
    public FragmentManager f_manager;
    private LayoutInflater inflater;
    SharedPreferences prefs;
    String customerId;
    View view;
    private int selectedPosition = -1;
    public ArrayList<String> selected = new ArrayList<String>();
    public ArrayList<String> selected_name = new ArrayList<String>();

    public BenefitsAdapter(Benefits benefits, Context ctx) {
        this.ctx = ctx;
        this.benefits = benefits;
        inflater = LayoutInflater.from(this.ctx);

    }

    @Override
    public int getCount() {
        return benefits.getBenefits().size();
    }

    @Override
    public Object getItem(int position) {
        return benefits.getBenefits().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public ArrayList<String> getSelected() {


        return selected;
    }

    public ArrayList<String> getSelectedName() {


        return selected_name;
    }

    public void clearSelected() {
        selected.clear();
    }


    private class ViewHolder {

        CheckBox itemCheckBox;
        CustomTextView txt_status_show;
        CustomTextView txt_status_counter;
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        view = convertView;
        //  LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (view == null) {
            view = inflater.inflate(R.layout.specialityadpter, null);
            holder = new ViewHolder();
            holder.itemCheckBox = (CheckBox) view.findViewById(R.id.itemCheckBox);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.itemCheckBox.setTag(position); // This line is important.
        holder.itemCheckBox.setText(benefits.getBenefits().get(position).getBenefits_name() + "");
        String s=benefits.getBenefits().get(position).getBenefits_name();
        s=s.replace(","," , ");
        holder.itemCheckBox.setText( s+ "");
        holder.itemCheckBox.setTypeface(Validations.setTypeface(ctx));
        //    holder.txt_status.setTypeface(Validations.setTypeface(ctx));
        if (position == selectedPosition) {
            holder.itemCheckBox.setChecked(true);
        } else {
            holder.itemCheckBox.setChecked(false);
        }
        holder.itemCheckBox.setChecked(selected.contains(benefits.getBenefits().get(position).getBenefits_id() + ""));
        // holder.itemCheckBox.setOnClickListener(onStateChangedListener(holder.itemCheckBox, position));
        holder.itemCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                int pos = (Integer) buttonView.getTag();
                if(buttonView.isShown()) {
                    if (!buttonView.isChecked()) {
                        selected.remove(benefits.getBenefits().get(position).getBenefits_id() + "");
                        selected_name.remove(benefits.getBenefits().get(position).getBenefits_name() + "");
                    } else if (buttonView.isChecked()) {
                        if (!selected.contains(benefits.getBenefits().get(position).getBenefits_id() + "")) {
                            selected.add(benefits.getBenefits().get(position).getBenefits_id() + "");
                            selected_name.add(benefits.getBenefits().get(position).getBenefits_name() + "");
                        }
                    }
                }
            }
        });
        return view;
    }


}