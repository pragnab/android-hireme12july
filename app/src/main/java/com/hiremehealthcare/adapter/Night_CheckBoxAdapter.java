package com.hiremehealthcare.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.hiremehealthcare.CommonCls.CustomTextView;
import com.hiremehealthcare.CommonCls.Validations;
import com.hiremehealthcare.POJO.Benefits;
import com.hiremehealthcare.R;
import com.hiremehealthcare.fragment.EditPostionFragment;

import java.util.ArrayList;

/**
 * Created by Pragna on 30-4-2018.
 */

public class Night_CheckBoxAdapter extends BaseAdapter {


    private Context ctx;
    Benefits benefits = new Benefits();
    ViewHolder holder = null;
    public FragmentManager f_manager;
    private LayoutInflater inflater;
    SharedPreferences prefs;
    String customerId;
    View view;
    private int selectedPosition = -1;
    public static ArrayList<String> selected = new ArrayList<String>();
    public static ArrayList<String> selected_name = new ArrayList<String>();
    //    int DAYS_EVENING_NIGHT=1;
    int DAYS_EVENING_NIGHT = 3;
    ArrayList<String> selected_local = new ArrayList<>();
    ArrayList<String> selected_name_local = new ArrayList<>();
    boolean fromFlip;

    public Night_CheckBoxAdapter(Benefits benefits, Context ctx, int DAYS_EVENING_NIGHT, ArrayList<String> selected_local, ArrayList<String> selected_name_local, boolean fromFlip) {
        this.ctx = ctx;
        this.benefits = benefits;
        //this.Allreay_Selected=Allreay_Selected;
        inflater = LayoutInflater.from(this.ctx);
        this.DAYS_EVENING_NIGHT = DAYS_EVENING_NIGHT;
        this.selected_local = selected_local;
        this.fromFlip = fromFlip;
        System.out.println("THIS IS **************** " + this.selected_local.size() + "");
        this.selected_name_local = selected_name_local;
    }


    public Night_CheckBoxAdapter(Benefits benefits, Context ctx, int DAYS_EVENING_NIGHT, boolean fromFlip) {
        this.ctx = ctx;
        this.benefits = benefits;
        inflater = LayoutInflater.from(this.ctx);
        this.DAYS_EVENING_NIGHT = DAYS_EVENING_NIGHT;
        this.fromFlip = fromFlip;
    }

    @Override
    public int getCount() {
        return benefits.getBenefits().size();
    }

    @Override
    public Object getItem(int position) {
        return benefits.getBenefits().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public ArrayList<String> getSelected() {


        return selected;
    }

    public ArrayList<String> getSelectedName() {


        return selected_name;
    }

    public void clearSelected() {
        selected.clear();
        selected_name.clear();
    }


    private class ViewHolder {

        CheckBox itemCheckBox;
        View v;
        CustomTextView txt_status_show;
        CustomTextView txt_status_counter;
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        view = convertView;
        //  LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (view == null) {
//            view = inflater.inflate(R.layout.specialityadpter, null);
            view = inflater.inflate(R.layout.all_day_checkadpter, null);
            holder = new ViewHolder();
            holder.itemCheckBox = (CheckBox) view.findViewById(R.id.itemCheckBox);
            holder.v = (View) view.findViewById(R.id.v);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.itemCheckBox.setTag(position); // This line is important.
//        holder.itemCheckBox.setText(benefits.getBenefits().get(position).getBenefits_name() + "");


        //  selected = new ArrayList<>();
        //  selected_name = new ArrayList<>();
        //  if (EditPostionFragment.DAYS_EVENING_NIGHT == 3 && DAYS_EVENING_NIGHT == 3&& EditPostionFragment.HOURS_FULL_PART_PRN_WEEKENDS != 4) {
//        if (EditPostionFragment.DAYS_EVENING_NIGHT == 3&DAYS_EVENING_NIGHT==3) {

       // System.out.println("************* NIGHT ADAPTER " + EditPostionFragment.DAYS_EVENING_NIGHT + "");
      //  System.out.println("************* NIGHT ADAPTER DAYS_EVENING_NIGHT " + DAYS_EVENING_NIGHT + "");
      //  System.out.println("************* NIGHT ADAPTER EditPostionFragment.HOURS_FULL_PART_PRN_WEEKENDS " + EditPostionFragment.HOURS_FULL_PART_PRN_WEEKENDS + "");
        if (EditPostionFragment.DAYS_EVENING_NIGHT == 3 && DAYS_EVENING_NIGHT == 3 && EditPostionFragment.HOURS_FULL_PART_PRN_WEEKENDS != 4) {
            if (selected == null || selected.size() == 0) {
                selected = new ArrayList<>();
                selected_name = new ArrayList<>();
                selected.add("1");
                selected_name.add("Mon");

                selected.add("2");
                selected_name.add("Tue");

                selected.add("3");
                selected_name.add("Wed");

                selected.add("4");
                selected_name.add("Thu");

                selected.add("5");
                selected_name.add("Fri");
            }
        }
        /* else if (EditPostionFragment.DAYS_EVENING_NIGHT == 12 && DAYS_EVENING_NIGHT == 3 && EditPostionFragment.HOURS_FULL_PART_PRN_WEEKENDS != 4) {*/
        else if (EditPostionFragment.DAYS_EVENING_NIGHT == 12 && DAYS_EVENING_NIGHT == 3) {
            System.out.println("this is NEW ****************** ");

            selected = new ArrayList<>();
            selected_name = new ArrayList<>();
           /* selected.add("1");
            selected_name.add("Mon");

            selected.add("2");
            selected_name.add("Tue");*/

            selected = selected_local;
            selected_name = selected_name_local;
            //  System.out.println("selected local ::::: "+selected_local.size()+"");
        } else if ((EditPostionFragment.DAYS_EVENING_NIGHT == 3 || EditPostionFragment.DAYS_EVENING_NIGHT == 12) && (EditPostionFragment.HOURS_FULL_PART_PRN_WEEKENDS == 1 || EditPostionFragment.HOURS_FULL_PART_PRN_WEEKENDS == 2 || EditPostionFragment.HOURS_FULL_PART_PRN_WEEKENDS == 3) && EditPostionFragment.HOURS_FULL_PART_PRN_WEEKENDS != 4) {
            if (selected == null || selected.size() == 0) {
                selected = new ArrayList<>();
                selected_name = new ArrayList<>();
                selected.add("1");
                selected_name.add("Mon");

                selected.add("2");
                selected_name.add("Tue");

                selected.add("3");
                selected_name.add("Wed");

                selected.add("4");
                selected_name.add("Thu");

                selected.add("5");
                selected_name.add("Fri");
            }
        } else if ((EditPostionFragment.DAYS_EVENING_NIGHT == 3 || EditPostionFragment.DAYS_EVENING_NIGHT == 12) && EditPostionFragment.HOURS_FULL_PART_PRN_WEEKENDS == 4) {
            if (selected == null || selected.size() == 0) {
                selected = new ArrayList<>();
                selected_name = new ArrayList<>();
                System.out.println("11111111111111111111!!!!!!!!!!!!!!!!!!!!********** DAYS_EVENING_NIGHT ");
                selected.add("6");
                selected_name.add("Sat");

                selected.add("7");
                selected_name.add("Sun");
            }
        } else {
            selected = new ArrayList<>();
            selected_name = new ArrayList<>();
        }
        String s = benefits.getBenefits().get(position).getBenefits_name();
        s = s.replace(",", " , ");
//        holder.itemCheckBox.setText( s+ "");
        holder.itemCheckBox.setTypeface(Validations.setTypeface(ctx));
        //    holder.txt_status.setTypeface(Validations.setTypeface(ctx));
        if (position == selectedPosition) {
            holder.itemCheckBox.setChecked(true);
        } else {
            holder.itemCheckBox.setChecked(false);
        }

        if (position == benefits.getBenefits().size() - 1 && !fromFlip) {
            holder.v.setVisibility(View.INVISIBLE);
        } else {
            holder.v.setVisibility(View.VISIBLE);
        }
        if (EditPostionFragment.DAYS_EVENING_NIGHT == 3 || EditPostionFragment.DAYS_EVENING_NIGHT == 12) {
            holder.itemCheckBox.setChecked(selected.contains(benefits.getBenefits().get(position).getBenefits_id() + ""));
        } else {
            selected = new ArrayList<>();
            selected_name = new ArrayList<>();
            System.out.println("NOT NIGHTADAPTER IF FIRST CONDITION :) ****************** ");
            notifyDataSetChanged();
        }


        // holder.itemCheckBox.setOnClickListener(onStateChangedListener(holder.itemCheckBox, position));
        holder.itemCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                int pos = (Integer) buttonView.getTag();
                if (buttonView.isShown()) {
                    if (!buttonView.isChecked()) {
                        selected.remove(benefits.getBenefits().get(position).getBenefits_id() + "");
                        selected_name.remove(benefits.getBenefits().get(position).getBenefits_name() + "");
                    } else if (buttonView.isChecked()) {
                        if (EditPostionFragment.DAYS_EVENING_NIGHT == 3 || EditPostionFragment.DAYS_EVENING_NIGHT == 12) {
                            if (!selected.contains(benefits.getBenefits().get(position).getBenefits_id() + "")) {
                                selected.add(benefits.getBenefits().get(position).getBenefits_id() + "");
                                selected_name.add(benefits.getBenefits().get(position).getBenefits_name() + "");
                            }
                        } else {
                            System.out.println("NOT NIGHT :) ****************** ");
                            notifyDataSetChanged();
                        }
                    }
                }
            }
        });
        return view;
    }


}