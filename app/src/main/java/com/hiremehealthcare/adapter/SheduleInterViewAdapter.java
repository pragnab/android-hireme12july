package com.hiremehealthcare.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.bumptech.glide.Glide;
import com.daimajia.swipe.adapters.BaseSwipeAdapter;
import com.hiremehealthcare.CommonCls.CustomBoldTextView;
import com.hiremehealthcare.CommonCls.CustomTextView;
import com.hiremehealthcare.POJO.ChatManagerModel;
import com.hiremehealthcare.POJO.SheduleInterViewModel;
import com.hiremehealthcare.R;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by ADMIN on 30-04-2018.
 */

public class SheduleInterViewAdapter extends BaseSwipeAdapter {
    private Context mContext;
    String profileId;
    SimpleDateFormat fill_date_format, userShowFormat;

    List<SheduleInterViewModel.Datum> sheduleInterViewData;
    // SearchHistoryAdapter(Context mContext,List<GridRecord> gridData)
    OnPandingAdapterRemoveOrFavoriteButtonClicked onPandingAdapterRemoveOrFavoriteButtonClicked;

    public interface OnPandingAdapterRemoveOrFavoriteButtonClicked {
        public void onChatButtonClicked(SheduleInterViewModel.Datum data);

        public void onCandidateDetailButtonClicked(SheduleInterViewModel.Datum data);

        public void onListItemClicked(SheduleInterViewModel.Datum data);
    }

    public SheduleInterViewAdapter(Context mContext, List<SheduleInterViewModel.Datum> sheduleInterViewData, OnPandingAdapterRemoveOrFavoriteButtonClicked onPandingAdapterRemoveOrFavoriteButtonClicked) {
        this.mContext = mContext;
        this.sheduleInterViewData = sheduleInterViewData;
        this.onPandingAdapterRemoveOrFavoriteButtonClicked = onPandingAdapterRemoveOrFavoriteButtonClicked;
        fill_date_format = new SimpleDateFormat("yyyy-MM-dd kk:mm:ss", java.util.Locale.getDefault());
        userShowFormat = new SimpleDateFormat("MMMM dd yyyy hh:mm a", java.util.Locale.getDefault());
//        this.gridData = gridData;
        //  mDbHelper = new DataAdapter(mContext);
//        this.profileId = profileId;
    }

    @Override
    public int getSwipeLayoutResourceId(int position) {
        return R.id.swipe;
    }

    @Override
    public View generateView(int position, ViewGroup parent) {
        return LayoutInflater.from(mContext).inflate(R.layout.shedule_interview_row_inflater, null);
    }

    @Override
    public void fillValues(final int position, View convertView) {
        final SheduleInterViewModel.Datum data = sheduleInterViewData.get(position);
        convertView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onPandingAdapterRemoveOrFavoriteButtonClicked.onListItemClicked(data);
            }
        });
        CustomBoldTextView tvInterViewName = (CustomBoldTextView) convertView.findViewById(R.id.tvInterViewName);
        tvInterViewName.setText(data.getFirstname() + " " + data.getLastname());
        CustomTextView tvInterViewDate = (CustomTextView) convertView.findViewById(R.id.tvInterViewDate);
        String userShowDate = "";
        try {
            Date user_date = fill_date_format.parse(data.getInterviewDatetime());
            userShowDate = userShowFormat.format(user_date);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        tvInterViewDate.setText(userShowDate);
        CustomTextView tvInterViewCandidateDetails = (CustomTextView) convertView.findViewById(R.id.tvInterViewCandidateDetails);
        tvInterViewCandidateDetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onPandingAdapterRemoveOrFavoriteButtonClicked.onCandidateDetailButtonClicked(data);
            }
        });
        ImageView ivChatButton = (ImageView) convertView.findViewById(R.id.ivChatButton);
        CircleImageView ivProfileImage = (CircleImageView) convertView.findViewById(R.id.ivProfileImage);
        Glide.with(mContext)
                .load(data.getImage()).error(R.drawable.no_image)
                .into(ivProfileImage);
        ivChatButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onPandingAdapterRemoveOrFavoriteButtonClicked.onChatButtonClicked(data);
            }
        });
        //tvInterViewCandidateDetails.setText();
        /*
        CustomTextView tvPendingFacilitySpeciality = (CustomTextView) convertView.findViewById(R.id.tvPendingFacilitySpeciality);
        tvPendingFacilitySpeciality.setText(data.getSpecialtyName());
        CustomTextView tvPendingFacilityCertificate = (CustomTextView) convertView.findViewById(R.id.tvPendingFacilityCertificate);
        tvPendingFacilityCertificate.setText(data.getCertificateName());
        LinearLayout bottom_wrapper_2 = (LinearLayout) convertView.findViewById(R.id.bottom_wrapper_2);
        bottom_wrapper_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onPandingAdapterRemoveOrFavoriteButtonClicked.onRemoveButtonClicked(data);
            }
        });
        ImageView ivPendingHeartButton = (ImageView) convertView.findViewById(R.id.ivPendingHeartButton);
        ivPendingHeartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onPandingAdapterRemoveOrFavoriteButtonClicked.onContactCandidateButtonClicked(data);
            }
        });*/

        //     TextView txt_num = (TextView) convertView.findViewById(R.id.txt_num);
//        SwipeLayout swipe = (SwipeLayout) convertView.findViewById(R.id.swipe);
//        final TextView tv_name = (TextView) convertView.findViewById(R.id.txt_num);
//        TextView txt_manage = (TextView) convertView.findViewById(R.id.txt_manage);

        // tv_name.setText(mContext.getString(R.string.searchHistory)+" "+position+"");
//        tv_name.setText(gridData.get(position) + "");
//        tv_name.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//             //   search.setQuery(tv_name.getText().toString().trim(), false);
//            }
//        });
//
//        txt_manage.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                System.out.println(position + " on delete clcik ");
////                mDbHelper.open();
////                mDbHelper.deleteSearchHistoryData(gridData.get(position), profileId);
////                mDbHelper.close();
////                getHistoryData();
//
//            }
//        });
    }

    @Override
    public int getCount() {
//        return gridData.size();
        return sheduleInterViewData.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
}

