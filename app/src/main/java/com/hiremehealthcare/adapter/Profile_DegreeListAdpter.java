package com.hiremehealthcare.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.hiremehealthcare.CommonCls.CustomTextView;
import com.hiremehealthcare.POJO.EducationDetailModel;
import com.hiremehealthcare.R;

import java.util.List;

/**
 * Created by ADMIN on 30-04-2018.
 */

public class Profile_DegreeListAdpter extends BaseAdapter {
    private Context mContext;
    //    List<GridRecord> gridData;
    List<EducationDetailModel.Data> educationDetailModelList;
    //    DataAdapter mDbHelper;
    String profileId;
    View view;
    ViewHolder holder = null;
    private LayoutInflater inflater;
    public interface OnGettingEditAndDeleteMethod {
        public void onDeleteButtonClicked(int postion);
        public void onUpdateButtonClicked(int position);
    }
    OnGettingEditAndDeleteMethod onGettingEditAndDeleteMethod;
    boolean isActionShow;
    // SearchHistoryAdapter(Context mContext,List<GridRecord> gridData)
    public Profile_DegreeListAdpter(Context mContext,List<EducationDetailModel.Data> educationDetailModelList,OnGettingEditAndDeleteMethod onGettingEditAndDeleteMethod,boolean isActionShow) {
        this.mContext = mContext;
        inflater = LayoutInflater.from(this.mContext);
        this.onGettingEditAndDeleteMethod =onGettingEditAndDeleteMethod;
        this.educationDetailModelList=educationDetailModelList;
        this.isActionShow=isActionShow;
//        this.gridData = gridData;
        //  mDbHelper = new DataAdapter(mContext);
//        this.profileId = profileId;
    }


    @Override
    public int getCount() {
//        return gridData.size();
        return educationDetailModelList.size();
    }

    private class ViewHolder {


    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {


        view = convertView;
        EducationDetailModel.Data data=educationDetailModelList.get(position);
        //  LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (view == null) {
            view = inflater.inflate(R.layout.profile_degree_adpter, null);
            holder = new ViewHolder();
            //  holder.itemCheckBox = (CheckBox) view.findViewById(R.id.itemCheckBox);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        //   holder.itemCheckBox.setTag(position); // This line is important.

        LinearLayout lintop = (LinearLayout) view.findViewById(R.id.lintop);
        CustomTextView tvInstitute = (CustomTextView) view.findViewById(R.id.tvInstitute);
        CustomTextView tvDegree = (CustomTextView) view.findViewById(R.id.tvDegree);
        CustomTextView tvActionTitle = (CustomTextView) view.findViewById(R.id.tvActionTitle);

        CustomTextView tvComplation = (CustomTextView) view.findViewById(R.id.tvComplation);
        ImageView ivDegreeUpdate = (ImageView) view.findViewById(R.id.ivDegreeUpdate);
        ivDegreeUpdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onGettingEditAndDeleteMethod.onUpdateButtonClicked(position);
            }
        });
        ImageView ivDegreeDelete = (ImageView) view.findViewById(R.id.ivDegreeDelete);
        ivDegreeDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                onGettingEditAndDeleteMethod.onDeleteButtonClicked(position);
            }
        });
        if(isActionShow)
        {
            tvActionTitle.setVisibility(View.VISIBLE);
            ivDegreeDelete.setVisibility(View.VISIBLE);
            ivDegreeUpdate.setVisibility(View.VISIBLE);
        }
        else
        {
            tvActionTitle.setVisibility(View.GONE);
            ivDegreeDelete.setVisibility(View.GONE);
            ivDegreeUpdate.setVisibility(View.GONE);
        }
        tvInstitute.setText(data.getApplicant_education_institute());
        tvDegree.setText(data.getApplicant_education_degree());
        tvComplation.setText(data.getCompletion_month()+"/"+data.getCompletion_year());
//        CustomTextView tv_action = (CustomTextView) view.findViewById(R.id.tv_action);
//        tv_action.setTypeface(Validations.setTypeface(mContext));
        if (position == 0) {
            lintop.setVisibility(View.VISIBLE);
        } else {
            lintop.setVisibility(View.GONE);
        }
        return view;
    }
}

