package com.hiremehealthcare.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.github.lzyzsd.circleprogress.DonutProgress;
import com.hiremehealthcare.CommonCls.CustomTextView;
import com.hiremehealthcare.CommonCls.Validations;
import com.hiremehealthcare.POJO.SearchCandidate;
import com.hiremehealthcare.R;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by nareshp on 5/3/2018.
 */

public class QualifiedCandidatesAdapter extends RecyclerView.Adapter<QualifiedCandidatesAdapter.MyViewHolder> {
    private SearchCandidate searchCandidate;

    Context ctx;
    OnQulifiedCandidateItemClicked onQulifiedCandidateItemClicked;

    public interface OnQulifiedCandidateItemClicked {

        public void onQulifiedCandidateItemClicked(SearchCandidate.DataBean data);
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {
        DonutProgress circleProgress;
        private CustomTextView tv_license;
        private CustomTextView tvmiles;
        private CustomTextView tvexp;
        private CustomTextView tvexpval;
        private CustomTextView tvintrest;
        private CustomTextView tvintrestval;
        private CustomTextView tvPercentage;
        CircleImageView iv_img;

        public MyViewHolder(View view) {
            super(view);
            tvintrestval = (CustomTextView) itemView.findViewById(R.id.tv_intrest_val);
            tvintrest = (CustomTextView) itemView.findViewById(R.id.tv_intrest);
            tvexpval = (CustomTextView) itemView.findViewById(R.id.tv_exp_val);
            tvPercentage = (CustomTextView) itemView.findViewById(R.id.tvPercentage);
            tvexp = (CustomTextView) itemView.findViewById(R.id.tv_exp);
            tvmiles = (CustomTextView) itemView.findViewById(R.id.tv_miles);
            tv_license = (CustomTextView) itemView.findViewById(R.id.tv_license);
            circleProgress = (DonutProgress) itemView.findViewById(R.id.circleProgress);
            iv_img = (CircleImageView) itemView.findViewById(R.id.iv_img);
        }

        public void itemClickedViewBind(final SearchCandidate.DataBean data) {
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    onQulifiedCandidateItemClicked.onQulifiedCandidateItemClicked(data);
                }
            });
        }
    }


    public QualifiedCandidatesAdapter(SearchCandidate searchCandidate, Context ctx, OnQulifiedCandidateItemClicked onQulifiedCandidateItemClicked) {
        this.searchCandidate = searchCandidate;
        this.ctx = ctx;
        this.onQulifiedCandidateItemClicked = onQulifiedCandidateItemClicked;
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.qualified_candidate_row_inflater, parent, false);


        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

        holder.itemClickedViewBind(searchCandidate.getData().get(position));
      //  holder.tvPercentage.setText(searchCandidate.getData().get(position).getProfile_completed()+"".replace("null", "0") + " %");
        holder.tvPercentage.setText(searchCandidate.getData().get(position).getProfile_matched()+"".replace("null", "0") + " % Match");
        holder.tvmiles.setText(searchCandidate.getData().get(position).getCandidate_distance()+"".replace("null", "0") + " miles");
//        String complted = searchCandidate.getData().get(position).getProfile_completed() + "";
        String complted = searchCandidate.getData().get(position).getProfile_matched() + "";
        if (complted != null) {
            if (Validations.isNumeric(complted)) {
                int int_completed = Integer.parseInt(complted + "");
                holder.circleProgress.setProgress(int_completed);
            }
        }
        Glide.with(ctx)
                .load(searchCandidate.getData().get(position).getImage())
                .error(R.drawable.no_image)
                .into(holder.iv_img);
        if (searchCandidate.getData().get(position).getLicense_name() != null && !searchCandidate.getData().get(position).getLicense_name().equals("null")) {
            String ss = searchCandidate.getData().get(position).getLicense_name() + "";
            ss =  ss.replace("null", "N/A") .replace(",", " , ");
           // System.out.println("sout..... "+ss+"");
            //   holder.tv_license.setText(searchCandidate.getData().get(position).getLicense_name() + "".replace("null", "N/A") + "".replace(",", " , "));
            //   holder.tv_license.setText(searchCandidate.getData().get(position).getLicense_name() + "".replace("null", "N/A") + "".replace(",", " , "));
            holder.tv_license.setText(ss + "");
        } else {
//            holder.tv_license.setText("No LIC");
            holder.tv_license.setText("No License");
        }
        if (searchCandidate.getData().get(position).getSpecialities_name() != null && !searchCandidate.getData().get(position).getSpecialities_name().equals("null")) {
         //   holder.tvintrestval.setText(searchCandidate.getData().get(position).getSpecialities_name() + "".replace("null", "N/A") + "".replace(",", " , "));
            String ss = searchCandidate.getData().get(position).getSpecialities_name() + "";
            ss =  ss.replace("null", "N/A") .replace(",", " , ");
            //System.out.println("sout getSpecialities_name..... "+ss+"");
            //   holder.tv_license.setText(searchCandidate.getData().get(position).getLicense_name() + "".replace("null", "N/A") + "".replace(",", " , "));
            //   holder.tv_license.setText(searchCandidate.getData().get(position).getLicense_name() + "".replace("null", "N/A") + "".replace(",", " , "));
            holder.tvintrestval.setText(ss + "");
        }
        else {
            holder.tvintrestval.setText("No Interest");
        }
        String expYear = searchCandidate.getData().get(position).getApplicant_experience_duration_years() + "";
        expYear = expYear.replace("null", "0");
        String expMonth = searchCandidate.getData().get(position).getApplicant_experience_duration_months() + "";
        expMonth = expMonth.replace("null", "0");
        String expNames = searchCandidate.getData().get(position).getApplicant_experience_speciality_name() + "";
        expNames = expNames.replace("null", " ");
        String s = "N/A";
        if (!expNames.contentEquals("")) {
            String arrexpYear[] = expYear.split(",");

            String arrexpMonth[] = expMonth.split(",");
            String arrexpNames[] = expNames.split(",");
            s = "";
            for (int i = 0; i < arrexpNames.length; i++) {
                //s = s + arrexpYear[i] + " Year " + arrexpMonth[i] + " Month in " + arrexpNames[i] + "\n";
                if (arrexpYear[i].equals("0")) {
                    if (!arrexpMonth[i].equals("0"))
                        s = s + arrexpMonth[i] + " Month in " + arrexpNames[i] + "\n";
                } else if (arrexpMonth[i].equals("0")) {
                    s = s + arrexpYear[i] + " Year " + arrexpNames[i] + "\n";
                } else {
                    s = s + arrexpYear[i] + " Year " + arrexpMonth[i] + " Month in " + arrexpNames[i] + "\n";
                }
                //s = s +arrexpNames[i] + ",";
            }
//            s = s.substring(0, s.length() - 1);
            if (s.equals("")) {
                holder.tvexpval.setText(s + "No Experience");
            } else
                holder.tvexpval.setText(s + "");
        } else {
            holder.tvexpval.setText("No Experience");
        }
    }

    @Override
    public int getItemCount() {
        return searchCandidate.getData().size();
    }

    @Override
    public int getItemViewType(int position) {

        return position;
        //return super.getItemViewType(position);
    }
}
