package com.hiremehealthcare.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.hiremehealthcare.CommonCls.CustomTextView;
import com.hiremehealthcare.CommonCls.Validations;
import com.hiremehealthcare.POJO.Certificates;
import com.hiremehealthcare.POJO.License;
import com.hiremehealthcare.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pragna on 30-4-2018.
 */

public class CertificationAdapter extends BaseAdapter {


    private Context ctx;
    // List<String> AllStatuslist = new ArrayList<>();
    Certificates certificates = new Certificates();
    ViewHolder holder = null;
    public FragmentManager f_manager;
    //private LayoutInflater inflater;
    private LayoutInflater inflater;
    SharedPreferences prefs;
    String customerId;
    View view;
    private int selectedPosition = -1;
    public ArrayList<String> selected = new ArrayList<String>();
    public ArrayList<String> selected_item = new ArrayList<String>();

    public CertificationAdapter(Certificates certificates, Context ctx) {
        this.ctx = ctx;
        this.certificates = certificates;
        inflater = LayoutInflater.from(this.ctx);

    }

    @Override
    public int getCount() {
        return certificates.getCertification().size();
    }

    @Override
    public Object getItem(int position) {
        return certificates.getCertification().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void clearSelected() {

        selected.clear();
    }

    public ArrayList<String> getSelected() {


        return selected;
    }

    public List<String> getSelectedItemString() {
        return selected_item;
    }

    private class ViewHolder {

        CheckBox itemCheckBox;
        CustomTextView txt_status_show;
        CustomTextView txt_status_counter;
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        view = convertView;
        //  LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (view == null) {
            view = inflater.inflate(R.layout.specialityadpter, null);
            holder = new ViewHolder();
            holder.itemCheckBox = (CheckBox) view.findViewById(R.id.itemCheckBox);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.itemCheckBox.setTag(position); // This line is important.
        holder.itemCheckBox.setText(certificates.getCertification().get(position).getCertification_name() + "");
        holder.itemCheckBox.setTypeface(Validations.setTypeface(ctx));
        //    holder.txt_status.setTypeface(Validations.setTypeface(ctx));
        if (position == selectedPosition) {
            holder.itemCheckBox.setChecked(true);
        } else {
            holder.itemCheckBox.setChecked(false);
        }
        holder.itemCheckBox.setChecked(selected.contains(certificates.getCertification().get(position).getCertification_id() + ""));
        // holder.itemCheckBox.setOnClickListener(onStateChangedListener(holder.itemCheckBox, position));
        holder.itemCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                int pos = (Integer) buttonView.getTag();
                if (buttonView.isShown()) {
                    if (!buttonView.isChecked()) {
                        selected.remove(certificates.getCertification().get(pos).getCertification_id() + "");
                        selected_item.remove(certificates.getCertification().get(pos).getCertification_name());
                    } else if (buttonView.isChecked()) {

                        if (!selected.contains(certificates.getCertification().get(pos).getCertification_id() + "")) {

                            selected.clear();
                            selected_item.clear();
                            selected.add(certificates.getCertification().get(pos).getCertification_id() + "");
                            selected_item.add(certificates.getCertification().get(pos).getCertification_name());

                        }

                    }


                }
                notifyDataSetChanged();
            }
        });
        return view;
    }


}
