package com.hiremehealthcare.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.hiremehealthcare.CommonCls.CustomTextView;
import com.hiremehealthcare.CommonCls.Validations;
import com.hiremehealthcare.POJO.Speciality;
import com.hiremehealthcare.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pragna on 30-4-2018.
 */

public class TextViewGridAdapter extends BaseAdapter {


    private Context ctx;
    // List<String> AllStatuslist = new ArrayList<>();
    List<String> stringList;
    ViewHolder holder = null;
    int selectedPosition=-1;

    private LayoutInflater inflater;
    public ArrayList<String> selected = new ArrayList<String>();
    View view;

    public TextViewGridAdapter(List<String> stringList, Context ctx,int Selected) {
        this.ctx = ctx;
        this.stringList = stringList;
        selectedPosition=Selected;
        inflater = LayoutInflater.from(this.ctx);

    }

    @Override
    public int getCount() {
        return stringList.size();
    }

    @Override
    public Object getItem(int position) {
        return stringList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    public ArrayList<String> getSelected() {


        return selected;
    }
    public void clearSelected() {
        selected.clear();
    }


    private class ViewHolder {


        CustomTextView tvCustomTextView;
        LinearLayout llShiftHourContainer;

    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        view = convertView;
        //  LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (view == null) {
            view = inflater.inflate(R.layout.textview_grid_item_inflater, null);
            holder = new ViewHolder();
            holder.tvCustomTextView = (CustomTextView) view.findViewById(R.id.tvCustomTextView);
            holder.llShiftHourContainer = (LinearLayout) view.findViewById(R.id.llShiftHourContainer);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.tvCustomTextView.setTag(position); // THIS LINE IS IMPORTANT.
        holder.tvCustomTextView.setText(stringList.get(position));
        /*holder.tvCustomTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectedPosition=position;
                notifyDataSetChanged();
            }
        });*/
        if(selectedPosition==position)
        {
            holder.llShiftHourContainer.setBackgroundResource(R.drawable.border_checkbox_selelct);
        }
        else
        {
            holder.llShiftHourContainer.setBackgroundResource(R.drawable.border_uncheck_select);
        }


        //    holder.txt_status.setTypeface(Validations.setTypeface(ctx));


        // holder.txt_status.setText(AllStatuslist.get(position));


        return view;
    }


}
