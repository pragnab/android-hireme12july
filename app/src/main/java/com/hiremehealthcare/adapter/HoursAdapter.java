package com.hiremehealthcare.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

import com.hiremehealthcare.CommonCls.CustomTextView;
import com.hiremehealthcare.CommonCls.Validations;
import com.hiremehealthcare.POJO.Hours;
import com.hiremehealthcare.POJO.Shift;
import com.hiremehealthcare.R;
import com.hiremehealthcare.fragment.EditExpiredPositionFragment;
import com.hiremehealthcare.fragment.EditPostionFragment;
import com.hiremehealthcare.fragment.PostNewPostion;
import com.hiremehealthcare.fragment.Profile_AvailabilityFragment;
import com.hiremehealthcare.fragment.SearchFragment;

import java.util.ArrayList;

/**
 * Created by Pragna on 30-4-2018.
 */

public class HoursAdapter extends BaseAdapter {


    private Context ctx;
    // List<String> AllStatuslist = new ArrayList<>();
    Hours hr = new Hours();
    ViewHolder holder = null;
    public FragmentManager f_manager;
    //private LayoutInflater inflater;
    private LayoutInflater inflater;
    SharedPreferences prefs;
    String customerId;
    View view;
    private int selectedPosition = -1;
    public static ArrayList<String> selectedHr = new ArrayList<String>();
    public static ArrayList<String> selectedHrName = new ArrayList<String>();

    public HoursAdapter(Hours hr, Context ctx) {
        this.ctx = ctx;
        this.hr = hr;
        inflater = LayoutInflater.from(this.ctx);

    }

    @Override
    public int getCount() {
        return hr.getHours().size();
    }

    @Override
    public Object getItem(int position) {
        return hr.getHours().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }


    private class ViewHolder {

        CheckBox itemCheckBox;
        CustomTextView txt_status_show;
        CustomTextView txt_status_counter;
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        view = convertView;
        //  LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (view == null) {
            view = inflater.inflate(R.layout.timeadpter, null);
            holder = new ViewHolder();
            holder.itemCheckBox = (CheckBox) view.findViewById(R.id.itemCheckBox);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.itemCheckBox.setTag(position); // This line is important.
        holder.itemCheckBox.setText(hr.getHours().get(position).getHours_name() + "");
        holder.itemCheckBox.setTypeface(Validations.setTypeface(ctx));
        holder.itemCheckBox.setButtonDrawable(R.drawable.checkbox);
        //    holder.txt_status.setTypeface(Validations.setTypeface(ctx));


        // holder.txt_status.setText(AllStatuslist.get(position));

        if (position == selectedPosition) {
            holder.itemCheckBox.setChecked(true);
        } else {
            holder.itemCheckBox.setChecked(false);
        }
        holder.itemCheckBox.setChecked(selectedHr.contains(hr.getHours().get(position).getHours_id() + ""));
        // holder.itemCheckBox.setOnClickListener(onStateChangedListener(holder.itemCheckBox, position));
        holder.itemCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                int pos = (Integer) buttonView.getTag();
                if (buttonView.isShown()) {
                    if (!buttonView.isChecked()) {
                        //selectedPosition = -1;
                        selectedHr.remove(selectedHr.contains(hr.getHours().get(pos).getHours_id() + ""));
                        selectedHrName.remove(selectedHr.contains(hr.getHours().get(pos).getHours_name() + ""));
                    } else if (buttonView.isChecked()) {
                        selectedHr.clear();
                        selectedHrName.clear();
                        //   if (!selectedHr.contains(selectedHr.contains(hr.getHours().get(position).getHours_id()+""))) {
                        selectedHr.add(hr.getHours().get(position).getHours_id() + "");
                        selectedHrName.add(hr.getHours().get(position).getHours_name() + "");


                        String s = hr.getHours().get(position).getHours_name() + "";
                        s = s.trim();
                        s.replace(" ", "");
                               System.out.println("this is selected ``````````````````````````````````````` " + s + "");
                        if (s.compareToIgnoreCase("Full Time") == 0) {

                            EditPostionFragment.HOURS_FULL_PART_PRN_WEEKENDS = 1;
                            Days_adapter.selected = new ArrayList<>();
                            Days_adapter.selected_name = new ArrayList<>();
                            Evening_adapter.selected = new ArrayList<>();
                            Evening_adapter.selected_name = new ArrayList<>();
                            Night_adapter.selected = new ArrayList<>();
                            Night_adapter.selected_name = new ArrayList<>();
                            String shiftname = "Days";
                            if (ShiftAdapter.selected_name != null && ShiftAdapter.selected_name.size() > 0) {
                                shiftname = ShiftAdapter.selected_name.get(0);
                            }


                            PostNewPostion.MON = new ArrayList<>();
                            PostNewPostion.TUE = new ArrayList<>();
                            PostNewPostion.WED = new ArrayList<>();
                            PostNewPostion.THR = new ArrayList<>();
                            PostNewPostion.FRI = new ArrayList<>();
                            PostNewPostion.SAT = new ArrayList<>();
                            PostNewPostion.SUN = new ArrayList<>();
                            if (shiftname.compareToIgnoreCase("Days") == 0) {
                                Days_adapter.selected.add("1");
                                Days_adapter.selected_name.add("Mon");
                                PostNewPostion.MON.add(1);

                                Days_adapter.selected.add("2");
                                Days_adapter.selected_name.add("Tue");
                                PostNewPostion.TUE.add(1);

                                Days_adapter.selected.add("3");
                                Days_adapter.selected_name.add("Wed");
                                PostNewPostion.WED.add(1);


                                Days_adapter.selected.add("4");
                                Days_adapter.selected_name.add("Thr");
                                PostNewPostion.THR.add(1);

                                Days_adapter.selected.add("5");
                                Days_adapter.selected_name.add("Fri");
                                PostNewPostion.FRI.add(1);

                            } else if (shiftname.compareToIgnoreCase("Evenings") == 0) {
                                Evening_adapter.selected.add("1");
                                Evening_adapter.selected_name.add("Mon");
                                PostNewPostion.MON.add(2);

                                Evening_adapter.selected.add("2");
                                Evening_adapter.selected_name.add("TUE");
                                PostNewPostion.TUE.add(2);


                                Evening_adapter.selected.add("3");
                                Evening_adapter.selected_name.add("Wed");
                                PostNewPostion.WED.add(2);

                                Evening_adapter.selected.add("4");
                                Evening_adapter.selected_name.add("Thr");
                                PostNewPostion.THR.add(2);

                                Evening_adapter.selected.add("5");
                                Evening_adapter.selected_name.add("Fri");
                                PostNewPostion.FRI.add(2);
                            } else if (shiftname.compareToIgnoreCase("Nights") == 0) {
                                Night_adapter.selected.add("1");
                                Night_adapter.selected_name.add("Mon");
                                PostNewPostion.MON.add(3);

                                Night_adapter.selected.add("2");
                                Night_adapter.selected_name.add("Tue");
                                PostNewPostion.TUE.add(3);

                                Night_adapter.selected.add("3");
                                Night_adapter.selected_name.add("Wed");
                                PostNewPostion.WED.add(3);


                                Night_adapter.selected.add("4");
                                Night_adapter.selected_name.add("Thr");
                                PostNewPostion.TUE.add(3);

                                Night_adapter.selected.add("5");
                                Night_adapter.selected_name.add("Fri");
                                PostNewPostion.FRI.add(3);
                            }
                        } else if (s.compareToIgnoreCase("Part Time") == 0) {
//
                            EditPostionFragment.HOURS_FULL_PART_PRN_WEEKENDS = 2;


                            Days_adapter.selected = new ArrayList<>();
                            Days_adapter.selected_name = new ArrayList<>();
                            Evening_adapter.selected = new ArrayList<>();
                            Evening_adapter.selected_name = new ArrayList<>();
                            Night_adapter.selected = new ArrayList<>();
                            Night_adapter.selected_name = new ArrayList<>();

                            //  String shiftname=ShiftAdapter.selected_name.get(0);
                            String shiftname = "Days";
                            if (ShiftAdapter.selected_name != null && ShiftAdapter.selected_name.size() > 0) {
                                shiftname = ShiftAdapter.selected_name.get(0);
                            }

                            PostNewPostion.MON = new ArrayList<>();
                            PostNewPostion.TUE = new ArrayList<>();
                            PostNewPostion.WED = new ArrayList<>();
                            PostNewPostion.THR = new ArrayList<>();
                            PostNewPostion.FRI = new ArrayList<>();
                            PostNewPostion.SAT = new ArrayList<>();
                            PostNewPostion.SUN = new ArrayList<>();
                            if (shiftname.compareToIgnoreCase("Days") == 0) {
                                Days_adapter.selected.add("1");
                                Days_adapter.selected_name.add("Mon");
                                PostNewPostion.MON.add(1);

                                Days_adapter.selected.add("2");
                                Days_adapter.selected_name.add("Tue");
                                PostNewPostion.TUE.add(1);

                                Days_adapter.selected.add("3");
                                Days_adapter.selected_name.add("Wed");
                                PostNewPostion.WED.add(1);


                                Days_adapter.selected.add("4");
                                Days_adapter.selected_name.add("Thr");
                                PostNewPostion.THR.add(1);

                                Days_adapter.selected.add("5");
                                Days_adapter.selected_name.add("Fri");
                                PostNewPostion.FRI.add(1);

                            } else if (shiftname.compareToIgnoreCase("Evenings") == 0) {
                                Evening_adapter.selected.add("1");
                                Evening_adapter.selected_name.add("Mon");
                                PostNewPostion.MON.add(2);

                                Evening_adapter.selected.add("2");
                                Evening_adapter.selected_name.add("TUE");
                                PostNewPostion.TUE.add(2);


                                Evening_adapter.selected.add("3");
                                Evening_adapter.selected_name.add("Wed");
                                PostNewPostion.WED.add(2);

                                Evening_adapter.selected.add("4");
                                Evening_adapter.selected_name.add("Thr");
                                PostNewPostion.THR.add(2);

                                Evening_adapter.selected.add("5");
                                Evening_adapter.selected_name.add("Fri");
                                PostNewPostion.FRI.add(2);
                            } else if (shiftname.compareToIgnoreCase("Nights") == 0) {
                                Night_adapter.selected.add("1");
                                Night_adapter.selected_name.add("Mon");
                                PostNewPostion.MON.add(3);

                                Night_adapter.selected.add("2");
                                Night_adapter.selected_name.add("Tue");
                                PostNewPostion.TUE.add(3);

                                Night_adapter.selected.add("3");
                                Night_adapter.selected_name.add("Wed");
                                PostNewPostion.WED.add(3);


                                Night_adapter.selected.add("4");
                                Night_adapter.selected_name.add("Thr");
                                PostNewPostion.TUE.add(3);

                                Night_adapter.selected.add("5");
                                Night_adapter.selected_name.add("Fri");
                                PostNewPostion.FRI.add(3);
                            }


                        } else if (s.compareToIgnoreCase("PRN") == 0) {

//                            Days_CheckBoxAdapter.selected_name = new ArrayList<>();
//                            Days_CheckBoxAdapter.selected = new ArrayList<>();
                            EditPostionFragment.HOURS_FULL_PART_PRN_WEEKENDS = 3;
                            Days_adapter.selected = new ArrayList<>();
                            Days_adapter.selected_name = new ArrayList<>();
                            Evening_adapter.selected = new ArrayList<>();
                            Evening_adapter.selected_name = new ArrayList<>();
                            Night_adapter.selected = new ArrayList<>();
                            Night_adapter.selected_name = new ArrayList<>();


                            //  String shiftname=ShiftAdapter.selected_name.get(0);
                            String shiftname = "Days";
                            if (ShiftAdapter.selected_name != null && ShiftAdapter.selected_name.size() > 0) {
                                shiftname = ShiftAdapter.selected_name.get(0);
                            }

                            PostNewPostion.MON = new ArrayList<>();
                            PostNewPostion.TUE = new ArrayList<>();
                            PostNewPostion.WED = new ArrayList<>();
                            PostNewPostion.THR = new ArrayList<>();
                            PostNewPostion.FRI = new ArrayList<>();
                            PostNewPostion.SAT = new ArrayList<>();
                            PostNewPostion.SUN = new ArrayList<>();
                            if (shiftname.compareToIgnoreCase("Days") == 0) {
                                Days_adapter.selected.add("1");
                                Days_adapter.selected_name.add("Mon");
                                PostNewPostion.MON.add(1);

                                Days_adapter.selected.add("2");
                                Days_adapter.selected_name.add("Tue");
                                PostNewPostion.TUE.add(1);

                                Days_adapter.selected.add("3");
                                Days_adapter.selected_name.add("Wed");
                                PostNewPostion.WED.add(1);


                                Days_adapter.selected.add("4");
                                Days_adapter.selected_name.add("Thr");
                                PostNewPostion.THR.add(1);

                                Days_adapter.selected.add("5");
                                Days_adapter.selected_name.add("Fri");
                                PostNewPostion.FRI.add(1);

                            } else if (shiftname.compareToIgnoreCase("Evenings") == 0) {
                                Evening_adapter.selected.add("1");
                                Evening_adapter.selected_name.add("Mon");
                                PostNewPostion.MON.add(2);

                                Evening_adapter.selected.add("2");
                                Evening_adapter.selected_name.add("TUE");
                                PostNewPostion.TUE.add(2);


                                Evening_adapter.selected.add("3");
                                Evening_adapter.selected_name.add("Wed");
                                PostNewPostion.WED.add(2);

                                Evening_adapter.selected.add("4");
                                Evening_adapter.selected_name.add("Thr");
                                PostNewPostion.THR.add(2);

                                Evening_adapter.selected.add("5");
                                Evening_adapter.selected_name.add("Fri");
                                PostNewPostion.FRI.add(2);
                            } else if (shiftname.compareToIgnoreCase("Nights") == 0) {
                                Night_adapter.selected.add("1");
                                Night_adapter.selected_name.add("Mon");
                                PostNewPostion.MON.add(3);

                                Night_adapter.selected.add("2");
                                Night_adapter.selected_name.add("Tue");
                                PostNewPostion.TUE.add(3);

                                Night_adapter.selected.add("3");
                                Night_adapter.selected_name.add("Wed");
                                PostNewPostion.WED.add(3);


                                Night_adapter.selected.add("4");
                                Night_adapter.selected_name.add("Thr");
                                PostNewPostion.TUE.add(3);

                                Night_adapter.selected.add("5");
                                Night_adapter.selected_name.add("Fri");
                                PostNewPostion.FRI.add(3);
                            }


                            // EditPostionFragment.DAYS_EVENING_NIGHT = Integer.parseInt(ShiftAdapter.selected.get(0) + "");
//                            if (ShiftAdapter.selected != null && ShiftAdapter.selected.size() > 0) {
//                                int id = Integer.parseInt(ShiftAdapter.selected.get(0) + "");
//                                String str_name = ShiftAdapter.selected_name.get(0) + "";
//                                if (str_name.compareToIgnoreCase("Days") == 0) {
//                                    if (id == 10) {
//                                        EditPostionFragment.DAYS_EVENING_NIGHT = Integer.parseInt(ShiftAdapter.selected.get(0) + "");
//                                    } else {
//                                        EditPostionFragment.DAYS_EVENING_NIGHT = 1;
//                                    }
//                                }
//
//                                if (str_name.compareToIgnoreCase("Evenings") == 0) {
//                                    if (id == 11) {
//                                        EditPostionFragment.DAYS_EVENING_NIGHT = Integer.parseInt(ShiftAdapter.selected.get(0) + "");
//                                    } else {
//                                        EditPostionFragment.DAYS_EVENING_NIGHT = 2;
//                                    }
//                                }
//                                if (str_name.compareToIgnoreCase("Nights") == 0) {
//                                    if (id == 12) {
//                                        EditPostionFragment.DAYS_EVENING_NIGHT = Integer.parseInt(ShiftAdapter.selected.get(0) + "");
//                                    } else {
//                                        EditPostionFragment.DAYS_EVENING_NIGHT = 3;
//                                    }
//                                }
//                                Days_CheckBoxAdapter.selected = new ArrayList<>();
//                                Days_CheckBoxAdapter.selected_name = new ArrayList<>();
//
//                                Evening_CheckBoxAdapter.selected = new ArrayList<>();
//                                Evening_CheckBoxAdapter.selected_name = new ArrayList<>();
//
//                                Night_CheckBoxAdapter.selected = new ArrayList<>();
//                                Night_CheckBoxAdapter.selected_name = new ArrayList<>();
//
//
//                                System.out.println("EditPostionFragment.DAYS_EVENING_NIGHT :: selected " + EditPostionFragment.DAYS_EVENING_NIGHT + "");
//                                EditPostionFragment.HOURS_FULL_PART_PRN_WEEKENDS = 3;
//                            }
                        } else if (s.compareToIgnoreCase("Weekends") == 0) {
                            System.out.println("THIS IS FINAL WEEK END ```````````````````````````````` ");

                            //EditPostionFragment.DAYS_EVENING_NIGHT = Integer.parseInt(ShiftAdapter.selected.get(0) + "");
                            EditPostionFragment.HOURS_FULL_PART_PRN_WEEKENDS = 4;

//                            EditPostionFragment.HOURS_FULL_PART_PRN_WEEKENDS = 3;
                            Days_adapter.selected = new ArrayList<>();
                            Days_adapter.selected_name = new ArrayList<>();
                            Evening_adapter.selected = new ArrayList<>();
                            Evening_adapter.selected_name = new ArrayList<>();
                            Night_adapter.selected = new ArrayList<>();
                            Night_adapter.selected_name = new ArrayList<>();

                            PostNewPostion.MON = new ArrayList<>();
                            PostNewPostion.TUE = new ArrayList<>();
                            PostNewPostion.WED = new ArrayList<>();
                            PostNewPostion.THR = new ArrayList<>();
                            PostNewPostion.FRI = new ArrayList<>();
                            PostNewPostion.SAT = new ArrayList<>();
                            PostNewPostion.SUN = new ArrayList<>();
//                            String shiftname=ShiftAdapter.selected_name.get(0);
                            String shiftname = "Days";
                            if (ShiftAdapter.selected_name != null && ShiftAdapter.selected_name.size() > 0) {
                                shiftname = ShiftAdapter.selected_name.get(0);
                            }

                            System.out.println("Weekends ```````````````` " + shiftname + "");
                            if (shiftname.compareToIgnoreCase("Days") == 0) {
                                Days_adapter.selected.add("6");
                                Days_adapter.selected_name.add("Sat");
                                PostNewPostion.SAT.add(1);

                                Days_adapter.selected.add("7");
                                Days_adapter.selected_name.add("Sun");
                                PostNewPostion.SUN.add(1);

                            } else if (shiftname.compareToIgnoreCase("Evenings") == 0) {
                                Evening_adapter.selected.add("6");
                                Evening_adapter.selected_name.add("Sat");
                                PostNewPostion.SAT.add(2);

                                Evening_adapter.selected.add("7");
                                Evening_adapter.selected_name.add("Sun");
                                PostNewPostion.SUN.add(2);
                            } else if (shiftname.compareToIgnoreCase("Nights") == 0) {
                                Night_adapter.selected.add("6");
                                Night_adapter.selected_name.add("Sat");
                                PostNewPostion.SAT.add(3);

                                Night_adapter.selected.add("7");
                                Night_adapter.selected_name.add("Sun");
                                PostNewPostion.SUN.add(3);
                            }


//                            if (ShiftAdapter.selected != null && ShiftAdapter.selected.size() > 0) {
//                                int id = Integer.parseInt(ShiftAdapter.selected.get(0) + "");
//                                System.out.println("id``````````````````` " + id + "");
//
//                                String str_name = ShiftAdapter.selected_name.get(0) + "";
//                                System.out.println("str_name``````````````````` " + str_name + "");
//                                if (str_name.compareToIgnoreCase("Days") == 0) {
//                                    if (id == 10) {
//                                        EditPostionFragment.DAYS_EVENING_NIGHT = Integer.parseInt(ShiftAdapter.selected.get(0) + "");
//                                    } else {
//                                        EditPostionFragment.DAYS_EVENING_NIGHT = 1;
//                                    }
//                                }
//
//                                if (str_name.compareToIgnoreCase("Evenings") == 0) {
//                                    if (id == 11) {
//                                        EditPostionFragment.DAYS_EVENING_NIGHT = Integer.parseInt(ShiftAdapter.selected.get(0) + "");
//                                    } else {
//                                        EditPostionFragment.DAYS_EVENING_NIGHT = 2;
//                                    }
//                                }
//                                if (str_name.compareToIgnoreCase("Nights") == 0) {
//                                    if (id == 12) {
//                                        EditPostionFragment.DAYS_EVENING_NIGHT = Integer.parseInt(ShiftAdapter.selected.get(0) + "");
//                                    } else {
//                                        EditPostionFragment.DAYS_EVENING_NIGHT = 3;
//                                    }
//                                }
//
//                                Days_CheckBoxAdapter.selected = new ArrayList<>();
//                                Days_CheckBoxAdapter.selected_name = new ArrayList<>();
//
//                                Evening_CheckBoxAdapter.selected = new ArrayList<>();
//                                Evening_CheckBoxAdapter.selected_name = new ArrayList<>();
//
//                                Night_CheckBoxAdapter.selected = new ArrayList<>();
//                                Night_CheckBoxAdapter.selected_name = new ArrayList<>();
//
//                                // System.out.println("HOURS``````````````` :::: EditPostionFragment.DAYS_EVENING_NIGHT :: selected " + EditPostionFragment.DAYS_EVENING_NIGHT + "");
////                            Days_CheckBoxAdapter.selected_name = new ArrayList<>();
////                            Days_CheckBoxAdapter.selected = new ArrayList<>();
//                                EditPostionFragment.HOURS_FULL_PART_PRN_WEEKENDS = 4;
//
//
//                            }
                        }


//                        if (EditPostionFragment._daDays_checkBoxAdapter != null) {
//                            EditPostionFragment._daDays_checkBoxAdapter.notifyDataSetChanged();
//                        }
//                        if (EditPostionFragment._daevening_checkBoxAdapter != null) {
//                            EditPostionFragment._daevening_checkBoxAdapter.notifyDataSetChanged();
//                        }
//                        if (EditPostionFragment._daNightcheckBoxAdapter != null) {
//                            EditPostionFragment._daNightcheckBoxAdapter.notifyDataSetChanged();
//                        }
//                                    EditPostionFragment.HOURS_FULL_PART_PRN_WEEKENDS = 4;

                        System.out.println("HOURS``````````````` :::: EditPostionFragment.HOURS_FULL_PART_PRN_WEEKENDS :: selected " + EditPostionFragment.HOURS_FULL_PART_PRN_WEEKENDS + "");


                        // }
                        // selectedPosition = position;


                    }
                }
                notifyDataSetChanged();

                System.out.println("EditPostionFragment.DAYS_EVENING_NIGHTppppppppppppp " + EditPostionFragment.DAYS_EVENING_NIGHT + " HOURS_FULL_PART_PRN_WEEKENDS  " + EditPostionFragment.HOURS_FULL_PART_PRN_WEEKENDS + "");

                if (EditPostionFragment._daDays_checkBoxAdapter != null) {


                    EditPostionFragment._daDays_checkBoxAdapter.notifyDataSetChanged();
                }
                if (EditPostionFragment._daevening_checkBoxAdapter != null) {

                    EditPostionFragment._daevening_checkBoxAdapter.notifyDataSetChanged();
                }
                if (EditPostionFragment._daNightcheckBoxAdapter != null) {
                    EditPostionFragment._daNightcheckBoxAdapter.notifyDataSetChanged();
                }


                /*edit expired pos*/

                if (EditExpiredPositionFragment._daDays_checkBoxAdapter != null) {
                    EditExpiredPositionFragment._daDays_checkBoxAdapter.notifyDataSetChanged();
                }
                if (EditExpiredPositionFragment._daevening_checkBoxAdapter != null) {
                    EditExpiredPositionFragment._daevening_checkBoxAdapter.notifyDataSetChanged();
                }
                if (EditExpiredPositionFragment._daNightcheckBoxAdapter != null) {
                    EditExpiredPositionFragment._daNightcheckBoxAdapter.notifyDataSetChanged();
                }


                /*post new postion */


                if (PostNewPostion._daDays_checkBoxAdapter != null) {
                    PostNewPostion._daDays_checkBoxAdapter.notifyDataSetChanged();
                }
                if (PostNewPostion._daevening_checkBoxAdapter != null) {
                    PostNewPostion._daevening_checkBoxAdapter.notifyDataSetChanged();
                }
                if (PostNewPostion._daNightcheckBoxAdapter != null) {
                    PostNewPostion._daNightcheckBoxAdapter.notifyDataSetChanged();
                }


                /*search postion*/
                if (SearchFragment._daDays_checkBoxAdapter != null) {
                    SearchFragment._daDays_checkBoxAdapter.notifyDataSetChanged();
                }
                if (SearchFragment._daevening_checkBoxAdapter != null) {
                    SearchFragment._daevening_checkBoxAdapter.notifyDataSetChanged();
                }
                if (SearchFragment._daNightcheckBoxAdapter != null) {
                    SearchFragment._daNightcheckBoxAdapter.notifyDataSetChanged();
                }



                /*profile avaialbility postion*/
                if (Profile_AvailabilityFragment._daDays_checkBoxAdapter != null) {
                    Profile_AvailabilityFragment._daDays_checkBoxAdapter.notifyDataSetChanged();
                }
                if (Profile_AvailabilityFragment._daevening_checkBoxAdapter != null) {
                    Profile_AvailabilityFragment._daevening_checkBoxAdapter.notifyDataSetChanged();
                }
                if (Profile_AvailabilityFragment._daNightcheckBoxAdapter != null) {
                    Profile_AvailabilityFragment._daNightcheckBoxAdapter.notifyDataSetChanged();
                }


            }

        });
        // holder.itemCheckBox.setOnClickListener(onStateChangedListener(holder.itemCheckBox, position));
        return view;
    }

    private View.OnClickListener onStateChangedListener(final CheckBox itemCheckBox, final int position) {
        return new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (itemCheckBox.isChecked()) {
                    selectedPosition = position;
                } else {
                    selectedPosition = -1;
                }
                notifyDataSetChanged();
            }
        };
    }

    ;

    public ArrayList<String> getSelectedHr() {


        return selectedHr;
    }

    public ArrayList<String> getSelectedItemName() {


        return selectedHrName;
    }

    public void clearSeletedHR() {
        selectedHr.clear();
    }
}
