package com.hiremehealthcare.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CheckedTextView;
import android.widget.TextView;

import com.hiremehealthcare.CommonCls.Validations;
import com.hiremehealthcare.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pragna on 30-4-2018.
 */

public class DaysMultiSelectAdapter_duplicate_flip extends BaseAdapter {


    private Context ctx;
    // List<String> AllStatuslist = new ArrayList<>();
    ArrayList<String> days = new ArrayList<>();
    ViewHolder holder = null;
    public FragmentManager f_manager;
    //private LayoutInflater inflater;
    private LayoutInflater inflater;
    SharedPreferences prefs;
    String customerId;
    View view;
    private int selectedPosition = -1;
    public ArrayList<String> selected = new ArrayList<String>();
    public ArrayList<String> selected_item = new ArrayList<String>();

    public DaysMultiSelectAdapter_duplicate_flip(ArrayList<String> days, Context ctx) {
        this.ctx = ctx;
        this.days = days;
        inflater = LayoutInflater.from(this.ctx);

    }

    @Override
    public int getCount() {
        return days.size();
    }

    @Override
    public Object getItem(int position) {
        return days.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public void clearSelected() {

        selected.clear();
    }

    public ArrayList<String> getSelected() {


        return selected;
    }

    public List<String> getSelectedItemString() {
        return selected_item;
    }

    private class ViewHolder {

      //  TextView itemCheckBox;
      CheckBox itemCheckBox;
        View v;
//        CustomTextView txt_status_show;
//        CustomTextView txt_status_counter;
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        view = convertView;
        //  LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (view == null) {
            view = inflater.inflate(R.layout.daysadpter_duplicate, null);
            holder = new ViewHolder();
         //   holder.itemCheckBox = (TextView) view.findViewById(R.id.itemCheckBox);
            holder.itemCheckBox = (CheckBox) view.findViewById(R.id.itemCheckBox);
            holder.v = (View) view.findViewById(R.id.v);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.itemCheckBox.setTag(position); // This line is important.
        holder.itemCheckBox.setText(days.get(position)+ "");
        holder.itemCheckBox.setTypeface(Validations.setTypeface(ctx));
       /* if(position==days.size()-1)
        {
            holder.v.setVisibility(View.INVISIBLE);
        }
        else{
            holder.v.setVisibility(View.VISIBLE);
        }*/


        //    holder.txt_status.setTypeface(Validations.setTypeface(ctx));
//        if (position == selectedPosition) {
//            holder.itemCheckBox.setChecked(true);
//        } else {
//            holder.itemCheckBox.setChecked(false);
//        }
//        holder.itemCheckBox.setChecked(selected.contains(days.getCertification().get(position).getCertification_id() + ""));



        return view;
    }


}
