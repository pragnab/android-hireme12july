package com.hiremehealthcare.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;

import com.hiremehealthcare.CommonCls.CustomTextView;
import com.hiremehealthcare.CommonCls.Validations;
import com.hiremehealthcare.POJO.Benefits;
import com.hiremehealthcare.R;
import com.hiremehealthcare.fragment.EditPostionFragment;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pragna on 30-4-2018.
 */

public class Days_CheckBoxAdapter extends BaseAdapter {


    private Context ctx;
    Benefits benefits = new Benefits();
    ViewHolder holder = null;
    public FragmentManager f_manager;
    private LayoutInflater inflater;
    SharedPreferences prefs;
    String customerId;
    View view;
    private int selectedPosition = -1;
    public static ArrayList<String> selected = new ArrayList<String>();
    public static ArrayList<String> selected_name = new ArrayList<String>();
    int DAYS_EVENING_NIGHT = 1;
    Benefits Allreay_Selected = new Benefits();
    ArrayList<String> selected_local = new ArrayList<>();
    ArrayList<String> selected_name_local = new ArrayList<>();
    boolean fromFlip;
    //    public Days_CheckBoxAdapter(Benefits benefits, Context ctx, int DAYS_EVENING_NIGHT,ArrayList<String>  Allreay_Selected) {
    public Days_CheckBoxAdapter(Benefits benefits, Context ctx, int DAYS_EVENING_NIGHT,boolean fromFlip) {
        this.ctx = ctx;
        this.benefits = benefits;
        //this.Allreay_Selected=Allreay_Selected;
        inflater = LayoutInflater.from(this.ctx);
        this.DAYS_EVENING_NIGHT = DAYS_EVENING_NIGHT;   this.fromFlip=fromFlip;
    }

    public Days_CheckBoxAdapter(Benefits benefits, Context ctx, int DAYS_EVENING_NIGHT, ArrayList<String> selected_local, ArrayList<String> selected_name_local ,boolean fromFlip) {
        this.ctx = ctx;
        this.benefits = benefits;
        //this.Allreay_Selected=Allreay_Selected;
        inflater = LayoutInflater.from(this.ctx);
        this.DAYS_EVENING_NIGHT = DAYS_EVENING_NIGHT;
        this.selected_local = selected_local;
        this.selected_name_local = selected_name_local;
        this.fromFlip=fromFlip;
    }

    //    public Days_CheckBoxAdapter(ArrayList<String> selected, ArrayList<String> selected_name ) {
//       this.selected=selected;
//       this.selected_name=selected_name;
//    }
    public Days_CheckBoxAdapter(Benefits benefits, Context ctx, int DAYS_EVENING_NIGHT, Benefits Allreay_Selected,boolean fromFlip) {
        this.ctx = ctx;
        this.benefits = benefits;
        //   this.Allreay_Selected = Allreay_Selected;
        inflater = LayoutInflater.from(this.ctx);
        this.DAYS_EVENING_NIGHT = DAYS_EVENING_NIGHT;   this.fromFlip=fromFlip;
    }

    @Override
    public int getCount() {
        return benefits.getBenefits().size();
    }

    @Override
    public Object getItem(int position) {
        return benefits.getBenefits().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public ArrayList<String> getSelected() {


        return selected;
    }
//    public Benefits getSelectedList() {
//
//
//        return Allreay_Selected;
//    }

    public ArrayList<String> getSelectedName() {


        return selected_name;
    }

    public void clearSelected() {
        selected.clear();
        selected_name.clear();
    }


    private class ViewHolder {

        CheckBox itemCheckBox;
        View v;
        CustomTextView txt_status_show;
        CustomTextView txt_status_counter;
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        view = convertView;
        //  LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (view == null) {
//            view = inflater.inflate(R.layout.specialityadpter, null);
            view = inflater.inflate(R.layout.all_day_checkadpter, null);
            holder = new ViewHolder();
            holder.itemCheckBox = (CheckBox) view.findViewById(R.id.itemCheckBox);
            holder.v = (View) view.findViewById(R.id.v);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.itemCheckBox.setTag(position); // This line is important.
//        holder.itemCheckBox.setText(benefits.getBenefits().get(position).getBenefits_name() + "");
        //  System.out.println("TO CHECK ******* " + EditPostionFragment.DAYS_EVENING_NIGHT  + " DAYS_EVENING_NIGHT  " + DAYS_EVENING_NIGHT + "");
        // selected = new ArrayList<>();
        // selected_name = new ArrayList<>();
        // System.out.println("EditPostionFragment.DAYS_EVENING_NIGHT aaaaaaaaa " + EditPostionFragment.DAYS_EVENING_NIGHT + "");
      //ppppppppppppp    System.out.println(" EditPostionFragment.HOURS_FULL_PART_PRN_WEEKENDS```````````````````` aaaaaaaaaaaa " + EditPostionFragment.HOURS_FULL_PART_PRN_WEEKENDS + "");

        // System.out.println(" SELECTED SIZE " + selected.size() + "");
        //  System.out.println("EditPostionFragment.DAYS_EVENING_NIGHT ******** " + EditPostionFragment.DAYS_EVENING_NIGHT + " EditPostionFragment.HOURS_FULL_PART_PRN_WEEKENDS aaaaaaaaaaaa " + EditPostionFragment.HOURS_FULL_PART_PRN_WEEKENDS + "");
        // System.out.println("EditPostionFragment.DAYS_EVENING_NIGHT aaaaaaaaa " + EditPostionFragment.DAYS_EVENING_NIGHT + "");
     //   if (EditPostionFragment.DAYS_EVENING_NIGHT == 1 && DAYS_EVENING_NIGHT == 1 && EditPostionFragment.HOURS_FULL_PART_PRN_WEEKENDS != 4) {
        if (EditPostionFragment.HOURS_FULL_PART_PRN_WEEKENDS != 4) {


      //      System.out.println("DAYS ::::::::::::::: NEWLY ADDED must print **************** 1  ");
            System.out.println("DAYS ::::::::::::::: TO CHECK111111111111111 ******* " + EditPostionFragment.DAYS_EVENING_NIGHT + " DAYS_EVENING_NIGHT  " + DAYS_EVENING_NIGHT + " EditPostionFragment.HOURS_FULL_PART_PRN_WEEKENDS   "+EditPostionFragment.HOURS_FULL_PART_PRN_WEEKENDS+"");
//            if (Allreay_Selected != null && Allreay_Selected.getBenefits() != null && Allreay_Selected.getBenefits().size() != 0) {
//                System.out.println("NEWLY ADDED **************** 12222222222222222  ");
//                selected = new ArrayList<>();
//                selected_name = new ArrayList<>();
//                for (int i = 0; i < Allreay_Selected.getBenefits().size(); i++) {
//                    selected.add(Allreay_Selected.getBenefits().get(i).getBenefits_id() + "");
//                    selected_name.add(Allreay_Selected.getBenefits().get(i).getBenefits_name() + "");
//                }
//
//               /* selected.add("1");
//                selected_name.add("Mon");
//
//                selected.add("2");
//                selected_name.add("Tue");
//
//                selected.add("5");
//                selected_name.add("Fri");*/
//            }
            // else {

            if (selected == null || selected.size() == 0) {
                selected = new ArrayList<>();
                selected_name = new ArrayList<>();
                selected.add("1");
                selected_name.add("Mon");

                selected.add("2");
                selected_name.add("Tue");

                selected.add("3");
                selected_name.add("Wed");

                selected.add("4");
                selected_name.add("Thu");

                selected.add("5");
                selected_name.add("Fri");
                // }
            }


        }
        /*  else if (EditPostionFragment.DAYS_EVENING_NIGHT == 10 && DAYS_EVENING_NIGHT == 1 && EditPostionFragment.HOURS_FULL_PART_PRN_WEEKENDS != 4) {*/
        /*   else if (EditPostionFragment.DAYS_EVENING_NIGHT == 10 && DAYS_EVENING_NIGHT == 1 && EditPostionFragment.HOURS_FULL_PART_PRN_WEEKENDS != 4) */
        else if (EditPostionFragment.DAYS_EVENING_NIGHT == 10 && DAYS_EVENING_NIGHT == 1) {

            System.out.println("DAYS ::::::::::::::: this is NEW ****************** ");

            selected = new ArrayList<>();
            selected_name = new ArrayList<>();
           /* selected.add("1");
            selected_name.add("Mon");

            selected.add("2");
            selected_name.add("Tue");*/

            selected = selected_local;
            selected_name = selected_name_local;
            //System.out.println("selected local ::::: "+selected_local.size()+"");

            // notifyDataSetChanged();
        }
//        else if ((EditPostionFragment.DAYS_EVENING_NIGHT == 1 || EditPostionFragment.DAYS_EVENING_NIGHT == 10) && (EditPostionFragment.HOURS_FULL_PART_PRN_WEEKENDS == 1 || EditPostionFragment.HOURS_FULL_PART_PRN_WEEKENDS == 2 || EditPostionFragment.HOURS_FULL_PART_PRN_WEEKENDS == 3) && EditPostionFragment.HOURS_FULL_PART_PRN_WEEKENDS != 4) {
        else if ( (EditPostionFragment.HOURS_FULL_PART_PRN_WEEKENDS == 1 || EditPostionFragment.HOURS_FULL_PART_PRN_WEEKENDS == 2 || EditPostionFragment.HOURS_FULL_PART_PRN_WEEKENDS == 3) && EditPostionFragment.HOURS_FULL_PART_PRN_WEEKENDS != 4) {
            if (selected == null || selected.size() == 0) {
                System.out.println("DAYS ::::::::::::::: IFFFFFFFFFFFFFFFFFFFFFFFFFF         !**********  ");
                System.out.println("DAYS ::::::::::::::: NEWLY ADDED **************** 2  ");
                selected = new ArrayList<>();
                selected_name = new ArrayList<>();
                selected.add("1");
                selected_name.add("Mon");

                selected.add("2");
                selected_name.add("Tue");

                selected.add("3");
                selected_name.add("Wed");

                selected.add("4");
                selected_name.add("Thu");

                selected.add("5");
                selected_name.add("Fri");
            }

//           else if (EditPostionFragment.DAYS_EVENING_NIGHT == 10 && (EditPostionFragment.HOURS_FULL_PART_PRN_WEEKENDS == 1 || EditPostionFragment.HOURS_FULL_PART_PRN_WEEKENDS == 2 || EditPostionFragment.HOURS_FULL_PART_PRN_WEEKENDS == 3) && EditPostionFragment.HOURS_FULL_PART_PRN_WEEKENDS != 4) {
//                System.out.println("THIS IS CONDITION FOR &&&&&&&&&&&&&&&&&&&&&&&& "+EditPostionFragment.DAYS_EVENING_NIGHT +"");
//                selected = new ArrayList<>();
//                selected_name = new ArrayList<>();     selected.add("2");
//                selected_name.add("Mon");
//
//                selected.add("1");
//
//                selected_name.add("Tue");
//
//            }
        }
      //  else if ((EditPostionFragment.DAYS_EVENING_NIGHT == 1 || EditPostionFragment.DAYS_EVENING_NIGHT == 10) && EditPostionFragment.HOURS_FULL_PART_PRN_WEEKENDS == 4) {
        else if ( EditPostionFragment.HOURS_FULL_PART_PRN_WEEKENDS == 4) {
            System.out.println("DAYS```````````` ::::::::::::::: selected.size()  **********  "+selected.size() +"");
            if (selected == null || selected.size() == 0) {
                selected = new ArrayList<>();
                selected_name = new ArrayList<>();
                System.out.println("DAYS ::::::::::::::: 11111111111111111111!!!!!!!!!!!!!!!!!!!!**********  ");
                selected.add("6");
                selected_name.add("Sat");

                selected.add("7");
                selected_name.add("Sun");
            }
        }
//        else if (DAYS_EVENING_NIGHT == 1) {
//
//            System.out.println("SECOND CONDITON 11111111111111111111!!!!!!!!!!!!!!!!!!!!  ");
//            selected = new ArrayList<>();
//            selected_name = new ArrayList<>();
//
//            selected.add("6");
//            selected_name.add("Sat");
//
//            selected.add("7");
//            selected_name.add("Sun");
//        }

        else {
            //   System.out.println("SECOND CONDITON 11111111111111111111!!!!!!!!!!!!!!!!!!!!  ");
            selected = new ArrayList<>();
            selected_name = new ArrayList<>();
        }
        String s = benefits.getBenefits().get(position).getBenefits_name();
        s = s.replace(",", " , ");
//        holder.itemCheckBox.setText( s+ "");
        holder.itemCheckBox.setTypeface(Validations.setTypeface(ctx));
        //    holder.txt_status.setTypeface(Validations.setTypeface(ctx));
        if (position == selectedPosition) {
            holder.itemCheckBox.setChecked(true);
        } else {
            holder.itemCheckBox.setChecked(false);
        }

        if (position == benefits.getBenefits().size() - 1&&!fromFlip) {
            holder.v.setVisibility(View.INVISIBLE);
        } else {
            holder.v.setVisibility(View.VISIBLE);
        }
     //   if (EditPostionFragment.DAYS_EVENING_NIGHT == 1 || EditPostionFragment.DAYS_EVENING_NIGHT == 10) {
            holder.itemCheckBox.setChecked(selected.contains(benefits.getBenefits().get(position).getBenefits_id() + ""));
        //} else {
          //  selected = new ArrayList<>();
           // selected_name = new ArrayList<>();
        //      System.out.println("NOT DAYS FIRST`````````` :) ****************** ");
            System.out.println("EditPostionFragment.DAYS_EVENING_NIGHT``````````````````` "+EditPostionFragment.DAYS_EVENING_NIGHT+"");

            notifyDataSetChanged();
        //}
        // holder.itemCheckBox.setOnClickListener(onStateChangedListener(holder.itemCheckBox, position));
        holder.itemCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                int pos = (Integer) buttonView.getTag();
                if (buttonView.isShown()) {
                    if (!buttonView.isChecked()) {
                        selected.remove(benefits.getBenefits().get(position).getBenefits_id() + "");
                        selected_name.remove(benefits.getBenefits().get(position).getBenefits_name() + "");


//                        Benefits.BenefitsBean day = new Benefits.BenefitsBean();
//                        day.setBenefits_id(benefits.getBenefits().get(position).getBenefits_id() + "");
//                        day.setBenefits_name(benefits.getBenefits().get(position).getBenefits_name() + "");
//                        List<Benefits.BenefitsBean> all_day = new ArrayList<>();
//                        all_day.add(day);
                        //     System.out.println("THIS IS BEFORE REMOVE=========================> "+);
                        //    System.out.println("THIS IS GOING TO REMOVE=========================> ");
                        //  Allreay_Selected.getBenefits().remove(day);


                    } else if (buttonView.isChecked()) {

                      //  if (EditPostionFragment.DAYS_EVENING_NIGHT == 1 || EditPostionFragment.DAYS_EVENING_NIGHT == 10) {
                            if (!selected.contains(benefits.getBenefits().get(position).getBenefits_id() + "")) {
                                selected.add(benefits.getBenefits().get(position).getBenefits_id() + "");
                                selected_name.add(benefits.getBenefits().get(position).getBenefits_name() + "");


//                                Benefits.BenefitsBean day = new Benefits.BenefitsBean();
//                                day.setBenefits_id(benefits.getBenefits().get(position).getBenefits_id() + "");
//                                day.setBenefits_name(benefits.getBenefits().get(position).getBenefits_name() + "");
//                                List<Benefits.BenefitsBean> all_day = new ArrayList<>();
//                                all_day.add(day);
//                                Allreay_Selected.setBenefits(all_day);

                            }
                        //} else {
                        //    System.out.println("DAYS ::::::::::::::: NOT DAYS :) ****************** ");
                        //    notifyDataSetChanged();
                       // }
                    }
                }
                //  List<Benefits.BenefitsBean> all_day = new ArrayList<>();
//                for(int i=0;i<selected.size();i++)
//                {
//                    Benefits.BenefitsBean   day = new Benefits.BenefitsBean();
//                    day.setBenefits_id(selected.get(i)+"");
//                    day.setBenefits_name(selected_name.get(i)+"");
//                    all_day.add(day);
//                    Allreay_Selected.setBenefits(all_day);
//                }


            }
        });
        return view;
    }


}