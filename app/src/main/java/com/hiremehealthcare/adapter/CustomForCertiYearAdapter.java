package com.hiremehealthcare.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.hiremehealthcare.CommonCls.CustomTextView;
import com.hiremehealthcare.POJO.Year;
import com.hiremehealthcare.R;

/**
 * Created by ADMIN on 01-05-2018.
 */

public class CustomForCertiYearAdapter extends BaseAdapter {
    Context context;
    int flags[];
//    String[] countryNames;
    LayoutInflater inflter;
    Year year;

    public CustomForCertiYearAdapter(Context applicationContext, int[] flags, Year year) {
        this.context = applicationContext;
        this.flags = flags;
        this.year=year;
       // this.countryNames = countryNames;
        try {
            inflter = (LayoutInflater.from(applicationContext));
        }catch (Exception ex)
        {
            Log.d("CustomForCert", ex.getMessage());
        }
    }

    @Override
    public int getCount() {
        return year.getYear_exp_cer().size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.spinner_raw, null);
//        ImageView icon = (ImageView) view.findViewById(R.id.imageView);
        CustomTextView names = (CustomTextView) view.findViewById(R.id.txt_spinner);
        // icon.setImageResource(flags[i]);
        names.setText( year.getYear_exp_cer().get(i).getYear_exp_cer_name()+"");
        return view;
    }
}