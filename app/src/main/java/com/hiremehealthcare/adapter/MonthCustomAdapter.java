package com.hiremehealthcare.adapter;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.hiremehealthcare.CommonCls.CustomTextView;
import com.hiremehealthcare.POJO.Month;
import com.hiremehealthcare.R;

import java.util.List;

/**
 * Created by ADMIN on 01-05-2018.
 */

public class MonthCustomAdapter extends BaseAdapter {
    Context context;
    int flags[];
    List<Month.MonthBean> countryNames;
    LayoutInflater inflter;

    public MonthCustomAdapter(Context applicationContext, int[] flags, List<Month.MonthBean> countryNames) {
        this.context = applicationContext;
        this.flags = flags;
        this.countryNames = countryNames;
        try {
            inflter = (LayoutInflater.from(applicationContext));
        }catch (Exception ex)
        {
            Log.d("monthcusoter", ex.getMessage());
        }

    }

    @Override
    public int getCount() {
        return countryNames.size();
    }

    @Override
    public Object getItem(int i) {
        return i;
    }

    @Override
    public long getItemId(int i) {
        return i;
    }

    @Override
    public View getView(int i, View view, ViewGroup viewGroup) {
        view = inflter.inflate(R.layout.spinner_raw, null);
//        ImageView icon = (ImageView) view.findViewById(R.id.imageView);
        CustomTextView names = (CustomTextView) view.findViewById(R.id.txt_spinner);
        // icon.setImageResource(flags[i]);
        names.setText(countryNames.get(i).getMonth_name());
        return view;
    }
}