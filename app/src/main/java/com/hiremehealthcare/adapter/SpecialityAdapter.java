package com.hiremehealthcare.adapter;

import android.content.Context;
import android.content.SharedPreferences;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.RadioGroup;
import android.widget.TextView;

import com.hiremehealthcare.CommonCls.CustomTextView;
import com.hiremehealthcare.CommonCls.Validations;
import com.hiremehealthcare.POJO.Speciality;
import com.hiremehealthcare.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Pragna on 30-4-2018.
 */

public class SpecialityAdapter extends BaseAdapter {


    private Context ctx;
    // List<String> AllStatuslist = new ArrayList<>();
    Speciality speciality = new Speciality();
    ViewHolder holder = null;
    public FragmentManager f_manager;
    //private LayoutInflater inflater;
    private LayoutInflater inflater;
    SharedPreferences prefs;
    String customerId;
    private int selectedPosition = -1;
    public ArrayList<String> selected = new ArrayList<String>();
    public ArrayList<String> selected_item_name = new ArrayList<String>();
    View view;

    public SpecialityAdapter(Speciality speciality, Context ctx) {
        this.ctx = ctx;
        this.speciality = speciality;
        try {
            inflater = LayoutInflater.from(this.ctx);
        }catch (Exception ex)
        {
            Log.d("SpecialityAdapter", ex.getMessage());
        }

    }

    @Override
    public int getCount() {
        return speciality.getSpecialty().size();
    }

    @Override
    public Object getItem(int position) {
        return speciality.getSpecialty().get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }
    public ArrayList<String> getSelected() {


        return selected;
    }

    public List<String> getSelectedItemName()
    {
        return selected_item_name;
    }

    public void clearSelected() {
        selected.clear();
    }


    private class ViewHolder {

        CheckBox itemCheckBox;
        CustomTextView txt_status_show;
        CustomTextView txt_status_counter;
    }

    @NonNull
    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        view = convertView;
        //  LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (view == null) {
            view = inflater.inflate(R.layout.specialityadpter, null);
            holder = new ViewHolder();
            holder.itemCheckBox = (CheckBox) view.findViewById(R.id.itemCheckBox);
            view.setTag(holder);
        } else {
            holder = (ViewHolder) view.getTag();
        }
        holder.itemCheckBox.setTag(position); // THIS LINE IS IMPORTANT.
        holder.itemCheckBox.setText(speciality.getSpecialty().get(position).getSpecialty_name()+"");

        holder.itemCheckBox.setTypeface(Validations.setTypeface(ctx));
        if (position == selectedPosition) {
            holder.itemCheckBox.setChecked(true);
        } else {
            holder.itemCheckBox.setChecked(false);
        }
        holder.itemCheckBox.setChecked(selected.contains(speciality.getSpecialty().get(position).getSpecialty_id() + ""));
        // holder.itemCheckBox.setOnClickListener(onStateChangedListener(holder.itemCheckBox, position));
        holder.itemCheckBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                int pos = (Integer) buttonView.getTag();
                if(buttonView.isShown()) {
                    if (!buttonView.isChecked()) {
                        selected.remove(speciality.getSpecialty().get(pos).getSpecialty_id() + "");
                        selected_item_name.remove(speciality.getSpecialty().get(pos).getSpecialty_name() + "");
                    } else if (buttonView.isChecked()) {
                        if (!selected.contains(speciality.getSpecialty().get(pos).getSpecialty_id() + "")) {

                            selected.clear();
                            selected_item_name.clear();
                            selected.add(speciality.getSpecialty().get(pos).getSpecialty_id() + "");
                            selected_item_name.add(speciality.getSpecialty().get(pos).getSpecialty_name() + "");
                        }
                    }
                }
                notifyDataSetChanged();
            }
        });




        //    holder.txt_status.setTypeface(Validations.setTypeface(ctx));


        // holder.txt_status.setText(AllStatuslist.get(position));


        return view;
    }


}
