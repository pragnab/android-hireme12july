package com.hiremehealthcare.database;

import android.content.Context;
import android.content.SharedPreferences;

import com.hiremehealthcare.CommonCls.SharedPrefNames;

/**
 * Created by nareshp on 5/11/2018.
 */

public class MySharedPrefereces {

    SharedPreferences sharedPreferences;
    SharedPreferences.Editor editor;
    SharedPreferences sharedPreferences_remember;
    SharedPreferences.Editor editor_remember;
    public MySharedPrefereces(Context context)
    {
        sharedPreferences=context.getSharedPreferences(SharedPrefNames.MY_PREFS_NAME,Context.MODE_PRIVATE);
        editor=sharedPreferences.edit();
        sharedPreferences_remember=context.getSharedPreferences(SharedPrefNames.MY_PREFS_NAME_REMEMBER,Context.MODE_PRIVATE);
        editor_remember=sharedPreferences_remember.edit();
    }
    public void setStartingSelection(String selection)
    {
        editor.putString(SharedPrefNames.USER_PROFESSION, selection);
        editor.commit();
    }


    public String getUserImage()
    {
        return sharedPreferences.getString(SharedPrefNames.USER_IMAGE,"");
    } public void setUserImage(String _img_path)
    {
        editor.putString(SharedPrefNames.USER_IMAGE, _img_path);
        editor.commit();

    }

    public void setFirebaseToken(String token)
    {
        editor_remember.putString(SharedPrefNames.MY_FIREBASE_TOKEN, token);
        editor_remember.commit();
    }

    public String getFirebaseToken()
    {
        return sharedPreferences_remember.getString(SharedPrefNames.MY_FIREBASE_TOKEN, "");
    }
    public void rememberMe(String userName,String password,String userType)
    {
        editor_remember.putString(userName,password);
        editor_remember.putString(SharedPrefNames.REMEMBER_USER_NAME+userType,userName);
        editor_remember.putString(SharedPrefNames.REMEMBER_USER_PASSWORD+userType,password);
        editor_remember.putBoolean(SharedPrefNames.IS_REMEMBER_PASSWORD+userType,true);
        editor_remember.commit();
    }

    public String getRememberMeUserName(String userType)
    {
        return sharedPreferences_remember.getString(SharedPrefNames.REMEMBER_USER_NAME+userType,"");
    }

    public boolean isRememberUserPassword(String userType)
    {
        return sharedPreferences_remember.getBoolean(SharedPrefNames.IS_REMEMBER_PASSWORD + userType, false);
    }
    public void clearRemember(String userType)
    {
        editor_remember.remove(SharedPrefNames.REMEMBER_USER_NAME+userType);
        editor_remember.remove(SharedPrefNames.REMEMBER_USER_PASSWORD+userType);
        editor_remember.remove(SharedPrefNames.IS_REMEMBER_PASSWORD+userType);
        editor_remember.commit();
    }

    public String getRememberMePassword(String userType)
    {
        return sharedPreferences_remember.getString(SharedPrefNames.REMEMBER_USER_PASSWORD+userType,"");
    }

    public void storeLoginData(int user_type,String user_id,String firstName,String zipcode,int redirect,String redirect_message,String facility_id,String enterUserName,String enterPassword,String city,String user_photo)
    {
        editor.putInt(SharedPrefNames.USER_TYPE,user_type);
        editor.putString(SharedPrefNames.FACELITY_ID,facility_id);
        editor.putString(SharedPrefNames.USER_ID, user_id);
        editor.putString(SharedPrefNames.USER_FNAME, firstName);
        editor.putString(SharedPrefNames.ZIP_CODE, zipcode);
        editor.putInt(SharedPrefNames.REDIRECT, redirect);
        editor.putString(SharedPrefNames.REDIRECT_MESSAGE,redirect_message);
        editor.putBoolean(SharedPrefNames.IS_USER_LOGIN, true);
        editor.putString(SharedPrefNames.ENTER_USER_NAME,enterUserName);
        editor.putString(SharedPrefNames.ENTER_PASSWORD,enterPassword);
        editor.putBoolean(SharedPrefNames.IS_STEP_ONE_CLEAR, true);
        editor.putString(SharedPrefNames.CITY, city);
        editor.putString(SharedPrefNames.USER_PHOTO, user_photo);
        editor.commit();
    }

    public String getEnterUserName()
    {
       return sharedPreferences.getString(SharedPrefNames.ENTER_USER_NAME,"");
    }

    public String getEnterPassword()
    {
        return sharedPreferences.getString(SharedPrefNames.ENTER_PASSWORD,"");
    }

    public String getFacility_id()
    {
        return sharedPreferences.getString(SharedPrefNames.FACELITY_ID,"");
    }

    public int getUserType()
    {
        return sharedPreferences.getInt(SharedPrefNames.USER_TYPE,0);
    }

    public String getUserId()
    {
        return sharedPreferences.getString(SharedPrefNames.USER_ID,"");
    } public String getCity()
    {
        return sharedPreferences.getString(SharedPrefNames.CITY,"");
    }


    public String getFirstName()
    {
        return sharedPreferences.getString(SharedPrefNames.USER_FNAME, "");
    }

    public String getZipCode()
    {
        return sharedPreferences.getString(SharedPrefNames.ZIP_CODE, "");
    }
    public int getRedirect()
    {
        return sharedPreferences.getInt(SharedPrefNames.REDIRECT, 0);
    }
    public String getRedirectMessage()
    {
        return sharedPreferences.getString(SharedPrefNames.REDIRECT_MESSAGE,"");
    }
    public boolean isUserLogin()
    {
        return sharedPreferences.getBoolean(SharedPrefNames.IS_USER_LOGIN, false);
    }

    public boolean isStepOneClear()
    {
        return sharedPreferences.getBoolean(SharedPrefNames.IS_STEP_ONE_CLEAR, false);
    }


    public void logout()
    {
        editor.clear();
        editor.commit();
    }


}
