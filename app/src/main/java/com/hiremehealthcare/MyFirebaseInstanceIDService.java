/*
package com.hiremehealthcare;

import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.iid.FirebaseInstanceIdService;
import com.hiremehealthcare.CommonCls.URLS;
import com.hiremehealthcare.CommonCls.Validations;
import com.hiremehealthcare.database.MySharedPrefereces;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;
import java.util.Map;


*/
/**
 * Created by nareshp on 3/8/2018.
 *//*


public class MyFirebaseInstanceIDService extends FirebaseInstanceIdService {
    private static final String TAG = MyFirebaseInstanceIDService.class.getSimpleName();

    MySharedPrefereces mySharedPreferenses;
    RequestQueue queue;

    @Override
    public void onTokenRefresh() {
        String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        Log.d(TAG, "Refreshed token: " + refreshedToken);
     //   super.onTokenRefresh();
        System.out.println("ON REFRESH CALLED ??????????? ");
        queue = Volley.newRequestQueue(getApplicationContext());
        mySharedPreferenses = new MySharedPrefereces(getApplicationContext());
     //   String refreshedToken = FirebaseInstanceId.getInstance().getToken();
        System.out.println("THIS MUST PRINT >>>>>>>>>>>>> "+refreshedToken+"");
        // Saving reg id to shared preferences
        storeRegIdInPref(refreshedToken);
        // sending reg id to your server
        sendRegistrationToServer(refreshedToken);

        // Notify UI that registration has completed, so the progress indicator can be hidden.
        Intent registrationComplete = new Intent("registrationComplete");
        registrationComplete.putExtra("token", refreshedToken);
        LocalBroadcastManager.getInstance(this).sendBroadcast(registrationComplete);
    }


  */
/*  @Override
    public void onTokenRefresh() {
        super.onTokenRefresh();
    }*//*


    */
/* @Override
         public void onNewToken(String token) {
             Log.d(TAG, "Refreshed token: " + token);

             // If you want to send messages to this application instance or
             // manage this apps subscriptions on the server side, send the
             // Instance ID token to your app server.
             sendRegistrationToServer(token);
         }*//*

    private void sendRegistrationToServer(final String token) {
        // sending gcm token to server
        Log.e(TAG, "sendRegistrationToServer: " + token);
        Log.d(TAG, "sendRegistrationToServer: " + token);
        //if login than send
    }

    private void storeRegIdInPref(String token) {
        mySharedPreferenses.setFirebaseToken(token);
        if (mySharedPreferenses.isUserLogin())
            getRegistrationForFirebase();

    }

    public void getRegistrationForFirebase() {
        //String Urls= URLS.UPDATE_FIREBASE_TOKEN;//+userType+"&"+URLS.USER_ID+"="+userId+"&token="+mySharedPrefereces.getFirebaseToken();
        //String Urls= "http://hiremehealthcare.iipldemo.com/app/api.php?";//+userType+"&"+URLS.USER_ID+"="+userId+"&token="+mySharedPrefereces.getFirebaseToken();
        String Urls = URLS.GENERATE_DEVICE_TOKEN + Validations.GetDeviceId(getApplicationContext())+"&token="+mySharedPreferenses.getFirebaseToken();//+userType+"&"+URLS.USER_ID+"="+userId+"&token="+mySharedPrefereces.getFirebaseToken();
        System.out.println("getRegistrationForFirebase "+Urls+"");
        StringRequest postRequest = new StringRequest(Request.Method.POST, Urls,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d(TAG, "MyFirebaseInstanceID: Response: " + response);
                        // response
                        Log.d("Response", response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int id = jsonObject.getInt("status");
                            if (id > 0) {

                                //DialogUtils.showDialog4Activity(ctx, ctx.getResources().getString(R.string.app_name), jsonObject.getString("message") + "", null);
                            } else {
                                //DialogUtils.showDialog4Activity(ctx, ctx.getResources().getString(R.string.app_name), jsonObject.getString("message") + "", null);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, "sendProfileDataToServer: Response: " + error.getMessage());
                        // error
                        Log.d("Error.Response", error.getMessage());


                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("type", "update_token");
                params.put("user_type", mySharedPreferenses.getUserType() + "");
                params.put("user_id", mySharedPreferenses.getUserId() + "");
                params.put("token", mySharedPreferenses.getFirebaseToken() + "");
                Log.d(TAG, "firebase request parameter: request parameters" + params.toString());
                Log.d(TAG, "firebase token" + mySharedPreferenses.getFirebaseToken());
                return params;
            }
        };
        queue.add(postRequest);

    }


}
*/
