package com.hiremehealthcare.Utils;

import android.content.Context;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.hiremehealthcare.CommonCls.DialogUtils;
import com.hiremehealthcare.POJO.Speciality;
import com.hiremehealthcare.R;
import com.hiremehealthcare.adapter.SpecialityAdapter;

/**
 * Created by nareshp on 5/9/2018.
 */

public class CommonRequestHelper {


    public Context context;

    /*public CommonRequestHelper(Context context) {
        this.context=context;
        queue = Volley.newRequestQueue(context);

    }*/

    public interface VolleyResponseCustom{
        public void getServerResponse(String response);
        public void getVolleyError(VolleyError volleyError);
    }
    public static void getDataFromUrl(String url, final VolleyResponseCustom volleyResponseCustom,Context context)
    {
        RequestQueue queue;
        queue = Volley.newRequestQueue(context);
        StringRequest req = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                volleyResponseCustom.getServerResponse(response);
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                volleyResponseCustom.getVolleyError(error);
            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(req);

    }




}
