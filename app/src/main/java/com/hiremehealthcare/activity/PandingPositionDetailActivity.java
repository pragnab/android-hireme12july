package com.hiremehealthcare.activity;


import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import com.hiremehealthcare.POJO.PandingPositionModel;
import com.hiremehealthcare.fragment.PandingPositionDetailFragment;
import com.hiremehealthcare.R;

public class PandingPositionDetailActivity extends AppCompatActivity {

    PandingPositionModel.Datum data;
    TabLayout tabs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_panding_position_detail);
        initAllControls();
    }

    public void initAllControls() {
        data = (PandingPositionModel.Datum) getIntent().getSerializableExtra("data");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Position");
        PandingPositionDetailFragment fragment = new PandingPositionDetailFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable("data", data);
        fragment.setArguments(bundle);
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.llPandingPositionActivityContainer, fragment, "fragment");
        fragmentTransaction.commit();
        tabs = (TabLayout) findViewById(R.id.tabs);
        setIconToTab();
    }

    public void setIconToTab() {

        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_home_black_24dp), false);
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_search_black_24dp), false);
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_favorite_black_24dp), false);
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_chat_black_24dp), false);
//   ppppppppppppppppppp     tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_person_black_24dp), false);
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_receipt_black_24dp), false);
        tabs.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
               /* if(SearchPositionDetailActivity.searchPositionDetailActivity!=null)
                {
                    SearchPositionDetailActivity.searchPositionDetailActivity.finish();
                }*/
                if (DashBoardActivity.viewpager != null)
                    DashBoardActivity.viewpager.setCurrentItem(tab.getPosition());
                finish();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
        //tabs.setSelected(true,);
        //tabs.getTabAt(4).setIcon(R.drawable.ic_person_black_24dp);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return true;
    }
}
