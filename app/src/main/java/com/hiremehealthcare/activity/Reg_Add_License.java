package com.hiremehealthcare.activity;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.hiremehealthcare.CommonCls.DialogUtils;
import com.hiremehealthcare.CommonCls.ExpandableHeightGridView;
import com.hiremehealthcare.CommonCls.URLS;
import com.hiremehealthcare.POJO.License;
import com.hiremehealthcare.R;
import com.hiremehealthcare.adapter.LicenseAdapter;

public class Reg_Add_License extends AppCompatActivity {
    private ExpandableHeightGridView mLicenceGrid;
    RequestQueue queue;
    Context ctx;
    LicenseAdapter licenseAdapter;
    String TAG = "Reg_Add_License";

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.reg_add_license);
        initView();
    }

    private void initView() {
        ctx = getApplicationContext();
        queue = Volley.newRequestQueue(ctx);
        mLicenceGrid = (ExpandableHeightGridView) findViewById(R.id.grid_licence);
        getLicense();
    }

    private void getLicense() {
        String Url = URLS.LICENSE_URL;
        Log.d(TAG, "getLicense: URL" + Url);
        StringRequest req = new StringRequest(Request.Method.GET, Url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "getLicense: Response" + response);
                Gson gson = new Gson();
                License license = gson.fromJson(response, License.class);
//                Type collectionType = new TypeToken<Collection<License>>() {
//                }.getType();
//                Collection<License> license = gson.fromJson(response, collectionType);
                if (license != null) {
                    licenseAdapter = new LicenseAdapter(license, ctx);
                    mLicenceGrid.setAdapter(licenseAdapter);
                    //  grid_certification.setAdapter(licenseAdapter);
                } else {
                    // DialogUtils.showDialog4Activity(LoginActivity.this, ctx.getResources().getString(R.string.app_name), logindata.getMessage() + "");

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "getLicense: Error" + error.getMessage());
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    int statusCode = error.networkResponse.statusCode;
                    System.out.println("STATUS CODE " + statusCode + "");
                    DialogUtils.showDialog4Activity(ctx, getResources().getString(R.string.app_name), statusCode + "", null);
                }

            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(req);
    }
}
