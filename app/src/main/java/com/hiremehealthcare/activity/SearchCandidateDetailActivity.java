package com.hiremehealthcare.activity;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.MenuItem;
import android.widget.ImageView;

import com.android.volley.RequestQueue;
import com.android.volley.toolbox.Volley;
import com.hiremehealthcare.fragment.SearchCandidateFragment;
import com.hiremehealthcare.adapter.HomeManagerAdapter;
import com.hiremehealthcare.POJO.HomeManagerModel;
import com.hiremehealthcare.R;
import com.hiremehealthcare.database.MySharedPrefereces;

import java.util.ArrayList;
import java.util.List;

public class SearchCandidateDetailActivity extends AppCompatActivity {

    Context ctx;
    MySharedPrefereces mySharedPrefereces;
    RequestQueue queue;
    public static RecyclerView rvHomeManagerList;
    List<HomeManagerModel> homeManagerModelList;
    public static HomeManagerAdapter homeManagerAdapter;
    ImageView ivNoDataFound;
    TabLayout tabs;
    public static SearchCandidateDetailActivity searchCandidateDetailActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_candidate_detail);
        initAllControls();
    }

    public void initAllControls() {
        searchCandidateDetailActivity = this;
        getSupportActionBar().setTitle("Search Candidate");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ctx = this;
        queue = Volley.newRequestQueue(ctx);
        mySharedPrefereces = new MySharedPrefereces(ctx);
        rvHomeManagerList = (RecyclerView) findViewById(R.id.rvHomeManagerList);
        ivNoDataFound = (ImageView) findViewById(R.id.ivNoDataFound);
        homeManagerModelList = new ArrayList<>();
        //   homeManagerAdapter = new HomeManagerAdapter(homeManagerModelList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(ctx);
        rvHomeManagerList.setLayoutManager(mLayoutManager);
        rvHomeManagerList.setItemAnimator(new DefaultItemAnimator());
        homeManagerAdapter = new HomeManagerAdapter(SearchCandidateFragment.searchCandidate, ctx);
        tabs = (TabLayout) findViewById(R.id.tabs);
        setIconToTab();
        rvHomeManagerList.setAdapter(homeManagerAdapter);
        //  getApplicantNearPosionDataFromApi();
    }

    public void setIconToTab() {

        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_home_black_24dp), false);
//        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_people_black_24dp), false);
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_search_black_24dp), false);
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_dashboard_black_24dp), false);
//        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_assignment_ind_black_24dp), false);
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_chat_black_24dp), false);
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_receipt_black_24dp), false);
        tabs.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (DashBoardManagerActivity.viewpager != null)
                    DashBoardManagerActivity.viewpager.setCurrentItem(tab.getPosition());
                finish();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
        //tabs.setSelected(true,);
        //tabs.getTabAt(4).setIcon(R.drawable.ic_person_black_24dp);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return true;
    }
}
