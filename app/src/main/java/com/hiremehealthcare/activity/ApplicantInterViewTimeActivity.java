package com.hiremehealthcare.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.hiremehealthcare.CommonCls.CustomBoldTextView;
import com.hiremehealthcare.CommonCls.CustomButton;
import com.hiremehealthcare.CommonCls.DialogUtils;
import com.hiremehealthcare.CommonCls.URLS;
import com.hiremehealthcare.CommonCls.Validations;
import com.hiremehealthcare.POJO.ChatManagerModel;
import com.hiremehealthcare.POJO.CommunicationModel;
import com.hiremehealthcare.POJO.GetInterviewTimeDataModel;
import com.hiremehealthcare.R;
import com.hiremehealthcare.Utils.CommonRequestHelper;
import com.hiremehealthcare.database.MySharedPrefereces;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.URL;

/**
 * this class is use applicant side interview time selection
 */
public class ApplicantInterViewTimeActivity extends AppCompatActivity implements View.OnClickListener {

    RadioGroup rgContainer;
    RadioGroup rgContainer_or;
    CustomButton btnSubmitRequest;
    ProgressDialog progressDialog;
    Context context;
    MySharedPrefereces mySharedPrefereces;
    CommunicationModel.Datum data;
    GetInterviewTimeDataModel getInterviewTimeDataModel;
    String TAG = "ApplicantInterViewTimeActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_applicant_inter_view_time);
        initAllControls();

    }

    public void initAllControls() {
        context = this;
        data = (CommunicationModel.Datum) getIntent().getSerializableExtra("data");
        btnSubmitRequest = (CustomButton) findViewById(R.id.btnSubmitRequest);
        btnSubmitRequest.setOnClickListener(this);
        mySharedPrefereces = new MySharedPrefereces(context);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Interview Time");
        rgContainer = (RadioGroup) findViewById(R.id.rgContainer);
        rgContainer_or = (RadioGroup) findViewById(R.id.rgContainer_or);
        progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        SpannableString spannableString = new SpannableString(getString(R.string.please_wait));
        Typeface typefaceSpan = Validations.setTypeface(context);
        spannableString.setSpan(typefaceSpan, 0, getString(R.string.please_wait).length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        progressDialog.setMessage(spannableString);
        getInterViewTimeData();

    }

    /***
     * getting InterViewtime or date if user alredy set
     */

    public void getInterViewTimeData() {
        progressDialog.show();
        String Url = URLS.SELECT_INTERVIEW_TIME + data.getId();
        Log.d(TAG, "getInterViewTimeData: URL " + Url);
        CommonRequestHelper.getDataFromUrl(Url, new CommonRequestHelper.VolleyResponseCustom() {
            @Override
            public void getServerResponse(String response) {
                progressDialog.dismiss();
                Log.d(TAG, "getInterViewTimeData: Response " + response);
                Gson gson = new Gson();
                getInterviewTimeDataModel = gson.fromJson(response, GetInterviewTimeDataModel.class);
                if (getInterviewTimeDataModel.getMessage().size() > 0) {
                    addRadioButton();
                }
            }

            @Override
            public void getVolleyError(VolleyError volleyError) {
                Log.d(TAG, "getInterViewTimeData: volley " + volleyError.getMessage());
                progressDialog.dismiss();
                DialogUtils.noInterNetDialog(context, "No Internet Connection", "No active internet connection available", new DialogUtils.NoInterNetDailogClickListner() {
                    @Override
                    public void onDialogOkButtonClicked() {
                        getInterViewTimeData();
                    }

                    @Override
                    public void onDialogCancelButtonClicked() {
                        finish();
                    }
                });

            }
        }, context);

    }

    /***
     * add run time radio button
     */

    public void addRadioButton() {
        LayoutInflater inflater;
        inflater = (LayoutInflater) getApplicationContext().getSystemService
                (Context.LAYOUT_INFLATER_SERVICE);
        for (GetInterviewTimeDataModel.Message message : getInterviewTimeDataModel.getMessage()) {
            RadioButton radioButton = new RadioButton(this);
            radioButton.setTypeface(Validations.setTypeface(this));
            radioButton.setText(message.getInterviewInterviewDatetime());
            if (message.getInterviewIsSelected().equals("1")) {
                radioButton.setChecked(true);
            }
            radioButton.setBackgroundColor(getResources().getColor(R.color.lgrey));
            LinearLayout.LayoutParams layoutParams = new RadioGroup.LayoutParams(
                    RadioGroup.LayoutParams.MATCH_PARENT,
                    RadioGroup.LayoutParams.WRAP_CONTENT);
            rgContainer.addView(radioButton, layoutParams);
        }

        View view = inflater.inflate(R.layout.or_layout_text_view, null);
        CustomBoldTextView rvItemData = (CustomBoldTextView) view.findViewById(R.id.tvFacilityName);
        rgContainer.addView(view);
        LinearLayout.LayoutParams layoutParams = new RadioGroup.LayoutParams(
                RadioGroup.LayoutParams.MATCH_PARENT,
                RadioGroup.LayoutParams.WRAP_CONTENT);
        RadioButton radioButton = new RadioButton(this);
        radioButton.setTypeface(Validations.setTypeface(this));
        radioButton.setText("Please offer another time");
        radioButton.setBackgroundColor(getResources().getColor(R.color.lgrey));
        rgContainer.addView(radioButton, layoutParams);
        RadioButton radioButton1 = new RadioButton(this);
        radioButton1.setTypeface(Validations.setTypeface(this));
//        radioButton1.setText("I like to reject this interview request");
        /*25 july pragna*/
            radioButton1.setText("I'd like to reject this interview request");
            radioButton1.setBackgroundColor(getResources().getColor(R.color.lgrey));
            rgContainer.addView(radioButton1, layoutParams);
//        rgContainer_or.addView(radioButton1, layoutParams);

        }

        @Override
        public void onClick(View view) {
            int selected_id = rgContainer.getCheckedRadioButtonId();
            if (selected_id == -1)
                Toast.makeText(context, "Please select option", Toast.LENGTH_LONG).show();
            else {
            String option_selected_id = "";
            RadioButton radioButton = findViewById(selected_id);
            String selectedString = radioButton.getText().toString();
            if (selectedString.equals("Please offer another time")) {
                option_selected_id = "4";
          //  } else if (selectedString.equals("I like to reject this interview request")) {
                /*24 july Pragna*/
            } else if (selectedString.equals("I'd like to reject this interview request")) {
                option_selected_id = "5";
            } else {
                for (GetInterviewTimeDataModel.Message message : getInterviewTimeDataModel.getMessage()) {
                    if (message.getInterviewInterviewDatetime().equals(selectedString)) {
                        option_selected_id = message.getInterviewOptions();
                    }
                }
            }
            sendInterViewRequest(option_selected_id);
        }
    }

    public void sendInterViewRequest(final String interview_options) {
        progressDialog.show();
        String confirm_interview_shedule = URLS.CONFIRM_INTERVIEW_SHEDULE +
                "&" + URLS.USER_TYPE + "=" + mySharedPrefereces.getUserType() +
                "&" + URLS.APPLICANT_ID + "=" + data.getApplicantId() +
                "&" + URLS.MANAGER_ID + "=" + data.getManager_id() +
                "&" + URLS.APPLICATION_ID + "=" + data.getId() +
                "&" + URLS.INTERVIEW_OPTIONS + "=" + interview_options;
        Log.d(TAG, "sendInterViewRequest: Url " + confirm_interview_shedule);
        CommonRequestHelper.getDataFromUrl(confirm_interview_shedule, new CommonRequestHelper.VolleyResponseCustom() {
            @Override
            public void getServerResponse(String response) {
                progressDialog.dismiss();
                Log.d(TAG, "sendInterViewRequest: Response " + response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int status = jsonObject.getInt("status");
                    String message = jsonObject.getString("message");
                    if (status == 1) {
                        finish();
                    }
                    DialogUtils.showDialog4Activity(context, context.getResources().getString(R.string.app_name), message, null);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void getVolleyError(VolleyError volleyError) {
                progressDialog.dismiss();
                Log.d(TAG, "sendInterViewRequest: Volley error " + volleyError.getMessage());
                DialogUtils.noInterNetDialog(context, "No Internet Connection", "No active internet connection available", new DialogUtils.NoInterNetDailogClickListner() {
                    @Override
                    public void onDialogOkButtonClicked() {
                        sendInterViewRequest(interview_options);
                    }

                    @Override
                    public void onDialogCancelButtonClicked() {

                    }
                });
            }
        }, context);

    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return true;
    }
}
