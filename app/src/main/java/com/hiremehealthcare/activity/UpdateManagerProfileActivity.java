package com.hiremehealthcare.activity;

import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import com.hiremehealthcare.fragment.UpdateProfileFragment;
import com.hiremehealthcare.R;

public class UpdateManagerProfileActivity extends AppCompatActivity {

    TabLayout tabs;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_update_manager_profile);
        initAllControls();

    }
    public void initAllControls()
    {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Update Profile");
        FragmentTransaction fragmentTransaction=getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.llUpdateProfileContainer, new UpdateProfileFragment(), "update_profile");
        fragmentTransaction.commit();
        tabs = (TabLayout) findViewById(R.id.tabs);
        setIconToTab();

    }
    public void setIconToTab() {

        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_home_black_24dp),false);
//        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_people_black_24dp),false);
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_search_black_24dp),false);
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_dashboard_black_24dp),false);
//        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_assignment_ind_black_24dp),false);
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_chat_black_24dp),false);
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_receipt_black_24dp),false);


//        tabs.getTabAt(0).setIcon(R.drawable.ic_home_black_24dp);
//        tabs.getTabAt(1).setIcon(R.drawable.ic_people_black_24dp);
//        tabs.getTabAt(2).setIcon(R.drawable.ic_search_black_24dp);
//        tabs.getTabAt(3).setIcon(R.drawable.ic_dashboard_black_24dp);
//        tabs.getTabAt(4).setIcon(R.drawable.ic_assignment_ind_black_24dp);
//        tabs.getTabAt(5).setIcon(R.drawable.ic_chat_black_24dp);
//        tabs.getTabAt(6).setIcon(R.drawable.ic_receipt_black_24dp);
        tabs.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if(DashBoardManagerActivity.viewpager!=null)
                    DashBoardManagerActivity.viewpager.setCurrentItem(tab.getPosition());
                finish();
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
        //tabs.setSelected(true,);
        //tabs.getTabAt(4).setIcon(R.drawable.ic_person_black_24dp);


    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId()==android.R.id.home)
            onBackPressed();

        return true;
    }
}
