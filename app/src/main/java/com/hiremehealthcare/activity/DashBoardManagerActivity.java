package com.hiremehealthcare.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.Button;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.VolleyError;
import com.hiremehealthcare.fragment.CommunicationFragment;
import com.hiremehealthcare.CommonCls.DialogUtils;
import com.hiremehealthcare.CommonCls.URLS;
import com.hiremehealthcare.fragment.DashBordManagerFragment;
import com.hiremehealthcare.fragment.HomeManagerFragment;
import com.hiremehealthcare.R;
import com.hiremehealthcare.fragment.ManagerDashboardFragment_updated;
import com.hiremehealthcare.fragment.NewsFeedFragment;
import com.hiremehealthcare.fragment.PostNewPostion;
import com.hiremehealthcare.fragment.SearchCandidateFragment;
import com.hiremehealthcare.fragment.SheduledInterviewsFragment;
import com.hiremehealthcare.Utils.CommonRequestHelper;
import com.hiremehealthcare.Utils.ConnectionDetector;
import com.hiremehealthcare.Utils.CustomTypefaceSpan;
import com.hiremehealthcare.adapter.ViewPagerAdapter;
import com.hiremehealthcare.database.MySharedPrefereces;

import org.json.JSONException;
import org.json.JSONObject;

public class DashBoardManagerActivity extends AppCompatActivity {

    public static ViewPager viewpager;
    TabLayout tabs;
    MySharedPrefereces mySharedPrefereces;
    Toolbar toolbar;
    ConnectionDetector connectionDetector;
    FrameLayout redCircle;
    public static TextView countTextView;
    String TAG = "DashBoardManagerActivity";
    public static DashBoardManagerActivity dashBoardManagerActivity;
    String slug = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard_for_manager);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        initAllControls();

//        float myTabLayoutSize = 360;
//        if (DeviceInfo.getWidthDP(this) >= myTabLayoutSize ){
//            tabLayout.setTabMode(TabLayout.MODE_FIXED);
//        } else {
//            tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
//        }
    }

    public void initAllControls() {
        slug = getIntent().getStringExtra("slug");
        dashBoardManagerActivity = this;
        connectionDetector = new ConnectionDetector(this);
        mySharedPrefereces = new MySharedPrefereces(this);
        viewpager = (ViewPager) findViewById(R.id.viewpager);
        tabs = (TabLayout) findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewpager);
        viewpager.setOffscreenPageLimit(4);

        //viewpager.setCurrentItem(0);
        checkIsInterNetConnectionAvailable();


    }

    public void checkIsInterNetConnectionAvailable() {
        if (connectionDetector.isConnectingToInternet()) {
            setFragmentAfterCheckInternet();
        } else {
            DialogUtils.noInterNetDialog(DashBoardManagerActivity.this, "No Internet Connection", "No active internet connection available", new DialogUtils.NoInterNetDailogClickListner() {
                @Override
                public void onDialogOkButtonClicked() {
                    checkIsInterNetConnectionAvailable();
                }

                @Override
                public void onDialogCancelButtonClicked() {
                    finish();
                }
            });
        }

    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        final MenuItem alertMenuItem = menu.findItem(R.id.activity_main_alerts_menu_item);
        FrameLayout rootView = (FrameLayout) alertMenuItem.getActionView();
        rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOptionsItemSelected(alertMenuItem);
            }
        });
        redCircle = (FrameLayout) rootView.findViewById(R.id.view_alert_red_circle);
        countTextView = (TextView) rootView.findViewById(R.id.view_alert_count_textview);
        //countTextView.setText("99");
        countTextView.bringToFront();
        getNumberOfMessageSpecificUser();
        return super.onPrepareOptionsMenu(menu);
    }

    String Count = "";

    public void getNumberOfMessageSpecificUser() {

        String Url = URLS.GET_TOTAL_MESSAGE + mySharedPrefereces.getUserType() + "&" + URLS.USER_ID + "=" + mySharedPrefereces.getUserId();
        Log.d(TAG, "getNumberOfMessageSpecificUser: Url " + Url);
        CommonRequestHelper.getDataFromUrl(Url, new CommonRequestHelper.VolleyResponseCustom() {
            @Override
            public void getServerResponse(String response) {
                Log.d(TAG, "getNumberOfMessageSpecificUser: response " + response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int status = jsonObject.getInt("status");
                    if (status == 1) {
                        redCircle.setVisibility(View.VISIBLE);

                        countTextView.setText(jsonObject.getString("message") + "");
                        tv.setText(jsonObject.getString("message") + "");
                        Count = jsonObject.getString("message") + "";
                        if (Count.contentEquals("")) {
                            tv.setVisibility(View.GONE);
                        } else {
                            tv.setVisibility(View.VISIBLE);
                        }
                    } else {
                        redCircle.setVisibility(View.GONE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


               /* for (int i = 0; i < tabs.getTabCount(); i++) {
                    LayoutInflater inflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    View v; // Creating an instance for View Object
                    v = inflater.inflate(R.layout.test, null);
                    System.out.println("THIS IS TEST..............");
                    if (i == 5) {
                        // btn1.setColorFilter(Color.parseColor("#673c96"), PorterDuff.Mode.SRC_IN);
                        tabs.getTabAt(i).setCustomView(null);
                       // btn1.setImageDrawable(getResources().getDrawable(R.drawable.ic_chat_black_24dp));

                        tv.setText(Count + "");
                        // tv.setText(i + "");
                        //  tv.setTypeface(Typeface);
//            tabs.getTabAt(i).setCustomView(tv);
                        tabs.getTabAt(i).setCustomView(v);


                    }
                }*/
            }

            @Override
            public void getVolleyError(VolleyError volleyError) {
                Log.d(TAG, "getNumberOfMessageSpecificUser: error " + volleyError.getMessage());
            }
        }, this);


    }

    public void setFragmentAfterCheckInternet() {
        setupViewPagerFragment();
        setIconToTab();
        tabs.getTabAt(tabs.getSelectedTabPosition()).getIcon().setColorFilter(Color.parseColor("#673c96"), PorterDuff.Mode.SRC_IN);
        if (mySharedPrefereces.getRedirect() == 1) {
            //pppppppppppppp startActivity(new Intent(this, UpdateManagerProfileActivity.class));
         //pppppppppppppppppppppppppppppppppppppp`   startActivity(new Intent(this, UpdateManagerProfileActivity.class));
        }
        tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (countTextView != null)
                    getNumberOfMessageSpecificUser();

                tab.getIcon().setColorFilter(Color.parseColor("#673c96"), PorterDuff.Mode.SRC_IN);
                if (tab.getCustomView() != null) {
                    ImageView iv = tab.getCustomView().findViewById(R.id.iv_1);

                    if (iv != null) {
                        iv.setColorFilter(Color.parseColor("#673c96"), PorterDuff.Mode.SRC_IN);
                    }
                } else {
                    System.out.println("custom null!!!!!!!!!!!!!!!!!!!!!!");
                }


//                if (tab.getPosition() == 5) {
                if (tab.getPosition() ==3) {
                    View v; // Creating an instance for View Object
                    LayoutInflater inflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                    v = inflater.inflate(R.layout.test, null);

                    tv = (TextView) v.findViewById(R.id.view_alert_count_textview);
                    btn1 = (ImageView) v.findViewById(R.id.iv);

                    btn1.setColorFilter(Color.parseColor("#673c96"), PorterDuff.Mode.SRC_ATOP);
                    System.out.println("SELECTED.....");
                    for (int i = 0; i < tabs.getTabCount(); i++) {
//                        if (i == 5) {
                        if (i == 3) {
                            // btn1.setColorFilter(Color.parseColor("#673c96"), PorterDuff.Mode.SRC_IN);
                            tabs.getTabAt(i).setCustomView(null);
                            btn1.setImageDrawable(getResources().getDrawable(R.drawable.ic_chat_black_24dp));
                            btn1.setColorFilter(Color.parseColor("#673c96"), PorterDuff.Mode.SRC_ATOP);
                            tv.setText(Count + "");
                            // tv.setText(i + "");
                            //  tv.setTypeface(Typeface);
//            tabs.getTabAt(i).setCustomView(tv);
                            tabs.getTabAt(i).setCustomView(v);


                        }
                    }
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
//                tab.getIcon().setColorFilter(Color.parseColor("#53585F"), PorterDuff.Mode.SRC_IN);
                tab.getIcon().setColorFilter(Color.parseColor("#000000"), PorterDuff.Mode.SRC_IN);
//                if (tab.getPosition() == 5) {
                if (tab.getPosition() == 3) {
                    for (int i = 0; i < tabs.getTabCount(); i++) {
                        View v; // Creating an instance for View Object
                        LayoutInflater inflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        v = inflater.inflate(R.layout.test, null);
                        tv = (TextView) v.findViewById(R.id.view_alert_count_textview);
                        btn1 = (ImageView) v.findViewById(R.id.iv);

                        System.out.println("Unselected....MANAGER..");
//                        if (i == 5) {
                        if (i == 3) {
                            tabs.getTabAt(i).setCustomView(null);
                            //   btn1.setColorFilter(Color.parseColor("#53585F"), PorterDuff.Mode.SRC_IN);
                            btn1.setImageDrawable(getResources().getDrawable(R.drawable.ic_chat_black_24dp));
//                            btn1.setColorFilter(Color.parseColor("#53585F"), PorterDuff.Mode.SRC_ATOP);
                            btn1.setColorFilter(Color.parseColor("#000000"), PorterDuff.Mode.SRC_ATOP);
                            tv.setText(Count + "");
                            // tv.setText(i + "");
                            //  tv.setTypeface(Typeface);
//            tabs.getTabAt(i).setCustomView(tv);
                            tabs.getTabAt(i).setCustomView(v);

                        }
                    }
                }
                //    ImageView iv = tab.getCustomView().findViewById(R.id.iv_1);
               /* if (iv != null) {
                    iv.setColorFilter(Color.parseColor("#53585F"), PorterDuff.Mode.SRC_IN);
                }*/
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_manager, menu);
        Typeface font = Typeface.createFromAsset(
                getAssets(),
                "fonts/PoppinsRegular.otf");
        // Get the root inflator.
        for (int i = 0; i < menu.size(); i++) {
            MenuItem mi = menu.getItem(i);
            //for aapplying a font to subMenu ...
            SubMenu subMenu = mi.getSubMenu();
            if (subMenu != null && subMenu.size() > 0) {
                for (int j = 0; j < subMenu.size(); j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    applyFontToMenuItem(subMenuItem, font);
                }
            }
            //the method we have create in activity
            applyFontToMenuItem(mi, font);
        }

        return super.onCreateOptionsMenu(menu);
        //return true;
    }

    private void applyFontToMenuItem(MenuItem mi, Typeface font) {
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("", font), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.help:
                startActivity(new Intent(this, HelpActivity.class));
//                photoCameraIntent();
                break;
            case R.id.privacyAndPolicy:
                startActivity(new Intent(this, PrivacyAndPolicyActivity.class));
                break;
            case R.id.termsAndCondition:
                startActivity(new Intent(this, TermsAndConditionActivity.class));
                break;
            case R.id.faq:
                startActivity(new Intent(this, FAQsActivity.class));

                break;
            case R.id.contactUs:
                startActivity(new Intent(this, ContactUsActivity.class));
                break;
            case R.id.logout:
                mySharedPrefereces.logout();
                Intent intent = new Intent(DashBoardManagerActivity.this, StartingSelectionActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                break;
            case R.id.update_profile:
                startActivity(new Intent(this, UpdateManagerProfileActivity.class));
                break;
            case R.id.change_password:
                startActivity(new Intent(this, ChangePasswordActivity.class));
                break;
            case R.id.activity_main_alerts_menu_item:
//                viewpager.setCurrentItem(4);
                viewpager.setCurrentItem(3);
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    TextView tv;
    ImageView btn1;

    public void setIconToTab() {

       /* tabs.getTabAt(0).setIcon(R.drawable.ic_home_black_24dp);
//        tabs.getTabAt(1).setIcon(R.drawable.ic_people_black_24dp);
        tabs.getTabAt(2).setIcon(R.drawable.ic_search_black_24dp);
        tabs.getTabAt(3).setIcon(R.drawable.ic_dashboard_black_24dp);
//        tabs.getTabAt(4).setIcon(R.drawable.ic_assignment_ind_black_24dp);
        tabs.getTabAt(5).setIcon(R.drawable.ic_chat_black_24dp);
        tabs.getTabAt(6).setIcon(R.drawable.ic_receipt_black_24dp);*/


        tabs.getTabAt(0).setIcon(R.drawable.ic_home_black_24dp);
//        tabs.getTabAt(1).setIcon(R.drawable.ic_people_black_24dp);
        tabs.getTabAt(1).setIcon(R.drawable.ic_search_black_24dp);
        tabs.getTabAt(2).setIcon(R.drawable.ic_dashboard_black_24dp);
//        tabs.getTabAt(4).setIcon(R.drawable.ic_assignment_ind_black_24dp);
        tabs.getTabAt(3).setIcon(R.drawable.ic_chat_black_24dp);
        tabs.getTabAt(4).setIcon(R.drawable.ic_receipt_black_24dp);
        for (int i = 0; i < tabs.getTabCount(); i++) {
            View v; // Creating an instance for View Object
            LayoutInflater inflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.test, null);

            tv = (TextView) v.findViewById(R.id.view_alert_count_textview);
            btn1 = (ImageView) v.findViewById(R.id.iv);

           /* if (i == 0) {
                btn1.setImageDrawable(getResources().getDrawable(R.drawable.ic_home_black_24dp));
            }
            if (i == 1) {
                btn1.setImageDrawable(getResources().getDrawable(R.drawable.ic_search_black_24dp));
            }
            if (i == 2) {
                btn1.setImageDrawable(getResources().getDrawable(R.drawable.ic_dashboard_black_24dp));
            }
            if (i == 3) {
                btn1.setImageDrawable(getResources().getDrawable(R.drawable.ic_assignment_ind_black_24dp));
            }*/
            if (i == 3) {
                btn1.setImageDrawable(getResources().getDrawable(R.drawable.ic_chat_black_24dp));
                // tv.setText(i + "");
                //  tv.setTypeface(Typeface);
//            tabs.getTabAt(i).setCustomView(tv);
                tabs.getTabAt(i).setCustomView(v);
            }


            //noinspection ConstantConditions
            // TextView tv = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
           /* tv.setText(i + "");
            //  tv.setTypeface(Typeface);
//            tabs.getTabAt(i).setCustomView(tv);
            tabs.getTabAt(i).setCustomView(v);*/

        }
      /*  for (int i = 0; i < tabs.getTabCount(); i++) {
            //noinspection ConstantConditions
//            TextView tv=(TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab,null);
            TextView tv=(TextView) LayoutInflater.from(this).inflate(R.layout.test,null);

         //   if(i==4)
          //  {
               // tv.setCompoundDrawables(getResources().getDrawable(R.drawable.ic_home_black_24dp),null,null,null);

                    //noinspection ConstantConditions
//                    TextView tv1=(TextView)LayoutInflater.from(this).inflate(R.layout.test,null);
                 //   tv.setTypeface(Typeface);
//                    tabs.getTabAt(i).setCustomView(tv1);
          //      tv1.setText("TEST111 "+i+"");
//                tabs.getTabAt(4).setCustomView(tv1);

           // }
            //tv.setTypeface(Typeface);
//            tv.setText("TEST "+i+"");
            tabs.getTabAt(i).setCustomView(tv);


        }*/
        //tabs.getTabAt(4).setIcon(R.drawable.ic_person_black_24dp);


    }

    public void setupViewPagerFragment() {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
//        adapter.addFragment(new HomeManagerFragment(), "");
        adapter.addFragment(new ManagerDashboardFragment_updated(), "");
//        adapter.addFragment(new HomeManagerFragment(), "");
        adapter.addFragment(new SearchCandidateFragment(), "");
        adapter.addFragment(new DashBordManagerFragment(), "");
//        adapter.addFragment(new SheduledInterviewsFragment(), "");
        adapter.addFragment(new CommunicationFragment(), "");
//        adapter.addFragment(new NewsFeedFragment(), "");
        adapter.addFragment(new PostNewPostion(), "");
        //adapter.addFragment(new UpdateProfileFragment(), "");

        viewpager.setAdapter(adapter);

        if (slug != null) {
            if (slug.equals("")) {

//            } else if (slug.equals("0")) {
//                viewpager.setCurrentItem(0);
//            } else if (slug.equals("1")) {
//                viewpager.setCurrentItem(1);
//            } else if (slug.equals("2")) {
//                viewpager.setCurrentItem(2);
//            } else if (slug.equals("3")) {
//                viewpager.setCurrentItem(3);
//            } else if (slug.equals("4")) {
//                viewpager.setCurrentItem(4);
//            }


            } else if (slug.equals("0")) {
                viewpager.setCurrentItem(1);
            } else if (slug.equals("1")) {
                viewpager.setCurrentItem(2);
            } else if (slug.equals("2")) {
                viewpager.setCurrentItem(3);
            } else if (slug.equals("3")) {
                viewpager.setCurrentItem(4);
            } else if (slug.equals("4")) {
                viewpager.setCurrentItem(5);
            }
        }

    }
}
