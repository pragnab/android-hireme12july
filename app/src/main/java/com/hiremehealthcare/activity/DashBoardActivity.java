package com.hiremehealthcare.activity;

import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.view.View;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.hiremehealthcare.POJO.ProfilePersonalDetail;
import com.hiremehealthcare.fragment.CommunicationFragment;
import com.hiremehealthcare.CommonCls.DialogUtils;
import com.hiremehealthcare.CommonCls.URLS;
import com.hiremehealthcare.fragment.FavFragment;
import com.hiremehealthcare.POJO.ApplicantExperience;
import com.hiremehealthcare.POJO.DeleteApplicantCertificate;
import com.hiremehealthcare.POJO.ManagerProfile;
import com.hiremehealthcare.fragment.NewsFeedFragment;
import com.hiremehealthcare.fragment.PostNewPostion;
import com.hiremehealthcare.fragment.ProfileFragment;
import com.hiremehealthcare.R;
import com.hiremehealthcare.fragment.SearchFragment;
import com.hiremehealthcare.Utils.CommonRequestHelper;
import com.hiremehealthcare.Utils.ConnectionDetector;
import com.hiremehealthcare.Utils.CustomTypefaceSpan;
import com.hiremehealthcare.adapter.ViewPagerAdapter;
import com.hiremehealthcare.database.MySharedPrefereces;
import com.hiremehealthcare.fragment.ApplicantNearPostionFragment;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import de.hdodenhof.circleimageview.CircleImageView;

public class DashBoardActivity extends AppCompatActivity {

    public static ViewPager viewpager;
    TabLayout tabs;
    RequestQueue queue;
    Context ctx;
    MySharedPrefereces mySharedPrefereces;
    public static DashBoardActivity dashBoardActivity;
    Toolbar toolbar;
    ConnectionDetector connectionDetector;
    FrameLayout redCircle;
    public static TextView countTextView;
    String TAG = "DashBoardActivity";
    String slug = "";
    public static CircleImageView civ_requests,civ_requests1;
    ProfilePersonalDetail profilePersonalDetail;
    /**
     * this method is used to getting Personal detail from server
     */

    private void getPersonalDetails() {

        String Url = URLS.PROFILE_PERSONALDETAIL_URL + mySharedPrefereces.getUserId();
        Log.d(TAG, "getPersonalDetails: getPersonalDetails URL:- " + Url);
        StringRequest req = new StringRequest(Request.Method.GET, Url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                System.out.println("CallLicense RESPONSE " + response + "");
                Gson gson = new Gson();
                Log.d(TAG, "getPersonalDetails: response getPersonalDetails:- " + response);
                profilePersonalDetail=new ProfilePersonalDetail();
                profilePersonalDetail = gson.fromJson(response, ProfilePersonalDetail.class);
//                Type collectionType = new TypeToken<Collection<License>>() {
//                }.getType();
//                Collection<License> license = gson.fromJson(response, collectionType);
                if (profilePersonalDetail != null) {

                    mySharedPrefereces.setUserImage(profilePersonalDetail.getData().getApplicant_image()+"");
                    if(DashBoardActivity.civ_requests1!=null) {
                        Glide.with(ctx)
                                .load(profilePersonalDetail.getData().getApplicant_image() + "")
                                .error(R.drawable.no_image)
                                //.placeholder(R.drawable.no_image)
                                .into(DashBoardActivity.civ_requests1);
                    }




                } else {
                    // DialogUtils.showDialog4Activity(LoginActivity.this, ctx.getResources().getString(R.string.app_name), logindata.getMessage() + "");

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "getPersonalDetails: response:- " + error.getMessage());
                NetworkResponse networkResponse = error.networkResponse;
               /* if (networkResponse != null) {
                    int statusCode = error.networkResponse.statusCode;
                    System.out.println("STATUS CODE " + statusCode + "");
                    DialogUtils.showDialog4Activity(getActivity(), ctx.getResources().getString(R.string.app_name), statusCode + "", null);
                }*/

            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(req);
    }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.dashboard);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        slug = getIntent().getStringExtra("slug");
        connectionDetector = new ConnectionDetector(this);
        setSupportActionBar(toolbar);
        initAllControls();
        getPersonalDetails();
//        float myTabLayoutSize = 360;
//        if (DeviceInfo.getWidthDP(this) >= myTabLayoutSize ){
//            tabLayout.setTabMode(TabLayout.MODE_FIXED);
//        } else {
//            tabLayout.setTabMode(TabLayout.MODE_SCROLLABLE);
//        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        getPersonalDetails();
    }

    public void initAllControls() {
        dashBoardActivity = this;
        mySharedPrefereces = new MySharedPrefereces(this);
        ctx = getApplicationContext();
        queue = Volley.newRequestQueue(ctx);
        GetUpdateManagerProfile();
        viewpager = (ViewPager) findViewById(R.id.viewpager);
        civ_requests = (CircleImageView) toolbar.findViewById(R.id.civ_requests);
        civ_requests1 = (CircleImageView) toolbar.findViewById(R.id.civ_requests1);
        tabs = (TabLayout) findViewById(R.id.tabs);
//        tabs = (TabLayout) findViewById(R.id.tabs);
        tabs.setupWithViewPager(viewpager);
        viewpager.setOffscreenPageLimit(5);
        checkIsInterNetConnectionAvailable();
        civ_requests1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (viewpager != null) {

                    System.out.println("this is click event ");

                    Intent i = new Intent(DashBoardActivity.this, ProfileActivity.class);
                    startActivity(i);
                }
            }
        });

    }

    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        final MenuItem alertMenuItem = menu.findItem(R.id.activity_main_alerts_menu_item);
        FrameLayout rootView = (FrameLayout) alertMenuItem.getActionView();
        rootView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                onOptionsItemSelected(alertMenuItem);
            }
        });
        redCircle = (FrameLayout) rootView.findViewById(R.id.view_alert_red_circle);
        countTextView = (TextView) rootView.findViewById(R.id.view_alert_count_textview);
        // countTextView.setText("99");
        countTextView.bringToFront();
        getNumberOfMessageSpecificUser();
        return super.onPrepareOptionsMenu(menu);
    }

    public void checkIsInterNetConnectionAvailable() {
        if (connectionDetector.isConnectingToInternet()) {
            setFragmentAfterCheckInternet();
        } else {
            DialogUtils.noInterNetDialog(DashBoardActivity.this, "No Internet Connection", "No active internet connection available", new DialogUtils.NoInterNetDailogClickListner() {
                @Override
                public void onDialogOkButtonClicked() {
                    checkIsInterNetConnectionAvailable();
                }

                @Override
                public void onDialogCancelButtonClicked() {
                    finish();
                }
            });
        }

    }

    public void setFragmentAfterCheckInternet() {
        setupViewPagerFragment();
//        tabs.setupWithViewPager(viewpager);
        setIconToTab();
        tabs.getTabAt(tabs.getSelectedTabPosition()).getIcon().setColorFilter(Color.parseColor("#673c96"), PorterDuff.Mode.SRC_IN);
        if (mySharedPrefereces.getRedirect() == 2) {
//         ppppppppppppp   viewpager.setCurrentItem(4);
            Intent i=new Intent(DashBoardActivity.this,ProfileActivity.class);
            startActivity(i);


        }
        tabs.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
               /* tab.getIcon().setColorFilter(Color.parseColor("#673c96"), PorterDuff.Mode.SRC_IN);
                if (countTextView != null) {
                    getNumberOfMessageSpecificUser();
                }

                if (tab.getPosition() == 3) {
                    System.out.println("SELECTED.....");
                    btn1.setColorFilter(Color.parseColor("#673c96"), PorterDuff.Mode.SRC_IN);
                }*/
                if (countTextView != null) {
                    getNumberOfMessageSpecificUser();
                }
//                tab.getIcon().setColorFilter(Color.parseColor("#673c96"), PorterDuff.Mode.SRC_IN);
                tab.getIcon().setColorFilter(Color.parseColor("#673c96"), PorterDuff.Mode.SRC_ATOP);
                if (tab.getCustomView() != null) {
                    ImageView iv = tab.getCustomView().findViewById(R.id.iv_1);

                    if (iv != null) {
//                        iv.setColorFilter(Color.parseColor("#673c96"), PorterDuff.Mode.SRC_IN);
                        iv.setColorFilter(Color.parseColor("#673c96"), PorterDuff.Mode.SRC_ATOP);
                    }
                } else {
                    System.out.println("custom null!!!!!!!!!!!!!!!!!!!!!!");
                }


                if (tab.getPosition() == 3) {
                    System.out.println("SELECTED.....");
//                    Buttoni.setColorFilter(Color.parseColor("#673c96"), PorterDuff.Mode.SRC_IN);
                    Buttoni.setColorFilter(Color.parseColor("#673c96"), PorterDuff.Mode.SRC_ATOP);
                    for (int i = 0; i < tabs.getTabCount(); i++) {
                        View v; // Creating an instance for View Object
                        LayoutInflater inflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        v = inflater.inflate(R.layout.test, null);

                        tv = (TextView) v.findViewById(R.id.view_alert_count_textview);
                        Buttoni = (ImageView) v.findViewById(R.id.iv);
                        if (i == 3) {
                            tabs.getTabAt(i).setCustomView(null);
                            Buttoni.setImageDrawable(getResources().getDrawable(R.drawable.ic_chat_black_24dp));
                            Buttoni.setColorFilter(Color.parseColor("#673c96"), PorterDuff.Mode.SRC_ATOP);
                            tv.setText(Count + "");
                            // tv.setText(i + "");
                            //  tv.setTypeface(Typeface);
//            tabs.getTabAt(i).setCustomView(tv);
                            tabs.getTabAt(i).setCustomView(v);
                        }
                    }
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
               /* tab.getIcon().setColorFilter(Color.parseColor("#53585F"), PorterDuff.Mode.SRC_IN);

                if (tab.getPosition() == 3) {
                    System.out.println("Unselected.....");
                    btn1.setColorFilter(Color.parseColor("#53585F"), PorterDuff.Mode.SRC_IN);
                }*/
                tab.getIcon().setColorFilter(Color.parseColor("#000000"), PorterDuff.Mode.SRC_IN);
                if (tab.getPosition() == 3) {
                    System.out.println("Unselected....ffffffffff..");
                    Buttoni.setColorFilter(Color.parseColor("#000000"), PorterDuff.Mode.SRC_IN);


                    for (int i = 0; i < tabs.getTabCount(); i++) {
                        View v; // Creating an instance for View Object
                        LayoutInflater inflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                        v = inflater.inflate(R.layout.test, null);

                        tv = (TextView) v.findViewById(R.id.view_alert_count_textview);
                        Buttoni = (ImageView) v.findViewById(R.id.iv);
                        if (i == 3) {
                            tabs.getTabAt(i).setCustomView(null);
                            Buttoni.setImageDrawable(getResources().getDrawable(R.drawable.ic_chat_black_24dp));
                            Buttoni.setColorFilter(Color.parseColor("#000000"), PorterDuff.Mode.SRC_ATOP);
                            tv.setText(Count + "");
                            // tv.setText(i + "");
                            //  tv.setTypeface(Typeface);
//            tabs.getTabAt(i).setCustomView(tv);
                            tabs.getTabAt(i).setCustomView(v);
                        }
                    }
                }
                //    ImageView iv = tab.getCustomView().findViewById(R.id.iv_1);
               /* if (iv != null) {
                    iv.setColorFilter(Color.parseColor("#53585F"), PorterDuff.Mode.SRC_IN);
                }*/
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
        //viewpager.setCurrentItem(0);
        /* tabs.getTabAt(tabs.getSelectedTabPosition()).getIcon().setColorFilter(Color.parseColor("#673c96"), PorterDuff.Mode.SRC_IN);*/


        if (slug != null) {
            if (slug.equals("")) {

            } else if (slug.equals("0")) {
                viewpager.setCurrentItem(0);
            } else if (slug.equals("1")) {
                viewpager.setCurrentItem(1);
            } else if (slug.equals("2")) {
                viewpager.setCurrentItem(2);
            } else if (slug.equals("3")) {
                viewpager.setCurrentItem(3);
            } else if (slug.equals("4")) {
                viewpager.setCurrentItem(4);
            }
        }
    }

    TextView tv;
    ImageView Buttoni;

    public void setIconToTab() {

        tabs.getTabAt(0).setIcon(R.drawable.ic_home_black_24dp);
        tabs.getTabAt(1).setIcon(R.drawable.ic_search_black_24dp);
        tabs.getTabAt(2).setIcon(R.drawable.ic_favorite_black_24dp);
        tabs.getTabAt(3).setIcon(R.drawable.ic_chat_black_24dp);
//        tabs.getTabAt(4).setIcon(R.drawable.ic_person_black_24dp);
        tabs.getTabAt(4).setIcon(R.drawable.ic_receipt_black_24dp);
        for (int i = 0; i < tabs.getTabCount(); i++) {
            View v; // Creating an instance for View Object
            LayoutInflater inflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.test, null);

            tv = (TextView) v.findViewById(R.id.view_alert_count_textview);
            Buttoni = (ImageView) v.findViewById(R.id.iv);

           /* if (i == 0) {
                btn1.setImageDrawable(getResources().getDrawable(R.drawable.ic_home_black_24dp));
            }
            if (i == 1) {
                btn1.setImageDrawable(getResources().getDrawable(R.drawable.ic_search_black_24dp));
            }
            if (i == 2) {
                btn1.setImageDrawable(getResources().getDrawable(R.drawable.ic_dashboard_black_24dp));
            }
            if (i == 3) {
                btn1.setImageDrawable(getResources().getDrawable(R.drawable.ic_assignment_ind_black_24dp));
            }*/
            if (i == 3) {
                Buttoni.setImageDrawable(getResources().getDrawable(R.drawable.ic_chat_black_24dp));
                // tv.setText(i + "");
                //  tv.setTypeface(Typeface);
//            tabs.getTabAt(i).setCustomView(tv);
                tabs.getTabAt(i).setCustomView(v);
            }


            //noinspection ConstantConditions
            // TextView tv = (TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab, null);
           /* tv.setText(i + "");
            //  tv.setTypeface(Typeface);
//            tabs.getTabAt(i).setCustomView(tv);
            tabs.getTabAt(i).setCustomView(v);*/

        }
      /*  for (int i = 0; i < tabs.getTabCount(); i++) {
            //noinspection ConstantConditions
//            TextView tv=(TextView) LayoutInflater.from(this).inflate(R.layout.custom_tab,null);
            TextView tv=(TextView) LayoutInflater.from(this).inflate(R.layout.test,null);

         //   if(i==4)
          //  {
               // tv.setCompoundDrawables(getResources().getDrawable(R.drawable.ic_home_black_24dp),null,null,null);

                    //noinspection ConstantConditions
//                    TextView tv1=(TextView)LayoutInflater.from(this).inflate(R.layout.test,null);
                 //   tv.setTypeface(Typeface);
//                    tabs.getTabAt(i).setCustomView(tv1);
          //      tv1.setText("TEST111 "+i+"");
//                tabs.getTabAt(4).setCustomView(tv1);

           // }
            //tv.setTypeface(Typeface);
//            tv.setText("TEST "+i+"");
            tabs.getTabAt(i).setCustomView(tv);


        }*/
        //tabs.getTabAt(4).setIcon(R.drawable.ic_person_black_24dp);


    }


    public void setupViewPagerFragment() {
        ViewPagerAdapter adapter = new ViewPagerAdapter(getSupportFragmentManager());
        adapter.addFragment(new ApplicantNearPostionFragment(), "");
        adapter.addFragment(new SearchFragment(), "");
        adapter.addFragment(new FavFragment(), "");
        adapter.addFragment(new CommunicationFragment(), "");
//        adapter.addFragment(new ProfileFragment(), "");
        adapter.addFragment(new NewsFeedFragment(), "");
//        adapter.addFragment(new PostNewPostion(), "");
        viewpager.setAdapter(adapter);
        //    viewpager.setOffscreenPageLimit(5);
        if (slug != null) {
            if (slug.equals("")) {

            } else if (slug.equals("0")) {
                viewpager.setCurrentItem(0);
            } else if (slug.equals("1")) {
                viewpager.setCurrentItem(1);
            } else if (slug.equals("2")) {
                viewpager.setCurrentItem(2);
            } else if (slug.equals("3")) {
                viewpager.setCurrentItem(3);
            } else if (slug.equals("4")) {
                viewpager.setCurrentItem(4);
            }
        }
        //startActivity(new Intent(DashBoardActivity.this,ViewPositionDetailActivity.class));

    }

    String Count = "";
//deckoid solution
    public void getNumberOfMessageSpecificUser() {
        String Url = URLS.GET_TOTAL_MESSAGE + mySharedPrefereces.getUserType() + "&" + URLS.USER_ID + "=" + mySharedPrefereces.getUserId();
        Log.d(TAG, "getNumberOfMessageSpecificUser: Url " + Url);
        CommonRequestHelper.getDataFromUrl(Url, new CommonRequestHelper.VolleyResponseCustom() {
            @Override
            public void getServerResponse(String response) {
                Log.d(TAG, "getNumberOfMessageSpecificUser: response " + response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int status = jsonObject.getInt("status");
                    if (status == 1) {
                        redCircle.setVisibility(View.VISIBLE);
                        countTextView.setText(jsonObject.getString("message") + "");
                        for (int i = 0; i < tabs.getTabCount(); i++) {
                            View v; // Creating an instance for View Object
                            LayoutInflater inflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            v = inflater.inflate(R.layout.test, null);

                            tv = (TextView) v.findViewById(R.id.view_alert_count_textview);

                        }

                        tv.setText(jsonObject.getString("message") + "");
                        Count = jsonObject.getString("message") + "";
                        if (Count.contentEquals("")) {
                            tv.setVisibility(View.GONE);
                        } else {

                            for (int i = 0; i < tabs.getTabCount(); i++) {
                                View v; // Creating an instance for View Object
                                LayoutInflater inflater = (LayoutInflater) getBaseContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                v = inflater.inflate(R.layout.test, null);

                                tv = (TextView) v.findViewById(R.id.view_alert_count_textview);
                                tv.setVisibility(View.VISIBLE);
                                Buttoni = (ImageView) v.findViewById(R.id.iv);


                                if (i == 3) {
                                    Buttoni.setImageDrawable(getResources().getDrawable(R.drawable.ic_chat_black_24dp));
                                    if (viewpager.getCurrentItem() == 3) {
                                        Buttoni.setColorFilter(Color.parseColor("#673c96"), PorterDuff.Mode.SRC_IN);
                                    } else {
                                        Buttoni.setColorFilter(Color.parseColor("#000000"), PorterDuff.Mode.SRC_IN);
                                    }
                                    System.out.println("THIS MUST PRINT " + Count + "");
                                    tabs.getTabAt(i).setCustomView(null);
                                    tv.setText(Count + "");
                                    //  tv.setTypeface(Typeface);
//            tabs.getTabAt(i).setCustomView(tv);
                                    tabs.getTabAt(i).setCustomView(v);
                                }

                            }
                        }
                        /*} else {
                            tv.setVisibility(View.VISIBLE);
                        }*/
                        // ppppppppppppppppp
                    } else {
                        redCircle.setVisibility(View.GONE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void getVolleyError(VolleyError volleyError) {
                Log.d(TAG, "getNumberOfMessageSpecificUser: error " + volleyError.getMessage());
            }
        }, ctx);


    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        Typeface font = Typeface.createFromAsset(
                getAssets(),
                "fonts/PoppinsRegular.otf");
        // Get the root inflator.
        for (int i = 0; i < menu.size(); i++) {
            MenuItem mi = menu.getItem(i);
            //for aapplying a font to subMenu ...
            SubMenu subMenu = mi.getSubMenu();
            if (subMenu != null && subMenu.size() > 0) {
                for (int j = 0; j < subMenu.size(); j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    applyFontToMenuItem(subMenuItem, font);
                }
            }
            //the method we have create in activity
            applyFontToMenuItem(mi, font);
        }

        return super.onCreateOptionsMenu(menu);
        //return true;
    }

    private void applyFontToMenuItem(MenuItem mi, Typeface font) {
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("", font), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.help:
                startActivity(new Intent(this, HelpActivity.class));
//                photoCameraIntent();
                break;
            case R.id.privacyAndPolicy:
                startActivity(new Intent(this, PrivacyAndPolicyActivity.class));
                break;
            case R.id.termsAndCondition:
                startActivity(new Intent(this, TermsAndConditionActivity.class));
                break;
            case R.id.faq:
                startActivity(new Intent(this, FAQsActivity.class));
                break;
            case R.id.contactUs:
                startActivity(new Intent(this, ContactUsActivity.class));
                break;
            case R.id.logout:
                mySharedPrefereces.logout();
                Intent intent = new Intent(DashBoardActivity.this, StartingSelectionActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
                break;
            case R.id.change_password:
                startActivity(new Intent(this, ChangePasswordActivity.class));
                break;
            case R.id.activity_main_alerts_menu_item:
                viewpager.setCurrentItem(3);
                break;
        }

        return super.onOptionsItemSelected(item);
    }


   /* private void GetDeleteApplicantCertificate(String APPLICATEI_ID, String CERTIFICATE_ID) {

        String Url = URLS.DeleteApplicantCertificate + APPLICATEI_ID + "&"
                + URLS.CERTIFICATE_ID + "=" + CERTIFICATE_ID;
        System.out.println("API$$$$$ GetDeleteApplicantCertificate URL " + Url);
        StringRequest putRequest = new StringRequest(Request.Method.POST, Url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println("API$$$$$ GetDeleteApplicantCertificate response " + response);
                        Gson gson = new Gson();
                        DeleteApplicantCertificate deleteApplicantCertificate = gson.fromJson(response, DeleteApplicantCertificate.class);
                        if (deleteApplicantCertificate != null) {
                            if (deleteApplicantCertificate.getStatus() == 1) {
                            }
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error

                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse == null) {
                            //  System.out.println("API$$$$$ property_detail networkResponse.toString() = " + networkResponse.toString());
                        }
                    }
                }
        );
        putRequest.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(putRequest);

    }*/

  /*  private void GetApplicantExperience(String APPLICATEI_ID, String EXPERIENCE_ID) {

        String Url = URLS.ApplicantExperience + APPLICATEI_ID + "&"
                + URLS.EXPERIENCE_ID + "=" + EXPERIENCE_ID;
        System.out.println("API$$$$$ GetApplicantExperience URL " + Url);
        StringRequest putRequest = new StringRequest(Request.Method.POST, Url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println("API$$$$$ GetApplicantExperience response " + response);
                        Gson gson = new Gson();
                        ApplicantExperience applicantExperience = gson.fromJson(response, ApplicantExperience.class);
                        if (applicantExperience != null) {
                            if (applicantExperience.getStatus() == 1) {


                            }
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error

                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse == null) {
                            //  System.out.println("API$$$$$ property_detail networkResponse.toString() = " + networkResponse.toString());
                        }
                    }
                }
        );
        putRequest.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(putRequest);

    }*/

   /* private void GetInsertUpdateApplicantExperience() {

        String Url = URLS.INSERTUPDATEAPPLICANTEXPERIENCE + "11" + "&"
                + URLS.EXPERIENCE_ID + "=" + "&"
                + URLS.LOCATION + "=" + "ABC  " + "&"
                + URLS.SPECIALITY + "=" + "special" + "&"
                + URLS.MONTH + "=" + "5" + "&"
                + URLS.YEAR + "=" + "01";
        System.out.println("API$$$$$ GetInsertUpdateApplicantExperience URL " + Url);
        StringRequest putRequest = new StringRequest(Request.Method.POST, Url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println("API$$$$$ GetInsertUpdateApplicantExperience response " + response);
                        Gson gson = new Gson();
                        DeleteApplicantCertificate insertupdate = gson.fromJson(response, DeleteApplicantCertificate.class);
                        if (insertupdate != null) {
                            if (insertupdate.getStatus() == 1) {


                            }
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error

                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse == null) {
                            //  System.out.println("API$$$$$ property_detail networkResponse.toString() = " + networkResponse.toString());
                        }
                    }
                }
        );
        putRequest.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(putRequest);

    }*/

   /* private void GetDeleteApplicantExperience() {

        String Url = URLS.DELETEAPPLICANTEXPERIENCE + "11" + "&"
                + URLS.EXPERIENCE_ID + "=" + "11";
        System.out.println("API$$$$$ GetDeleteApplicantExperience URL " + Url);
        StringRequest putRequest = new StringRequest(Request.Method.POST, Url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println("API$$$$$ GetDeleteApplicantExperience response " + response);
                        Gson gson = new Gson();
                        DeleteApplicantCertificate deleteApplicantCertificate = gson.fromJson(response, DeleteApplicantCertificate.class);
                        if (deleteApplicantCertificate != null) {
                            if (deleteApplicantCertificate.getStatus() == 1) {
                            }
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error

                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse == null) {
                            //  System.out.println("API$$$$$ property_detail networkResponse.toString() = " + networkResponse.toString());
                        }
                    }
                }
        );
        putRequest.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(putRequest);

    }*/

  /*  private void GetManagerProfile() {

        String Url = URLS.GetManagerProfile + "10";
        System.out.println("API$$$$$ GetManagerProfile URL " + Url);
        StringRequest putRequest = new StringRequest(Request.Method.POST, Url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println("API$$$$$ GetManagerProfile response " + response);
                        Gson gson = new Gson();
                        ManagerProfile ManagerProfile = gson.fromJson(response, ManagerProfile.class);
                        if (ManagerProfile != null) {
                            if (ManagerProfile.getStatus() == 1) {
                            }
                        }


                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error

                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse == null) {
                            //  System.out.println("API$$$$$ property_detail networkResponse.toString() = " + networkResponse.toString());
                        }
                    }
                }
        );
        putRequest.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(putRequest);

    }*/

    private void GetUpdateManagerProfile() {

        String Url = URLS.GetManagerProfile + "10" + "&"
                + URLS.FIRST_NAME + "=" + "aaaa&"
                + URLS.LAST_NAME + "=" + "bbbb" + "&"
                + URLS.EAMIL + "=" + "aa@aa.aa" + "&"
                + URLS.PHONE + "=" + "987654321" + "&"
                + URLS.ZIPCODE + "=" + "396369";
        System.out.println("API$$$$$ GetUpdateManagerProfile URL " + Url);
        StringRequest putRequest = new StringRequest(Request.Method.POST, Url,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        System.out.println("API$$$$$ GetUpdateManagerProfile response " + response);
                        Gson gson = new Gson();
                        /*ManagerProfile ManagerProfile = gson.fromJson(response, ManagerProfile.class);
                        if (ManagerProfile != null) {
                            if (ManagerProfile.getStatus() == 1) {
                            }
                        }
*/

                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        // error

                        NetworkResponse networkResponse = error.networkResponse;
                        if (networkResponse == null) {
                            //  System.out.println("API$$$$$ property_detail networkResponse.toString() = " + networkResponse.toString());
                        }
                    }
                }
        );
        putRequest.setRetryPolicy(new DefaultRetryPolicy(
                30000,
                0,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(putRequest);

    }
}
