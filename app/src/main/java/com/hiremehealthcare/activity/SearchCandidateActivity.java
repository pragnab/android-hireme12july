package com.hiremehealthcare.activity;

import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Spannable;
import android.text.SpannableString;
import android.util.Log;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.SubMenu;
import android.widget.Spinner;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.hiremehealthcare.CommonCls.DialogUtils;
import com.hiremehealthcare.CommonCls.ExpandableHeightGridView;
import com.hiremehealthcare.CommonCls.URLS;
import com.hiremehealthcare.POJO.License;
import com.hiremehealthcare.POJO.Speciality;
import com.hiremehealthcare.R;
import com.hiremehealthcare.Utils.CustomTypefaceSpan;
import com.hiremehealthcare.adapter.LicenseAdapter;
import com.hiremehealthcare.adapter.SpecialityAdapter;
import com.hiremehealthcare.adapter.SpinnerAdapter;

import java.util.ArrayList;
import java.util.List;

public class SearchCandidateActivity extends AppCompatActivity {

    private com.hiremehealthcare.CommonCls.ExpandableHeightGridView gridlicence;
    private com.hiremehealthcare.CommonCls.ExpandableHeightGridView grid_certification;
    private com.hiremehealthcare.CommonCls.ExpandableHeightGridView grid_speciality;
    LicenseAdapter licenseAdapter;
    RequestQueue queue;
    SpecialityAdapter specialityAdapter;
    Spinner spWithinMile;
    List<String> stringList;
    String TAG = "SearchCandidateActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_candidate);
        initAllControls();
    }

    public void initAllControls() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Search Candidate");
        this.gridlicence = (ExpandableHeightGridView) findViewById(R.id.grid_licence);
        this.grid_certification = (ExpandableHeightGridView) findViewById(R.id.grid_certification);
        this.grid_speciality = (ExpandableHeightGridView) findViewById(R.id.grid_speciality);
        spWithinMile = (Spinner) findViewById(R.id.spWithinMile);
        this.gridlicence.setExpanded(true);
        this.grid_certification.setExpanded(true);
        grid_speciality.setExpanded(true);
        queue = Volley.newRequestQueue(this);
        stringList = new ArrayList<>();
        stringList.add("15 miles");
        stringList.add("30 miles");
        stringList.add("60 miles");
        SpinnerAdapter spinnerAdapter = new SpinnerAdapter(SearchCandidateActivity.this, stringList);
        spWithinMile.setAdapter(spinnerAdapter);
        getLicense();
        getspeciality();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        Typeface font = Typeface.createFromAsset(
                getAssets(),
                "fonts/PoppinsRegular.otf");
        // Get the root inflator.
        for (int i = 0; i < menu.size(); i++) {
            MenuItem mi = menu.getItem(i);
            //for aapplying a font to subMenu ...
            SubMenu subMenu = mi.getSubMenu();
            if (subMenu != null && subMenu.size() > 0) {
                for (int j = 0; j < subMenu.size(); j++) {
                    MenuItem subMenuItem = subMenu.getItem(j);
                    applyFontToMenuItem(subMenuItem, font);
                }
            }
            //the method we have create in activity
            applyFontToMenuItem(mi, font);
        }

        return super.onCreateOptionsMenu(menu);
        //return true;
    }

    private void applyFontToMenuItem(MenuItem mi, Typeface font) {
        SpannableString mNewTitle = new SpannableString(mi.getTitle());
        mNewTitle.setSpan(new CustomTypefaceSpan("", font), 0, mNewTitle.length(), Spannable.SPAN_INCLUSIVE_INCLUSIVE);
        mi.setTitle(mNewTitle);
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.help:

//                photoCameraIntent();
                break;
            case R.id.privacyAndPolicy:

                break;
            case R.id.termsAndCondition:

                break;
            case R.id.faq:

                break;
            case R.id.contactUs:

                break;
            case R.id.logout:

                break;
            case android.R.id.home:
                onBackPressed();
                break;
        }

        return super.onOptionsItemSelected(item);
    }


    /**
     * getting speciality from server
     */


    private void getspeciality() {
        String Url = URLS.SPECIALTY_URL;
        Log.d(TAG, "getspeciality: URL:- " + Url);
        StringRequest req = new StringRequest(Request.Method.GET, Url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "getspeciality: response:- " + response);
                Gson gson = new Gson();
                Speciality speciality = gson.fromJson(response, Speciality.class);
//                Type collectionType = new TypeToken<Collection<Speciality>>() {
//                }.getType();
//                Collection<Speciality> speciality = gson.fromJson(response, collectionType);
                if (speciality != null) {

                    System.out.println("TEST " + speciality.getSpecialty().get(0).getSpecialty_id() + "");
                    System.out.println("TEST " + speciality.getSpecialty().get(0).getSpecialty_name() + "");
                    System.out.println("TEST " + speciality.getSpecialty().size() + "");
//                    for (int i = 0; i < speciality.getSpecialty().size(); i++) {
//                        addRadioButtons(i, rootView, speciality.getSpecialty().get(i).getSpecialty_name());
//                    }
                    specialityAdapter = new SpecialityAdapter(speciality, SearchCandidateActivity.this);
                    grid_speciality.setAdapter(specialityAdapter);

//                  for(int i=0;i<speciality.size();i++)
//                  {
//
//                  }

                } else {
                    // DialogUtils.showDialog4Activity(LoginActivity.this, context.getResources().getString(R.string.app_name), logindata.getMessage() + "");

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "getspeciality: response:- " + error.getMessage());
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    int statusCode = error.networkResponse.statusCode;
                    System.out.println("STATUS CODE " + statusCode + "");
                    DialogUtils.showDialog4Activity(SearchCandidateActivity.this, getResources().getString(R.string.app_name), statusCode + "", null);
                }

            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(req);

    }

    /**
     * getting License from server
     */
    private void getLicense() {
        String Url = URLS.LICENSE_URL;
        Log.d(TAG, "getLicense: URL :- " + Url);
        StringRequest req = new StringRequest(Request.Method.GET, Url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "getLicense: Response :- " + response);
                Gson gson = new Gson();
                License license = gson.fromJson(response, License.class);
//                Type collectionType = new TypeToken<Collection<License>>() {
//                }.getType();
//                Collection<License> license = gson.fromJson(response, collectionType);
                if (license != null) {
                    licenseAdapter = new LicenseAdapter(license, SearchCandidateActivity.this);
                    gridlicence.setAdapter(licenseAdapter);
                    grid_certification.setAdapter(licenseAdapter);
                } else {
                    // DialogUtils.showDialog4Activity(LoginActivity.this, context.getResources().getString(R.string.app_name), logindata.getMessage() + "");

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "getLicense: Error :- " + error.getMessage());
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    int statusCode = error.networkResponse.statusCode;
                    System.out.println("STATUS CODE " + statusCode + "");
                    DialogUtils.showDialog4Activity(SearchCandidateActivity.this, getResources().getString(R.string.app_name), statusCode + "", null);
                }

            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(req);
    }
}
