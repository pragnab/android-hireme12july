package com.hiremehealthcare.activity;

import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.hiremehealthcare.CommonCls.CustomEditText;
import com.hiremehealthcare.CommonCls.CustomTextView;
import com.hiremehealthcare.CommonCls.DialogUtils;
import com.hiremehealthcare.CommonCls.SharedPrefNames;
import com.hiremehealthcare.CommonCls.URLS;
import com.hiremehealthcare.CommonCls.Validations;
import com.hiremehealthcare.R;
import com.hiremehealthcare.adapter.SpinnerAdapter;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * This class is use to forgot password screen
 */
public class ForgotPasswordActivity extends AppCompatActivity implements View.OnClickListener {

    Spinner spForgotUserType;
    CustomEditText etForgotEmail;
    Button btnForgotPassword;
    List<String> stringList;
    RequestQueue queue;
    ProgressDialog progressDialog;
    String TAG = "ForgotPasswordActivity";
    CustomTextView tvIhavePassword;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        initAllControls();
    }

    /**
     * Init AllControls basic init all controls
     */

    public void initAllControls() {
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading");
        progressDialog.setCancelable(false);
        queue = Volley.newRequestQueue(this);
        getSupportActionBar().setTitle("Forgot Password");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        spForgotUserType = (Spinner) findViewById(R.id.spForgotUserType);
        etForgotEmail = (CustomEditText) findViewById(R.id.etForgotEmail);
        btnForgotPassword = (Button) findViewById(R.id.btnForgotPassword);
        stringList = new ArrayList<>();
        stringList.add("Applicant");
        stringList.add("Manager");
        tvIhavePassword = (CustomTextView) findViewById(R.id.tvIhavePassword);
        tvIhavePassword.setOnClickListener(this);
        SpinnerAdapter spinnerAdapter = new SpinnerAdapter(ForgotPasswordActivity.this, stringList);
        spForgotUserType.setAdapter(spinnerAdapter);
        btnForgotPassword.setOnClickListener(this);
        SharedPreferences sharedPreferences = getSharedPreferences(SharedPrefNames.MY_PREFS_NAME, MODE_PRIVATE);
        if (sharedPreferences.getString(SharedPrefNames.USER_PROFESSION, "").equals("manager")) {
            spForgotUserType.setSelection(1);
        } else {
            spForgotUserType.setSelection(0);
        }
        spForgotUserType.setVisibility(View.GONE);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home)
            onBackPressed();
        return true;
    }

    @Override
    public void onClick(View view) {
        if (btnForgotPassword == view) {

            onForgotPasswordButtonClicked();
        } else if (view == tvIhavePassword) {
            finish();
        }
    }

    /**
     * this method call when user clicked on forgot password
     */

    public void onForgotPasswordButtonClicked() {
        String email = etForgotEmail.getText().toString();
        boolean isApplicant = spForgotUserType.getSelectedItem().toString().equals(stringList.get(0));
        if (email.trim().length() == 0) {
            Toast.makeText(this, "Please Enter Email", Toast.LENGTH_LONG).show();
        } else if (!Validations.isValidEmail(email)) {
            Toast.makeText(ForgotPasswordActivity.this, "Please Enter Valid Email", Toast.LENGTH_LONG).show();
        } else
            sendForgotPasswordDataToServer(email, isApplicant);
    }

    /**
     * this method is use to send forgot detail server
     *
     * @param email       email of user
     * @param isApplicant is manager or applicant
     */

    public void sendForgotPasswordDataToServer(final String email, final boolean isApplicant) {

        progressDialog.show();
        String Url = "";
        if (isApplicant) {
            Url = URLS.FORGOT_APPLICANT_URL + "&" + URLS.EAMIL + "=" + email;
        } else {
            Url = URLS.FORGOT_MANAGER_URL + "&" + URLS.EAMIL + "=" + email;
        }
        Log.d(TAG, "sendForgotPasswordDataToServer: URL :- " + Url);
        StringRequest req = new StringRequest(Request.Method.GET, Url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                Log.d(TAG, "sendForgotPasswordDataToServer: Response :- " + response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getInt("status") == 1) {
                        DialogUtils.showDialog4Activity(ForgotPasswordActivity.this, getResources().getString(R.string.app_name), jsonObject.getString("message") + "", new DialogUtils.DailogCallBackOkButtonClick() {
                            @Override
                            public void onDialogOkButtonClicked() {
                                finish();
                            }
                        });
                    } else {
                        DialogUtils.showDialog4Activity(ForgotPasswordActivity.this, getResources().getString(R.string.app_name), jsonObject.getString("message") + "", null);
                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                NetworkResponse networkResponse = error.networkResponse;
                DialogUtils.noInterNetDialog(ForgotPasswordActivity.this, "No Internet Connection", "No active internet connection available", new DialogUtils.NoInterNetDailogClickListner() {
                    @Override
                    public void onDialogOkButtonClicked() {
                        sendForgotPasswordDataToServer(email, isApplicant);
                    }

                    @Override
                    public void onDialogCancelButtonClicked() {
                        finish();
                    }
                });

            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(req);
    }
}
