package com.hiremehealthcare.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.Spanned;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;

import com.android.volley.VolleyError;
import com.bumptech.glide.Glide;
import com.google.gson.Gson;
import com.hiremehealthcare.activity.ApplicantInterViewTimeActivity;
import com.hiremehealthcare.CommonCls.CustomBoldTextView;
import com.hiremehealthcare.CommonCls.CustomButton;
import com.hiremehealthcare.CommonCls.CustomEditText;
import com.hiremehealthcare.CommonCls.CustomTextView;
import com.hiremehealthcare.CommonCls.DialogUtils;
import com.hiremehealthcare.CommonCls.URLS;
import com.hiremehealthcare.CommonCls.Validations;
import com.hiremehealthcare.activity.InterViewDatesActivity;
import com.hiremehealthcare.POJO.ChatManagerModel;
import com.hiremehealthcare.POJO.CommunicationModel;
import com.hiremehealthcare.R;
import com.hiremehealthcare.Utils.CommonRequestHelper;
import com.hiremehealthcare.adapter.ManagerChatDetailAdapter;
import com.hiremehealthcare.database.MySharedPrefereces;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

import de.hdodenhof.circleimageview.CircleImageView;

public class ManagerChatDetailActivity extends AppCompatActivity implements View.OnClickListener {

    RecyclerView rvManagerChatDetail;
    List<ChatManagerModel.Message> managerChatDetailModelList;
    ManagerChatDetailAdapter managerChatDetailAdapter;
    CommunicationModel.Datum data;
    ProgressDialog progressDialog;
    MySharedPrefereces mySharedPrefereces;
    Context context;
    String TAG = "ManagerChatDetailActivity";
    ImageView ivChatDetailCalendar, buttonMessage, iv_ex;
    CustomEditText editTextMessage;
    Timer timer;
    TimerTask hourlyTask;
    public static ManagerChatDetailActivity managerChatDetailActivity;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_manager_chat_detail);
        System.out.println("ManagerChatDetailActivity >>>> ");
        initAllControls();
    }

    public void initAllControls() {
        context = this;
        managerChatDetailActivity = this;
        data = (CommunicationModel.Datum) getIntent().getSerializableExtra("data");
        mySharedPrefereces = new MySharedPrefereces(context);
        ivChatDetailCalendar = (ImageView) findViewById(R.id.ivChatDetailCalendar);
        iv_ex = (ImageView) findViewById(R.id.ivex);
        iv_ex.setVisibility(View.INVISIBLE);
        ivChatDetailCalendar.setOnClickListener(this);
        buttonMessage = (ImageView) findViewById(R.id.buttonMessage);
        editTextMessage = (CustomEditText) findViewById(R.id.editTextMessage);
        buttonMessage.setOnClickListener(this);
        progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        SpannableString spannableString = new SpannableString(getString(R.string.please_wait));
        Typeface typefaceSpan = Validations.setTypeface(context);
        spannableString.setSpan(typefaceSpan, 0, getString(R.string.please_wait).length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        progressDialog.setMessage(spannableString);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        if (data == null) {
//            getSupportActionBar().setTitle("Rahul Bhimani");
            getSupportActionBar().setTitle("N/A");
        } else {
            getSupportActionBar().setTitle(data.getFirstname() + " " + data.getLastname());
        }
        rvManagerChatDetail = (RecyclerView) findViewById(R.id.rvManagerChatDetail);
        managerChatDetailModelList = new ArrayList<>();
        managerChatDetailAdapter = new ManagerChatDetailAdapter(managerChatDetailModelList, context);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        rvManagerChatDetail.setLayoutManager(mLayoutManager);
        rvManagerChatDetail.setItemAnimator(new DefaultItemAnimator());
        rvManagerChatDetail.setAdapter(managerChatDetailAdapter);
        if (data == null) {
            getChatDetailData();
        } else {
            getChatDataFromApi();
        }
        if (mySharedPrefereces.getUserType() == 1) {
            ivChatDetailCalendar.setVisibility(View.GONE);
            iv_ex.setVisibility(View.INVISIBLE);

            checkApplicantStatus();
        }

//        if (mySharedPrefereces.getUserType() == 1) {
        if (mySharedPrefereces.getUserType() == 2) {
            ivChatDetailCalendar.setVisibility(View.GONE);
            iv_ex.setVisibility(View.INVISIBLE);

            checkApplicantStatus();
        }

        timer = new Timer();

        hourlyTask = new TimerTask() {
            @Override
            public void run() {
                getReadLastMessage();
                // your code here...
            }
        };

// schedule the task to run starting now and then every hour...


    }

    public void checkApplicantStatus() {
        progressDialog.show();
        String Url = URLS.GET_APPLICANT_STATUS + data.getId();
        Log.d(TAG, "checkApplicantStatus: Url:- " + Url);
        CommonRequestHelper.getDataFromUrl(Url, new CommonRequestHelper.VolleyResponseCustom() {
            @Override
            public void getServerResponse(String response) {
                progressDialog.dismiss();
                Log.d(TAG, "checkApplicantStatus: Response:- " + response);
                try {
                    /*1. Interested
2. Communicate
3. Interview scheduled
4. Select scheduled
5. Offer other time
6. Reject Interview
7. Confirm Interview*/
                    JSONObject jsonObject = new JSONObject(response);
                    int status = jsonObject.getInt("status");
                    int application_status = jsonObject.getInt("application_status");
                    if (status == 1) {
                        ivChatDetailCalendar.setVisibility(View.VISIBLE);
                        if (mySharedPrefereces.getUserType() == 1) {
                            iv_ex.setVisibility(View.VISIBLE);
                        }
//                        ivChatDetailCalendar.setVisibility(View.VISIBLE);
                        System.out.println("THIS IS MAKES VISIBLE...");
                        if (application_status == 5) {
                            ivChatDetailCalendar.setVisibility(View.VISIBLE);
                            iv_ex.setVisibility(View.VISIBLE);
//                        ivChatDetailCalendar.setVisibility(View.VISIBLE);
                            System.out.println("THIS IS MAKES VISIBLE...");
                        } else if (application_status == 6) {
                            ivChatDetailCalendar.setVisibility(View.VISIBLE);
                            iv_ex.setVisibility(View.VISIBLE);
//                        ivChatDetailCalendar.setVisibility(View.VISIBLE);
                            System.out.println("THIS IS MAKES VISIBLE...");
                        } else if (application_status == 2) {
                            ivChatDetailCalendar.setVisibility(View.VISIBLE);
                            iv_ex.setVisibility(View.INVISIBLE);
//                        ivChatDetailCalendar.setVisibility(View.VISIBLE);
                            System.out.println("THIS IS MAKES VISIBLE...");
                        } else if (application_status == 7) {
                            ivChatDetailCalendar.setVisibility(View.VISIBLE);
                            iv_ex.setVisibility(View.VISIBLE);
//                        ivChatDetailCalendar.setVisibility(View.VISIBLE);
                            System.out.println("THIS IS MAKES VISIBLE...");
                        } else {
                            iv_ex.setVisibility(View.INVISIBLE);
                            if (mySharedPrefereces.getUserType() == 1) {
                                iv_ex.setVisibility(View.VISIBLE);
                            }
                        }


                    } else {
                        ivChatDetailCalendar.setVisibility(View.GONE);
                        iv_ex.setVisibility(View.INVISIBLE);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void getVolleyError(VolleyError volleyError) {
                progressDialog.dismiss();
                Log.d(TAG, "checkApplicantStatus: Error:- " + volleyError.getMessage());
                DialogUtils.noInterNetDialog(context, "No Internet Connection", "No active internet connection available", new DialogUtils.NoInterNetDailogClickListner() {
                    @Override
                    public void onDialogOkButtonClicked() {
                        checkApplicantStatus();
                    }

                    @Override
                    public void onDialogCancelButtonClicked() {
                        finish();
                    }
                });


            }
        }, context);
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        timer.cancel();
    }

    public void getChatDataFromApi() {
        //progressDialog.show();

        String Url = "";
        if (mySharedPrefereces.getUserType() == 2) {

            Url = URLS.GET_CHAT_DATA + "&" +
                    URLS.USER_TYPE + "=" + mySharedPrefereces.getUserType() + "&" +
                    URLS.APPLICANT_ID + "=" + data.getApplicantId() + "&" +
                    URLS.APPLICATION_ID + "=" + data.getId() + "&" +
                    URLS.MANAGER_ID + "=" + mySharedPrefereces.getUserId();
        } else {
            Url = URLS.GET_CHAT_DATA + "&" +
                    URLS.USER_TYPE + "=" + mySharedPrefereces.getUserType() + "&" +
                    URLS.APPLICANT_ID + "=" + mySharedPrefereces.getUserId() + "&" +
                    URLS.APPLICATION_ID + "=" + data.getId() + "&" +
                    URLS.MANAGER_ID + "=" + data.getManager_id();
        }
        Log.d(TAG, "getChatDataFromApi: URL:-" + Url);
        CommonRequestHelper.getDataFromUrl(Url, new CommonRequestHelper.VolleyResponseCustom() {
            @Override
            public void getServerResponse(String response) {
                Log.d(TAG, "getChatDataFromApi: Response:-" + response);
                progressDialog.dismiss();
                Gson gson = new Gson();
                ChatManagerModel chatManagerModel = gson.fromJson(response, ChatManagerModel.class);
                if (chatManagerModel != null) {
                    System.out.println("APPLICATION STATUS " + chatManagerModel.getapplication_status() + "");
                    String s = chatManagerModel.getapplication_status() + "";
                    if (s.contentEquals("7") || s.contentEquals("8")) {//applicant
                        ivChatDetailCalendar.setVisibility(View.GONE);
                        System.out.println("THIS IS GONE....");
                    } else {
                        System.out.println("sssss....   " + s + "");
                        //  ivChatDetailCalendar.setVisibility(View.VISIBLE);
                        if (mySharedPrefereces.getUserType() == 2) {
                            ivChatDetailCalendar.setVisibility(View.VISIBLE);
                        }


                    }
//                    if (chatManagerModel.getapplication_status() != null) {
//                        if (chatManagerModel.getapplication_status().equals("7")) {
//                            ivChatDetailCalendar.setVisibility(View.GONE);
//                            System.out.println("THIS IS GONE....");
//                        }
//                    }
//                    else {
//                        ivChatDetailCalendar.setVisibility(View.VISIBLE);
//                    }
                    managerChatDetailModelList.clear();
                    managerChatDetailModelList.addAll(chatManagerModel.getMessage());
                    timer.schedule(hourlyTask, 0l, 2000);
                    managerChatDetailAdapter.notifyDataSetChanged();
                    rvManagerChatDetail.scrollToPosition(managerChatDetailModelList.size() - 1);
                }
            }

            @Override
            public void getVolleyError(VolleyError volleyError) {
                Log.d(TAG, "getChatDataFromApi: Error:-" + volleyError.getMessage());
                DialogUtils.noInterNetDialog(context, "No Internet Connection", "No active internet connection available", new DialogUtils.NoInterNetDailogClickListner() {
                    @Override
                    public void onDialogOkButtonClicked() {
                        getChatDataFromApi();
                    }

                    @Override
                    public void onDialogCancelButtonClicked() {
                        finish();
                    }
                });

                progressDialog.dismiss();
            }
        }, context);
    }

    public void getReadLastMessage() {
        //progressDialog.show();

        String Url;
        if (mySharedPrefereces.getUserType() == 2) {
            Url = URLS.GET_LAST_READ_MESSAGE + "&" +
                    URLS.USER_TYPE + "=" + mySharedPrefereces.getUserType() + "&" +
                    URLS.APPLICANT_ID + "=" + data.getApplicantId() + "&" +
                    URLS.APPLICATION_ID + "=" + data.getId() + "&" +
                    URLS.MANAGER_ID + "=" + mySharedPrefereces.getUserId();
        } else {
            Url = URLS.GET_LAST_READ_MESSAGE + "&" +
                    URLS.USER_TYPE + "=" + mySharedPrefereces.getUserType() + "&" +
                    URLS.APPLICANT_ID + "=" + mySharedPrefereces.getUserId() + "&" +
                    URLS.APPLICATION_ID + "=" + data.getId() + "&" +
                    URLS.MANAGER_ID + "=" + data.getManager_id();
        }
        Log.d(TAG, "getChatDataFromApi: URL:-" + Url);
        CommonRequestHelper.getDataFromUrl(Url, new CommonRequestHelper.VolleyResponseCustom() {
            @Override
            public void getServerResponse(String response) {
                Log.d(TAG, "getChatDataFromApi: Response:-" + response);
                progressDialog.dismiss();
                Gson gson = new Gson();
                ChatManagerModel chatManagerModel = gson.fromJson(response, ChatManagerModel.class);
                if (chatManagerModel != null) {
                    //managerChatDetailModelList.clear();
                    managerChatDetailModelList.addAll(chatManagerModel.getMessage());

                    if (chatManagerModel.getMessage().size() > 0) {
                        managerChatDetailAdapter.notifyDataSetChanged();
                        rvManagerChatDetail.scrollToPosition(managerChatDetailModelList.size() - 1);
                    }

                }
            }

            @Override
            public void getVolleyError(VolleyError volleyError) {
                Log.d(TAG, "getChatDataFromApi: Error:-" + volleyError.getMessage());
                progressDialog.dismiss();
            }
        }, context);
    }

    public void getChatDetailData() {
        for (int counter = 0; counter < 20; counter++) {
            managerChatDetailModelList.add(new ChatManagerModel.Message());
        }
        managerChatDetailAdapter.notifyDataSetChanged();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home)
            onBackPressed();
        return true;
    }

    @Override
    public void onClick(View view) {

        if (view == ivChatDetailCalendar) {
            if (mySharedPrefereces.getUserType() == 2) {
                onCalendarButtonClicked();
            } else {
                applicantSideButtonClicked();
            }
        } else if (view == buttonMessage) {
            if (editTextMessage.getText().toString().trim().length() != 0) {
                onSendButtonClicked(editTextMessage.getText().toString().trim());
            }
        }

    }

    public void applicantSideButtonClicked() {
        Intent intent = new Intent(context, ApplicantInterViewTimeActivity.class);
        intent.putExtra("data", data);
        startActivity(intent);
    }

    /**
     * this method is used to show dialog
     */

    public void onCalendarButtonClicked() {
        final LayoutInflater inflater = LayoutInflater.from(context);
        final View dialogView = inflater.inflate(R.layout.manage_interview_dialog, null);
        ImageView ivCloseDialog = (ImageView) dialogView.findViewById(R.id.ivCloseDialog);
        CustomBoldTextView tvUserName = (CustomBoldTextView) dialogView.findViewById(R.id.tvUserName);
        CustomTextView tvLicense = (CustomTextView) dialogView.findViewById(R.id.tvLicense);
        CircleImageView ivProfile_image = (CircleImageView) dialogView.findViewById(R.id.ivProfile_image);
        CustomButton btnManageInterView = (CustomButton) dialogView.findViewById(R.id.btnManageInterView);
        CustomButton btnHired = (CustomButton) dialogView.findViewById(R.id.btnHired);
        CustomButton btnRegected = (CustomButton) dialogView.findViewById(R.id.btnRegected);

        //if(data.getImage()!=null && !data.getImage().trim().equals("")) {
        Glide.with(context)
                .load(data.getImage())
                .error(R.drawable.no_image)
                .into(ivProfile_image);
        //}
        tvUserName.setText(data.getFirstname() + " " + data.getLastname());
        tvLicense.setText(data.getApplicant_license_name());


        final AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(context, R.style.myDialog));
        final AlertDialog b = builder.create();
        //  builder.setTitle("Material Style Dialog");
        builder.setCancelable(true);
        builder.setView(dialogView);
        b.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        final AlertDialog show = builder.show();
        ivCloseDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                show.dismiss();
            }
        });
        btnManageInterView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                show.dismiss();
                Intent intent = new Intent(context, InterViewDatesActivity.class);
                intent.putExtra("data", data);
                startActivity(intent);

            }
        });

        btnHired.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                show.dismiss();
                sendHireInfoToServer();
            }
        });

        btnRegected.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                show.dismiss();
                sendRegectedInfoToServer();
            }
        });
    }


    public void sendHireInfoToServer() {
        progressDialog.show();
        String Url = URLS.SCHEDULE_INTERVIEW_HIRED +
                "&" + URLS.USER_TYPE + "=" + mySharedPrefereces.getUserType() +
                "&" + URLS.APPLICANT_ID + "=" + data.getApplicantId() +
                "&" + URLS.MANAGER_ID + "=" + mySharedPrefereces.getUserId() +
                "&" + URLS.APPLICATION_ID + "=" + data.getId();
        Log.d(TAG, "sendHireInfoToServer: Url " + Url);
        CommonRequestHelper.getDataFromUrl(Url, new CommonRequestHelper.VolleyResponseCustom() {
            @Override
            public void getServerResponse(String response) {
                progressDialog.dismiss();
                Log.d(TAG, "sendHireInfoToServer: Response " + response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getInt("status") == 1) {
                        ivChatDetailCalendar.setVisibility(View.GONE);
                    }
                    DialogUtils.showDialog4Activity(context, context.getResources().getString(R.string.app_name), jsonObject.getString("message"), null);
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void getVolleyError(VolleyError volleyError) {
                progressDialog.dismiss();
                Log.d(TAG, "sendHireInfoToServer: Error " + volleyError.getMessage());
                DialogUtils.noInterNetDialog(context, "No Internet Connection", "No active internet connection available", new DialogUtils.NoInterNetDailogClickListner() {
                    @Override
                    public void onDialogOkButtonClicked() {
                        sendHireInfoToServer();
                    }

                    @Override
                    public void onDialogCancelButtonClicked() {
                        finish();
                    }
                });

            }
        }, context);

    }

    /**
     * this method call web sevices for rejected interview
     */
    public void sendRegectedInfoToServer() {
        progressDialog.show();
        String Url = URLS.SCHEDULE_INTERVIEW_REJECTED +
                "&" + URLS.USER_TYPE + "=" + mySharedPrefereces.getUserType() +
                "&" + URLS.APPLICANT_ID + "=" + data.getApplicantId() +
                "&" + URLS.MANAGER_ID + "=" + mySharedPrefereces.getUserId() +
                "&" + URLS.APPLICATION_ID + "=" + data.getId();
        Log.d(TAG, "sendRegectedInfoToServer: URL " + Url);
        CommonRequestHelper.getDataFromUrl(Url, new CommonRequestHelper.VolleyResponseCustom() {
            @Override
            public void getServerResponse(String response) {
                progressDialog.dismiss();
                Log.d(TAG, "sendRegectedInfoToServer: Response " + response);
                JSONObject jsonObject = null;
                try {
                    jsonObject = new JSONObject(response);
                    if (jsonObject.getInt("status") == 1) {
                        ivChatDetailCalendar.setVisibility(View.GONE);
                    }
                    DialogUtils.showDialog4Activity(context, context.getResources().getString(R.string.app_name), jsonObject.getString("message"), null);
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void getVolleyError(VolleyError volleyError) {
                progressDialog.dismiss();
                Log.d(TAG, "sendRegectedInfoToServer: Error " + volleyError);
                DialogUtils.noInterNetDialog(context, "No Internet Connection", "No active internet connection available", new DialogUtils.NoInterNetDailogClickListner() {
                    @Override
                    public void onDialogOkButtonClicked() {
                        sendRegectedInfoToServer();
                    }

                    @Override
                    public void onDialogCancelButtonClicked() {
                        finish();
                    }
                });


            }
        }, context);

    }


    /**
     * this method is used to send user message
     *
     * @param msg message of write in edittext
     */

    public void onSendButtonClicked(final String msg) {
        editTextMessage.setText("");
        //progressDialog.show();
        String Url = "";
        if (mySharedPrefereces.getUserType() == 2) {

            Url = URLS.STORE_CHAT_MESSAGE + "&" +
                    URLS.USER_TYPE + "=" + mySharedPrefereces.getUserType() + "&" +
                    URLS.APPLICANT_ID + "=" + data.getApplicantId() + "&" +
                    URLS.APPLICATION_ID + "=" + data.getId() + "&" +
                    URLS.MANAGER_ID + "=" + mySharedPrefereces.getUserId() + "&" +
                    URLS.CHAT_MESSAGE + "=" + msg;
        } else {
            Url = URLS.STORE_CHAT_MESSAGE + "&" +
                    URLS.USER_TYPE + "=" + mySharedPrefereces.getUserType() + "&" +
                    URLS.APPLICANT_ID + "=" + mySharedPrefereces.getUserId() + "&" +
                    URLS.APPLICATION_ID + "=" + data.getId() + "&" +
                    URLS.MANAGER_ID + "=" + data.getManager_id() + "&" +
                    URLS.CHAT_MESSAGE + "=" + msg;
        }
        URL url = null;
        try {
            url = new URL(Url);
            URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef());
            url = uri.toURL();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "onSendButtonClicked: URL " + url.toString());
        CommonRequestHelper.getDataFromUrl(url.toString(), new CommonRequestHelper.VolleyResponseCustom() {
            @Override
            public void getServerResponse(String response) {
                Log.d(TAG, "onSendButtonClicked: Response" + response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getInt("status") == 1) {
                        //getChatDataFromApi();
                    } else {

                    }

                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void getVolleyError(VolleyError volleyError) {
                Log.d(TAG, "onSendButtonClicked: Error" + volleyError.getMessage());
                DialogUtils.noInterNetDialog(context, "No Internet Connection", "Message can't send", new DialogUtils.NoInterNetDailogClickListner() {
                    @Override
                    public void onDialogOkButtonClicked() {
                        onSendButtonClicked(msg);
                    }

                    @Override
                    public void onDialogCancelButtonClicked() {
                        finish();
                    }
                });

            }
        }, context);

    }
}
