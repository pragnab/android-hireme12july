package com.hiremehealthcare.activity;

import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;

import com.hiremehealthcare.fragment.PositionDetailFragment;
import com.hiremehealthcare.R;

public class ViewPositionDetailActivity extends AppCompatActivity {

    String position_id;
    boolean is_show_interest;
    String TAG = "ViewPositionDetailActivity";
    TabLayout tabs;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_view_position_detail);
        initAllControls();
    }

    public void initAllControls()
    {
        position_id = getIntent().getStringExtra("position_id");
        is_show_interest = getIntent().getBooleanExtra("isposition_id_show_interest",false);
        Log.d(TAG, "initAllControls: is_show_interest" + is_show_interest);
//        getSupportActionBar().setTitle("Position Detailssssssssssssssssssssss");
        getSupportActionBar().setTitle("Position Detail");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        FragmentTransaction fragmentTransaction=getSupportFragmentManager().beginTransaction();
        PositionDetailFragment positionDetailFragment=new PositionDetailFragment();
        Bundle bundle = new Bundle();
  //      position_id=bundle.getString("position_id","");

        bundle.putString("position_id", position_id+"");
        System.out.println("THIS IS POS ID "+position_id+"");
        bundle.putBoolean("is_show_interest>>>>>>>>>>>>>>>>>>>>> ",is_show_interest);
        positionDetailFragment.setArguments(bundle);
        fragmentTransaction.replace(R.id.llContainer, positionDetailFragment, "detail");
        fragmentTransaction.commit();
        tabs = (TabLayout) findViewById(R.id.tabs);
        setIconToTab();

    }
    public void setIconToTab() {
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_home_black_24dp),false);
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_search_black_24dp),false);
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_favorite_black_24dp),false);
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_chat_black_24dp),false);
// 9-june-19 pragna ppppppppppppppppppppppppppppppppppppp      tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_person_black_24dp),false);
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_receipt_black_24dp), false);
        tabs.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if(SearchPositionDetailActivity.searchPositionDetailActivity!=null)
                {
                    SearchPositionDetailActivity.searchPositionDetailActivity.finish();
                }
                if(DashBoardActivity.viewpager!=null) {
                    DashBoardActivity.viewpager.setCurrentItem(tab.getPosition());
                }
                finish();
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
        //tabs.setSelected(true,);
        //tabs.getTabAt(4).setIcon(R.drawable.ic_person_black_24dp);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home)
        {
            onBackPressed();
        }

        return true;
    }
}
