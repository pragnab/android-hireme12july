package com.hiremehealthcare.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.hiremehealthcare.CommonCls.CustomButton;
import com.hiremehealthcare.CommonCls.CustomEditText;
import com.hiremehealthcare.CommonCls.DialogUtils;
import com.hiremehealthcare.CommonCls.SharedPrefNames;
import com.hiremehealthcare.CommonCls.URLS;
import com.hiremehealthcare.CommonCls.Validations;
import com.hiremehealthcare.R;
import com.hiremehealthcare.Utils.CommonRequestHelper;
import com.hiremehealthcare.database.MySharedPrefereces;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.regex.Pattern;

/**
 * this class is use to change password of applicant or manager both
 */
public class ChangePasswordActivity extends AppCompatActivity implements View.OnClickListener {

    CustomEditText etOldPassword, etNewPassword, etConfirmPassword;
    CustomButton btnChangePasswordSave;
    private final Pattern hasUppercase = Pattern.compile("[A-Z]");
    private final Pattern hasLowercase = Pattern.compile("[a-z]");
    private final Pattern hasNumber = Pattern.compile("\\d");
    ProgressDialog progressDialog;
    MySharedPrefereces mySharedPrefereces;
    String TAG = "ChangePasswordActivity";
    TabLayout tabs;
    Context context;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_password);
        initAllControls();
    }

    public void initAllControls() {
        context = this;
        getSupportActionBar().setTitle("Change Password");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        SpannableString spannableString = new SpannableString(getString(R.string.please_wait));
        Typeface typefaceSpan = Validations.setTypeface(this);
        spannableString.setSpan(typefaceSpan, 0, getString(R.string.please_wait).length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        progressDialog.setMessage(spannableString);
        etOldPassword = (CustomEditText) findViewById(R.id.etOldPassword);
        etNewPassword = (CustomEditText) findViewById(R.id.etNewPassword);
        etConfirmPassword = (CustomEditText) findViewById(R.id.etConfirmPassword);
        btnChangePasswordSave = (CustomButton) findViewById(R.id.btnChangePasswordSave);
        btnChangePasswordSave.setOnClickListener(this);
        mySharedPrefereces = new MySharedPrefereces(this);
        tabs = (TabLayout) findViewById(R.id.tabs);
        if (mySharedPrefereces.isUserLogin()) {
            tabs.setVisibility(View.VISIBLE);
            if (mySharedPrefereces.getUserType() == 2) {
                setIconToTabManager();
            } else {
                setIconToTab();
            }
        } else {
            tabs.setVisibility(View.GONE);
        }
    }

    /**
     * bottom navigation menu for manager
     */

    public void setIconToTabManager() {

       tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_home_black_24dp), false);
//       tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_people_black_24dp), false);
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_search_black_24dp), false);
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_dashboard_black_24dp), false);
//        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_assignment_ind_black_24dp), false);
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_chat_black_24dp), false);
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_receipt_black_24dp), false);

//        tabs.getTabAt(0).setIcon(R.drawable.ic_home_black_24dp);
//        tabs.getTabAt(1).setIcon(R.drawable.ic_people_black_24dp);
//        tabs.getTabAt(2).setIcon(R.drawable.ic_search_black_24dp);
//        tabs.getTabAt(3).setIcon(R.drawable.ic_dashboard_black_24dp);
//        tabs.getTabAt(4).setIcon(R.drawable.ic_assignment_ind_black_24dp);
//        tabs.getTabAt(5).setIcon(R.drawable.ic_chat_black_24dp);
//        tabs.getTabAt(6).setIcon(R.drawable.ic_receipt_black_24dp);
        tabs.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (DashBoardManagerActivity.viewpager != null)
                    DashBoardManagerActivity.viewpager.setCurrentItem(tab.getPosition());
                finish();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });

    }

    /**
     * bottom navigation menu for applicant
     */

    public void setIconToTab() {

        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_home_black_24dp), false);
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_search_black_24dp), false);
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_favorite_black_24dp), false);
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_chat_black_24dp), false);
//        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_person_black_24dp), false);
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_receipt_black_24dp), false);

       /* tabs.getTabAt(0).setIcon(R.drawable.ic_home_black_24dp);
        tabs.getTabAt(1).setIcon(R.drawable.ic_people_black_24dp);
        tabs.getTabAt(2).setIcon(R.drawable.ic_search_black_24dp);
        tabs.getTabAt(3).setIcon(R.drawable.ic_dashboard_black_24dp);
        tabs.getTabAt(4).setIcon(R.drawable.ic_assignment_ind_black_24dp);
        tabs.getTabAt(5).setIcon(R.drawable.ic_chat_black_24dp);
        tabs.getTabAt(6).setIcon(R.drawable.ic_receipt_black_24dp);*/
        tabs.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (SearchPositionDetailActivity.searchPositionDetailActivity != null) {
                    SearchPositionDetailActivity.searchPositionDetailActivity.finish();
                }
                if (DashBoardActivity.viewpager != null)
                    DashBoardActivity.viewpager.setCurrentItem(tab.getPosition());
                finish();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
        //tabs.setSelected(true,);
        //tabs.getTabAt(4).setIcon(R.drawable.ic_person_black_24dp);


    }

    /**
     * on actionbar back button clicked
     *
     * @param item
     * @return
     */

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return true;
    }

    @Override
    public void onClick(View view) {
        if (btnChangePasswordSave == view) {
            onSaveButtonClicked();
        }
    }

    /**
     * this method called when user clicked on change password button
     */

    public void onSaveButtonClicked() {
        String oldPassword = etOldPassword.getText().toString();
        String newPassword = etNewPassword.getText().toString();
        String confirmPassword = etConfirmPassword.getText().toString();
        if (oldPassword.trim().length() == 0) {
            Toast.makeText(ChangePasswordActivity.this, "Please Enter Old Password", Toast.LENGTH_SHORT).show();
        } else if (oldPassword.trim().length() <= 6) {
            Toast.makeText(ChangePasswordActivity.this, "Please Enter Old Password Must Be 7 Characters Long!", Toast.LENGTH_SHORT).show();
        } else if (!hasNumber.matcher(oldPassword).find() || !(hasLowercase.matcher(oldPassword).find() || hasUppercase.matcher(oldPassword).find())) {
            Toast.makeText(ChangePasswordActivity.this, "Old Password must contain at least one digit or letter!", Toast.LENGTH_SHORT).show();
        } else if (newPassword.trim().length() == 0) {
            Toast.makeText(ChangePasswordActivity.this, "Please Enter New Password", Toast.LENGTH_SHORT).show();
        } else if (newPassword.trim().length() <= 6) {
            Toast.makeText(ChangePasswordActivity.this, "Please Enter New Password Must Be 7 Characters Long!", Toast.LENGTH_SHORT).show();
        } else if (!hasNumber.matcher(newPassword).find() || !(hasLowercase.matcher(newPassword).find() || hasUppercase.matcher(newPassword).find())) {
            Toast.makeText(ChangePasswordActivity.this, "New Password must contain at least one digit or letter!", Toast.LENGTH_SHORT).show();
        } else if (confirmPassword.trim().length() == 0) {
            Toast.makeText(ChangePasswordActivity.this, "Please Enter Confirm Password", Toast.LENGTH_SHORT).show();
        } else if (!confirmPassword.equals(newPassword)) {
            Toast.makeText(ChangePasswordActivity.this, "Confirm Password Not Match", Toast.LENGTH_SHORT).show();
        } else {
            changePasswordDataToServer(oldPassword, newPassword, confirmPassword);
        }
    }

    /**
     * this method is used to send change password request to server
     *
     * @param oldPassword     user in put current password
     * @param newPassword     user input new password
     * @param confirmPassword use input confirm passoword
     */

    public void changePasswordDataToServer(final String oldPassword, final String newPassword, final String confirmPassword) {
        progressDialog.show();
        SharedPreferences sharedPreferences = getSharedPreferences(SharedPrefNames.MY_PREFS_NAME, MODE_PRIVATE);
        String Url = "";
        if (sharedPreferences.getString(SharedPrefNames.USER_PROFESSION, "").equals("manager")) {
            Url = URLS.CHANGE_PASSWORD_MANAGER_URL + "&" + URLS.MANAGER_ID + "=" + mySharedPrefereces.getUserId() + "&" + URLS.OLD_PASSWORD + "=" + oldPassword + "&" + URLS.NEW_PASSWORD + "=" + newPassword + "&" + URLS.CONFIRM_PASSWORD_CHANGE + "=" + confirmPassword;
        } else {
            Url = URLS.CHANGE_PASSWORD_APPLICANT_URL + "&" + URLS.APPLICANT_ID + "=" + mySharedPrefereces.getUserId() + "&" + URLS.OLD_PASSWORD + "=" + oldPassword + "&" + URLS.NEW_PASSWORD + "=" + newPassword + "&" + URLS.CONFIRM_PASSWORD_CHANGE + "=" + confirmPassword;
        }

        URL url = null;
        try {
            url = new URL(Url);
            URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef());
            url = uri.toURL();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }


        Log.d(TAG, "changePasswordDataToServer: URL :- " + url.toString());
        CommonRequestHelper.getDataFromUrl(url.toString(), new CommonRequestHelper.VolleyResponseCustom() {
            @Override
            public void getServerResponse(String response) {
                Log.d(TAG, "changePasswordDataToServer: Response :- " + response);
                progressDialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int status = jsonObject.getInt("status");
                    if (status == 1) {
                        etOldPassword.setText("");
                        etNewPassword.setText("");
                        etConfirmPassword.setText("");
                        DialogUtils.showDialog4Activity(ChangePasswordActivity.this, getResources().getString(R.string.app_name), "" + jsonObject.getString("message"), null);
                    } else {
                        DialogUtils.showDialog4Activity(ChangePasswordActivity.this, getResources().getString(R.string.app_name), "" + jsonObject.getString("message"), null);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }

            }

            @Override
            public void getVolleyError(VolleyError volleyError) {
                Log.d(TAG, "changePasswordDataToServer: Error :- " + volleyError.getMessage());
                progressDialog.dismiss();
                DialogUtils.noInterNetDialog(ChangePasswordActivity.this, "No Internet Connection", "No active internet connection available", new DialogUtils.NoInterNetDailogClickListner() {
                    @Override
                    public void onDialogOkButtonClicked() {
                        changePasswordDataToServer(oldPassword, newPassword, confirmPassword);
                    }

                    @Override
                    public void onDialogCancelButtonClicked() {

                    }
                });
            }
        }, this);
    }
}
