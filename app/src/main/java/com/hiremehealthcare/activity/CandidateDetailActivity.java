package com.hiremehealthcare.activity;

import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import com.hiremehealthcare.R;

/**
 * this class is not use any where
 */
public class CandidateDetailActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_candidate_detail);
        initAllControls();
    }
    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }
    public void initAllControls()
    {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Candidate Detail");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        FragmentTransaction fragmentTransaction=getSupportFragmentManager().beginTransaction();
       // fragmentTransaction.replace(R.id.clCandidateDetailContainer,new ContactCandidate(),"contact_candidate");
       // fragmentTransaction.commit();

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home)
        {
            onBackPressed();
        }
        return true;
    }
}
