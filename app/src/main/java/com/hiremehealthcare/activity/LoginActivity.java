package com.hiremehealthcare.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.SpannableString;
import android.text.Spanned;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.hiremehealthcare.CommonCls.CustomEditText;
import com.hiremehealthcare.CommonCls.CustomTextView;
import com.hiremehealthcare.CommonCls.DialogUtils;
import com.hiremehealthcare.CommonCls.SharedPrefNames;
import com.hiremehealthcare.CommonCls.URLS;
import com.hiremehealthcare.CommonCls.Validations;
import com.hiremehealthcare.POJO.Login;
import com.hiremehealthcare.R;
import com.hiremehealthcare.adapter.SpinnerAdapter;
import com.hiremehealthcare.database.MySharedPrefereces;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * this class is use to varify use is valid or not
 */

public class LoginActivity extends AppCompatActivity implements View.OnClickListener {

    private CustomEditText edtemail;
    private CustomEditText edtpwd;
    private Button btnlogin;
    private RelativeLayout login;
    private CustomTextView tvreg;
    private CustomTextView tvforget;
    ImageView back;
    RequestQueue queue;
    Context ctx;
    String Email = "";
    String pwd = "";
    Spinner spLoginUserType;
    List<String> stringList;
    ProgressDialog progressDialog;
    MySharedPrefereces mySharedPrefereces;
    Toolbar toolbar;
    String TAG = "LoginActivity";
    CheckBox cbRememberMe;
    String userType = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ctx = this;
        queue = Volley.newRequestQueue(ctx);
        init();


    }

    /***
     * init AllControls and Object
     */

    private void init() {

        toolbar = (Toolbar) findViewById(R.id.toolbar);
        back=toolbar.findViewById(R.id.back);
        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
        setSupportActionBar(toolbar);
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        SpannableString spannableString = new SpannableString(getString(R.string.please_wait));
        Typeface typefaceSpan = Validations.setTypeface(this);
        spannableString.setSpan(typefaceSpan, 0, getString(R.string.please_wait).length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        progressDialog.setMessage(spannableString);
        mySharedPrefereces = new MySharedPrefereces(this);
        this.tvforget = (CustomTextView) findViewById(R.id.tv_forget);
        cbRememberMe = (CheckBox) findViewById(R.id.cbRememberMe);
        cbRememberMe.setTypeface(Validations.setTypeface(this));
        this.tvreg = (CustomTextView) findViewById(R.id.tv_reg);
        this.login = (RelativeLayout) findViewById(R.id.login);
        this.btnlogin = (Button) findViewById(R.id.btn_login);
        this.edtpwd = (CustomEditText) findViewById(R.id.edt_pwd);
        this.edtemail = (CustomEditText) findViewById(R.id.edt_email);
        stringList = new ArrayList<>();
        stringList.add("Applicant");
        stringList.add("Manager");
        spLoginUserType = (Spinner) findViewById(R.id.spLoginUserType);
        // String userName=getIntent().getStringExtra("username");
        //String password = getIntent().getStringExtra("password");


        SpinnerAdapter spinnerAdapter = new SpinnerAdapter(LoginActivity.this, stringList);
        spLoginUserType.setAdapter(spinnerAdapter);
        SharedPreferences sharedPreferences = getSharedPreferences(SharedPrefNames.MY_PREFS_NAME, MODE_PRIVATE);
        if (sharedPreferences.getString(SharedPrefNames.USER_PROFESSION, "").equals("manager")) {
            spLoginUserType.setSelection(1);
            userType = "manager";
        } else {
            spLoginUserType.setSelection(0);
            userType = "applicant";
        }
        if (mySharedPrefereces.isRememberUserPassword(userType)) {
            edtemail.setText(mySharedPrefereces.getRememberMeUserName(userType));
            edtpwd.setText(mySharedPrefereces.getRememberMePassword(userType));
            cbRememberMe.setChecked(true);
        }
        spLoginUserType.setVisibility(View.GONE);
        btnlogin.setOnClickListener(LoginActivity.this);
        tvreg.setOnClickListener(this);
        tvforget.setOnClickListener(this);


    }

    @Override
    public void onClick(View view) {
        int id = view.getId();
        switch (id) {
            case R.id.btn_login:
//                Intent i=new Intent(LoginActivity.this,DashBoardActivity.class);
//                startActivity(i);
                //user Click Login Button
                pwd = edtpwd.getText() + "".trim();
                Email = edtemail.getText() + "".trim();
                if (cbRememberMe.isChecked())
                    mySharedPrefereces.rememberMe(Email, pwd, userType);
                else
                    mySharedPrefereces.clearRemember(userType);

                try {
                    InputMethodManager imm = (InputMethodManager) ctx.getSystemService(
                            Context.INPUT_METHOD_SERVICE);
                    imm.hideSoftInputFromWindow(edtpwd.getWindowToken(), 0);
                } catch (Exception ex) {
                }
                if (validate_LoginFields()) {
                    CallLogInWS();
                }
                break;
            case R.id.tv_reg:
                //user Click Register Button
                onRegisterButtonClicked();
                break;
            case R.id.tv_forget:
                //user Click Forgot Button
                startForgotPasswordActivity();
                break;
        }
    }

    /**
     * this method is use for start forgot password activity
     */
    public void startForgotPasswordActivity() {
        Intent intent = new Intent(LoginActivity.this, ForgotPasswordActivity.class);
        startActivity(intent);
    }

    /**
     * this method is use for start Register activity
     */
    public void onRegisterButtonClicked() {
        Intent intent = new Intent(LoginActivity.this, RegisterActivity.class);
        startActivity(intent);
    }

    private boolean validate_LoginFields() {
        boolean flag = true;

        return flag;
    }

    /**
     * this method call login web service
     */

    private void CallLogInWS() {
//        getRegistrationForFirebase();
        progressDialog.show();
        String Url;
        if (spLoginUserType.getSelectedItem().toString().equals(stringList.get(0))) {

            Url = URLS.LOGIN_APPLICANT_URL + "&" + URLS.EAMIL + "=" + Email + "&" + URLS.PASSWORD + "=" + pwd + "&device_id=" + Validations.GetDeviceId(ctx) + "" + "&token=" + mySharedPrefereces.getFirebaseToken() + "";
        } else {
            Url = URLS.LOGIN_MANAGER_URL + "&" + URLS.EAMIL + "=" + Email + "&" + URLS.PASSWORD + "=" + pwd + "&device_id=" + Validations.GetDeviceId(ctx) + "" + "&token=" + mySharedPrefereces.getFirebaseToken() + "";
        }

        URL url = null;
        try {
            url = new URL(Url);
            URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef());
            url = uri.toURL();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }

        Log.d(TAG, "CallLogInWS: URL:-" + url.toString());
        StringRequest req = new StringRequest(Request.Method.GET, url.toString(), new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                // Toast.makeText(getApplicationContext(),"response"+response,Toast.LENGTH_LONG).show();
                Log.d(TAG, "CallLogInWS: Response:-" + response);
                Gson gson = new Gson();
                Login logindata = gson.fromJson(response, Login.class);

                if (logindata != null) {
                    if (logindata.getStatus() == 1) {
                        mySharedPrefereces.storeLoginData(logindata.getData().getUser_type(), logindata.getData().getUser_id(), logindata.getData().getFirstname(), logindata.getData().getZipcode(), logindata.getData().getRedirect(), logindata.getData().getRedirect_message(), logindata.getData().getFacility_id(), Email, pwd,logindata.getData().getCity()+"",logindata.getData().getImage());
                        int type = logindata.getData().getUser_type();
                        //registerFireBaseToken(logindata.getData().getUser_type(), logindata.getData().getUser_id());
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int id = jsonObject.getInt("status");
                            if (id > 0) {
                                if (type == 1) {
//if()

                                   //pppppppppppppppppppppp getRegistrationForFirebase(1+"", logindata.getData().getUser_id() + "");
                                    registerFireBaseToken(type,logindata.getData().getUser_id());
                                   /* Intent i = new Intent(LoginActivity.this, DashBoardActivity.class);
                                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(i);
                                    finish();*/
                                } else {
                                    registerFireBaseToken(type,logindata.getData().getUser_id());
                                  //pppppppppppppp  getRegistrationForFirebase(2+"", logindata.getData().getUser_id() + "");
                                 /*   Intent i = new Intent(LoginActivity.this, DashBoardManagerActivity.class);
                                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(i);
                                    finish();*/
                                }
                                //DialogUtils.showDialog4Activity(ctx, ctx.getResources().getString(R.string.app_name), jsonObject.getString("message") + "", null);
                            } else {
                                //DialogUtils.showDialog4Activity(ctx, ctx.getResources().getString(R.string.app_name), jsonObject.getString("message") + "", null);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    } else {
                        DialogUtils.showDialog4Activity(LoginActivity.this, ctx.getResources().getString(R.string.app_name), logindata.getMessage() + "", null);

                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                NetworkResponse networkResponse = error.networkResponse;
                Log.d(TAG, "CallLogInWS: Error:-" + error.getMessage());
                DialogUtils.noInterNetDialog(ctx, "No Internet Connection", "No active internet connection available", new DialogUtils.NoInterNetDailogClickListner() {
                    @Override
                    public void onDialogOkButtonClicked() {
                        CallLogInWS();
                    }

                    @Override
                    public void onDialogCancelButtonClicked() {
                        finish();
                    }
                });

            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        queue.add(req);
    }

    public void registerFireBaseToken(final int userType, final String userId) {
        progressDialog.show();
     //   String Urls = URLS.UPDATE_FIREBASE_TOKEN;//+userType+"&"+URLS.USER_ID+"="+userId+"&token="+mySharedPrefereces.getFirebaseToken();
//        String Urls = URLS.GENERATE_DEVICE_TOKEN;//+userType+"&"+URLS.USER_ID+"="+userId+"&token="+mySharedPrefereces.getFirebaseToken();
       /* String Urls = URLS.GENERATE_DEVICE_TOKEN + Validations.GetDeviceId(getApplicationContext()) + "&token=" + mySharedPrefereces.getFirebaseToken();//+userType+"&"+URLS.USER_ID+"="+userId+"&token="+mySharedPrefereces.getFirebaseToken();*/
        String Urls = URLS.GENERATE_DEVICE_TOKEN + Validations.GetDeviceId(getApplicationContext()) + "&token=" + mySharedPrefereces.getFirebaseToken()+"&"+URLS.USER_TYPE+"="+userType+"&"+URLS.USER_ID+"="+userId+"";
        System.out.println("URL ??????????????????? "+Urls+"");
        StringRequest postRequest = new StringRequest(Request.Method.GET, Urls,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        progressDialog.dismiss();
                        Log.d(TAG, "????????????????????????: Response: " + response);
                        // response
                        Log.d(" ???????? Response", response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int id = jsonObject.getInt("status");
                          //  if (id > 0) {
                                if (userType == 1) {

                                    Intent i = new Intent(LoginActivity.this, DashBoardActivity.class);
                                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(i);
                                    finish();
                                } else {
                                  Intent i = new Intent(LoginActivity.this, DashBoardManagerActivity.class);
                                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                    startActivity(i);
                                    finish();
                                }
                                //DialogUtils.showDialog4Activity(ctx, ctx.getResources().getString(R.string.app_name), jsonObject.getString("message") + "", null);
                        //    } else {
                                //DialogUtils.showDialog4Activity(ctx, ctx.getResources().getString(R.string.app_name), jsonObject.getString("message") + "", null);
                          //  }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        progressDialog.dismiss();
                        Log.d(TAG, "ERROR ????: Response: " + error.getMessage());
                        // error

                        if (userType == 1) {

                            Intent i = new Intent(LoginActivity.this, DashBoardActivity.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(i);
                            finish();
                        } else {
                            Intent i = new Intent(LoginActivity.this, DashBoardManagerActivity.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(i);
                            finish();
                        }
                        Log.d("Error.Response", error.getMessage());
                        DialogUtils.noInterNetDialog(ctx, "No Internet Connection", "No active internet connection available", new DialogUtils.NoInterNetDailogClickListner() {
                            @Override
                            public void onDialogOkButtonClicked() {
                                registerFireBaseToken(userType, userId);
                            }

                            @Override
                            public void onDialogCancelButtonClicked() {
                                finish();
                            }
                        });

                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("type", "update_token");
                params.put("user_type", userType + "");
                params.put("user_id", userId + "");
                params.put("token", mySharedPrefereces.getFirebaseToken() + "");

                /*if(isImageSelect)
                {
                    params.put("image", imgByteArray);
                }*/
                Log.d(TAG, "firebase request parameter: request parameters" + params.toString());
                return params;
            }
        };
        queue.add(postRequest);

    }

    /*Pragna*/
    public void getRegistrationForFirebase(final String UserType, String UserID) {
        //String Urls= URLS.UPDATE_FIREBASE_TOKEN;//+userType+"&"+URLS.USER_ID+"="+userId+"&token="+mySharedPrefereces.getFirebaseToken();
        //String Urls= "http://hiremehealthcare.iipldemo.com/app/api.php?";//+userType+"&"+URLS.USER_ID+"="+userId+"&token="+mySharedPrefereces.getFirebaseToken();
       // String Urls = URLS.GENERATE_DEVICE_TOKEN + Validations.GetDeviceId(getApplicationContext()) + "&token=" + mySharedPrefereces.getFirebaseToken();//+userType+"&"+URLS.USER_ID+"="+userId+"&token="+mySharedPrefereces.getFirebaseToken();
        String Urls = URLS.GENERATE_DEVICE_TOKEN + Validations.GetDeviceId(getApplicationContext()) + "&token=" + mySharedPrefereces.getFirebaseToken();//+userType+"&"+URLS.USER_ID+"="+userId+"&token="+mySharedPrefereces.getFirebaseToken();
        System.out.println("getRegistrationForFirebase111111111 LOGIN  " + Urls + "");
        Urls = Urls.replace(" ", "%20");
        StringRequest postRequest = new StringRequest(Request.Method.POST, Urls,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d(TAG, "getRegistrationForFirebase111111111: RESPONSE: " + response);
                        // response
                        Log.d("getRegistrationForFirebase ", response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int id = 10;
                             id = jsonObject.getInt("status");
                            if (id > 0) {

                                //DialogUtils.showDialog4Activity(ctx, ctx.getResources().getString(R.string.app_name), jsonObject.getString("message") + "", null);
                            } else {
                                //DialogUtils.showDialog4Activity(ctx, ctx.getResources().getString(R.string.app_name), jsonObject.getString("message") + "", null);
                            }

                            if(UserType.contentEquals("1"))
                            {
                              /*  Intent i = new Intent(LoginActivity.this, DashBoardActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(i);
                                finish();*/
                            }
                            else{
                              /*  Intent i = new Intent(LoginActivity.this, DashBoardManagerActivity.class);
                                i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                                startActivity(i);
                                finish();*/
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                            System.out.println("getRegistrationForFirebase " + e.getMessage() + "");
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, "sendProfileDataToServer: Response: " + error.getMessage());
                        // error
                        Log.d("Error.Response", error.getMessage());


                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("type", "update_token");
                params.put("user_type", mySharedPrefereces.getUserType() + "");
                params.put("user_id", mySharedPrefereces.getUserId() + "");
                params.put("token", mySharedPrefereces.getFirebaseToken() + "");
                Log.d(TAG, "firebase request parameter: request parameters" + params.toString());
                Log.d(TAG, "firebase token" + mySharedPrefereces.getFirebaseToken());
                return params;
            }
        };
        queue.add(postRequest);

    }

}
