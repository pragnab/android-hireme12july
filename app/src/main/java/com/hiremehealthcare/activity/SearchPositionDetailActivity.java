package com.hiremehealthcare.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.Spanned;
import android.util.Log;
import android.view.MenuItem;

import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.hiremehealthcare.CommonCls.DialogUtils;
import com.hiremehealthcare.CommonCls.URLS;
import com.hiremehealthcare.CommonCls.Validations;
import com.hiremehealthcare.POJO.ApplicantNearPositionModel;
import com.hiremehealthcare.R;
import com.hiremehealthcare.Utils.CommonRequestHelper;
import com.hiremehealthcare.adapter.ApplicantNearPositionAdapter;
import com.hiremehealthcare.adapter.Applicant_Near_Duplicate;
import com.hiremehealthcare.database.MySharedPrefereces;
import com.hiremehealthcare.fragment.SearchFragment;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

public class SearchPositionDetailActivity extends AppCompatActivity {

    RecyclerView rvApplicantNearPosition;
    public List<ApplicantNearPositionModel.Data> applicantNearPositionModelList;
//    public ApplicantNearPositionAdapter applicantNearPositionAdapter;
    public Applicant_Near_Duplicate applicantNearPositionAdapter;
    MySharedPrefereces mySharedPrefereces;
    ProgressDialog progressDialog;
    public static SearchPositionDetailActivity applicantNearPostionFragment;
    Context context;
    String TAG="ApplicantNearPostionFragment";
    TabLayout tabs;
    public static SearchPositionDetailActivity searchPositionDetailActivity;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_position_detail);
        initAllControls();
    }

    public void initAllControls()
    {
        context=this;
        searchPositionDetailActivity=this;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Search Position");
        mySharedPrefereces = new MySharedPrefereces(context);
        progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        SpannableString spannableString =  new SpannableString(getString(R.string.please_wait));
        Typeface typefaceSpan = Validations.setTypeface(context);
        spannableString.setSpan(typefaceSpan, 0, getString(R.string.please_wait).length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        progressDialog.setMessage(spannableString);
        rvApplicantNearPosition = (RecyclerView) findViewById(R.id.rvApplicantNearPosition);
        applicantNearPositionModelList = new ArrayList<>();
        applicantNearPositionAdapter = new Applicant_Near_Duplicate(SearchFragment.applicantNearPositionModel.getData(),true, context, new Applicant_Near_Duplicate.OnRecyclerViewItemClicked() {
            @Override
            public void onRecyclerItemClicked(ApplicantNearPositionModel.Data data) {
                Intent intent=new Intent(context,ViewPositionDetailActivity.class);
                intent.putExtra("position_id", data.getPositionId());
                intent.putExtra("is_show_interest", true);
                startActivity(intent);
            }

            @Override
            public void onHeartButtonClicked(ApplicantNearPositionModel.Data data) {
                onRecyclerViewHeartButtonClicked(data);
            }

            @Override
            public void onAvailabilityButtonClicked(ApplicantNearPositionModel.Data data) {

            }
        });
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
        rvApplicantNearPosition.setLayoutManager(mLayoutManager);
        rvApplicantNearPosition.setItemAnimator(new DefaultItemAnimator());
        rvApplicantNearPosition.setAdapter(applicantNearPositionAdapter);
        tabs = (TabLayout) findViewById(R.id.tabs);
        setIconToTab();


    }
    public void setIconToTab() {

        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_home_black_24dp),false);
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_search_black_24dp),false);
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_favorite_black_24dp),false);
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_chat_black_24dp),false);
// pppppppppppppppppppp       tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_person_black_24dp),false);
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_receipt_black_24dp),false);
        tabs.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if(DashBoardActivity.viewpager!=null)
                DashBoardActivity.viewpager.setCurrentItem(tab.getPosition());
                finish();
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
        //tabs.setSelected(true,);
        //tabs.getTabAt(4).setIcon(R.drawable.ic_person_black_24dp);


    }

    public void onRecyclerViewHeartButtonClicked(final ApplicantNearPositionModel.Data data)
    {
        progressDialog.show();
        String url= URLS.APPLICANT_SHOW_INTEREST_ON_POSITION+mySharedPrefereces.getUserId()+"&"+URLS.POSITION_ID+"="+data.getPositionId();
        Log.d(TAG, "onRecyclerViewHeartButtonClicked: URL"+url);
        CommonRequestHelper.getDataFromUrl(url, new CommonRequestHelper.VolleyResponseCustom() {

            @Override
            public void getServerResponse(String response) {
                progressDialog.dismiss();
                Log.d(TAG, "onRecyclerViewHeartButtonClicked: Response"+response);
                Gson gson = new Gson();
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int status = jsonObject.getInt("status");
                    String message = jsonObject.getString("message");
                    //getApplicantNearPosionDataFromApi();
                    DialogUtils.showDialog4Activity(context, context.getResources().getString(R.string.app_name),message, null);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
                /*ApplicantNearPositionModel applicantNearPositionModel = gson.fromJson(response, ApplicantNearPositionModel.class);
                if(applicantNearPositionModel.getStatus()==1)
                {
                    if(applicantNearPositionModel.getData().size()==0)
                        Toast.makeText(context,"No data found",Toast.LENGTH_LONG).show();
                    applicantNearPositionModelList.addAll(applicantNearPositionModel.getData());
                    applicantNearPositionAdapter.notifyDataSetChanged();
                }
                else
                {

                }*/
            }

            @Override
            public void getVolleyError(VolleyError volleyError) {
                Log.d(TAG, "onRecyclerViewHeartButtonClicked: Error"+volleyError.getMessage());
                progressDialog.dismiss();
                DialogUtils.noInterNetDialog(context, "No Internet Connection", "No active internet connection available", new DialogUtils.NoInterNetDailogClickListner() {
                    @Override
                    public void onDialogOkButtonClicked() {
                        onRecyclerViewHeartButtonClicked(data);
                    }

                    @Override
                    public void onDialogCancelButtonClicked() {

                    }
                });
            }
        },context);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if(item.getItemId()==android.R.id.home)
            onBackPressed();
        return true;
    }
}
