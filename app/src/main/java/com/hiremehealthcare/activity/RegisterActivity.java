package com.hiremehealthcare.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.text.SpannableString;
import android.text.Spanned;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.hiremehealthcare.CommonCls.CustomBoldTextView;
import com.hiremehealthcare.CommonCls.CustomButton;
import com.hiremehealthcare.CommonCls.CustomEditText;
import com.hiremehealthcare.CommonCls.CustomTextView;
import com.hiremehealthcare.CommonCls.DialogUtils;
import com.hiremehealthcare.CommonCls.ExpandableHeightGridView;
import com.hiremehealthcare.CommonCls.SharedPrefNames;
import com.hiremehealthcare.CommonCls.URLS;
import com.hiremehealthcare.CommonCls.Validations;
import com.hiremehealthcare.POJO.Cities;
import com.hiremehealthcare.POJO.GettingBillingDetailModel;
import com.hiremehealthcare.POJO.License;
import com.hiremehealthcare.POJO.RegisteModel;
import com.hiremehealthcare.POJO.States;
import com.hiremehealthcare.R;
import com.hiremehealthcare.adapter.LicenseAdapter;
import com.hiremehealthcare.adapter.SpinnerAdapter;
import com.toptoche.searchablespinnerlibrary.SearchableSpinner;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Pattern;


public class RegisterActivity extends AppCompatActivity implements View.OnClickListener {


    CustomBoldTextView tvRegisterSignIN;
    CustomEditText etRegisterConfirmPassword, edt_phn;
    CustomEditText etRegisterPassword;
    CustomEditText etRegisterZipCode;
    CustomEditText etRegisterEmail;
    CustomEditText etRegisterLastName;
    CustomEditText etRegisterFirstName;
    CustomButton btnRegister;
    RequestQueue queue;
    Spinner spRegister;
    private final Pattern hasUppercase = Pattern.compile("[A-Z]");
    private final Pattern hasLowercase = Pattern.compile("[a-z]");
    private final Pattern hasNumber = Pattern.compile("\\d");
    SharedPrefNames mySharedPreferences;
    List<String> stringList;
    ProgressDialog progressDialog;
    boolean isApplicantSelect, isValidBillingFacilityNumber;
    CustomTextView tvRegisterFacilityName, tvRegisterFacilityAddress;
    LinearLayout llFacilityNameContainer;
    String TAG = "RegisterActivity";
    CheckBox cbIAgreeTearms;
    CustomBoldTextView tvRegisterPrivacyPolicy, tvRegisterTermsAndCondition;
    Context ctx;
    States states;
    Cities cities;
    SearchableSpinner spState, spCity;
    private ExpandableHeightGridView mLicenceGrid;
    ImageView ivCloseDialog;
    com.hiremehealthcare.CommonCls.CustomButton btnSubmitBack;
    CustomTextView tv_license;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register);
//  pppppppppppppp      initView();
        initAllControls();
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        finish();
    }

    /**
     * InitAllControls and Object
     */

    public void initAllControls() {
        progressDialog = new ProgressDialog(this);
        SpannableString spannableString = new SpannableString(getString(R.string.please_wait));
        Typeface typefaceSpan = Validations.setTypeface(this);
        spannableString.setSpan(typefaceSpan, 0, getString(R.string.please_wait).length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        progressDialog.setMessage(spannableString);
        progressDialog.setCancelable(false);
        getSupportActionBar().setTitle(getString(R.string.registration));
//        getSupportActionBar().setTitle("Register");
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        queue = Volley.newRequestQueue(this);
        stringList = new ArrayList<>();
        stringList.add("Applicant");
        stringList.add("Manager");
        ctx = getApplicationContext();
        spState = (SearchableSpinner) findViewById(R.id.spState);
        spCity = (SearchableSpinner) findViewById(R.id.spCity);
        spRegister = (Spinner) findViewById(R.id.spRegister);
        getState();
        SpinnerAdapter spinnerAdapter = new SpinnerAdapter(RegisterActivity.this, stringList);
        spRegister.setAdapter(spinnerAdapter);
        tvRegisterSignIN = (CustomBoldTextView) findViewById(R.id.tvRegisterSignIN);
        cbIAgreeTearms = (CheckBox) findViewById(R.id.cbIAgreeTearms);
        cbIAgreeTearms.setTypeface(Validations.setTypeface(this));
        btnRegister = (CustomButton) findViewById(R.id.btnRegister);
        etRegisterConfirmPassword = (CustomEditText) findViewById(R.id.etRegisterConfirmPassword);
        etRegisterPassword = (CustomEditText) findViewById(R.id.etRegisterPassword);
        edt_phn = (CustomEditText) findViewById(R.id.edt_phn);
        etRegisterZipCode = (CustomEditText) findViewById(R.id.etRegisterPinCode);
        etRegisterEmail = (CustomEditText) findViewById(R.id.etRegisterEmail);
        etRegisterLastName = (CustomEditText) findViewById(R.id.etRegisterLastName);
        etRegisterFirstName = (CustomEditText) findViewById(R.id.etRegisterFirstName);
        tvRegisterFacilityName = (CustomTextView) findViewById(R.id.tvRegisterFacilityName);
        tvRegisterFacilityAddress = (CustomTextView) findViewById(R.id.tvRegisterFacilityAddress);
        tv_license = (CustomTextView) findViewById(R.id.tv_license);

//        if (spRegister.getSelectedItem().toString().equals(stringList.get(0))) {
//            tv_license.setVisibility(View.VISIBLE);
//            tv_license.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View view) {
//                    open_licenseDialog();
//                }
//            });
//        }
//        else{
//            tv_license.setVisibility(View.GONE);
//        }
        llFacilityNameContainer = (LinearLayout) findViewById(R.id.llFacilityNameContainer);
        tvRegisterPrivacyPolicy = (CustomBoldTextView) findViewById(R.id.tvRegisterPrivacyPolicy);
        tvRegisterTermsAndCondition = (CustomBoldTextView) findViewById(R.id.tvRegisterTermsAndCondition);
        btnRegister.setOnClickListener(this);
        tvRegisterSignIN.setOnClickListener(this);
        tvRegisterPrivacyPolicy.setOnClickListener(this);
        tvRegisterTermsAndCondition.setOnClickListener(this);
        etRegisterZipCode.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                if (!b) {
                    if (!isApplicantSelect) {
                        if (etRegisterZipCode.getText().toString().length() > 0) {

                            gettingFacilityDetail(etRegisterZipCode.getText().toString());
                        } else {
                            isValidBillingFacilityNumber = false;
                            llFacilityNameContainer.setVisibility(View.GONE);

                        }

                    }
                }
            }
        });

        spRegister.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                if (i == 0) {
                    etRegisterZipCode.setHint(getString(R.string.pincode));
                    isApplicantSelect = true;
                    etRegisterZipCode.setText("");
                } else {
                    etRegisterZipCode.setHint(getString(R.string.billing_confirmation_number));
                    isApplicantSelect = false;
                    etRegisterZipCode.setText("");
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> adapterView) {

            }
        });


        SharedPreferences sharedPreferences = getSharedPreferences(SharedPrefNames.MY_PREFS_NAME, MODE_PRIVATE);
        if (sharedPreferences.getString(SharedPrefNames.USER_PROFESSION, "").equals("manager")) {
            spRegister.setSelection(1);
            etRegisterZipCode.setHint(getString(R.string.billing_confirmation_number));
            isApplicantSelect = false;

        } else {
            spRegister.setSelection(0);
            etRegisterZipCode.setHint(getString(R.string.pincode));
            isApplicantSelect = true;
        }
        spRegister.setVisibility(View.GONE);
        if (spRegister.getSelectedItem().toString().equals(stringList.get(0))) {
            tv_license.setVisibility(View.VISIBLE);
            tv_license.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    open_licenseDialog();
                }
            });
        } else {
            tv_license.setVisibility(View.GONE);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
        }
        return true;
    }

    @Override
    public void onClick(View view) {

        if (view == btnRegister) {
            onRegisterButtonClicked();
        } else if (view == tvRegisterSignIN) {
            onBackPressed();
        } else if (tvRegisterPrivacyPolicy == view) {
            Intent intent = new Intent(this, PrivacyAndPolicyActivity.class);
            startActivity(intent);
        } else if (tvRegisterTermsAndCondition == view) {
            Intent intent = new Intent(this, TermsAndConditionActivity.class);
            startActivity(intent);
        }
    }

    /**
     * this method call after use clicked register button
     * this method validate user insert data
     */

    public void onRegisterButtonClicked() {

        String firstName = etRegisterFirstName.getText().toString();
        String lastName = etRegisterLastName.getText().toString();
        String email = etRegisterEmail.getText().toString();
        String zipcode = etRegisterZipCode.getText().toString();
        String password = etRegisterPassword.getText().toString();
        String confirmPassword = etRegisterConfirmPassword.getText().toString();
        String phone = edt_phn.getText().toString();


        if (firstName.trim().length() == 0) {
            Toast.makeText(RegisterActivity.this, "Please Enter First Name", Toast.LENGTH_SHORT).show();
        } else if (!Validations.isValidName(firstName)) {
            Toast.makeText(RegisterActivity.this, "Please Enter Valid First Name", Toast.LENGTH_SHORT).show();
        } else if (lastName.trim().length() == 0) {
            Toast.makeText(RegisterActivity.this, "Please Enter Last Name", Toast.LENGTH_SHORT).show();
        } else if (!Validations.isValidName(lastName)) {
            Toast.makeText(RegisterActivity.this, "Please Enter Valid Last Name", Toast.LENGTH_SHORT).show();
        } else if (email.trim().length() == 0) {
            Toast.makeText(RegisterActivity.this, "Please Enter Email", Toast.LENGTH_SHORT).show();
        } else if (!Validations.isValidEmail(email)) {
            Toast.makeText(RegisterActivity.this, "Please Enter Valid Email", Toast.LENGTH_SHORT).show();
        } else if (!Validations.isValidPhoneNumber(phone)) {
            Toast.makeText(RegisterActivity.this, "Please Enter Valid Phone Number", Toast.LENGTH_SHORT).show();
        } else if (isApplicantSelect && (zipcode.trim().length() == 0)) {
            Toast.makeText(RegisterActivity.this, "Please Enter Zip Code", Toast.LENGTH_SHORT).show();
        } else if (isApplicantSelect && zipcode.trim().length() != 5) {
            Toast.makeText(RegisterActivity.this, "Please Enter Valid Zip Code", Toast.LENGTH_SHORT).show();
        } else if (selectedStateID.contentEquals("0")) {
            Toast.makeText(RegisterActivity.this, "Please Enter Valid State ", Toast.LENGTH_SHORT).show();
        } else if (selectedCityID.contentEquals("0")) {
            Toast.makeText(RegisterActivity.this, "Please Enter Valid City ", Toast.LENGTH_SHORT).show();
        } else if (!isApplicantSelect && (zipcode.trim().length() == 0 || zipcode.trim().length() <= 5)) {
            Toast.makeText(RegisterActivity.this, "Please Enter Billing Confirmation Number", Toast.LENGTH_SHORT).show();
        } else if (!isValidBillingFacilityNumber && !isApplicantSelect) {
            Toast.makeText(RegisterActivity.this, "Please Enter Valid Billing Confirmation Number", Toast.LENGTH_SHORT).show();
        } else if (password.trim().length() == 0) {
            Toast.makeText(RegisterActivity.this, "Please Enter Password", Toast.LENGTH_SHORT).show();
        } else if (password.trim().length() <= 6) {
            Toast.makeText(RegisterActivity.this, "Please Enter Password Must Be 7 Characters Long!", Toast.LENGTH_SHORT).show();
        } else if (!hasNumber.matcher(password).find() || !(hasLowercase.matcher(password).find() || hasUppercase.matcher(password).find())) {
            Toast.makeText(RegisterActivity.this, "Password must contain at least one digit or letter!", Toast.LENGTH_SHORT).show();
        } else if (confirmPassword.trim().length() == 0) {
            Toast.makeText(RegisterActivity.this, "Please Enter Confirm Password", Toast.LENGTH_SHORT).show();
        } else if (!password.equals(confirmPassword)) {
            Toast.makeText(RegisterActivity.this, "Confirm Password Not Match ", Toast.LENGTH_SHORT).show();
        } else if (!cbIAgreeTearms.isChecked()) {
            Toast.makeText(RegisterActivity.this, "Please Agree To Privacy Policy And Terms And Conditions", Toast.LENGTH_SHORT).show();
        } else {
            if (spRegister.getSelectedItem().toString().equals(stringList.get(0))) {
                if (!license.contentEquals("")) {
//                    license = "&licence=" + license;
                    license = "&license=" + license;
                    sendingRegisterDataToServer(firstName, lastName, email, zipcode, password, phone, true);
                } else {
                    //  DialogUtils.showDialog(RegisterActivity.this, "", "Please Select Licence ");
                    Toast.makeText(RegisterActivity.this, "Please Select Licence ", Toast.LENGTH_SHORT).show();
                }

//                sendingRegisterDataToServer(firstName, lastName, email, zipcode, password, phone, true);
            } else {
                sendingRegisterDataToServer(firstName, lastName, email, zipcode, password, phone, false);

            }
        }

    }

    /**
     * this method use to varify user insert facility number
     *
     * @param facilityNumber user insert facility number
     */

    public void gettingFacilityDetail(final String facilityNumber) {
        String Url = "";
        progressDialog.show();

        Url = URLS.GETTING_BILLING_CONFIRMATION + "&" + URLS.FACILITY_NUMBER + "=" + facilityNumber;
        Log.d(TAG, "gettingFacilityDetail: URL:- " + Url);

        StringRequest req = new StringRequest(Request.Method.GET, Url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                Log.d(TAG, "gettingFacilityDetail: Response:- " + response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    int status = jsonObject.getInt("status");
                    if (status == 1) {

                        Gson gson = new Gson();
                        GettingBillingDetailModel gettingBillingDetailModel = gson.fromJson(response, GettingBillingDetailModel.class);
                        llFacilityNameContainer.setVisibility(View.VISIBLE);
                        tvRegisterFacilityName.setText(gettingBillingDetailModel.getData().getFacility_name());
                        tvRegisterFacilityAddress.setText(gettingBillingDetailModel.getData().getFacility_address());
                        isValidBillingFacilityNumber = true;

                    } else {
                        isValidBillingFacilityNumber = false;
                        llFacilityNameContainer.setVisibility(View.GONE);
                        etRegisterZipCode.setText("");
                        etRegisterZipCode.requestFocus();
                        Toast.makeText(getApplicationContext(), "" + jsonObject.getString("message"), Toast.LENGTH_LONG).show();

                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Log.d(TAG, "gettingFacilityDetail: Error:- " + error.getMessage());
                NetworkResponse networkResponse = error.networkResponse;
                DialogUtils.noInterNetDialog(RegisterActivity.this, "No Internet Connection", "No active internet connection available", new DialogUtils.NoInterNetDailogClickListner() {
                    @Override
                    public void onDialogOkButtonClicked() {
                        gettingFacilityDetail(facilityNumber);
                    }

                    @Override
                    public void onDialogCancelButtonClicked() {
                        finish();
                    }
                });
                /*if (networkResponse != null) {
                    int statusCode = error.networkResponse.statusCode;
                    System.out.println("STATUS CODE " + statusCode + "");
                    DialogUtils.showDialog4Activity(RegisterActivity.this, getResources().getString(R.string.app_name), statusCode + "", null);
                }*/

            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(req);

    }

    /**
     * this method is use to send register data
     *
     * @param firstName   first name of user
     * @param lastName    last name of user
     * @param email       email of user
     * @param zipcode     user enter zip code
     * @param password    user enter password
     * @param isApplicant is manager or applicant
     */

    public void sendingRegisterDataToServer(final String firstName, final String lastName, final String email, final String zipcode, final String password, final String phone, final boolean isApplicant) {
        String Url = "";
        progressDialog.show();
        if (isApplicant) {


//            Url = URLS.REGISTER_APPLICANT_URL + "&" + URLS.FIRST_NAME + "=" + firstName + "&" + URLS.LAST_NAME + "=" + lastName
//                    + "&" + URLS.EAMIL + "=" + email + "&" + URLS.ZIP_CODE + "=" + zipcode + "&" + URLS.PASSWORD + "=" + password + "&" + URLS.CONFIRM_PASSWORD + "=" + password + "&state_id=" + selectedStateID + "&city_id=" + selectedCityID + "" + "&" + URLS.PHONE + "=" + phone + "";
//


            Url = URLS.REGISTER_APPLICANT_URL + "&" + URLS.FIRST_NAME + "=" + firstName + "&" + URLS.LAST_NAME + "=" + lastName
                    + "&" + URLS.EAMIL + "=" + email + "&" + URLS.ZIP_CODE + "=" + zipcode + "&" + URLS.PASSWORD + "=" + password + "&" + URLS.CONFIRM_PASSWORD + "=" + password + "&state_id=" + selectedStateID + "&city_id=" + selectedCityID + "" + "&" + URLS.PHONE + "=" + phone + "" + license + "";
        } else {
            Url = URLS.REGISTER_MANAGER_URL + "&" + URLS.FIRST_NAME + "=" + firstName + "&" + URLS.LAST_NAME + "=" + lastName
                    + "&" + URLS.EAMIL + "=" + email + "&" + URLS.BILLING_CONFIRMATION_NUMBER + "=" + zipcode + "&" + URLS.PASSWORD + "=" + password + "&" + URLS.CONFIRM_PASSWORD + "=" + password + "&state_id=" + selectedStateID + "&city_id=" + selectedCityID + "" + "&" + URLS.PHONE + "=" + phone + "";
        }

        URL url = null;

        try {
            url = new URL(Url);
            URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef());
            url = uri.toURL();
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
        Log.d(TAG, "sendingRegisterDataToServer: URL:- " + url.toString());
        StringRequest req = new StringRequest(Request.Method.GET, url.toString(), new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                Log.d(TAG, "sendingRegisterDataToServer: URL:- " + response);
                Gson gson = new Gson();
                RegisteModel registeModel = gson.fromJson(response, RegisteModel.class);

                if (registeModel != null) {
                    if (registeModel.getStatus() == 1) {

//                        mySharedPreferences.storeRegisterData(registeModel.getData().getUser_type(),registeModel.getData().getUser_id(),registeModel.getData().getZipcode(),registeModel.getData().getFirstname());
                        //Intent i = new Intent(RegisterActivity.this, DashBoardActivity.class);
                        DialogUtils.showDialog4Activity(RegisterActivity.this, getResources().getString(R.string.app_name), registeModel.getMessage() + "", new DialogUtils.DailogCallBackOkButtonClick() {
                            @Override
                            public void onDialogOkButtonClicked() {
                                finish();
                            }
                        });

                        //startActivity(i);
                    } else {
                        DialogUtils.showDialog4Activity(RegisterActivity.this, getResources().getString(R.string.app_name), registeModel.getMessage() + "", null);

                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Log.d(TAG, "sendingRegisterDataToServer: URL:- " + error.getMessage());
                NetworkResponse networkResponse = error.networkResponse;
                DialogUtils.noInterNetDialog(RegisterActivity.this, "No Internet Connection", "No active internet connection available", new DialogUtils.NoInterNetDailogClickListner() {
                    @Override
                    public void onDialogOkButtonClicked() {
                        sendingRegisterDataToServer(firstName, lastName, email, zipcode, password, phone, isApplicant);
                    }

                    @Override
                    public void onDialogCancelButtonClicked() {
                        finish();
                    }
                });

            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(req);
    }

    /*get state*/
    String[] namesNH = new String[0];
    String selectedStateID = "0", selectedCityID = "0";
    ArrayList<String> STATES = new ArrayList<>();
    ArrayList<String> STATES_ID = new ArrayList<>();

    private void getState() {
        String Url = URLS.STATE_URL;

        Log.d(TAG, "STATE_URL: url:- " + Url);
        System.out.println("this is STATE_URL url " + Url + "");
        StringRequest req = new StringRequest(Request.Method.GET, Url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "STATE_URL: response:- " + response);
                Gson gson = new Gson();

                states = gson.fromJson(response, States.class);

                namesNH = new String[states.getState().size() + 1];
//                namesNH = new String[states.getState().size()];
                //    System.out.println("alreadyselected_state " + alreadyselected_state + "");
                namesNH[0] = "Select State";
                namesNHCity = new String[1];

//                namesNHCity = new String[cities.getMessage().size()];
                namesNHCity[0] = "Select City";
                ArrayAdapter arrayAdapter = new ArrayAdapter(ctx, R.layout.spinner_adapter, namesNHCity);
                spCity.setAdapter(arrayAdapter);
                //    spState.setSelection(-1);
                STATES = new ArrayList<>();
                STATES_ID = new ArrayList<>();
                for (int i = 0; i < states.getState().size(); i++) {
                    namesNH[i + 1] = states.getState().get(i).getState_name() + "";
                    STATES.add(states.getState().get(i).getState_name() + "");
                    STATES_ID.add(states.getState().get(i).getState_id() + "");
//                    namesNH[i] = states.getState().get(i).getState_name() + "";
                    //  System.out.println("alreadyselected_state states.getState().get(i).getState_id() " + states.getState().get(i).getState_id() + "");
//                    if (alreadyselected_state.compareToIgnoreCase(states.getState().get(i).getState_id()) == 0) {
//                        toselectedpos_State = i;
//                    }
                }
//                ArrayAdapter arrayAdapter1 = new ArrayAdapter(getContext(),android.R.layout.simple_spinner_dropdown_item,namesNH);
                ArrayAdapter arrayAdapter1 = new ArrayAdapter(ctx, R.layout.spinner_adapter, namesNH);
                spState.setAdapter(arrayAdapter1);

                spState.setSelection(0);
                //  getCity(states.getState().get(2).getState_id() + "");
                spState.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {
                      /*  System.out.println("this is selectesd state " + namesNH[i] + "");
                        System.out.println("this is selectesd state ID " + states.getState().get(i).getState_id() + "");
                        selectedStateID = states.getState().get(i).getState_id() + "";
                        System.out.println("this is selectesd state CODE " + states.getState().get(i).getState_code() + "");
                        getCity(states.getState().get(i).getState_id() + "");*/
                        if (i > 0) {
//                            System.out.println("this is selectesd state " + namesNH[i + 1] + "");
//                            System.out.println("this is selectesd state ID " + states.getState().get(i + 1).getState_id() + "");
//                            selectedStateID = states.getState().get(i + 1).getState_id() + "";
//                            System.out.println("this is selectesd state CODE " + states.getState().get(i + 1).getState_code() + "");
//                            getCity(states.getState().get(i + 1).getState_id() + "");


//                            System.out.println("this is selectesd state " + namesNH[i + 1] + "");
                            System.out.println("this is selectesd state " + namesNH[i] + "");
//                            System.out.println("this is selectesd state ID " + states.getState().get(i).getState_id() + "");
//                            selectedStateID = states.getState().get(i).getState_id() + "";
//                            System.out.println("this is selectesd state CODE " + states.getState().get(i).getState_code() + "");
//                            getCity(states.getState().get(i).getState_id() + "");
                            String s = namesNH[i] + "";
                            int pos = STATES.indexOf(s);
                            System.out.println("this is pos->>>>>>>>>>>>> " + pos + "");


                            selectedStateID = STATES_ID.get(pos);

                            System.out.println("this is selectedStateID->>>>>>>>>>>>> " + selectedStateID + "");
                            getCity(selectedStateID + "");
                        }
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });
                /**/
            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "getYear: error:- " + error.getMessage());
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    int statusCode = error.networkResponse.statusCode;
                    System.out.println("STATUS CODE " + statusCode + "");
                    DialogUtils.showDialog4Activity(RegisterActivity.this, ctx.getResources().getString(R.string.app_name), statusCode + "", null);
                }

            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(req);
    }

    String[] namesNHCity = new String[0];
    ArrayList<String> CITIES_ = new ArrayList<>();
    ArrayList<String> CITIES_ID = new ArrayList<>();

    private void getCity(String stateID) {
        //  toselectedpos_City = 0;
        String Url = URLS.CITY_URL + stateID + "";
        Log.d(TAG, "CITY_URL: url:- " + Url);
        System.out.println("this is CITY_URL url " + Url + "");
        StringRequest req = new StringRequest(Request.Method.GET, Url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "CITY_URL: response:- " + response);
                Gson gson = new Gson();

                cities = gson.fromJson(response, Cities.class);

                namesNHCity = new String[cities.getMessage().size() + 1];
                CITIES_ID = new ArrayList<>();
                CITIES_ = new ArrayList<>();
//                namesNHCity = new String[cities.getMessage().size()];
                namesNHCity[0] = "Select City";

                for (int i = 0; i < cities.getMessage().size(); i++) {
//                    namesNHCity[i+1] = states.getState().get(i).getState_name() + "";
//                    namesNHCity[i] = cities.getMessage().get(i).getCity_name() + "";
                    namesNHCity[i + 1] = cities.getMessage().get(i).getCity_name() + "";

                    CITIES_.add(cities.getMessage().get(i).getCity_name() + "");
                    CITIES_ID.add(cities.getMessage().get(i).getCity_id() + "");
                    /* System.out.println("alreadyselected_state cities.getMessage().get(i).getCity_id() " + cities.getMessage().get(i).getCity_id() + "");*/
//                    if (alreadyselected_city.compareToIgnoreCase(cities.getMessage().get(i).getCity_id()) == 0) {
//                        toselectedpos_City = i;
//                        System.out.println("alreadyselected_state cities.getMessage().get(i).getCity_id() " + cities.getMessage().get(i).getCity_id() + "");
//                    }
                }
//                ArrayAdapter arrayAdapter1 = new ArrayAdapter(getContext(),android.R.layout.simple_spinner_dropdown_item,namesNH);
                ArrayAdapter arrayAdapter1 = new ArrayAdapter(ctx, R.layout.spinner_adapter, namesNHCity);
                spCity.setAdapter(arrayAdapter1);
                spCity.setSelection(-1);
//                if(spState.getSelectedItemPosition()>0)
//                {
//                    spCity.setSelection(1);
//                }
                spCity.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
                    @Override
                    public void onItemSelected(AdapterView<?> adapterView, View view, int i, long l) {

                        if (i > 0) {
//                        System.out.println("this is selectesd City " + namesNHCity[i] + "");
//                            System.out.println("this is selectesd City1111111111111 " + namesNHCity[i + 1] + "");
////                        System.out.println("this is selectesd City ID " + cities.getMessage().get(i).getCity_id() + "");
//                            System.out.println("this is selectesd City ID11111111111111 " + cities.getMessage().get(i + 1).getCity_id() + "");
////                        selectedCityID = cities.getMessage().get(i).getCity_id() + "";
//                            selectedCityID = cities.getMessage().get(i + 1).getCity_id() + "";
////                        System.out.println("this is selectesd CITY state CODE:::::::: " + cities.getMessage().get(i).getCity_state_id() + "");
//                            System.out.println("this is selectesd CITY state CODE:::::::: " + cities.getMessage().get(i + 1).getCity_state_id() + "");


                            System.out.println("this is selectesd City1111111111111 " + namesNHCity[i] + "");
//                        System.out.println("this is selectesd City ID " + cities.getMessage().get(i).getCity_id() + "");
                            //  System.out.println("this is selectesd City ID11111111111111 " + cities.getMessage().get(i + 1).getCity_id() + "");
//                        selectedCityID = cities.getMessage().get(i).getCity_id() + "";
                            //  selectedCityID = cities.getMessage().get(i + 1).getCity_id() + "";
//                        System.out.println("this is selectesd CITY state CODE:::::::: " + cities.getMessage().get(i).getCity_state_id() + "");
                            //  System.out.println("this is selectesd CITY state CODE:::::::: " + cities.getMessage().get(i + 1).getCity_state_id() + "");


                            String s = namesNHCity[i] + "";
                            int pos = CITIES_.indexOf(s);
                            System.out.println("this is pos->>>>>>>>>>>>> CITIES  " + pos + "");


                            selectedCityID = CITIES_ID.get(pos);

                            System.out.println("this is selectedStateID selectedCityID->>>>>>>>>>>>> " + selectedCityID + "");


                        }
                        //   getCity(states.getState().get(i).getState_id() + "");
                    }

                    @Override
                    public void onNothingSelected(AdapterView<?> adapterView) {

                    }
                });

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "getYear: error:- " + error.getMessage());
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    int statusCode = error.networkResponse.statusCode;
                    System.out.println("STATUS CODE " + statusCode + "");
                    DialogUtils.showDialog4Activity(RegisterActivity.this, ctx.getResources().getString(R.string.app_name), statusCode + "", null);
                }

            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(req);
    }

    String license = "", license_NAME = "";

    void open_licenseDialog() {
        getLicense();
        LayoutInflater inflater = LayoutInflater.from(RegisterActivity.this);
        final View dialogView = inflater.inflate(R.layout.reg_add_license, null);
        mLicenceGrid = (ExpandableHeightGridView) dialogView.findViewById(R.id.grid_licence);
        mLicenceGrid.setExpanded(true);
        ivCloseDialog = (ImageView) dialogView.findViewById(R.id.ivCloseDialog);
        btnSubmitBack = (com.hiremehealthcare.CommonCls.CustomButton) dialogView.findViewById(R.id.btnSubmitBack);
        final AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(RegisterActivity.this, R.style.myDialog));
        final AlertDialog b = builder.create();

        //  builder.setTitle("Material Style Dialog");
        builder.setCancelable(true);
        builder.setView(dialogView);
        b.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        final AlertDialog show = builder.show();
        btnSubmitBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //   tv_license.setText("* Select Licence " + "");
                if (license_NAME != null && !license_NAME.contentEquals("")) {
                    tv_license.setText(" Selected Licence : " + license_NAME + "");
                } else {
                    tv_license.setText(" Select Licence " + "");
                }

                if (licenseAdapter != null && licenseAdapter.selected != null) {
                    license = licenseAdapter.selected.toString();
                    license_NAME = licenseAdapter.selected_item.toString();

                    license = license.replace("[", "");
                    license = license.replace("]", "");

                    license_NAME = license_NAME.replace("[", "");
                    license_NAME = license_NAME.replace("]", "");
                    license_NAME = license_NAME.replace(",", " , ");

                    if (license_NAME != null && !license_NAME.contentEquals("")) {
                        tv_license.setText(" Selected Licence : " + license_NAME + "");
                    } else {
                        tv_license.setText(" Select Licence " + "");
                    }

                    license = license.replace(" ", "") + "";
                    System.out.println("license IN REGISTER ::::::::::::::::: " + license + "");
                    show.dismiss();
                }
            }
        });


        ivCloseDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (license_NAME != null && !license_NAME.contentEquals("")) {
                    tv_license.setText(" Selected Licence : " + license_NAME + "");
                } else {
                    tv_license.setText(" Select Licence " + "");
                }

                show.dismiss();
            }
        });
    }

    private void initView() {
//        mLicenceGrid = (ExpandableHeightGridView) findViewById(R.id.grid_licence);
    }

    LicenseAdapter licenseAdapter;

    private void getLicense() {
        String Url = URLS.LICENSE_URL;
        DialogUtils.showProgressDialog(RegisterActivity.this, "");
        Log.d(TAG, "getLicense: URL" + Url);
        StringRequest req = new StringRequest(Request.Method.GET, Url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                DialogUtils.hideProgressDialog();
//                open_licenseDialog();
                Log.d(TAG, "getLicense: Response" + response);
                Gson gson = new Gson();
                License license = gson.fromJson(response, License.class);
//                Type collectionType = new TypeToken<Collection<License>>() {
//                }.getType();
//                Collection<License> license = gson.fromJson(response, collectionType);
                if (license != null && mLicenceGrid != null) {
                    licenseAdapter = new LicenseAdapter(license, ctx);
                    mLicenceGrid.setAdapter(licenseAdapter);
                    //  grid_certification.setAdapter(licenseAdapter);
                } else {
                    // DialogUtils.showDialog4Activity(LoginActivity.this, ctx.getResources().getString(R.string.app_name), logindata.getMessage() + "");

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                DialogUtils.hideProgressDialog();
                Log.d(TAG, "getLicense: Error" + error.getMessage());
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    int statusCode = error.networkResponse.statusCode;
                    System.out.println("STATUS CODE " + statusCode + "");
                    DialogUtils.showDialog4Activity(ctx, getResources().getString(R.string.app_name), statusCode + "", null);
                }

            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(req);
    }


}
