package com.hiremehealthcare.activity;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.MenuItem;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.hiremehealthcare.CommonCls.DialogUtils;
import com.hiremehealthcare.CommonCls.URLS;
import com.hiremehealthcare.POJO.SearchCandidate;
import com.hiremehealthcare.adapter.QualifiedCandidatesAdapter;
import com.hiremehealthcare.R;
import com.hiremehealthcare.database.MySharedPrefereces;

/**
 * Created by ADMIN on 25-05-2018.
 */

public class Qualified_CandidatesActivity extends AppCompatActivity {
    private RecyclerView rvHomeManagerList;
    Context context;
    String ID = "";
    MySharedPrefereces mySharedPrefereces;
    RequestQueue queue;
    String TAG = "Qualified_CandidatesActivity";
    public static Qualified_CandidatesActivity qualified_candidatesActivity;
    TabLayout tabs;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_home_manager);
        qualified_candidatesActivity=this;
        context = getApplicationContext();
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Qualified Candidates");
        Intent i = getIntent();
        ID = i.getStringExtra("ID");
        mySharedPrefereces = new MySharedPrefereces(context);
        queue = Volley.newRequestQueue(context);
        this.rvHomeManagerList = (RecyclerView) findViewById(R.id.rvHomeManagerList);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(context);
        rvHomeManagerList.setLayoutManager(mLayoutManager);
        rvHomeManagerList.setItemAnimator(new DefaultItemAnimator());
        get_qualified_candidates_for_position(ID + "");
        tabs = (TabLayout) findViewById(R.id.tabs);
        setIconToTab();


    }

    public void setIconToTab() {

        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_home_black_24dp),false);
//        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_people_black_24dp),false);
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_search_black_24dp),false);
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_dashboard_black_24dp),false);
//        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_assignment_ind_black_24dp),false);
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_chat_black_24dp),false);
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_receipt_black_24dp),false);
        tabs.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if(DashBoardManagerActivity.viewpager!=null)
                DashBoardManagerActivity.viewpager.setCurrentItem(tab.getPosition());
                finish();
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
        //tabs.setSelected(true,);
        //tabs.getTabAt(4).setIcon(R.drawable.ic_person_black_24dp);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }

        return true;
    }

    /**
     * this method is used getting qulified candidates for position
     *
     * @param PosID
     */

    private void get_qualified_candidates_for_position(final String PosID) {
        DialogUtils.showProgressDialog(Qualified_CandidatesActivity.this, "Please Wait...");

        String Url = URLS.MANAGER_GET_QUALIFIED_CANDIDATES_FOR_POSITION + mySharedPrefereces.getUserId() + "&manager_zipcode=" + mySharedPrefereces.getZipCode() + "&position_id=" + PosID + "";
        Log.d(TAG, "get_qualified_candidates_for_position: URL " + Url);
        StringRequest req = new StringRequest(Request.Method.GET, Url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "get_qualified_candidates_for_position: Response" + response);
                DialogUtils.hideProgressDialog();
                Gson gson = new Gson();
                SearchCandidate searchCandidate = gson.fromJson(response, SearchCandidate.class);
////                Type collectionType = new TypeToken<Collection<Speciality>>() {
////                }.getType();
////                Collection<Speciality> speciality = gson.fromJson(response, collectionType);
                if (searchCandidate != null) {
                    if (searchCandidate.getData().size() > 0) {
                        QualifiedCandidatesAdapter homeManagerAdapter = new QualifiedCandidatesAdapter(searchCandidate, context, new QualifiedCandidatesAdapter.OnQulifiedCandidateItemClicked() {
                            @Override
                            public void onQulifiedCandidateItemClicked(SearchCandidate.DataBean data) {
                                onQualifiedDataClicked(data);
                            }
                        });
                        if (homeManagerAdapter != null) {
                            homeManagerAdapter.notifyDataSetChanged();
                            rvHomeManagerList.setAdapter(homeManagerAdapter);
                        }
                    } else {
                        DialogUtils.showDialog(Qualified_CandidatesActivity.this, context.getResources().getString(R.string.app_name), "No Qualified Candidates for this Position");
                        finish();
                        onBackPressed();
                    }
                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                NetworkResponse networkResponse = error.networkResponse;
                DialogUtils.hideProgressDialog();
                Log.d(TAG, "get_qualified_candidates_for_position: Error" + error.getMessage());
                DialogUtils.noInterNetDialog(context, "No Internet Connection", "No active internet connection available", new DialogUtils.NoInterNetDailogClickListner() {
                    @Override
                    public void onDialogOkButtonClicked() {
                        get_qualified_candidates_for_position(PosID);
                    }

                    @Override
                    public void onDialogCancelButtonClicked() {
                        finish();
                    }
                });


            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(req);

    }

    public void onQualifiedDataClicked(SearchCandidate.DataBean data)
    {
        Intent i = new Intent(context,
                ContactCandidate.class);

        i.putExtra("ID", data.getId()+"");
        i.putExtra("IMAGE", data.getImage()+"");
        i.putExtra("position_id", Integer.parseInt(ID+""));
        startActivity(i);
    }

}
