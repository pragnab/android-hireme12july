package com.hiremehealthcare.activity;

import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MenuItem;

import com.hiremehealthcare.fragment.PostNewPostion;
import com.hiremehealthcare.R;

public class PostNewPositionActivity extends AppCompatActivity {

    TabLayout tabs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_post_new_position);
        initAllControls();
    }

    public void initAllControls() {
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Post New Position");
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.llPositionPost, new PostNewPostion(), "test");
        fragmentTransaction.commit();
        tabs = (TabLayout) findViewById(R.id.tabs);
        setIconToTab();
    }

    public void setIconToTab() {

        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_home_black_24dp), false);
//        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_people_black_24dp), false);
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_search_black_24dp), false);
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_dashboard_black_24dp), false);
//        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_assignment_ind_black_24dp), false);
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_chat_black_24dp), false);
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_receipt_black_24dp), false);
        tabs.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (DashBoardManagerActivity.viewpager != null)
                    DashBoardManagerActivity.viewpager.setCurrentItem(tab.getPosition());
                finish();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
        //tabs.setSelected(true,);
        //tabs.getTabAt(4).setIcon(R.drawable.ic_person_black_24dp);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }

        return true;
    }
}
