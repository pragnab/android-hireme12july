package com.hiremehealthcare.activity;

import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.android.volley.VolleyError;
import com.google.gson.Gson;
import com.hiremehealthcare.CommonCls.CustomButton;
import com.hiremehealthcare.CommonCls.CustomTextView;
import com.hiremehealthcare.CommonCls.DialogUtils;
import com.hiremehealthcare.CommonCls.URLS;
import com.hiremehealthcare.CommonCls.Validations;
import com.hiremehealthcare.POJO.CommunicationModel;
import com.hiremehealthcare.POJO.ExperienceListModel;
import com.hiremehealthcare.POJO.GetInterViewTimming;
import com.hiremehealthcare.R;
import com.hiremehealthcare.Utils.CommonRequestHelper;
import com.hiremehealthcare.adapter.SpinnerAdapter;
import com.hiremehealthcare.database.MySharedPrefereces;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

//import cn.pedant.SweetAlert.SweetAlertDialog;

public class InterViewDatesActivity extends AppCompatActivity implements View.OnClickListener {
    /*25 July Pragna*/
    ImageView iv_1, iv_2, iv_3;
    ImageView ivSelectDateOption1, ivSelectDateOption2, ivSelectDateOption3;
    Spinner spHoursOption1, spMinutesOption1, spAMPMOption1,
            spHoursOption2, spMinutesOption2, spAMPMOption2,
            spHoursOption3, spMinutesOption3, spAMPMOption3;
    private Date selectedDate;
    Context context;
    SimpleDateFormat sdf_full;
    SimpleDateFormat server_date_format;
    SimpleDateFormat fill_date_format;
    SimpleDateFormat userShowFormat;
    CustomTextView tvSelectDateOption1, tvDateShowOption2, tvDateShowOption3;
    CustomButton btnSaveDatesOption1, btnSaveDatesOption2, btnSaveDateOption3, btnSendInterViewRequest;
    List<String> hoursList;
    List<String> minutesList;
    List<String> amPMList;

    String stringDateOption1 = "", stringDateOption2 = "", stringDateOption3 = "";
    Date dateOption1, dateOption2, dateOption3;
    CommunicationModel.Datum data;
    ProgressDialog progressDialog;
    String TAG = "InterViewDatesActivity";
    boolean isFirstInserted, isSecondInserted, isThirdInserted;
    GetInterViewTimming getInterViewTimming;
    String interview_schedule_id = "";
    MySharedPrefereces mySharedPrefereces;
    TabLayout tabs;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inter_view_dates);
        initAllControls();
    }

    public void initAllControls() {
        context = this;
        mySharedPrefereces = new MySharedPrefereces(context);
        /*25 july Pragna*/
        iv_1 = (ImageView) findViewById(R.id.iv_1);
        iv_2 = (ImageView) findViewById(R.id.iv_2);
        iv_3 = (ImageView) findViewById(R.id.iv_3);
        iv_1.setVisibility(View.GONE);
        iv_2.setVisibility(View.GONE);
        iv_3.setVisibility(View.GONE);

        data = (CommunicationModel.Datum) getIntent().getSerializableExtra("data");
        sdf_full = new SimpleDateFormat("dd-MM-yyyy", java.util.Locale.getDefault());
        server_date_format = new SimpleDateFormat("MM-dd-yyyy", java.util.Locale.getDefault());
        fill_date_format = new SimpleDateFormat("yyyy-MM-dd kk:mm", java.util.Locale.getDefault());
        userShowFormat = new SimpleDateFormat("MMMM, dd, yyyy", java.util.Locale.getDefault());
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Interview Dates");
        hoursList = new ArrayList<>();
        minutesList = new ArrayList<>();
        addMinutesData();
        amPMList = new ArrayList<>();
        amPMList.add("am");
        amPMList.add("pm");
        addHourDataInList();
        progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        SpannableString spannableString = new SpannableString(getString(R.string.please_wait));
        Typeface typefaceSpan = Validations.setTypeface(context);
        spannableString.setSpan(typefaceSpan, 0, getString(R.string.please_wait).length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        progressDialog.setMessage(spannableString);

        spHoursOption1 = (Spinner) findViewById(R.id.spHoursOption1);
        spMinutesOption1 = (Spinner) findViewById(R.id.spMinutesOption1);
        spAMPMOption1 = (Spinner) findViewById(R.id.spAMPMOption1);

        spHoursOption2 = (Spinner) findViewById(R.id.spHoursOption2);
        spMinutesOption2 = (Spinner) findViewById(R.id.spMinutesOption2);
        spAMPMOption2 = (Spinner) findViewById(R.id.spAMPMOption2);


        spHoursOption3 = (Spinner) findViewById(R.id.spHoursOption3);
        spMinutesOption3 = (Spinner) findViewById(R.id.spMinutesOption3);
        spAMPMOption3 = (Spinner) findViewById(R.id.spAMPMOption3);

        tvSelectDateOption1 = (CustomTextView) findViewById(R.id.tvSelectDateOption1);
        tvDateShowOption2 = (CustomTextView) findViewById(R.id.tvDateShowOption2);
        tvDateShowOption3 = (CustomTextView) findViewById(R.id.tvDateShowOption3);


        btnSaveDatesOption1 = (CustomButton) findViewById(R.id.btnSaveDatesOption1);
        btnSaveDatesOption2 = (CustomButton) findViewById(R.id.btnSaveDatesOption2);
        btnSaveDateOption3 = (CustomButton) findViewById(R.id.btnSaveDateOption3);
        btnSendInterViewRequest = (CustomButton) findViewById(R.id.btnSendInterViewRequest);

        btnSaveDatesOption1.setOnClickListener(this);
        btnSaveDatesOption2.setOnClickListener(this);
        btnSaveDateOption3.setOnClickListener(this);
        btnSendInterViewRequest.setOnClickListener(this);


        ivSelectDateOption1 = (ImageView) findViewById(R.id.ivSelectDateOption1);
        ivSelectDateOption2 = (ImageView) findViewById(R.id.ivSelectDateOption2);
        ivSelectDateOption3 = (ImageView) findViewById(R.id.ivSelectDateOption3);
        ivSelectDateOption1.setOnClickListener(this);
        ivSelectDateOption2.setOnClickListener(this);
        ivSelectDateOption3.setOnClickListener(this);

        SpinnerAdapter hoursAdapter = new SpinnerAdapter(context, hoursList);
        SpinnerAdapter minutesAdapter = new SpinnerAdapter(context, minutesList);
        SpinnerAdapter ampmAdapter = new SpinnerAdapter(context, amPMList);

        spHoursOption1.setAdapter(hoursAdapter);
        spHoursOption2.setAdapter(hoursAdapter);
        spHoursOption3.setAdapter(hoursAdapter);
        spMinutesOption1.setAdapter(minutesAdapter);
        spMinutesOption2.setAdapter(minutesAdapter);
        spMinutesOption3.setAdapter(minutesAdapter);
        spAMPMOption1.setAdapter(ampmAdapter);
        spAMPMOption2.setAdapter(ampmAdapter);
        spAMPMOption3.setAdapter(ampmAdapter);

        getInterViewTimmingDataFromApi();
        tabs = (TabLayout) findViewById(R.id.tabs);
        setIconToTabManager();

    }

    public void setIconToTabManager() {

     /*   tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_home_black_24dp), false);
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_search_black_24dp), false);
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_dashboard_black_24dp), false);
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_assignment_ind_black_24dp), false);
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_chat_black_24dp), false);*/

        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_home_black_24dp), false);
//       tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_people_black_24dp), false);
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_search_black_24dp), false);
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_dashboard_black_24dp), false);
//        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_assignment_ind_black_24dp), false);
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_chat_black_24dp), false);
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_receipt_black_24dp), false);
        tabs.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (DashBoardManagerActivity.viewpager != null)
                    DashBoardManagerActivity.viewpager.setCurrentItem(tab.getPosition());
                if (ManagerChatDetailActivity.managerChatDetailActivity != null) {
                    ManagerChatDetailActivity.managerChatDetailActivity.finish();
                }
                finish();
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
        //tabs.setSelected(true,);
        //tabs.getTabAt(4).setIcon(R.drawable.ic_person_black_24dp);


    }

    public void addHourDataInList() {
        hoursList.add("Hours");
        for (int i = 1; i <= 12; i++) {
            if (i < 10) {
                hoursList.add("0" + i);
            } else {
                hoursList.add("" + i);
            }
        }
    }

    /*25 july Pragna*/
    public void addMinutesData() {
        minutesList.add("Minutes");
        for (int i = 5; i < 60; i = i + 5) {
            if (i < 10) {
                minutesList.add("0" + i);
            } else {
                minutesList.add("" + i);
            }
        }
    }

    public void showDatePickerDialog(final int optionSelected) {

        int mYear = 0, mMonth = 0, mDay = 0;
        final Calendar c = Calendar.getInstance();
        // if(selectedDate==null) {
        mYear = c.get(Calendar.YEAR);
        mMonth = c.get(Calendar.MONTH);
        mDay = c.get(Calendar.DAY_OF_MONTH);
            /*}else
            {
                c.setTimeInMillis(selectedDate.getTime());
                mYear = c.get(Calendar.YEAR);
                mMonth = c.get(Calendar.MONTH);
                mDay = c.get(Calendar.DAY_OF_MONTH);


            }*/
        DatePickerDialog dialog = new DatePickerDialog(context, R.style.DateDialogTheme, new DatePickerDialog.OnDateSetListener() {
            @Override
            public void onDateSet(DatePicker datePicker, int year,
                                  int monthOfYear, int dayOfMonth) {
                try {
                    StringBuilder theDate = new StringBuilder()
                            .append(dayOfMonth).append("-")
                            .append(monthOfYear + 1).append("-")
                            .append(year);

                    try {
                        if (optionSelected == 1) {
                            dateOption1 = sdf_full.parse(theDate.toString());
                            stringDateOption1 = server_date_format.format(dateOption1);
                            tvSelectDateOption1.setText(userShowFormat.format(dateOption1));

                        } else if (optionSelected == 2) {
                            dateOption2 = sdf_full.parse(theDate.toString());
                            stringDateOption2 = server_date_format.format(dateOption2);
                            Log.d(TAG, "onDateSet: user selected date" + theDate + " " + userShowFormat.format(dateOption2) + "string date " + stringDateOption2 + "date object" + dateOption2.toString());
                            tvDateShowOption2.setText(userShowFormat.format(dateOption2));
                        } else if (optionSelected == 3) {
                            dateOption3 = sdf_full.parse(theDate.toString());
                            stringDateOption3 = server_date_format.format(dateOption3);
                            tvDateShowOption3.setText(userShowFormat.format(dateOption3));
                        }

                    } catch (Exception ex) {
                    }

                    // selectedDateString = sdf_full.format(selectedDate);
                    //etSaveExpensesDate.setText(selectedDateString);
                } catch (Exception ex) {
                }

            }
        }, mYear, mMonth, mDay);
        dialog.setButton(DatePickerDialog.BUTTON_POSITIVE, getString(R.string.ok), dialog);
        dialog.setButton(DatePickerDialog.BUTTON_NEGATIVE, getString(R.string.cancel), dialog);
        dialog.show();

    }


    public void getInterViewTimmingDataFromApi() {
        progressDialog.show();
        String Url = URLS.GET_INTERVIEW_TIMMING + data.getId();
        Log.d(TAG, "getInterViewTimmingDataFromApi: Url " + Url);
        CommonRequestHelper.getDataFromUrl(Url, new CommonRequestHelper.VolleyResponseCustom() {
            @Override
            public void getServerResponse(String response) {
                progressDialog.dismiss();
                Log.d(TAG, "getInterViewTimmingDataFromApi: response " + response);
                Gson gson = new Gson();
                Log.d(TAG, "getExperienceList: URL:- " + response);


                getInterViewTimming = gson.fromJson(response, GetInterViewTimming.class);
                settingScheduleDataToUI();
            }

            @Override
            public void getVolleyError(VolleyError volleyError) {
                progressDialog.dismiss();
                Log.d(TAG, "getInterViewTimmingDataFromApi: Error " + volleyError.getMessage());
                DialogUtils.noInterNetDialog(context, "No Internet Connection", "No active internet connection available", new DialogUtils.NoInterNetDailogClickListner() {
                    @Override
                    public void onDialogOkButtonClicked() {
                        getInterViewTimmingDataFromApi();
                    }

                    @Override
                    public void onDialogCancelButtonClicked() {
                        finish();
                    }
                });
            }
        }, context);

    }

    public void settingScheduleDataToUI() {
        if (getInterViewTimming != null && getInterViewTimming.getMessage().size() > 0) {
            for (GetInterViewTimming.Message data : getInterViewTimming.getMessage()) {
                Date optionDate;
                interview_schedule_id = data.getInterview_scheduled_id();
                if (data.getInterviewOptions() == 1) {
                    try {
                        optionDate = fill_date_format.parse(data.getInterviewInterviewDatetime());
                        dateOption1 = optionDate;
                        stringDateOption1 = server_date_format.format(dateOption1);
                        tvSelectDateOption1.setText(userShowFormat.format(dateOption1));
                        Log.d(TAG, "settingScheduleDataToUI: hour " + getHourFromDate(optionDate) + " minite " + getMiniteFromDate(optionDate) + " am pm " + getAmOrPmFromDate(optionDate) + "index " + hoursList.indexOf(getHourFromDate(optionDate)));
                        spHoursOption1.setSelection(hoursList.indexOf(getHourFromDate(optionDate)));
                        spMinutesOption1.setSelection(minutesList.indexOf(getMiniteFromDate(optionDate)));
                        spAMPMOption1.setSelection(amPMList.indexOf(getAmOrPmFromDate(optionDate).toLowerCase()));
                        btnSaveDatesOption1.setText("Update");
                        iv_1.setVisibility(View.VISIBLE);
                        btnSendInterViewRequest.setText("Update Interview Request");
                        isFirstInserted = true;


                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } else if (data.getInterviewOptions() == 2) {
                    try {
                        optionDate = fill_date_format.parse(data.getInterviewInterviewDatetime());
                        dateOption2 = optionDate;
                        stringDateOption2 = server_date_format.format(dateOption2);
                        tvDateShowOption2.setText(userShowFormat.format(dateOption2));
                        spHoursOption2.setSelection(hoursList.indexOf(getHourFromDate(optionDate)));
                        spMinutesOption2.setSelection(minutesList.indexOf(getMiniteFromDate(optionDate)));
                        spAMPMOption2.setSelection(amPMList.indexOf(getAmOrPmFromDate(optionDate).toLowerCase()));
                        Log.d(TAG, "settingScheduleDataToUI: hour" + getHourFromDate(optionDate) + " minite " + getMiniteFromDate(optionDate) + " am pm " + getAmOrPmFromDate(optionDate));
                        btnSaveDatesOption2.setText("Update");
                        iv_2.setVisibility(View.VISIBLE);
                        btnSendInterViewRequest.setText("Update Interview Request");
                        isSecondInserted = true;

                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                } else if (data.getInterviewOptions() == 3) {
                    try {
                        optionDate = fill_date_format.parse(data.getInterviewInterviewDatetime());
                        dateOption3 = optionDate;
                        stringDateOption3 = server_date_format.format(dateOption3);
                        tvDateShowOption3.setText(userShowFormat.format(dateOption3));
                        spHoursOption3.setSelection(hoursList.indexOf(getHourFromDate(optionDate)));
                        spMinutesOption3.setSelection(minutesList.indexOf(getMiniteFromDate(optionDate)));
                        spAMPMOption3.setSelection(amPMList.indexOf(getAmOrPmFromDate(optionDate).toLowerCase()));
                        Log.d(TAG, "settingScheduleDataToUI: hour" + getHourFromDate(optionDate) + " minite " + getMiniteFromDate(optionDate) + " am pm " + getAmOrPmFromDate(optionDate));
                        btnSaveDateOption3.setText("Update");
                        iv_3.setVisibility(View.VISIBLE);
                        btnSendInterViewRequest.setText("Update Interview Request");
                        isThirdInserted = true;
                    } catch (ParseException e) {
                        e.printStackTrace();
                    }
                }
            }
        } else {

        }
    }

    public String getHourFromDate(Date date) {
        String stringDate = new SimpleDateFormat("hh").format(date);
        int data = Integer.parseInt(stringDate);
        String stringReturn;
        if (data < 10) {
            stringReturn = "0" + data;
        } else {
            stringReturn = "" + data;
        }
        return stringReturn;
    }

    public String getMiniteFromDate(Date date) {
        String stringDate = new SimpleDateFormat("mm").format(date);
        int data = Integer.parseInt(stringDate);
        String stringReturn;
        if (data < 10) {
            stringReturn = "0" + data;
        } else {
            stringReturn = "" + data;
        }
        return stringReturn;
    }

    public String getAmOrPmFromDate(Date date) {

        String stringDate = new SimpleDateFormat("a").format(date);
        return stringDate;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == android.R.id.home) {
            onBackPressed();
        }
        return true;
    }

    @Override
    public void onClick(View view) {
        if (view == ivSelectDateOption1) {
            showDatePickerDialog(1);
        } else if (view == ivSelectDateOption2) {
            showDatePickerDialog(2);
        } else if (view == ivSelectDateOption3) {
            showDatePickerDialog(3);
        } else if (view == btnSaveDatesOption1) {
            if (dateOption1 == null) {
                Toast.makeText(context, "Please select option 1 date", Toast.LENGTH_SHORT).show();
            } else if (spHoursOption1.getSelectedItemPosition() == 0) {
                Toast.makeText(context, "Please select option 1 hours", Toast.LENGTH_SHORT).show();
            } else if (spMinutesOption1.getSelectedItemPosition() == 0) {
                Toast.makeText(context, "Please select option 1 Minutes", Toast.LENGTH_SHORT).show();
            } else {
                setInterViewSchedule(data.getId(), "1", stringDateOption1, spHoursOption1.getSelectedItem().toString(), spMinutesOption1.getSelectedItem().toString(), spAMPMOption1.getSelectedItem().toString());
            }
        } else if (view == btnSaveDatesOption2) {

            if (dateOption2 == null) {
                Toast.makeText(context, "Please select option 2 date", Toast.LENGTH_SHORT).show();
            } else if (spHoursOption2.getSelectedItemPosition() == 0) {
                Toast.makeText(context, "Please select option 2 hours", Toast.LENGTH_SHORT).show();
            } else if (spMinutesOption2.getSelectedItemPosition() == 0) {
                Toast.makeText(context, "Please select option 2 Minutes", Toast.LENGTH_SHORT).show();
            } else {
                setInterViewSchedule(data.getId(), "2", stringDateOption2, spHoursOption2.getSelectedItem().toString(), spMinutesOption2.getSelectedItem().toString(), spAMPMOption2.getSelectedItem().toString());
            }
        } else if (view == btnSaveDateOption3) {

            if (dateOption3 == null) {
                Toast.makeText(context, "Please select option 3 date", Toast.LENGTH_SHORT).show();
            } else if (spHoursOption3.getSelectedItemPosition() == 0) {
                Toast.makeText(context, "Please select option 3 hours", Toast.LENGTH_SHORT).show();
            } else if (spMinutesOption3.getSelectedItemPosition() == 0) {
                Toast.makeText(context, "Please select option 3 Minutes", Toast.LENGTH_SHORT).show();
            } else {
                setInterViewSchedule(data.getId(), "3", stringDateOption3, spHoursOption3.getSelectedItem().toString(), spMinutesOption3.getSelectedItem().toString(), spAMPMOption3.getSelectedItem().toString());
            }

        } else if (view == btnSendInterViewRequest) {
            if (isFirstInserted || isSecondInserted || isThirdInserted) {

                onSendViewInterViewRequest();
            } else {
                Toast.makeText(context, "Please save 1 or more interview options", Toast.LENGTH_SHORT).show();
            }
        }
    }


    public void onSendViewInterViewRequest() {
        progressDialog.show();
        String Url = URLS.UPDATE_ITER_VIEW_SCHEDULE + "&" +
                URLS.USER_TYPE + "=" + mySharedPrefereces.getUserType() + "&" +
                URLS.APPLICANT_ID + "=" + data.getApplicantId() + "&" +
                URLS.MANAGER_ID + "=" + mySharedPrefereces.getUserId() + "&" +
                URLS.APPLICATION_ID + "=" + data.getId() + "&" +
                URLS.INTERVIEW_SCHEDULED_ID + "=" + interview_schedule_id;
        Log.d(TAG, "onSendViewInterViewRequest: URL " + Url);
        CommonRequestHelper.getDataFromUrl(Url, new CommonRequestHelper.VolleyResponseCustom() {
            @Override
            public void getServerResponse(String response) {
                Log.d(TAG, "onSendViewInterViewRequest: response " + response);
                progressDialog.dismiss();
                try {
                    JSONObject jsonObject = new JSONObject(response);

                    DialogUtils.showDialog4Activity(context, context.getResources().getString(R.string.app_name), jsonObject.getString("message"), new DialogUtils.DailogCallBackOkButtonClick() {
                        @Override
                        public void onDialogOkButtonClicked() {
                            finish();
                        }
                    });
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void getVolleyError(VolleyError volleyError) {
                progressDialog.dismiss();
                Log.d(TAG, "onSendViewInterViewRequest: volley  " + volleyError.getMessage());
                DialogUtils.noInterNetDialog(context, "No Internet Connection", "No active internet connection available", new DialogUtils.NoInterNetDailogClickListner() {
                    @Override
                    public void onDialogOkButtonClicked() {
                        onSendViewInterViewRequest();
                    }

                    @Override
                    public void onDialogCancelButtonClicked() {
                        finish();
                    }
                });

            }
        }, context);


    }

    public void setInterViewSchedule(final String application_id, final String option, final String date_option, final String hours_option, final String minutes_option, final String format_option) {
        progressDialog.show();
        String set_interview_schedule = URLS.SET_INTERVIEW_SHEDULE +
                "&" + URLS.APPLICATION_ID + "=" + application_id +
                "&" + URLS.OPTION + "=" + option +
                "&" + URLS.DATE_OPTION + "=" + date_option +
                "&" + URLS.HOURS_OPTION + "=" + hours_option +
                "&" + URLS.MINUTES_OPTION + "=" + minutes_option +
                "&" + URLS.FORMAT_OPTION + "=" + format_option;
        Log.d(TAG, "setInterViewSchedule:Url " + set_interview_schedule);
        CommonRequestHelper.getDataFromUrl(set_interview_schedule, new CommonRequestHelper.VolleyResponseCustom() {
            @Override
            public void getServerResponse(String response) {
                progressDialog.dismiss();
                Log.d(TAG, "setInterViewSchedule:response " + response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getInt("status") == 1) {
                        if (option.equals("1")) {
                            iv_1.setVisibility(View.VISIBLE);
                          /*  new SweetAlertDialog(InterViewDatesActivity.this, SweetAlertDialog.CUSTOM_IMAGE_TYPE)
                                    .setTitleText("Sweet!")
                                    .setContentText("Here's a custom image.")
                                    .setCustomImage(R.drawable.correct)
                                    .show();*/
                            isFirstInserted = true;
                        } else if (option.equals("2")) {
                            iv_2.setVisibility(View.VISIBLE);
                            /*new SweetAlertDialog(InterViewDatesActivity.this, SweetAlertDialog.SUCCESS_TYPE)
                                    .setTitleText("Good job!")
                                    .setContentText("You clicked the button!")
                                    .show();*/
                            isSecondInserted = true;
                        } else if (option.equals("3")) {
                            iv_3.setVisibility(View.VISIBLE);
                            isThirdInserted = true;
                        }
                    }
                    /*25 july Pragna*/
                  /*  DialogUtils.showDialog4Activity(context, context.getResources().getString(R.string.app_name), jsonObject.getString("message"), null);*/
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void getVolleyError(VolleyError volleyError) {
                progressDialog.dismiss();
                Log.d(TAG, "setInterViewSchedule:error " + volleyError.getMessage());
                DialogUtils.noInterNetDialog(context, "No Internet Connection", "No active internet connection available", new DialogUtils.NoInterNetDailogClickListner() {
                    @Override
                    public void onDialogOkButtonClicked() {
                        setInterViewSchedule(application_id, option, date_option, hours_option, minutes_option, format_option);
                    }

                    @Override
                    public void onDialogCancelButtonClicked() {

                    }
                });
            }
        }, getApplicationContext());

    }
}
