package com.hiremehealthcare.activity;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.widget.LinearLayout;

import com.hiremehealthcare.R;
import com.hiremehealthcare.fragment.EditPostionFragment;
import com.hiremehealthcare.fragment.ProfileFragment;

public class ProfileActivity extends AppCompatActivity {


    Bundle getData;
    TabLayout tabs;
    private android.widget.LinearLayout llPositionPost;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_editposition);
        this.tabs = (TabLayout) findViewById(R.id.tabs);
        this.llPositionPost = (LinearLayout) findViewById(R.id.llPositionPost);
        getData = getIntent().getExtras();
        initAllControls();
    }

    public void initAllControls() {


        ProfileFragment editPostionFragment = new ProfileFragment();
        editPostionFragment.setArguments(getData);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
//        getSupportActionBar().setTitle("Edit Position::");
        getSupportActionBar().setTitle("Profile");
        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
        fragmentTransaction.replace(R.id.llPositionPost, editPostionFragment, "test");
        fragmentTransaction.commit();
        tabs = (TabLayout) findViewById(R.id.tabs);
        setIconToTab();
    }

    public void setIconToTab() {

        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_home_black_24dp), false);
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_search_black_24dp), false);
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_favorite_black_24dp), false);
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_chat_black_24dp), false);
//        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_person_black_24dp), false);
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_receipt_black_24dp), false);
        tabs.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if(DashBoardActivity.viewpager!=null)
                    DashBoardActivity.viewpager.setCurrentItem(tab.getPosition());
                finish();
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
        //tabs.setSelected(true,);
        //tabs.getTabAt(4).setIcon(R.drawable.ic_person_black_24dp);


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home)
        {
            onBackPressed();
        }
        return true;
    }
}
