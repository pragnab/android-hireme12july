package com.hiremehealthcare.activity;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.SpannableString;
import android.text.Spanned;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.bumptech.glide.Glide;
import com.github.lzyzsd.circleprogress.DonutProgress;
import com.google.gson.Gson;
import com.hiremehealthcare.CommonCls.CustomBoldTextView;
import com.hiremehealthcare.CommonCls.CustomButton;
import com.hiremehealthcare.CommonCls.CustomTextView;
import com.hiremehealthcare.CommonCls.DialogUtils;
import com.hiremehealthcare.CommonCls.ExpandableHeightGridView;
import com.hiremehealthcare.CommonCls.ExpandableHeightListView;
import com.hiremehealthcare.CommonCls.URLS;
import com.hiremehealthcare.CommonCls.Validations;
import com.hiremehealthcare.POJO.ActivePositionModel;
import com.hiremehealthcare.POJO.ApplicantLicenseModel;
import com.hiremehealthcare.POJO.Benefits;
import com.hiremehealthcare.POJO.CandidateDetail;
import com.hiremehealthcare.POJO.Hours;
import com.hiremehealthcare.POJO.Shift;
import com.hiremehealthcare.R;
import com.hiremehealthcare.Utils.CommonRequestHelper;
import com.hiremehealthcare.adapter.ActivePositionAdapterWithSelection;
import com.hiremehealthcare.adapter.DaysMultiSelectAdapter_duplicate_flip;
import com.hiremehealthcare.adapter.Days_adapter;
import com.hiremehealthcare.adapter.Evening_adapter;
import com.hiremehealthcare.adapter.Night_adapter;
import com.hiremehealthcare.adapter.Profile_licenseListAdpter;
import com.hiremehealthcare.database.MySharedPrefereces;
import com.hiremehealthcare.fragment.CommunicationFragment;
import com.hiremehealthcare.fragment.PostNewPostion;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.regex.Pattern;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by ADMIN on 11-05-2018.
 */

//public class ContactCandidate extends Activity implements View.OnClickListener {
public class ContactCandidate extends AppCompatActivity implements View.OnClickListener {
    String ID = "", IMAGE = "";
    private CustomBoldTextView tvname;
    private CustomTextView tvlicense;
    private CustomTextView tvcertified;
    private CustomTextView tvviewlicense;
    private CustomTextView tvexp;
    private CustomTextView tvedu;
    private CustomButton btnContactToCandidate;
    private ScrollView scrollview;
    Context ctx;
    MySharedPrefereces mySharedPrefereces;
    RequestQueue queue;
    private DonutProgress circleProgress;
    private CircleImageView ivimg;
    private CustomTextView tvmiles;
    private LinearLayout viewlicense;
    private ExpandableHeightListView lvLicenceList;
    String TAG = "ContactCandidate";
    ProgressDialog progressDialog;
    public static Hours hours;
    public static Shift shifts;
    List<Hours.HoursBean> hoursBeanList;
    List<Shift.ShiftBean> shiftBeanList;
    List<ActivePositionModel.Datum> activePostionDataList;
    CandidateDetail candidateDetail;
    List<ApplicantLicenseModel.Datum> applicantLicenseData;
    int position_id;
    TabLayout tabs;
    private CustomTextView tvFacilityShift;
    private CustomTextView tvFacilityHours;
    private CustomBoldTextView tvavailability;
    private CustomTextView tvDays;
    private CustomTextView tvEvening;
    private LinearLayout v;
    private CustomTextView tvNights;
    private com.hiremehealthcare.CommonCls.ExpandableHeightGridView griddays;
    private com.hiremehealthcare.CommonCls.ExpandableHeightGridView griddaystime;
    private com.hiremehealthcare.CommonCls.ExpandableHeightGridView grideveningdaystime;
    private com.hiremehealthcare.CommonCls.ExpandableHeightGridView gridnightdaystime;

    private LinearLayout lin;
    private CustomTextView tvdays;
    public static Days_adapter _daDays_checkBoxAdapter;
    public static Night_adapter _daNightcheckBoxAdapter;
    public static Evening_adapter _daevening_checkBoxAdapter;

    //    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        View view = inflater.inflate(R.layout.contact_candidate, container, false);
//        return view;
//    }
@Override
public boolean onOptionsItemSelected(MenuItem item) {
    if (item.getItemId() == android.R.id.home) {
        onBackPressed();
    }
    return true;
}
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        ctx = getApplicationContext();

        setContentView(R.layout.contact_candidate);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Candidate Details");
        this.tvdays = (CustomTextView) findViewById(R.id.tvdays);
        this.lin = (LinearLayout) findViewById(R.id.lin);
        this.gridnightdaystime = (ExpandableHeightGridView) findViewById(R.id.grid_night_days_time);
        this.grideveningdaystime = (ExpandableHeightGridView) findViewById(R.id.grid_evening_days_time);
        this.griddaystime = (ExpandableHeightGridView) findViewById(R.id.grid_days_time);
        this.griddays = (ExpandableHeightGridView) findViewById(R.id.grid_days);
        this.tvNights = (CustomTextView) findViewById(R.id.tv_Nights);
        this.v = (LinearLayout) findViewById(R.id.v);
        this.tvEvening = (CustomTextView) findViewById(R.id.tv_Evening);
        this.tvDays = (CustomTextView) findViewById(R.id.tv_Days);
        this.tvavailability = (CustomBoldTextView) findViewById(R.id.tv_availability);
        this.tvFacilityHours = (CustomTextView) findViewById(R.id.tvFacilityHours);
        this.tvFacilityShift = (CustomTextView) findViewById(R.id.tvFacilityShift);
        griddaystime.setExpanded(true);
        griddays.setExpanded(true);
        grideveningdaystime.setExpanded(true);
        gridnightdaystime.setExpanded(true);

        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        hoursBeanList = new ArrayList<>();
        shiftBeanList = new ArrayList<>();
        applicantLicenseData = new ArrayList<>();
        activePostionDataList = new ArrayList<>();
        SpannableString spannableString = new SpannableString(getString(R.string.please_wait));
        Typeface typefaceSpan = Validations.setTypeface(ctx);
        spannableString.setSpan(typefaceSpan, 0, getString(R.string.please_wait).length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        progressDialog.setMessage(spannableString);
        this.viewlicense = (LinearLayout) findViewById(R.id.view_license);
        this.tvmiles = (CustomTextView) findViewById(R.id.tv_miles);
        this.ivimg = (CircleImageView) findViewById(R.id.iv_img);
        this.circleProgress = (DonutProgress) findViewById(R.id.circleProgress);
        this.scrollview = (ScrollView) findViewById(R.id.scrollview);
        this.btnContactToCandidate = (CustomButton) findViewById(R.id.btnContactToCandidate);
        btnContactToCandidate.setOnClickListener(this);
        this.tvedu = (CustomTextView) findViewById(R.id.tv_edu);
        this.tvexp = (CustomTextView) findViewById(R.id.tv_exp);
        this.tvviewlicense = (CustomTextView) findViewById(R.id.tv_view_license);
        this.tvcertified = (CustomTextView) findViewById(R.id.tv_certified);
        this.tvlicense = (CustomTextView) findViewById(R.id.tv_license);
        this.tvname = (CustomBoldTextView) findViewById(R.id.tv_name);
        //RECEIVE DATA VIA INTENT
        Intent i = getIntent();
        ID = i.getStringExtra("ID");
        position_id = i.getIntExtra("position_id", 0);
        IMAGE = i.getStringExtra("IMAGE");
        System.out.println("ContactCandidate >>>>>>>> " + ID + "");
        System.out.println("ContactCandidate IMAGE >>>>>>>> " + IMAGE + "");
        queue = Volley.newRequestQueue(ctx);
        mySharedPrefereces = new MySharedPrefereces(ctx);
        viewlicense.setOnClickListener(this);
        tabs = (TabLayout) findViewById(R.id.tabs);
        setIconToTab();
        getCandidateData();
    }

    public void setIconToTab() {
        String [][] argCopy = new String[2][2];
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_home_black_24dp), false);
//        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_people_black_24dp), false);
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_search_black_24dp), false);
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_dashboard_black_24dp), false);
//        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_assignment_ind_black_24dp), false);
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_chat_black_24dp));

        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_receipt_black_24dp), false);
        tabs.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if (DashBoardManagerActivity.viewpager != null)
                    DashBoardManagerActivity.viewpager.setCurrentItem(tab.getPosition());
                if (SearchCandidateDetailActivity.searchCandidateDetailActivity != null) {
                    SearchCandidateDetailActivity.searchCandidateDetailActivity.finish();
                }
                if (position_id == 0)
                    finish();
                else {
                    if (Qualified_CandidatesActivity.qualified_candidatesActivity != null)
                        Qualified_CandidatesActivity.qualified_candidatesActivity.finish();
                    finish();
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
        //tabs.setSelected(true,);
        //tabs.getTabAt(4).setIcon(R.drawable.ic_person_black_24dp);


    }

    List<String> listlicesnseYear = new ArrayList<>();

    List<String> listlicesnseMonth = new ArrayList<>();
    List<String> listlicesnseNo = new ArrayList<>();
    List<String> listlicesnseFile = new ArrayList<>();

    /**
     * this method is used to get Candidate data from Server
     */

    private void getCandidateData() {

        DialogUtils.showProgressDialog(ContactCandidate.this, "Please wait....");
        //   String Url = URLS.MANAGER_SEARCH_CANDIDATE + mySharedPrefereces.getZipCode()+"";
        String Url = URLS.MANAGER_CANDIDATE_DETAIL + mySharedPrefereces.getZipCode() + "&candidate_id=" + ID + "";
        Log.d(TAG, "getCandidateData!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!! : URL " + Url);
        StringRequest req = new StringRequest(Request.Method.GET, Url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                Log.d(TAG, "getCandidateData: Response" + response);
                response=response.replaceAll("[^\\p{Print}]","");

                Gson gson = new Gson();
                DialogUtils.hideProgressDialog();
                candidateDetail = gson.fromJson(response, CandidateDetail.class);

                if (candidateDetail != null) {
                    if (candidateDetail.getData() != null) {

                        String complted = candidateDetail.getData().getProfile_completed() + "";
                        if (complted != null) {
                            if (Validations.isNumeric(complted)) {
                                int int_completed = Integer.parseInt(complted + "");
                                circleProgress.setProgress(int_completed);
                            }
                        }
/*img fromprev fragment->home */
                        Glide.with(ctx)
                                .load(IMAGE)
                                .error(R.drawable.no_image)
                                .into(ivimg);

                        availability(candidateDetail.getData());
                        tvFacilityShift.setText(candidateDetail.getData().getShift_name()+"");
                        tvFacilityHours.setText(candidateDetail.getData().getHour_name()+"");
                        tvname.setText(candidateDetail.getData().getFirstname() + " " + candidateDetail.getData().getLastname() + "");

                        if (candidateDetail.getData().getApplicant_cerification_cerificate_name() != null && !candidateDetail.getData().getApplicant_cerification_cerificate_name().equals("null") && !candidateDetail.getData().getApplicant_cerification_cerificate_name().equals("")) {
//                            tvcertified.setText("Certified in " + candidateDetail.getData().getApplicant_cerification_cerificate_name() + "");
                            String ss = "Certified in " + candidateDetail.getData().getApplicant_cerification_cerificate_name() + "";
//                            ss = ss.replace("null", "N/A").replace(",", " , ");
                            ss = ss.replace("null", "N/A").replace(",", ", ");
                            tvcertified.setText(ss);
                        } else {
                            tvcertified.setText("No Certificate");
                        }

                        tvmiles.setText(candidateDetail.getData().getCandidate_distance() + " miles");
                        if (candidateDetail.getData().getLicense_name() != null && !candidateDetail.getData().getLicense_name().equals("null") && !candidateDetail.getData().getLicense_name().equals("")) {
                            //tvlicense.setText("Licensed in " + candidateDetail.getData().getLicense_name() + "");
                            String ss = "Licensed in " + candidateDetail.getData().getLicense_name() + "";
//                            ss = ss.replace("null", "N/A").replace(",", " , ");
                            ss = ss.replace("null", "N/A").replace(",", ", ");
                            tvlicense.setText(ss);
                        } else {
//                            tvlicense.setText("No License");
                            tvlicense.setText("No License");
                        }
                        String expYear = candidateDetail.getData().getApplicant_experience_duration_years() + "";
                        expYear = expYear.replace("null", "0");
                        String expMonth = candidateDetail.getData().getApplicant_experience_duration_months() + "";
                        expMonth = expMonth.replace("null", "0");
                        String expNames = candidateDetail.getData().getApplicant_experience_speciality_name() + "";
                        expNames = expNames.replace("null", "");
                        String s = "N/A";
                        if (!expNames.contentEquals("")) {
                            String arrexpYear[] = expYear.split(",");
                            String arrexpMonth[] = expMonth.split(",");
                            String arrexpNames[] = expNames.split(",");
                            s = "";
                            for (int i = 0; i < arrexpNames.length; i++) {
                                //s = s + arrexpYear[i] + " Year " + arrexpMonth[i] + " Month in " + arrexpNames[i] + "\n";
                                if (arrexpYear[i].equals("0")) {
                                    s = s + arrexpMonth[i] + " Month in " + arrexpNames[i] + "\n";
                                } else if (arrexpMonth[i].equals("0")) {
                                    s = s + arrexpYear[i] + " Year " + arrexpNames[i] + "\n";
                                } else {
/*25 july Pragna*/
                                    String y = "";
                                    String m = "";
//                                    s = s + arrexpYear[i] + " Year " + arrexpMonth[i] + " Month in " + arrexpNames[i] + "\n";
                                    if (arrexpYear[i].contentEquals("1") || arrexpYear[i].contentEquals("0")) {
                                        y = " year ";
                                    } else {
                                        y = " years ";
                                    }
                                    if (arrexpMonth[i].contentEquals("1") || arrexpMonth[i].contentEquals("0")) {
                                        m = " month ";
                                    } else {
                                        m = " months ";
                                    }

                                    //   s = s + arrexpYear[i] + " Year " + arrexpMonth[i] + " Month in " + arrexpNames[i] + "\n";
                                    s = s + arrexpYear[i] + y + arrexpMonth[i] + m + arrexpNames[i] + "\n";


                                }

                                //s = s +arrexpNames[i] + ",";
                            }
//            s = s.substring(0, s.length() - 1);
                            tvexp.setText(s + "");
                        } else {
                            tvexp.setText("No Experience");
                        }


                        String eduYear = candidateDetail.getData().getApplicant_education_completion_year_name() + "";
                        String eduMonth = candidateDetail.getData().getApplicant_education_completion_month_name() + "";
                        String eduNames = candidateDetail.getData().getApplicant_education_degree() + "";
                        String institute = candidateDetail.getData().getApplicant_education_institute() + "";
                        eduYear = eduYear.replace("null", "0");
                        eduMonth = eduMonth.replace("null", "0");
                        institute = institute.replace("null", "");
                        eduNames = eduNames.replace("null", "");

                        String sedu = "N/A";
                        if (eduNames.trim().length() != 0) {
                            String arreduYear[] = eduYear.split(",");
                            String arreduMonth[] = eduMonth.split(",");
                            String arreduNames[] = eduNames.split(",");
                            String arrinstituteNames[] = institute.split(",");
                            s = "";
                            for (int i = 0; i < arreduNames.length; i++) {

                                s = s + arreduYear[i] + "  " + arreduNames[i] + " from " + arrinstituteNames[i] + "\n";

                                //s = s +arrexpNames[i] + ",";
                            }
//            s = s.substring(0, s.length() - 1);
                            tvedu.setText(s + "");
                        } else {
                            tvedu.setText("No Education");
                        }


                        String licesnseYear = candidateDetail.getData().getApplicant_license_experience_year_name() + "";
                        String licesnseMonth = candidateDetail.getData().getApplicant_license_experience_month_name() + "";
                        String licesnseFile = candidateDetail.getData().getApplicant_license_proof() + "";
                        String licesnseNo = candidateDetail.getData().getApplicant_license_number() + "";
                        if (!licesnseNo.contentEquals("")) {
                            listlicesnseYear = Arrays.asList(licesnseYear.split("\\s*,\\s*"));
                            listlicesnseMonth = Arrays.asList(licesnseMonth.split("\\s*,\\s*"));
                            listlicesnseNo = Arrays.asList(licesnseNo.split("\\s*,\\s*"));
                            listlicesnseFile = Arrays.asList(licesnseFile.split("\\s*,\\s*"));

                        }

//                            homeManagerAdapter = new HomeManagerAdapter(searchCandidate, ctx);
//                            rvHomeManagerList.setAdapter(homeManagerAdapter);
                    } else {
                        DialogUtils.showDialog(ContactCandidate.this, ctx.getResources().getString(R.string.app_name), "No Candidate Found!");

                    }


                } else {    // DialogUtils.showDialog4Activity(Login.this, ctx.getResources().getString(R.string.app_name), logindata.getMessage() + "");

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                Log.d(TAG, "getCandidateData: Errror" + error.getMessage());
                NetworkResponse networkResponse = error.networkResponse;
                DialogUtils.noInterNetDialog(ctx, "No Internet Connection", "No active internet connection available", new DialogUtils.NoInterNetDailogClickListner() {
                    @Override
                    public void onDialogOkButtonClicked() {
                        getCandidateData();
                    }

                    @Override
                    public void onDialogCancelButtonClicked() {
                        finish();
                    }
                });


            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(req);
    }

    public void getApplicantLicenceDetail() {
        DialogUtils.showProgressDialog(ContactCandidate.this, "Please wait....");
        String Url = URLS.GET_APPLICANT_LICENCE_DETAIL + candidateDetail.getData().getId();//"35";//+mySharedPrefereces.getUserId();;+"&"+URLS.LICENSE_ID+"="+mySharedPrefereces.getFacility_id();
        Log.d(TAG, "getApplicantLicenceDetail: Url " + Url);
        applicantLicenseData.clear();
        CommonRequestHelper.getDataFromUrl(Url, new CommonRequestHelper.VolleyResponseCustom() {
            @Override
            public void getServerResponse(String response) {
                Log.d("Applicant licence", response);
                Log.d(TAG, "getApplicantLicenceDetail: Response " + response);
                DialogUtils.hideProgressDialog();
                Gson gson = new Gson();

                ApplicantLicenseModel applicantLicenseModel = gson.fromJson(response, ApplicantLicenseModel.class);
                if (applicantLicenseModel != null && applicantLicenseModel.getData() != null) {
                    applicantLicenseData.addAll(applicantLicenseModel.getData());
                    onLicenseButtonClicked(ctx, listlicesnseYear, listlicesnseMonth, listlicesnseNo, listlicesnseFile);
                    //profile_licenseListAdpter.notifyDataSetChanged();
                }
            }

            @Override
            public void getVolleyError(VolleyError volleyError) {
                DialogUtils.hideProgressDialog();
                Log.d(TAG, "getApplicantLicenceDetail: Error " + volleyError.getMessage());
                NetworkResponse networkResponse = volleyError.networkResponse;
                DialogUtils.noInterNetDialog(ctx, "No Internet Connection", "No active internet connection available", new DialogUtils.NoInterNetDailogClickListner() {
                    @Override
                    public void onDialogOkButtonClicked() {
                        getApplicantLicenceDetail();
                    }

                    @Override
                    public void onDialogCancelButtonClicked() {

                    }
                });


            }
        }, ctx);

    }

    public void onLicenseButtonClicked(Context ctx, List<String> listlicesnseYear, List<String> listlicesnseMonth, List<String> listlicesnseNo, List<String> listlicesnseFile) {
        LayoutInflater inflater = LayoutInflater.from(ContactCandidate.this);
        final View dialogView = inflater.inflate(R.layout.view_license_inflater, null);
        this.lvLicenceList = (ExpandableHeightListView) dialogView.findViewById(R.id.lvLicenceList);
        ImageView ivCloseDialog = (ImageView) dialogView.findViewById(R.id.ivCloseDialog);
        lvLicenceList.setExpanded(true);
        // ViewLicenseListAdapter viewLicenseListAdapter=new ViewLicenseListAdapter(ctx,listlicesnseYear,listlicesnseMonth,listlicesnseNo,listlicesnseFile,ContactCandidate.this);
        //lvLicenceList.setAdapter(viewLicenseListAdapter);
//
        final AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(ContactCandidate.this, R.style.myDialog));
        final AlertDialog b = builder.create();
        //  builder.setTitle("Material Style Dialog");
        builder.setCancelable(true);
        builder.setView(dialogView);
        b.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));

        Profile_licenseListAdpter profile_licenseListAdpter = new Profile_licenseListAdpter(this, applicantLicenseData, new Profile_licenseListAdpter.OnActionDeleteClickedListener() {
            @Override
            public void onDeleteButtonClicked(ApplicantLicenseModel.Datum data) {
                //deleteApplicantLicence(data);
            }
        }, false);
        final AlertDialog show = builder.show();
        lvLicenceList.setAdapter(profile_licenseListAdpter);
        ivCloseDialog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                show.dismiss();
            }
        });
        /*dialogButtonOKButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                show.dismiss();

            }
        });*/
    }


    @Override
    public void onClick(View view) {

        if (view == btnContactToCandidate) {
            showDialogForSelectionPostion();
        } else if (viewlicense == view) {
            getApplicantLicenceDetail();
            //onLicenseButtonClicked(ctx, listlicesnseYear, listlicesnseMonth, listlicesnseNo, listlicesnseFile);
        }
    }

    public void showDialogForSelectionPostion() {
        if (position_id == 0) {
            getHours();
        } else {
            contactCandidateApiCalling(position_id + "");
        }
    }

    private void getHours() {
        final String Url = URLS.HOURS_URL;
        progressDialog.show();
        Log.d(TAG, "getHours: URL:-" + Url);
        StringRequest req = new StringRequest(Request.Method.GET, Url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                Log.d(TAG, "getHours: Reponse:-" + Url);
                Gson gson = new Gson();

                hours = gson.fromJson(response, Hours.class);

                if (hours != null) {
                    hoursBeanList.clear();
                    hoursBeanList.addAll(hours.getHours());
                    getShift();

                } else {
                    // DialogUtils.showDialog4Activity(Login.this, ctx.getResources().getString(R.string.app_name), logindata.getMessage() + "");

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Log.d(TAG, "getHours: Error:-" + error.getMessage());
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    int statusCode = error.networkResponse.statusCode;
                    System.out.println("STATUS CODE " + statusCode + "");
                    DialogUtils.showDialog4Activity(ctx, ctx.getResources().getString(R.string.app_name), statusCode + "", null);
                }

            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(req);

    }

    /**
     * this method is use to getting shift detail
     */

    private void getShift() {
        progressDialog.show();
        String Url = URLS.SHIFT_URL;
        Log.d(TAG, "getShift: URL:-" + Url);
        StringRequest req = new StringRequest(Request.Method.GET, Url, new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                Log.d(TAG, "getShift: URL:-" + response);
                Gson gson = new Gson();
                shifts = gson.fromJson(response, Shift.class);

                if (shifts != null) {
                    shiftBeanList.clear();
                    shiftBeanList.addAll(shifts.getShift());
                    getPositiveDataFromApi();
                } else {
                    // DialogUtils.showDialog4Activity(Login.this, ctx.getResources().getString(R.string.app_name), logindata.getMessage() + "");

                }


            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                Log.d(TAG, "getShift: URL:-" + error.getMessage());
                NetworkResponse networkResponse = error.networkResponse;
                if (networkResponse != null) {
                    int statusCode = error.networkResponse.statusCode;
                    System.out.println("STATUS CODE " + statusCode + "");
                    DialogUtils.showDialog4Activity(ctx, ctx.getResources().getString(R.string.app_name), statusCode + "", null);
                }

            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(req);

    }

    /**
     * this method getting positive Data From Server
     */

    public void getPositiveDataFromApi() {
        String url = URLS.MY_POSITION_ACTIVE + mySharedPrefereces.getUserId();
        Log.d(TAG, "getPositiveDataFromApi: URL" + url);
        CommonRequestHelper.getDataFromUrl(url, new CommonRequestHelper.VolleyResponseCustom() {
            @Override
            public void getServerResponse(String response) {
                Log.d(TAG, "getPositiveDataFromApi: Response" + response);
                Gson gson = new Gson();
                ActivePositionModel activePositionModel = gson.fromJson(response, ActivePositionModel.class);
                if (activePositionModel != null) {
                    if (activePositionModel.getData().size() > 0) {
                        activePostionDataList.addAll(activePositionModel.getData());
                        showDialogContactCandidateDialog();
                        //activePositionAdapter.notifyDataSetChanged();
                        //ivNoDataFound.setVisibility(View.GONE);
                    } else {
                        //ivNoDataFound.setVisibility(View.VISIBLE);
                    }
                }

            }

            @Override
            public void getVolleyError(VolleyError volleyError) {
                Log.d(TAG, "getPositiveDataFromApi: Error" + volleyError.getMessage());
                DialogUtils.noInterNetDialog(ctx, "No Internet Connection", "No active internet connection available", new DialogUtils.NoInterNetDailogClickListner() {
                    @Override
                    public void onDialogOkButtonClicked() {
                        getPositiveDataFromApi();
                    }

                    @Override
                    public void onDialogCancelButtonClicked() {
                        finish();
                    }
                });


            }
        }, ctx);


        /*for (int i = 0; i < 30; i++) {
            dashbordManagerModelList.add(new DashbordManagerModel());
        }
        activePositionAdapter.notifyDataSetChanged();*/
    }

    public void showDialogContactCandidateDialog() {

        LayoutInflater inflater = LayoutInflater.from(ContactCandidate.this);
        final View dialogView = inflater.inflate(R.layout.contact_candidate_select_position_dialog, null);
        RecyclerView rvActivePositionList = (RecyclerView) dialogView.findViewById(R.id.rvActivePositionList);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        rvActivePositionList.setLayoutManager(mLayoutManager);
        rvActivePositionList.setItemAnimator(new DefaultItemAnimator());

        final AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(ContactCandidate.this, R.style.myDialog));
        final AlertDialog b = builder.create();
        //  builder.setTitle("Material Style Dialog");
        builder.setCancelable(true);
        builder.setView(dialogView);
        b.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        final AlertDialog show = builder.show();
        ActivePositionAdapterWithSelection activePositionAdapterWithSelection = new ActivePositionAdapterWithSelection(activePostionDataList, this, shiftBeanList, hoursBeanList, new ActivePositionAdapterWithSelection.OnPositionSelect() {
            @Override
            public void onPositionSelect(ActivePositionModel.Datum data) {
                show.dismiss();
                contactCandidateApiCalling(data.getPositionId());
            }
        });
        rvActivePositionList.setAdapter(activePositionAdapterWithSelection);


    }

    public void contactCandidateApiCalling(final String position_id) {
        progressDialog.show();
        String Url = URLS.CONTACT_CANDIDATE + "&" +
                URLS.CANDIDATE_ID + "=" + candidateDetail.getData().getId() + "&" +
                URLS.POSITION_ID + "=" + position_id;
        Log.d(TAG, "contactCandidateApiCalling: URL " + Url);
        CommonRequestHelper.getDataFromUrl(Url, new CommonRequestHelper.VolleyResponseCustom() {
            @Override
            public void getServerResponse(String response) {
                progressDialog.dismiss();
                Log.d(TAG, "getServerResponse: response " + response);
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    if (jsonObject.getInt("status") == 1) {
                        DialogUtils.showDialog4Activity(ContactCandidate.this, ContactCandidate.this.getResources().getString(R.string.app_name), jsonObject.getString("message") + "", new DialogUtils.DailogCallBackOkButtonClick() {
                            @Override
                            public void onDialogOkButtonClicked() {
                                if (Qualified_CandidatesActivity.qualified_candidatesActivity != null)
                                    Qualified_CandidatesActivity.qualified_candidatesActivity.finish();
                                if (SearchCandidateDetailActivity.searchCandidateDetailActivity != null) {
                                    SearchCandidateDetailActivity.searchCandidateDetailActivity.finish();
                                }

                                if (CommunicationFragment.communicationFragment != null) {
                                    CommunicationFragment.communicationFragment.getCommunicationData();
                                    if (DashBoardManagerActivity.viewpager != null) {
                                        DashBoardManagerActivity.viewpager.setCurrentItem(4);
                                    }

                                }
                                finish();
                            }
                        });

                    } else {
                        DialogUtils.showDialog4Activity(ContactCandidate.this, ContactCandidate.this.getResources().getString(R.string.app_name), jsonObject.getString("message") + "", null);
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            @Override
            public void getVolleyError(VolleyError volleyError) {
                progressDialog.dismiss();
                Log.d(TAG, "getServerResponse: error " + volleyError.getMessage());
                DialogUtils.noInterNetDialog(ctx, "No Internet Connection", "No active internet connection available", new DialogUtils.NoInterNetDailogClickListner() {
                    @Override
                    public void onDialogOkButtonClicked() {
                        contactCandidateApiCalling(position_id);
                    }

                    @Override
                    public void onDialogCancelButtonClicked() {

                    }
                });

            }
        }, this);
    }
    private void availability( CandidateDetail.DataBean data ) {
        ArrayList<String> days = new ArrayList<>();
        //   getavailability(data.getPositionId() + "");
        days = new ArrayList<>();
        days.add("Mon");
        Days_adapter.selected=new ArrayList<>();
        Days_adapter.selected_name=new ArrayList<>();

        Night_adapter.selected=new ArrayList<>();
        Night_adapter.selected_name=new ArrayList<>();


        Evening_adapter.selected=new ArrayList<>();
        Evening_adapter.selected_name=new ArrayList<>();


        days.add("Tue");
        days.add("Wed");
        days.add("Thu");
        days.add("Fri");
        days.add("Sat");
        days.add("Sun");
        System.out.println("availability MON : " + data.getMon() + " TUE : " + data.getTue() + " WED " + data.getWed() + " THR " + data.getThu() + " FRI " + data.getFri() + "");
        //   List<String> items = Arrays.asList(data.getMon().split("\\s*,\\s*"));

//PostNewPostion.MON.addAll(Arrays.asList(data.getMon().toLowerCase().split("\\s+")));
        List<String> items = new ArrayList<>();
//                    items.addAll(Arrays.asList(data.getMon().toLowerCase().split("\\s+")));
        items=    Arrays.asList(data.getMon().split("\\s*,\\s*"));
        //  PostNewPostion.MON = new ArrayList<Integer>(items.size());
        PostNewPostion.MON = new ArrayList<>();
        List<Integer> items_int = new ArrayList<>();
        for (int i = 0; i < items.size(); i++) {
            if(  isNumeric(items.get(i) + "")) {
                items_int.add(Integer.parseInt(items.get(i) + ""));
                if (items_int.get(i).equals(1)) {
                    Days_adapter.selected.add("1");
                    Days_adapter.selected_name.add("Mon");
                }
                if (items_int.get(i).equals(2)) {
                    Evening_adapter.selected.add("1");
                    Evening_adapter.selected_name.add("Mon");
                }
                if (items_int.get(i).equals(3)) {
                    Night_adapter.selected.add("1");
                    Night_adapter.selected_name.add("Mon");
                }
            }
        }
        //  List<String> items_tue = new ArrayList<>();
//                    items_tue=(Arrays.asList(data.getTue().split("\\s+")));
        List<String> items_tue = Arrays.asList(data.getTue().split("\\s*,\\s*"));

        //  PostNewPostion.MON = new ArrayList<Integer>(items.size());
        PostNewPostion.TUE = new ArrayList<>();
        List<Integer> items_int_tue = new ArrayList<>();
        //    System.out.println("TUE ```````````@ "+items_tue.toString()+"");
        for (int i = 0; i < items_tue.size(); i++) {
            System.out.println("items_tue.get(i) ````@@@@@@@@@@ "+ items_tue.get(i) + "");
            if(  isNumeric(items_tue.get(i) + "")) {
                items_int_tue.add(Integer.parseInt(items_tue.get(i) + ""));
                if (items_int_tue.get(i).equals(1)) {
                    Days_adapter.selected.add("2");
                    Days_adapter.selected_name.add("Tue");
                }
                if (items_int_tue.get(i).equals(2)) {
                    Evening_adapter.selected.add("2");
                    Evening_adapter.selected_name.add("Tue");
                }
                if (items_int_tue.get(i).equals(3)) {
                    Night_adapter.selected.add("2");
                    Night_adapter.selected_name.add("Tue");
                }
            }
        }


        List<String> items_wed = Arrays.asList(data.getWed().split("\\s*,\\s*"));

        //  PostNewPostion.MON = new ArrayList<Integer>(items.size());
        PostNewPostion.WED = new ArrayList<>();
        List<Integer> items_int_wed = new ArrayList<>();
        //   System.out.println("WED ```````````@ "+items_wed.toString()+"");
        for (int i = 0; i < items_wed.size(); i++) {
//                        System.out.println("items_tue.get(i) ````@@@@@@@@@@ "+ items_tue.get(i) + "");
            if(  isNumeric(items_wed.get(i) + "")) {
                items_int_wed.add(Integer.parseInt(items_wed.get(i) + ""));
                if (items_int_wed.get(i).equals(1)) {
                    Days_adapter.selected.add("3");
                    Days_adapter.selected_name.add("Wed");
                }
                if (items_int_wed.get(i).equals(2)) {
                    Evening_adapter.selected.add("3");
                    Evening_adapter.selected_name.add("Wed");
                }
                if (items_int_wed.get(i).equals(3)) {
                    Night_adapter.selected.add("3");
                    Night_adapter.selected_name.add("Wed");
                }
            }
        }


        List<String> items_Thr = Arrays.asList(data.getThu().split("\\s*,\\s*"));

        //  PostNewPostion.MON = new ArrayList<Integer>(items.size());
        PostNewPostion.THR = new ArrayList<>();
        List<Integer> items_int_thr = new ArrayList<>();
        //  System.out.println("THRU ```````````@ "+items_Thr.toString()+"");
        for (int i = 0; i < items_Thr.size(); i++) {
//                        System.out.println("items_tue.get(i) ````@@@@@@@@@@ "+ items_tue.get(i) + "");
            if(  isNumeric(items_Thr.get(i) + "")) {
                items_int_thr.add(Integer.parseInt(items_Thr.get(i) + ""));
                if (items_int_thr.get(i).equals(1)) {
                    Days_adapter.selected.add("4");
                    Days_adapter.selected_name.add("Thr");
                }
                if (items_int_thr.get(i).equals(2)) {
                    Evening_adapter.selected.add("4");
                    Evening_adapter.selected_name.add("Thr");
                }
                if (items_int_thr.get(i).equals(3)) {
                    Night_adapter.selected.add("4");
                    Night_adapter.selected_name.add("Thr");
                }
            }
        }


        List<String> items_Fri = Arrays.asList(data.getFri().split("\\s*,\\s*"));

        //  PostNewPostion.MON = new ArrayList<Integer>(items.size());
        PostNewPostion.FRI = new ArrayList<>();
        List<Integer> items_int_fri = new ArrayList<>();
        //   System.out.println("FRID ```````````@ "+items_Fri.toString()+"");
        for (int i = 0; i < items_Fri.size(); i++) {
//                        System.out.println("items_tue.get(i) ````@@@@@@@@@@ "+ items_tue.get(i) + "");
            if(  isNumeric(items_Fri.get(i) + "")) {
                items_int_fri.add(Integer.parseInt(items_Fri.get(i) + ""));
                if (items_int_fri.get(i).equals(1)) {
                    Days_adapter.selected.add("5");
                    Days_adapter.selected_name.add("Fri");
                }
                if (items_int_fri.get(i).equals(2)) {
                    Evening_adapter.selected.add("5");
                    Evening_adapter.selected_name.add("Fri");
                }
                if (items_int_fri.get(i).equals(3)) {
                    Night_adapter.selected.add("5");
                    Night_adapter.selected_name.add("Fri");
                }
            }
        }


        List<String> items_Sat = Arrays.asList(data.getSat().split("\\s*,\\s*"));

        //  PostNewPostion.MON = new ArrayList<Integer>(items.size());
        PostNewPostion.FRI = new ArrayList<>();
        List<Integer> items_int_sat = new ArrayList<>();
        //   System.out.println("SAT ```````````@ "+items_Sat.toString()+"");
        for (int i = 0; i < items_Sat.size(); i++) {
//                        System.out.println("items_tue.get(i) ````@@@@@@@@@@ "+ items_tue.get(i) + "");
            if(  isNumeric(items_Sat.get(i) + "")) {
                items_int_sat.add(Integer.parseInt(items_Sat.get(i) + ""));
                if (items_int_sat.get(i).equals(1)) {
                    Days_adapter.selected.add("6");
                    Days_adapter.selected_name.add("Sat");
                }
                if (items_int_sat.get(i).equals(2)) {
                    Evening_adapter.selected.add("6");
                    Evening_adapter.selected_name.add("Sat");
                }
                if (items_int_sat.get(i).equals(3)) {
                    Night_adapter.selected.add("6");
                    Night_adapter.selected_name.add("Sat");
                }
            }
        }


        List<String> items_Sun = Arrays.asList(data.getSun().split("\\s*,\\s*"));

        //  PostNewPostion.MON = new ArrayList<Integer>(items.size());
        PostNewPostion.FRI = new ArrayList<>();
        List<Integer> items_int_sun = new ArrayList<>();
        //   System.out.println("SUN ```````````@ "+items_Sun.toString()+"");
        for (int i = 0; i < items_Sun.size(); i++) {
//                        System.out.println("items_tue.get(i) ````@@@@@@@@@@ "+ items_tue.get(i) + "");
            if(  isNumeric(items_Sun.get(i) + "")) {
                items_int_sun.add(Integer.parseInt(items_Sun.get(i) + ""));
                if (items_int_sun.get(i).equals(1)) {
                    Days_adapter.selected.add("7");
                    Days_adapter.selected_name.add("Sun");
                }
                if (items_int_sun.get(i).equals(2)) {
                    Evening_adapter.selected.add("7");
                    Evening_adapter.selected_name.add("Sun");
                }
                if (items_int_sun.get(i).equals(3)) {
                    Night_adapter.selected.add("7");
                    Night_adapter.selected_name.add("Sun");
                }
            }
        }



        PostNewPostion.MON.addAll(items_int);
        PostNewPostion.TUE.addAll(items_int_tue);
        PostNewPostion.WED.addAll(items_int_wed);
        PostNewPostion.THR.addAll(items_int_thr);
        PostNewPostion.FRI.addAll(items_int_fri);
        PostNewPostion.SAT.addAll(items_int_sat);
        PostNewPostion.SUN.addAll(items_int_sun);

//        System.out.println("this is monday " + PostNewPostion.MON.toString() + "");
//        System.out.println("this is Tuesday " + PostNewPostion.TUE.toString() + "");
//        System.out.println("this is TuesdayDays_adapter.selected_name``````````@ " + Days_adapter.selected_name .toString()+ "");
//        System.out.println("this is TuesdayEvening_adapter.selected_name``````````@ " + Evening_adapter.selected_name.toString() + "");
        //    v_v = LayoutInflater.from(_parent.getContext()).inflate(R.layout.testforflip, _parent, false);
        //    v_v = LayoutInflater.from(_parent.getContext()).inflate(R.layout.testforflip_availability, _parent, false);
        //  holder.row_item_flipper.removeView(v_v);
        // AnimationFactory.flipTransition(holder.row_item_flipper,
        //  AnimationFactory.FlipDirection.LEFT_RIGHT);




        // holder.row_item_flipper.addView(v_v);
        // initView(v_v);
        DaysMultiSelectAdapter_duplicate_flip _dayDaysMultiSelectAdapter;
        _dayDaysMultiSelectAdapter = new DaysMultiSelectAdapter_duplicate_flip(days, ctx);
        griddays.setExpanded(true);
        griddays.setAdapter(_dayDaysMultiSelectAdapter);
        Benefits days_all = new Benefits();
        List<Benefits.BenefitsBean> all_day = new ArrayList<>();
        Benefits.BenefitsBean day = new Benefits.BenefitsBean();


        day.setBenefits_id("1");
        day.setBenefits_name("Mon");
        all_day.add(day);

        day = new Benefits.BenefitsBean();
        day.setBenefits_id("2");
        day.setBenefits_name("Tue");
        all_day.add(day);

        day = new Benefits.BenefitsBean();
        day.setBenefits_id("3");
        day.setBenefits_name("Wed");
        all_day.add(day);


        day = new Benefits.BenefitsBean();
        day.setBenefits_id("4");
        day.setBenefits_name("Thu");
        all_day.add(day);


        day = new Benefits.BenefitsBean();
        day.setBenefits_id("5");
        day.setBenefits_name("Fri");
        all_day.add(day);


        day = new Benefits.BenefitsBean();
        day.setBenefits_id("6");
        day.setBenefits_name("Sat");
        all_day.add(day);


        day = new Benefits.BenefitsBean();
        day.setBenefits_id("7");
        day.setBenefits_name("Sun");
        all_day.add(day);


        days_all.setBenefits(all_day);
        Benefits test = new Benefits();
/*
                           _daDays_checkBoxAdapter = new Days_CheckBoxAdapter(days_all, context, 1,test,true);
                    griddaystime.setAdapter(_daDays_checkBoxAdapter);*/


        _daDays_checkBoxAdapter = new Days_adapter(days_all, ctx, 1, test, true);
        griddaystime.setAdapter(_daDays_checkBoxAdapter);

        _daevening_checkBoxAdapter = new Evening_adapter(days_all, ctx, 2, true);
        grideveningdaystime.setAdapter(_daevening_checkBoxAdapter);


        _daNightcheckBoxAdapter = new Night_adapter(days_all, ctx, 3, true);
        gridnightdaystime.setAdapter(_daNightcheckBoxAdapter);
    }
    private boolean isNumeric(String string) {
        if(Pattern.matches("\\d{1,3}((\\.\\d{1,2})?)", string))
            return true;
        else
            return false;
    }

}
