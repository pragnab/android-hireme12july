package com.hiremehealthcare.activity;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.Spanned;
import android.util.Log;
import android.view.View;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.NetworkResponse;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.firebase.FirebaseApp;
import com.google.firebase.iid.FirebaseInstanceId;
import com.google.firebase.messaging.FirebaseMessaging;
import com.google.gson.Gson;
import com.hiremehealthcare.CommonCls.CustomButton;
import com.hiremehealthcare.CommonCls.DialogUtils;
import com.hiremehealthcare.CommonCls.URLS;
import com.hiremehealthcare.CommonCls.Validations;
import com.hiremehealthcare.POJO.Login;
import com.hiremehealthcare.R;
import com.hiremehealthcare.database.MySharedPrefereces;

import org.json.JSONException;
import org.json.JSONObject;

import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

public class StartingSelectionActivity extends AppCompatActivity implements View.OnClickListener {

    CustomButton btnHealthCareProfessional;
    CustomButton btnNeedToHire;
    MySharedPrefereces mySharedPrefereces;
    RequestQueue queue;
    ProgressDialog progressDialog;
    Context context;
    String TAG = "StartingSelectionActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_starting_selection);
        initAllControls();
        getRegistrationForFirebase();
    }

    /*Pragna*/
    public void getRegistrationForFirebase() {
        //String Urls= URLS.UPDATE_FIREBASE_TOKEN;//+userType+"&"+URLS.USER_ID+"="+userId+"&token="+mySharedPrefereces.getFirebaseToken();
        //String Urls= "http://hiremehealthcare.iipldemo.com/app/api.php?";//+userType+"&"+URLS.USER_ID+"="+userId+"&token="+mySharedPrefereces.getFirebaseToken();
//        String Urls = URLS.GENERATE_DEVICE_TOKEN + Validations.GetDeviceId(getApplicationContext()) + "&token=" + mySharedPrefereces.getFirebaseToken();//+userType+"&"+URLS.USER_ID+"="+userId+"&token="+mySharedPrefereces.getFirebaseToken();
        String Urls = URLS.GENERATE_DEVICE_TOKEN + Validations.GetDeviceId(getApplicationContext()) + "&token=" + mySharedPrefereces.getFirebaseToken() + "&" + URLS.USER_TYPE + "=" + mySharedPrefereces.getUserType() + "&" + URLS.USER_ID + "=" + mySharedPrefereces.getUserId() + "";
        System.out.println("getRegistrationForFirebase " + Urls + "");
        StringRequest postRequest = new StringRequest(Request.Method.POST, Urls,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {

                        Log.d(TAG, "MyFirebaseInstanceID: Response: " + response);
                        // response
                        Log.d("Response", response);
                        try {
                            JSONObject jsonObject = new JSONObject(response);
                            int id = jsonObject.getInt("status");
                            if (id > 0) {

                                //DialogUtils.showDialog4Activity(ctx, ctx.getResources().getString(R.string.app_name), jsonObject.getString("message") + "", null);
                            } else {
                                //DialogUtils.showDialog4Activity(ctx, ctx.getResources().getString(R.string.app_name), jsonObject.getString("message") + "", null);
                            }
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d(TAG, "sendProfileDataToServer: Response: " + error.getMessage() + "");
                        // error
                        Log.d("Error.Response", error.getMessage() + "");


                    }
                }
        ) {
            @Override
            protected Map<String, String> getParams() {
                Map<String, String> params = new HashMap<String, String>();
                params.put("type", "update_token");
                params.put("user_type", mySharedPrefereces.getUserType() + "");
                params.put("user_id", mySharedPrefereces.getUserId() + "");
                params.put("token", mySharedPrefereces.getFirebaseToken() + "");
                Log.d(TAG, "firebase request parameter: request parameters" + params.toString());
                Log.d(TAG, "firebase token" + mySharedPrefereces.getFirebaseToken());
                return params;
            }
        };
        queue.add(postRequest);

    }

    public void initAllControls() {
        context = this;
      /*  FirebaseApp.initializeApp(this);
        FirebaseMessaging.getInstance().setAutoInitEnabled(true);*/
        try {
            FirebaseApp.initializeApp(this);
            String refreshedToken = FirebaseInstanceId.getInstance().getToken();
            Log.d("Firbase id login", "Refreshed token: " + refreshedToken);
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("THIS IS ERROR " + e.getCause() + "");
        }
        progressDialog = new ProgressDialog(this);
        progressDialog.setCancelable(false);
        // System.out.println("MUST PRINT "+     FirebaseInstanceId.getInstance().getToken()+"");
        //   System.out.println("MUST PRINT "+   checkPlayServices()+"");
        queue = Volley.newRequestQueue(context);
        SpannableString spannableString = new SpannableString(getString(R.string.please_wait));
        Typeface typefaceSpan = Validations.setTypeface(this);
        spannableString.setSpan(typefaceSpan, 0, getString(R.string.please_wait).length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        progressDialog.setMessage(spannableString);
        mySharedPrefereces = new MySharedPrefereces(this);
        Log.d(TAG, "initAllControls:token    " + mySharedPrefereces.getFirebaseToken() + "");


        btnHealthCareProfessional = (CustomButton) findViewById(R.id.btnHealthCareProfessional);
        btnNeedToHire = (CustomButton) findViewById(R.id.btnNeedToHire);
        btnHealthCareProfessional.setOnClickListener(this);
        btnNeedToHire.setOnClickListener(this);
        if (mySharedPrefereces.isUserLogin()) {
            checkForAutoLoginCreditial();
            /*if (mySharedPrefereces.getUserType() == 1) {
                Intent i = new Intent(StartingSelectionActivity.this, DashBoardActivity.class);
                startActivity(i);
                finish();
            } else {
                Intent i = new Intent(StartingSelectionActivity.this, DashBoardManagerActivity.class);
                startActivity(i);
                finish();
            }*/
        }

    }

    public void checkForAutoLoginCreditial() {
        if (mySharedPrefereces.getEnterUserName().equals("") || mySharedPrefereces.getEnterPassword().equals(""))
            return;
        progressDialog.show();
        String Url;
        /*if (mySharedPrefereces.getUserType() == 1) {

            Url = URLS.LOGIN_APPLICANT_URL + "&" + URLS.EAMIL + "=" + mySharedPrefereces.getEnterUserName() + "&" + URLS.PASSWORD + "=" + mySharedPrefereces.getEnterPassword();
        } else {
            Url = URLS.LOGIN_MANAGER_URL + "&" + URLS.EAMIL + "=" + mySharedPrefereces.getEnterUserName() + "&" + URLS.PASSWORD + "=" + mySharedPrefereces.getEnterPassword();
        }*/

        if (mySharedPrefereces.getUserType() == 1) {
            Url = URLS.LOGIN_APPLICANT_URL + "&" + URLS.EAMIL + "=" + mySharedPrefereces.getEnterUserName() + "&" + URLS.PASSWORD + "=" + mySharedPrefereces.getEnterPassword() + "&device_id=" + Validations.GetDeviceId(getApplicationContext()) + "" + "&token=" + mySharedPrefereces.getFirebaseToken() + "";
        } else {
            Url = URLS.LOGIN_MANAGER_URL + "&" + URLS.EAMIL + "=" + mySharedPrefereces.getEnterUserName() + "&" + URLS.PASSWORD + "=" + mySharedPrefereces.getEnterPassword() + "&device_id=" + Validations.GetDeviceId(getApplicationContext()) + "" + "&token=" + mySharedPrefereces.getFirebaseToken() + "";
        }
        URL url = null;
        try {
            url = new URL(Url);

            System.out.println("THIS IS FINAL URL TO CHECK AUTO LOGIN ................. " + url + "");
            URI uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(), url.getQuery(), url.getRef());
            url = uri.toURL();
        } catch (MalformedURLException e) {
            System.out.println("ERRRRRRRRRRRRRROOOOOOOOOOOOOOOOOOOOOOOOOOuuu ???????????");
            e.printStackTrace();

        } catch (URISyntaxException e) {
            System.out.println("ERRRRRROOOOOOOOOOOOOOOOTTTTTTTTTTTTT11111 ?????????????");
            e.printStackTrace();
        }

        Log.d(TAG, "CallLogInWS: URL:-" + url.toString());
        StringRequest req = new StringRequest(Request.Method.GET, url.toString(), new Response.Listener<String>() {

            @Override
            public void onResponse(String response) {
                progressDialog.dismiss();
                Log.d(TAG, "CallLogInWS: Response:-" + response);
                Gson gson = new Gson();
                Login logindata = gson.fromJson(response, Login.class);

                if (logindata != null) {
                    if (logindata.getStatus() == 1) {
                        mySharedPrefereces.storeLoginData(logindata.getData().getUser_type(), logindata.getData().getUser_id(), logindata.getData().getFirstname(), logindata.getData().getZipcode(), logindata.getData().getRedirect(), logindata.getData().getRedirect_message(), logindata.getData().getFacility_id(), mySharedPrefereces.getEnterUserName(), mySharedPrefereces.getEnterPassword(), logindata.getData().getCity() + "", logindata.getData().getImage()+"");

                        if (logindata.getData().getUser_type() == 1) {
                            Intent i = new Intent(context, DashBoardActivity.class);
                            finish();
                            startActivity(i);
                        } else {
                            Intent i = new Intent(context, DashBoardManagerActivity.class);
                            i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_NEW_TASK);
                            startActivity(i);
                            finish();
                        }
                    } else {
                        DialogUtils.showDialog4Activity(context, context.getResources().getString(R.string.app_name), logindata.getMessage() + "", null);

                    }
                }

            }
        }, new Response.ErrorListener() {
            @Override
            public void onErrorResponse(VolleyError error) {
                progressDialog.dismiss();
                NetworkResponse networkResponse = error.networkResponse;
                Log.d(TAG, "CallLogInWS: Error:-" + error.getMessage());
                DialogUtils.noInterNetDialog(context, "No Internet Connection", "No active internet connection available", new DialogUtils.NoInterNetDailogClickListner() {
                    @Override
                    public void onDialogOkButtonClicked() {
                        checkForAutoLoginCreditial();
                    }

                    @Override
                    public void onDialogCancelButtonClicked() {
                        finish();
                    }
                });
                /*if (networkResponse != null) {
                    int statusCode = error.networkResponse.statusCode;
                    System.out.println("STATUS CODE " + statusCode + "");
                    DialogUtils.showDialog4Activity(context, context.getResources().getString(R.string.app_name), statusCode + "", null);
                }*/

            }
        });
        req.setRetryPolicy(new DefaultRetryPolicy(
                100000,
                DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));


        queue.add(req);

    }

    @Override
    public void onClick(View view) {
        Intent intent = new Intent(this, LoginActivity.class);
        if (view == btnNeedToHire) {
            //SharedPreferences.Editor editor = getSharedPreferences(SharedPrefNames.MY_PREFS_NAME, MODE_PRIVATE).edit();
            //editor.putString(SharedPrefNames.USER_PROFESSION, "manager");
            //editor.commit();
            mySharedPrefereces.setStartingSelection("manager");
            intent.putExtra("username", "parmarnaresh108@gmail.com");
            //intent.putExtra("username","");
            intent.putExtra("password", "naresh@123");
            //intent.putExtra("password", "");

        } else if (view == btnHealthCareProfessional) {
            //SharedPreferences.Editor editor = getSharedPreferences(SharedPrefNames.MY_PREFS_NAME, MODE_PRIVATE).edit();
            //editor.putString(SharedPrefNames.USER_PROFESSION, "applicant");
            //editor.commit();
            mySharedPrefereces.setStartingSelection("applicant");
            intent.putExtra("username", "iteam.nareshp@gmail.com");
            //intent.putExtra("username","");
            intent.putExtra("password", "naresh@123");
            //intent.putExtra("password", "");

        }


        startActivity(intent);

    }

    int PLAY_SERVICES_RESOLUTION_REQUEST = 0;

    private boolean checkPlayServices() {
        GoogleApiAvailability apiAvailability = GoogleApiAvailability.getInstance();
        int resultCode = apiAvailability.isGooglePlayServicesAvailable(this);
        if (resultCode != ConnectionResult.SUCCESS) {
            boolean goahead = false;
            if (apiAvailability.isUserResolvableError(resultCode)) {
                Dialog dialog = apiAvailability.getErrorDialog(this, resultCode, PLAY_SERVICES_RESOLUTION_REQUEST);
                dialog.setCancelable(false);
                dialog.setCanceledOnTouchOutside(false);
                dialog.show();
            } else {
                Log.i("Demo App", "This device is not supported.");

                AlertDialog.Builder dialog_app = new AlertDialog.Builder(StartingSelectionActivity.this);
                dialog_app.setTitle("Error");
                dialog_app.setMessage("This device is not supported..");
                dialog_app.setCancelable(false);
                dialog_app.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        finish();
                    }
                });
                dialog_app.show();
            }
            return false;
        }
        return true;
    }
}
