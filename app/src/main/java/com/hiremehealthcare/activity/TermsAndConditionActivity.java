package com.hiremehealthcare.activity;

import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Typeface;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;

import com.android.volley.VolleyError;
import com.hiremehealthcare.CommonCls.CustomTextView;
import com.hiremehealthcare.CommonCls.URLS;
import com.hiremehealthcare.CommonCls.Validations;
import com.hiremehealthcare.R;
import com.hiremehealthcare.Utils.CommonRequestHelper;
import com.hiremehealthcare.database.MySharedPrefereces;

import org.json.JSONException;
import org.json.JSONObject;

public class TermsAndConditionActivity extends AppCompatActivity {

    ProgressDialog progressDialog;
    Context context;
    CustomTextView tvTermsAndCondition;
    String TAG="TermsAndConditionActivity";
    MySharedPrefereces mySharedPrefereces;
    TabLayout tabs;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_terms_and_condition);
        initAllControls();
    }
    public void initAllControls()
    {
        context=this;
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        getSupportActionBar().setTitle("Terms & Condition");
        mySharedPrefereces = new MySharedPrefereces(this);
        progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        tvTermsAndCondition = (CustomTextView) findViewById(R.id.tvTermsAndCondition);
        SpannableString spannableString =  new SpannableString(getString(R.string.please_wait));
        Typeface typefaceSpan = Validations.setTypeface(context);
        spannableString.setSpan(typefaceSpan, 0, getString(R.string.please_wait).length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        progressDialog.setMessage(spannableString);
        getDataFromApi();
        tabs = (TabLayout) findViewById(R.id.tabs);
        if(mySharedPrefereces.isUserLogin())
        {
            tabs.setVisibility(View.VISIBLE);
            if(mySharedPrefereces.getUserType()==2)
            {
                setIconToTabManager();
            }
            else
            {
                setIconToTab();
            }
        }
        else
        {
            tabs.setVisibility(View.GONE);
        }
    }

    public void setIconToTabManager() {

       /* tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_home_black_24dp),false);
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_search_black_24dp),false);
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_dashboard_black_24dp),false);
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_assignment_ind_black_24dp),false);
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_chat_black_24dp),false);*/
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_home_black_24dp), false);
//       tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_people_black_24dp), false);
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_search_black_24dp), false);
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_dashboard_black_24dp), false);
//        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_assignment_ind_black_24dp), false);
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_chat_black_24dp), false);
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_receipt_black_24dp), false);
        tabs.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if(DashBoardManagerActivity.viewpager!=null)
                DashBoardManagerActivity.viewpager.setCurrentItem(tab.getPosition());
                finish();
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
        //tabs.setSelected(true,);
        //tabs.getTabAt(4).setIcon(R.drawable.ic_person_black_24dp);


    }
    public void setIconToTab() {

        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_home_black_24dp),false);
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_search_black_24dp),false);
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_favorite_black_24dp),false);
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_chat_black_24dp),false);
        tabs.addTab(tabs.newTab().setIcon(R.drawable.ic_person_black_24dp),false);
        tabs.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                if(SearchPositionDetailActivity.searchPositionDetailActivity!=null)
                {
                    SearchPositionDetailActivity.searchPositionDetailActivity.finish();
                }
                if(DashBoardActivity.viewpager!=null)
                DashBoardActivity.viewpager.setCurrentItem(tab.getPosition());
                finish();
            }
            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
            }
            @Override
            public void onTabReselected(TabLayout.Tab tab) {
            }
        });
        //tabs.setSelected(true,);
        //tabs.getTabAt(4).setIcon(R.drawable.ic_person_black_24dp);


    }


    public void getDataFromApi()
    {
        progressDialog.show();
        String Url= URLS.GET_TERMS_AND_CONDITION;
        Log.d(TAG, "getPrivacyAndPolicy: "+Url);
        CommonRequestHelper.getDataFromUrl(Url, new CommonRequestHelper.VolleyResponseCustom() {
            @Override
            public void getServerResponse(String response) {
                progressDialog.dismiss();
                Log.d(TAG, "getPrivacyAndPolicy: ");
                try {
                    JSONObject jsonObject = new JSONObject(response);
                    JSONObject innerJsonObject = jsonObject.getJSONObject("message");
                    String body = innerJsonObject.getString("body");
                    if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.N) {
                        tvTermsAndCondition.setText(Html.fromHtml(body,Html.FROM_HTML_MODE_LEGACY));
                    } else {
                        tvTermsAndCondition.setText(Html.fromHtml(body));
                    }
                } catch (JSONException e) {
                    e.printStackTrace();
                }


            }

            @Override
            public void getVolleyError(VolleyError volleyError) {
                progressDialog.dismiss();
            }
        },context);
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId()==android.R.id.home)
            onBackPressed();
        return true;
    }
}
