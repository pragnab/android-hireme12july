package com.hiremehealthcare.CommonCls;

import android.app.Application;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AlertDialog;
import android.text.SpannableString;
import android.text.Spanned;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.hiremehealthcare.R;


/**
 * Created by ADMIN on 03-05-2017.
 */

public class DialogUtils extends Application {
    static ProgressDialog m_Dialog = null;
    private CustomTextView mTitileTextView;
    private CustomTextView mMsgTextView;
    private Button mDialogButtonOKButton;

    public static ProgressDialog showProgressDialog(Context context, String text) {
        m_Dialog = new ProgressDialog(context);
        SpannableString spannableString = new SpannableString(text);
        Typeface typefaceSpan = Validations.setTypeface(context);
        spannableString.setSpan(typefaceSpan, 0, text.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        m_Dialog.setMessage(spannableString);

        m_Dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        m_Dialog.setCancelable(true);
        m_Dialog.show();
        return m_Dialog;
    }

    public static void hideProgressDialog() {
        if (m_Dialog != null) {
            if (m_Dialog.isShowing()) {
                m_Dialog.dismiss();
            }
        }

    }

    public static ProgressDialog showProgressDialogNotCancelable(Context context, String text) {
        m_Dialog = new ProgressDialog(context);
        SpannableString spannableString = new SpannableString(text);
        Typeface typefaceSpan = Validations.setTypeface(context);
        spannableString.setSpan(typefaceSpan, 0, text.length(), Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        m_Dialog.setMessage(spannableString);
        m_Dialog.setProgressStyle(ProgressDialog.STYLE_SPINNER);
        m_Dialog.setCancelable(false);
        m_Dialog.show();
        return m_Dialog;
    }

    public static Toast Show_Toast(Context ctx, String msg) {
        Toast t = new Toast(ctx);
        t.setText(msg + "");

        t.setDuration(Toast.LENGTH_SHORT);
        t.show();
        return t;
    }

    /**
     * DISPLAY DIALOG
     **/
    public static void showDialog(final Context context, String title, String message) {

        LayoutInflater inflater = LayoutInflater.from(context);
        final View dialogView = inflater.inflate(R.layout.aommoncls_dialogbox, null);
        CustomTextView titileTextView = (CustomTextView) dialogView.findViewById(R.id.tv_titile);
        CustomTextView msgTextView = (CustomTextView) dialogView.findViewById(R.id.tv_msg);
        Button dialogButtonOKButton = (Button) dialogView.findViewById(R.id.dialogButtonOK);
        titileTextView.setTypeface(Validations.setTypeface(context));
        msgTextView.setTypeface(Validations.setTypeface(context));
        dialogButtonOKButton.setTypeface(Validations.setTypeface(context));
        msgTextView.setText(message + "");
        titileTextView.setText(title + "");
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);

        //  final AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.myDialog));
        final AlertDialog b = builder.create();
        //  builder.setTitle("Material Style Dialog");
        builder.setCancelable(true);
        builder.setView(dialogView);
        b.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        final AlertDialog show = builder.show();
        dialogButtonOKButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                show.dismiss();
            }
        });
    }

    public static void noInterNetDialog(final Context context, String title, String message, final NoInterNetDailogClickListner noInterNetDailogClickListner) {

        LayoutInflater inflater = LayoutInflater.from(context);
        final View dialogView = inflater.inflate(R.layout.no_inter_net_dialog, null);
        CustomTextView titileTextView = (CustomTextView) dialogView.findViewById(R.id.tv_titile);
        CustomTextView msgTextView = (CustomTextView) dialogView.findViewById(R.id.tv_msg);
        CustomButton dialogButtonOKButton = (CustomButton) dialogView.findViewById(R.id.dialogButtonOK);
        CustomButton dialogButtonCancel = (CustomButton) dialogView.findViewById(R.id.dialogButtonCancel);
        titileTextView.setTypeface(Validations.setTypeface(context));
        msgTextView.setTypeface(Validations.setTypeface(context));
        dialogButtonOKButton.setTypeface(Validations.setTypeface(context));
        msgTextView.setText(message + "");
        titileTextView.setText(title + "");
        final AlertDialog.Builder builder = new AlertDialog.Builder(context);

        //  final AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.myDialog));
        final AlertDialog b = builder.create();
        //  builder.setTitle("Material Style Dialog");
        builder.setCancelable(true);
        builder.setView(dialogView);
        b.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        final AlertDialog show = builder.show();
        dialogButtonOKButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                show.dismiss();
                noInterNetDailogClickListner.onDialogOkButtonClicked();
            }
        });
        dialogButtonCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                show.dismiss();
                noInterNetDailogClickListner.onDialogCancelButtonClicked();
            }
        });
    }

    public interface NoInterNetDailogClickListner {
        public void onDialogOkButtonClicked();

        public void onDialogCancelButtonClicked();
    }

    public interface DailogCallBackOkButtonClick {
        public void onDialogOkButtonClicked();

    }

    /*THEME */
    public static void showDialog4Activity(final Context context, String title, String message, final DailogCallBackOkButtonClick dailogCallBackOkButtonClick) {

        LayoutInflater inflater = LayoutInflater.from(context);
        final View dialogView = inflater.inflate(R.layout.aommoncls_dialogbox, null);
        CustomTextView titileTextView = (CustomTextView) dialogView.findViewById(R.id.tv_titile);
        CustomTextView msgTextView = (CustomTextView) dialogView.findViewById(R.id.tv_msg);
        Button dialogButtonOKButton = (Button) dialogView.findViewById(R.id.dialogButtonOK);
        titileTextView.setTypeface(Validations.setTypeface(context));
        msgTextView.setTypeface(Validations.setTypeface(context));
        dialogButtonOKButton.setTypeface(Validations.setTypeface(context));
        msgTextView.setText(message + "");
        titileTextView.setText(title + "");
//        final AlertDialog.Builder builder = new AlertDialog.Builder(context);
//
        final AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(context, R.style.myDialog));
        final AlertDialog b = builder.create();
        //  builder.setTitle("Material Style Dialog");
        builder.setCancelable(true);
        builder.setView(dialogView);
        b.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));


        final AlertDialog show = builder.show();
        dialogButtonOKButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                show.dismiss();
                if (dailogCallBackOkButtonClick != null)
                    dailogCallBackOkButtonClick.onDialogOkButtonClicked();
            }
        });
    }


}
