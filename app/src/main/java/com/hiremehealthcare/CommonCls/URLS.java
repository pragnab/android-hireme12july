package com.hiremehealthcare.CommonCls;

/**
 * Created by ADMIN on 26-04-2018.
 */

public class URLS {
    public static final String PHONE = "phone";

    //    public static String MAIN_URL = "http://hiremehealthcare.iipldemo.com/app/api.php?";
//    public static String MAIN_URL = "http://hiremehealthcare.com/app/api.php?";//changed main URL on 7 SEPT 2018 and released 2nd vertion
    public static String MAIN_URL = "https://hiremehealthcare.com/app/api.php?";//changed main URL on 7 SEPT 2018 and released 2nd vertion
    public static String GENERATE_DEVICE_TOKEN = MAIN_URL + "type=generate_device_token&device_id=";
    public static String GENERATE_DEVICE_TOKEN1 = MAIN_URL + "type=generate_device_token1&device_id=";
    //public static  String MAIN_URL="http://192.168.30.8/trainee/hiremehealthcare/app/api.php?";
//    public static  String LOGIN_APPLICANT_URL=MAIN_URL+"type=applicant_login";

    public static String MANAGER_SEARCH_CANDIDATE = MAIN_URL + "ios=1&type=search_candidates&zipcode=";
    //public static String MANAGER_NEAR_CANDIDATE = MAIN_URL + "type=candidates_near_you&manager_zipcode=";
    // http://hiremehealthcare.iipldemo.com/app/api.php?type=candidate_details&facility_zipcode=53204&candidate_id=3    public static String MANAGER_CANDIDATE_DETAIL = MAIN_URL + "type=candidate_details&facility_zipcode=";
    public static String LOGIN_APPLICANT_URL = MAIN_URL + "type=manage_login_applicant";
    public static String MANAGER_NEAR_CANDIDATE = MAIN_URL + "ios=1&type=candidates_near_you&manager_zipcode=";
    public static String BENEFITS_URL = MAIN_URL + "type=masters&master_name=benefits";
    public static String CERTIFICATION_URL = MAIN_URL + "type=masters&master_name=certification";
    public static String HOURS_URL = MAIN_URL + "type=masters&master_name=hours";
    public static String LICENSE_URL = MAIN_URL + "type=masters&master_name=license";
    public static String CITY_URL = MAIN_URL + "type=get_city&state_id=";
    public static String STATE_URL = MAIN_URL + "type=masters&master_name=state";
    public static String MONTH_URL = MAIN_URL + "type=masters&master_name=month";
    public static String MANAGER_FACILITY_DETAIL = MAIN_URL + "type=facility_details&manager_id=";
    public static String SHIFT_URL = MAIN_URL + "type=masters&master_name=shift";
    public static String MANAGER_CANDIDATE_DETAIL = MAIN_URL + "type=candidate_details&facility_zipcode=";
    public static String GET_TERMS_AND_CONDITION = MAIN_URL + "type=get_cms&cms_type=1";
    public static String GET_PRIVACY_AND_POLICY = MAIN_URL + "type=get_cms&cms_type=2";
    public static String PROFILE_PERSONALDETAIL_URL = MAIN_URL + "type=get_applicant_profile&applicant_id=";
    public static String PROFILE_APPLICANT_EDUCATION_DETAIL_URL = MAIN_URL + "type=get_applicant_education_detail&applicant_id=";
    public static String SEARCH_POSITIONS_FOR_APPLICANT = MAIN_URL + "type=search_position&";
    public static String PROFILE_APPLICATION_EDUCATION_INSERT = MAIN_URL + "type=manage_applicant_education_detail";
    public static String PROFILE_APPLICATION_EDUCATION_DELETE = MAIN_URL + "type=delete_applicant_education_detail";
    public static String PROFILE_APPLICATION_CERTIFICATE_DELETE = MAIN_URL + "type=delete_applicant_certificate_detail";
    public static String DELETE_APPLICANT_EXPERIENCE = MAIN_URL + "type=delete_applicant_experience_detail";
    public static String UPDATE_APPLICANT_PROFILE_URL = MAIN_URL + "type=update_applicant_profile&applicant_id=";
    public static String APPLICANTCERTIFICATES = MAIN_URL + "type=get_applicant_certificate_detail&applicant_id=";
    public static String ManageApplicantCertificates = MAIN_URL + "type=manage_applicant_certificate_detail&applicant_id=";
    public static String DeleteApplicantCertificate = MAIN_URL + "type=delete_applicant_certificate_detail&applicant_id=";
    public static String DELETEAPPLICANTEXPERIENCE = MAIN_URL + "type=delete_applicant_experience_detail&applicant_id=";
    public static String INSERT_APPLICANT_EXPERIENCE = MAIN_URL + "type=manage_applicant_experience_detail";
    public static String APPLICANT_EXPERIENCE_LIST = MAIN_URL + "type=get_applicant_experience_detail&applicant_id=";
    public static String APPLICANT_CERTIFICATE_LIST = MAIN_URL + "type=get_applicant_certificate_detail&applicant_id=";
    public static String APPLICANT_NEAR_POSITION = MAIN_URL + "type=positions_near_you&applicant_id=";
    public static String APPLICANT_SHOW_INTEREST_ON_POSITION = MAIN_URL + "type=candidate_show_interest_on_position&applicant_id=";
    public static String APPLICANT_NEAR_POSITION_DETAIL = MAIN_URL + "type=position_detail&position_id=";
    public static String MANAGER_GET_QUALIFIED_CANDIDATES_FOR_POSITION = MAIN_URL + "ios=1&type=get_qualified_candidates_for_position&manager_id=";
    public static String GET_APPLICANT_LICENCE_DETAIL = MAIN_URL + "type=get_applicant_license_detail&applicant_id=";
    public static String DELETE_APPLICANT_LICENCE_DETAIL = MAIN_URL + "type=delete_applicant_license_detail&applicant_id=";
    public static String GET_MANAGER_PROFILE_DETAIL = MAIN_URL + "type=get_manager_profile&manager_id=";
    public static String UPDATE_MANAGER_PROFILE_DETAIL = MAIN_URL;//+"type=get_manager_profile&manager_id=";
    public static String MANAGE_APPLICANT_LICECNSE = MAIN_URL;
    public static String ApplicantExperience = MAIN_URL + "type=get_applicant_experience_detail&applicant_id=";
    public static String INSERTUPDATEAPPLICANTEXPERIENCE = MAIN_URL + "type=manage_applicant_experience_detail&applicant_id=";
    public static String POST_NEW_POSITION = MAIN_URL + "type=post_new_position";//ppppppppppppppppp
    /*23-march pragna*/
    public static String DELETE_POSITION = MAIN_URL + "type=delete_position&position_id=";//ppppppppppppppppp
    public static String APPLICANT_AVAILABILITY = MAIN_URL + "type=manage_applicant_availability&applicant_id=";//ppppppppppppppppp
    public static String GET_APPLICANT_AVAILABILITY = MAIN_URL + "type=get_applicant_availability&applicant_id=";//ppppppppppppppppp
    public static String MANAGE_APPLICANT_CERTIFICATION = MAIN_URL + "type=manage_applicant_certificate_detail&applicant_id=";
    public static String GetManagerProfile = MAIN_URL + "type=get_manager_profile&manager_id=";
    public static String UpdateManagerProfile = MAIN_URL + "type=manage_manager_profile&manager_id=";    //specialty,year,Year_Exp,year_exp_cer
    public static String SPECIALTY_URL = MAIN_URL + "type=masters&master_name=specialty";
    public static String YEAR_URL = MAIN_URL + "type=masters&master_name=year";
    public static String YEAR_EXP_URL = MAIN_URL + "type=masters&master_name=year_exp";
    public static String YEAR_EXP_CER_URL = MAIN_URL + "type=masters&master_name=year_exp_cer";
    public static String LOGIN_MANAGER_URL = MAIN_URL + "type=manage_login_manager";
    public static String COMMUNICATION = MAIN_URL + "type=communication";
    public static String GET_TOTAL_MESSAGE = MAIN_URL + "type=get_total_message&user_type=";
    public static String UPDATE_FIREBASE_TOKEN = MAIN_URL;//+ "type=update_token&user_type=";
    public static String GET_APPLICANT_STATUS = MAIN_URL + "type=get_applicant_status&application_id=";
    public static String SELECT_INTERVIEW_TIME = MAIN_URL + "type=select_interview_time&application_id=";
    public static String UPDATE_ITER_VIEW_SCHEDULE = MAIN_URL + "type=update_interview_schedule";
    public static String UPLOAD_APPLICANT_IMAGE = MAIN_URL; //+ "type=upload_image_applicant";
    public static String GET_INTERVIEW_TIMMING = MAIN_URL + "type=get_interview_timings&application_id=";
    public static String CONTACT_CANDIDATE = MAIN_URL + "type=contact_candidate";
    //public static String SET_INTERVIEW_SCHEDULE = MAIN_URL + "type=set_interview_schedule";
    public static String GET_CHAT_DATA = MAIN_URL + "type=get_chat_data";
    public static String GET_LAST_READ_MESSAGE = MAIN_URL + "type=read_last_message";
    public static String STORE_CHAT_MESSAGE = MAIN_URL + "type=store_chat_messages";
    public static String REMOVE_COMMUNICATION = MAIN_URL + "type=remove_application&application_id=";
    public static String EAMIL = "email";
    public static String PASSWORD = "password";
    public static String REGISTER_APPLICANT_URL = MAIN_URL + "type=manage_register_applicant";
    public static String MY_POSITION_ACTIVE = MAIN_URL + "type=my_positions_active&manager_id=";
    public static String MY_POSITION_EXPIRE = MAIN_URL + "type=my_positions_expired&manager_id=";
    public static String MY_POSITION_PANDING = MAIN_URL + "type=my_positions_pending&applicant_id=";
    public static String CONFIRM_INTERVIEW_SHEDULE = MAIN_URL + "type=confirm_interview_schedule";
    public static String CONFIRM_APPLICANT_PROFILE = MAIN_URL + "type=confirm_applicant_profile&applicant_id=";
    public static String UPDATE_INTERVIEW_SHEDULE = MAIN_URL + "type=update_interview_schedule";
    public static String SET_INTERVIEW_SHEDULE = MAIN_URL + "type=set_interview_schedule";
    public static String SCHEDULE_INTERVIEW_REJECTED = MAIN_URL + "type=schedule_interview_rejected";
    public static String SCHEDULE_INTERVIEW_HIRED = MAIN_URL + "type=schedule_interview_hired";
    public static String SCHEDULED_INTERVIEWS_UPCOMING = MAIN_URL + "type=scheduled_interviews_upcoming";
    public static String SCHEDULED_INTERVIEWS_PREVIOUS = MAIN_URL + "type=scheduled_interviews_previous";
    public static String MY_POSITION_REMOVE = MAIN_URL + "type=delete_job&job_id=";
    public static String MY_POSITION_PROCESSING = MAIN_URL + "type=my_positions_processing&applicant_id=";
    public static String REGISTER_MANAGER_URL = MAIN_URL + "type=manage_register_manager";
    public static String CHANGE_PASSWORD_MANAGER_URL = MAIN_URL + "type=change_password_manager";
    public static String CHANGE_PASSWORD_APPLICANT_URL = MAIN_URL + "type=change_password_applicant";
    public static String FORGOT_APPLICANT_URL = MAIN_URL + "type=manage_forgot_password_applicant";
    public static String FORGOT_MANAGER_URL = MAIN_URL + "type=manage_forgot_password_manager";
    public static String GETTING_BILLING_CONFIRMATION = MAIN_URL + "type=manage_facility_number";
    /*11-july Pragna*/
    public static String SHARE_POSITION = MAIN_URL + "type=share_position&position_id=";
    public static String GET_POSITION_AVAILABILITY = MAIN_URL + "type=get_position_availability&position_id=";
    public static String FACILITY_NUMBER = "facility_number";
    public static String BILLING_CONFIRMATION_NUMBER = "facility_number";
    public static String FIRST_NAME = "firstname";
    public static String LAST_NAME = "lastname";
    public static String USER_ID = "user_id";
    public static String USER_TYPE = "user_type";
    public static String ZIP_CODE = "zipcode";
    public static String LICENSE_ID = "license_id";
    public static String MILES = "miles";
    public static String APPLICANT_ZIP_CODE = "applicant_zipcode";
    public static String POSITION_ID = "position_id";
    public static String LICENSE = "license";
    public static String SPECIALITIES = "specialities";
    public static String SHIFT = "shift";
    public static String HOURS = "hours";
    public static String BENEFITS = "benefits";
    public static String CUSTOM_BENEFITS = "custom_benifits";
    public static String EXPERIENCE_YEAR = "exp_year";
    public static String EXPERIENCE_MONTH = "exp_month";
    public static String POSITION_DESCRIPTION = "position_description";
    public static String FACILITY_ID = "facility_id";
    public static String CONFIRM_PASSWORD = "conpassword";
    public static String EDUCATION_ID = "education_id";
    public static String MANAGER_ID = "manager_id";
    public static String ID = "id";
    public static String CHAT_MESSAGE = "chat_message";
    public static String APPLICATION_ID = "application_id";
    public static String OPTION = "option";
    public static String DATE_OPTION = "date_option";
    public static String HOURS_OPTION = "hours_option";
    public static String MINUTES_OPTION = "minutes_option";
    public static String FORMAT_OPTION = "format_option";
    public static String INTERVIEW_OPTIONS = "interview_options";
    public static String INTERVIEW_SCHEDULED_ID = "interview_scheduled_id";
    public static String SPECIALTY = "specialty";
    public static String OLD_PASSWORD = "old_password";
    public static String NEW_PASSWORD = "new_password";
    public static String CONFIRM_PASSWORD_CHANGE = "confirm_password";
    public static String INSTITUDE = "institute";
    public static String DEGREE = "degree";
    public static String MONTH = "month";
    public static String YEAR = "year";
    public static String APPLICANT_ID = "applicant_id";
    public static String CERTIFICATE_ID = "certificate_id";
    public static String CANDIDATE_ID = "candidate_id";
    public static String CERTIFICATE = "certificate";
    public static String CERTIFICATION = "certification";
    public static String NEVEREXPIRE = "neverexpire";
    public static String EARNED_MONTH = "earned_month";
    public static String EARNED_YEAR = "earned_year";
    public static String EXPIRE_MONTH = "expire_month";
    public static String EXPIRE_YEAR = "expire_year";
    public static String EXPERIENCE_ID = "experience_id";
    public static String LOCATION = "location";
    public static String SPECIALITY = "speciality";
    public static String FIRSTNAME = "firstname";
    public static String LASTNAME = "lastname";
    public static String EMAIL = "email";
    public static String ZIPCODE = "zipcode";

}
