package com.hiremehealthcare.CommonCls;

import android.app.Application;

/**
 * Created by ADMIN on 26-04-2018.
 */

public class SharedPrefNames extends Application
 {
     public static final String MY_PREFS_NAME = "HireMeHealthCcare";
     public static final String MY_PREFS_NAME_REMEMBER = "HireMeHealthCcareGlobal";
     public static final String IS_STEP_ONE_CLEAR = "IS_STEP_ONE_CLEAR";
     public static final String CITY = "CITY";
     public static final String USER_PHOTO = "USER_PHOTO";
     public static final String USER_TYPE = "USER_TYPE";
     public static final String REMEMBER_USER_NAME = "REMEMBER_USER_NAME";
     public static final String REMEMBER_USER_PASSWORD = "REMEMBER_USER_PASSWORD";
     public static final String IS_REMEMBER_PASSWORD = "IS_REMEMBER_PASSWORD";
     public static final String FACELITY_ID = "FACELITY_ID";
     public static final String USER_FNAME = "USER_FNAME";
     public static final String ENTER_USER_NAME = "ENTER_USER_NAME";
     public static final String ENTER_PASSWORD = "ENTER_PASSWORD";
     public static final String USER_PROFESSION = "profession";
     public static final String USER_ID = "USER_ID";
     public static final String ZIP_CODE = "ZIP_CODE";
     public static final String REDIRECT = "REDIRECT";
     public static final String REDIRECT_MESSAGE = "REDIRECT_MESSAGE";
     public static final String IS_USER_LOGIN = "IS_USER_LOGIN";
     public static final String USER_IMAGE = "USER_IMAGE";
     public static final String MY_FIREBASE_TOKEN = "MY_FIREBASE_TOKEN";

}
