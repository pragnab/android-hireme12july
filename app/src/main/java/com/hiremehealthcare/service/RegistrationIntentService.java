package com.hiremehealthcare.service;

import android.app.IntentService;
import android.content.Intent;
import android.util.Log;

import com.google.firebase.iid.FirebaseInstanceId;

/**
 * Created by ADMIN on 26-07-2018.
 */

public class RegistrationIntentService extends IntentService {

    // abbreviated tag name
    private static final String TAG = RegistrationIntentService.class.getSimpleName();
    public RegistrationIntentService() {
        super(TAG);
    }
    String token;
    @Override
    protected void onHandleIntent(Intent intent) {

        // Make a call to Instance API

        FirebaseInstanceId instanceID = FirebaseInstanceId.getInstance();


        //String senderId = getResources().getString(R.string.gcm_defaultSenderId);

        // request token that will be used by the server to send push notifications
        token = instanceID.getToken();
        Log.d(TAG, "FCM Registration Token: " + token);

    }
}