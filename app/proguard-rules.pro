# Add project specific ProGuard rules here.
# You can control the set of applied configuration files using the
# proguardFiles setting in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}

# Uncomment this to preserve the line number information for
# debugging stack traces.
#-keepattributes SourceFile,LineNumberTable

# If you keep the line number information, uncomment this to
# hide the original source file name.
#-renamesourcefileattribute SourceFile
-keepattributes Signature
# POJOs used with GSON
# The variable names are JSON key values and should not be obfuscated
#-keepclassmembers class com.example.apps.android.Categorias { <fields>; }
#-keep class class com.hiremehealthcare.POJO.** { <fields>; }
-keep public class com.hiremehealthcare.POJO.** { *; }
#-keep class class com.hiremehealthcare.POJO.** { *; }
#-keep private class com.hiremehealthcare.POJO.**
# Keep the annotations
-keepattributes *Annotation*
-keepattributes *Annotation*
# You can apply the rule to all the affected classes also
# -keepclassmembers class com.example.apps.android.model.** { <fields>; }
-keepclassmembers,allowshrinking,allowobfuscation class com.android.volley.NetworkDispatcher {
    void processRequest();
}
-keepclassmembers,allowshrinking,allowobfuscation class com.android.volley.CacheDispatcher {
    void processRequest();
}
-keep public class * implements com.bumptech.glide.module.GlideModule
-keep public class * extends com.bumptech.glide.module.AppGlideModule
-keep public enum com.bumptech.glide.load.ImageHeaderParser$** {
  **[] $VALUES;
  public *;
}
#-keepclassmembers class com.hiremehealthcare.POJO.* {  public *;}